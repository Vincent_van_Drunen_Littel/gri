trigger CalendarInviteTrigger on Calendar_Invite__c (after insert, after update, before delete) {
    
    set<Id> calendarInviteIds = new set<Id>();
    set<string> calendarInviteOldIds = new set<string>();
    Map<Id,Boolean> calInviteToEmailMap = new Map<Id,Boolean>();
    //Boolean isEmailSend=false;
    if(trigger.isAfter){
    
        if(Trigger.isInsert ){ 
            for(Calendar_Invite__c calendarInvites:trigger.new){
                if(calendarInvites.start_Date__c != null && calendarInvites.End_Date__c != null ){
                    calendarInviteIds.add(calendarInvites.Id);
                    calInviteToEmailMap.put(calendarInvites.Id,true);
                }
            }
        }
         
    }
    if(trigger.isBefore){
        if(trigger.isDelete){
            for(Calendar_Invite__c calObj:Trigger.old){
                calendarInviteOldIds.add(calObj.GoogleCalendarEventId__c);
            }
        }
    } 
    
    if(trigger.isUpdate && trigger.isAfter){
        for(Calendar_Invite__c calendarInvites:trigger.new){
            if(calendarInvites.EventTimeZone__c != trigger.oldmap.get(calendarInvites.id).EventTimeZone__c || 
                 calendarInvites.Invite_Type__c != trigger.oldmap.get(calendarInvites.id).Invite_Type__c || 
                 calendarInvites.Description__c != trigger.oldmap.get(calendarInvites.id).Description__c || 
                 calendarInvites.Start_Date__c != trigger.oldmap.get(calendarInvites.id).Start_Date__c || 
                 calendarInvites.End_Date__c != trigger.oldmap.get(calendarInvites.id).End_Date__c ||
                 calendarInvites.Venue__c != trigger.oldmap.get(calendarInvites.id).Venue__c){
                 calendarInviteIds.add(calendarInvites.Id);
                 if(calendarInvites.Start_Date__c != trigger.oldmap.get(calendarInvites.id).Start_Date__c || 
                     calendarInvites.End_Date__c != trigger.oldmap.get(calendarInvites.id).End_Date__c ||
                     calendarInvites.Venue__c != trigger.oldmap.get(calendarInvites.id).Venue__c){
                     calInviteToEmailMap.put(calendarInvites.Id,true);
                 }else{
                     calInviteToEmailMap.put(calendarInvites.Id,false);
                 }
            }
            
        }
    
    }
    system.debug('@@calendarInviteIds'+calendarInviteIds);
    
    system.debug('@@calInviteToEmailMap'+calInviteToEmailMap);
    if(!RecursionCls.isBatchAlreadyRun){
        if(Trigger.isInsert || ( !RecursionCls.isUpdateLogicNotRequired && Trigger.isUpdate)){
            if(calendarInviteIds.size()>0){
                CalendarInviteHandler.CreateInviteInGoolgeCalendar(calendarInviteIds,'insrtUpd',calInviteToEmailMap);
            }
        }
        
        if(Trigger.isBefore && trigger.isDelete){
            if(calendarInviteOldIds.size()>0){
                CalendarInviteHandler.DeleteCalendarEvent(calendarInviteOldIds,'delete');
            }
        }
    }
    
}