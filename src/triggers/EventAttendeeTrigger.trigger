/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * Trigger for the sObject Event_Attendee__c
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * 24/07/2018  AndersonP      new trigger for Event_Attendee__c sObject.
 */ 
trigger EventAttendeeTrigger on Event_Attendee__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) 
{
    if (Trigger.isBefore) 
    {
        EventAttendeeHandler.before();
    } 
    else if (Trigger.isAfter) 
    {
        EventAttendeeHandler.after();
    }
}