/*

PAH 2016-07-29 this was developed By Sneha to calcaulte the VAT field

Ed is replacing the the Process to populate the Opportunity.Company_Vat_Number__c and the Opportunity.SagePay_VendorName__c from either the Event or the Club
Sneha - 28/3/2017 Added some filed updated on before and update, to replicate workflows and process builders causing issue in mass op

GRI Dev added one line to call their class from this trigger to avoid having 2 triggers  OpportunityVinculaPricebook.execute();

Sneha - 11/10/2017 removed condition for waiving vat for UK, it should always be charged

Sneha - 01/03/2018 added spain vat condition for retreat

Anderson - 24/07/2018 - OpportunityVinculaPricebook.execute() removed

RM - 27/02/2019 - Added Portugal
*/

trigger VATonOpportunityTrigger on Opportunity (before insert, before update) 
{
    System.debug('################################# VATonOpportunityTrigger');

    Set<id> oppIds = new Set<id>();
    Set<id> eventIds = new Set<id>();
    Set<id> clubIds = new Set<id>();
    Set<id> conIds = new Set<id>();
    private Opportunity sorder;
    public List<OpportunityLineItem> lines {get; set;}
    public List<Event__c> eventlines {get; set;}
    public List<Club__c> clublines {get; set;}
    public List<Contact> conlines {get; set;}
    public decimal vatswiss;
    
    public decimal vatspainevent;
   
    public boolean matchfound = false;
    public List<Quote> quotesToUpdate {get; set;}
    Boolean updatequote = false;
    Boolean updatingnow = false;
     
    if(Trigger.isInsert)
    {
        //Classe incluida de acordo com a solicitação da GRI 23/03/2017
        //OpportunityVinculaPricebook.execute();
        
        for (Opportunity op : Trigger.new)
        {
            oppIds.add(op.Id);
            eventIds.add(op.Event__c);
            clubIds.add(op.Club__c);
            conIds.add(op.Contact__c);
        } 
        
        eventlines = [SELECT Company_Name__c, Vendor_Name__c 
                FROM Event__c WHERE Id = :eventIds];
        clublines  = [SELECT Company_Name__c, Vendor_Name__c 
                FROM Club__c WHERE Id = :clubIds];
        conlines  = [SELECT Salutation, LastName, FirstName, Email, MailingStreet, MailingState, MailingPostalCode, MailingCountryCode, MailingCountry, MailingCity
                FROM Contact WHERE Id = :conIds];     
              
        for (Opportunity thisopp :System.Trigger.new) 
        {
            //Update_RE_Club_Vat__c
            if(thisopp.Re_Club_Opp__c == true)
            {       
                thisopp.Applicable_VAT_Rate__c = '20';
                thisopp.CompanyName__c = 'GRI Services Ltd';    
                thisopp.Company_Vat_Number__c = 'GB854134336';    
                thisopp.Vendor_Name__c = 'globalreal1';              
            }
            //update billing country name
             if(thisopp.Billing_Country_Name__c == '' || thisopp.Billing_Country_Name__c == null)
            {
                if(thisopp.Billing_Country__c != '' || thisopp.Billing_Country__c != null) 
                {      
                    string ccode = thisopp.Billing_Country__c;
                    string countrynm = incomeUtil.countryName(ccode);    
                    thisopp.Billing_Country_Name__c = countrynm; 
                }
            } 
            //update record type event vat fields      
            if(thisopp.OppRecordTypeName__c == 'Events')
            {
                for(Event__c thisItem : eventlines)
                {
                    thisopp.CompanyName__c = thisitem.Company_Name__c;
                    thisopp.Vendor_Name__c = thisitem.Vendor_Name__c;
                }
            }
            //update record type club vat fields 
            else if(thisopp.OppRecordTypeName__c == 'Membership')
            {
                for(Club__c thisItem1 : clublines)
                {
                    thisopp.CompanyName__c = thisItem1.Company_Name__c;
                    thisopp.Vendor_Name__c = thisItem1.Vendor_Name__c;
                }
            }           
        }       
    }
    
    if(Trigger.isUpdate)
    {
        System.debug('EADLOG ---------- inicio da trigger ----------- ' +system.now());
        //added by sneha to grab bucket user

        quotesToUpdate = new List<Quote>();

        string uid = userinfo.getUserId();
        System.debug('#### uid: ' + uid);
        //Select u.ProfileId, u.Id From User u where Id = '00536000002XB5uAAG'
        User sprofile = [SELECT Id,Name,ProfileId FROM User WHERE Id = :uid];
        Id thisPrId = sprofile.ProfileId;
        System.debug('#### thisPrId: ' + thisPrId);
        // erase it
        System.debug('<<<<<<<<<<<>>>>>>'+Application_Config__c.getInstance('BucketUserId'));

        ID bucketuserid = (ID)string.valueOf(Application_Config__c.getInstance('BucketUserId').Value__c);
        System.debug('#### bucketuserid: ' + bucketuserid);
        List<Allowed_Profiles__c> mcs = Allowed_Profiles__c.getall().values();
        System.debug('#### mcs: ' + mcs);

        for(Allowed_Profiles__c curr : mcs)
        {
            System.debug('#### curr.Value__c: ' + curr.Value__c);
            System.debug('#### curr.Name: ' + curr.Name);
            if(curr.Value__c == thisPrId)
            {
                matchfound = true;      //do not do anything
            }
        }

        for (Opportunity oppty : trigger.new)
        {
            System.debug('#### oppty.OwnerId: ' + oppty.OwnerId);
            System.debug('#### trigger.oldMap.get(oppty.Id).OwnerId: ' + trigger.oldMap.get(oppty.Id).OwnerId);

            if ((oppty.OwnerId != trigger.oldMap.get(oppty.Id).OwnerId) && (uid != trigger.oldMap.get(oppty.Id).OwnerId))
            {   
                System.debug('#### owner id is changed');          
                if(!matchfound)
                {
                    System.debug('#### matchfound is false');   
                    if(trigger.oldMap.get(oppty.Id).OwnerId != bucketuserid)
                    {
                        System.debug('#### throw error');   
                        oppty.addError('Not sufficient rights');
                    }
                }
            }

            oppIds.add(oppty.Id);
        }

        lines = [SELECT Product_Name__c, Quantity, OpportunityId, TotalPrice, opp__c, ProductCode
                FROM OpportunityLineItem 
                WHERE OpportunityId = :oppIds]; 
                
        System.debug('#### lines: ' + lines);
        
        // pah - 2019-04-19
        //List<Quote> quotes = [SELECT Id, Product_Exception__c, General_Notes__c, Payment_Condition_s__c,approval_updated_dt__c
        //                    FROM Quote WHERE Quote_Locked__c = false and OpportunityId = :oppIds];     
        
        List<Quote> quotes = [SELECT Id, Product_Exception__c, General_Notes__c, Payment_Condition_s__c,approval_updated_dt__c
                            FROM Quote WHERE OpportunityId = :oppIds];     

        System.debug('#### quotes: ' + quotes);        
              
        for (Opportunity thisopp : System.Trigger.new) 
        {
            System.debug('#### thisopp: ' + thisopp);

            System.debug('#### quotes.isEmpty(): ' + quotes.isEmpty());
            System.debug('#### thisopp.IsWon: ' + thisopp.IsWon);
            //added for quotes
            if (!quotes.isEmpty() && thisopp.IsWon == false)
            {
                System.debug('####quotes' + quotes);          
                for(Quote thisquotes : quotes)
                {
                    System.debug('####thisquotes' + thisquotes); 
                    
                    if(thisopp.Product_Exceptions__c != null && thisopp.Product_Exceptions__c != trigger.oldMap.get(thisopp.Id).Product_Exceptions__c)
                    {
                        System.debug('############### thisopp.Product_Exceptions__c =' + thisopp.Product_Exceptions__c); 
                        thisquotes.Product_Exception__c = thisopp.Product_Exceptions__c;
                        updatingnow = true;
                    }

                    if(thisopp.General_Notes__c != null && thisopp.General_Notes__c != trigger.oldMap.get(thisopp.Id).General_Notes__c)
                    {
                        System.debug('############### thisopp.General_Notes__c =' + thisopp.General_Notes__c); 
                        thisquotes.General_Notes__c = thisopp.General_Notes__c;
                        updatingnow = true;
                    }

                    if(thisopp.Payment_Condition_s__c != null && thisopp.Payment_Condition_s__c != trigger.oldMap.get(thisopp.Id).Payment_Condition_s__c)
                    {
                        System.debug('############### thisopp.Payment_Condition_s__c =' + thisopp.Payment_Condition_s__c);
                        thisquotes.Payment_Condition_s__c = thisopp.Payment_Condition_s__c;
                        updatingnow = true;
                    }   

                    System.debug('############### updatingnow =' + updatingnow);      
     
                    if(updatingnow != false)
                    {
                        System.debug('############### updating checkbox'); 
                        thisquotes.approval_updated_dt__c = DateTime.now();

                        quotesToUpdate.add(thisquotes);
                    }     
                }
            }                
                 
            if(thisopp.Re_Club_Opp__c == true)
            {       
                thisopp.Applicable_VAT_Rate__c = '20';
                thisopp.CompanyName__c = 'GRI Services Ltd';    
                thisopp.Company_Vat_Number__c = 'GB854134336';    
                thisopp.Vendor_Name__c = 'globalreal1';
            }

            if(thisopp.Billing_Country_Name__c == '')
            {
                if(thisopp.Billing_Country__c != '') 
                {      
                    string ccode = thisopp.Billing_Country__c;
                    string countrynm = incomeUtil.countryName(ccode);    
                    thisopp.Billing_Country_Name__c = countrynm; 
                }
            }
            
            if(!thisopp.Waive_Vat__c)
            {
                if(thisopp.OppRecordTypeName__c == 'Events')
                {
                    System.debug('################################# VATEvents opp record type: ' + thisopp.OppRecordTypeName__c );

                    if(thisopp.VAT_Country__c == 'UK' && thisopp.CompanyName__c == 'GRI Services Ltd')
                    {                  
                      
                        thisopp.Company_Vat_Number__c = 'GB854134336 ';
                    
                        thisopp.Applicable_VAT_Rate__c = '20';
        
                    }
                    else if(thisopp.VAT_Country__c == 'France' && thisopp.CompanyName__c == 'GRI American European LLC')
                    {
                        thisopp.Company_Vat_Number__c = 'FR33793559626 ';
                    
                        if(thisopp.Billing_Country__c == 'FR' && (thisopp.VAT_Number__c != null && thisopp.VAT_Number__c.length() != 0))
                        {
                            thisopp.Applicable_VAT_Rate__c = '0';
                        }
                        else
                        {
                            thisopp.Applicable_VAT_Rate__c = '20';
                        }

                    }
                    else if(thisopp.VAT_Country__c == 'Spain' && thisopp.CompanyName__c == 'GRI American European LLC')
                    {
                        thisopp.Company_Vat_Number__c = 'N4007060I';
                        if(thisopp.Billing_Country__c == 'ES' && (thisopp.VAT_Number__c != null && thisopp.VAT_Number__c.length() != 0))
                        {
                            thisopp.Applicable_VAT_Rate__c = '0';
                            thisopp.spain_vat_calculation__c = 0;
                            vatspainevent = 0;
                        }
                        else
                        {
                          //  thisopp.Applicable_VAT_Rate__c = '21';
                        //}
                        
    	                    System.debug('####Spain');                  
    	                    Id thisOrderId = thisopp.Id;
    	                    vatspainevent = 0;  
    	                    for(OpportunityLineItem thisItem : lines)
    	                    {
    	                        System.debug('####thisItem' + thisItem);
    	                        System.debug('####thisItem.Product_Name__c' + thisItem.Product_Name__c);
    	                        System.debug('####thisItem.ProductCode' + thisItem.ProductCode);                   
    	                        
    	                        
    	                        //if(thisItem.Product_Name__c == 'Retreat Reunion')
    	                        if(thisItem.ProductCode != 'P-001258')
    	                        {
    	                            System.debug('!Retreat Membership');
    	                            //thisopp.Applicable_VAT_Rate__c = '21';   
    	                            System.debug('vatspainevent Reunion' + vatspainevent);
    	                            vatspainevent = vatspainevent + ((21 * (thisItem.TotalPrice))/100);
    	                            System.debug('vatspainevent Reunion' + vatspainevent);                         
    	                        }
    	                        else
    	                        {
    	                            System.debug('spain other');
    	                            vatspainevent = vatspainevent + 0;
    	                        }
    	                        
    	                        System.debug('####thisopp.Applicable_VAT_Rate__c' + thisopp.Applicable_VAT_Rate__c);  
    	                    }
    	                    System.debug('####vatspainevent' + vatspainevent);
    	                    thisopp.spain_vat_calculation__c = vatspainevent;                
    	                    thisopp.Applicable_VAT_Rate__c = '21';    
                        }                
                    }
                    else if(thisopp.VAT_Country__c == 'Germany' && thisopp.CompanyName__c == 'GRI American European LLC')
                    {
                        thisopp.Company_Vat_Number__c = 'No205/5926/2529';
                        //if(thisopp.Billing_Country__c == 'DE' && (thisopp.VAT_Number__c.length() != 0)) Fixed by eddy vat might be null
                        if(thisopp.Billing_Country__c == 'DE' && (thisopp.VAT_Number__c != null && thisopp.VAT_Number__c.length() != 0))
                        {
                            thisopp.Applicable_VAT_Rate__c = '0';
                        }
                        else
                        {
                            thisopp.Applicable_VAT_Rate__c = '19';
                        }
                    }
                    else if(thisopp.VAT_Country__c == 'Swiss')
                    {
                        System.debug('####Swiss');
                        vatswiss=0;
                        thisopp.Company_Vat_Number__c = 'CHE- 309.507.531 MWST';
                        Id thisOrderId = thisopp.Id;
                     
                        for(OpportunityLineItem thisItem : lines)
                        {
                            System.debug('####thisItem' + thisItem);
                            System.debug('####thisItem.Product_Name__c' + thisItem.Product_Name__c);
                            System.debug('####thisItem.ProductCode' + thisItem.ProductCode);                   

                            //if(thisItem.Product_Name__c == 'Retreat Reunion')
                            if(thisItem.ProductCode == 'P-001260')
                            {
                                System.debug('Retreat Reunion');
                                System.debug('vatswiss Reunion' + vatswiss);
                                vatswiss = vatswiss + ((7.7 * (thisItem.TotalPrice))/100);
                                System.debug('vatswiss Reunion' + vatswiss);                             
                            }
                            //else if(thisItem.Product_Name__c == 'GRI State Member' || thisItem.Product_Name__c == 'Retreat Membership')
                            else if(thisItem.ProductCode == 'P-001259' || thisItem.ProductCode == 'P-001258')
                            {
                                System.debug('Retreat other');
                                vatswiss = vatswiss + 0;
                            }
                            else
                            {
                                System.debug('other');
                                vatswiss = vatswiss + ((7.7 * thisopp.Amt_After_Discount__c)/100);
                            }
                        }

                        System.debug('####vatswiss' + vatswiss);
                        thisopp.swiss_vat_calculation__c = vatswiss;                
                        thisopp.Applicable_VAT_Rate__c = '7.7';
                        thisopp.Apply_SWISS_Vat__c = true;
                    }
                    else if(thisopp.VAT_Country__c =='Portugal')
                    {
                        thisopp.Company_Vat_Number__c = '980640482';
                        thisopp.Applicable_VAT_Rate__c = '23';
                    }
                }
                else if(thisopp.OppRecordTypeName__c == 'Membership' || thisopp.OppRecordTypeName__c == 'SPEX')
                {          
                    System.debug('################################# VATMembership or SPEX');
                    if(thisopp.Billing_Country__c == 'GB' && thisopp.CompanyName__c == 'GRI Services Ltd')
                    {
                        thisopp.Applicable_VAT_Rate__c = '20';
        
                    }
                    else if(thisopp.Billing_Country__c != 'GB' && thisopp.CompanyName__c == 'GRI American European LLC')
                    {
                        thisopp.Applicable_VAT_Rate__c = '0';    
                    }
                }
                else
                {
                    thisopp.Applicable_VAT_Rate__c = '0';
                }
                
                //wiredpayments
                if(thisopp.CompanyName__c != '')
                {               
                    string wired = incomeUtil.wiredValue(thisopp.CurrencyIsoCode, thisopp.CompanyName__c);    
                    thisopp.wired_payment_details_update__c = wired;
                }
            }
            else 
            {
                thisopp.Applicable_VAT_Rate__c = '0';
            }  
        }

        if(updatingnow)     
        {
            update quotesToUpdate;  
        }  

        System.debug('EADLOG ---------- fim da trigger ----------- ' +system.now());      
    }
}