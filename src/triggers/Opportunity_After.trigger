/*******************************************************************************
*                               Cloud2b - 2016 
*-------------------------------------------------------------------------------
* 
* Trigger que dispara ações AFTER do objeto oportunidade.
*
* NAME: Opportunity_After.trigger
* AUTHOR: RVR                                                  DATE: 06/04/2016
* History Log
* ------------	-----------------------------------------------	--------------
* Anderson 		ApprovalCommitteeManager functionality added	02/05/2018
* Paschoalon	updates Approval_Committee_Session_Status__c
* 				and Submit_for_Approval_Committee__c  related
* 				fields
* Anderson      AutoProcessMembershipOpps.AutoSetWon() method 
* Paschoalon    added on Trigger.isUpdate 
*******************************************************************************/
trigger Opportunity_After on Opportunity (after insert, after update, after delete, after undelete)
{
    CheckRecursive2.conter();
    if(!TriggerUtils.isEnabled('Opportunity')) return;


    system.debug('###################################################   Opportunity_After.trigger ################################################### ');

    if(Trigger.isInsert) {
        system.debug('Trigger.isInsert');

        OpportunitySetPrimaryContactRole.execute();
        OpportunityCountOppOpenEvent.execute();
        OpportunityAtualizaSessionAttendee.execute();
        GDPR_Trigger_Handler.execute();

        // Added by Renato Matheus Simião in 13/10/2018
        for (Opportunity o : Trigger.new) {

            if (String.isNotEmpty(o.CNPJ_Number__c))
                WSSerproCallout.fUpdateOpportunity(o.Id, o.CNPJ_Number__c);
        }
    }

    if(Trigger.isUpdate) 
    {
        system.debug('Trigger.isUpdate');
        system.debug('#### Updating OP ####');

        if(!CheckRecursive2.UpdatePricebookEProdutoIsActive)
        {
        	OpportunityUpdatePricebookEProduto.execute();
        	CheckRecursive2.UpdatePricebookEProdutoIsActive=True;
        }
            
        if(!CheckRecursive2.CountOppOpenEventIsActive)
        {
        	OpportunityCountOppOpenEvent.execute();
        	CheckRecursive2.CountOppOpenEventIsActive=True;
        }
        
        OpportunityCriaEventAttendee.execute();
        
        //EventSumOrderAndOpp.executeSumInOppToEvent();
        if(!CheckRecursive2.AtualizaSessionAttendeeIsActive)
        {
        	OpportunityAtualizaSessionAttendee.execute();
        	CheckRecursive2.AtualizaSessionAttendeeIsActive=True;
        }
        //OpportunityCriaEventAttendeeInOrder.execute();
        if(TriggerUtils.isEnabled('OpportunityVerificaProdutos')){
            //OpportunityVerificaProdutos.execute(); // do not execute queries
            OpportunityCheckProducts.execute(Trigger.new, FALSE);
        }

        //Autor: CFA  Data da criação : 13/03/2017
        if(!CheckRecursive2.AtualizaLastOpportunityWonIsActive)
        {
        	OpportunityAtualizaLastOpportunityWon.execute();
        	CheckRecursive2.AtualizaLastOpportunityWonIsActive=True;
        }
        
        //Autor: CFA  Data da criação : 15/03/2017
        //if(!CheckRecursive2.GlobalApprovalProcessIsActive)
        //{
            System.debug('%% OpportunityGlobalApprovalProcess.execute()');
       		OpportunityGlobalApprovalProcess.execute();
        //	CheckRecursive2.GlobalApprovalProcessIsActive=True;
        //}
        
        //Autor: CFA  Data da criação : 16/03/2017
        OpportunityNewMember.execute(); // already doesrecursivity prevention

        //Autor: EAD Data da criação : 19/01/2018
        if(!CheckRecursive2.AlteraEventAttendeeIsActive)
        {
        	OpportunityAlteraEventAttendee.execute();
        	CheckRecursive2.AlteraEventAttendeeIsActive=True;
        }

        if(!CheckRecursive2.GDPRTriggerHandlerIsActive)
        {
        	GDPR_Trigger_Handler.execute();
        	CheckRecursive2.GDPRTriggerHandlerIsActive=True;
        }
        
        // Author: Anderson Paschoalon 19/06/2019
        System.debug('%% AutoProcessMembershipOpps.AutoSetWon() '+CheckRecursive2.conterValue());
        AutoProcessMembershipOpps.AutoSetWon(Trigger.new);

        // Added by Renato Matheus Simião in 13/10/2018
        for (Opportunity o : Trigger.new) {

            if (String.isNotEmpty(o.CNPJ_Number__c) && o.CNPJ_Number__c != Trigger.oldMap.get(o.Id).CNPJ_Number__c)
                WSSerproCallout.fUpdateOpportunity(o.Id, o.CNPJ_Number__c);
        }
    }

    if(Trigger.isDelete) {
        system.debug('Trigger.isDelete');
        OpportunityCountOppOpenEvent.execute();
        //EventSumOrderAndOpp.executeSumInOppToEventOnDelete();
    }

    if(Trigger.isUndelete) {
        system.debug('Trigger.isUndelete');
        OpportunityCountOppOpenEvent.execute();
    }

}