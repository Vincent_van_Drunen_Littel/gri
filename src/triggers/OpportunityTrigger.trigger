trigger OpportunityTrigger on Opportunity (after Update) {
    
    set<Id> oppObjIds = new set<Id>();
    set<Id> eventIds = new set<Id>();
    Id eventsRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Open_Events').getRecordTypeId();
    system.debug('@@eventsRecordTypeId'+eventsRecordTypeId);
    if(RecursionCls.isOptyCalendarRun == false){
        if(Trigger.isUpdate && Trigger.isAfter){
            for(Opportunity oppObj:Trigger.new){
                if(oppObj.RecordTypeId== eventsRecordTypeId && oppObj.StageName == 'Approved by Finance' && oppObj.StageName != trigger.oldmap.get(oppObj.id).StageName){
                    oppObjIds.add(oppObj.Id);
                    eventIds.add(oppObj.Event__c);
                }
                if(oppObjIds.size()>0 && eventIds.size()>0){
                    RecursionCls.isOptyCalendarRun = true;
                    OpportunityTriggerHandler.CreateInviteesForEvent(oppObjIds);
                }
            }
        }
    }
}