trigger WEBOpportunity on Opportunity (after insert, after update) {
    
    list<String> lstOpp = new list<String>();
    map<String, list<OpportunityLineItem>> mapOppLineItem = new map<String, list<OpportunityLineItem>>();
    
    for(Opportunity opp : trigger.new) {
        if(opp.StageName == 'Approved by Finance'){
            
            lstOpp.add(opp.Id);
        }
    }
     if(!lstOpp.isEmpty()) WebOpportunity.testeOpportunity(lstOpp);
    /*
    for(OpportunityLineItem oppLine :[SELECT Product2.Name, OpportunityId, Quantity, ListPrice, UnitPrice, Product2Id, ProductCode
                                                FROM OpportunityLineItem 
                                      WHERE OpportunityId =: lstOpp]){
                                          
       list<OpportunityLineItem> lstOppLineItem = mapOppLineItem.get(oppLine.OpportunityId);
        
       if(lstOppLineItem == null) lstOppLineItem = new list<OpportunityLineItem>();
       
       lstOppLineItem.add(oppLine);
                                          
       mapOppLineItem.put(oppLine.OpportunityId, lstOppLineItem);                                 
    }
         
    for(Opportunity opp : trigger.new) {    
           
            
            String[] tmp1 = New String[]{};
                String[] tmp2 = New String[]{};
                    String[] tmp3 = New String[]{};
                        String[] tmp4 = New String[]{};
                            String[] tmp5 = New String[]{};
                                String[] tmp6 = New String[]{};
                                    
                                    String SOpportunityId, SQuantity, SListPrice, STotalPrice, SName, SProductCode;
        if(mapOppLineItem.containsKey(opp.id)){
            for(OpportunityLineItem ol : mapOppLineItem.get(opp.id)){
                tmp1.add(ol.Product2.Name);
                tmp2.add(String.valueOf(ol.Quantity));
                tmp3.add(String.valueOf(ol.ListPrice));
                tmp4.add(String.valueOf(ol.UnitPrice));
                tmp5.add(ol.Product2Id);
                tmp6.add(ol.ProductCode);
            }
        }  
            SOpportunityId = String.join(tmp1, ',');
            SQuantity = String.join(tmp2, ',');
            SListPrice = String.join(tmp3, ',');
            STotalPrice = String.join(tmp4, ',');
            SName = String.join(tmp4, ',');
            SProductCode = String.join(tmp5, ',');
            String Prod1, Prod2, Prod3, prec1, prec2, prec3, tp1, tp2, tp3; 
            Decimal Quant1, Quant2, Quant3;
            //Nome do Produto   
            /*  
            if(tmp1.size() == 1){
                Opp.Produto_1__c = tmp1[0];
                Insert Opp;
                System.debug('Prod1'+Prod1);
            }
            if(tmp1.size() == 2){
                Opp.Produto_1__c = tmp1[0];
                Opp.Produto_2__c = tmp1[1];
                Insert Opp;
                System.debug('Prod1'+Prod1);
                System.debug('Prod2'+Prod2);
            }
            if(tmp1.size() == 3){
                Opp.Produto_1__c = tmp1[0];
                Opp.Produto_2__c = tmp1[1];
                Opp.Produto_3__c = tmp1[2];
                    Insert Opp;
                System.debug('Prod1'+Prod1);
                System.debug('Prod2'+Prod2);
                System.debug('Prod3'+Prod3);
            }
            
            //Quantidade do Produto            
            if(tmp2.size() == 1){
                Opp.Produto_Quantidade_1__c = tmp2[0];
                insert Opp;
                System.debug('Quant1'+Quant1);
            }
            if(tmp2.size() == 2){
                 Opp.Produto_Quantidade_1__c =  tmp2[0];
                 Opp.Produto_Quantidade_2__c =  tmp2[1];
                Insert Opp;
                System.debug('Quant1'+Quant1);
                System.debug('Quant2'+Quant2);
            }
            if(tmp2.size() == 3){
                Opp.Produto_Quantidade_1__c =  tmp2[0];
                Opp.Produto_Quantidade_2__c =  tmp2[1];
                Opp.Produto_Quantidade_3__c =  tmp2[2];
                Insert Opp;
                System.debug('Quant1'+Quant1);
                System.debug('Quant2'+Quant2);
                System.debug('Quant3'+Quant3);
            }
            
            //Lista de Preço do Produto            
            if(tmp3.size() == 1){
                Opp.Produto_List_Price_1__c = tmp3[0] ;
                Insert Opp;
                System.debug('prec1'+prec1);
            }
            if(tmp3.size() == 2){
                Opp.Produto_List_Price_1__c = tmp3[0] ;
                Opp.Produto_List_Price_2__c = tmp3[1] ;
                Insert Opp;
                System.debug('prec1'+prec1);
                System.debug('prec2'+prec2);
            }
            if(tmp3.size() == 3){
                Opp.Produto_List_Price_1__c = tmp3[0] ;
                Opp.Produto_List_Price_2__c = tmp3[1] ;
                Opp.Produto_List_Price_3__c = tmp3[2] ;
                Insert Opp;
                System.debug('prec1'+prec1);
                System.debug('prec2'+prec2);
                System.debug('prec3'+prec3);
            }
            
            //Preço Total do Produto            
            if(tmp4.size() == 1 ){
                Opp.Produto_Total_Price_1__c = tmp4[0];
                Insert Opp;
                System.debug('tp1'+tp1);
            }
            if(tmp4.size() == 2 ){
                Opp.Produto_Total_Price_1__c = tmp4[0];
                Opp.Produto_Total_Price_2__c = tmp4[1];
                Insert Opp;
                System.debug('tp1'+tp1);
                System.debug('tp2'+tp2);
            }
            if(tmp4.size() == 3){
                
                Opp.Produto_Total_Price_1__c = tmp4[0];
                Opp.Produto_Total_Price_2__c = tmp4[1];
                Opp.Produto_Total_Price_3__c = tmp3[2];
                Insert Opp;
                System.debug('tp1'+tp1);
                System.debug('tp2'+tp2);
                System.debug('tp3'+tp3);
            }*/
            /*system.debug('tmp1: '+tmp1[0]);
            system.debug('tmp2: '+tmp2[0]);
            system.debug('tmp3: '+tmp3[0]);
            system.debug('tmp4: '+tmp4[0]);
            system.debug('tmp5: '+tmp5[0]);
            system.debug('tmp6: '+tmp6[0]);
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            //-----------Por enquanto vai passar apenas 1 artigo-----------------
            /*
            if( tmp6[0] != null){   
                gen.writeStringField('ITMREF1',  tmp6[0]);
				gen.writeStringField('YPRODCOD1',  tmp6[0]);
            }
            if(!tmp3.isEmpty() && tmp3[0] != null){   
                gen.writeNumberField('GROPRI1',  double.valueOf(tmp3[0]));
                gen.writeNumberField('CPRPRI1',  double.valueOf(tmp3[0]));
            }
            if(!tmp4.isEmpty() && tmp4[0] != null){   
                gen.writeNumberField('NETPRIATI1',  double.valueOf(tmp4[0]));
            }
            if(!tmp2.isEmpty() && tmp2[0] != null){   
               gen.writeNumberField('QTY1',  double.valueOf(tmp2[0]));
            }
         	//-------------------------------------------------------------------
           // if(Opp.Produto_Quantidade_1__c != null){   
              // gen.writeObjectField('QTY1',  Opp.Produto_Quantidade_1__c);
            //}
            /*  if(Opp.Produto_Quantidade_2__c != null){   
                gen.writeStringField('QTY2', Opp.Produto_Quantidade_2__c);
            }
              if(Opp.Produto_Quantidade_3__c != null){   
                gen.writeStringField('QTY3', Opp.Produto_Quantidade_3__c);
            }
			if( Opp.Produto_1__c != null){   
                gen.writeStringField('ITMREF1',  Opp.Produto_1__c);
            }
            if( Opp.Produto_2__c != null){   
            gen.writeStringField('ITMREF2',  Opp.Produto_2__c);
            }if( Opp.Produto_3__c != null){   
            gen.writeStringField('ITMREF2',  Opp.Produto_3__c);
			}
            /************************Falta Lista de PReços e Preço Total do Produto  **************************/
            /*gen.writeNumberField('XTOIMP', 1); //status necessary for X3
            
            if(Opp.Club__c != null){                
                gen.writeObjectField('YCLUB', Opp.Club__c);
            }
            if(Opp.Contact_ID_CaseSafe__c != null){
                //String strId = Id.valueOf(Opp.Contact_ID_CaseSafe__c);
                gen.writeObjectField('YIDCONT', Opp.Contact_ID_CaseSafe__c); //****************Campo Unico *****************
            }
            if(Opp.Id != null){
                system.debug('Id: '+Opp.Id);
                gen.writeObjectField('YIDOP', Opp.Id);
            }
            
            if(Opp.Payment_Notes__c != null ){
                gen.writeObjectField('YPAYNOTE', Opp.Payment_Notes__c);      //Text Area-> X3 alfanumérico
            }
            if(Opp.Payment_Notes__c != null ){
                gen.writeObjectField('PTE', Opp.Payment_Notes__c);      //Text Area-> X3 alfanumérico
            }
            if(Opp.CloseDate != null){
                gen.writeObjectField('ORDDAT', Opp.CloseDate); 
            }
            if(Opp.CurrencyIsoCode != null){
            gen.writeObjectField('SOHCUR', Opp.CurrencyIsoCode);
         	}
            if((Opp.GRI_Branch__c!= null) && (Opp.GRI_Branch__c!='N/A')){
            	String tmp;
                if(Opp.GRI_Branch__c == 'GRI AE LLC'){tmp = '3-LLC';}
                if(Opp.GRI_Branch__c == 'GRI Brazil'){tmp = '1-BRA';}
                if(Opp.GRI_Branch__c == 'GRI Services Ltd'){tmp = '5-LTD';}
                if(Opp.GRI_Branch__c == 'GRI USA Inc'){tmp = '2-INC';}
                if(Opp.GRI_Branch__c == 'Blue-X'){tmp = '4-BLX';}
            gen.writeObjectField('SALFCY', tmp);
            }
            if(Opp.VAT_Number__c != null){
            gen.writeObjectField('EECNUM', Opp.VAT_Number__c);
         	}
            if(Opp.Account_Taxa_Rule__c != null){
            gen.writeObjectField('VACBPR', Opp.Account_Taxa_Rule__c);
         	}
            if(Opp.X3_Sales_Type__c != null){
            gen.writeObjectField('SOHTYP', Opp.X3_Sales_Type__c);
         	}
            /*
            if(Opp.EventID__c!= null){
                gen.writeObjectField('YEVENTID', Opp.EventID__c);	//	Campo Id -> X3 campo númerico
            }
            
            if(Opp.Government_Company__c != null){
                gen.writeObjectField('YENTPUBLI', Opp.Government_Company__c); // Alfamumérico X3 | Checkbox no Salesforce
            }
            /*
            if(opp.ContactID__c != null){
                gen.writeObjectField('CCNCRM', opp.ContactID__c); }
}

            /* if(Opp.Amount!= null){
            gen.writeObjectField('', Opp.Amount);
           	}*/
            /*  if(Opp.Opportunity_Owner__c != null){                           //No Salesforce é um Alfanúmerico -> X3 Campo númerico
                gen.writeObjectField('REP', Opp.Opportunity_Owner__c); 
            }
            
            
            
             //***********************Contact**************************  (100% campos)
            if(opp.Last_Name__c != null){
                gen.writeObjectField('CNTLNA', opp.Last_Name__c);
            }
            if(Opp.First_Name__c != null){
                gen.writeObjectField('CNTFNA', Opp.First_Name__c);
            }
          /*  if(Opp.Full_Consent_Given_to_GDPR__c != null && Opp.Full_Consent_Given_to_GDPR__c == 'Yes' ){
                //gen.writeObjectField('YFULLCONS', Opp.Full_Consent_Given_to_GDPR__c);
                gen.writeNumberField('YFULLCONS', 2);
                
            }else if(Opp.Full_Consent_Given_to_GDPR__c != null && Opp.Full_Consent_Given_to_GDPR__c == 'No'){
                gen.writeNumberField('YFULLCONS', 1);
                
            }
            if(Opp.Soft_Consent_Given_to_GDPR__c != null && Opp.Soft_Consent_Given_to_GDPR__c == 'Yes' ){
                //gen.writeObjectField('YSOFTCONS', Opp.Soft_Consent_Given_to_GDPR__c);
                gen.writeNumberField('YSOFTCONS', 2);
                
            }else if(Opp.Soft_Consent_Given_to_GDPR__c != null && Opp.Soft_Consent_Given_to_GDPR__c == 'No'){
                gen.writeNumberField('YSOFTCONS', 1);
                
            }*/
            /*if(Opp.Contact_Status__c != null && Opp.Contact_Status__c == 'Yes' ){
                //gen.writeObjectField('YSTA', Opp.Contact_Status__c);						No Excel tem Active/Inactive -->No X3 tem Yes/No
                gen.writeNumberField('YSTA', 2);
                
            }else if(Opp.Contact_Status__c != null && Opp.Contact_Status__c == 'No'){
                gen.writeNumberField('YSTA', 1);
                
            }
        
            
            if(Opp.Contact_Language__c != null && Opp.Contact_Language__c=='Portuguese'){
            String tmp;
                If(Opp.Contact_Language__c=='Portuguese'){tmp='BRA';}
            gen.writeObjectField('CNTLAN', tmp);  //X3 Alfanumérico  com 3 digitos -> salesforce alfanumerico mas nome da lingua por extenso
            }
            /* if(Opp.Contact_Funcao__c != null){ 
gen.writeObjectField('CNTFNC', Opp.Contact_Funcao__c);  //X3 valores picklist/List -> no Salesforce é alfanumerico
} 

if(Opp.Contact_Gender__c != null){
gen.writeObjectField('YGENDER', Opp.Contact_Gender__c);   // No X3 yes/no ->salesforce Female/Male
}
            if(Opp.Contact_Email_1__c != null){
                gen.writeObjectField('YSECEMAIL2', Opp.Contact_Email_1__c);
            }
            if(Opp.Contact_Email_2__c != null){
                gen.writeObjectField('YSECEMAIL2', Opp.Contact_Email_2__c);
            }
            if(Opp.Contact_Name_1__c != null){
                gen.writeObjectField('YSECNAME1', Opp.Contact_Name_1__c);
            }
            if(Opp.Contact_Name_2__c != null){
                gen.writeObjectField('YSECNAME2', Opp.Contact_Name_2__c);
            }
            if(Opp.Contact_Phone_1__c != null){
                gen.writeObjectField('YSECPHONE1', Opp.Contact_Phone_1__c);
            }
            if(Opp.Contact_Phone_2__c != null){
                gen.writeObjectField('YSECPHONE2', Opp.Contact_Phone_2__c);
            }
            
            //Account
            
            /* if(Opp.AccountId != null){
                gen.writeObjectField('BPCNUM', Opp.AccountId);    //Não dá
               
            }
            
            if(Opp.Account_Name__c != null){
                String strTmp = Opp.Account_Name__c;
                Integer maxSize = 35;
                    if(strTmp.length() > maxSize ){
                    strTmp = strTmp.substring(0, maxSize);
                }
                gen.writeObjectField('BPRNAM', strTmp);
                
            }
         
          if(Opp.Billing_Country__c != null){
                gen.writeObjectField('ACRY', Opp.Billing_Country__c);
                
            }
          if(Opp.Billing_Postcode__c != null){
                gen.writeObjectField('POSCOD', Opp.Billing_Postcode__c);
                
            }  
            if(Opp.Billing_State__c != null){
                gen.writeObjectField('SAT', Opp.Billing_State__c);   //não dá
                
            }
            if(Opp.Billing_Address__c != null){
                gen.writeObjectField('BPAADDLIG0', Opp.Billing_Address__c);
                
            }
            
            // Inicio comentário : Novos campos Opportunity [29-01-2019]
            If (Opp.Designated_Office__c != null){
                //gen.writeObjectField(, o)
            }
            If (Opp.Event_Division__c != null){
                //gen.writeObjectField(, o)
            }
            If (Opp.Division__c != null){
                //gen.writeObjectField(, o)
            }
            If (Opp.Business_Unit__c != null){
                //gen.writeObjectField(, o)
            }
            If (Opp.Event_Business_Unit__c != null){
                //gen.writeObjectField(, o)
            }
            If (Opp.Sales_Division_new__c != null){
                //gen.writeObjectField(, o)
            }
            If (Opp.Company_Vat_Number__c != null){
                gen.writeObjectField('YVATNUM', Opp.Company_Vat_Number__c);
            }
            
            //Falta o campo 'ProductCode' (não está na objeto do salesforce)
			// Fim comentário
            
          /* if(Opp.Busines_Phone__c != null){					 				Vários Registo no mesmo campo
                gen.writeObjectField('TEL0', Opp.Busines_Phone__c);
                
            }        */  
         /*    if(Opp.Direct_Line__c != null){
                gen.writeObjectField('TEL1', Opp.Direct_Line__c);
                
            } 
            if(Opp.Mobile_Phone__c != null){
                gen.writeObjectField('TEL2', Opp.Mobile_Phone__c);     
                
            } 
            if(Opp.Business_Email__c != null){
                gen.writeObjectField('WEB0', Opp.Business_Email__c);
                
            }
            if(Opp.Account_Other_Email__c != null){
                gen.writeObjectField('WEB1', Opp.Account_Other_Email__c);
                
            }
            
            //Envia o CNPJ Ou CPF
           if(Opp.CNPJ__c != null){
                gen.writeObjectField('XQBPACRN', Opp.CNPJ__c);
                
            } else if(Opp.CPF__c != null){
                gen.writeObjectField('XQBPACRN', Opp.CPF__c);
            }
        */		/*
             if(Opp.Account_Brazilian__c != null){
                gen.writeObjectField('XQRETCALC', Opp.Account_Brazilian__c);
            }
             if( Opp.Account_Company_Name__c != null){
                 gen.writeObjectField('XQRAZAO', Opp.Account_Company_Name__c);
            }
                 
            
            //Consoante o valor da picklist envia para determinado campo do X3
            //
            
           if( Opp.Account_Tax_Regime__c == 'Simples Nacional'){
                gen.writeNumberField('XQBPRSN ',  2);
                
            }else if( Opp.Account_Tax_Regime__c == 'Contribuinte de ICMS'){
                
                gen.writeNumberField('XQBPRCI', 2);
                
            }else if( Opp.Account_Tax_Regime__c == 'Consumidor Final'){			//Por testar ainda
                
                gen.writeNumberField('XQCONSUFINAL', 2); 
                
            }else if ( Opp.Account_Tax_Regime__c == 'Órgão Público'){
                
                gen.writeNumberField('XQBPROP',  2); 
                
            }
            //gen.writeObjectField('VACBPR', Opp.Account_Taxa_Rule__c);
            gen.writeEndObject();
            String jsonSOpportunity = gen.getAsString();
            System.debug('jsonSOpportunity'+jsonSOpportunity);

            WebOpportunity.testeOpportunity(jsonSOpportunity);
    }*/
    
}