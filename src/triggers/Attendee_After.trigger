/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Trigger que dispara ações AFTER do objeto Session.
*
* NAME: Session_After.trigger
* AUTHOR: VMDL                                                DATE: 15/08/2016
*******************************************************************************/
trigger Attendee_After on Attendee__c (after insert, after update, after delete, after undelete) {

	if(!TriggerUtils.isEnabled('Attendee__c')) return;

	if(Trigger.isInsert) 
	{
		AttendeeCountInSession.executeCountInsert();
	}

	if(Trigger.isDelete) 
	{
		AttendeeCountInSession.executeCountDelete();
	}
}