/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Trigger que desempenha ações after do Pricebook2.
*
* NAME: Pricebook2_after.cls
* AUTHOR: EAD                                                  DATE: 07/02/2018
*******************************************************************************/

trigger Pricebook2_after on Pricebook2 (after update) {
	if(!TriggerUtils.isEnabled('Pricebook2')) return;

    if(Trigger.isUpdate) 
	{
		PricebookUpdatesOportunidades.execute();
	} 
}