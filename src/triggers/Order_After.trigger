/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Trigger que dispara ações AFTER do objeto Pedido.
*
* NAME: Order_After.trigger
* AUTHOR: VMDL                                                DATE: 04/08/2016
*******************************************************************************/
trigger Order_After on Order (after insert, after update, after delete, after undelete) {

    if(!TriggerUtils.isEnabled('Order')) return;
    
    if(Trigger.isUpdate) 
    {
       // EventSumOrderAndOpp.executeSumInOrderToEvent();
    }

    if(Trigger.isDelete) 
    {
       // EventSumOrderAndOpp.executeSumInOrderToEventOnDelete();
    }
}