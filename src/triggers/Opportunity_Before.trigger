/*******************************************************************************
*                               Cloud2b - 2016
*------------------------------------------------------------------------------- 
* 
* Trigger que dispara ações BEFORE do objeto oportunidade.
*
* NAME: Opportunity_Before.trigger
* AUTHOR: RVR                                                  DATE: 06/04/2016
* History Log
* ------------	-----------------------------------------------	--------------
* Anderson 		ApprovalCommitteeManager functionality added	02/05/2018
* Paschoalon	updates Approval_Committee_Session_Status__c  
* 				and Submit_for_Approval_Committee__c  related 
* 				fields 
*******************************************************************************/
trigger Opportunity_Before on Opportunity (before insert, before update)
{
    
    //private static Boolean m_tiggerIsActive = False;

    if(!TriggerUtils.isEnabled('Opportunity')) return;
    
    if(Trigger.isInsert) 
    {
        OpportunityVinculaPricebook.execute2();
        //OpportunityVinculaPricebook.execute();
    }

    
    if(Trigger.isUpdate)
    {
        new OpportunityTriggerHandler().run();
        OpportunityUpdateSalesDivision.execute(Trigger.new, Trigger.old);
        if(CheckRecursive2.DiamondMemberAutoApprovalIsActive) return;
        AutoProcessMembershipOpps.DiamondMemberAutoApproval(Trigger.new);
        AutoProcessMembershipOpps.updateOppDates(Trigger.new);
        CheckRecursive2.DiamondMemberAutoApprovalIsActive=True;
    }

    // TODO put htis into a class approval Committee manager
    if(Trigger.isUpdate)
    {
        if(CheckRecursive2.ApprovalCommitteeManagerIsActive) return;
        CheckRecursive2.ApprovalCommitteeManagerIsActive=True;  
        // Auto process diamond membership
        //AutoProcessMembershipOpps.setAppByFinance(Trigger.new);
        //AutoProcessMembershipOpps.DiamondMemberAutoApproval(Trigger.new);

        for(Opportunity opp: Trigger.new )
        {
            if( Trigger.oldMap.get( opp.Id ).StageName 
               != Trigger.newMap.get( opp.Id ).StageName )
            {
                List<String> errorMessege = new List<String>();
                if(!OpportunityApproovalCommitteeManager.execute(trigger.new, errorMessege)){
                    opp.addError(errorMessege[0]); // TODO vector here is bas
                }
				break;                
            }
        }
    }
    
    /*
    if(Trigger.isUpdate)
    {
        List<String> errorMessege = new List<String>();
        if(!OpportunityApproovalCommitteeManager.execute(trigger.new, errorMessege)){
            Opportunity opp = trigger.new[0];
            opp.addError(errorMessege[0]+errorMessege[1]);
        }
    }
*/
}