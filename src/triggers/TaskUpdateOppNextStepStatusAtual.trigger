/*******************************************************************************
*                     Copyright (C) 2012 - Cloud2b
*-------------------------------------------------------------------------------
*
* Trigger que faz: 
*  1) atualização dos campos Next_Step e Date_of_the_next_step__c 
*     da oportunidade com dados da tarefa quando o flag Upgrade_to_Next_Step__c tá ligado; 
*  2) atualização dos campos Actual_Status__c e Date_of_Actual_Status__c 
*     da oportunidade com dados da tarefa quando o flag Upgrade_to_Actual_Status__c tá ligado; 
* NAME: TaskUpdateOppNextStepStatusAtualTest.cls
* AUTHOR: IGOR GOIS                         DATE: 04/05/2016
*
*-------------------------------------------------------------------------------
* MAINTENANCE
* AUTHOR:                                           DATE:
*******************************************************************************/

trigger TaskUpdateOppNextStepStatusAtual on Task (before insert, before update) {
    
  List< String > lOppIds = new List< String >();
  List< Task > lTaskList = new List< Task >();
  boolean lHasError = false;
  
  for ( Task lTask : Trigger.new )
  {
    Task lTaskOld;
    if ( Trigger.isUpdate ) lTaskOld = Trigger.oldMap.get( lTask.id );
    
    if ( lTask.WhatId != null && ( lTask.Upgrade_to_Next_Step__c || lTask.Upgrade_to_Actual_Status__c )
    && ( lTaskOld == null || lTask.Upgrade_to_Next_Step__c != lTaskOld.Upgrade_to_Next_Step__c
    || lTask.Upgrade_to_Actual_Status__c != lTaskOld.Upgrade_to_Actual_Status__c ) )
    {
      lOppIds.add( lTask.WhatId );
      lTaskList.add( lTask );
    }
  }
  
  if ( lOppIds.size() == 0 ) return;
  
  Map< String, Opportunity > lOppMap = new Map< String, Opportunity >();
  List< Opportunity > lOppList = [ Select id, Date_of_the_next_step__c , Next_Step__c, Date_of_Actual_Status__c,
                                   Actual_Status__c from Opportunity where id =: lOppIds ];
  
  for ( Opportunity lOpp : lOppList )
    lOppMap.put( lOpp.id, lOpp );
    
  lOppList.clear();
  for ( Task lTask : Trigger.new )
  {
    Opportunity lOpp = lOppMap.get( lTask.WhatId );
    if ( lOpp != null )
    {
      if ( lTask.Upgrade_to_Next_Step__c )
      {
        lOpp.Date_of_the_next_step__c  = lTask.ActivityDate;
        if ( lTask.Description != null )
            if ( lTask.Description.length() <= 255 )
              lOpp.Next_Step__c = lTask.Description;
            else
              lOpp.Next_Step__c = lTask.Description.substring( 0, 255 );
      }
      if ( lTask.Upgrade_to_Actual_Status__c )
      {
        lOpp.Date_of_Actual_Status__c = lTask.ActivityDate;
        if ( lTask.Description != null )
            if ( lTask.Description.length() <= 255 )
              lOpp.Actual_Status__c = lTask.Description;
            else
              lOpp.Actual_Status__c = lTask.Description.substring( 0, 255 );
      }
      lOppList.add( lOpp );
    }
    else
    {
      if ( lTask.Upgrade_to_Next_Step__c )
      {
        lTask.Upgrade_to_Next_Step__c.addError( 'You can only upgrade if the task is related with opportunity' );
        lHasError = true;
      }
      if ( lTask.Upgrade_to_Actual_Status__c )
      {
        lTask.Upgrade_to_Actual_Status__c.addError( 'You can only upgrade if the task is related with opportunity' );
        lHasError = true;
      }
    }
  }
  
  if ( lHasError ) return;
  
  if ( lOppList.size() > 0 )
    update lOppList;
  
  if ( lTaskList.size() > 0 )
  {
    for ( Task lTask : lTaskList )
    {
      lTask.Upgrade_to_Next_Step__c = false;
      lTask.Upgrade_to_Actual_Status__c = false;
    }
  }

}