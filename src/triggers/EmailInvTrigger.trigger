trigger EmailInvTrigger on Invoice__c (after insert,after update) {
     boolean updaterecord = false;
     boolean sendemail = false;
     Invoice__c[] invtoupdate = new Invoice__c[]{};
     List<string> emailrecipients = new List<string>();
     List<string> ccemailrecipients = new List<string>();
      ID emailtemplateid;
       ID invoiceid;
         ID contactid;
         
         List<Invoice__c > olis = [SELECT Id, Send_Invoice__c, Email_Sent__c,email_to_send__c,template__c,Contact_Id__c,regemail__c FROM Invoice__c WHERE Id IN: Trigger.newMap.keySet()];
         
         for (Invoice__c thisinv :olis)
         {
        if( thisinv.Email_Sent__c != true && thisinv.Send_Invoice__c == true)
        {
            thisinv.Email_Sent__c = true;     
            updaterecord = true;
            sendemail = true;
            invtoupdate.add(thisinv);
            emailrecipients.add(thisinv.email_to_send__c);
            ccemailrecipients.add(thisinv.regemail__c);
            emailtemplateid = thisinv.template__c;
            invoiceid= thisinv.Id;
            contactid = thisinv.Contact_Id__c;
         } else {
         	//thisinv.Email_Sent__c = true;     
            updaterecord = false;
            sendemail = false;
            //invtoupdate.add(thisinv);
            //emailrecipients.add(thisinv.email_to_send__c);
            //ccemailrecipients.add(thisinv.regemail__c);
            //emailtemplateid = thisinv.template__c;
            //invoiceid= thisinv.Id;
            //contactid = thisinv.Contact_Id__c;
         }
       }
            if (invtoupdate != null)
            {
                update invtoupdate;
            }

            if (sendemail )
            {
               
                 ID orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('orgwideemailaddressid').Value__c);
                 
                try
                {
                 system.debug('emailrecipients ' + emailrecipients);
                 system.debug('ccemailrecipients ' + ccemailrecipients);
                 system.debug('emailtemplateid ' + emailtemplateid);
                 system.debug('invoiceid ' + invoiceid);
                 system.debug('contactid ' + contactid);
                 system.debug('orgwideemailaddressid ' + orgwideemailaddressid);
                EmailUtils.sendEmail(emailrecipients,ccemailrecipients, emailtemplateid, invoiceid, contactid , orgwideemailaddressid);
                   
                }
                catch (Exception err){
                }
            }
        
     

}