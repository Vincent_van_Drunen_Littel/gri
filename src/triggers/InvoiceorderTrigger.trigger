trigger InvoiceorderTrigger on Invoice__c (before insert, before update) {
    system.debug('################################# InvoiceorderTrigger just started');
    Set<id> oppIds = new Set<id>();
    private Opportunity sorder;
    public List<OpportunityLineItem> lines {get; set;}
    boolean sendemail;
    public String productTableHtml = '';
    public boolean emailsent = false;
    public String emailtemp = '';
    public String emailtempName = '';
    public String wireddetails = '';

        
    for (Invoice__c op : Trigger.new)
    {
        oppIds.add(op.Opportunity__c);
    }
    sorder = [SELECT StageName,wired_payment_details_update__c FROM Opportunity WHERE Id = :oppIds];
    lines = [SELECT Product_Name__c, Quantity, OpportunityId,   TotalPrice,opp__c,Currency_Code__c
    FROM OpportunityLineItem WHERE OpportunityId = :sorder.Id];
    Id thisOrderId = sorder.Id;
    wireddetails = sorder.wired_payment_details_update__c;
    Opportunity thisOrd = [SELECT Id, 
    (SELECT Product_Name__c, Quantity, OpportunityId,   TotalPrice,opp__c,Currency_Code__c FROM OpportunityLineItems) 
    FROM Opportunity where Id = :thisOrderId];
   
    productTableHtml = '<table border="0" cellspacing="0" cellpadding="0" style="text-align:left; padding:10px">';
    productTableHtml += '<tr>';
    productTableHtml += '<th></th>';
    productTableHtml += '<th style="border-bottom:1px solid black; text-align:left;"><b>Quantity</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</th>';
    productTableHtml += '<th style="border-bottom:1px solid black;text-align:left;"><b>Details</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th>';
    productTableHtml += '<th style="border-bottom:1px solid black;text-align:left;"><b>Net Amount</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th>';
    productTableHtml += '</tr>';

    for(OpportunityLineItem thisItem : thisOrd.OpportunityLineItems)
    {
        Decimal amountToTest = thisItem.TotalPrice;
        string ccode = thisItem.Currency_Code__c;
        amountToTest = amountToTest + 0.001;
        String stringAmount = String.valueof(amountToTest.setscale(2));
        productTableHtml += '<tr>';
        productTableHtml += '<td>' + '</td>';
        productTableHtml += '<td>' + thisItem.Quantity + '&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</td>';            
        productTableHtml += '<td>' + thisItem.Product_Name__c + '&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td>';                                        
        productTableHtml += '<td>' + ccode + '&nbsp;' + stringAmount  + '&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td>';
        productTableHtml += '</tr>';
    }
    productTableHtml += '</table>';  
    
     for (Invoice__c thisinv : System.Trigger.new)
    {
        if(thisinv.Event_Date_Range__c != null)
        {
             productTableHtml = '<b>' + thisinv.product_name__c + '</b>' + '<br/>' + '\n\r' + thisinv.Event_Date_Range__c + '</b>' + productTableHtml;
        }
        else
        {
            productTableHtml = '<b>' + thisinv.product_name__c + '</b>' + '<br/>' + '</b>' + productTableHtml;
        }
        
        thisinv.Order_Description__c= productTableHtml;
        thisinv.wired_payment_details__c = wireddetails;
        
        //thisinv.email_to_send__c= thisinv.Email_add__c;
        
        if(thisinv.WebCreated__c == true)
        {
        	thisinv.ThanksMessage__c = 'Payment recieved with thanks';
        }
        else if(thisinv.WebCreated__c == false)
        {
        	thisinv.ThanksMessage__c = 'Thank you for your prompt payment';
        }
                   
        //ID orgwideemailaddressid  = (ID)string.valueOf(Application_Config__c.getInstance('orgwideemailaddressid').Value__c);
                       
        
        //ID orgwideemailaddressid  = '0D27A0000004Cu9';
        ID emailtemplateid;
       if(thisinv.Company_Name__c == 'GRI American European LLC')
        {
            if(thisinv.India_Invoice__c == true)
            {
                emailtemplateid  = (ID)string.valueOf(Application_Config__c.getInstance('GRIIndia').Value__c);
                emailtempName = 'GRIIndia';
            }
            else if(thisinv.Swiss_Vat_Invoice__c == true)
            {
                emailtemplateid  = (ID)string.valueOf(Application_Config__c.getInstance('GRIAmericanEuropeanLLCSwiss').Value__c);
                emailtempName = 'GRIAmericanEuropeanLLCSwiss';
            }
            else
            {
                emailtemplateid  = (ID)string.valueOf(Application_Config__c.getInstance('GRIAmericanEuropeanLLC').Value__c);
                emailtempName = 'GRIAmericanEuropeanLLC';
            }

        
            
        //emailtemplateid = '00X7A000000MHqQUAW';
        }
        if(thisinv.Company_Name__c == 'GRI Services Ltd')
        {
           if(thisinv.Swiss_Vat_Invoice__c == true)
            {
                emailtemplateid  = (ID)string.valueOf(Application_Config__c.getInstance('GRIServicesLtdSwiss').Value__c);
                emailtempName = 'GRIServicesLtdSwiss';
            }
            else
            {
                emailtemplateid  = (ID)string.valueOf(Application_Config__c.getInstance('GRIServicesLtd').Value__c);
                emailtempName = 'GRIServicesLtd';
            } 
      
         
        //emailtemplateid = '00X7A000000MHqVUAW';
        }
        if(thisinv.Company_Name__c == 'GRI USA Inc')
        {
            if(thisinv.Swiss_Vat_Invoice__c == true)
            {
                emailtemplateid  = (ID)string.valueOf(Application_Config__c.getInstance('GRIUSAIncSwiss').Value__c);
                emailtempName = 'GRIUSAIncSwiss';
            }
            else
            {
                emailtemplateid  = (ID)string.valueOf(Application_Config__c.getInstance('GRIUSAInc').Value__c);
                emailtempName = 'GRIUSAInc';
            } 
      
        //emailtemplateid = '00X7A000000MHqaUAG';
        }
        system.debug('################################# emailtemplateid' + emailtemplateid);
        emailtemp = emailtemplateid;
        thisinv.template__c = emailtemp;
        thisinv.emailtempName__c = emailtempName;
        
        //List<string> emailrecipients = new List<string>();
        //emailrecipients.add(thisinv.email_to_send__c);
        //try
        //{
        //system.debug('################################# thisinv.Id' + thisinv);
            //EmailUtils.sendEmail(emailrecipients, emailtemplateid, thisinv.Id, thisinv.Contact_Id__c, orgwideemailaddressid);
             //thisinv.Email_Sent__c = true;
             //thisinv.Email_Sent__c = true;
             //emailsent = true;
        //}
        //catch (Exception err){

        //}
        
       
    }
        
   
    
}