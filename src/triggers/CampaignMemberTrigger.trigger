/**
 * Created by Eric Bueno on 23/02/2019.
 */

trigger CampaignMemberTrigger on CampaignMember (after insert) {
    new CampaignMemberTriggerHandler().run();
}