/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Trigger que dispara ações AFTER do objeto Contact.
*
* NAME: Contact_After.trigger
* AUTHOR: VMDL                                                DATE: 19/08/2016
*******************************************************************************/
trigger Contact_After on Contact (after insert, after update) {

	if(!TriggerUtils.isEnabled('Contact')) return;

	if(Trigger.isInsert) 
	{
		ContactAtualizaConta.execute();
		SetExternallyAvailableImageTrue upd = new SetExternallyAvailableImageTrue();
		Id batchId = Database.executeBatch(upd);

		// Added by Renato Matheus Simião in 13/10/2018
		for (Contact c : Trigger.new) {

			if (String.isNotEmpty(c.CNPJ__c))
				WSSerproCallout.fUpdateContact(c.Id, c.CNPJ__c);
		}
	}
	else if(Trigger.isUpdate) 
	{
		ContactAtualizaConta.execute();
		SetExternallyAvailableImageTrue upd = new SetExternallyAvailableImageTrue();
		Id batchId = Database.executeBatch(upd);

		// Added by Renato Matheus Simião in 13/10/2018
		for (Contact c : Trigger.new) {

			if (String.isNotEmpty(c.CNPJ__c) && c.CNPJ__c != Trigger.oldMap.get(c.Id).CNPJ__c)
				WSSerproCallout.fUpdateContact(c.Id, c.CNPJ__c);
		}
	}
}