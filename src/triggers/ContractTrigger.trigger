/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure 
 * players
 ******************************************************************************
 * Trigger for Contract SObject (Membership)
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date      Author     Description
 * 02/08/18  AndersonP  First version.
 */
trigger ContractTrigger on Contract ( 
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) 
{

    if (Trigger.isBefore) 
    {
        ContractHandler.before();
    } 
    else if (Trigger.isAfter) 
    {
        ContractHandler.after();
    }
}