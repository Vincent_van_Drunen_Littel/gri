/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Trigger que dispara ações After do objeto Event Attendee.
*
* NAME: EventAttendee_after.trigger
* AUTHOR: EAD                                                DATE: 22/01/2018 
* Date        Author         Description
* 24/07/18    AndersonP      Trigger disabled. Now Event attendee triggers are
*                            managed by the class EventAttendeeHandler.
*******************************************************************************/
trigger EventAttendee_after on Event_Attendee__c (after insert, after update) {
    /*
	if(!TriggerUtils.isEnabled('Event_Attendee__c')) return;
    
    if(Trigger.isInsert){
        
    }
    
    if(Trigger.isUpdate){
        //EventAttendeeAlteraSessionAttendee.execute();
    } 
    */ 
}