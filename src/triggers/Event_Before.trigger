/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Trigger que dispara ações Before do objeto Session.
*
* NAME: Event_Before.trigger
* AUTHOR: VMDL                                                DATE: 15/08/2016
*******************************************************************************/
trigger Event_Before on Event (before insert, before update) {

	if(!TriggerUtils.isEnabled('Event')) return;

	if(Trigger.isInsert) 
	{
		EventUpdateOppNextStepStatusAtual.execute();
	}

	if(Trigger.isUpdate) 
	{
		EventUpdateOppNextStepStatusAtual.execute();
	}
}