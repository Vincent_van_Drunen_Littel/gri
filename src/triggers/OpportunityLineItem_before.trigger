/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Trigger que dispara ações BEFORE do objeto OpportunityLineItem.
*
* NAME: OpportunityLineItem_before.trigger
* AUTHOR: EAD                                                  DATE: 07/02/2018
*******************************************************************************/

trigger OpportunityLineItem_before on OpportunityLineItem (before insert, before update) {
	if(!TriggerUtils.isEnabled('OpportunityLineItem')) return;
	
	if(Trigger.isInsert) {
	  OpportunityLineItemUpdatesOportunidade.execute();
	}
    if(Trigger.isUpdate){
      OpportunityLineItemUpdatesOportunidade.execute();
    }
}