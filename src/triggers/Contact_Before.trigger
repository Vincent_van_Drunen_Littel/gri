/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Trigger que dispara ações BEFORE do objeto Contact.
*
* NAME: Contact_Before.trigger
* AUTHOR: VOD                                                DATE: 29/01/2018
*******************************************************************************/
trigger Contact_Before on Contact (before update) {

if(!TriggerUtils.isEnabled('Contact')) return;

    if(Trigger.isUpdate) 
    {
        ContactUnmerge.execute();
    }
    
}