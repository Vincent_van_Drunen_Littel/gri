/*
Added new custome settings (Added by Darren)
Custome Settings: 
quoteTriggerSettings (quoteTriggerSettings__c)
quoteTriggerSettingsSPEX (quoteTriggerSettingsSPEX__c)

Update done by Darren on 28-03-2018. 
*/
trigger quoteTrigger on Quote (before insert, after insert, after update)
{
	list<Quote> qList = new list<Quote>();  
	Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>();
	set<ID> oppIDs = new set<ID>();
	boolean updatinnow = false;

	/*
	Added by Darren 28-03-2018
	*/
	Approval.ProcessSubmitRequest approvalrequest;


	String ApprovalProcess_InfraClubIndia = quoteTriggerSettings__c.getInstance('Infra Club India').ApprovalProcessID__c;
	String ApprovalProcess_InfraClubLatAm = quoteTriggerSettings__c.getInstance('Infra Club LatAm').ApprovalProcessID__c;	
	String ApprovalProcess_REClubBrazil = quoteTriggerSettings__c.getInstance('RE Club Brazil').ApprovalProcessID__c;
	String ApprovalProcess_REClubEurope = quoteTriggerSettings__c.getInstance('RE Club Europe').ApprovalProcessID__c;
	String ApprovalProcess_REClubIndia = quoteTriggerSettings__c.getInstance('RE Club India').ApprovalProcessID__c;
	String ApprovalProcess_REClubLatAM = quoteTriggerSettings__c.getInstance('RE Club LatAm').ApprovalProcessID__c;
	
	String ApprovalProcessSPEX_InfraIndia = quoteTriggerSettingsSPEX__c.getInstance('Infra Club India').ProcessID__c;
	String ApprovalProcessSPEX_InfraLatAm = quoteTriggerSettingsSPEX__c.getInstance('Infra Club LatAm').ProcessID__c;	
	String ApprovalProcessSPEX_REClubIndia = quoteTriggerSettingsSPEX__c.getInstance('RE Club India').ProcessID__c;
	String ApprovalProcessSPEX_REClubBrazil = quoteTriggerSettingsSPEX__c.getInstance('RE Club Brazil').ProcessID__c;
	String ApprovalProcessSPEX_REClubLatAm = quoteTriggerSettingsSPEX__c.getInstance('RE Club LatAm').ProcessID__c;
	String ApprovalProcessSPEX_REClubEurope = quoteTriggerSettingsSPEX__c.getInstance('RE Club Europe').ProcessID__c;
	String ApprovalProcessSPEX_SmartUs = quoteTriggerSettingsSPEX__c.getInstance('Smartus - Real Estate').ProcessID__c;

	String ProposalApprovalProcess;
	boolean secCanAdd=false;

	system.debug('#### ApprovalProcess_InfraClubIndia:' + ApprovalProcess_InfraClubIndia);
	system.debug('#### ApprovalProcess_InfraClubLatAm:' + ApprovalProcess_InfraClubLatAm);
	system.debug('#### ApprovalProcess_REClubBrazil:' + ApprovalProcess_REClubBrazil);
	system.debug('#### ApprovalProcess_REClubEurope:' + ApprovalProcess_REClubEurope);
	system.debug('#### ApprovalProcess_REClubIndia:' + ApprovalProcess_REClubIndia);
	system.debug('#### ApprovalProcess_REClubLatAM:' + ApprovalProcess_REClubLatAM);
	
	system.debug('#### ApprovalProcessSPEX_InfraIndia:' + ApprovalProcessSPEX_InfraIndia);
	system.debug('#### ApprovalProcessSPEX_InfraLatAm:' + ApprovalProcessSPEX_InfraLatAm);
	system.debug('#### ApprovalProcessSPEX_REClubIndia:' + ApprovalProcessSPEX_REClubIndia);
	system.debug('#### ApprovalProcessSPEX_REClubBrazil:' + ApprovalProcessSPEX_REClubBrazil);
	system.debug('#### ApprovalProcessSPEX_REClubLatAm:' + ApprovalProcessSPEX_REClubLatAm);
	system.debug('#### ApprovalProcessSPEX_REClubEurope:' + ApprovalProcessSPEX_REClubEurope);
	system.debug('#### ApprovalProcessSPEX_SmartUs:' + ApprovalProcessSPEX_SmartUs);

	/*
	Added by Darren 28-03-2018 End
	*/

	//system.debug('#### TriggerUtilsT.IsInsertQuote =' + TriggerUtilsT.IsInsertQuote);
	Set<Id> quoteIds = new Set<Id>();

    for (Quote thisq : Trigger.new)
    {
		oppIDs.add(thisq.OpportunityId);

		if(thisq.Id != null)
		{
			quoteIds.add(thisq.Id);
		}
    }
    
    Opportunity[] allopps= [SELECT Id, Product_Exceptions__c ,General_Notes__c ,Payment_Condition_s__c, OppRecordTypeName__c, Club_ID__c, Club_Name__c,
    							Club__r.Name, Event__r.Club__r.Name 
    						FROM Opportunity 
    						WHERE Id IN :oppIDs];

    system.debug('#### allopps=' + allopps); 
    for(Opportunity oh : allopps)
    {
       oppMap.put(oh.id, oh);
    }

    List<ProcessInstance> approvalInstances = [SELECT Id, TargetObjectId, Status FROM ProcessInstance WHERE TargetObjectId IN :quoteIds AND Status = 'Pending'];
    
    for (Quote thisquote :System.Trigger.new) 
    {
		system.debug('#### thisquote =' + thisquote);  
		           
		Opportunity thisopp = oppMap.get(thisquote.OpportunityId);
		system.debug('#### thisopp =' + thisopp); 
	   
		secCanAdd=false;

		if (thisquote.Product_Exception__c !=null || thisquote.General_Notes__c !=null || thisquote.Payment_Condition_s__c !=null ){
			
			system.debug('#### Can be added!!!!!');
			secCanAdd=true;	   	
		}	   
	        
		if(Trigger.isBefore)
		{       
	       	//Quote tquote = new Quote(id = thisquote.id);
	      
	        if(thisopp.Product_Exceptions__c != null)
	        {
	            system.debug('#### thisopp.Product_Exceptions__c =' + thisopp.Product_Exceptions__c); 
	            thisquote.Product_Exception__c = thisopp.Product_Exceptions__c;
	            updatinnow = true;
	        }

	        if(thisopp.General_Notes__c != null)
	        {
	            system.debug('#### thisopp.General_Notes__c =' + thisopp.General_Notes__c); 
	            thisquote.General_Notes__c = thisopp.General_Notes__c;
	            updatinnow = true;
	        }

	        if(thisopp.Payment_Condition_s__c != null)
	        {
	            system.debug('#### thisopp.Payment_Condition_s__c =' + thisopp.Payment_Condition_s__c);
	            thisquote.Payment_Condition_s__c = thisopp.Payment_Condition_s__c;
	            updatinnow = true;
	        }
	        
	        system.debug('#### updatinnow =' + updatinnow);      
	         
	        if(updatinnow != false && (thisopp.OppRecordTypeName__c == 'SPEX' || thisopp.OppRecordTypeName__c == 'Smartus - Spex'))
	        {
	            system.debug('#### updating checkbox'); 
	            thisquote.spex_approval_insert__c = true;
	        }
	        else if(updatinnow != false && thisquote.Opp_Club_name__c != null)
	        {
	            system.debug('#### updating checkbox'); 
	            thisquote.approval_checkbox__c = true;
	        }	        
    	}

        /*
     	* Update:28-03-2018
     	* Added by Darren 
     	* Start
    	*/    	
    	
    	if(trigger.isafter && trigger.isinsert)
    	{
    		if (secCanAdd == true)
    		{    			
	    		if(thisopp.Club_ID__c != null && thisquote.approval_checkbox__c ==true)
	    		{
		        	/*
		        	Get the value from the table quoteTriggerSettings__c, iterate each of the clubs and then 
		        	check to see if the values stored in the current op and current quite match the 
		        	values, if they do then execute the corresponding aproval process. 
		        	*/
		        	
		        	System.debug('##### Club_Name__c:' +thisopp.Club_Name__c );
		        	
					approvalrequest = new Approval.ProcessSubmitRequest();
	     			approvalrequest.setComments('Submitting request for approval.');
	     			approvalrequest.setObjectId(thisquote.id);
	     			approvalrequest.setSubmitterId(UserInfo.getUserId());
	     			approvalrequest.setSkipEntryCriteria(true);

	        		if(thisopp.Club_Name__c == 'RE Club LatAm')
	        		{
		     			approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubLatAM);							
						System.debug('#### approvalrequest: ' + approvalrequest);
						Approval.ProcessResult result = Approval.process(approvalrequest); 	
	        		}
	        		else if(thisopp.Club_Name__c == 'RE Club India')
	        		{
	     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubIndia);
						System.debug('#### approvalrequest: ' + approvalrequest);
						Approval.ProcessResult result = Approval.process(approvalrequest); 
	        		}			        		
	        		else if(thisopp.Club_Name__c == 'RE Club Europe')
	        		{ 
	     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubEurope);
						System.debug('#### approvalrequest: ' + approvalrequest);
						Approval.ProcessResult result = Approval.process(approvalrequest); 
	        		}		        		
	        		else if(thisopp.Club_Name__c == 'RE Club Brazil')
	        		{ 
	     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubBrazil);
						System.debug('#### approvalrequest: ' + approvalrequest);
						Approval.ProcessResult result = Approval.process(approvalrequest); 
	        		}		        		
	        		else if(thisopp.Club_Name__c == 'Infra Club LatAm')
	        		{
	     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_InfraClubLatAm);
						System.debug('#### approvalrequest: ' + approvalrequest);
						Approval.ProcessResult result = Approval.process(approvalrequest); 
	        		}		        		
	        		else if(thisopp.Club_Name__c == 'Infra Club India')
	        		{
	     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_InfraClubIndia);
						System.debug('#### approvalrequest: ' + approvalrequest);
						Approval.ProcessResult result = Approval.process(approvalrequest); 
	        		}
	     		}
     		
		     	if (thisquote.spex_approval_insert__c == true)
		     	{
     				approvalrequest = new Approval.ProcessSubmitRequest();
		     		approvalrequest.setComments('Submitting request for approval.');
		     		approvalrequest.setObjectId(thisquote.id);
		     		approvalrequest.setSubmitterId(UserInfo.getUserId()); 	
		     		approvalrequest.setSkipEntryCriteria(true);

     				if (thisopp.OppRecordTypeName__c == 'SPEX')
		     		{
		     			if(thisopp.Club__r.Name == 'RE Club Brazil' || thisopp.Event__r.Club__r.Name == 'RE Club Brazil')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubBrazil);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'RE Club LatAm' || thisopp.Event__r.Club__r.Name == 'RE Club LatAm')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubLatAm);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'Infra Club LatAm' || thisopp.Event__r.Club__r.Name == 'Infra Club LatAm')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_InfraLatAm);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'Infra Club India' || thisopp.Event__r.Club__r.Name == 'Infra Club India')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_InfraIndia);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'RE Club India' || thisopp.Event__r.Club__r.Name == 'RE Club India')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubIndia);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}			   			
		     			else if(thisopp.Club__r.Name == 'RE Club Europe' || thisopp.Event__r.Club__r.Name == 'RE Club Europe')
		     			{
		     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubEurope);
		     				System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     		}
		     		else if(thisopp.OppRecordTypeName__c == 'Smartus - Spex')
		     		{
		     			if(thisopp.Club__r.Name == 'Smartus - Real Estate' || thisopp.Event__r.Club__r.Name == 'Smartus - Real Estate')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_SmartUs);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     		}	     			
		     	} 
		    }   		
    	}

     	if(trigger.isafter && trigger.isupdate)
     	{
     		system.debug('####------------------####');
     		system.debug('#### IsUpdate Trigger ####');
     		system.debug('####------------------####');
     		
     		Boolean alertInApproval = false;

			for(ProcessInstance pi : approvalInstances)
			{
				if(pi.TargetObjectId == thisquote.Id)
				{
					alertInApproval = true;
					break;
				}
			}

			if(alertInApproval == true)
			{
				continue;
			}	     		
	     	
	     	System.debug('#### thisopp.OppRecordTypeName__c:' + thisopp.OppRecordTypeName__c);
	     	
	     	if (secCanAdd == true)
	     	{
	     		if(thisopp.Club_ID__c != null)
		     	{
		     		if(Trigger.oldMap.get(thisquote.id).approval_updated_dt__c != thisquote.approval_updated_dt__c)
		     		{		     		
		 				approvalrequest = new Approval.ProcessSubmitRequest();
			     		approvalrequest.setComments('Submitting request for approval.');
			     		approvalrequest.setObjectId(thisquote.id);
			     		approvalrequest.setSubmitterId(UserInfo.getUserId());
			     		approvalrequest.setSkipEntryCriteria(true);

			     		if(thisopp.Club_Name__c == 'RE Club LatAm')
			     		{
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubLatAM);							
							System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest); 
			     		}

			     		if(thisopp.Club_Name__c == 'RE Club India')
			     		{
			     			approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubIndia);
							System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
			     		}

			     		if(thisopp.Club_Name__c == 'RE Club Europe')
			     		{
			     			approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubEurope);
							System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
			     		}

			     		if(thisopp.Club_Name__c == 'RE Club Brazil')
			     		{
			     			approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_REClubBrazil);
							System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest); 
			     		}

			     		if(thisopp.Club_Name__c == 'Infra Club LatAm')
			     		{
			     			approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_InfraClubLatAm);
							System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest); 	
			     		}

			     		if(thisopp.Club_Name__c == 'Infra Club India')
			     		{
			     			approvalrequest.setProcessDefinitionNameOrId(ApprovalProcess_InfraClubIndia);
							System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest); 
			     		}			     	
				    }
		     	}

		     	if (Trigger.oldMap.get(thisquote.id).spex_approval_update_dt__c != thisquote.spex_approval_update_dt__c)
		     	{		     			     				
	     			approvalrequest = new Approval.ProcessSubmitRequest();
		     		approvalrequest.setComments('Submitting request for approval.');
		     		approvalrequest.setObjectId(thisquote.id);
		     		approvalrequest.setSubmitterId(UserInfo.getUserId()); 	
		     		approvalrequest.setSkipEntryCriteria(true);

     				if (thisopp.OppRecordTypeName__c == 'SPEX')
		     		{
		     			if(thisopp.Club__r.Name == 'RE Club Brazil' || thisopp.Event__r.Club__r.Name == 'RE Club Brazil')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubBrazil);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'RE Club LatAm' || thisopp.Event__r.Club__r.Name == 'RE Club LatAm')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubLatAm);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'Infra Club LatAm' || thisopp.Event__r.Club__r.Name == 'Infra Club LatAm')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_InfraLatAm);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'Infra Club India' || thisopp.Event__r.Club__r.Name == 'Infra Club India')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_InfraIndia);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     			else if(thisopp.Club__r.Name == 'RE Club India' || thisopp.Event__r.Club__r.Name == 'RE Club India')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubIndia);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}			   			
		     			else if(thisopp.Club__r.Name == 'RE Club Europe' || thisopp.Event__r.Club__r.Name == 'RE Club Europe')
		     			{
		     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubEurope);
		     				System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     		}
		     		else if(thisopp.OppRecordTypeName__c == 'Smartus - Spex')
		     		{
		     			if(thisopp.Club__r.Name == 'Smartus - Real Estate' || thisopp.Event__r.Club__r.Name == 'Smartus - Real Estate')
		     			{ 	
				     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_SmartUs);
				     		System.debug('#### approvalrequest: ' + approvalrequest);
							Approval.ProcessResult result = Approval.process(approvalrequest);
		     			}
		     		}
		     	}
		     	else
		     	{		     				     			
	     			System.debug('####-------------------------------####');
	     			System.debug('#### thisquote.Status:' + thisquote.Status);
	     			System.debug('#### thisquote.Status(OLD):' + Trigger.oldMap.get(thisquote.id).Status);
	     			System.debug('####-------------------------------####');
	     				     			
	     			if (Trigger.oldMap.get(thisquote.id) != thisquote)
	     			{
	     				if ((Trigger.oldMap.get(thisquote.id).Status == 'In Review' && thisquote.Status == 'Approved') 
	     					|| (Trigger.oldMap.get(thisquote.id).Status == 'In Review' && thisquote.Status == 'Rejected'))
	     				{
	     				}
	     				else
	     				{
	     					if (thisquote.Status != 'In Review')
	     					{				     				
				     			approvalrequest = new Approval.ProcessSubmitRequest();
					     		approvalrequest.setComments('Submitting request for approval.');
					     		approvalrequest.setObjectId(thisquote.id);
					     		approvalrequest.setSubmitterId(UserInfo.getUserId()); 	
					     		approvalrequest.setSkipEntryCriteria(true);

			     				if (thisopp.OppRecordTypeName__c == 'SPEX')
					     		{
					     			if(thisopp.Club__r.Name == 'RE Club Brazil' || thisopp.Event__r.Club__r.Name == 'RE Club Brazil')
					     			{ 	
							     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubBrazil);
							     		System.debug('#### approvalrequest: ' + approvalrequest);
										Approval.ProcessResult result = Approval.process(approvalrequest);
					     			}
					     			else if(thisopp.Club__r.Name == 'RE Club LatAm' || thisopp.Event__r.Club__r.Name == 'RE Club LatAm')
					     			{ 	
							     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubLatAm);
							     		System.debug('#### approvalrequest: ' + approvalrequest);
										Approval.ProcessResult result = Approval.process(approvalrequest);
					     			}
					     			else if(thisopp.Club__r.Name == 'Infra Club LatAm' || thisopp.Event__r.Club__r.Name == 'Infra Club LatAm')
					     			{ 	
							     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_InfraLatAm);
							     		System.debug('#### approvalrequest: ' + approvalrequest);
										Approval.ProcessResult result = Approval.process(approvalrequest);
					     			}
					     			else if(thisopp.Club__r.Name == 'Infra Club India' || thisopp.Event__r.Club__r.Name == 'Infra Club India')
					     			{ 	
							     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_InfraIndia);
							     		System.debug('#### approvalrequest: ' + approvalrequest);
										Approval.ProcessResult result = Approval.process(approvalrequest);
					     			}
					     			else if(thisopp.Club__r.Name == 'RE Club India' || thisopp.Event__r.Club__r.Name == 'RE Club India')
					     			{ 	
							     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubIndia);
							     		System.debug('#### approvalrequest: ' + approvalrequest);
										Approval.ProcessResult result = Approval.process(approvalrequest);
					     			}			   			
					     			else if(thisopp.Club__r.Name == 'RE Club Europe' || thisopp.Event__r.Club__r.Name == 'RE Club Europe')
					     			{
					     				approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_REClubEurope);
					     				System.debug('#### approvalrequest: ' + approvalrequest);
										Approval.ProcessResult result = Approval.process(approvalrequest);
					     			}
					     		}
					     		else if(thisopp.OppRecordTypeName__c == 'Smartus - Spex')
					     		{
					     			if(thisopp.Club__r.Name == 'Smartus - Real Estate' || thisopp.Event__r.Club__r.Name == 'Smartus - Real Estate')
					     			{ 	
							     		approvalrequest.setProcessDefinitionNameOrId(ApprovalProcessSPEX_SmartUs);
							     		System.debug('#### approvalrequest: ' + approvalrequest);
										Approval.ProcessResult result = Approval.process(approvalrequest);
					     			}
					     		}
		     				}	
	     				}	     				
	     			}		     			     		     		
		     	} 
	     	}	
     	}     	
    }
}