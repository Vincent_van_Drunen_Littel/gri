@isTest
public class Test_AttendeeSyncService {

    private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');

  @isTest
  static void TestAttendeeSyncService1()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);
    Application_Config__c cs = new Application_Config__c();
    cs.Name='BucketUserId';
    cs.value__c ='00536000004lQpq';
        //cs.Other fiels values
    insert cs;

    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Day 1';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    calNewObj.isInviteesLimitReached__c = false;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.GoogleCalendarEventId__c = 'd5vpi2u6qv834a6tasu2nuctvg';
    calNewObj.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj.Visibility__c =  'private';
    insert calNewObj;
    
    Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
    calInviteeObj.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj.Invite_Status__c = 'Pending';
    calInviteeObj.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj.Opportunity__c = iOpp.Id;
    insert calInviteeObj;
    
    Test.startTest();
    GoolgeCalendarCalloutMock fakeResponse1 = new GoolgeCalendarCalloutMock(200,
                                                 'Complete',
        '{"kind": "calendar#event","etag": "3085641145925000\","id": "d5vpi2u6qv834a6tasu2nuctvg","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=ZDV2cGkydTZxdjgzNGE2dGFzdTJudWN0dmcgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-21T11:59:09.000Z","updated": "2018-11-21T17:45:13.051Z","summary": "Europe GRI - Day 2","description": "Day 2 - GMT + 00 - EU","location": "Tower of London , St Katharine & Wapping, London, UK","creator": {"email": "griclub@griclub.org","self": true},"organizer": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-21T21:00:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-21T21:15:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "d5vpi2u6qv834a6tasu2nuctvg@google.com","sequence": 3,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"},{"email": "alicia.botelho@griclub.org","responseStatus": "declined"}],"guestsCanInviteOthers": false,"guestsCanSeeOtherGuests": false,"reminders": {"useDefault": true}}',
         null);
    Test.setMock(HttpCalloutMock.class, fakeResponse1); 
    RestRequest request = new RestRequest();
    request.requestUri ='/AttendeeSyncService';
    request.httpMethod = 'POST';
    request.headers.put('X-Goog-Channel-ID', 'd5vpi2u6qv834a6tasu2nuctvg');
    request.headers.put('X-Goog-Resource-ID', 'ig80O_ha8GmNFBD9Kju0C1l7GZ0');
    request.headers.put('X-Goog-Resource-URI', 'https://www.googleapis.com/calendar/v3/calendars/griclub@griclub.org/events?maxResults=250&alt=json');
    request.addHeader('Content-Type', 'application/json');
    RestContext.request = request;
    try{
    AttendeeSyncService.dopost();
    }catch(exception e){
    
    }
    
    Test.StopTest();
  }
 @isTest
  static void TestAttendeeSyncService2()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Application_Config__c cs = new Application_Config__c();
    cs.Name='BucketUserId';
    cs.value__c ='00536000004lQpq';
        //cs.Other fiels values
    insert cs;
    
    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Day 1';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    calNewObj.isInviteesLimitReached__c = false;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.GoogleCalendarEventId__c = 'd5vpi2u6qv834a6tasu2nuctvg';
    calNewObj.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj.Visibility__c =  'private';
    insert calNewObj;
    
    Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
    calInviteeObj.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj.Invite_Status__c = 'Pending';
    calInviteeObj.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj.Opportunity__c = iOpp.Id;
    insert calInviteeObj;
    
    Test.startTest();
    GoolgeCalendarCalloutMock fakeResponse1 = new GoolgeCalendarCalloutMock(200,
                                                 'Complete',
        '{"kind": "calendar#event","etag": "3085641145925000\","id": "d5vpi2u6qv834a6tasu2nuctvg","status": "Cancelled","htmlLink": "https://www.google.com/calendar/event?eid=ZDV2cGkydTZxdjgzNGE2dGFzdTJudWN0dmcgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-21T11:59:09.000Z","updated": "2018-11-21T17:45:13.051Z","summary": "Europe GRI - Day 2","description": "Day 2 - GMT + 00 - EU","location": "Tower of London , St Katharine & Wapping, London, UK","creator": {"email": "griclub@griclub.org","self": true},"organizer": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-21T21:00:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-21T21:15:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "d5vpi2u6qv834a6tasu2nuctvg@google.com","sequence": 3,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"},{"email": "alicia.botelho@griclub.org","responseStatus": "Cancelled"}],"guestsCanInviteOthers": false,"guestsCanSeeOtherGuests": false,"reminders": {"useDefault": true}}',
         null);
    Test.setMock(HttpCalloutMock.class, fakeResponse1); 
    RestRequest request = new RestRequest();
    request.requestUri ='/AttendeeSyncService';
    request.httpMethod = 'POST';
    request.headers.put('X-Goog-Channel-ID', 'd5vpi2u6qv834a6tasu2nuctvg');
    request.headers.put('X-Goog-Resource-ID', 'ig80O_ha8GmNFBD9Kju0C1l7GZ0');
    request.headers.put('X-Goog-Resource-URI', 'https://www.googleapis.com/calendar/v3/calendars/griclub@griclub.org/events?maxResults=250&alt=json');
    request.addHeader('Content-Type', 'application/json');
    RestContext.request = request;
    try{
    AttendeeSyncService.dopost();
    }catch(exception e){
    
    }
    
    Test.StopTest();
  }
}