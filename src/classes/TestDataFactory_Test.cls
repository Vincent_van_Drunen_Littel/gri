/**************************************************************************************************
* GRI Club - Gathering the world’s leading real estate and infrastructure players 
**************************************************************************************************
* A test class for TestDataFactory
* 
* @author  Anderson Paschoalon
* @version 1.0
* @since
* Date        Author         Description
* 11/06/18    AndPaschoalon  First version
*/
@IsTest
public class TestDataFactory_Test
{  
    private static final Integer NumberContactsPerAccount=10;
    private static final Integer NumberAccounts=2;
    private static final String LOGTAG='%% TestDataFactory: ';
    
    @isTest
    static void test_build()
    {
        List<Opportunity> listOpps = [SELECT Id FROM Opportunity];
        List<Contact> listContacts = [SELECT Id FROM Contact];
        List<Account> listAccounts = [SELECT Id FROM Account];
        List<Club__c> listClubs = [SELECT Id FROM Club__c];
        
        Test.startTest();
        System.assert(listOpps.size()>0);
        System.assert(listContacts.size()>0);
        System.assert(listAccounts.size()>0);
        System.assert(listClubs.size()>0);
        for(Opportunity opp:listOpps)
        {
            opp.bypassValidationRules__c = True;
            opp.ApprovalCommitteeResult__c = TestDataFactory.OPP_COMMITTEE_ACCEPTED;
            //opp.StageName = 'Approved by Finance';
        }
        update listOpps;
        Test.stopTest();
    }
    
    @isTest
    static void test_addProduct()
    {
        List<Opportunity> listOpps = [SELECT Id FROM Opportunity LIMIT 10];
        //List<Contact> listContacts = [SELECT Id FROM Contact];
        //List<Account> listAccounts = [SELECT Id FROM Account];
        Product2 theProd = [SELECT Id FROM Product2 WHERE ProductCode='P-0001212'];
        Product2 abacate = [SELECT Id FROM Product2 WHERE ProductCode='Abacate'];
        PricebookEntry pricebook = [SELECT Id FROM PricebookEntry WHERE Product2Id=:theProd.Id];
        PricebookEntry pricebookAbacate = [SELECT Id FROM PricebookEntry WHERE Product2Id=:abacate.Id];
        TestDataFactory.addProductToOpportunity(pricebook, theProd, listOpps);
        List<OpportunityLineItem> oppLineItems = [SELECT Id FROM OpportunityLineItem];
        //List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItems>();
        
        Test.startTest();
        System.assert(listOpps.size()>0);
        System.assertEquals(listOpps.size(), oppLineItems.size(), 
                            'Number of Opportunities and OpportunityLineItems sont match');
        TestDataFactory.addProductToOpportunity(pricebookAbacate, abacate, listOpps);
        oppLineItems = [SELECT Id FROM OpportunityLineItem];
        System.assertEquals(2*listOpps.size(), oppLineItems.size(), 
                            'Number of Opportunities and OpportunityLineItems sont match');        
        Test.stopTest(); 
    }

    @isTest
    static void test_addProduct2()
    {
        List<Opportunity> listOpps = [SELECT Id FROM Opportunity LIMIT 10];
        Product2 theProd = [SELECT Id FROM Product2 WHERE ProductCode='P-0001212'];

        Test.startTest();
        List<OpportunityLineItem> oppLineItems = TestDataFactory.addProductToOpportunity(TestDataFactory.createProduct('Prod-test'), listOpps);
        System.assert(listOpps.size()>0);
        System.assertEquals(listOpps.size(), oppLineItems.size(), 
            'Number of Opportunities and OpportunityLineItems dont match');
        Test.stopTest(); 
    }

    
    @isTest
    static void test_membership_creation()
    {
        
        Account someAccount = [SELECT Id,Name FROM Account LIMIT 1];
        List<Contact> listContacts = [SELECT Id,FirstName,AccountId 
                                      FROM Contact 
                                      WHERE AccountId=:someAccount.Id];
        Club__c someClub = [SELECT Id,Name FROM Club__c WHERE Name=:TestDataFactory.CLUB_RE_BRAZIL];
        
        ///////////////////////////////////////////////////////////////////////
        // test membership creation
        ///////////////////////////////////////////////////////////////////////
        
        Test.startTest();
        //CheckRecursive2.DisableCheckRecursive2=True;
        List<Contract> memberships = TestDataFactory.createMemberships(listContacts, 
                                                                       someClub, System.today()+365);
        System.assertEquals(listContacts.size(), memberships.size(), 
                            'The number of contacts and memberships created are different');
        Test.stopTest();
        
    }

    @isTest
    static void test_duplicateMemberships()
    {
        // TODO => this test is not running rightly
        Integer nContactacts = 20;
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(nContactacts);
        Club__c clubReBrazil = [SELECT Id,Name FROM Club__c WHERE Name=:HelperSObject.CLUB_RE_BRAZIL];
        Club__c clubReEurope = [SELECT Id,Name FROM Club__c WHERE Name=:HelperSObject.CLUB_RE_EUROPE];

        Test.startTest();

        //List<Contract> membershipsBr = TestDataFactory.createMemberships(
        //    listContacts, clubReBrazil, System.today()+365);
        //System.assertEquals(nContactacts, membershipsBr.size());
        //System.debug(LOGTAG+'membershipsBr='+HelperSObject.json(membershipsBr, 
        //    'Id, Status, Club__c, Club_Name__c, Name'));

        List<Contract> membershipsEu = TestDataFactory.createMemberships(
            listContacts, clubReEurope, System.today()+365);
        System.debug(LOGTAG+'membershipsEu='+HelperSObject.json(membershipsEu, 
            'Id, Status, Club__c, Club_Name__c, Name'));
        System.assertEquals(nContactacts, membershipsEu.size());

        Test.stopTest();

    }
    
    @isTest
    static void test_createContactsWithDefaultAccount()
    {
        Test.startTest();
        Integer nContactacts = 20;
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(nContactacts);
        System.assertEquals(nContactacts, listContacts.size(), 'Not righly created');
        Test.stopTest();
    }
    @isTest
    static void test_createContactsAccount()
    {
        Test.startTest();
        Integer nContactacts = 20;
        Integer nAccounts = 1;
        List<Account> listAcc = TestDataFactory.createAccounts(nAccounts);
        List<Contact> listCon = TestDataFactory.createContacts(listAcc[0], nContactacts);
        Test.stopTest();
    }
   
    @isTest
    static void test_createClub()
    {
        Test.startTest();
        Club__c aClub = TestDataFactory.createClubs('Cool club');
        System.assertNotEquals(NULL, aClub, 'Not righly created');
        Test.stopTest();
    }
    
    /*
    @isTest
    static void test_single()
    {
        
        Contact singleContact = TestDataFactory.createContactsWithDefaultAccount();
        Club__c someClub = [SELECT Id,Name FROM Club__c WHERE Name=:TestDataFactory.CLUB_RE_BRAZIL];
        Event__c theGreatEvent = [SELECT Id,Name,Status__c FROM Event__c WHERE Name='theGreatEvent'];
        
        Opportunity oppMemb = TestDataFactory.createMembershipOpps(singleContact, 
                                                                   someClub, System.today()+10);
        Opportunity oppEvent = TestDataFactory.createEventOpps(singleContact, 
                                                               theGreatEvent, someClub, System.today()+10);
        
        //Test.startTest();
        //CheckRecursive2.DisableCheckRecursive2=True;
        Contract membership = TestDataFactory.createMemberships(singleContact, 
                                                                someClub, System.today()+365);
        System.assertNotEquals(NULL, membership, 'Membership was not righly created');
        //Test.stopTest();
        
    }
    */

    @isTest
    static void test_UserCreation()
    {
        String jobRole = 'Business Analyst';
        Integer numberOfUsers = 50;

        Test.startTest();
        List<User> listCreatedUsers = TestDataFactory.createUsers(jobRole, numberOfUsers, jobRole);
        //List<User> listUsers = [SELECT Id, Name, Title FROM User WHERE Id:=listCreatedUsers];
        System.assertEquals(numberOfUsers, listCreatedUsers.size(), 
            'Number of users created is incorrect!');
        Test.stopTest();
    }


    @isTest
    static void test_pricebooks()
    {
        Club__c brClub = [SELECT Id,Name FROM Club__c WHERE Name=:TestDataFactory.CLUB_RE_BRAZIL];
        Event__c theGreatEvent = [SELECT Id,Name,Status__c FROM Event__c WHERE Name='theGreatEvent'];
        Magazine__c vejaMagazine = TestDataFactory.createMagazine('Test Magazine');

        Test.startTest();
        Pricebook2 thePriceBookEvent = TestDataFactory.createPricebookEvent(System.today()-100, System.today()+99999, brClub.Id, theGreatEvent.Id);
        Pricebook2 thePriceBookSpex = TestDataFactory.createPricebookSpex(System.today()-100, System.today()+99999, brClub.Id);
        Pricebook2 thePriceBookSpexEvent = TestDataFactory.createPricebookSpexSingleEvent(System.today()-100, System.today()+99999, brClub.Id, theGreatEvent.Id);
        Pricebook2 thePriceBookMembership = TestDataFactory.createPricebookMembership(System.today()-100, System.today()+99999, brClub.Id);
        Pricebook2 thePriceBookMagazine = TestDataFactory.createPricebookMagazine(System.today()-100, System.today()+99999, vejaMagazine.Id);
        System.assertNotEquals(NULL, thePriceBookEvent, 'thePriceBookEvent');
        System.assertNotEquals(NULL, thePriceBookSpex, 'thePriceBookSpex');
        System.assertNotEquals(NULL, thePriceBookSpexEvent, 'thePriceBookSpexEvent');
        System.assertNotEquals(NULL, thePriceBookMembership, 'thePriceBookMembership');
        System.assertNotEquals(NULL, thePriceBookMagazine, 'thePriceBookMagazine');
        System.assertEquals(True, thePriceBookEvent.IsActive, 'thePriceBookEvent.IsActive');
        System.assertEquals(True, thePriceBookSpex.IsActive, 'thePriceBookSpex.IsActive');
        System.assertEquals(True, thePriceBookSpexEvent.IsActive, 'thePriceBookSpexEvent.IsActive');
        System.assertEquals(True, thePriceBookMembership.IsActive, 'thePriceBookMembership.IsActive');
        System.assertEquals(True, thePriceBookMagazine.IsActive, 'thePriceBookMagazine.IsActive');
        Test.stopTest();
    }

    //@istest Error on this test
    //@isTest
    static private void test_createProd() 
    {
        Club__c brClub = [SELECT Id,Name FROM Club__c WHERE Name=:TestDataFactory.CLUB_RE_BRAZIL];
        Event__c theGreatEvent = [SELECT Id,Name,Status__c FROM Event__c WHERE Name='theGreatEvent'];
        List<String> listStr = new List<String>{'P-001', 'P-002', 'P-003', 'P-004'};
        Pricebook2 thePriceBookEvent = TestDataFactory.createPricebookEvent(System.today()-100, System.today()+99999, brClub.Id, theGreatEvent.Id);

        Test.startTest();

        List<Product2> listProd = TestDataFactory.createProduct(listStr);
        for(Integer i=0; i<listStr.size(); i++)
        {
            System.assertEquals(listStr[i], listProd[i].ProductCode, 'Code and ProdCode dont match. Error on TestDataFactory.createProduct()');
        }
        List<PricebookEntry> listPbEntries = TestDataFactory.addProdToPricebook(listProd, thePriceBookEvent);
        System.assertEquals(listStr.size(), listProd.size(), 'Error on TestDataFactory.createProduct()');
        System.assertEquals(listStr.size(), listPbEntries.size(), 'Error on TestDataFactory.addProdToPricebook()');

        Test.stopTest(); 
    }

    @isTest 
    private static void test_createProdList()
    {
        List<String> listStr = new List<String>{'P-001', 'P-002', 'P-003', 'P-004'};
        List<Product2> listProd = TestDataFactory.createProduct(listStr);
        Test.startTest();
        System.assertEquals(listStr.size(), listProd.size());
        Test.stopTest();
    }

    @isTest 
    private static void test_errors()
    {
        List<Account> manyAcc = TestDataFactory.createAccounts(80);
        Event__c theGreatEvent = [SELECT Id, Name FROM Event__c WHERE Name='theGreatEvent'];
        Club__c someClub = [SELECT Id,Name FROM Club__c WHERE Name=:TestDataFactory.CLUB_RE_BRAZIL];
        List<Contact> listContacts = [SELECT Id, FirstName, LastName, AccountId FROM Contact];
        List<Account> emptyAccList = new List<Account>();
        List<Contact> nullConList = NULL;
        Event__c nullEvent = NULL;
        Club__c nullClub = NULL;

        Test.startTest();
        try 
        {
            TestDataFactory.createAccounts(0);
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        }
        try 
        {
            TestDataFactory.createContacts(emptyAccList , 80);
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        }
        try 
        {
            TestDataFactory.createContacts(manyAcc, 0);
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        }
        try 
        {
            TestDataFactory.createClubs(new List<String>());
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        }
        try 
        {
            TestDataFactory.createClubs('');
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        }
        try 
        {
            //TestDataFactory.createEventOpps(nullConList, theGreatEvent, someClub, System.today()+10);
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        } 
        try 
        {
            //TestDataFactory.createEventOpps(listContacts, nullEvent, someClub, System.today()+10);
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        } 
        try 
        {
            //TestDataFactory.createEventOpps(listContacts, theGreatEvent, nullClub, System.today()+10);
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        } 
        try 
        {
            TestDataFactory.getRecTypeByDevName('Contact', 'Cogumelo_Frufru');
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        } 
        try 
        {
            TestDataFactory.getRecType('Contact', 'Zap sete copa espadilha e oro');
        }
        catch(TestDataFactory.TestDataException e)
        {
            System.debug('Error e='+e+' catch!');
        } 
        Test.stopTest();
    }

	@isTest
    private static void test_newdata1()
    {
        Integer nData = 30;
        Club__c clubReBr = [SELECT Id,Name FROM Club__c WHERE Name=:HelperSObject.CLUB_RE_BRAZIL];
        Event__c theGreatEvent2 = TestDataFactory.createEvent('theGreatEvent2',  clubReBr);
        
        // Contacts, Accounts and Leads
        List<Lead> listLeads = TestDataFactory.createLeadsWithDefaultCompany(nData);
        System.assertEquals(nData, listLeads.size());
        Account newAcc = TestDataFactory.createAccount('Single Acc');
        System.assertNotEquals(NULL, newAcc);
        List<String> accsnames = new List<String> {'Test1', 'Test2', 'Test3'};
        List<Account> listAccStrs = TestDataFactory.createAccounts(accsnames);
        System.assertEquals(accsnames.size(), listAccStrs.size());
        List<Contact> listContact = TestDataFactory.createContactsWithDefaultAccount(nData);
        System.assertEquals(nData, listContact.size());

        // Opportunities
        //List<Opportunity> listOppsMg = TestDataFactory.createMagazineOpps(listContact, clubReBr, System.today());
        //System.assertEquals(listContact.size(), listOppsMg.size());


        // Pricebook
        Pricebook2 pb = TestDataFactory.createPricebookSpex('Some Pb', System.today()-100, System.today()+99999, clubReBr.Id);
        
        // Email template
        //EmailTemplate theEmailTemplate = TestDataFactory.createEmailTemplate('Email Template');
        //System.assertNotEquals(NULL, theEmailTemplate);
    }
    
	@isTest
    private static void test_newdata2()
    {
        Integer nData = 10;
        Club__c clubReBr = [SELECT Id,Name FROM Club__c WHERE Name=:HelperSObject.CLUB_RE_BRAZIL];
        Event__c theGreatEvent2 = TestDataFactory.createEvent('theGreatEvent2',  clubReBr);
        List<Contact> listContact = TestDataFactory.createContactsWithDefaultAccount(nData);
		//List<Opportunity> listOppsMe = TestDataFactory.createMembershipOpps(listContact, clubReBr, System.today());
        //System.assertEquals(listContact.size(), listOppsMe.size());
    }

	@isTest
    private static void test_newdata3()
    {
        Integer nData = 10;
        Club__c clubReBr = [SELECT Id,Name FROM Club__c WHERE Name=:HelperSObject.CLUB_RE_BRAZIL];        
        List<Contact> listContact = TestDataFactory.createContactsWithDefaultAccount(nData);
        Event__c theGreatEvent2 = TestDataFactory.createEvent('theGreatEvent2',  clubReBr);
        //List<Opportunity> listOppsEv = TestDataFactory.createEventOpps(listContact, theGreatEvent2, clubReBr, System.today());
        //System.assertEquals(listContact.size(), listOppsEv.size());
  
    }
    
    @testSetup 
    static void buildData()
    {
        TestDataFactory.init();
        Integer nConPerAcc = NumberContactsPerAccount;
        Integer nAcc = NumberAccounts;
        List<Account> listAcc = TestDataFactory.createAccounts(nAcc);
        List<Contact> listCon = TestDataFactory.createContacts(listAcc[0], nConPerAcc);
        List<Contact> listCon2 = TestDataFactory.createContacts(listAcc[1], nConPerAcc);
        List<Club__c> listClubs = TestDataFactory.createClubs(TestDataFactory.listGriClubs());
        Club__c smartusCorp = TestDataFactory.createClubs(TestDataFactory.SMARTUS_REAL_ESTATE);
        Product2 theProd = TestDataFactory.createProduct('P-0001212');
        PricebookEntry thePricebookEntry = TestDataFactory.createPricebookEntry(theProd);
        Product2 abacate = TestDataFactory.createProduct('Abacate');
        PricebookEntry thePricebookAbacate = TestDataFactory.createPricebookEntry(abacate);
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent',  listClubs[0]);
        Event__c smartusEvent = TestDataFactory.createEvent('SmartusEvent',  listClubs[0]);
        List<Campaign> newListCampaign = TestDataFactory.createCampaigns(1, 'USD');//<<<<<OK
        List<Lead> listLeads = TestDataFactory.createLeadsWithDefaultCompany(2);//<<<<<OK


        Test.startTest();
        TestDataFactory.createSpexOpps(listCon, theGreatEvent, listClubs[0], System.today()+10);
        List<Opportunity> newListOpp = TestDataFactory.createMembershipOpps(listCon, listClubs[0], System.today()+10);//<<<<<OK
        TestDataFactory.createEventOpps(listCon, theGreatEvent, listClubs[0], System.today()+10);
        TestDataFactory.createSmartusEventOpps(listCon, smartusEvent, System.today()+10);
        TestDataFactory.createSmartusSpexOpps(listCon, theGreatEvent, smartusCorp, System.today()+10);
        TestDataFactory.createCampaignMembersWithContacts(newListCampaign[0], listCon, newListOpp);//<<<<<<OK
        TestDataFactory.createCampaignMembersWithLeads(newListCampaign[0], listLeads);//<<<<<<OK
        TestDataFactory.createOrderWithActEvtClub(listAcc[0], theGreatEvent, listClubs[0]);//<<<<<<OK
        TestDataFactory.createPricebookSpex('teste price', System.today(), System.today()+10, listClubs[0].Id);//<<<<<<OK
        Order newOrder = TestDataFactory.createOrderWithActEvtClub(listAcc[0], theGreatEvent, smartusCorp);//<<<<<<OK
        TestDataFactory.createEventOpps2(listCon2, theGreatEvent, listClubs[0], System.today()+10);//<<<<<<OK
        Test.stopTest();

        //TestDataFactory.addProductToOpportunity(thePricebookEntry, theProd, lOppMemb);
        //Opportunity  membOpp = TestDataFactory.createMembershipOpps(listCon[0],  listClubs[0], System.today()+10);
        //Opportunity  evOpp = TestDataFactory.createEventOpps(listCon[0], listClubs[0],  System.today()+10);
        
    }
    @isTest
    private static void test_newdata4()
    {
        TestDataFactory.init();
        Integer nConPerAcc = NumberContactsPerAccount;
        Integer nAcc = NumberAccounts;
        List<Account> listAcc = TestDataFactory.createAccounts(nAcc);
        List<Contact> listCon = TestDataFactory.createContacts(listAcc[0], nConPerAcc);
        List<Club__c> listClubs = TestDataFactory.createClubs(TestDataFactory.listGriClubs());
        Club__c smartusCorp = TestDataFactory.createClubs(TestDataFactory.SMARTUS_REAL_ESTATE);
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent',  listClubs[0]);
        Event__c smartusEvent = TestDataFactory.createEvent('SmartusEvent',  listClubs[0]);
        List<String> listDigitalFunnelStage = new List<String>{'Interest', 'Intent', 'Purchase'};//<<<<<OK
        List<Opportunity> newListOpp = TestDataFactory.createMembershipOpps(listCon, listClubs[0], System.today()+10);//<<<<<OK


        Test.startTest();
        TestDataFactory.createEventAttendee(newListOpp, theGreatEvent);//<<<<<OK
        TestDataFactory.createCampaignsDigitalFunnelWithOpps(1, listCon, smartusCorp, smartusEvent, listDigitalFunnelStage);//<<<<<OK
        TestDataFactory.createEventAttendee(newListOpp, theGreatEvent);//<<<<<OK
        Session__c newSession = TestDataFactory.createSessionWithEvent(theGreatEvent);//<<<<<OK
        TestDataFactory.createSessionAttendeesWithSession(newListOpp, theGreatEvent, newSession);//<<<<<OK
        TestDataFactory.createMembershipOpps(listCon[0], smartusCorp, System.today()+10);
        Test.stopTest();
    }
}