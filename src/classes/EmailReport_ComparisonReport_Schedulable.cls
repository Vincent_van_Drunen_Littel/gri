global class EmailReport_ComparisonReport_Schedulable implements Schedulable
{
	global void execute(SchedulableContext sc)
	{
		try {
			EmailTemplate templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Comparison_Report'];

			List<OrgWideEmailAddress> orgWideAddress = [select Id from OrgWideEmailAddress where Address = 'registrations@griclub.org'];

	        String recipientIds = Application_Config__c.getInstance('ComparisonReportRecipientUserIds').Value__c;
	        List<String> recipientIdsList = recipientIds.split(',');

	        String recipientEmails = Application_Config__c.getInstance('ComparisonReportRecipientEmails').Value__c;
	        List<String> recipientEmailsList = recipientEmails.split(',');

	        List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
	        
	    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	    	
	    	mail.setTargetObjectId(recipientIdsList[0]);

	    	List<String> fullRecipientList = new List<String>();
	    	fullRecipientList.addAll(recipientIdsList);

	    	if(recipientEmailsList != null && recipientEmailsList.Size() > 0)
	    	{
	    		fullRecipientList.addAll(recipientEmailsList);
	    	}

	    	mail.setToAddresses(fullRecipientList);
	    	mail.setSaveAsActivity(false);

			mail.setTemplateId(templateId.Id);

			if (orgWideAddress.size() > 0 ) {
			    mail.setOrgWideEmailAddressId(orgWideAddress[0].Id);
			}

			emailMessages.add(mail);	

			Messaging.sendEmail(emailMessages);
		}
		catch(Exception e)
		{
			System.debug(e.getMessage());
		}        
	}
}