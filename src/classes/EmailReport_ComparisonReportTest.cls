@isTest
private class EmailReport_ComparisonReportTest {

	private static Id REC_OPP_MEMBERSHIP = RecordTypeMemory.getRecType( 'Opportunity', 'Membership' );
	private static Id REC_OPP_MAGAZINE_AD = RecordTypeMemory.getRecType( 'Opportunity', 'Magazine' );
	private static Id REC_OPP_UNIQUE = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
	private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
	
	private static Account conta;
	private static Contact contato;
	private static List<Event__c> testEvents;
	private static List<Club__c> clubs;
	private static List<Magazine__c> magazines;
	private static List<PricebookEntry> pricebookEntries;
	private static List<Pricebook2> testPricebooks;	
	private static List<Product2> testProducts;

	private static void createTestData()
	{
		Application_Config__c conf = new Application_Config__c();
		conf.Name = 'BucketUserId';
		conf.Value__c = '00536000004lQpq';
		insert conf;

		conta = SObjectInstance.createAccount2();
	    Database.insert(conta);
	    
	    contato = SObjectInstance.createContato(conta.Id);
	    contato.LastName = 'Teste contato';
	    Database.insert(contato);

	    clubs = new List<Club__c>();
	    testEvents = new List<Event__c>();
	    magazines = new List<Magazine__c>();
	    pricebookEntries = new List<PricebookEntry>();
	    testPricebooks = new List<Pricebook2>();
	    testProducts = new List<Product2>();

	    Club__c club = SObjectInstance.createClub();
	    club.Designated_Region__c = 'Brazil RE'; 
	    club.Active__c = true;
	    club.Year_Target__c = 100;
	    clubs.add(club);

	    Club__c club2 = SObjectInstance.createClub();
	    club2.Designated_Region__c = 'Infra LatAm';
	    club2.Active__c = true; 
	    club2.Year_Target__c = 100;
	    clubs.add(club2);
	    insert clubs;	    

	    Event__c previousEvent = SObjectInstance.createEvent();
	    previousEvent.Type__c = 'Open Event';
	    previousEvent.Club__c = clubs[0].Id;
	    previousEvent.Show_in_Comparison_Report__c = true;
	    testEvents.add(previousEvent);

	    Event__c testEvent = SObjectInstance.createEvent();
	    testEvent.Type__c = 'Open Event';
	    testEvent.Club__c = clubs[0].Id;
	    testEvent.Show_in_Comparison_Report__c = true;
	    testEvents.add(testEvent);

	    Event__c testClubEvent = SObjectInstance.createEvent();
	    testClubEvent.Type__c = 'Club Event';
	    testClubEvent.Club__c = clubs[0].Id;
	    testClubEvent.Show_in_Comparison_Report__c = true;
	    testEvents.add(testClubEvent);

	    Event__c testClubEventPrevious = SObjectInstance.createEvent();
	    testClubEventPrevious.Type__c = 'Club Event';
	    testClubEventPrevious.Club__c = clubs[0].Id;
	    testClubEventPrevious.Show_in_Comparison_Report__c = true;
	    testEvents.add(testClubEventPrevious);

	    insert testEvents;

	    testEvents[0].Start_Date__c = system.today().addDays(-368);
	    testEvents[0].End_Date__c = system.today().addDays(-367);

	    testEvents[1].Start_Date__c = system.today().addDays(22);
	    testEvents[1].End_Date__c = system.today().addDays(23);
	    testEvents[1].Previous_Event__c = testEvents[0].Id;

	    testEvents[2].Start_Date__c = system.today().addDays(22);
	    testEvents[2].End_Date__c = system.today().addDays(23);
	    testEvents[2].Previous_Event__c = testEvents[3].Id;

	    testEvents[3].Start_Date__c = system.today().addDays(-368);
	    testEvents[3].End_Date__c = system.today().addDays(-367);	    

	    update testEvents;

	    Magazine__c magazine = SObjectInstance.createMagazine();
	    magazine.CurrencyIsoCode = 'BRL';
	    magazine.Club__c = clubs[0].Id;
	    magazine.Designated_Region_2__c = 'Brazil RE';
	    magazines.add(magazine);

	    /*magazine2 = SObjectInstance.createMagazine();
	    magazine2.CurrencyIsoCode = 'BRL';
	    magazine2.Club__c = clubs[1].Id;
	    magazine2.Designated_Region_2__c = 'Infra LatAm';
	    magazines.add(magazine2);

	    insert magazines;*/

	    Pricebook2 pb = SObjectInstance.catalogoDePreco();
	    pb.Club__c = clubs[0].Id;
	    pb.Magazine__c = magazines[0].Id;
	    pb.Type__c = 'Magazine Subscriptions';
	    pb.Event__c = testEvents[1].Id;
	    pb.Starting_date__c = system.today();
	    pb.End_date__c = system.today()+10;
	    testPricebooks.add(pb);

	    Pricebook2 pb2 = SObjectInstance.catalogoDePreco();
	    pb2.Club__c = clubs[0].Id;
	    pb2.Magazine__c = magazines[0].Id;
	    pb2.Type__c = 'Spex';
	    pb2.Event__c = testEvents[1].Id;
	    pb2.Starting_date__c = system.today()+3;	    
	    testPricebooks.add(pb2);

	    insert testPricebooks;

	    System.debug('#### testPricebooks' + testPricebooks);

	    Product2 testProduct = SObjectInstance.createProduct2();
	    testProduct.Name = 'Co-Chair';
	    testProducts.add(testProduct);

	    Product2 testProduct1 = SObjectInstance.createProduct2();
	    testProduct1.Name = 'Member Free';
	    testProducts.add(testProduct1);

	    Product2 testProduct2 = SObjectInstance.createProduct2();
	    testProduct2.Name = 'Guest Free';
	    testProducts.add(testProduct2);

	    insert testProducts;

	    pricebookEntries.add(SObjectInstance.entradaDePreco(test.getStandardPricebookId(), testProduct.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(pb.Id, testProduct.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(pb2.Id, testProduct.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(test.getStandardPricebookId(), testProduct1.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(pb.Id, testProduct1.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(pb2.Id, testProduct1.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(test.getStandardPricebookId(), testProduct2.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(pb.Id, testProduct2.Id));
	    pricebookEntries.add(SObjectInstance.entradaDePreco(pb2.Id, testProduct2.Id));
	    insert pricebookEntries;

	    System.debug('#### test.getStandardPricebookId()' + test.getStandardPricebookId());
	}

	@isTest static void testEmailReport_ComparisonReport() {
		createTestData();	   	
	   	
	    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	    opp.Event__c = testEvents[1].Id;
	    opp.Contact__c = contato.Id;
	    opp.CloseDate = system.today();
	    opp.StageName = 'Approved By Finance';
	    opp.Approved_by_Finance_is_Today__c = Date.Today();
	    opp.Club__c = clubs[0].Id;
	    opp.Finance_Status__c = 'Outstanding';
	    insert opp;

	    System.debug('#### opp' + opp);

	    OpportunityLineItem oppLineItem = new OpportunityLineItem(OpportunityId = opp.Id,
	    														PricebookEntryId = pricebookEntries[1].Id);
	    insert oppLineItem;

	    List<Event_Attendee__c> eventAttendees = new List<Event_Attendee__c>();

	    Event_Attendee__c eventAttendee = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee.Event_Attendees_Status__c = 'Registered';
	    eventAttendee.Session_Position__c = 'Co-Chair';
	    eventAttendees.add(eventAttendee);

	    Event_Attendee__c eventAttendee2 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee2.Event_Attendees_Status__c = 'Registered';
	    eventAttendee2.Session_Position__c = 'Delegate';
	    eventAttendees.add(eventAttendee2);

	    Event_Attendee__c eventAttendee3 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee3.Event_Attendees_Status__c = 'Registered';
	    eventAttendee3.Session_Position__c = 'Member Free';
	    eventAttendees.add(eventAttendee3);	    

	    insert eventAttendees;

	    Test.startTest();
	    Opportunity opp2 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    opp2.Event__c = testEvents[1].Id;
	    opp2.Contact__c = contato.Id;
	    opp2.CloseDate = system.today();
	    opp2.StageName = 'Approved By Finance';
	    opp2.Approved_by_Finance_is_Today__c = Date.Today();
	    opp2.Club__c = clubs[0].Id;
	    opp2.Finance_Status__c = 'Outstanding';
	    insert opp2;

	    System.debug('#### opp2' + opp2);

	    OpportunityLineItem oppLineItem2 = new OpportunityLineItem(OpportunityId = opp2.Id,
	    														PricebookEntryId = pricebookEntries[2].Id);
	    insert oppLineItem2;

	    Event_Attendee__c eventAttendee4 = SObjectInstance.createEventAttendee(opp2.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee4.Event_Attendees_Status__c = 'Registered';
	    eventAttendee4.Session_Position__c = 'Co-Chair';
	    insert eventAttendee4;

	    //Opportunity opp3 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    //opp3.Event__c = testEvents[0].Id;
	    //opp3.Contact__c = contato.Id;
	    //opp3.CloseDate = system.today();
	    //opp3.StageName = 'Approved By Finance';
	    //opp3.Approved_by_Finance_is_Today__c = Date.Today().addDays(-365);
	    //opp3.Club__c = clubs[0].Id;
	    //opp3.Finance_Status__c = 'Outstanding';
	    //insert opp3;

	    //System.debug('#### opp3' + opp3);

	    //OpportunityLineItem oppLineItem3 = new OpportunityLineItem(OpportunityId = opp3.Id,
	    //														PricebookEntryId = pricebookEntries[2].Id);
	    //insert oppLineItem3;

	    //Event_Attendee__c eventAttendee5 = SObjectInstance.createEventAttendee(opp3.Id, conta.Id, contato.Id, testEvents[1].Id);
	    //eventAttendee5.Event_Attendees_Status__c = 'Registered';
	    //eventAttendee5.Session_Position__c = 'Guest Free';
	    //insert eventAttendee5;

	    EmailReport_ComparisonReportGenerator step1 = new EmailReport_ComparisonReportGenerator();
	    step1.execute(null);

	    Test.stopTest();
	    
		EmailReport_ComparisonReport cont = new EmailReport_ComparisonReport();
		List<EmailReport_ComparisonReportGen_Helper.ClubComparison> club = cont.clubComparison;
		
	}

	@isTest static void testEmailReport_ComparisonReport2() {
		createTestData();	   	
	   	
	    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	    opp.Event__c = testEvents[1].Id;
	    opp.Contact__c = contato.Id;
	    opp.CloseDate = system.today();
	    opp.StageName = 'Approved By Finance';
	    opp.Approved_by_Finance_is_Today__c = Date.Today();
	    opp.Club__c = clubs[0].Id;
	    opp.Finance_Status__c = 'Outstanding';
	    opp.Pricebook2Id = testPricebooks[0].Id;
	    insert opp;

	    System.debug('#### opp' + opp);

	    List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();

	    oppLineItems.add(new OpportunityLineItem(OpportunityId = opp.Id,
	    										PricebookEntryId = pricebookEntries[1].Id));
	    

	    oppLineItems.add(new OpportunityLineItem(OpportunityId = opp.Id,
	    										PricebookEntryId = pricebookEntries[4].Id));
	    

	    oppLineItems.add(new OpportunityLineItem(OpportunityId = opp.Id,
	    										PricebookEntryId = pricebookEntries[7].Id));
	    insert oppLineItems;

	    List<Event_Attendee__c> eventAttendees = new List<Event_Attendee__c>();

	    Event_Attendee__c eventAttendee = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee.Event_Attendees_Status__c = 'Registered';
	    eventAttendee.Session_Position__c = 'Co-Chair';
	    eventAttendees.add(eventAttendee);

	    Event_Attendee__c eventAttendee2 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee2.Event_Attendees_Status__c = 'Registered';
	    eventAttendee2.Session_Position__c = 'Delegate';
	    eventAttendees.add(eventAttendee2);

	    Event_Attendee__c eventAttendee3 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee3.Event_Attendees_Status__c = 'Registered';
	    eventAttendee3.Session_Position__c = 'Member Free';
	    eventAttendees.add(eventAttendee3);

	    Event_Attendee__c eventAttendee4 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[0].Id);
	    eventAttendee4.Event_Attendees_Status__c = 'Registered';
	    eventAttendee4.Session_Position__c = 'Co-Chair';
	    eventAttendees.add(eventAttendee4);

	    Event_Attendee__c eventAttendee5 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[0].Id);
	    eventAttendee5.Event_Attendees_Status__c = 'Registered';
	    eventAttendee5.Session_Position__c = 'Delegate';
	    eventAttendees.add(eventAttendee5);

	    Event_Attendee__c eventAttendee6 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[0].Id);
	    eventAttendee6.Event_Attendees_Status__c = 'Registered';
	    eventAttendee6.Session_Position__c = 'Member Free';
	    eventAttendees.add(eventAttendee6);

	    insert eventAttendees;

	    //Test.startTest();
	    //Opportunity opp2 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    //opp2.Event__c = testEvents[1].Id;
	    //opp2.Contact__c = contato.Id;
	    //opp2.CloseDate = system.today();
	    //opp2.StageName = 'Approved By Finance';
	    //opp2.Approved_by_Finance_is_Today__c = Date.Today();
	    //opp2.Club__c = clubs[0].Id;
	    //opp2.Finance_Status__c = 'Outstanding';
	    //insert opp2;

	    //System.debug('#### opp2' + opp2);

	    //OpportunityLineItem oppLineItem2 = new OpportunityLineItem(OpportunityId = opp2.Id,
	    //														PricebookEntryId = pricebookEntries[2].Id);
	    //insert oppLineItem2;

	    //Event_Attendee__c eventAttendee7 = SObjectInstance.createEventAttendee(opp2.Id, conta.Id, contato.Id, testEvents[1].Id);
	    //eventAttendee7.Event_Attendees_Status__c = 'Registered';
	    //eventAttendee7.Session_Position__c = 'Co-Chair';
	    //insert eventAttendee7;

	    //Opportunity opp3 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    //opp3.Event__c = testEvents[0].Id;
	    //opp3.Contact__c = contato.Id;
	    //opp3.CloseDate = system.today();
	    //opp3.StageName = 'Approved By Finance';
	    //opp3.Approved_by_Finance_is_Today__c = Date.Today().addDays(-365);
	    //opp3.Club__c = clubs[0].Id;
	    //opp3.Finance_Status__c = 'Outstanding';
	    //insert opp3;

	    //System.debug('#### opp3' + opp3);

	    //OpportunityLineItem oppLineItem3 = new OpportunityLineItem(OpportunityId = opp3.Id,
	    //														PricebookEntryId = pricebookEntries[2].Id);
	    //insert oppLineItem3;

	    //Event_Attendee__c eventAttendee8 = SObjectInstance.createEventAttendee(opp3.Id, conta.Id, contato.Id, testEvents[1].Id);
	    //eventAttendee8.Event_Attendees_Status__c = 'Registered';
	    //eventAttendee8.Session_Position__c = 'Guest Free';
	    //insert eventAttendee8;

	    EmailReport_ComparisonReportGenerator step1 = new EmailReport_ComparisonReportGenerator();
	    step1.execute(null);

	    //Test.stopTest();
	    
		EmailReport_ComparisonReport cont = new EmailReport_ComparisonReport();
		List<EmailReport_ComparisonReportGen_Helper.EventComparison> futureEventComparison = cont.futureEventComparison;
		List<EmailReport_ComparisonReportGen_Helper.EventComparison> previousEventComparison = cont.previousEventComparison;
		
		
	}

	@isTest static void testEmailReport_ComparisonReport3() {
		createTestData();	   	
	   	
	    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	    opp.Event__c = testEvents[2].Id;
	    opp.Contact__c = contato.Id;
	    opp.CloseDate = system.today();
	    opp.StageName = 'Approved By Finance';
	    opp.Approved_by_Finance_is_Today__c = Date.Today();
	    opp.Club__c = clubs[0].Id;
	    opp.Finance_Status__c = 'Outstanding';
	    insert opp;

	    System.debug('#### opp' + opp);

	    OpportunityLineItem oppLineItem = new OpportunityLineItem(OpportunityId = opp.Id,
	    														PricebookEntryId = pricebookEntries[1].Id);
	    insert oppLineItem;

	    List<Event_Attendee__c> eventAttendees = new List<Event_Attendee__c>();

	    Event_Attendee__c eventAttendee = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[2].Id);
	    eventAttendee.Event_Attendees_Status__c = 'Registered';
	    eventAttendee.Session_Position__c = 'Co-Chair';
	    eventAttendees.add(eventAttendee);

	    insert eventAttendees;

	    Test.startTest();

	    Opportunity opp2 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	    opp2.Event__c = testEvents[3].Id;
	    opp2.Contact__c = contato.Id;
	    opp2.CloseDate = system.today();
	    opp2.StageName = 'Approved By Finance';
	    opp2.Approved_by_Finance_is_Today__c = Date.Today();
	    opp2.Club__c = clubs[0].Id;
	    opp2.Finance_Status__c = 'Outstanding';
	    insert opp2;

	    System.debug('#### opp2' + opp2);

	    OpportunityLineItem oppLineItem1 = new OpportunityLineItem(OpportunityId = opp2.Id,
	    														PricebookEntryId = pricebookEntries[1].Id);
	    insert oppLineItem1;

	    List<Event_Attendee__c> eventAttendees2 = new List<Event_Attendee__c>();

	    Event_Attendee__c eventAttendee1 = SObjectInstance.createEventAttendee(opp2.Id, conta.Id, contato.Id, testEvents[3].Id);
	    eventAttendee1.Event_Attendees_Status__c = 'Registered';
	    eventAttendee1.Session_Position__c = 'Co-Chair';
	    eventAttendees2.add(eventAttendee1);

	    insert eventAttendees2;
	    
	    EmailReport_ComparisonReportGenerator step1 = new EmailReport_ComparisonReportGenerator();
	    step1.execute(null);

	    Test.stopTest();
	    
		EmailReport_ComparisonReport cont = new EmailReport_ComparisonReport();
		List<EmailReport_ComparisonReportGen_Helper.EventComparison> futureEventComparison = cont.futureEventComparison;
		List<EmailReport_ComparisonReportGen_Helper.EventComparison> previousEventComparison = cont.previousEventComparison;
		
		
	}

	@isTest static void testEmailReport_ComparisonReport4() {
		createTestData();	   	
	   	
	    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	    opp.Event__c = testEvents[0].Id;
	    opp.Contact__c = contato.Id;
	    opp.CloseDate = system.today();
	    opp.StageName = 'Approved By Finance';
	    opp.Approved_by_Finance_is_Today__c = Date.Today().addDays(-365);
	    opp.Club__c = clubs[0].Id;
	    opp.Finance_Status__c = 'Outstanding';
	    opp.Pricebook2Id = testPricebooks[0].Id;
	    insert opp;

	    System.debug('#### opp' + opp);

	    List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();

	    oppLineItems.add(new OpportunityLineItem(OpportunityId = opp.Id,
	    										PricebookEntryId = pricebookEntries[1].Id));
	    

	    oppLineItems.add(new OpportunityLineItem(OpportunityId = opp.Id,
	    										PricebookEntryId = pricebookEntries[4].Id));
	    

	    oppLineItems.add(new OpportunityLineItem(OpportunityId = opp.Id,
	    										PricebookEntryId = pricebookEntries[7].Id));
	    insert oppLineItems;

	    List<Event_Attendee__c> eventAttendees = new List<Event_Attendee__c>();

	    Event_Attendee__c eventAttendee = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[0].Id);
	    eventAttendee.Event_Attendees_Status__c = 'Registered';
	    eventAttendee.Session_Position__c = 'Guest Free';
	    insert eventAttendee;

	    insert eventAttendees;

	    Test.startTest();

	    EmailReport_ComparisonReportGenerator step1 = new EmailReport_ComparisonReportGenerator();
	    step1.execute(null);

	    Test.stopTest();
	    
		EmailReport_ComparisonReport cont = new EmailReport_ComparisonReport();
		List<EmailReport_ComparisonReportGen_Helper.EventComparison> futureEventComparison = cont.futureEventComparison;
		List<EmailReport_ComparisonReportGen_Helper.EventComparison> previousEventComparison = cont.previousEventComparison;
		
		
	}

	@isTest static void testEmailReport_ComparisonReport5() {
		createTestData();	   	
	   	
	    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	    opp.Event__c = testEvents[1].Id;
	    opp.Contact__c = contato.Id;
	    opp.CloseDate = system.today();
	    opp.StageName = 'Approved By Finance';
	    opp.Approved_by_Finance_is_Today__c = Date.Today();
	    opp.Club__c = clubs[0].Id;
	    opp.Finance_Status__c = 'Outstanding';
	    insert opp;

	    System.debug('#### opp' + opp);

	    OpportunityLineItem oppLineItem = new OpportunityLineItem(OpportunityId = opp.Id,
	    														PricebookEntryId = pricebookEntries[1].Id);
	    insert oppLineItem;

	    Test.startTest();
	    Opportunity opp3 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    opp3.Event__c = testEvents[0].Id;
	    opp3.Contact__c = contato.Id;
	    opp3.CloseDate = system.today();
	    opp3.StageName = 'Approved By Finance';
	    opp3.Approved_by_Finance_is_Today__c = Date.Today().addDays(-365);
	    opp3.Club__c = clubs[0].Id;
	    opp3.Finance_Status__c = 'Outstanding';
	    insert opp3;

	    System.debug('#### opp3' + opp3);

	    OpportunityLineItem oppLineItem3 = new OpportunityLineItem(OpportunityId = opp3.Id,
	    														PricebookEntryId = pricebookEntries[2].Id);
	    insert oppLineItem3;

	    Event_Attendee__c eventAttendee5 = SObjectInstance.createEventAttendee(opp3.Id, conta.Id, contato.Id, testEvents[1].Id);
	    eventAttendee5.Event_Attendees_Status__c = 'Registered';
	    eventAttendee5.Session_Position__c = 'Guest Free';
	    insert eventAttendee5;

	    EmailReport_ComparisonReportGenerator step1 = new EmailReport_ComparisonReportGenerator();
	    step1.execute(null);

	    Test.stopTest();
	    
		EmailReport_ComparisonReport cont = new EmailReport_ComparisonReport();
		List<EmailReport_ComparisonReportGen_Helper.ClubComparison> club = cont.clubComparison;
		
	}

	//@isTest static void testEmailReport_ComparisonReport6()
	//{
	//	createTestData();	   	
	   	
	//    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	//    opp.Event__c = testEvents[1].Id;
	//    opp.Contact__c = contato.Id;
	//    opp.CloseDate = system.today();
	//    opp.StageName = 'Approved By Finance';
	//    opp.Approved_by_Finance_is_Today__c = Date.Today();
	//    opp.Club__c = clubs[0].Id;
	//    opp.Finance_Status__c = 'Outstanding';
	//    opp.Pricebook2Id = testPricebooks[0].Id;
	//    insert opp;


	//    Event_Attendee__c eventAttendee7 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvents[1].Id);
	//    eventAttendee7.Event_Attendees_Status__c = 'Registered';
	//    eventAttendee7.Session_Position__c = 'Co-Chair';
	//    insert eventAttendee7;

	//    Opportunity opp3 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	//    opp3.Event__c = testEvents[0].Id;
	//    opp3.Contact__c = contato.Id;
	//    opp3.CloseDate = system.today();
	//    opp3.StageName = 'Approved By Finance';
	//    opp3.Approved_by_Finance_is_Today__c = Date.Today().addDays(-365);
	//    opp3.Club__c = clubs[0].Id;
	//    opp3.Finance_Status__c = 'Outstanding';
	//    insert opp3;

	//    System.debug('#### opp3' + opp3);

	//    OpportunityLineItem oppLineItem3 = new OpportunityLineItem(OpportunityId = opp3.Id,
	//    														PricebookEntryId = pricebookEntries[2].Id);
	//    insert oppLineItem3;

	//    Event_Attendee__c eventAttendee8 = SObjectInstance.createEventAttendee(opp3.Id, conta.Id, contato.Id, testEvents[1].Id);
	//    eventAttendee8.Event_Attendees_Status__c = 'Registered';
	//    eventAttendee8.Session_Position__c = 'Guest Free';
	//    insert eventAttendee8;

	//    EmailReport_ComparisonReportGenerator step1 = new EmailReport_ComparisonReportGenerator();
	//    step1.execute(null);

	//}

	@isTest static void testEmailReport_ComparisonReportdsda4() {
		EmailReport_ComparisonReportGen_Helper.eye();
	}
		
}