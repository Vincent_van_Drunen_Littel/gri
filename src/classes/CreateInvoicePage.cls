public with sharing class CreateInvoicePage {
    public Invoice__c invoice  {get; set;}
    public String invId {get; set;}
    
    public CreateInvoicePage(ApexPages.StandardController stdContIn)
    {
        invId = ApexPages.currentPage().getParameters().get('ids');
    }
    
    public CreateInvoicePage(ApexPages.StandardSetController stdContIn) {

       System.debug('#### CreateInvoicePage  Started');
       
       System.debug('#### stdContIn: ' + stdContIn.getSelected());

        //stdContIn.setPageSize(stdContIn.getSelected().Size());
        Set<Id> invoiceIds = new Map<Id, Invoice__c>((List<Invoice__c>)stdContIn.getSelected()).keySet();
        
        System.debug('#### invoiceIds' + invoiceIds);

         invoice = [SELECT Id,ThanksMessage__c, VAT__c, Custom_French_Spain_Message__c, VAT_Country__c,Country_Name__c,Company_BadgeName__c, FirstName__c, LastName__c, Name, Company_Vat_Number__c, Billing_Street__c, Billing_City__c, Billing_PostCode__c, Billing_Country__c, VAT_Number__c, Invoice_Date__c, Customer_Order_Number__c, Order_Description__c, CurrencyIsoCode, Total_Net_Amount__c, Discount__c, VAT_Amount__c, Invoice_Total__c,Reunion_Fee_for_Swiss_Vat__c,Membership_fee_for_Swiss_vat__c,VAT_Amount_for_swiss_vat__c,Total_Invoice_amount_for_swiss__c,wired_payment_details__c,regemail__c
                    FROM Invoice__c 
                    WHERE Id IN :invoiceIds];

    }

}