@isTest
private class GDPR_Trigger_Handler_Test {

	private static Id REC_OPP_MEMBERSHIP = RecordTypeMemory.getRecType( 'Opportunity', 'Membership' );
	private static Id REC_OPP_MAGAZINE_AD = RecordTypeMemory.getRecType( 'Opportunity', 'Magazine' );
	private static Id REC_OPP_UNIQUE = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
	private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );

	private static Account conta;
	private static Contact contato;
	private static Event__c testEvent;
	private static Club__c club;
	private static Club__c club2;
	private static Magazine__c magazine;
	private static Magazine__c magazine2;
	private static Pricebook2 pb;
	private static Pricebook2 pb2;
	private static Product2 testProduct;
	private static PricebookEntry standardpbe;
	private static PricebookEntry pbe;
	private static PricebookEntry pbe2;

	@isTest static void test_Single_Record() {
		createTestData();
		Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
		opp.Event__c = testEvent.Id;
		opp.Contact__c = contato.Id;
		opp.CloseDate = system.today();
		opp.StageName = 'Approved By Finance';
		opp.Approved_by_Finance_is_Today__c = Date.Today();
		insert opp;

		Contact lContactAfter = [SELECT Id, Consent_Given_to_GDPR__c, Consent_Given_to_GDPR_by__c FROM Contact WHERE Id =: contato.Id];
		system.Assert(lContactAfter.Consent_Given_to_GDPR_by__c == 'Registration Form');
		system.Assert(lContactAfter.Consent_Given_to_GDPR__c == 'Yes');
	}


	private static void createTestData()
{
	Application_Config__c conf = new Application_Config__c();
	conf.Name = 'BucketUserId';
	conf.Value__c = '00536000004lQpq';
	insert conf;

	conta = SObjectInstance.createAccount2();
		Database.insert(conta);

		contato = SObjectInstance.createContato(conta.Id);
		contato.LastName = 'Teste contato';
		Database.insert(contato);

		club = SObjectInstance.createClub();
		club.Designated_Region__c = 'Brazil RE';
		Database.insert(club);

		club2 = SObjectInstance.createClub();
		club2.Designated_Region__c = 'Infra LatAm';
		Database.insert(club2);

		testEvent = SObjectInstance.createEvent();
		testEvent.Type__c = 'Open Event';
		testEvent.Club__c = club.Id;
		insert testEvent;

		testEvent.Start_Date__c = system.today().addDays(22);
		testEvent.End_Date__c = system.today().addDays(22);
		update testEvent ;

		magazine = SObjectInstance.createMagazine();
		magazine.CurrencyIsoCode = 'BRL';
		magazine.Club__c = club.Id;
		magazine.Designated_Region_2__c = 'Brazil RE';
		Database.insert(magazine);

		magazine2 = SObjectInstance.createMagazine();
		magazine2.CurrencyIsoCode = 'BRL';
		magazine2.Club__c = club2.Id;
		magazine2.Designated_Region_2__c = 'Infra LatAm';
		Database.insert(magazine2);

		List<Pricebook2> testPricebooks = new List<Pricebook2>();

		pb = SObjectInstance.catalogoDePreco();
		pb.Club__c = club.Id;
		pb.Magazine__c = magazine.Id;
		pb.Type__c = 'Magazine Subscriptions';
		pb.Event__c = testEvent.Id;
		pb.Starting_date__c = system.today();
		pb.End_date__c = system.today()+10;
		insert pb;
		testPricebooks.add(pb);

		pb2 = SObjectInstance.catalogoDePreco();
		pb2.Club__c = club.Id;
		pb2.Magazine__c = magazine.Id;
		pb2.Type__c = 'Spex';
		pb2.Event__c = testEvent.Id;
		pb2.Starting_date__c = system.today()+3;
		insert pb2;
		testPricebooks.add(pb2);

		System.debug('#### testPricebooks' + testPricebooks);

		testProduct = SObjectInstance.createProduct2();
		insert testProduct;

		standardpbe = SObjectInstance.entradaDePreco(test.getStandardPricebookId(), testProduct.Id);
		insert standardpbe;

		pbe = SObjectInstance.entradaDePreco(pb.Id, testProduct.Id);
		insert pbe;

		pbe2 = SObjectInstance.entradaDePreco(pb2.Id, testProduct.Id);
		insert pbe2;

		System.debug('#### test.getStandardPricebookId()' + test.getStandardPricebookId());
}
}