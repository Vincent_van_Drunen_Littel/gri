/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since    
 *
 */

/**
 * @author
 * @date 29/08/2018
 * @group
 * @group-content
 * @description Class to check if the Opportunity has one or more products
 */
public class OpportunityCheckProducts 
{
    private static final String SYSTEM_ADMIN ='System Administrator';
    private static final Id RECORDTYPE_OPEN_EVENTS = Schema.getGlobalDescribe().get('Opportunity').newSObject().getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('Open_Events').getRecordTypeId();
    private static final Id RECORDTYPE_SMARTUS_EVENTS = Schema.getGlobalDescribe().get('Opportunity').newSObject().getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('Smartus_Events').getRecordTypeId();
    //private static final Id RECORDTYPE_SMARTUS_EVENTS ;
    //private static final Id RECORDTYPE_OPEN_EVENTS;

    /**
     * @description Check if Opportunities from Open Events or Smartus Events:
     * (1) If the  Opportunity is on Stage Closing, Negotiation Approved or Won, 
     * check if if has have one or more products. 
     * (2) OpportunityUpdatePricebookEProduto is executing 
     * (3) The user performing the update is a System administrator
     * (4) Is a trigger test
     * If any of above is TRUE, just continue the update. Otherwish, trow an error
     * @param newOpps list of Opportunities
     * @return void
     * @example
     * OpportunityCheckProducts.execute(Trigger.new);
     */
    public static void execute(List<Opportunity> newOpps, Boolean enableTest) 
    {
        for(Opportunity singleOpp: newOpps)
        {
            System.debug('@@ singleOpp.CurrentUserProfile__c='+singleOpp.CurrentUserProfile__c);
            System.debug('@@ OpportunityUpdatePricebookEProduto.isExecuting='+OpportunityUpdatePricebookEProduto.isExecuting);

            if(OpportunityUpdatePricebookEProduto.isExecuting) continue;
            if( (singleOpp.RecordTypeId==RECORDTYPE_OPEN_EVENTS || singleOpp.RecordTypeId==RECORDTYPE_SMARTUS_EVENTS) && 
               singleOpp.Products__c==0 && 
               singleOpp.CurrentUserProfile__c!=SYSTEM_ADMIN && 
               (singleOpp.StageName=='Closing' || singleOpp.StageName=='Negotiation' || singleOpp.StageName=='Approved' || singleOpp.StageName=='Won'))
            {
                //if(Test.isRunningTest() && !enableTest) continue;
                singleOpp.addError('The opportunity need one or more Product(s)');
            }
        }
    }
}