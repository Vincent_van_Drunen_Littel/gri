/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe para vincular Pricebook com Oportunidade
*
* NAME: OpportunityVinculaPricebook.cls
* AUTHOR: RVR                                                  DATE: 08/04/2016
* 13/07/2018 : AndersonP : No the Class supports the Smartus Events and Spex
*                          Also it was simplified and optimized 
*******************************************************************************/
public with sharing class OpportunityVinculaPricebook 
{
    // Opportunity Record Type 
    private static Id REC_OPP_MEMBERSHIP = RecordTypeMemory.getRecType( 'Opportunity', 'Membership' );
    private static Id REC_OPP_MAGAZINE_AD = RecordTypeMemory.getRecType( 'Opportunity', 'Magazine' );
    private static Id REC_OPP_SPEX = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
    private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
    private static Id REC_OPP_SMARTUS_EVENTS = RecordTypeMemory.getRecType('Opportunity', 'Smartus_Events');
    private static Id REC_OPP_SMARTUS_SPEX = RecordTypeMemory.getRecType('Opportunity', 'Smartus_Spex');
    private static Id REC_PRICEBOOK_MAGAZINE = RecordTypeMemory.getRecType( 'Pricebook2', 'Magazine' );
    private static Id REC_PRICEBOOK_MEMBERSHIPS = RecordTypeMemory.getRecType( 'Pricebook2', 'Memberships' );
    private static Id REC_PRICEBOOK_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Pricebook2', 'Open_Events' ); // Events GRI and Smartus
    private static Id REC_PRICEBOOK_SPEX = RecordTypeMemory.getRecType( 'Pricebook2', 'Unique' ); // Spex GRI and Smartus
    
    public static Boolean exec = false;
    private static final String logtag = '%% OpportunityVinculaPricebook: ';
   
   /* old 
    // old trigger
    public static void execute() 
    {
        TriggerUtils.assertTrigger();
        if(exec) return;
        exec = true;
        
        List<Pricebook2> listPricebooks =  [SELECT Id, Name, RecordTypeId, Club__c, Event__c, Event__r.Name, Magazine__c, Type__c, Starting_date__c, End_date__c, Single_Event_Pricebook__c
                                            FROM Pricebook2 
                                            WHERE IsActive = TRUE OR Club__c != null OR Magazine__c != null OR Event__c != null];
        if(listPricebooks.isEmpty()) return;
*/
/*        
for(Opportunity opp: (List<Opportunity>)Trigger.new)
{
for(Pricebook2 thePricebook: listPricebooks)
{
if(thePricebook.Club__c == opp.Club__c && opp.RecordTypeId == REC_OPP_MEMBERSHIP) 
{
opp.PriceBook2Id = thePricebook.Id;
}
}
}
for(Opportunity opp: (List<Opportunity>)Trigger.new)
{
for(Pricebook2 thePricebook: listPricebooks)
{
if(thePricebook.Magazine__c == opp.Magazine__c && opp.RecordTypeId == REC_OPP_MAGAZINE_AD)
{
opp.PriceBook2Id = thePricebook.Id;
}
}
}
//for(Opportunity opp: (List<Opportunity>)Trigger.new)
//{
//	for(Pricebook2 thePricebook: listPricebooks)
//	{
//		if(opp.RecordTypeId == REC_OPP_MAGAZINE_SUBSCRIPTIONS && thePricebook.Type__c == 'Magazine Subscriptions')
//		{
//			opp.PriceBook2Id = thePricebook.Id;
//		}
//	}
//}
for(Opportunity opp: (List<Opportunity>)Trigger.new)
{
for(Pricebook2 thePricebook: listPricebooks)
{
if(opp.RecordTypeId == REC_OPP_SPEX && thePricebook.Type__c == 'Spex')
{
opp.PriceBook2Id = thePricebook.Id;
}
}
}
for(Opportunity opp: (List<Opportunity>)Trigger.new)
{
for(Pricebook2 thePricebook: listPricebooks)
{
if(thePricebook.Event__c == opp.Event__c && opp.RecordTypeId == REC_OPP_OPEN_EVENTS && opp.CloseDate >= thePricebook.Starting_date__c && opp.CloseDate <= thePricebook.End_date__c)
{
opp.PriceBook2Id = thePricebook.Id;
}
}
}
*/
/*
        for(Opportunity opp: (List<Opportunity>)Trigger.new)
        {
            for(Pricebook2 thePricebook: listPricebooks) 
            {
                if(thePricebook.RecordTypeId==NULL)
                {
                    System.debug('%% Pricebook '+thePricebook.Name+' Id='+thePricebook.Id+' does not have record type');
                    continue;
                }
                // Bind Merbership Pricebook
                else if ( thePricebook.RecordTypeId==REC_PRICEBOOK_MEMBERSHIPS && opp.RecordTypeId==REC_OPP_MEMBERSHIP &&
                         thePricebook.Club__c==opp.Club__c)
                {
                    opp.PriceBook2Id = thePricebook.Id;
                    break;
                }
                // Bind GRI and Smartus Events pricebook
                else if ( (opp.RecordTypeId==REC_OPP_OPEN_EVENTS || opp.RecordTypeId==REC_OPP_SMARTUS_EVENTS) &&
                         thePricebook.RecordTypeId==REC_PRICEBOOK_OPEN_EVENTS &&   
                         thePricebook.Event__c==opp.Event__c && 
                         opp.CloseDate>=thePricebook.Starting_date__c && opp.CloseDate<=thePricebook.End_date__c)
                {
                    opp.PriceBook2Id = thePricebook.Id;
                    break;
                }
                // Binds GRI and Smartus SPEX Pricebook
                else if((opp.RecordTypeId==REC_OPP_SPEX || opp.RecordTypeId==REC_OPP_SMARTUS_SPEX)&&
                        (thePricebook.RecordTypeId==REC_PRICEBOOK_SPEX))
                {
                    if(thePricebook.Single_Event_Pricebook__c && thePricebook.Event__c==opp.Event__c)
                    {
                        opp.PriceBook2Id = thePricebook.Id;
                        break;
                    }
                    else if(!thePricebook.Single_Event_Pricebook__c && thePricebook.Club__c==opp.Club__c)
                    {
                        opp.PriceBook2Id = thePricebook.Id;
                        break;
                    }
                }
                // Bind Magazine Pricebook
                else if (thePricebook.RecordTypeId==REC_PRICEBOOK_MAGAZINE && opp.RecordTypeId==REC_OPP_MAGAZINE_AD &&
                         thePricebook.Magazine__c==opp.Magazine__c)
                {
                    opp.PriceBook2Id = thePricebook.Id;
                    break;
                }
            }//endfor
            if(opp.PriceBook2Id==NULL)
            {
                String errMsg = '\nNo Pricebook was found for Opportunity '+opp.Name+'\nPlease check:';
                if(opp.RecordTypeId==REC_OPP_MEMBERSHIP)
                {
                    errMsg+='* Club is assigned to the Opportunity;\n';
                    errMsg+='* If there is a Pricebook with the same Club.\n';
                }
                else if(opp.RecordTypeId==REC_OPP_MAGAZINE_AD)
                {
                    errMsg+='* If the Magazine has a Pricebook.\n';
                }
                else if(opp.RecordTypeId==REC_OPP_SPEX)
                {
                    errMsg+='* If the Pricebook is assigned to a Club;\n';
                    errMsg+='* If the pricebook is exclusive for this Event, the Option `Event Only` is checked.\n';
                }
                else if(opp.RecordTypeId==REC_OPP_OPEN_EVENTS)
                {
                    errMsg+='* If the Event has a Pricebook;\n';
                    errMsg+='* If the Opportunity is inside the Pricebook Lifespan.\n';
                }
                else if(opp.RecordTypeId==REC_OPP_SMARTUS_EVENTS)
                {
                    errMsg+='* If the Event has a Pricebook;\n';
                    errMsg+='* If the Opportunity is inside the Pricebook Lifespan.\n';
                }
                else if(opp.RecordTypeId==REC_OPP_SMARTUS_SPEX)
                {
                    errMsg+='* If the Pricebook is assigned to a Club;\n';
                    errMsg+='* If the pricebook is exclusive for this Event, the Option `Event Only` is checked.\n';
                }
                System.debug(errMsg);
                //opp.addError(errMsg);
            }
        }//endfor
    }
*/   
    public static void execute2() 
    {
        TriggerUtils.assertTrigger();
        if(exec) return;
        exec = true;

        List<Opportunity> listOpps = Trigger.new;
        Set<String> setOppEventIds = HelperSObject.setFromFieldSafe(listOpps, 'Event__c');
        Map<Id, Event__c> mapOppEvents = new Map<Id, Event__c>(
            [SELECT Id, Name, Club__c FROM Event__c WHERE Id IN :setOppEventIds]);
        List<Pricebook2> listPricebooks =  
            [SELECT Id, Name, RecordTypeId, Club__c, Event__c, Event__r.Name, Magazine__c, Type__c, Starting_date__c, End_date__c, Single_Event_Pricebook__c
            FROM Pricebook2 
            WHERE IsActive=TRUE AND (Club__c!=NULL OR Magazine__c!=NULL OR Event__c!=NULL)];
        System.debug(logtag+'setOppEventIds='+setOppEventIds);
        System.debug(logtag+'mapOppEvents='+mapOppEvents);
        System.debug(logtag+'listOpps='+listOpps);

        Map<String, Id> map_OppId_EventClubId = new Map<String, Id>();
        for(Id evId:setOppEventIds)
        {
            Id clubId = mapOppEvents.get(evId).Club__c;
            System.debug(logtag+'clubId='+clubId);
            if(clubId==NULL) continue;
            for(Opportunity opp:listOpps)
            {
                if(opp.Event__c==evId)
                {
                    map_OppId_EventClubId.put(newOppHash(opp), clubId);
                    break;
                }
            }
        }
        System.debug(logtag+'map_OppId_EventClubId='+map_OppId_EventClubId);
        System.debug(logtag+'setOppEventIds='+setOppEventIds);

        if(listPricebooks.isEmpty()) return;
        for(Opportunity opp: (List<Opportunity>)Trigger.new)
        {
            for(Pricebook2 thePricebook: listPricebooks) 
            {
                if(thePricebook.RecordTypeId==NULL)
                {
                    //System.debug('%% Pricebook '+thePricebook.Name+' Id='+thePricebook.Id+' does not have record type');
                    continue;
                }
                else if(isSamePricebookOpenEvents(thePricebook, opp) ||
                        isSamePricebookSpex(thePricebook, opp, map_OppId_EventClubId.get(newOppHash(opp))) ||
                        isSamePricebookMagazine(thePricebook, opp) ||
                        isSamePricebookSmartusEvent(thePricebook, opp) ||
                        isSamePricebookSmartusSpex(thePricebook, opp, map_OppId_EventClubId.get(newOppHash(opp))) ||
                        isSamePricebookMembership(thePricebook, opp))
                {
                    opp.PriceBook2Id = thePricebook.Id;
                    if(!thePricebook.Single_Event_Pricebook__c && 
                        (opp.RecordTypeId==REC_OPP_SPEX || opp.RecordTypeId==REC_OPP_SMARTUS_SPEX))
                        continue;
                    break;
                }
            }
            if(opp.PriceBook2Id==NULL)
                oppVinculaPricebookErrMsg(opp);
        }
    }
    
    private static Boolean isSamePricebookOpenEvents(Pricebook2 thePricebook, Opportunity opp)
    {
        //System.debug('%%%% ~~'+'opp.Name='+opp.Name + ', opp.RecordTypeId='+opp.RecordTypeId+', REC_OPP_OPEN_EVENTS='+REC_OPP_OPEN_EVENTS);
        //System.assertEquals(True, opp.RecordTypeId==REC_OPP_OPEN_EVENTS ,'######');
        //System.assertEquals(True, thePricebook.RecordTypeId==REC_PRICEBOOK_OPEN_EVENTS ,'=========');
        //System.assertEquals(True, thePricebook.Event__c==opp.Event__c ,'$$$$$$$$$$');
        //System.assertEquals(True, opp.CloseDate>=thePricebook.Starting_date__c && opp.CloseDate<=thePricebook.End_date__c ,
        //    '%%%% opp.CloseDate='+opp.CloseDate+'thePricebook.Starting_date__c='+thePricebook.Starting_date__c+'thePricebook.End_date__c='+thePricebook.End_date__c);
        //System.assertEquals(True, opp.RecordTypeId==REC_OPP_OPEN_EVENTS ,'@@@@@@@@@@@@@@');
        if(thePricebook.Event__c==NULL) return False;
        return opp.RecordTypeId==REC_OPP_OPEN_EVENTS &&
            thePricebook.RecordTypeId==REC_PRICEBOOK_OPEN_EVENTS &&   
            thePricebook.Event__c==opp.Event__c && 
            opp.CloseDate>=thePricebook.Starting_date__c && opp.CloseDate<=thePricebook.End_date__c;
    }
    /*
    private static Boolean isSamePricebookSpex(Pricebook2 thePricebook, Opportunity opp)
    {
        Boolean isSpex = opp.RecordTypeId==REC_OPP_SPEX &&
            thePricebook.RecordTypeId==REC_PRICEBOOK_SPEX;

        if(thePricebook.Name=='Spex Europe RE')
        {
            System.debug('%% !!!!!!!!! Pb='+thePricebook);
            System.debug('%% !!!!!!!!! Opp='+opp);
            System.debug(logtag+'opp.Event__r.Club__c='+opp.Event__r.Club__c);
            System.debug(logtag+'thePricebook.Club__c='+thePricebook.Club__c);
            System.debug(logtag+'opp.Event__c='+opp.Event__c);
        }

        System.debug(logtag+'opp.RecordTypeId==REC_OPP_SPEX && thePricebook.RecordTypeId==REC_PRICEBOOK_SPEX='+isSpex);            
        if(isSpex && 
           ( (thePricebook.Single_Event_Pricebook__c && thePricebook.Event__c==opp.Event__c && thePricebook.Event__c!=NULL) || 
            (!thePricebook.Single_Event_Pricebook__c && thePricebook.Club__c==opp.Club__c && thePricebook.Club__c!=NULL) ||
            (!thePricebook.Single_Event_Pricebook__c && thePricebook.Club__c==opp.Event__r.Club__c && thePricebook.Club__c!=NULL) ))
        {
            System.debug('%% thePricebook.Id='+thePricebook.Id);
            System.debug('%% >>>>>>>>> return True');
            return True;
        }
        else
        {
            return False;
        }
    }
    */
    private static Boolean isSamePricebookSpex(Pricebook2 thePricebook, Opportunity opp, Id eventClubId)
    {
        Boolean isSpex = opp.RecordTypeId==REC_OPP_SPEX &&
            thePricebook.RecordTypeId==REC_PRICEBOOK_SPEX;
        if(isSpex && 
           ( (thePricebook.Single_Event_Pricebook__c && thePricebook.Event__c==opp.Event__c && thePricebook.Event__c!=NULL) || 
            (!thePricebook.Single_Event_Pricebook__c && thePricebook.Club__c==opp.Club__c && thePricebook.Club__c!=NULL) ||
             !thePricebook.Single_Event_Pricebook__c && thePricebook.Club__c==eventClubId && thePricebook.Club__c!=NULL))
        {
            return True;
        }
        else 
        {
            return False;
        }
    }    
    private static Boolean isSamePricebookMagazine(Pricebook2 thePricebook, Opportunity opp)
    {
        if(thePricebook.Magazine__c==NULL) return False;
        return thePricebook.RecordTypeId==REC_PRICEBOOK_MAGAZINE && 
            opp.RecordTypeId==REC_OPP_MAGAZINE_AD &&
            thePricebook.Magazine__c==opp.Magazine__c;
    }
    private static Boolean isSamePricebookSmartusEvent(Pricebook2 thePricebook, Opportunity opp)
    {
        if(thePricebook.Event__c==NULL) return False;
        return opp.RecordTypeId==REC_OPP_SMARTUS_EVENTS &&
            thePricebook.RecordTypeId==REC_PRICEBOOK_OPEN_EVENTS &&   
            thePricebook.Event__c==opp.Event__c && 
            opp.CloseDate>=thePricebook.Starting_date__c && opp.CloseDate<=thePricebook.End_date__c;
    }
    private static Boolean isSamePricebookSmartusSpex(Pricebook2 thePricebook, Opportunity opp, Id eventClubId)
    {
        Boolean isSpexSmartus = opp.RecordTypeId==REC_OPP_SMARTUS_SPEX &&
            thePricebook.RecordTypeId==REC_PRICEBOOK_SPEX;
        if(isSpexSmartus && 
           ( (thePricebook.Single_Event_Pricebook__c && thePricebook.Event__c==opp.Event__c && thePricebook.Event__c!=NULL) || 
            (!thePricebook.Single_Event_Pricebook__c && thePricebook.Club__c==opp.Club__c && thePricebook.Club__c!=NULL) ||
             !thePricebook.Single_Event_Pricebook__c && thePricebook.Club__c==eventClubId && thePricebook.Club__c!=NULL))
        {
            return True;
        }
        else 
        {
            return False;
        }        
    }
    private static Boolean isSamePricebookMembership(Pricebook2 thePricebook, Opportunity opp)
    {
        return thePricebook.Club__c==opp.Club__c && 
            thePricebook.RecordTypeId==REC_PRICEBOOK_MEMBERSHIPS &&
            opp.RecordTypeId==REC_OPP_MEMBERSHIP;
    }
    private static void oppVinculaPricebookErrMsg(Opportunity opp)
    {
        String errMsg = '\nNo Pricebook was found for Opportunity '+opp.Name+'\nPlease check:';
        if(opp.RecordTypeId==REC_OPP_MEMBERSHIP)
        {
            errMsg+='(1) Club is assigned to the Opportunity;\n';
            errMsg+='(2) If there is a Pricebook with the same Club.\n';
        }
        else if(opp.RecordTypeId==REC_OPP_MAGAZINE_AD)
        {
            errMsg+='(1) If the Magazine has a Pricebook.\n';
        }
        else if(opp.RecordTypeId==REC_OPP_SPEX)
        {
            errMsg+='(1) If the Opportunity was assigned to a Club;\n';
            errMsg+='(2) If there is a Pricebook is assigned for the same Club;\n';
            errMsg+='(3) If the pricebook is exclusive for this Event, the Option `Event Only` is checked.\n';
        }
        else if(opp.RecordTypeId==REC_OPP_OPEN_EVENTS)
        {
            errMsg+='(1) If the Event has a Pricebook;\n';
            errMsg+='(2) If the Opportunity is inside the Pricebook Lifespan.\n';
        }
        else if(opp.RecordTypeId==REC_OPP_SMARTUS_EVENTS)
        {
            errMsg+='(1) If the Event has a Pricebook;\n';
            errMsg+='(2) If the Opportunity is inside the Pricebook Lifespan.\n';
        }
        else if(opp.RecordTypeId==REC_OPP_SMARTUS_SPEX)
        {
            errMsg+='(1) If the Opportunity was assigned to a Club;\n';
            errMsg+='(2) If there is a Pricebook assigned for the same Club;\n';
            errMsg+='(3) If the pricebook is exclusive for a Event, the Option `Event Only` is checked.\n';
        }
        errMsg+=' Error@OpportunityVinculaPricebook:execute2';
        System.debug(errMsg);
        //opp.addError(errMsg);    
    }

    // hash to emulate a Opp Id
    private static String newOppHash(Opportunity opp)
    {
        return(String.valueOf(opp.RecordTypeId)+String.valueOf(opp.Contact__c)+opp.Name+String.valueOf(opp.Event__c)+String.valueOf(opp.Club__c));
    }

}