/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since    
 *
 */

/**
 * @author
 * @date 29/08/2018
 * @group
 * @group-content
 * @description Test Class for OpportunityCheckProducts
 */
 @IsTest
public class OpportunityCheckProducts_Test 
{
    private static final String LOGTAG = '@@ OpportunityCheckProducts_Test: ';
    private static final String BI_USER_NAME = 'Bi User';
    private static final Integer N_CONTACTS = 2;

/***
        String jobRole = 'Business Analyst';
        Integer numberOfUsers = 50;

        Test.startTest();
        List<User> listCreatedUsers = TestDataFactory.createUsers(jobRole, numberOfUsers, jobRole);
        //List<User> listUsers = [SELECT Id, Name, Title FROM User WHERE Id:=listCreatedUsers];
        System.assertEquals(numberOfUsers, listCreatedUsers.size(), 
            'Number of users created is incorrect!');
        Test.stopTest();
***/


    @isTest
    private static void testCheckProducts_Method()
    {
        System.debug(LOGTAG+'Class cover tes:t testCheckProducts_Method()');
        User telesalesUser = [SELECT Id, Name, ProfileId, Profile_1__c FROM User WHERE IsActive=TRUE AND Profile_1__c='TeleSales' LIMIT 1];
        System.assertNotEquals(NULL, telesalesUser, LOGTAG+'Error, the query on testCheckProducts_Method() do not returned any contact');
        Test.startTest();
        System.RunAs(telesalesUser)
        {
            List<Opportunity> listOpps = [SELECT Id, Name, StageName, RecordTypeId, CurrentUserProfile__c, Products__c FROM Opportunity];
            System.debug(LOGTAG+' telesalesUser='+telesalesUser);
            for(Opportunity singleOpp:listOpps)
            {
                singleOpp.StageName = 'Closing';
            }
            System.debug(LOGTAG+'testCheckProducts_Method(): listOpps='+HelperSObject.json(listOpps, 'Id, StageName, RecordTypeId, Products__c, CurrentUserProfile__c, Products__c'));
            OpportunityCheckProducts.execute(listOpps, TRUE);
        }
        Test.stopTest();
    }

    @isTest
    private static void testCheckProductst_Trigger()
    {
        System.debug(LOGTAG+'Trigger cover test: testCheckProductst_Trigger()');
        List<Opportunity> listOpps = [SELECT Id, Name, StageName, RecordTypeId, CurrentUserProfile__c, Products__c FROM Opportunity];
        Test.startTest();
        for(Opportunity singleOpp:listOpps)
        {
            singleOpp.StageName = 'Closing';
        }
        update listOpps;  
        System.debug(LOGTAG+ 'testCheckProductst_Trigger(): listOpps='+HelperSObject.json(listOpps, 'Id, StageName, RecordTypeId, Products__c, CurrentUserProfile__c, Products__c'));
        Test.stopTest();         

    }

    @testSetup 
    private static void buildData()
    {
        TestDataFactory.init();

        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(N_CONTACTS);
        List<Club__c> listClubs = TestDataFactory.createClubs(TestDataFactory.listGriClubs());
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent',  listClubs[0]);
        List<Opportunity> listOpps = TestDataFactory.createEventOpps(listContacts, theGreatEvent, listClubs[0], System.today()+10);
        //TestDataFactory.addProductToOpportunity(TestDataFactory.createProduct('P-00022'), listOpps);
    }

}