/**
* Generate the enviroment for the Unit Tests
* @author Sebastian Muñoz - Force.com Labs
* @createddate 06/08/2010
*/
public with sharing class InlineContactHerachy_TestUtilities{

 	public List<Contact>	 	testConList  { get; set; }	
 	
  	/**
  	* Check over all object field if the loged User has right over Object
  	* @parms sObject , CRUD check ( isCreateable, isDeleteable, isUpdateable ) 
  	* @return Boolean
  	*/
  	public Boolean checkObjectCrud( String objName, Set<String> fieldsToCheck, String crudType ){  		
  		
  		// Get the global describe
        Schema.DescribeSObjectResult objectResult = Schema.getGlobalDescribe().get( objName ).getDescribe();

  		for( String fieldToCheck : objectResult.fields.getMap().keySet() ){	
  			
  			Schema.DescribeFieldResult current_field = objectResult.fields.getMap().get( fieldToCheck ).getDescribe();
  			
  			if( fieldsToCheck.contains( current_field.getName() ) ) {
  			
	  			if( 'create'.equalsIgnoreCase( crudType ) && !current_field.isCreateable()){
		  			return false;
		  		}
		  		else if( 'update'.equalsIgnoreCase( crudType ) && !current_field.isUpdateable() ){
		  			return false;
		  		}
  			}
  		}
  		
  		return true;
  	}
  	
  	/**
  	* Create Contacts's
  	* @params contactToCreate ( the total amount of contact )
  	*/
  	public void createContacts( Integer contactToCreate, Set<String> fieldsToCheck ){

  		List<Contact> auxList = new List<Contact>();
  		
  		for( Integer i = 1; i <= contactToCreate; i++ ){
  			Contact cttAux 				= new Contact();
  			cttAux.LastName 				= this.createRandomWord();
  			cttAux.ReportsToId				= null;
			auxList.add( cttAux );
  		}
  		
  		if ( this.checkObjectCrud('Contact', fieldsToCheck, 'create') ){
  			try{
  				insert auxList;
  			}
  			catch( Exception ex ){
 				System.assert( false ,'Pre deploy test failed, This may be because of custom validation rules in your Org. You can check ignore apex errors or temporarily deactivate your validation rules for Contacts and try again.');
 			}
 			this.testConList = new List<Contact>();
 			this.testConList.addAll( auxList );
  		}
  		/*else{
  			System.Assert(false , 'You need right over Contact Object');
  		}*/
  	}
  	
  	/**
    * Method for Update a Contact
    * @param fieldsToCheck
    */
  	public void updateContactList( Set<String> fieldsToCheck ){ 
  		
  		if ( this.checkObjectCrud('Contact', fieldsToCheck, 'create') && !this.testConList.isEmpty() ){
  			try{
  				update this.testConList;
  			}
  			catch( Exception ex ){
 				System.assert( false ,'Pre deploy test failed, This may be because of custom validation rules in your Org. You can check ignore apex errors or temporarily deactivate your validation rules for Contacts and try again.');
 			}
  		}
  		else{
  			System.Assert(false , 'You need right over Contact Object');
  		}
  	}
  	
  	/**
    * Random words are required for testing 
    * as you will likely run into any word I can insert
    * during a test class and produce false test results.
    */
    public String createRandomWord(){
      String ret = 'word' + math.rint( math.random() * 100000 );
      
      return ret;
    }
}