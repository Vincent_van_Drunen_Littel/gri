global class GDPR_Forget_Account_Schedulable implements Schedulable {
    global void execute(SchedulableContext sc) {
        
        List<Contact> contacts = new List<Contact>();
        GDPR_Forget_Accounts forgetAccounts = new GDPR_Forget_Accounts();
        database.executebatch(forgetAccounts, 1);
    }
}