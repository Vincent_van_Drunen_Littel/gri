@isTest
private class quoteTriggerTest
{
    /*(seealldata=true)*/
    static Opportunity oportunidade;
    static Opportunity spexOportunidade;
    static Opportunity spexSmartUsOportunidade;
    static RecordType recType;
    static Contact con;
    static Account acc;
    static Id recTypeID;
    static Id spexRecTypeID;
    static Id spexSmartUsRecTypeID;
    static Id conRecTypeID;
    static id accTypeId;
    static Event__c evento;
    static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
    static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
    static Contact contato;
    static Quote newQuote;
    static Quote newSpexQuote;
    static Quote newSpexSmartUsQuote;
    static Id qId; 
    static list<Club__c> club;  
    static id clubId;   
    
    private static void getSettings()
    {
        Application_Config__c conf = new Application_Config__c();
        conf.Name = 'BucketUserId';
        conf.Value__c = '00536000004lQpq';
        insert conf;

        recType =[Select SobjectType, NamespacePrefix, Name, Id From RecordType  where SobjectType='Opportunity' and Name='Membership'];
        recTypeID =recType.id;

        recType =[Select SobjectType, NamespacePrefix, Name, Id From RecordType  where SobjectType='Opportunity' and Name='SPEX'];
        spexRecTypeID =recType.id;

        recType =[Select SobjectType, NamespacePrefix, Name, Id From RecordType  where SobjectType='Opportunity' and Name='Smartus - Spex'];
        spexSmartUsRecTypeID =recType.id;
        
        recType =[Select SobjectType, NamespacePrefix, Name, Id From RecordType where SobjectType='Contact' and Name='Customer Contact'];
        conRecTypeID=recType.id;
        
        recType =[Select SobjectType, NamespacePrefix, Name, Id From RecordType where SobjectType='Account' and Name='Customer'];
        accTypeId =recType.id;
        
        recType =[Select SobjectType, NamespacePrefix, Name, Id From RecordType  where SobjectType='Quote' and Name='Quote'];
        qId=recType.id;

        List<ProcessDefinition> approvals = [SELECT Id, Name, DeveloperName FROM ProcessDefinition];
        System.debug('#### approvals: ' + approvals);

        Map<String, ProcessDefinition> approvalsByDeveloperName = new Map<String, ProcessDefinition>();
        
        for(ProcessDefinition pd : approvals)
        {
            System.debug('#### pd: ' + pd);
            approvalsByDeveloperName.put(pd.DeveloperName, pd);
        }

        List<quoteTriggerSettings__c> settings = new List<quoteTriggerSettings__c>();
        settings.add(new quoteTriggerSettings__c(Name = 'Infra Club India', ClubName__c = 'Infra Club India', ApprovalProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_Approval_Process_InfrClub_India').Id).substring(0, 15), ApprovalProcessName__c = 'Proposal_Approval_Process_InfrClub_India'));
        settings.add(new quoteTriggerSettings__c(Name = 'Infra Club LatAm', ClubName__c = 'Infra Club LatAm', ApprovalProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_Approval_Process').Id).substring(0, 15), ApprovalProcessName__c = 'Proposal_Approval_Process'));
        settings.add(new quoteTriggerSettings__c(Name = 'RE Club Brazil', ClubName__c = 'RE Club Brazil', ApprovalProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_Approval_Process_RE_Club_Brazil').Id).substring(0, 15), ApprovalProcessName__c = 'Proposal_Approval_Process_RE_Club_Brazil'));
        settings.add(new quoteTriggerSettings__c(Name = 'RE Club Europe', ClubName__c = 'RE Club Europe', ApprovalProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_Approval_Process_RE_Club_Europe').Id).substring(0, 15), ApprovalProcessName__c = 'Proposal_Approval_Process_RE_Club_Europe'));
        settings.add(new quoteTriggerSettings__c(Name = 'RE Club India', ClubName__c = 'RE Club India', ApprovalProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_Approval_Process_RE_Club_India').Id).substring(0, 15), ApprovalProcessName__c = 'Proposal_Approval_Process_RE_Club_India'));
        settings.add(new quoteTriggerSettings__c(Name = 'RE Club LatAm', ClubName__c = 'RE Club LatAm', ApprovalProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_Approval_Process_RE_Club_LatAM').Id).substring(0, 15), ApprovalProcessName__c = 'Proposal_Approval_Process_RE_Club_LatAM'));
        insert settings;
    
        System.debug('#### settings: ' + settings);

        List<quoteTriggerSettingsSPEX__c> spexsettings = new List<quoteTriggerSettingsSPEX__c>();
        spexsettings.add(new quoteTriggerSettingsSPEX__c(Name = 'Infra Club India', ProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_SPEX_Infra_India').Id).substring(0, 15), ProcessName__c = 'Proposal_SPEX_Infra_India'));
        spexsettings.add(new quoteTriggerSettingsSPEX__c(Name = 'Infra Club LatAm', ProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_SPEX_Infra_LatAm').Id).substring(0, 15), ProcessName__c = 'Proposal_SPEX_Infra_LatAm'));
        spexsettings.add(new quoteTriggerSettingsSPEX__c(Name = 'RE Club India', ProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_SPEX_RE_India').Id).substring(0, 15), ProcessName__c = 'Proposal_SPEX_RE_India'));
        spexsettings.add(new quoteTriggerSettingsSPEX__c(Name = 'RE Club Brazil', ProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_SPEX_BR_RE').Id).substring(0, 15), ProcessName__c = 'Proposal_SPEX_BR_RE'));
        spexsettings.add(new quoteTriggerSettingsSPEX__c(Name = 'RE Club LatAm', ProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_SPEX_LatAm_RE').Id).substring(0, 15), ProcessName__c = 'Proposal_SPEX_LatAm_RE'));
        spexsettings.add(new quoteTriggerSettingsSPEX__c(Name = 'RE Club Europe', ProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_SPEX_EU_RE').Id).substring(0, 15), ProcessName__c = 'Proposal_SPEX_EU_RE'));
        spexsettings.add(new quoteTriggerSettingsSPEX__c(Name = 'Smartus - Real Estate', ProcessID__c = String.valueOf(approvalsByDeveloperName.get('Proposal_SPEX_Smartus').Id).substring(0, 15), ProcessName__c = 'Proposal_SPEX_Smartus'));
        insert spexsettings;
    }

    private static void addClubs()
    {
        club = new List<Club__c>();

        club.add(SObjectInstance.createClub());
        club.add(SObjectInstance.createClub());
        club.add(SObjectInstance.createClub());
        club.add(SObjectInstance.createClub());
        club.add(SObjectInstance.createClub());
        club.add(SObjectInstance.createClub());
        club.add(SObjectInstance.createClub());
        club.add(SObjectInstance.createClub());

        club[0].Name = 'Infra Club India';
        club[1].Name = 'Infra Club LatAm';
        club[2].Name = 'RE Club Brazil';
        club[3].Name = 'RE Club Europe';
        club[4].Name = 'RE Club India';
        club[5].Name = 'RE Club LatAm';
        club[6].Name = 'Smartus - Real Estate';

        club[0].Designated_Region__c = 'India Infra';
        club[1].Designated_Region__c = 'Infra LatAm';
        club[2].Designated_Region__c = 'Brazil RE';
        club[3].Designated_Region__c = 'Europe RE';
        club[4].Designated_Region__c = 'India RE';
        club[5].Designated_Region__c = 'LatAm RE';

        Database.insert(club);
    }
    
    private static void addAccount()
    {
        acc = new   Account();
        acc.RecordTypeId = accTypeId;
        acc.Name ='Test Account';
         
        insert acc;
        string accID=acc.id; 
        acc = [Select ID from Account where ID=:accID];      
    }
    
    private static void addContact()
    {
        contato = SObjectInstance.createContato(acc.Id);
        Database.insert(contato);
    }   
        
    private static void addOpportunity(string ClubName)
    {
        evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);
    
        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(22);
        update(evento);
    
        oportunidade = SObjectInstance.createOpportunidade(acc.Id, ID_EVENT);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Approved by Finance';
        oportunidade.Event__c = evento.Id;
        oportunidade.Event_Held__c = true;
        oportunidade.Finance_Status__c = 'Paid';
        oportunidade.Payment_Method__c = 'International Transfer';
        oportunidade.GRI_Branch__c = 'GRI Brazil';
        
        oportunidade.Product_Exceptions__c  ='test';
        oportunidade.General_Notes__c='test';
        oportunidade.Payment_Condition_s__c='test';
        oportunidade.RecordTypeId = recTypeID;
        
        system.debug('##### ClubName OP:' +ClubName);       
         
        oportunidade.Club__c=Id.valueOf(ClubName);
        
        system.debug('##### ClubName OP2:' +oportunidade.Club__c);      
    }

    private static void addSpexOpportunity(string ClubName)
    {
        evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);
    
        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(22);
        update(evento);
    
        spexOportunidade = SObjectInstance.createOpportunidade(acc.Id, ID_EVENT);
        spexOportunidade.Contact__c = contato.Id;
        spexOportunidade.StageName = 'Approved by Finance';
        spexOportunidade.Event__c = evento.Id;
        spexOportunidade.Event_Held__c = true;
        spexOportunidade.Finance_Status__c = 'Paid';
        spexOportunidade.Payment_Method__c = 'International Transfer';
        spexOportunidade.GRI_Branch__c = 'GRI Brazil';
        
        spexOportunidade.Product_Exceptions__c  ='test';
        spexOportunidade.General_Notes__c='test';
        spexOportunidade.Payment_Condition_s__c='test';
        spexOportunidade.Spex_Type__c = 'Club Sponsorship';
        spexOportunidade.RecordTypeId = spexRecTypeID;
        
        system.debug('##### ClubName OP:' +ClubName);       
         
        spexOportunidade.Club__c=Id.valueOf(ClubName);
        
        system.debug('##### ClubName OP2:' +spexOportunidade.Club__c);      
    }

    private static void addSmartUsSpexOpportunity(string ClubName)
    {
        evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);
    
        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(22);
        update(evento);
    
        spexSmartUsOportunidade = SObjectInstance.createOpportunidade(acc.Id, ID_EVENT);
        spexSmartUsOportunidade.Contact__c = contato.Id;
        spexSmartUsOportunidade.StageName = 'Approved by Finance';
        spexSmartUsOportunidade.Event__c = evento.Id;
        spexSmartUsOportunidade.Event_Held__c = true;
        spexSmartUsOportunidade.Finance_Status__c = 'Paid';
        spexSmartUsOportunidade.Payment_Method__c = 'International Transfer';
        spexSmartUsOportunidade.GRI_Branch__c = 'GRI Brazil';
        
        spexSmartUsOportunidade.Product_Exceptions__c  ='test';
        spexSmartUsOportunidade.General_Notes__c='test';
        spexSmartUsOportunidade.Payment_Condition_s__c='test';
        spexSmartUsOportunidade.Spex_Type__c = 'Club Sponsorship';
        spexSmartUsOportunidade.RecordTypeId = spexSmartUsRecTypeID;
        
        system.debug('##### ClubName OP:' +ClubName);       
         
        spexSmartUsOportunidade.Club__c=Id.valueOf(ClubName);
        
        system.debug('##### ClubName OP2:' +spexSmartUsOportunidade.Club__c);      
    }    
    
    private static void addQuote()
    {
        newQuote = new Quote();
        newQuote.RecordTypeId = qId;
        newQuote.Name = 'Test';
        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test';
        newQuote.OpportunityId = oportunidade.id;
        newQuote.Proposal_Valid_Until__c = Date.Today() + 30;
    }

    private static void addSpexQuote()
    {
        newSpexQuote = new Quote();
        newSpexQuote.RecordTypeId = qId;
        newSpexQuote.Name = 'Test';
        newSpexQuote.approval_checkbox__c = true;
        newSpexQuote.Product_Exception__c = 'test';
        newSpexQuote.OpportunityId = spexOportunidade.id;
        newSpexQuote.Proposal_Valid_Until__c = Date.Today() + 30;
    }

    private static void addSpexSmartUsQuote()
    {
        newSpexSmartUsQuote = new Quote();
        newSpexSmartUsQuote.RecordTypeId = qId;
        newSpexSmartUsQuote.Name = 'Test';
        newSpexSmartUsQuote.approval_checkbox__c = true;
        newSpexSmartUsQuote.Product_Exception__c = 'test';
        newSpexSmartUsQuote.OpportunityId = spexSmartUsOportunidade.id;
        newSpexSmartUsQuote.Proposal_Valid_Until__c = Date.Today() + 30;
    }

    static testMethod void testSmartUs()
    {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida = club[6].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addSmartUsSpexOpportunity(ida);
        insert spexSmartUsOportunidade;

        addSpexSmartUsQuote();
        insert newSpexSmartUsQuote;

        addSmartUsSpexOpportunity(ida);
        spexSmartUsOportunidade.Club__c = null;
        spexSmartUsOportunidade.Event__c = null;
        insert spexSmartUsOportunidade;

        addSpexSmartUsQuote();
        insert newSpexSmartUsQuote;

        spexSmartUsOportunidade.Club__c = club[6].id;
        spexSmartUsOportunidade.Event__c = evento.Id;
        update spexSmartUsOportunidade;

        newSpexSmartUsQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexSmartUsQuote.Product_Exception__c = 'test';
        update newSpexSmartUsQuote;
    }
    
    static testMethod void TestA()
    {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida = club[0].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        Database.insert(oportunidade);

        addSpexOpportunity(ida);
        insert spexOportunidade;
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();
        insert newQuote; 

        addSpexQuote();
        insert newSpexQuote;

        addSpexOpportunity(ida);
        spexOportunidade.Club__c = null;
        spexOportunidade.Event__c = null;
        insert spexOportunidade;

        addSpexQuote();
        insert newSpexQuote;

        spexOportunidade.Club__c = club[0].id;
        spexOportunidade.Event__c = evento.Id;
        update spexOportunidade;

        newSpexQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexQuote.Product_Exception__c = 'test';
        update newSpexQuote;
    }

    static testMethod void TestB() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[1].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        Database.insert(oportunidade);

        addSpexOpportunity(ida);
        insert spexOportunidade;
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        insert newQuote;  

        addSpexQuote();
        insert newSpexQuote; 

        addSpexOpportunity(ida);
        spexOportunidade.Club__c = null;
        spexOportunidade.Event__c = null;
        insert spexOportunidade;

        addSpexQuote();
        insert newSpexQuote;

        spexOportunidade.Club__c = club[1].id;
        spexOportunidade.Event__c = evento.Id;
        update spexOportunidade;

        newSpexQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexQuote.Product_Exception__c = 'test';
        update newSpexQuote;    
    }
    
    static testMethod void TestC() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[2].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        Database.insert(oportunidade);

        addSpexOpportunity(ida);
        insert spexOportunidade;
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();     
        insert newQuote; 

        addSpexQuote();
        insert newSpexQuote;      

        addSpexOpportunity(ida);
        spexOportunidade.Club__c = null;
        spexOportunidade.Event__c = null;
        insert spexOportunidade;

        addSpexQuote();
        insert newSpexQuote;

        spexOportunidade.Club__c = club[2].id;
        spexOportunidade.Event__c = evento.Id;
        update spexOportunidade;

        newSpexQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexQuote.Product_Exception__c = 'test';
        update newSpexQuote;     
    }
    
    static testMethod void TestD() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[3].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        Database.insert(oportunidade);

        addSpexOpportunity(ida);
        insert spexOportunidade;
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();       
        insert newQuote; 

        addSpexQuote();
        insert newSpexQuote; 

        addSpexOpportunity(ida);
        spexOportunidade.Club__c = null;
        spexOportunidade.Event__c = null;
        insert spexOportunidade;

        addSpexQuote();
        insert newSpexQuote;

        spexOportunidade.Club__c = club[3].id;
        spexOportunidade.Event__c = evento.Id;
        update spexOportunidade;

        newSpexQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexQuote.Product_Exception__c = 'test';
        update newSpexQuote;          
    }
    
    static testMethod void TestE() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[4].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        Database.insert(oportunidade);

        addSpexOpportunity(ida);
        insert spexOportunidade;
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();       
        insert newQuote;   

        addSpexQuote();
        insert newSpexQuote; 

        addSpexOpportunity(ida);
        spexOportunidade.Club__c = null;
        spexOportunidade.Event__c = null;
        insert spexOportunidade;

        addSpexQuote();
        insert newSpexQuote;

        spexOportunidade.Club__c = club[4].id;
        spexOportunidade.Event__c = evento.Id;
        update spexOportunidade;

        newSpexQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexQuote.Product_Exception__c = 'test';
        update newSpexQuote;        
    }
    
    static testMethod void TestF() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[5].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        Database.insert(oportunidade);

        addSpexOpportunity(ida);
        insert spexOportunidade;
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();     
        insert newQuote;  

        addSpexQuote();
        insert newSpexQuote;  

        addSpexOpportunity(ida);
        spexOportunidade.Club__c = null;
        spexOportunidade.Event__c = null;
        insert spexOportunidade;

        addSpexQuote();
        insert newSpexQuote;

        spexOportunidade.Club__c = club[5].id;
        spexOportunidade.Event__c = evento.Id;
        update spexOportunidade;

        newSpexQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexQuote.Product_Exception__c = 'test';
        update newSpexQuote;        
    }
    
    static testMethod void TestG() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[6].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        Database.insert(oportunidade);

        addSpexOpportunity(ida);
        insert spexOportunidade;
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();      
        insert newQuote; 

        addSpexQuote();
        insert newSpexQuote; 

        addSpexOpportunity(ida);
        spexOportunidade.Club__c = null;
        spexOportunidade.Event__c = null;
        insert spexOportunidade;

        addSpexQuote();
        insert newSpexQuote;

        spexOportunidade.Club__c = club[6].id;
        spexOportunidade.Event__c = evento.Id;
        update spexOportunidade;

        newSpexQuote.approval_updated_dt__c = DateTime.now() + 1;
        newSpexQuote.Product_Exception__c = 'test';
        update newSpexQuote;          
    }
    
    static testMethod void TestH() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[0].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        oportunidade.Product_Exceptions__c = null;
        oportunidade.General_Notes__c = null;
        oportunidade.Payment_Condition_s__c = null;
        Database.insert(oportunidade);
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        newQuote.approval_checkbox__c = false;
        newQuote.Product_Exception__c = null; 
        insert newQuote;      

        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test'; 
        newQuote.approval_updated_dt__c = DateTime.now();
        update newQuote; 
    }

    static testMethod void TestI() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[1].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        oportunidade.Product_Exceptions__c = null;
        oportunidade.General_Notes__c = null;
        oportunidade.Payment_Condition_s__c = null;
        Database.insert(oportunidade);
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        newQuote.approval_checkbox__c = false;
        newQuote.Product_Exception__c = null; 
        insert newQuote;      

        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test'; 
        newQuote.approval_updated_dt__c = DateTime.now();
        update newQuote; 
    }

    static testMethod void TestJ() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[2].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        oportunidade.Product_Exceptions__c = null;
        oportunidade.General_Notes__c = null;
        oportunidade.Payment_Condition_s__c = null;
        Database.insert(oportunidade);
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        newQuote.approval_checkbox__c = false;
        newQuote.Product_Exception__c = null; 
        insert newQuote;      

        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test'; 
        newQuote.approval_updated_dt__c = DateTime.now();
        update newQuote; 
    }

    static testMethod void TestK() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[3].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        oportunidade.Product_Exceptions__c = null;
        oportunidade.General_Notes__c = null;
        oportunidade.Payment_Condition_s__c = null;
        Database.insert(oportunidade);
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        newQuote.approval_checkbox__c = false;
        newQuote.Product_Exception__c = null; 
        insert newQuote;      

        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test'; 
        newQuote.approval_updated_dt__c = DateTime.now();
        update newQuote; 
    }

    static testMethod void TestL() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[4].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        oportunidade.Product_Exceptions__c = null;
        oportunidade.General_Notes__c = null;
        oportunidade.Payment_Condition_s__c = null;
        Database.insert(oportunidade);
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        newQuote.approval_checkbox__c = false;
        newQuote.Product_Exception__c = null; 
        insert newQuote;      

        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test'; 
        newQuote.approval_updated_dt__c = DateTime.now();
        update newQuote; 
    }

    static testMethod void TestM() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[5].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        oportunidade.Product_Exceptions__c = null;
        oportunidade.General_Notes__c = null;
        oportunidade.Payment_Condition_s__c = null;
        Database.insert(oportunidade);
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        newQuote.approval_checkbox__c = false;
        newQuote.Product_Exception__c = null; 
        insert newQuote;      

        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test'; 
        newQuote.approval_updated_dt__c = DateTime.now();
        update newQuote; 
    }

    static testMethod void TestO() {
        getSettings();
        addAccount();
        addContact();
        addClubs();
        
        string ida= club[6].id;
        
        system.debug('##### Club ID:' + ida);   
        
        addOpportunity(ida);
        oportunidade.Product_Exceptions__c = null;
        oportunidade.General_Notes__c = null;
        oportunidade.Payment_Condition_s__c = null;
        Database.insert(oportunidade);
        
        oportunidade = [Select id, Club__c, Club_formula__c, Club_ID__c, Club_Name__c  from Opportunity where id=:oportunidade.id];

        addQuote();    
        newQuote.approval_checkbox__c = false;
        newQuote.Product_Exception__c = null; 
        insert newQuote;      

        newQuote.approval_checkbox__c = true;
        newQuote.Product_Exception__c = 'test'; 
        newQuote.approval_updated_dt__c = DateTime.now();
        update newQuote; 
    }
}