/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 * Class responsible for event business rule
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 02/02/19  EricB  First version.
 *
 */

public without sharing class ClubBO {

    public static final ClubBO instance = new ClubBO();
    private ClubBO (){}
    public static ClubBO getInstance(){ return instance; }

    private ClubDAO dao = ClubDAO.getInstance();

    public List<Contract> findContractByClubId(Id clubId){
        return dao.findContractByClubId(new Set<Id>{clubId});
    }

    public List<Club__c> findClubById(Id clubId){
        return dao.findClubById(new Set<Id>{clubId});
    }

    public List<Order> findOrderByClubId(Id clubId){
        return dao.findOrderByClubId(new Set<Id>{clubId});
    }

}