/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsável por atualizar um campo no objeto session attendee a partir
* de um critério na opportunity
*
* NAME: OpportunityAtualizaSessionAttendee.cls
* AUTHOR: GC                                               DATE: 20/02/2017
*******************************************************************************/
public with sharing class OpportunityAtualizaSessionAttendee 
{
  private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');

  public static void execute()
  {
    TriggerUtils.assertTrigger();

    list<Opportunity> iLstOpp = new List<Opportunity>();

    for(Opportunity iOpp : (list<Opportunity>) trigger.new)
    {
      if((iOpp.RecordTypeId == ID_EVENT ) && (iOpp.StageName == 'Cancelled' || iOpp.StageName == 'Lost'))
      {
        iLstOpp.add(iOpp);
      }
    }
    if(iLstOpp.isEmpty()) return;

    map<String, list<Attendee__c>> mapSessionAtt = new map<String, list<Attendee__c>>();
    list<Attendee__c> lstSessionToUpdate = new list<Attendee__c>();

    for(Attendee__c iSessionAtt : [SELECT Id, Opportunity__c, Session_Status__c 
                                   FROM Attendee__c
                                   WHERE Opportunity__c =: iLstOpp])
    {
      List<Attendee__c> lstAtt = mapSessionAtt.get(iSessionAtt.Opportunity__c);
      if(lstAtt == null) lstAtt = new List <Attendee__c>();
      lstAtt.add(iSessionAtt);
      mapSessionAtt.put(iSessionAtt.Opportunity__c, lstAtt);
    }

    for(Opportunity eOpp : iLstOpp)
    {
      list<Attendee__c> eLstSession = mapSessionAtt.get(eOpp.Id);
      if(eLstSession == null || eLstSession.isEmpty()) return;

      for(Attendee__c pSession: eLstSession)
      {
        //pSession.Opportunity__c = eOpp.Id;
        pSession.Session_Status__c = 'Declined';
        lstSessionToUpdate.add(pSession);
      }
    } 
    database.upsert(lstSessionToUpdate);
  }
}