/**
* Generate the enviroment for the Unit Tests
* @author Sebastian Muñoz - Force.com Labs
* @createddate 06/08/2010
*/
public with sharing class ContactHierarchyTestData{
	
	/**
	* Set the necesary attributes used during test
	*/
    public static void createTestHierarchy(){
    	
    	InlineContactHerachy_TestUtilities testUtils = new InlineContactHerachy_TestUtilities();
    	//Set of Fields should be checked
    	Set<String> fieldsToCheck = new Set<String>{'ReportsToId', 'LastName'};
		
		//Create my Parent Contact 
		testUtils.createContacts( 1 , fieldsToCheck );
 		testUtils.testConList[0].LastName = 'HierarchyTest0';
 		testUtils.updateContactList( fieldsToCheck );
 		
 		Contact parentContact = testUtils.testConList[0];
        Id ReportsToId = parentContact.Id;
        System.Assert( ReportsToId != null , 'Parent Id not found' );
        
        // Create 10 sub accounts
    	testUtils.createContacts( 10 , fieldsToCheck );
    	Integer i = 0;
        for ( Contact cttAux : testUtils.testConList ){ //Now i need change the names
        	cttAux.LastName = 'HierarchyTest' + String.valueOf( i );
            i++;
        }
        testUtils.updateContactList( fieldsToCheck );        
        
        List<Contact> contactList = [ Select Id, ReportsToId, LastName from Contact where LastName like 'HierarchyTest%' ORDER BY LastName limit 10 ];
                
        for ( Integer x = 0; x < contactList.size(); x++ ){
            if ( contactList[x].LastName != 'HierarchyTest0' ){
                contactList[x].ReportsToId = ReportsToId;
                ReportsToId = contactList[x].Id; 
            }
        }
        
        testUtils.testConList.clear();
        testUtils.testConList.addAll( contactList );
        testUtils.updateContactList( fieldsToCheck );

		// Create 10 sub accounts
		Contact subTreeParent = [ Select Id, ReportsToId, LastName from contact where LastName = 'HierarchyTest4' limit 10 ];
        ReportsToId = subTreeParent.Id;
        testUtils.createContacts( 10, fieldsToCheck );
    	 
		i = 0;
		for ( Contact cttAux : testUtils.testConList ){ //Now i need change the names
        	cttAux.LastName = 'HierarchyTest' + '4.' + String.valueOf( i );
        }
		testUtils.updateContactList( fieldsToCheck );

        List<Contact> subContactsList = [ Select Id, ReportsToId, LastName from contact where LastName like 'HierarchyTest4%' limit 10  ];
        for ( Integer z = 1; z < subContactsList.size(); z++ ){
            subContactsList[z].ReportsToId = ReportsToId;
            ReportsToId = contactList[z].Id; 
        }
        
        testUtils.testConList.clear();
        testUtils.testConList.addAll( subContactsList );
    }
}