global class RemoveCalendarInviteBatch implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts
{

    public List<Calendar_Invite__c> calinviteObjLst{get;set;}
    public Map<Id,set<string>> notifiedEmailsMap{get;set;}
    public set<string> setOfEmails{get;set;}
    public string accessToken{get;set;}
    public Opportunity opptyObj{get;set;}
     private static string tokenEndPointValue{get;set;}
    private static string endPointValue{get;set;}
     private static string googleClientID{get;set;}
    private static string googleSecretCode{get;set;}
    private static string redirectURI{get;set;}
    private static string authorizationCode{get;set;}
    private static string batchAttendeeEndpoint;
    public Map<Id,Id> eventToOppMap{get;set;}
    public string automationType{get;set;}
    public set<Id> oppIds{get;set;}
    private static string accessToken;
    private map<Id,string> CalinviteToAttendeeStatusMap = new Map<Id,String>();
    private List<Calendar_Invite__c> finalInviteLst = new List<Calendar_Invite__c>();
    public RemoveCalendarInviteBatch(list<Calendar_Invite__c> calInviteLst,Map<Id,set<string>> notifiedEmailsMapVal,set<string> setOfEmailsVal,string  accessTokenVal,Opportunity opptyObjVal, string automationTypeVal,Map<Id,Id> eventToOppMapVal,set<Id> oppIdsVal, map<Id,string> CalinviteToAttendeeStatusMapVal) {
        calinviteObjLst = calInviteLst;
        notifiedEmailsMap = notifiedEmailsMapVal;
        setOfEmails = setOfEmailsVal;
        accessToken = accessTokenVal;
        opptyObj = opptyObjVal;
        automationType = automationTypeVal;
        eventToOppMap = eventToOppMapVal;
        oppIds = oppIdsVal;
        CalinviteToAttendeeStatusMap = CalinviteToAttendeeStatusMapVal;
    }
    global List<SObject> start(Database.BatchableContext context)
    {
        system.debug('@@calinviteObjLst'+calinviteObjLst);
        return calinviteObjLst;
    }   
    
    global void execute(Database.BatchableContext BC, List<Calendar_Invite__c> scope)
    { 
        system.debug('@@scope'+scope);
        List<Google_Calendar_API_Detail__mdt> mtdCalendarApiLst =  [ SELECT GoogleClientId__c,GoogleBatchEndpoint__c,GoogleEndPoint__c,GoogleRedirectURI__c,GoogleSecretCode__c,GoogleTokenEndpoint__c, GoogleUserCode__c,GoogleAttendeeEndPoint__c FROM Google_Calendar_API_Detail__mdt ]; 
        system.debug('@@mtdCalendarApiLst'+mtdCalendarApiLst);
        String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
        list<CalendarInvite_attendees> clAttendeesLst = new List<CalendarInvite_attendees>();
        if(mtdCalendarApiLst != null && mtdCalendarApiLst.size()>0){
            googleClientID = mtdCalendarApiLst[0].GoogleClientId__c;
            googleSecretCode = mtdCalendarApiLst[0].GoogleSecretCode__c;
            redirectURI = mtdCalendarApiLst[0].GoogleRedirectURI__c;
            authorizationCode = mtdCalendarApiLst[0].GoogleUserCode__c;
            tokenEndPointValue = mtdCalendarApiLst[0].GoogleTokenEndpoint__c;
            
            endPointValue = mtdCalendarApiLst[0].GoogleAttendeeEndPoint__c;
            batchAttendeeEndpoint = mtdCalendarApiLst[0].GoogleBatchEndpoint__c;
        }
        
        for(Calendar_Invite__c calndrInviteRecord:scope){
                if(CalinviteToAttendeeStatusMap.containsKey(calndrInviteRecord.Id)){
                system.debug('@@CalinviteToAttendeeStatusMap.get(calndrInviteRecord.Id)'+CalinviteToAttendeeStatusMap.get(calndrInviteRecord.Id));
                if(CalinviteToAttendeeStatusMap.get(calndrInviteRecord.Id) != 'Yes'){
                finalInviteLst.add(calndrInviteRecord);
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                if(calndrInviteRecord.GoogleCalendarEventId__c != null && calndrInviteRecord.GoogleCalendarId__c != null && notifiedEmailsMap.containsKey(calndrInviteRecord.id)){
                    req.setEndpoint(endPointValue+calndrInviteRecord.GoogleCalendarId__c+'/events/'+calndrInviteRecord.GoogleCalendarEventId__c);
        
                    set<string> finalSetofEmails =  new set<string>();
                    if(calndrInviteRecord.GoogleCalendarId__c !=''){
                        finalSetofEmails.add(calndrInviteRecord.GoogleCalendarId__c);
                    }
                    
                    for(Calendar_Invitee__c calendarInviteeObj:calndrInviteRecord.Calendar_Invitees__r){
                        if(calendarInviteeObj.Invite_Status__c !='Yes'){
                            finalSetofEmails.add(calendarInviteeObj.InviteeEmail__c);
                        }
                    }
                    if(setOfEmails.size()>0){
                        for(string strObj:setOfEmails){
                            if(strObj != null)
                                finalSetofEmails.add(strObj);
                        }
                    }
                    system.debug('@@finalSetofEmails'+finalSetofEmails);
                    
                    for(string strEmail:notifiedEmailsMap.get(calndrInviteRecord.Id)){
                        if(finalSetofEmails.contains(strEmail)){
                            finalSetofEmails.remove(strEmail);
                        }
                    }
                    system.debug('@@finalSetofEmails'+finalSetofEmails);
                    
                    Integer finalHoursToBeAdded = 0;
                    
                    if(calndrInviteRecord.Timezone__c != null){
                        List<string> dateLst;
                        List<string> finalHoursLst;
                        Boolean IsNegativeValue = false;
                        if(calndrInviteRecord.Timezone__c.contains('+')){
                            dateLst =  calndrInviteRecord.Timezone__c.split('\\+');
                        
                        }else if(calndrInviteRecord.Timezone__c.contains('-')){
                            IsNegativeValue = true;
                            dateLst = calndrInviteRecord.Timezone__c.split('\\-');
                        }
                        system.debug('@dateLst'+dateLst);
                        if(dateLst != null && dateLst.size()>1 ){
                            finalHoursLst = dateLst[1].split('\\:');
                        }
                        system.debug('@finalHoursLst'+finalHoursLst);
                        if(finalHoursLst != null){
                            if(!IsNegativeValue){
                                finalHoursToBeAdded = -1*(Integer.valueOf(finalHoursLst[0]));
                            }else{     
                                finalHoursToBeAdded = Integer.valueOf(finalHoursLst[0]);
                            }
                        }
                    }
                    system.debug('@@finalHoursToBeAdded'+finalHoursToBeAdded);
                    if(calndrInviteRecord.Start_Date__c.month() != Date.Today().Month()){
                        if(string.valueOf(finalHoursToBeAdded).contains('-')){
                            finalHoursToBeAdded = finalHoursToBeAdded+1;
                        }else{
                            finalHoursToBeAdded = finalHoursToBeAdded-1;
                        }
                    }
                    system.debug('@@finalHoursToBeAddedFinal'+finalHoursToBeAdded);
                    finalHoursToBeAdded = finalHoursToBeAdded+2;
                    //system.debug('@@finalHoursToBeAdded128'+finalHoursToBeAdded);

                    //String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
                    list<CalendarInvite_recurrence> clRecurrenceLst= new List<CalendarInvite_recurrence>();
                    list<CalendarInvite_attendees> notifyAttendeesLst=new List<CalendarInvite_attendees>();
                    system.debug('@@calndrInviteRecord.Start_Date__c'+calndrInviteRecord.Start_Date__c+'@@calndrInviteRecord.End_Date__c'+calndrInviteRecord.End_Date__c);
                    
                    //DateTime startDt = datetime.newInstance(calndrInviteRecord.Start_Date__c.year(), calndrInviteRecord.Start_Date__c.month(),calndrInviteRecord.Start_Date__c.day()); 
                    String startDateString = (calndrInviteRecord.Start_Date__c).addHours(finalHoursToBeAdded).format(dateFormat);
                    System.debug('@@startDateString'+startDateString); // 2016-02-29T22:30:48Z
                    
                    //DateTime endDt = datetime.newInstance(calndrInviteRecord.End_Date__c.year(), calndrInviteRecord.End_Date__c.month(),calndrInviteRecord.End_Date__c.day());
                    String endDateString = calndrInviteRecord.End_Date__c.addHours(finalHoursToBeAdded).format(dateFormat);
                    System.debug('@@endDateString'+endDateString); // 2016-02-29T22:30:48Z
                    
                    system.debug('@@UserInfo.getTimeZone()'+UserInfo.getTimeZone());
                    
                    CalendarInvite_start clStart = new CalendarInvite_start(startDateString,string.valueof(UserInfo.getTimeZone()));
                    CalendarInvite_End clEnd = new CalendarInvite_End(endDateString,string.valueOf(UserInfo.getTimeZone()));
                    system.debug('@@clStart'+clStart+'@@clEnd'+clEnd);
                    
                    for(string strEmail:finalSetofEmails){
                        if(strEmail != null)
                            notifyAttendeesLst.add(new CalendarInvite_attendees(strEmail));
                    }
                    //clAttendeesLst.add(new CalendarInvite_attendees(defaultGoogleCalendarId));
                    
                    clRecurrenceLst.add(new CalendarInvite_recurrence('RRULE:FREQ=DAILY;UNTIL=20300930T170000Z'));
                    
                    system.debug('@@calndrInviteRecord.Invite_Others__c'+calndrInviteRecord.Invite_Others__c+'@@calndrInviteRecord.Modify_Event__c'+calndrInviteRecord.Modify_Event__c+'@@calndrInviteRecord.See_Guest_List__c'+calndrInviteRecord.See_Guest_List__c);
                    system.debug('@@calndrInviteRecord.Description__c'+calndrInviteRecord.Description__c);
                    
                    CalendarNotifyInviteWrapper calInviteWrapObj = new CalendarNotifyInviteWrapper(calndrInviteRecord.Event__r.name+' - '+ calndrInviteRecord.Invite_Type__c,calndrInviteRecord.Venue__r.name+' , '+calndrInviteRecord.Venue_Address__c,clStart,clEnd, null,notifyAttendeesLst,calndrInviteRecord.Description__c
                                                                                        ,calndrInviteRecord.Invite_Others__c, calndrInviteRecord.Modify_Event__c, calndrInviteRecord.See_Guest_List__c);
                    
                    string bodyRequest = JSON.serialize(calInviteWrapObj);
                    bodyRequest = bodyRequest.replace('"enddate":', '"end":');
                    bodyRequest = bodyRequest.replace('"cld_dateTime":', '"dateTime":');
                    
                    System.debug('@@bodyRequest'+bodyRequest);
                  
                    req.setBody(bodyRequest);     

                    req.setHeader('Authorization', 'Bearer ' + getAccessToken());
                    req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
                    req.setHeader('Content-Type', 'application/json; charset=UTF-8');
                    
                    req.setMethod('PUT');
                    
                    req.setTimeout(10000);
                    if(!Test.isRunningTest()){
                        HttpResponse res = h.send(req); 
                        System.debug('@@res.getBody()'+res.getBody());
                    }
                    }
                }
                }else{
                    finalInviteLst.add(calndrInviteRecord);
                
                }
                }
            }

    global void finish(Database.BatchableContext BC)
    {
        
            CreateCalendarInviteBatch be = new CreateCalendarInviteBatch(finalInviteLst,notifiedEmailsMap,setOfEmails,'',opptyObj,'Opportunity',eventToOppMap,oppIds,CalinviteToAttendeeStatusMap);
            database.executeBatch(be,1);
        
    }
    
     
    public static string getAccessToken(){
        
        string bodyRequest = '';
        
        system.debug('@@authorizationCode'+authorizationCode);       
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(tokenEndPointValue);

        bodyRequest = 'refresh_token=' + EncodingUtil.urlEncode(authorizationCode, 'UTF-8');
       
        bodyRequest += '&client_id=' + EncodingUtil.urlEncode(googleClientID, 'UTF-8');
        bodyRequest += '&client_secret=' + EncodingUtil.urlEncode(googleSecretCode, 'UTF-8');
        bodyRequest += '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8');
       
        bodyRequest += '&grant_type=refresh_token';
        req.setBody(bodyRequest);     
        req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setTimeout(10000);
        string accessRequestToken ='';
        if(!Test.isRunningTest()){
                        
            HttpResponse res = h.send(req);   
            map<string, string> jsonValues = new map<string, string>();
          
            System.debug('Response Value:'+res.getBody());
            jsonValues = parseJSONToMap(res.getBody());
            if(jsonValues.containsKey('error')){ 
            }else{
                //Try to get a cell value in the Google Spreadsheet
                accessToken = jsonValues.get('access_token');            
            }
        }else{
            accessToken = 'as23as32ease23asd';
        }
        
        if(accessToken <> ''){
           return accessToken ;
        } else{
        return 'Invalid Refresh token';
        }
           
    }
      public static map<string, string> parseJSONToMap(string JSONValue){
        JSONParser parser = JSON.createParser(JSONValue);
        map<string, string> jsonMap = new map<string, string>();
        string keyValue = '';
        string tempValue = '';
        while (parser.nextToken() != null) {
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME){
                keyValue = parser.getText();
                parser.nextToken();
                tempValue = parser.getText();
                jsonMap.put(keyValue, tempValue);             
            }
        }
        return jsonMap;
    }
    public class CalendarInvite_start {
            public String cld_dateTime; //2018-09-29T08:00:00.000-07:00
            public String timeZone; //America/Los_Angeles
            public CalendarInvite_start(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
    public class CalendarInvite_end {
            private String cld_dateTime{get;set;} //2018-09-30T08:30:00.000-07:00
            private String timeZone; //America/Los_Angeles
            private CalendarInvite_end(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
    public class CalendarInvite_overrides{
        private String method; //2018-09-30T08:30:00.000-07:00
        private Integer minutes; //America/Los_Angeles
        public CalendarInvite_overrides(string methodVal, Integer minutesVal){
            this.method = methodVal;
            this.minutes = minutesVal;
        }
        
    }
    public class CalendarNotifyInviteWrapper{
    public String summary;  //Sales Call
    public String location; //Conference Room A
    public CalendarInvite_start start;
    public CalendarInvite_end enddate;
    public List<CalendarInvite_recurrence> recurrence;
    public List<CalendarInvite_attendees> attendees;
    public string description;
    
    public boolean guestsCanInviteOthers;
    public boolean guestsCanModify;
    public boolean guestsCanSeeOtherGuests;
    
        public CalendarNotifyInviteWrapper(string summaryVal,string locationVal,CalendarInvite_start cld_StartDate,CalendarInvite_end cld_endDate, List<CalendarInvite_recurrence> cld_recurList,List<CalendarInvite_attendees> cld_recurAttList,string calendarDescription,
                                        boolean guestsCanInvite, boolean guestsCanMod, boolean guestsCanSee){
            this.summary = summaryVal;
            this.location = locationVal;
            this.start = cld_StartDate;
            this.endDate = cld_endDate;
            this.recurrence = cld_recurList;
            this.attendees = cld_recurAttList;
            this.description = calendarDescription;
            this.guestsCanInviteOthers = guestsCanInvite;
            this.guestsCanModify = guestsCanMod;
            this.guestsCanSeeOtherGuests = guestsCanSee;
        }
        
    
    }

     public class CalendarInvite_recurrence {
            public String recurrence;
            public CalendarInvite_recurrence(string recurVal){
                this.recurrence = recurVal;
            }
        }    
    public class CalendarInvite_attendees {
            public String email;    //maddy.crmdev@gmail.com
            public CalendarInvite_attendees(string organizerEmail){
                this.email = organizerEmail;
            }
        }
  
}