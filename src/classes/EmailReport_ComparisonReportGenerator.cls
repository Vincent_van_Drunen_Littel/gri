global class EmailReport_ComparisonReportGenerator implements Schedulable {
	global void execute(SchedulableContext sc) {
		
		List<EmailReport__c> emailReport = [SELECT Id, Report_Type__c, 
												(SELECT Id FROM Attachments)  
											FROM EmailReport__c 
											WHERE Report_Date__c = TODAY AND Report_Type__c = 'Comparison'];

		EmailReport__c report;

		if(emailReport.Size() > 0)
		{
			report = emailReport[0];
			delete report.Attachments;
		}
		else
		{
			report = new EmailReport__c(Report_Date__c = Date.Today(), Report_Type__c = 'Comparison', Report_Week__c = System.Now().Format('w'));
			insert report;
		}

		processStep1(report.Id);
		processStep2(report.Id);
		processStep3(report.Id);
	}

	webservice static Id generateComparisonReport()
	{
		EmailReport_ComparisonReportGenerator gen = new EmailReport_ComparisonReportGenerator();
		gen.execute(null);

		return [SELECT Id  
				FROM EmailReport__c 
				WHERE Report_Date__c = TODAY AND Report_Type__c = 'Comparison'].Id;
	}

	@future
	private static void processStep1(Id reportId)
	{
		EmailReport_ComparisonReportGen_Helper comparisonReport = new EmailReport_ComparisonReportGen_Helper();

		comparisonReport.queryClubComparison();
		//comparisonReport.queryClubComparison2();

		List<Attachment> reportAttachments = new List<Attachment>();

		//reportAttachments.add(new Attachment(Name = 'ClubBreakdown.txt', Body = Blob.valueOf(JSON.serialize(comparisonReport.club)), ParentId = reportId));
		reportAttachments.add(new Attachment(Name = 'ClubComparison.txt', Body = Blob.valueOf(JSON.serialize(comparisonReport.clubComparison)), ParentId = reportId));

		insert reportAttachments;
	}

	@future
	private static void processStep2(Id reportId)
	{
		EmailReport_ComparisonReportGen_Helper comparisonReport = new EmailReport_ComparisonReportGen_Helper();

		comparisonReport.queryEventComparison();

		List<Attachment> reportAttachments = new List<Attachment>();

		reportAttachments.add(new Attachment(Name = 'FutureEventsComparison.txt', Body = Blob.valueOf(JSON.serialize(comparisonReport.futureEventComparison)), ParentId = reportId));
		reportAttachments.add(new Attachment(Name = 'PreviousEventsComparison.txt', Body = Blob.valueOf(JSON.serialize(comparisonReport.previousEventComparison)), ParentId = reportId));

		insert reportAttachments;
	}

	@future
	private static void processStep3(Id reportId)
	{
		EmailReport_ComparisonReportGen_Helper comparisonReport = new EmailReport_ComparisonReportGen_Helper();

		comparisonReport.queryBusinessUnits();

		List<Attachment> reportAttachments = new List<Attachment>();

		reportAttachments.add(new Attachment(Name = 'BusinessUnitsComparison.txt', Body = Blob.valueOf(JSON.serialize(comparisonReport.businessUnitComparison)), ParentId = reportId));

		insert reportAttachments;
	}
}