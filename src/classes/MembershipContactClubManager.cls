public class MembershipContactClubManager {

    private static Boolean isExecuting = false;
    
    private static final String MEMBERSHIP_STATUS_ACTIVATED = 'Activated';
    private static final String MEMBERSHIP_STATUS_EXPIRED = 'Expired'; // grace period
    private static final String MEMBERSHIP_STATUS_DRAFT = 'Draft'; // grace period

    private static final String CLUB_INFRA_INDIA = 'Infra Club India';
    private static final String CLUB_INFRA_LATAM = 'Infra Club LatAm';
    private static final String CLUB_RE_AFRICA = 'RE Club Africa';
    private static final String CLUB_RE_BRAZIL = 'RE Club Brazil';
    private static final String CLUB_RE_EUROPE = 'RE Club Europe';
    private static final String CLUB_GRI_GLOBAL_CLUB = 'GRI Global Club';
    private static final String CLUB_RE_INDIA = 'RE Club India';
    private static final String CLUB_RE_LATAM = 'RE Club LatAm';
    private static final String CLUB_RETAIL_BRAZIL = 'Retail Club Brazil';
        
    /**
     * This metod supose that the input list of membership were set to  Activated.
     * It updates contact fields
     */
    public static List<Contact> updateContactsFields(List<Contract>  newMap)
    {
        System.debug('%% [Debug] newMap='+newMap);
        List<Contact> listCon = new List<Contact>();
        
        // Prevent infinit loop
        if(isExecuting) return listCon;
        isExecuting = True;
        
        for(Contract memb : newMap)
        {
            
            Contact ccc = memb.Contact__r;
            System.debug('%% [Debug] before>ccc='+ccc);
            if( (memb.Status != MEMBERSHIP_STATUS_ACTIVATED) &&
               (memb.Status != MEMBERSHIP_STATUS_EXPIRED) &&
               (memb.Status != MEMBERSHIP_STATUS_DRAFT) )
            {
                ccc = setContactStatus(memb.Club_Name__c, ccc, False);   
            }
            else if (memb.Status == MEMBERSHIP_STATUS_ACTIVATED)
            {
                ccc = setContactStatus(memb.Club_Name__c, ccc, True); 
            }
            else // membership log
            {
                System.debug('%% [info] memb.Status='+memb.Status);
            }
            System.debug('%% [Debug] after>ccc='+ccc);
            listCon.add(ccc);
            System.debug('%% [Debug] listCon.size()='+listCon.size());
        }

        return listCon;
    }    
    
    public static Contact setContactStatus(String ClubName, Contact ccc, Boolean status)
    {
        if(ClubName == CLUB_INFRA_INDIA)
        {
            ccc.Infra_Club_India_Member__c = status; 
        }
        else if(ClubName == CLUB_INFRA_LATAM)
        {
            ccc.Infrastructure_BR_Club__c = status;
        }
        else if(ClubName == CLUB_RE_AFRICA)
        {
            ccc.RE_Club_Africa_Member__c  = status;
        }
        else if(ClubName == CLUB_RE_BRAZIL)
        {
            ccc.Real_Estate_BR_Club__c = status;                 
        }
        else if(ClubName == CLUB_RE_EUROPE)
        {
            ccc.Global_Club_Member__c = status; 
        }
        else if(ClubName == CLUB_GRI_GLOBAL_CLUB)
        {
            ccc.Global_Club_Member__c = status;                
        }
        else if(ClubName == CLUB_RE_INDIA)
        {
            ccc.India_Club_Member__c = status;
        }
        else if(ClubName == CLUB_RE_LATAM)
        {
            ccc.RE_Club_Latam_Member__c = status;
        }
        else if(ClubName == CLUB_RETAIL_BRAZIL)
        {
            ccc.Retail_Club_BR__c = status;
        }
        else 
        {
            System.debug('%% [Debug] memberhip does not belong to any tracket club ClubName='
                         +ClubName);
        }
        return(ccc);
    }
}