/***************************************************************************************************
*                               Cloud2b - 2014
*---------------------------------------------------------------------------------------------------
* 
* Class responsible for storing in memory a map with the sObjects record types.
*
* NAME: RecordTypeMemory.cls
* AUTHOR: RNM                                                                       DATE: 08/05/2014
***************************************************************************************************/
public with sharing class RecordTypeMemory {

  private static Map< String, Map< String, Id > > fRecTypes = new Map< String, Map< String, Id > >();
  
  static {
    List< RecordType > lList = [ select id, SobjectType, DeveloperName from RecordType ];
    for ( RecordType lRec : lList )
    {
      Map< String, Id > lObjMap = fRecTypes.get( lRec.SobjectType );
      if ( lObjMap == null )
      {
        lObjMap = new Map< String, Id >();
        fRecTypes.put( lRec.SobjectType, lObjMap );
      }
      lObjMap.put( lRec.DeveloperName, lRec.id );
    }
  }
  
  public static id getRecType( String aObj, String aDevName )
  {
    Map< String, Id > lObjMap = fRecTypes.get( aObj );
    if ( lObjMap == null ) return null;
    return lObjMap.get( aDevName );
  }
  
}