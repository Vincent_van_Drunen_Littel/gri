/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste para classe ContactCreateMassOppControllerTest.cls
*
* NAME: ContactCreateMassOppControllerTest.cls
* AUTHOR: KHPS                                                 DATE: 14/04/2016
* MAINTENANCE: AFC                                             DATE: 30/06/2016 
*******************************************************************************/
@isTest
private class ContactCreateMassOppControllerTest 
{
  private static Club__c club;
  private static Event__c event;
  private static Magazine__c magazine;
  private static Product2 prod1;
  private static Id IdPBStandard;
  private static PricebookEntry pbe, pbeCustom;
  private static Pricebook2 priceCustom;
  private static Contact ctt, ctt2;
  private static Campaign camp;
  private static list<Contact> lstContact;
  
  static
  {
    club = SObjectInstance.createClub();
    Database.insert(club);
    
    event = SObjectInstance.createEvent();
    Database.insert(event);

    event.Start_Date__c = system.today().addDays(22);
    event.End_Date__c = system.today().addDays(25);
    update(event);
    
    magazine = SObjectInstance.createMagazine();
    magazine.Name = 'Teste Dev';
    insert magazine;
    
    prod1 = new Product2();
    prod1.Name = 'Teste Produto';
    insert prod1;
    
    IdPBStandard = SObjectInstance.catalogoDePrecoPadrao2();
    
    pbe = SObjectInstance.entradaDePreco(IdPBStandard, prod1.Id);
    insert pbe;
    
    priceCustom = SObjectInstance.catalogoDePreco();
    priceCustom.IsActive = true;
    priceCustom.Club__c = club.Id;
    insert priceCustom;
    
    pbeCustom = SObjectInstance.entradaDePreco(priceCustom.Id, prod1.Id);
    insert pbeCustom;
    
    Account acc = SObjectInstance.createAccount2();
    acc.ShippingStreet     = '1 Main St.';
    acc.ShippingState    = 'VA';
    acc.ShippingPostalCode  = '12345';
    acc.ShippingCountry    = 'USA';
    acc.ShippingCity      = 'Anytown';
    acc.Description      = 'This is a test account';
    acc.BillingStreet    = '1 Main St.';
    acc.BillingState      = 'VA';
    acc.BillingPostalCode  = '12345';
    acc.BillingCountry     = 'USA';
    acc.BillingCity      = 'Anytown';
    acc.AnnualRevenue    = 10000;
    acc.ParentId        = null;
    Database.insert(acc);

    ctt = SObjectInstance.createContato(acc.Id);
    ctt.Email = 'a@b.com.br';
        
    ctt2 = SObjectInstance.createContato(acc.Id);
    ctt2.Email = 'b@a.com.br';

    lstContact = new list<Contact>{ctt, ctt2};
    Database.insert(lstContact);
        
    camp = SObjectInstance.createCampaign();
    camp.Name = 'camp name';
    camp.Event__c = event.Id;
    Database.insert(camp);
  }
  
  static testMethod void testeFuncional() 
  {
    Test.startTest();
    ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstContact);
    ContactCreateMassOpportunityController ctl = new ContactCreateMassOpportunityController(stdSetController);
    
    ctl.oppCttType = 'Magazine';
    ctl.changeName();
    
    ctl.oppCttType = 'Open_Events';
    ctl.changeName();
        
    ctl.oppCttType = 'SPEX';
    ctl.changeName();
    
    ctl.oppCttType = 'Membership';
    ctl.changeName();

    ctl.opp.Club__c = club.Id;
    ctl.opp.OwnerId = UserInfo.getUserId();
    ctl.SelecionaPricebook();
    ctl.CarregaProdutos();
    
    List<SelectOption> lstOptions = ctl.getTypeOppCamp;
    
    ctl.loadPopup();
    
    for(ContactCreateMassOpportunityController.ProductWrapper lPWpp : ctl.lstProdutoWpp)
    {
      lPWpp.IsSelected = true;
    }
    
    ctl.closePopup();    
    ctl.createMassOpportunities();
    
    Test.stopTest();
  }
  
  static testMethod void testeErros() 
  {
    Test.startTest();
    ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstContact);
    ContactCreateMassOpportunityController ctl = new ContactCreateMassOpportunityController(stdSetController);
    
    ctl.oppCttType = 'Membership';
    ctl.changeName();

    ctl.opp.Club__c = club.Id;
    ctl.opp.OwnerId = UserInfo.getUserId();
    ctl.SelecionaPricebook();
    ctl.CarregaProdutos();
    
    ctl.loadPopup();
    
    for(ContactCreateMassOpportunityController.ProductWrapper lPWpp : ctl.lstProdutoWpp)
    {
      lPWpp.IsSelected = true;
    }
    
    ctl.closePopup();
    
    ctl.oppCttType = 'Open_Events';
    ctl.changeName();
    ctl.opp.Event__c = null;
    ctl.createMassOpportunities();
    
    ctl.oppCttType = 'Magazine';
    ctl.changeName();
    ctl.opp.Magazine__c = null;
    ctl.createMassOpportunities();
    
    ctl.oppCttType = 'Membership';
    ctl.changeName();
    ctl.opp.Club__c = null;
    ctl.createMassOpportunities();
    
    ctl.opp.OwnerId = null;
    ctl.createMassOpportunities();
    
    ctl.loadPopup();
    
    for(ContactCreateMassOpportunityController.ProductWrapper lPWpp : ctl.lstProdutoWpp)
    {
      lPWpp.IsSelected = false;
    }
    
    ctl.closePopup();
    ctl.createMassOpportunities();   
    
    ctl.opp.CloseDate = null;    
    ctl.createMassOpportunities();
    
    ctl.cancel();
    
    Test.stopTest();
  }
}