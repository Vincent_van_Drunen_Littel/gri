/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe responsável por enviar opportunity para o processo de approvação
* sempre que a porcentagem for alterada.
*
* NAME: OpportunityLineItemUpdatesOportunidade.trigger
* AUTHOR: EAD                                                  DATE: 07/02/2018
*******************************************************************************/

public class OpportunityLineItemUpdatesOportunidade {
  
  //=== Método principal ===    
  public static void execute(){
    TriggerUtils.assertTrigger();
    set<String> lstOppId = new set<String>();
    
    for(OpportunityLineItem oli : (list<OpportunityLineItem>)Trigger.new){
        if(Trigger.isInsert){
        	if(oli.Discount > 0) lstOppId.add(oli.OpportunityId);
        } else if(Trigger.isUpdate){

            if(TriggerUtils.wasChangedTo(oli, OpportunityLineItem.Discount, 0) || 
              TriggerUtils.wasChangedTo(oli, OpportunityLineItem.Discount, null)){
            	continue;
            } else {
                if(TriggerUtils.wasChanged(oli, OpportunityLineItem.Discount)) lstOppId.add(oli.OpportunityId);
            }
        }        
    }
      
    list<Opportunity> lstOpp = [SELECT Id, Approval_Process__c FROM Opportunity WHERE id =: lstOppId];  

    /* == Atualização do campo Approval_Process__c da oportunidade. 
     * Isso forçará a trigger do approval proccess a rodar == */
    if(!lstOpp.isEmpty()){
        for(Opportunity opp : lstOpp){
          opp.Approval_Process__c = '2 - Pending';    
        }
    }

    if(!lstOpp.isEmpty()) Database.update(lstOpp); 

  }
}