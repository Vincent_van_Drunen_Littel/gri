@isTest
public with sharing class OpportunityAtualizaSessionAttendeeTest 
{
  private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');

  static testMethod void TesteFuncionalLost()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);

    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);

    Test.StartTest();
    iOpp.StageName = 'Lost';
    iOpp.Lost_reason__c = 'Teste';
    Database.update(iOpp);
    Test.StopTest();
  }

  static testMethod void TesteFuncionalCancelled()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);

    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);

    Test.StartTest();
    iOpp.StageName = 'Cancelled';
    Database.update(iOpp);
    Test.StopTest();
  }
}