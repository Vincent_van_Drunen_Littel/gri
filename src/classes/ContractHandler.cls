/******************************************************************************
* GRI Club - Gathering the world’s leading real estate and infrastructure 
* players 
******************************************************************************
* Class to handle the Membership trigger
*
* @author  Anderson Paschoalon
* @version 1.0
* @since
* Date      Author     Description
* 23/05/18  AndersonP  First version. I transfered the code from the deprecated 
*                      Contract(Membership) trigger to here to cleanup it.
*                      Now the Contract SObject has just a single trigger.
*/
public class ContractHandler 
{
    
    ///////////////////////////////////////////////////////////////////////////
    // Triggler handlers
    ///////////////////////////////////////////////////////////////////////////
    
    /**
     * Before Update handler
     */
    public static void before()
    {
        System.debug(LOGTAG+'ContractHandler.before() ');
        if(Trigger.isInsert)
        {
        }
        if(Trigger.isUpdate)
        {
        }
    }
    
    /**
     * After Update handler
     */
    public static void after()
    {
        System.debug(LOGTAG+'ContractHandler.after() ');
        if(Trigger.isInsert || Trigger.isUpdate)
        {
            //updateContactClubFields_old(Trigger.new);
            updateContactClubFields(Trigger.new);
            //autoWonRenewedMemberships(Trigger.new);
            autoWonRenewedMemberships(Trigger.new);
        }
        else if(Trigger.isInsert)
        {
        }
        else if(Trigger.isUpdate)
        {
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Properties
    ///////////////////////////////////////////////////////////////////////////
    private static final String LOGTAG = '%% ContractHandler: ';
    
    ///////////////////////////////////////////////////////////////////////////
    // Contact Field Club Update 
    ///////////////////////////////////////////////////////////////////////////
    
    /**
     * This method take as input a list of memberships, and updates the custom
     * checkbox field on the contact related to that mebership, setting it as
     * TRUE if the membership is Active, Expired (Grace Period) or Draft, and
     * as FALSE otherwise.  
     */
    public static void updateContactClubFields(List<Contract> listMemberships)
    {
        System.debug(LOGTAG+'updateContactClubFields(): '+HelperSObject.json(listMemberships, 'Id, Name, Status'));
        Set<String> setContactsIds = HelperSObject.setFromFieldSafe(listMemberships, 'Contact__c');
        Map<Id, Contact> mapId_Contact = new Map<Id, Contact>(
            [SELECT  Id, FirstName, Real_Estate_BR_Club__c, RE_Club_Latam_Member__c, 
             Infrastructure_BR_Club__c, Retail_Club_BR__c, India_Club_Member__c, 
             Global_Club_Member__c, RE_Club_Africa_Member__c, Infra_Club_India_Member__c
             FROM Contact WHERE Id IN :setContactsIds]);     
        List<String> listActiveMembershipStatus = new List<String>{
            HelperSObject.MEMBERSHIP_STATUS_DRAFT,
            HelperSObject.MEMBERSHIP_STATUS_EXPIRED,
            HelperSObject.MEMBERSHIP_STATUS_ACTIVATED
        };
                    
        for(Contract membership:listMemberships)
        {
            Contact singleContact = mapId_Contact.get(membership.Contact__c);
            if(singleContact==NULL)
            {
                System.debug(LOGTAG+'updateContactClubFields(): mapId_Contact returned a NULL value membership='+
                    HelperSObject.json(membership, 'Id, Name, Status, Club_Name__c'));
                continue;
            }
                    
            if(listActiveMembershipStatus.contains(membership.Status))
            {
                singleContact = setContactStatus(membership.Club_Name__c, singleContact, TRUE);
            }
            else
            {
                singleContact = setContactStatus(membership.Club_Name__c, singleContact, FALSE); 
            }
            mapId_Contact.put(singleContact.Id, singleContact);
        }
        
        System.debug(LOGTAG+'mapId_Contact='+mapId_Contact);
        update mapId_Contact.values();
    }
    
    /**
     * Updates the contact "theContact" its field related to the Club "clubName" 
     * with the value on "status" membership.
     * The mappting is:
     * Club Name          | Contact Field Name        | Contact field API Name      
     * -------------------+---------------------------+-------------------------------
     * RE Club Brazil     | RE Club Brazil Member     | Real_Estate_BR_Club__c    
     * RE Club Latam      | RE Club Latam Member      | RE_Club_Latam_Member__c  
     * Infra Club LatAm   | Infra Club LatAm Member   | Infrastructure_BR_Club__c
     * Retail Club Brazil | Retail Club Brazil Member | Retail_Club_BR__c
     * RE Club India      | RE Club India Member      | India_Club_Member__c
     * RE Club Europe     | RE Club Europe Member     | Global_Club_Member__c   
     * RE Club Africa     | RE Club Africa Member     | RE_Club_Africa_Member__c    
     * Infra Club India   | Infra Club India Member   | Infra_Club_India_Member__c 
     * 
     */
    public static Contact setContactStatus(String clubName, Contact theContact, Boolean status)
    {
        if(clubName == HelperSObject.CLUB_INFRA_INDIA)
        {
            theContact.Infra_Club_India_Member__c = status; 
        }
        else if(clubName == HelperSObject.CLUB_INFRA_LATAM)
        {
            theContact.Infrastructure_BR_Club__c = status;
        }
        else if(clubName == HelperSObject.CLUB_RE_AFRICA)
        {
            theContact.RE_Club_Africa_Member__c  = status;
        }
        else if(clubName == HelperSObject.CLUB_RE_BRAZIL)
        {
            theContact.Real_Estate_BR_Club__c = status;                 
        }
        else if(clubName == HelperSObject.CLUB_RE_EUROPE)
        {
            theContact.Global_Club_Member__c = status; 
        }
        else if(clubName == HelperSObject.CLUB_GRI_GLOBAL_CLUB)
        {
            theContact.Global_Club_Member__c = status;                
        }
        else if(clubName == HelperSObject.CLUB_RE_INDIA)
        {
            theContact.India_Club_Member__c = status;
        }
        else if(clubName == HelperSObject.CLUB_RE_LATAM)
        {
            theContact.RE_Club_Latam_Member__c = status;
        }
        else if(clubName == HelperSObject.CLUB_RETAIL_BRAZIL)
        {
            theContact.Retail_Club_BR__c = status;
        }
        else 
        {
            System.debug(LOGTAG+'memberhip does not belong to any tracket club ClubName='+clubName);
        }
        return(theContact);
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Auto-Won Renewed-Memberships Opportunities
    ///////////////////////////////////////////////////////////////////////////
    
    /** **OLD METHOD**
    public static void autoWonRenewedMemberships_OLD(List<Contract> listMemberships)
    {
        System.debug(LOGTAG+'autoWonRenewedMemberships(): listMemberships='+HelperSObject.json(listMemberships,'Id, Name, Status, Membership_Level__c') );
        // Memberships that are active and have membership level diamond or platinum
        List<Contract> membershipsActivePd = 
            activatedPlatinumDiamondmemberships(listMemberships);
        System.debug(LOGTAG+'membershipsActivePd:'+HelperSObject.json(membershipsActivePd,'Id, Name, Status, Membership_Level__c'));
        // Map of memberships and contacts Ids
        Map<String, SObject> mapContactId_Membership =  
            HelperSObject.mapFromField(membershipsActivePd,  'Contact__c');
        Set<String> setContactsIds = mapContactId_Membership.keySet();
        if(setContactsIds.size()==0) return;
        Id recTypeEvents = HelperSObject.opportunity_recordTypeIdEvents();
        System.debug(LOGTAG+'setContactsIds:'+setContactsIds);
        System.debug(LOGTAG+'recTypeEvents='+recTypeEvents);
        
        // Opportunities related to the memberships
        List<Opportunity> relatedOpps = 
            [SELECT Id, Name, RecordTypeId, StageName,  Contact__c, Event__r.End_Date__c, Event__r.Start_Date__c, Event__r.Club__c 
             FROM Opportunity WHERE (Contact__c IN :setContactsIds) AND (RecordTypeId=:recTypeEvents) ];
        List<Opportunity> oppsAppByFinance = new List<Opportunity>();
        for(Opportunity opp : relatedOpps)
        {
            Contract membership = (Contract)mapContactId_Membership.get(opp.Contact__c);
            //System.debug(LOGTAG+'DELETE IT ON LOOP! membership='+ HelperSObject.json(membership, 'Id, Name, Status, Membership_Level__c') ) ;
            
            if((HelperSObject.date(opp.Event__r.End_Date__c)<=membership.EndDate) && 
               (HelperSObject.date(opp.Event__r.Start_Date__c)>=membership.StartDate))
            {
                //System.debug(LOGTAG+' DELETE IT ON LOOP! hot Opp!! >>> opp='+
                //    opp+' >>> Its Membership is-> membership='+membership);
                if((opp.StageName==HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE || opp.StageName==HelperSObject.OPPORTUNITY_STAGE_APPROVED) && 
                   (opp.Event__r.Club__c==membership.Club__c))
                {
                    opp.bypassValidationRules__c=True;
                    opp.StageName=HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE;
                    oppsAppByFinance.add(opp);
                }
            }
        }
        update oppsAppByFinance;
    }
    **/
    
    /**
     * This methods updates all Opportunities related to the Contact on the Membership
     * to "Approved by Finance" and the hidden custom field "bypassValidationRules__c" to 
     * true, if they fill the requirements. The requirements are:
     * - The Opportunity has just the product "Member Free"
     * - The Opportunity Stage is "Approved" or "Approved by Finance"
     * - The Membership level is Diamond, Platinum or related.
     * - It is a "Open Event" Opportunity event start and end date is contained on the
     *   membership start and end date.
     * - The Opportunity Event Date 
     * - The Event Club is the same from the Opportunity Club.
     * On the follow update, both are set on the same time, the Opportunity is set
     * to "Won".
     * Note: This method does not assumes that an user may have just one membership in 
     * one club. Although the usual case is that the Contract:Contact is a 1:1 
     * relationhip, it works for the n:1 case as well: a Contact have many 
     * memberships (Contract).
     */
    public static void autoWonRenewedMemberships(List<Contract> listMemberships) 
    {
        System.debug(LOGTAG+'autoWonRenewedMemberships(): listMemberships='+HelperSObject.json(listMemberships, 'Id, Name, Status, Membership_Level__c') );
        List <Contract> membershipsActivePd = activatedPlatinumDiamondmemberships(listMemberships);
        System.debug(LOGTAG+'membershipsActivePd:'+HelperSObject.json(membershipsActivePd, 'Id, Name, Status, Membership_Level__c'));
        Set <String> setContactsIds = HelperSObject.setFromFieldSafe(listMemberships, 'Contact__c');
        if (setContactsIds.size()==0) return;
        Id recTypeEvents = HelperSObject.opportunity_recordTypeIdEvents();
        System.debug(LOGTAG+'setContactsIds:'+setContactsIds);
        System.debug(LOGTAG+'recTypeEvents='+recTypeEvents);
        
        // Opportunities related to the memberships
        List <Opportunity> relatedOpps = [SELECT Id, Name, RecordTypeId, StageName, Contact__c, Event__r.End_Date__c, Event__r.Start_Date__c, Event__r.Club__c, Club__c, 
                                         (SELECT Id, ProductCode FROM OpportunityLineItems)
                                         FROM Opportunity WHERE(Contact__c IN :setContactsIds) AND (RecordTypeId = :recTypeEvents)];
        List <Opportunity> relatedOppsMemberFree = 
            HelperSObject.opportunity_filterByProductCode(relatedOpps, HelperSObject.PRODUCT_MEMBER_FREE);
        System.debug(LOGTAG+'relatedOppsMemberFree.size()='+relatedOppsMemberFree.size());

        List <Opportunity> oppsAppByFinance = new List <Opportunity> ();
        for (Opportunity opp:relatedOppsMemberFree) 
        {
            Contract membFinal = NULL;
            for (Contract membership: membershipsActivePd) 
            {
                if ((membership.Club__c == opp.Event__r.Club__c) &&
                    (membership.Contact__c == opp.Contact__c)) 
                {
                        membFinal = membership;
                        break;
                }
            }
            if (membFinal == NULL) continue;
            if ((HelperSObject.date(opp.Event__r.End_Date__c) <= membFinal.EndDate) && 
                (HelperSObject.date(opp.Event__r.Start_Date__c) >= membFinal.StartDate) ) 
            {
                if ((opp.StageName == HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE || opp.StageName == HelperSObject.OPPORTUNITY_STAGE_APPROVED) &&
                    (opp.Event__r.Club__c == membFinal.Club__c)) 
                {
                        opp.bypassValidationRules__c = True;
                        opp.StageName = HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE;
                        oppsAppByFinance.add(opp);
                    }
            }
        }
        System.debug(LOGTAG+'UPDATE Opportunities: oppsAppByFinance.size()='+oppsAppByFinance.size());
        update oppsAppByFinance;
    }
    
    // this methods filter all Contracts(Memberships) that have the status Activated and have
    // membership level of platinum,  diamond or related
    private static List<Contract> activatedPlatinumDiamondmemberships(
        List<Contract> listMemberships)
    {
        Set<String> listDiamondPlatinumMemberships = HelperSObject.setDiamondPlatinumMemberships();
        List<Contract> membershipsActive = new List<Contract>();
        for(Contract memb:listMemberships)
        {
            if(listDiamondPlatinumMemberships.contains(memb.Membership_Level__c) &&
               memb.Status==HelperSObject.MEMBERSHIP_STATUS_ACTIVATED)
            {
                membershipsActive.add(memb);
            }
        }
        return membershipsActive;
    }  
}