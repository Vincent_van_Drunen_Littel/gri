/******************************************************************************* 
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsável por criar/excluir registro Event Attendee conforme
* informações e status da Oportunidade e Produto da oportunidade.
*
* NAME: OpportunityCriaEventAttendee.cls
* AUTHOR: LMdO                                                 DATE: 02/06/2016
* 31/03/2017    CFA     Code changes made by Darren on 23,24 May 2017. 
*                       Made the code a little easier to understand also started 
*                       putting debugs in so when the code runs, it can write 
*                       out what it doing to a log. As the code as the curret 
*                       time dose not do this. 
*                       Section / Mod done
*                       The code was altered so that it will add the opertunaty
*                       line was added to the new Event Attendee.
* 06/06/2017    CFA     Changes made so that it would set the Event Position on 
*                       the Event Attendees based on the products added to the 
*                       Opportunity. 
* 16/0/2018     AndersonP   * Now the class creates Events Attendees for 
*                             Smartus Events as well. 
*                           * Commented logs inside loops, because this practice
*                             create a mess in the logs, and also increase the
*                             chance of excedding the log limmits per operation
*******************************************************************************/

public with sharing class OpportunityCriaEventAttendee
{
    
    private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
    private static Id REC_OPP_SMARTUS_EVENTS = RecordTypeMemory.getRecType('Opportunity', 'Smartus_Events');
    public static List<Event_Attendee__c> lstToUpdateEventAtt;
    public static Map<String,String> Products = new  Map<String,String>();
    
    public static void buildList(Map<String,Product2> productMap){
        System.debug('#### Building Dynamic map ####');
        /*
Values need to be:
Networking Dinner = Networking Dinner
Networking Dinner Club Member Free = Networking Dinner
Networking Dinner (pay-per-use Club) = Networking Dinner
Co-Chair Sponsor – Free = Co-Chair Sponsor**
Delegate (Law Firm) = Delegate -Advisor
Delegate Sponsor – Free = Delegate Sponsor
//GRI State High Member Free = Member Free
Group Rate Delegate = Delegate
//Group Rate – Forum = Delegate
Media Partner – Free = Media Partner
Retailers – Members = Member Free
Networking Dinner:P-000798 ----->   Networking Dinner
Networking Dinner Club Member Free:P-000804 ----->  Networking Dinner
Networking Dinner (pay-per-use Club):P-000799 ----->    Networking Dinner
Co-Chair Sponsor – Free: P-001319 or P-001588 (Live     P-001319)  -------> Co-Chair - Sponsor
Delegate (Law Firm):P-001601    -------> Delegate - Advisor
Delegate Sponsor - Free: P-001320 or P-001587 (Live     P-001320) -------> Delegate - Sponsor
Group Rate Delegate:P-000035 ------->Delegate
Media Partner - Free:P-000114 -------> Media Partner
Retailers – Members:P-000193 -------> Member Free
GRI State High Member Free:P-001259 -------> Member Free
Group Rate – Forum: P-002241 ------->Delegate
Co-Chair Sponsor - Free:P-001319
Delegate - Advisor:P-001488
Member Free:P-000049
Delegate:P-000033
Tech Club Member: P-004593
Co-chair - Hospitality: P-005386
Co-chair - Residential: P-005387
Delegate - Hospitality: P-005382
Delegate - Residential: P-005383


*/
        //Changed to make the build more dynamic then static. 
        String BuildProductName='';
        
        for(String s: productMap.keyset())
        {
            //System.debug('Map S' +s);
            Product2 getProduct = productMap.get(s);
            //System.debug(getProduct);
            BuildProductName=getProduct.Name;
            //System.debug('BuildProductName:' +BuildProductName);
            if (s=='P-000798' || s=='P-000804' || s=='P-000799')
                Products.put(BuildProductName,'Networking Dinner');
          
            if(s=='P-001319' || s=='P-001588' || s=='P-005386' ||  s=='P-000053' ||s=='P-000034'||s=='P-005387'||s=='P-003918'||s=='P-003916')
                Products.put(BuildProductName,'Co-Chair');
                      
            if( s=='P-000033'|| s=='P-000035'|| s=='P-000489'|| s=='P-001320'|| s=='P-001487'|| s=='P-001488'|| s=='P-001601'|| s=='P-002241'|| s=='P-003385'|| s=='P-003386'|| s=='P-003387'|| s=='P-003915'|| s=='P-003917'|| s=='P-005382'|| s=='P-005383'|| s=='P-005923'|| s=='P-005924'||s=='P-005938'|| s=='P-005939')
                Products.put(BuildProductName,'Delegate');  
                
                
             if (s=='P-003073'|| s=='P-003074'|| s=='P-003075'|| s=='P-004200'|| s=='P-000044'|| s=='P-000045'|| s=='P-003078'|| s=='P-000046'|| s=='P-000051')
                Products.put(BuildProductName,'Guest Free');     
                
              
                         
            if(s=='P-004070'|| s=='P-004071'|| s=='P-001259'|| s=='P-000059'|| s=='P-000049'|| s=='P-000064'|| s=='P-004072'|| s=='P-001258'|| s=='P-004089'|| s=='P-005534')
                Products.put(BuildProductName,'Member Free');
                   
                
        }
        
/*Products.put('Networking Dinner','Networking Dinner');
Products.put('Networking Dinner Club Member Free','Networking Dinner');
Products.put('Networking Dinner (pay-per-use Club)','Networking Dinner');
Products.put('Co-Chair Sponsor Free','Co-Chair Sponsor');
Products.put('Delegate (Law Firm)','Delegate -Advisor');
Products.put('Delegate Sponsor - Free','Delegate Sponsor');
Products.put('GRI State High Member Free','Member Free');
Products.put('Group Rate Delegate','Delegate');
Products.put('Group Rate - Forum','Delegate');
Products.put('Media Partner - Free','Media Partner');
Products.put('Retailers - Members','Member Free');
*/
        //System.debug('_____________________________________________________________________________________________________________________________________________________');
        System.debug(Products);
        //System.debug('_____________________________________________________________________________________________________________________________________________________');
    }
    
    public static void execute()
    {
        
        System.debug('########## OpportunityCriaEventAttendee Executing ##########');
        TriggerUtils.assertTrigger();
        Set<Id> setIdOpp = new set<Id>();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        Set<String> setStageName = new Set<String>{'Approved', 'Approved by Finance', 'Won', 'Cancelled'};
            Set<String> setStageNameToDel = new Set<String>{'Suspect', 'Prospect', 'Information Sent', 'Negotiation', 'Closing'};
                
                // full list of GRI products
                List<Product2> prodFullList = [SELECT SystemModstamp, Product_number__c, ProductCode, Name, LastViewedDate, LastReferencedDate, LastModifiedDate,
                                               LastModifiedById, IsDeleted, IsActive, Id, Family,Event_Attendee__c, Description, CurrencyIsoCode, CreatedDate, CreatedById FROM Product2];
        
        Map<String,Product2> productNameWithCode = new Map<string,Product2>();
        for(Product2 prods : prodFullList ){
            productNameWithCode.put(prods.ProductCode,prods);
        }
        //system.debug('**** Event Position Building list ****');
        buildList(productNameWithCode); //Build the list of changes to the pick list!
        for(Opportunity opp : (List<Opportunity>) Trigger.new)
        {   
            //Para criar Event Attendee
            if(setStageName.contains(opp.StageName) && 
               (opp.RecordTypeId==REC_OPP_OPEN_EVENTS || opp.RecordTypeId==REC_OPP_SMARTUS_EVENTS))
            {
                mapOpp.put(opp.Id, opp);
            }
            if(setStageNameToDel.contains(opp.StageName) && 
               (opp.RecordTypeId==REC_OPP_OPEN_EVENTS || opp.RecordTypeId==REC_OPP_SMARTUS_EVENTS))
            {
                //Para excluir Event Attendee
                //criar um novo objeto
                mapOpp.put(opp.Id, opp);
                setIdOpp.add(opp.Id);
            }
        }
        
        List<Event_Attendee__c> lstEventAtt = new List<Event_Attendee__c>();
        if(!mapOpp.isEmpty())
        { 
            Map<Id, Opportunity> mapControllerDinner = new Map<Id, Opportunity>();
            for(Opportunity iOpp : mapOpp.values())
            {
                Opportunity lOpp = new Opportunity();
                lOpp.Id = iOpp.Id;
                lOpp.Controller_Dinner__c = false;
                mapControllerDinner.put(lOpp.Id, lOpp);
            }
            //Pega todos os Produtos da oportunidade.
            Map<Id, Integer> mapQtOppLine = new Map<Id, Integer>();
            Map<id, String> mapQtOppLinT = new Map<id, String>(); // Add a second map to hold the product name  
            //Change the code from:
            //for(OpportunityLineItem oppLineIt : [SELECT Id, Quantity, Name, OpportunityId, Product2.Event_Attendee__c, Opportunity.Controller_Dinner__c
            //  FROM OpportunityLineItem 
            //  WHERE OpportunityId =: mapOpp.keySet()])
            //  To Add to a list to make processing easier laster on!
            List<OpportunityLineItem> opItemListGet = [SELECT Id, Quantity, Name,ProdName__c, OpportunityId, Product2.Event_Attendee__c, Opportunity.Controller_Dinner__c,ProductCode 
                                                       FROM OpportunityLineItem 
                                                       WHERE OpportunityId =: mapOpp.keySet()];
            List<String> opItemList = new List<String>();
            Integer oldi=1;
            for(OpportunityLineItem oppLineIt : opItemListGet)
            {
                if(oppLineIt.Product2.Event_Attendee__c)
                {
                    Opportunity lOpp = mapControllerDinner.get(oppLineIt.OpportunityId);
                    lOpp.Controller_Dinner__c = true;
                    continue;
                }
                
                Integer quantTot = mapQtOppLine.get(oppLineIt.OpportunityId);
                //System.debug('quantTot First get:' +quantTot);
                if(quantTot == NULL)
                {
                    quantTot = oppLineIt.Quantity.intValue();
                    //System.debug('quantTot Org:' +quantTot);
                    mapQtOppLine.put(oppLineIt.OpportunityId, quantTot);
                    mapQtOppLinT.put(oppLineIt.OpportunityId,oppLineIt.ProdName__c);
                }
                else
                {
                    quantTot += oppLineIt.Quantity.intValue();
                    System.debug('No Counter:' +oppLineIt.Quantity.intValue() + ' Name:' +oppLineIt.ProdName__c);
                    System.debug('quantTot New:' +quantTot + ' Name:' +oppLineIt.ProdName__c);
                    mapQtOppLine.remove(oppLineIt.OpportunityId);
                    mapQtOppLinT.remove(oppLineIt.OpportunityId);
                    mapQtOppLine.put(oppLineIt.OpportunityId, quantTot);
                    mapQtOppLinT.put(oppLineIt.OpportunityId,oppLineIt.ProdName__c);
                }
                System.debug('Product Name:' + oppLineIt.ProdName__c);
                for(Integer i = oldi; i <= quantTot; i++){
                    opItemList.add(oppLineIt.ProdName__c);
                    //productNameWithCode.put(oppLineIt.ProdName__c,oppLineIt.ProductCode);
                }
                oldi =quantTot;
                oldi++;
                //System.debug('oldi value:' +oldi);
            }
            if(mapQtOppLine.isEmpty()) return;
            
            //Pega todos os Eventes Attendee da oportunidade.
            Map<Id, List<Id>> mapQtEventAtt = new Map<Id, List<Id>>();
            List<Event_Attendee__c> lstOfEventAtt = new List<Event_Attendee__c>();
            List<Event_Attendee__c> getAttList =[SELECT Id, Event_Attendees_Status__c, Opportunity__c 
                                                 FROM Event_Attendee__c 
                                                 WHERE Opportunity__c =: mapOpp.keySet()];
            //System.debug('getAttList Count:' + getAttList.size());
            //Changed to put the search crit in to a list to make understanding the code a little
            //bit easier. 
            //The following is the oroginal line of code. 
            //  for(Event_Attendee__c eventAtt : [SELECT Id, Event_Attendees_Status__c, Opportunity__c 
            //                               FROM Event_Attendee__c 
            //                               WHERE Opportunity__c =: mapOpp.keySet()])
            
            for(Event_Attendee__c eventAtt : getAttList)                                 
            {
                List<Id> lstEventA = mapQtEventAtt.get(eventAtt.Opportunity__c);
                if(lstEventA == NULL)
                {
                    lstEventA = new List<Id>();
                    
                    mapQtEventAtt.put(eventAtt.Opportunity__c, lstEventA);
                }
                lstEventA.add(eventAtt.Id);
                lstOfEventAtt.add(eventAtt);
            }
            
            //System.debug('Attendee list count:' + lstOfEventAtt.size());
            lstToUpdateEventAtt = new List<Event_Attendee__c>(); 
            if(!lstOfEventAtt.isEmpty())
            {
                System.debug('Attendee list not empty!');
                for(Event_Attendee__c iEventAtt : lstOfEventAtt)
                {   
                    Opportunity lOpp = mapOpp.get(iEventAtt.Opportunity__c);
                    Opportunity lOppCheck = mapControllerDinner.get(iEventAtt.Opportunity__c);
                    if(lOpp == NULL) continue;
                    if(lOpp.Id == iEventAtt.Opportunity__c && lOppCheck.Id == iEventAtt.Opportunity__c)
                    {
                        Event_Attendee__c objEvent = new Event_Attendee__c(Id = iEventAtt.Id);
                        objEvent.VIP_Dinner_Invitee__c = lOppCheck.Controller_Dinner__c;
                        //objEvent.Session_Position__c ='This is a test!';
                        
                        objEvent.Event_Attendees_Status__c =(lOpp.StageName == 'Approved') ? 'Processing' : 
                        (lOpp.StageName == 'Approved by Finance')   ? 'Confirmed' :
                        (lOpp.StageName == 'Won') ? 'Registered' : 
                        (lOpp.StageName == 'Cancelled') ? 'Not Attending' : 'Cancelled';
                        lstToUpdateEventAtt.add(objEvent);
                    }
                }
                if(!lstToUpdateEventAtt.isEmpty()) Database.update(lstToUpdateEventAtt);
            }
            
            //Comparação e inserção de Evente Attendee.
            //System.debug('mapOpp Size:' +mapOpp.size());
            for(Opportunity oppUp : mapOpp.values())
            {
                Integer quantOppLine = mapQtOppLine.get(oppUp.Id);
                String Pname=mapQtOppLinT.get(oppUp.Id);
                Integer quantEventAtt = 0;
                Integer quant = 0;
                if(!mapQtEventAtt.isEmpty())
                {
                    List<Id> lstIdEventAtt = mapQtEventAtt.get(oppUp.Id);
                    if(lstIdEventAtt == NULL || lstIdEventAtt.isEmpty()) continue;
                    quantEventAtt = lstIdEventAtt.size();
                }
                //System.debug('Got to quantOppLine or quantEventAtt check!');
                //System.debug('quantOppLine:' + quantOppLine);
                //System.debug('quantEventAtt:' +quantEventAtt);
                if(quantOppLine==NULL || quantEventAtt==NULL) continue;
                //System.debug('Passing to line processsor!');
                quant = quantOppLine - quantEventAtt;
                //System.debug('~~~~ quant:' + quant + '~~~~');
                if(quant <= 0) continue;
                Opportunity lOppCheck = mapControllerDinner.get(oppUp.Id);
                //System.debug('opItemList Count:'+opItemList.size());
                //System.debug('opItemList:' +opItemList);
                /*This is the section where the product name needs adding......*/
                for(Integer i = 0; i < quant; i++)
                {
                    if (i > quant ) continue;
                    //System.debug('Loop Value:' +i);
                    //System.debug('Building New Event Attendee!');
                    Event_Attendee__c objEvent = new Event_Attendee__c();
                    objEvent.Opportunity__c = oppUp.Id;
                    objEvent.Account__c = oppUp.AccountId;
                    objEvent.Contact__c = oppUp.Contact__c;
                    objEvent.Event__c = oppUp.Event__c;
                    objEvent.VIP_Dinner_Invitee__c = lOppCheck.Controller_Dinner__c;
                    Integer loopCheckVale= i+1;
                    //System.debug('Loop Value:' + loopCheckVale +'    quantOppLine:' + quantOppLine);
                    string tempString=(string)opItemList[i];
                    string findString='';
                    //System.debug('tempString:' + tempString);
                    //Get the name to set on the "Event Position"
                    try 
                    {
                        //System.debug('Getting item from MAP!');
                        //System.debug('Map size:' +Products.size());
                        findString = Products.get(tempString);
                        //System.debug('Map value retreved!');
                        //System.debug('findString:' + findString);
                        
                        if (findString !=NULL)
                            tempString=findString;
                        
                    }
                    catch (Exception e) 
                    {
                        //System.debug('Error getting value from Map!');
                        tempString ='';
                    }           
                    //System.debug('**** Event Position tempString:' + tempString + ' ****');
                    try 
                    {
                        if (tempString.length() >=1)
                            objEvent.Session_Position__c =tempString;   
                    }
                    catch (Exception e) 
                    {
                        //objEvent.Session_Position__c ='';
                    }   
                    
                    objEvent.Event_Attendees_Status__c = (oppUp.StageName == 'Approved') ? 'Processing' : 
                    (oppUp.StageName == 'Approved by Finance')  ? 'Confirmed' :
                    (oppUp.StageName == 'Won') ? 'Registered' : 
                    (oppUp.StageName == 'Cancelled') ? 'Not Attending' : 'Cancelled';
                    if(oppUp.StageName == 'Approved' || oppUp.StageName == 'Approved by Finance') lstEventAtt.add(objEvent);
                }//endfor
            }//endfor
        }
        
        List<Event_Attendee__c> lstDelEventAtt = new List<Event_Attendee__c>();
        if(!setIdOpp.isEmpty())
        {
            //Added by Darren to check to see what opertunatys where been cheked!
            //System.debug('Getting Op IDS');
            //for(id ids : setIdOpp ){
            //  System.debug(ids);
            //}
            //System.debug('Getting Op IDS Completed');
            //End Darrens write to log code!
            for(Event_Attendee__c eventAtt : [SELECT Id FROM Event_Attendee__c WHERE Opportunity__c =: setIdOpp])
            {
                //System.debug('Adding eventline contact for delete!'); //Added to know the line was been added to delete by Darren
                lstDelEventAtt.add(eventAtt);
            }
        }
        System.debug('Executing delete! no count:' + lstDelEventAtt.size());
        if(!lstDelEventAtt.isEmpty()) Database.delete(lstDelEventAtt);
        System.debug('Executing insert no count:' + lstEventAtt.size());
        if(!lstEventAtt.isEmpty()) Database.insert(lstEventAtt);
        System.debug('########## OpportunityCriaEventAttendee Executing Completed! ##########');
    }
}