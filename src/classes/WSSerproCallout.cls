/**
 * @author Renato Matheus Simião
 * @date 2018-10-13
 * @description Essa classe provem metodos para consumir o serviço da classe WSSerpro e efetuar DML em SObject
 */
public with sharing class WSSerproCallout {
	
	@future(callout=true)
	public static void fUpdateContact(Id pContactId, String pCNPJ) {

        Contact contact = [SELECT Id, Account.Company_s_Legal_Name__c, Account.Name,
        						  OtherCountry, OtherPostalCode, OtherState, OtherCity, OtherStreet
        					 FROM Contact
        					WHERE Id = :pContactId];

		WSSerpro.WrapperConsulta response = WSSerpro.consultarCNPJ(pCNPJ);

       	if (response.status == 'error') {

            System.debug('Erro ao consumir o serviço da Receita Federal: '+response.message);
        	contact.CNPJStatus__c = 'Error';
        	contact.SerproMessage__c = response.message;
        	update contact;
        	return;
        }

        WSSerpro.ConsultaCNPJ ret = response.ConsultaCNPJ;


        Account acc = contact.Account;
        //acc.Name = String.isEmpty(ret.nome_fantasia) ? ret.nome_empresarial : ret.nome_fantasia;
        acc.Company_s_Legal_Name__c = ret.nome_empresarial;

        contact.CNPJStatus__c = ret.situacao_cadastral.descricao;
        contact.SerproMessage__c = '';
        contact.OtherPostalCode = ret.endereco.cep;
        contact.OtherCountry = 'Brazil';
        contact.OtherState = ret.endereco.uf;
        contact.OtherCity = ret.endereco.municipio;
        contact.OtherStreet = ret.endereco.logradouro;
        if (String.isNotEmpty(ret.endereco.numero)) contact.OtherStreet += ', '+ ret.endereco.numero;
        if (String.isNotEmpty(ret.endereco.complemento)) contact.OtherStreet += ' - '+ ret.endereco.complemento;
        if (String.isNotEmpty(ret.endereco.bairro)) contact.OtherStreet += ' - '+ ret.endereco.bairro;

        update acc;
        update contact;
	}

	@future(callout=true)
	public static void fUpdateOpportunity(Id pOpportunityId, String pCNPJ) {

        Opportunity opp = [SELECT Id, Account.Company_s_Legal_Name__c, Account.Name,
        						 Contact__r.OtherCountry, Contact__r.OtherPostalCode, Contact__r.OtherState,
        						 Contact__r.OtherCity, Contact__r.OtherStreet,
        						 Billing_Postcode__c, Billing_Country__c, Billing_State__c, Billing_City__c, Billing_Address__c
        					FROM Opportunity
        				   WHERE Id = :pOpportunityId];

		WSSerpro.WrapperConsulta response = WSSerpro.consultarCNPJ(pCNPJ);
	              
       	if (response.status == 'error') {
            System.debug('Erro ao consumir o serviço da Receita Federal: '+response.message);
        	opp.CNPJStatus__c = 'Error';
        	opp.SerproMessage__c = response.message;
        	update opp;
        	return;
        }

        WSSerpro.ConsultaCNPJ ret = response.ConsultaCNPJ;


        Account acc = opp.Account;
        //acc.Name = String.isEmpty(ret.nome_fantasia) ? ret.nome_empresarial : ret.nome_fantasia;
        acc.Company_s_Legal_Name__c = ret.nome_empresarial;

        Contact contact = opp.Contact__r;
        contact.OtherPostalCode = ret.endereco.cep;
        contact.OtherCountry = 'Brazil';
        contact.OtherState = ret.endereco.uf;
        contact.OtherCity = ret.endereco.municipio;
        contact.OtherStreet = ret.endereco.logradouro;
        if (String.isNotEmpty(ret.endereco.numero)) contact.OtherStreet += ', '+ ret.endereco.numero;
        if (String.isNotEmpty(ret.endereco.complemento)) contact.OtherStreet += ' - '+ ret.endereco.complemento;
        if (String.isNotEmpty(ret.endereco.bairro)) contact.OtherStreet += ' - '+ ret.endereco.bairro;

        opp.CNPJStatus__c = ret.situacao_cadastral.descricao;
        opp.SerproMessage__c = '';
        opp.Billing_Postcode__c = contact.OtherPostalCode;
        opp.Billing_Country__c = contact.OtherCountry;
        opp.Billing_State__c = contact.OtherState;
        opp.Billing_City__c = contact.OtherCity;
        opp.Billing_Address__c = contact.OtherStreet;

        update acc;
        update contact;
        update opp;
	}
}