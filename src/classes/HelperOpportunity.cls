/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * A collection of methods and constants for the Opportunity SObject
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * @testClasses HelperSObject_Test  
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */

public class HelperOpportunity extends HelperSObject
{
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //  Approval Committee
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public static final String COMMITTEE_REJECTED = 'Rejected';
    public static final String COMMITTEE_ACCEPTED = 'Approved';
    public static final String COMMITTEE_PROCESSING = 'Processing';

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Opportunity stages
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public static final String STAGE_PROSPECT = 'Prospect';
    public static final String STAGE_ON_HOLD = 'On Hold';
    public static final String STAGE_APPROVED = 'Approved';
    public static final String STAGE_APPROVED_BY_FINANCE = 'Approved by Finance';
    public static final String STAGE_WON = 'Won';

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Record Types
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public static final String RECORD_TYPE_EVENTS = 'Events';
    public static final String RECORD_TYPE_MEMBERSHIP = 'Membership';
    public static final String RECORD_TYPE_MAGAZINE = 'Magazine';
    public static final String RECORD_TYPE_SPEX = 'SPEX';

    public static Id recordTypeIdEvents()
    {
        return HelperRecordType.get('Opportunity', 'Events');
    }
    public static Id recordTypeIdMembership()
    {
        return HelperRecordType.get('Opportunity', 'Membership');
    }
    public static Id recordTypeIdMagazine()
    {
        return HelperRecordType.get('Opportunity', 'Magazine Subscription');
    }
    public static Id recordTypeIdSPEX()
    {
        return HelperRecordType.get('Opportunity', 'SPEX');
    }


    //public static final Id RECORD_TYPE_EVENTS_ID = HelperRecordType.get('Opportunity', 'Events');
    //public static final Id RECORD_TYPE_MEMBERSHIP_ID = HelperRecordType.get('Opportunity', 'Membership');
    //public static final Id RECORD_TYPE_MAGAZINE_ID = HelperRecordType.get('Opportunity', 'Magazine');
    //public static final Id RECORD_TYPE_SPEX_ID = HelperRecordType.get('Opportunity', 'SPEX');
	//public static final String RECORD_TYPE_EVENTS_ID = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
	//public static final String RECORD_TYPE_MEMBERSHIP_ID = RecordTypeMemory.getRecType( 'Opportunity', 'Membership');

}