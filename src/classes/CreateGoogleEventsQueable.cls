public class CreateGoogleEventsQueable implements Queueable, Database.AllowsCallouts {
public List<Calendar_Invite__c> calInviteObjLst;
  public CreateGoogleEventsQueable(List<Calendar_Invite__c> calInviteObjLstVal) {
        this.calInviteObjLst = calInviteObjLstVal;
    } 
    public void execute(QueueableContext context){
        List<Calendar_Invite__c> calInviteUpdLst = new List<Calendar_Invite__c>();
            
            List<Calendar_Invite__c> calInviteInstUpdLst = new List<Calendar_Invite__c>();                                                                   
            if(calInviteObjLst != null && calInviteObjLst.size()>0 ){
                for(Calendar_Invite__c calObj:calInviteObjLst){
                        
                    calObj.IsInviteesLimitReached__c = true;
                    calInviteUpdLst.add(calObj);
                    
                    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
                    calNewObj.Event__c = calObj.Event__c;
                    calNewObj.Description__c = calObj.Description__c;
                    calNewObj.End_Date__c = calObj.End_Date__c;
                    calNewObj.Invite_Type__c = calObj.Invite_Type__c;
                    calNewObj.See_Guest_List__c = calObj.See_Guest_List__c;
                    calNewObj.Send_Notification__c = calObj.Send_Notification__c;
                    calNewObj.Start_Date__c = calObj.Start_Date__c;
                    calNewObj.EventTimeZone__c = calObj.EventTimeZone__c;
                    calNewObj.Venue__c = calObj.Venue__c;
                    calNewObj.Visibility__c =  calObj.Visibility__c;
                    calNewObj.ParentCalendarInvite__c = calObj.Id;
                    calInviteInstUpdLst.add(calNewObj);
                }
            }
            system.debug('@@calInviteInstUpdLst'+calInviteInstUpdLst);
            
            if(calInviteUpdLst.size()>0){
                update calInviteUpdLst;
            }
            if(calInviteInstUpdLst.size()>0){
                //RecursionCls.isBatchAlreadyRun = false;
                if(!Test.isRunningTest())
                	insert calInviteInstUpdLst;
            }
    
    
    }
    
}