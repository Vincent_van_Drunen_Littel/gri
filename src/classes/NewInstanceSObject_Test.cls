@isTest (SeeAllData = true)
public class NewInstanceSObject_Test {
    
    private static Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
    
    @isTest  private static void test()
    {
        String AccName = 'Test acc';
        String clubname = 'Amendobobos club';
        String FirstName = 'first-test';
        String LastName = 'last-test';

        Account testAccount1 = NewInstanceSObject.createAccount();
        Account testAccount2 = NewInstanceSObject.createAccount(AccName);
        Database.upsert(testAccount1);
        Database.upsert(testAccount2);
  
        System.debug('testAccount2.Id='+testAccount2.Id);
        Contact testCc1 = NewInstanceSObject.createContact(testAccount2.Id);
        Contact testCc2 = NewInstanceSObject.createContact(testAccount2.Id, FirstName, LastName);
       database.upsert(testCc1);
        database.upsert(testCc2);
        
        Club__c testClub1 = NewInstanceSObject.createClub();
        Club__c testClub2 = NewInstanceSObject.createClub(clubname);
        database.upsert(testClub1);
        database.upsert(testClub2);
        
        
        Opportunity testOpp1 = NewInstanceSObject.createOpportunityMembership( 
            testAccount2,  testCc2, testClub2, date.newInstance(2018, 5, 1));
        Product2 testProduct = NewInstanceSObject.createProduct();
       // Id  stdPricebook = NewInstanceSObject.getStandardPricebookId();
        //PricebookEntry newPricebook = NewInstanceSObject.pricebookEntry( stdPricebook, testProduct.Id );        
 
        database.upsert(testOpp1);
        database.upsert(testProduct);
        //database.upsert(newPricebook);


        
        List<Id> accListId = new List<id>{testAccount1.Id, testAccount2.Id};
        List<Account> lacc = [SELECT Id,Name FROM Account WHERE ( Id IN :accListId )];
        System.assertEquals(2, lacc.size(), 
                            'Account list error, inserted and retrieved accounts dont match lacc='+lacc);
        
        List<Id> clubListId = new List<id>{testClub1.Id, testClub2.Id};
        List<Club__c> lclub = [SELECT Id,Name FROM Club__c WHERE ( Id IN :clubListId )];
        System.assertEquals(2, lclub.size(), 
                            'Club__c list error, inserted and retrieved accounts dont match lclub='+lclub);        

        List<Id> ccListId = new List<id>{testCc1.Id, testCc2.Id};
        List<Contact> lcc = [SELECT Id,Name FROM Contact WHERE ( Id IN :ccListId )];
        System.assertEquals(2, lcc.size(), 
                            'Contact list error, inserted and retrieved accounts dont match lcc='+lcc);  

       /*
        List<Id> lOppId = new List<id>{testOpp1.Id};
        List<Opportunity> lopp = [SELECT Id,Name FROM Opportunity WHERE ( Id IN :lOppId )];
        System.assertEquals(1, lopp.size(), 
                            'Contact list error, inserted and retrieved accounts dont match lopp='+lopp); 
        
        List<Id> prodListId = new List<id>{testProduct.Id};
        List<Product2> lprod = [SELECT Id,Name FROM Product2 WHERE ( Id IN :prodListId )];
        System.assertEquals(1, lprod.size(), 
                            'Contact list error, inserted and retrieved accounts dont match lcc='+lprod); 
        
        List<Id> pbListId = new List<id>{testCc1.Id, testCc2.Id};
        List<PricebookEntry> lpb = [SELECT Id,Name FROM PricebookEntry WHERE ( Id IN :pbListId )];
        System.assertEquals(2, lclub.size(), 
                            'Contact list error, inserted and retrieved accounts dont match lpb='+lpb);         
    */
        //test.startTest();
        //test.stopTest();
    }
    
}