/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 *  Class responsible for receiving the request from an external site.
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 02/02/19  EricB  First version.
 *
 */

@RestResource(urlMapping='/club/*')
global with sharing class ClubREST extends ServiceRestBase{

    private static final ClubREST instance = new ClubREST();
    private static ClubFacade facade = ClubFacade.getInstance();

    global ClubREST(){
        super.setServiceRestBase('ClubService');
    }

    @HttpGet
    global static void processGet(){
        instance.processService('GET');
    }

    global override void processService(String method) {
        try{
            if(method == 'GET'){

                RestRequest req = RestContext.request;
                String endpoint = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

                if(endpoint == 'find-by-id'){

                    super.setServiceRestBaseName('FindById');

                    // get params
                    Map<String,String> mapRequestParams = req.params;
                    String clubId = mapRequestParams.get('club-id');

                    SiteTO.ClubTO clubTO = facade.findClubById(Id.valueOf(clubId));
                    this.returnSuccess(JSON.serialize(clubTO, true),200);

                }else if(endpoint == 'find-sponsors-by-club-id'){

                    super.setServiceRestBaseName('FindSponsorsByClubId');

                    // get params
                    Map<String,String> mapRequestParams = req.params;
                    String clubId = mapRequestParams.get('club-id');

                    SiteTO.ClubTO clubTO = facade.findClubSponsorByClubId(Id.valueOf(clubId));
                    this.returnSuccess(JSON.serialize(clubTO, true),200);

                }


            }
        }catch (CustomException e){
            if(e.statusCode != null){
                this.returnError(e.getMessage(),e.statusCode);
            }else{
                this.returnError(e.getMessage());
            }
        }

    }
}