/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsavel pela cobertura do controller FileUploadController
*
*
* NAME: FileUploadControllerTest.cls
* AUTHOR: JFS                                                 DATE: 21/09/2016
*******************************************************************************/
@isTest
private class FileUploadControllerTest {
	
	private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
	private static Photo_Name__c rename;
	static
	{
	  rename = new Photo_Name__c();
	  rename.Name = 'PhotoName';
	  rename.separator__c = '_';
	  rename.Extension__c = '.jpg';
	  
	  insert rename;
	}

  static testMethod void TestSucesso() {
        
    Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
		Database.insert(conta);

		Contact contato = SObjectInstance.createContato(conta.Id);
		Database.insert(contato);
		
		PageReference pageRef = Page.FileUpload;
    Test.setCurrentPage(pageRef);
    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(contato);
    System.currentPageReference().getParameters().put('id', conta.Id);
        
		FileUploadController controller = new FileUploadController(new ApexPages.StandardController(contato));
		
		controller.previsao = 'Original Photo';
		controller.document.body = Blob.valueOf('teste');
    controller.document.Name = 'Teste';
		controller.upload();
		
		controller.previsao = 'Original Logo';
		controller.document.Body = Blob.valueOf('testeMarketing');
		controller.document.Name = 'Teste1';
		controller.upload();
		
		controller.previsao = 'Marketing Edited Photo';
		controller.document.Body = Blob.valueOf('testeMarketing');
		controller.document.Name = 'Teste2';
		controller.upload();	
		
		controller.previsao = 'Marketing Edited Logo';
		controller.document.Body = Blob.valueOf('testeMarketing');
		controller.document.Name = 'Teste3';
		controller.upload();			
  }
    
  static testMethod void TestErro() {
        
    Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
		Database.insert(conta);

		Contact contato = SObjectInstance.createContato(conta.Id);
		Database.insert(contato);
		
		PageReference pageRef = Page.FileUpload;
    Test.setCurrentPage(pageRef);
    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(contato);
    System.currentPageReference().getParameters().put('id', conta.Id);
        
		FileUploadController controller = new FileUploadController(new ApexPages.StandardController(contato));
    //erro sem previsao
		controller.upload();
		//erro sem documento
		controller.previsao = 'Original Photo';
    controller.upload();		
        
  }
}