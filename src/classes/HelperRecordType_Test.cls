/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * RecordType helper test class
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */
@IsTest
public class HelperRecordType_Test {

	@IsTest
	public static void test()
	{
		System.assertNotEquals(NULL, HelperRecordType.get('Opportunity', 'Events'), 'HelperRecordType returned NULL, wtf??');
		try
		{
			System.assertNotEquals(NULL, HelperRecordType.get('Opportunity', 'Dragonite'), 'HelperRecordType returned NULL, wtf??');
		}
		catch(HelperRecordType.HelperRecordTypeException e)
		{
			System.assertNotEquals('', e.getMessage(), 'HelperRecordType exception dont make sense, it is empty');
		}
	}
}