/**
 * Created by Eric Bueno on 24/02/2019.
 */

@IsTest
public with sharing class CampaignMemberTriggerHandlerTest {

    @TestSetup
    static void setup(){

        TestDataFactory.init();
        // Cria os Leads
        List<Lead> listLead = TestDataFactory.createLeadsWithDefaultCompany(2);
        List<Account> listAcc = TestDataFactory.createAccounts(1);
        List<Contact> listContacts = TestDataFactory.createContacts(listAcc[0],2);
        List<Contact> listContactsWithoutOpp = TestDataFactory.createContacts(listAcc[0],2);
        Club__c club = TestDataFactory.createClubs(TestDataFactory.CLUB_RE_LATAM);
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent', club);
        theGreatEvent.DigitalCampaignOpportunityOwner__c = UserInfo.getUserId();
        update theGreatEvent;

        // Cria Campanhas
//        List<Campaign> listCampaigns = TestDataFactory.createCampaigns(1, 'BRL');
        List<Campaign> listCampaigns = TestDataFactory.createCampaignsDigitalFunnelWithOpps(1, listContacts, club, theGreatEvent, new List<String>{'Intent'});
        Set<Id> setCampaignId = new Set<Id>();

        for(Campaign campaign : listCampaigns){
            setCampaignId.add(campaign.Id);
        }

        List<Opportunity> listOpportunity = [SELECT Id, Contact__c FROM Opportunity WHERE CampaignId =: setCampaignId];

        Test.startTest();

        // Cria CampaignMembers com Leads
        List<CampaignMember> listCampaignMembersLead = TestDataFactory.createCampaignMembersWithLeads(listCampaigns[0], listLead);

        System.debug('MEMBROS DA CAMPANHA QUE SAO LEADS ----------------------------------------------- ' + listCampaignMembersLead);

        // Cria CampaignMembers com Contatos
        List<CampaignMember> listCampaignMembersContact = TestDataFactory.createCampaignMembersWithContacts(listCampaigns[0], listContactsWithoutOpp, null);
        System.debug('MEMBROS DA CAMPANHA QUE SAO CONTATOS SEM OPORTUNIDADE ----------------------------------------------- ' + listCampaignMembersContact);

        List<CampaignMember> listCampaignMembersContactWithOpp = TestDataFactory.createCampaignMembersWithContacts(listCampaigns[0], null, ListOpportunity);
        System.debug('MEMBROS DA CAMPANHA QUE SAO CONTATOS COM OPORTUNIDADE ----------------------------------------------- ' + listCampaignMembersContactWithOpp);

        Test.stopTest();

    }

    @IsTest
    static void testAssignCampaignMemberToOpportunity(){

        List<CampaignMember> listCampaignMembers = [SELECT Id,Campaign.Digital_Funnel_Stage__c,ContactId, Opportunity__c, Opportunity__r.Contact__c, Opportunity__r.Digital_Funnel_Stage__c, LeadId, Type FROM CampaignMember ORDER BY Id];

        // Verifica se todos os leads foram convertidos e se atualizado no membro da campanha
        for(CampaignMember campaignMember : listCampaignMembers){

            System.assertEquals('Contact', campaignMember.Type);
            System.assert(campaignMember.Opportunity__c != null);
            System.assert(campaignMember.Opportunity__r.Contact__c != null);
            System.assert(campaignMember.Opportunity__r.Digital_Funnel_Stage__c == 'Intent');
        }



    }

}