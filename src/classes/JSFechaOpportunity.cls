/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Classe responsavel por avaliar Oportunidades e encerrá-las.
*
* NAME: JSFechaOpportunity.cls
* AUTHOR: RVR                                                 DATE: 13/04/2016
*******************************************************************************/

global with sharing class JSFechaOpportunity
{
	static webservice String fechaOportunidade(Id IdEvent)
	{  
	  Id IdUserRun = UserInfo.getUserId();
	  
	  list<User> lstUserRun = [SELECT Id, Profile.Name FROM User WHERE Id = :IdUserRun limit 1];
	  String ProfileNameUserRun = lstUserRun[0].Profile.Name;
	    
		list<Opportunity> lstOppUpdate = new list<Opportunity>();
									
		for(Opportunity opp: [SELECT Id, StageName, Lost_reason__c, Event__c, Event__r.OwnerId
																FROM Opportunity 
																WHERE Event__c =: IdEvent AND (IsClosed != true) Limit 200])
		{
		  if(opp.Event__r.OwnerId != IdUserRun && ProfileNameUserRun != 'System Administrator') continue;
		  
		  opp.Event__c = IdEvent;
			opp.StageName = 'Lost';
		  opp.Lost_reason__c = 'Campaing closed';
			lstOppUpdate.add(opp);		  
		}
		if(lstOppUpdate.isEmpty()) return 'Could not complete the action, ' +
		'contact the administrator. Possible causes: there is no opportunity or insufficient permission.';

		Database.update(lstOppUpdate);
		return 'The opportunities were successfully closed.';
	}
}