/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste para classe CreateMassOpportunityController.cls
*
* NAME: CreateMassOpportunityControllerTest.cls
* AUTHOR: KHPS                                                  DATE: 14/04/2016
* MAINTENANCE: LMdO                                             DATE: 30/07/2016
*******************************************************************************/
@isTest
private class CreateMassOpportunityControllerTest {

  private static Product2 produto;
  private static PricebookEntry priceEntryPadrao;
  private static Account conta;
  private static Contact contato;
  private static Event__c evento;
  private static Magazine__c magazine;
  private static Club__c clube;
  private static Campaign campanha;
  private static CampaignMember campMember;
  private static Opportunity oportunidade, oportunidade2;
  
  static 
  {
    produto = SObjectInstance.createProduct2();
    Database.insert(produto);
      
    Id catalog = SObjectInstance.catalogoDePrecoPadrao2();
      
    priceEntryPadrao = SObjectInstance.entradaDePreco(catalog, produto.Id);
    Database.insert(priceEntryPadrao);

    conta = SObjectInstance.createAccount2();
    Database.insert(conta);
      
    //Configuracao personalizada que o campo valor e um id
    Application_Config__c apConf = new Application_Config__c();
    apConf.Name = 'BucketUserId';
    apConf.Value__c = conta.id;
    Database.insert(apConf);    
    
    contato = SObjectInstance.createContato(conta.Id);
    contato.FirstName = 'ctt';
    contato.LastName = 'name';
    contato.Email = 'a@b.com.br';
    Database.insert(contato);
    
    Contact contato2 = SObjectInstance.createContato(conta.Id);
    contato2.FirstName = 'ctt2';
    contato2.LastName = 'name2';
    contato2.Email = 'a@b44.com.br';
    Database.insert(contato2);
      
    evento = SObjectInstance.createEvent();
    Database.insert(evento);
      
    magazine = SObjectInstance.createMagazine();
    Database.insert(magazine);
      
    clube = SObjectInstance.createClub();
    Database.insert(clube);
    
    Pricebook2 catalog1 = SObjectInstance.catalogoDePreco();
    catalog1.Event__c = evento.Id;
    catalog1.CurrencyIsoCode = 'USD';
    catalog1.Starting_date__c = system.today();
    catalog1.End_date__c = system.today();
    Database.insert(catalog1);
    
    PricebookEntry priceEntry1 = SObjectInstance.entradaDePreco(catalog1.Id, produto.Id);
    priceEntry1.UnitPrice = 100.00;
    priceEntry1.CurrencyIsoCode = 'USD';
    Database.insert(priceEntry1);
    
    campanha = SObjectInstance.createCampaign();
    campanha.Name = 'camp name';
    campanha.CurrencyIsoCode = 'USD';
    campanha.Event__c = evento.Id;
    campanha.Magazine__c = magazine.Id;
    Database.insert(campanha);
      
    campMember = SObjectInstance.createCampaignMember();
    campMember.CampaignId = campanha.Id;
    campMember.ContactId = contato.Id;
    campMember.Opportunity_Created__c = false;
    Database.insert(campMember);
   
    oportunidade = SObjectInstance.createOpportunidade(conta.Id, CreateMassOpportunityController.recOppUnique);
    oportunidade.Contact__c = contato2.Id;
    oportunidade.CloseDate = system.today();
    oportunidade.OwnerId = UserInfo.getUserId();
    oportunidade.Pricebook2Id = catalog1.Id;
    Database.insert(oportunidade);
  }

  static testMethod void testFuncionalCampanha()
  {
    Product2 produtoClub = SObjectInstance.createProduct2();
    Database.insert(produtoClub);
      
    Id catalogClub = SObjectInstance.catalogoDePrecoPadrao2();
      
    PricebookEntry priceEntryPadraoClub = SObjectInstance.entradaDePreco(catalogClub, produtoClub.Id);
    Database.insert(priceEntryPadraoClub);

    Account contaClub = SObjectInstance.createAccount2();
    Database.insert(contaClub);
    
    Contact contato2 = SObjectInstance.createContato(contaClub.Id);
    contato2.FirstName = 'ctt2';
    contato2.Email = 'a@b22.com.br';
    //contato2.Contact_Field_unique__c = 'teste2@gmail.com';
    Database.insert(contato2);
      
    Pricebook2 catalogoClub = new Pricebook2();
    catalogoClub.Name = 'teste';
    catalogoClub.Type__c = 'Memberships';
    catalogoClub.Club__c = clube.Id;
    catalogoClub.CurrencyIsoCode = 'USD';
    catalogoClub.Starting_date__c = system.today();
    catalogoClub.End_date__c = system.today();
    Database.insert(catalogoClub);
    
    PricebookEntry priceEntry1 = SObjectInstance.entradaDePreco(catalogoClub.Id, produtoClub.Id);
    priceEntry1.UnitPrice = 100.00;
    priceEntry1.CurrencyIsoCode = 'USD';
    Database.insert(priceEntry1);
    
    Campaign campanhaClub = SObjectInstance.createCampaign();
    campanhaClub.Name = 'camp name';
    campanhaClub.CurrencyIsoCode = 'USD';
    campanhaClub.Opportunity_Type__c = 'Membership';
    campanhaClub.Club__c = clube.Id;
    Database.insert(campanhaClub);
   
    Opportunity oportunidadeClub = SObjectInstance.createOpportunidade(contaClub.Id, CreateMassOpportunityController.recOppMembership);
    oportunidadeClub.Contact__c = contato2.Id;
    oportunidadeClub.CloseDate = system.today();
    oportunidadeClub.OwnerId = UserInfo.getUserId();
    oportunidadeClub.Pricebook2Id = catalogoClub.Id;
    Database.insert(oportunidadeClub);

    Test.setCurrentPage(Page.CreateMassOpportunity);
    Test.startTest();
    CreateMassOpportunityController controller = new CreateMassOpportunityController(new ApexPages.StandardController(campanhaClub));

    controller.opp.StageName = 'Suspect';
    controller.opp.CloseDate = System.today();
    controller.opp.OwnerId = UserInfo.getUserId();
    controller.lstProdutoWpp[0].IsSelected = true;

    controller.SelectDate();
    controller.SelectedCurrency();
    controller.SelecionaPricebook();
    controller.CarregaProdutos();
    controller.countProductSelect();
    PageReference salvar = controller.createMassOpportunities();
    PageReference cancelar = controller.cancel();
    
    controller.loadPopup();
    controller.closePopup();
    controller.closePopupAndClear();

    List<SelectOption> lstOppCurrency = controller.getCurrencyOpp;
    List<SelectOption> lstOppCamp = controller.getTypeOppCamp;
    List<SelectOption> lstOppStatus = controller.getStatusOpp;

    Test.stopTest();
  }

  static testMethod void testFuncionalCampanhaExistente()
  {
    Product2 produtoClub = SObjectInstance.createProduct2();
    Database.insert(produtoClub);
      
    Id catalogClub = SObjectInstance.catalogoDePrecoPadrao2();
      
    PricebookEntry priceEntryPadraoClub = SObjectInstance.entradaDePreco(catalogClub, produtoClub.Id);
    Database.insert(priceEntryPadraoClub);

    Account contaClub = SObjectInstance.createAccount2();
    Database.insert(contaClub);
    
    Contact contato2 = SObjectInstance.createContato(contaClub.Id);
    contato2.FirstName = 'ctt2';
    contato2.Email = 'a@b2.com.br';
    //contato2.Contact_Field_unique__c = 'teste@gmail.com';
    Database.insert(contato2);
      
    Pricebook2 catalogoClub = new Pricebook2();
    catalogoClub.Name = 'teste';
    catalogoClub.Type__c = 'Memberships';
    catalogoClub.Club__c = clube.Id;
    catalogoClub.CurrencyIsoCode = 'USD';
    catalogoClub.Starting_date__c = system.today();
    catalogoClub.End_date__c = system.today();
    Database.insert(catalogoClub);
    
    PricebookEntry priceEntry1 = SObjectInstance.entradaDePreco(catalogoClub.Id, produtoClub.Id);
    priceEntry1.UnitPrice = 100.00;
    priceEntry1.CurrencyIsoCode = 'USD';
    Database.insert(priceEntry1);
    
    Campaign campanhaClub = SObjectInstance.createCampaign();
    campanhaClub.Name = 'camp name';
    campanhaClub.CurrencyIsoCode = 'USD';
    campanhaClub.Opportunity_Type__c = 'Membership';
    campanhaClub.Club__c = clube.Id;
    Database.insert(campanhaClub);
      
    CampaignMember campMemberClub = SObjectInstance.createCampaignMember();
    campMemberClub.CampaignId = campanhaClub.Id;
    campMemberClub.ContactId = contato2.Id;
    campMemberClub.Opportunity_Created__c = false;
    Database.insert(campMemberClub);
   
    Opportunity oportunidadeClub = SObjectInstance.createOpportunidade(contaClub.Id, CreateMassOpportunityController.recOppMembership);
    oportunidadeClub.Club__c = clube.Id;
    oportunidadeClub.Contact__c = contato2.Id;
    oportunidadeClub.CloseDate = system.today();
    oportunidadeClub.OwnerId = UserInfo.getUserId();
    oportunidadeClub.Pricebook2Id = catalogoClub.Id;
    Database.insert(oportunidadeClub);

    Test.setCurrentPage(Page.CreateMassOpportunity);
    Test.startTest();

        Opportunity oportunidadeClub2 = SObjectInstance.createOpportunidade(contaClub.Id, CreateMassOpportunityController.recOppMembership);
        oportunidadeClub2.Club__c = clube.Id;
        oportunidadeClub2.Contact__c = contato2.Id;
        oportunidadeClub2.CloseDate = system.today();
        oportunidadeClub2.OwnerId = UserInfo.getUserId();
        oportunidadeClub2.Pricebook2Id = catalogoClub.Id;
        Database.insert(oportunidadeClub2);

        CreateMassOpportunityController controller = new CreateMassOpportunityController(new ApexPages.StandardController(campanhaClub));
    
    Test.stopTest();
  }

  static testMethod void testFuncional()
  {
    Test.setCurrentPage(Page.CreateMassOpportunity);
    Test.startTest();
    CreateMassOpportunityController controller = new CreateMassOpportunityController(new ApexPages.StandardController(campanha));

    controller.opp.StageName = 'Suspect';
    controller.opp.CloseDate = System.today();
    controller.opp.OwnerId = UserInfo.getUserId();
    controller.lstProdutoWpp[0].IsSelected = true;

    controller.SelectDate();
    controller.SelectedCurrency();
    controller.SelecionaPricebook();
    controller.CarregaProdutos();
    controller.countProductSelect();
    PageReference salvar = controller.createMassOpportunities();
    PageReference cancelar = controller.cancel();
    
    controller.loadPopup();
    controller.closePopup();
    controller.closePopupAndClear();

    List<SelectOption> lstOppCurrency = controller.getCurrencyOpp;
    List<SelectOption> lstOppCamp = controller.getTypeOppCamp;
    List<SelectOption> lstOppStatus = controller.getStatusOpp;

    Test.stopTest();
  }

  static testMethod void testFuncionalExistente()
  {
    Test.setCurrentPage(Page.CreateMassOpportunity);
    
    Test.startTest();

    String chaveOpp = campMember.ContactId+' - '+conta.Id+' - '+evento.Id;
    oportunidade2 = SObjectInstance.createOpportunidade(conta.Id, CreateMassOpportunityController.recOppUnique);
    oportunidade2.Contact__c = contato.Id;
    oportunidade2.Event__c = campanha.Event__c;
    oportunidade2.opp_unique__c = chaveOpp;
    oportunidade2.CloseDate = system.today();
    Database.insert(oportunidade2);

    CreateMassOpportunityController controller = new CreateMassOpportunityController(new ApexPages.StandardController(campanha));

    Test.stopTest();
  }
 
  static testMethod void testErro()
  {
    Test.setCurrentPage(Page.CreateMassOpportunity);
    
    Test.startTest();
    CreateMassOpportunityController controller = new CreateMassOpportunityController(new ApexPages.StandardController(campanha));
    controller.opp.StageName = 'Suspect';
    
    PageReference salvar = controller.createMassOpportunities();
    controller.opp.CloseDate = System.today();
    PageReference salvar2 = controller.createMassOpportunities();
    controller.opp.OwnerId = UserInfo.getUserId();
    PageReference salvar3 = controller.createMassOpportunities();
    controller.changeCloseDate();
    Test.stopTest();
  }
  
  static testMethod void testIgual()
  {
    Test.setCurrentPage(Page.CreateMassOpportunity);
    
    Test.startTest();
    CreateMassOpportunityController controller = new CreateMassOpportunityController(new ApexPages.StandardController(campanha));
    //controller.oppCampType = 'Open_Events';
    controller.SelecionaPricebook();
    controller.opp.StageName = 'Suspect';
    controller.opp.CloseDate = System.today();
    controller.opp.OwnerId = UserInfo.getUserId();
    
    PageReference salvar = controller.createMassOpportunities();
    
    Test.stopTest();
  }
  
 /* static testMethod void testIgual2()
  {
    Test.setCurrentPage(Page.CreateMassOpportunity);
    campanha.Opportunity_Type__c = 'Membership';
    oportunidade = SObjectInstance.createOpportunidade(conta.Id, CreateMassOpportunityController.recOppMembership);
    oportunidade.Contact__c = contato.Id;
    oportunidade.Event__c = evento.Id;
    oportunidade.Magazine__c = magazine.Id;
    oportunidade.Club__c = clube.Id;
    oportunidade.CloseDate = system.today();
    oportunidade.OwnerId = UserInfo.getUserId();
    oportunidade.Spex_Type__c = 'Event Sponsorship';

    Database.insert(oportunidade);
   
    Database.update(oportunidade);
    
    Test.startTest();
    CreateMassOpportunityController controller = new CreateMassOpportunityController(new ApexPages.StandardController(campanha));
    controller.oppStatus = 'teste';
    
    controller.opp.CloseDate = System.today();
    controller.opp.OwnerId = UserInfo.getUserId();
    PageReference salvar = controller.createMassOpportunities();
    
    Test.stopTest();
  }*/
}