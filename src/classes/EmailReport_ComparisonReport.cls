public with sharing class EmailReport_ComparisonReport
{
	public String comparisonReportId {get; set;}

	public EmailReport__c report {get; set;}

	public String weekOfYear {get; set;}

	public String reportDayOfYear {get; set;}

	public String buYTD {get; set;}
	public String clubYTD {get; set;}
	public String annualConfYTD {get; set;}

	public Decimal buYTD_Decimal {get; set;}
	public Decimal clubYTD_Decimal {get; set;}
	public Decimal annualConfYTD_Decimal {get; set;}

	public Decimal buCompTotal_target_RevenuePercentageAchieved {get; set;}
	public Decimal clubCompTotal_target_RevenuePercentageAchieved {get; set;}
	public Decimal annualConfCompTotal_target_RevenuePercentageAchieved {get; set;}

	public List<EmailReport_ComparisonReportGen_Helper.BusinessUnitsComparison> businessUnitComparison {get {
																											loadReport();
																											return businessUnitComparison;
																										}
																										set;}

	public List<EmailReport_ComparisonReportGen_Helper.ClubComparison> clubComparison {get {
																							loadReport();
																							return clubComparison;
																						}
																						set;}																		

	public List<EmailReport_ComparisonReportGen_Helper.EventComparison> futureEventComparison {get {
																									loadReport();
																									return futureEventComparison;
																								}
																								set;}
	public List<EmailReport_ComparisonReportGen_Helper.EventComparison> previousEventComparison {get {
																									loadReport();
																									return previousEventComparison;
																								}
																								set;}

	private void loadReport()
	{
		System.debug('#### loadReport');

		if(report != null)
		{
			return;
		}

		if(String.isNotBlank(comparisonReportId))
		{
			System.debug('#### loadReport with Report Id');
			report = [SELECT Id, Report_Week__c, Report_Date__c FROM EmailReport__c WHERE Id = :comparisonReportId];
		}
		else
		{
			System.debug('#### loadReport with Todays Date');
			report = [SELECT Id, Report_Week__c, Report_Date__c FROM EmailReport__c WHERE Report_Date__c = TODAY];
		}

		System.debug('#### report: ' + report);

		reportDayOfYear = String.valueOf(report.Report_Date__c.dayOfYear());

		buYTD_Decimal = ((100.00/365.00) * report.Report_Date__c.dayOfYear());
		clubYTD_Decimal = ((100.00/365.00) * report.Report_Date__c.dayOfYear());
		annualConfYTD_Decimal = ((100.00/365.00) * report.Report_Date__c.dayOfYear());

		buYTD = buYTD_Decimal.setScale(2).format();
		clubYTD = clubYTD_Decimal.setScale(2).format();
		annualConfYTD = annualConfYTD_Decimal.setScale(2).format();		

		System.debug('#### buYTD: ' + buYTD);
		System.debug('#### clubYTD: ' + clubYTD);
		System.debug('#### annualConfYTD: ' + annualConfYTD);

		List<Attachment> reportJSONAttachments = [SELECT Id, Name, Body FROM Attachment WHERE ParentId = :report.Id];

		for(Attachment a : reportJSONAttachments)
		{
			System.debug('#### Section: ' + a.Name);
			System.debug('#### Body: ' + a.Body.toString());

			if(a.Name == 'ClubComparison.txt')
			{	
				clubComparison = (List<EmailReport_ComparisonReportGen_Helper.ClubComparison>)JSON.deserialize(a.Body.toString(), List<EmailReport_ComparisonReportGen_Helper.ClubComparison>.class);
			}
			else if(a.Name == 'FutureEventsComparison.txt')
			{
				futureEventComparison = (List<EmailReport_ComparisonReportGen_Helper.EventComparison>)JSON.deserialize(a.Body.toString(), List<EmailReport_ComparisonReportGen_Helper.EventComparison>.class);
				futureEventComparison.sort();
			}
			else if(a.Name == 'PreviousEventsComparison.txt')
			{
				previousEventComparison = (List<EmailReport_ComparisonReportGen_Helper.EventComparison>)JSON.deserialize(a.Body.toString(), List<EmailReport_ComparisonReportGen_Helper.EventComparison>.class);
				previousEventComparison.sort();		
			}
			else if(a.Name == 'BusinessUnitsComparison.txt')
			{	
				businessUnitComparison = (List<EmailReport_ComparisonReportGen_Helper.BusinessUnitsComparison>)JSON.deserialize(a.Body.toString(), List<EmailReport_ComparisonReportGen_Helper.BusinessUnitsComparison>.class);
			}
		}

		try
		{
			Decimal buTotal_ThisYearAmount = 0;
			Decimal buTotal_ThisYearTargetCurrency = 0;

			for(EmailReport_ComparisonReportGen_Helper.BusinessUnitsComparison buComp : businessUnitComparison)
			{
				buTotal_ThisYearAmount += buComp.buTypeResults.get('Totals').thisYearAmount;
				buTotal_ThisYearTargetCurrency += buComp.buTypeResults.get('Totals').thisYearTargetCurrency;
			}

			buCompTotal_target_RevenuePercentageAchieved = ((buTotal_ThisYearAmount / buTotal_ThisYearTargetCurrency) * 100);
		}
		catch(Exception e)
		{
			System.debug(e.getMessage());
		}

		try
		{
			Decimal clubCompTotal_ThisYearAmount = 0;
			Decimal clubCompTotal_ThisYearTargetCurrency = 0;

			for(EmailReport_ComparisonReportGen_Helper.ClubComparison clubComp : clubComparison)
			{
				clubCompTotal_ThisYearAmount += clubComp.membershipTypeResults.get('Totals').thisYearAmount;
				clubCompTotal_ThisYearTargetCurrency += clubComp.membershipTypeResults.get('Totals').thisYearTargetCurrency;
			}

			clubCompTotal_target_RevenuePercentageAchieved = ((clubCompTotal_ThisYearAmount / clubCompTotal_ThisYearTargetCurrency) * 100);
		}
		catch(Exception e)
		{
			System.debug(e.getMessage());
		}

		try
		{
			Decimal annualConfCompTotal_ThisYearAmount = 0;
			Decimal annualConfCompTotal_ThisYearTargetCurrency = 0;

			for(EmailReport_ComparisonReportGen_Helper.EventComparison futureEventComp : futureEventComparison)
			{
				annualConfCompTotal_ThisYearAmount += futureEventComp.registrationTypeResults.get('Totals').thisYearAmount;
				annualConfCompTotal_ThisYearTargetCurrency += futureEventComp.registrationTypeResults.get('Totals').thisYearTargetCurrency;
			}

			for(EmailReport_ComparisonReportGen_Helper.EventComparison prevEventComp : previousEventComparison)
			{
				annualConfCompTotal_ThisYearAmount += prevEventComp.registrationTypeResults.get('Totals').thisYearAmount;
				annualConfCompTotal_ThisYearTargetCurrency += prevEventComp.registrationTypeResults.get('Totals').thisYearTargetCurrency;
			}

			annualConfCompTotal_target_RevenuePercentageAchieved = ((annualConfCompTotal_ThisYearAmount / annualConfCompTotal_ThisYearTargetCurrency) * 100);	
		}
		catch(Exception e)
		{
			System.debug(e.getMessage());
		}
	}
}