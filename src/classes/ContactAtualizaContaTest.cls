/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe teste da classe ContactAtualizaConta
*
*
* NAME: ContactAtualizaContaTest.cls
* AUTHOR: VMDL                                                 DATE: 19/08/2016
*******************************************************************************/
@isTest
private class ContactAtualizaContaTest {
	
	private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');

	static testMethod void testeFuncional()
	{
		Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
		Database.insert(conta);

		Contact contato = SObjectInstance.createContato(conta.Id);
		Database.insert(contato);

		contato.Global_Club_Member__c = true;
		contato.Infrastructure_BR_Club__c = true;
		contato.Real_Estate_BR_Club__c = true;
		contato.India_Club_Member__c = true;
		contato.Retail_Club_BR__c = true;

		Test.startTest();
    	Database.update(contato);
    Test.stopTest();
	}

}