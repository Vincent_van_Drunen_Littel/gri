/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsável por Atualizar campos em Account, verifica os contatos da conta
* se algum campo estiver true ou false, atualiza a Conta.
*
* NAME: ContactAtualizaConta.cls
* AUTHOR: VMDL                                                 DATE: 19/08/2016
*******************************************************************************/
public with sharing class ContactAtualizaConta {

  public static void execute()
  {
    TriggerUtils.assertTrigger();

    Set<Id> setAccountId = new Set<Id>(); 

    for(Contact iContact : (list<Contact>) trigger.new)
    {
      setAccountId.add(iContact.AccountId);
    }

    if(setAccountId.isEmpty()) return;

    map<Id, Account> mapAccountToUpdate = new map<Id, Account>();
    for(Contact iContact : [Select Id, Name , Infrastructure_BR_Club__c, India_Club_Member__c,
                Global_Club_Member__c, Real_Estate_BR_Club__c, AccountId, Retail_Club_BR__c, 
                RE_Club_Latam_Member__c, RE_Club_Africa_Member__c, Infra_Club_India_Member__c
                from Contact
                WHERE AccountId =: setAccountId])
    {

      Account lAcc = mapAccountToUpdate.get(iContact.AccountId);
      if(lAcc == null) lAcc = new Account(Id = iContact.AccountId,
        Global_Club_Member__c = false,
        Infrastructure_BR_Club__c = false,
        Real_Estate_BR_Club__c = false,
        India_Club_Member__c = false,
        Retail_Club_BR__c = false,
        RE_Club_Africa_Company_Member__c= false,
        RE_Club_LatAM_Company_Member__c= false, 
        Infra_Club_India_Company_Member__c= false
        );

      //Exemplo Boolean b = false; -> b = b || (1>0) tabela verdade falso com verdadeiro é verdadeiro
      //Exemplo acima melhorado b |= (1>0) segue abaixo.
      lAcc.Global_Club_Member__c |= (iContact.Global_Club_Member__c);
      lAcc.Infrastructure_BR_Club__c |= (iContact.Infrastructure_BR_Club__c);
      lAcc.Real_Estate_BR_Club__c |= (iContact.Real_Estate_BR_Club__c);
      lAcc.India_Club_Member__c |= (iContact.India_Club_Member__c);
      lAcc.Retail_Club_BR__c |= (iContact.Retail_Club_BR__c);
      lAcc.RE_Club_LatAM_Company_Member__c|= (iContact.RE_Club_Latam_Member__c);
      lAcc.Infra_Club_India_Company_Member__c|= (iContact.Infra_Club_India_Member__c);
      lAcc.RE_Club_Africa_Company_Member__c|= (iContact.RE_Club_Africa_Member__c);
      

      mapAccountToUpdate.put(lAcc.Id, lAcc);
    }
    if(!mapAccountToUpdate.isEmpty()) Database.Update(mapAccountToUpdate.values());
  }  
}