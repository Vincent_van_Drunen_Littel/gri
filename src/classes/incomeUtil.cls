public with sharing class incomeUtil {
  public static string countryName( String stringCode )
  {
     String codeValue = stringCode;
     String nameValue = '';
        if(codeValue == 'AF'){  nameValue = 'Afghanistan'; }
        else if(codeValue == 'AX'){nameValue = 'Aland Islands';} 
        else if(codeValue == 'AL'){nameValue = 'Albania';} 
        else if(codeValue == 'DZ'){nameValue = 'Algeria';} 
        else if(codeValue == 'AS'){nameValue = 'American Samoa';} 
        else if(codeValue == 'AD'){nameValue = 'Andorra';} 
        else if(codeValue == 'AO'){nameValue = 'Angola';} 
        else if(codeValue == 'AI'){nameValue = 'Anguilla';} 
        else if(codeValue == 'AQ'){nameValue = 'Antarctica';} 
        else if(codeValue == 'AG'){nameValue = 'Antigua and Barbuda';} 
        else if(codeValue == 'AR'){nameValue = 'Argentina';} 
        else if(codeValue == 'AM'){nameValue = 'Armenia';} 
        else if(codeValue == 'AW'){nameValue = 'Aruba';} 
        else if(codeValue == 'AU'){nameValue = 'Australia';} 
        else if(codeValue == 'AT'){nameValue = 'Austria';} 
        else if(codeValue == 'AZ'){nameValue = 'Azerbaijan';} 
        else if(codeValue == 'BS'){nameValue = 'Bahamas the';} 
        else if(codeValue == 'BH'){nameValue = 'Bahrain';} 
        else if(codeValue == 'BD'){nameValue = 'Bangladesh';} 
        else if(codeValue == 'BB'){nameValue = 'Barbados';} 
        else if(codeValue == 'BY'){nameValue = 'Belarus';} 
        else if(codeValue == 'BE'){nameValue = 'Belgium';} 
        else if(codeValue == 'BZ'){nameValue = 'Belize';} 
        else if(codeValue == 'BJ'){nameValue = 'Benin';} 
        else if(codeValue == 'BM'){nameValue = 'Bermuda';} 
        else if(codeValue == 'BT'){nameValue = 'Bhutan';} 
        else if(codeValue == 'BO'){nameValue = 'Bolivia';} 
        else if(codeValue == 'BA'){nameValue = 'Bosnia and Herzegovina';} 
        else if(codeValue == 'BW'){nameValue = 'Botswana';} 
        else if(codeValue == 'BV'){nameValue = 'Bouvet Island (Bouvetoya)';} 
        else if(codeValue == 'BR'){nameValue = 'Brazil';} 
        else if(codeValue == 'IO'){nameValue = 'British Indian Ocean Territory (Chagos Archipelago)';} 
        else if(codeValue == 'VG'){nameValue = 'British Virgin Islands';} 
        else if(codeValue == 'BN'){nameValue = 'Brunei Darussalam';} 
        else if(codeValue == 'BG'){nameValue = 'Bulgaria';} 
        else if(codeValue == 'BF'){nameValue = 'Burkina Faso';} 
        else if(codeValue == 'BI'){nameValue = 'Burundi';} 
        else if(codeValue == 'KH'){nameValue = 'Cambodia';} 
        else if(codeValue == 'CM'){nameValue = 'Cameroon';} 
        else if(codeValue == 'CA'){nameValue = 'Canada';} 
        else if(codeValue == 'CV'){nameValue = 'Cape Verde';} 
        else if(codeValue == 'KY'){nameValue = 'Cayman Islands';} 
        else if(codeValue == 'CF'){nameValue = 'Central African Republic';} 
        else if(codeValue == 'TD'){nameValue = 'Chad';} 
        else if(codeValue == 'CL'){nameValue = 'Chile';} 
        else if(codeValue == 'CN'){nameValue = 'China';} 
        else if(codeValue == 'CX'){nameValue = 'Christmas Island';} 
        else if(codeValue == 'CC'){nameValue = 'Cocos (Keeling) Islands';} 
        else if(codeValue == 'CO'){nameValue = 'Colombia';} 
        else if(codeValue == 'KM'){nameValue = 'Comoros the';} 
        else if(codeValue == 'CD'){nameValue = 'Congo';} 
        else if(codeValue == 'CG'){nameValue = 'Congo the';} 
        else if(codeValue == 'CK'){nameValue = 'Cook Islands';} 
        else if(codeValue == 'CR'){nameValue = 'Costa Rica';} 
        else if(codeValue == 'CI'){nameValue = 'Cote dIvoire';} 
        else if(codeValue == 'HR'){nameValue = 'Croatia';} 
        else if(codeValue == 'CU'){nameValue = 'Cuba';} 
        else if(codeValue == 'CY'){nameValue = 'Cyprus';} 
        else if(codeValue == 'CZ'){nameValue = 'Czech Republic';} 
        else if(codeValue == 'DK'){nameValue = 'Denmark';} 
        else if(codeValue == 'DJ'){nameValue = 'Djibouti';} 
        else if(codeValue == 'DM'){nameValue = 'Dominica';} 
        else if(codeValue == 'DO'){nameValue = 'Dominican Republic';} 
        else if(codeValue == 'EC'){nameValue = 'Ecuador';} 
        else if(codeValue == 'EG'){nameValue = 'Egypt';} 
        else if(codeValue == 'SV'){nameValue = 'El Salvador';} 
        else if(codeValue == 'GQ'){nameValue = 'Equatorial Guinea';} 
        else if(codeValue == 'ER'){nameValue = 'Eritrea';} 
        else if(codeValue == 'EE'){nameValue = 'Estonia';} 
        else if(codeValue == 'ET'){nameValue = 'Ethiopia';} 
        else if(codeValue == 'FO'){nameValue = 'Faroe Islands';} 
        else if(codeValue == 'FK'){nameValue = 'Falkland Islands (Malvinas)';} 
        else if(codeValue == 'FJ'){nameValue = 'Fiji the Fiji Islands';} 
        else if(codeValue == 'FI'){nameValue = 'Finland';} 
        else if(codeValue == 'FR'){nameValue = 'France, French Republic';} 
        else if(codeValue == 'GF'){nameValue = 'French Guiana';} 
        else if(codeValue == 'PF'){nameValue = 'French Polynesia';} 
        else if(codeValue == 'TF'){nameValue = 'French Southern Territories';} 
        else if(codeValue == 'GA'){nameValue = 'Gabon';} 
        else if(codeValue == 'GB'){nameValue = 'United Kingdom';} 
        else if(codeValue == 'GM'){nameValue = 'Gambia the';} 
        else if(codeValue == 'GE'){nameValue = 'Georgia';} 
        else if(codeValue == 'DE'){nameValue = 'Germany';} 
        else if(codeValue == 'GH'){nameValue = 'Ghana';} 
        else if(codeValue == 'GI'){nameValue = 'Gibraltar';} 
        else if(codeValue == 'GR'){nameValue = 'Greece';} 
        else if(codeValue == 'GL'){nameValue = 'Greenland';} 
        else if(codeValue == 'GD'){nameValue = 'Grenada';} 
        else if(codeValue == 'GP'){nameValue = 'Guadeloupe';} 
        else if(codeValue == 'GU'){nameValue = 'Guam';} 
        else if(codeValue == 'GT'){nameValue = 'Guatemala';} 
        else if(codeValue == 'GG'){nameValue = 'Guernsey';} 
        else if(codeValue == 'GN'){nameValue = 'Guinea';} 
        else if(codeValue == 'GW'){nameValue = 'Guinea-Bissau';} 
        else if(codeValue == 'GY'){nameValue = 'Guyana';} 
        else if(codeValue == 'HT'){nameValue = 'Haiti';} 
        else if(codeValue == 'HM'){nameValue = 'Heard Island and McDonald Islands';} 
        else if(codeValue == 'VA'){nameValue = 'Holy See (Vatican City State)';} 
        else if(codeValue == 'HN'){nameValue = 'Honduras';} 
        else if(codeValue == 'HK'){nameValue = 'Hong Kong';} 
        else if(codeValue == 'HU'){nameValue = 'Hungary';} 
        else if(codeValue == 'IS'){nameValue = 'Iceland';} 
        else if(codeValue == 'IN'){nameValue = 'India';} 
        else if(codeValue == 'ID'){nameValue = 'Indonesia';} 
        else if(codeValue == 'IR'){nameValue = 'Iran';} 
        else if(codeValue == 'IQ'){nameValue = 'Iraq';} 
        else if(codeValue == 'IE'){nameValue = 'Ireland';} 
        else if(codeValue == 'IM'){nameValue = 'Isle of Man';} 
        else if(codeValue == 'IL'){nameValue = 'Israel';} 
        else if(codeValue == 'IT'){nameValue = 'Italy';} 
        else if(codeValue == 'JM'){nameValue = 'Jamaica';} 
        else if(codeValue == 'JP'){nameValue = 'Japan';} 
        else if(codeValue == 'JE'){nameValue = 'Jersey';} 
        else if(codeValue == 'JO'){nameValue = 'Jordan';} 
        else if(codeValue == 'KZ'){nameValue = 'Kazakhstan';} 
        else if(codeValue == 'KE'){nameValue = 'Kenya';} 
        else if(codeValue == 'KI'){nameValue = 'Kiribati';} 
        else if(codeValue == 'KP'){nameValue = 'Korea';} 
        else if(codeValue == 'KR'){nameValue = 'Korea';} 
        else if(codeValue == 'KW'){nameValue = 'Kuwait';} 
        else if(codeValue == 'KG'){nameValue = 'Kyrgyz Republic';} 
        else if(codeValue == 'LA'){nameValue = 'Lao';} 
        else if(codeValue == 'LV'){nameValue = 'Latvia';} 
        else if(codeValue == 'LB'){nameValue = 'Lebanon';} 
        else if(codeValue == 'LS'){nameValue = 'Lesotho';} 
        else if(codeValue == 'LR'){nameValue = 'Liberia';} 
        else if(codeValue == 'LY'){nameValue = 'Libyan Arab Jamahiriya';} 
        else if(codeValue == 'LI'){nameValue = 'Liechtenstein';} 
        else if(codeValue == 'LT'){nameValue = 'Lithuania';} 
        else if(codeValue == 'LU'){nameValue = 'Luxembourg';} 
        else if(codeValue == 'MO'){nameValue = 'Macaoelse';} 
        else if(codeValue == 'MK'){nameValue = 'Macedonia';}
        else if(codeValue == 'MG'){nameValue = 'Madagascar';}
        else if(codeValue == 'MW'){nameValue = 'Malawi';}
        else if(codeValue == 'MY'){nameValue = 'Malaysia';}
        else if(codeValue == 'MV'){nameValue = 'Maldives';}
        else if(codeValue == 'ML'){nameValue = 'Mali';}
        else if(codeValue == 'MT'){nameValue = 'Malta';}
        else if(codeValue == 'MH'){nameValue = 'Marshall Islands';}
        else if(codeValue == 'MQ'){nameValue = 'Martinique';}
        else if(codeValue == 'MR'){nameValue = 'Mauritania';}
        else if(codeValue == 'MU'){nameValue = 'Mauritius';}
        else if(codeValue == 'YT'){nameValue = 'Mayotte';}
        else if(codeValue == 'MX'){nameValue = 'Mexico';}
        else if(codeValue == 'FM'){nameValue = 'Micronesia';}
        else if(codeValue == 'MD'){nameValue = 'Moldova';}
        else if(codeValue == 'MC'){nameValue = 'Monaco';}
        else if(codeValue == 'MN'){nameValue = 'Mongolia';}
        else if(codeValue == 'ME'){nameValue = 'Montenegro';}
        else if(codeValue == 'MS'){nameValue = 'Montserrat';}
        else if(codeValue == 'MA'){nameValue = 'Morocco';}
        else if(codeValue == 'MZ'){nameValue = 'Mozambique';}
        else if(codeValue == 'MM'){nameValue = 'Myanmar';}
        else if(codeValue == 'NA'){nameValue = 'Namibia';}
        else if(codeValue == 'NR'){nameValue = 'Nauru';}
        else if(codeValue == 'NP'){nameValue = 'Nepal';}
        else if(codeValue == 'AN'){nameValue = 'Netherlands Antilles';}
        else if(codeValue == 'NL'){nameValue = 'Netherlands the';}
        else if(codeValue == 'NC'){nameValue = 'New Caledonia';}
        else if(codeValue == 'NZ'){nameValue = 'New Zealand';}
        else if(codeValue == 'NI'){nameValue = 'Nicaragua';}
        else if(codeValue == 'NE'){nameValue = 'Niger';}
        else if(codeValue == 'NG'){nameValue = 'Nigeria';}
        else if(codeValue == 'NU'){nameValue = 'Niue';}
        else if(codeValue == 'NF'){nameValue = 'Norfolk Island';}
        else if(codeValue == 'MP'){nameValue = 'Northern Mariana Islands';}
        else if(codeValue == 'NO'){nameValue = 'Norway';}
        else if(codeValue == 'OM'){nameValue = 'Oman';}
        else if(codeValue == 'PK'){nameValue = 'Pakistan';}
        else if(codeValue == 'PW'){nameValue = 'Palau';}
        else if(codeValue == 'PS'){nameValue = 'Palestinian Territory';}
        else if(codeValue == 'PA'){nameValue = 'Panama';}
        else if(codeValue == 'PG'){nameValue = 'Papua New Guinea';}
        else if(codeValue == 'PY'){nameValue = 'Paraguay';}
        else if(codeValue == 'PE'){nameValue = 'Peru';}
        else if(codeValue == 'PH'){nameValue = 'Philippines';}
        else if(codeValue == 'PN'){nameValue = 'Pitcairn Islands';}
        else if(codeValue == 'PL'){nameValue = 'Poland';}
        else if(codeValue == 'PT'){nameValue = 'Portugal, Portuguese Republic';}
        else if(codeValue == 'PR'){nameValue = 'Puerto Rico';}
        else if(codeValue == 'QA'){nameValue = 'Qatar';}
        else if(codeValue == 'RE'){nameValue = 'Reunion';}
        else if(codeValue == 'RO'){nameValue = 'Romania';}
        else if(codeValue == 'RU'){nameValue = 'Russian Federation';}
        else if(codeValue == 'RW'){nameValue = 'Rwanda';}
        else if(codeValue == 'BL'){nameValue = 'Saint Barthelemy';}
        else if(codeValue == 'SH'){nameValue = 'Saint Helena';}
        else if(codeValue == 'KN'){nameValue = 'Saint Kitts and Nevis';}
        else if(codeValue == 'LC'){nameValue = 'Saint Lucia';}
        else if(codeValue == 'MF'){nameValue = 'Saint Martin';}
        else if(codeValue == 'PM'){nameValue = 'Saint Pierre and Miquelon';}
        else if(codeValue == 'VC'){nameValue = 'Saint Vincent and the Grenadines';}
        else if(codeValue == 'WS'){nameValue = 'Samoa';}
        else if(codeValue == 'SM'){nameValue = 'San Marino';}
        else if(codeValue == 'ST'){nameValue = 'Sao Tome and Principe';}
        else if(codeValue == 'SA'){nameValue = 'Saudi Arabia';}
        else if(codeValue == 'SN'){nameValue = 'Senegal';}
        else if(codeValue == 'RS'){nameValue = 'Serbia';}
        else if(codeValue == 'SC'){nameValue = 'Seychelles';}
        else if(codeValue == 'SL'){nameValue = 'Sierra Leone';}
        else if(codeValue == 'SG'){nameValue = 'Singapore';}
        else if(codeValue == 'SK'){nameValue = 'Slovakia (Slovak Republic)';}
        else if(codeValue == 'SI'){nameValue = 'Slovenia';}
        else if(codeValue == 'SB'){nameValue = 'Solomon Islands';}
        else if(codeValue == 'SO'){nameValue = 'Somalia, Somali Republic';}
        else if(codeValue == 'ZA'){nameValue = 'South Africa';}
        else if(codeValue == 'GS'){nameValue = 'South Georgia and the South Sandwich Islands';}
        else if(codeValue == 'ES'){nameValue = 'Spain';}
        else if(codeValue == 'LK'){nameValue = 'Sri Lanka';}
        else if(codeValue == 'SD'){nameValue = 'Sudan';}
        else if(codeValue == 'SR'){nameValue = 'Suriname';}
        else if(codeValue == 'SJ'){nameValue = 'Svalbard &amp; Jan Mayen Islands';}
        else if(codeValue == 'SZ'){nameValue = 'Swaziland';}
        else if(codeValue == 'SE'){nameValue = 'Sweden';}
        else if(codeValue == 'CH'){nameValue = 'Switzerland, Swiss Confederation';}
        else if(codeValue == 'SY'){nameValue = 'Syrian Arab Republic';}
        else if(codeValue == 'TW'){nameValue = 'Taiwan';}
        else if(codeValue == 'TJ'){nameValue = 'Tajikistan';}
        else if(codeValue == 'TZ'){nameValue = 'Tanzania';}
        else if(codeValue == 'TH'){nameValue = 'Thailand';}
        else if(codeValue == 'TL'){nameValue = 'Timor-Leste';}
        else if(codeValue == 'TG'){nameValue = 'Togo';}
        else if(codeValue == 'TK'){nameValue = 'Tokelau';}
        else if(codeValue == 'TO'){nameValue = 'Tonga';}
        else if(codeValue == 'TT'){nameValue = 'Trinidad and Tobago';}
        else if(codeValue == 'TN'){nameValue = 'Tunisia';}
        else if(codeValue == 'TR'){nameValue = 'Turkey';}
        else if(codeValue == 'TM'){nameValue = 'Turkmenistan';}
        else if(codeValue == 'TC'){nameValue = 'Turks and Caicos Islands';}
        else if(codeValue == 'TV'){nameValue = 'Tuvalu';}
        else if(codeValue == 'UG'){nameValue = 'Uganda';}
        else if(codeValue == 'UA'){nameValue = 'Ukraine';}
        else if(codeValue == 'AE'){nameValue = 'United Arab Emirates';}
        else if(codeValue == 'GB'){nameValue = 'United Kingdom';}
        else if(codeValue == 'US'){nameValue = 'United States of America';}
        else if(codeValue == 'UM'){nameValue = 'United States Minor Outlying Islands';}
        else if(codeValue == 'VI'){nameValue = 'United States Virgin Islands';}
        else if(codeValue == 'UY'){nameValue = 'Uruguay, Eastern Republic of';}
        else if(codeValue == 'UZ'){nameValue = 'Uzbekistan';}
        else if(codeValue == 'VU'){nameValue = 'Vanuatu';}
        else if(codeValue == 'VE'){nameValue = 'Venezuela';}
        else if(codeValue == 'VN'){nameValue = 'Vietnam';}
        else if(codeValue == 'WF'){nameValue = 'Wallis and Futuna';}
        else if(codeValue == 'EH'){nameValue = 'Western Sahara';}
        else if(codeValue == 'YE'){nameValue = 'Yemen';}
        else if(codeValue == 'ZM'){nameValue = 'Zambia';}
        else if(codeValue == 'ZW'){nameValue = 'Zimbabwe';}
        else if(codeValue == 'UK'){nameValue = 'United Kingdom';}
        return nameValue;
  }
  
   public static string wiredValue( String Curr, String CompName )
  {
  	string wiredval ='';
  	if(Curr == 'EUR' && CompName == 'GRI American European LLC')
  	{
  		wiredval = 'Beneficiary: <b>GRI American European LLC</b><br/>Reference: Invoice number + account number (see above) <br/> Bank: UBS AG<br/>Bank address: Postfach 1770, CH-8098 Zurich, Switzerland<br/>IBAN: CH82 0023 0230 2183 6460R<br/>BIC(SWIFT):   UBSWCHZH80A<br/>Account number: 0230-00218364.60R';
  	}
  	else if(Curr == 'USD' && CompName == 'GRI American European LLC')
  	{
  		wiredval = 'Beneficiary:<b> GRI American European LLC</b><br/>Reference: Invoice number + account number (see above)<br/>Bank: UBS AG<br/>Bank Address: Postfach 1770, CH-8098 Zurich, Switzerland<br/>IBAN: CH81 0023 0230 2183 6470 F<br/>BIC(SWIFT):   UBSWCHZH80A<br/> Account number: 230-218364.70 F<br/>';
  	}
  	else if(Curr == 'GBP' && CompName == 'GRI American European LLC')
  	{
  		wiredval = 'Payable to: <b>GRI American European LLC</b><br/>Reference: Invoice number + name of event (see "Details" above) <br/>Bank: UBS AG<br/>Bank address: Postfach 1770, CH-8098 Zurich, Switzerland<br/> BIC (SWIFT): UBSWCHZH80A<br/>Reference: Invoice number + name of event (see "Details" above) <br/>IBAN No: CH36 0023 0230 2183 6475 Y<br/>Account number: 0230-00218364.75Y <br/>';
  	}
  	else if(Curr == 'USD' && CompName == 'GRI USA Inc')
  	{
  		wiredval = ' Beneficiary: <b>GRI American European LLC</b><br/>Reference: Invoice number + account number (see above) <br/>Bank: UBS AG<br/>Bank Address: Postfach 1770, CH-8098 Zurich, Switzerland<br/>IBAN: CH43 0023 0230 3838 4702 L<br/>BIC(SWIFT):   UBSWCHZH80A<br/>Account number: 0230 383847.02L <br/>';
  	}
    return wiredval;
  }
}