/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe teste da classe OpportunityCriaEventAttendeeInOrder
*
*
* NAME: OpportunityCriaEventAttendeeInOrderTest.cls
* AUTHOR: VMDL                                                 DATE: 12/08/2016
*******************************************************************************/
@isTest
private class OpportunityCriaEventAttendeeInOrderTest {

	private static Id REC_OPP_SPEX = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
	private static Id REC_ORD_ADD_PASSES = RecordTypeMemory.getRecType( 'Order', 'Aditional_Passes' );
	private static Id REC_ORD_EVENT_SPONSOR = RecordTypeMemory.getRecType( 'Order', 'Event_Sponsor' );
	private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');

	static testMethod void testeFuncionalInsert()
	{
		Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

		Product2 produto = SObjectInstance.createProduct2();
		produto.NAME = 'Teste prod';
		Database.insert(produto);

		PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
		Database.insert(entradaDePreco);

		Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
		Database.insert(conta);

		Contact contato = SObjectInstance.createContato(conta.Id);
		Database.insert(contato);

		Event__c evento = SObjectInstance.createEvent();
    evento.Type__c = 'Open Event';
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

		Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_SPEX);
		oportunidade.Contact__c = contato.Id;
		oportunidade.StageName = 'Negotiation';
		oportunidade.Event__c = evento.Id;
		Database.insert(oportunidade);

		OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
		Database.insert(produtoOpp);

		Order lOrder = SObjectInstance.createorder(REC_ORD_ADD_PASSES, conta.Id, idCatalogoPreco, evento.Id);
		lOrder.OpportunityId = oportunidade.Id;
		Database.insert(lOrder);

		Test.startTest();
		oportunidade.StageName = 'Approved';
		Database.update(oportunidade);
		Test.stopTest();
	}

	static testMethod void testeFuncionalUpdate()
	{
		Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

		Product2 produto = SObjectInstance.createProduct2();
		produto.NAME = 'Teste prod';
		Database.insert(produto);

		PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
		Database.insert(entradaDePreco);

		Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
		Database.insert(conta);

		Contact contato = SObjectInstance.createContato(conta.Id);
		Database.insert(contato);

		Event__c evento = SObjectInstance.createEvent();
    evento.Type__c = 'Open Event';
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

		Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_SPEX);
		oportunidade.Contact__c = contato.Id;
		oportunidade.StageName = 'Negotiation';
		oportunidade.Event__c = evento.Id;
		Database.insert(oportunidade);

		OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
		Database.insert(produtoOpp);

		Order lOrder = SObjectInstance.createorder(REC_ORD_ADD_PASSES, conta.Id, idCatalogoPreco, evento.Id);
		lOrder.OpportunityId = oportunidade.Id;
		Database.insert(lOrder);

		Event_Attendee__c lEventAttendee = SObjectInstance.createEventAttendee(oportunidade.Id, conta.Id, contato.Id, evento.Id);
		Database.insert(lEventAttendee);

		Test.startTest();
		oportunidade.StageName = 'Approved by Finance';
		Database.update(oportunidade);
		Test.stopTest();
	}

	static testMethod void testeFuncionalDelete()
	{
		Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

		Product2 produto = SObjectInstance.createProduct2();
		produto.NAME = 'Teste prod';
		Database.insert(produto);

		PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
		Database.insert(entradaDePreco);

		Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
		Database.insert(conta);

		Contact contato = SObjectInstance.createContato(conta.Id);
		Database.insert(contato);

		Event__c evento = SObjectInstance.createEvent();
    evento.Type__c = 'Open Event';
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

		Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_SPEX);
		oportunidade.Contact__c = contato.Id;
		oportunidade.StageName = 'Negotiation';
		oportunidade.Event__c = evento.Id;
		Database.insert(oportunidade);

		OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
		Database.insert(produtoOpp);

		Order lOrder = SObjectInstance.createorder(REC_ORD_ADD_PASSES, conta.Id, idCatalogoPreco, evento.Id);
		lOrder.OpportunityId = oportunidade.Id;
		Database.insert(lOrder);

		Event_Attendee__c lEventAttendee = SObjectInstance.createEventAttendee(oportunidade.Id, conta.Id, contato.Id, evento.Id);
		Database.insert(lEventAttendee);

		Test.startTest();
		oportunidade.StageName = 'Prospect';
		Database.update(oportunidade);
		Test.stopTest();
	}


}