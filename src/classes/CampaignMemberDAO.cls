/**
 * Created by Eric Bueno on 24/02/2019.
 */

public with sharing class CampaignMemberDAO {

    private static final CampaignMemberDAO instance = new CampaignMemberDAO();
    private CampaignMemberDAO(){}
    public static CampaignMemberDAO getInstance(){
        return instance;
    }

    public List<CampaignMember> findCampaignMemberFromDigitalCampaignByCampaignMemberId(Set<Id> setCampaignMemberId){
        return [
                SELECT
                        Id,
                        Name,
                        CampaignId,
                        Campaign.Name,
                        Campaign.OwnerId,
                        Campaign.Club__c,
                        Campaign.DigitalCampaignEvent__c,
                        Campaign.DigitalCampaignEvent__r.DigitalCampaignOpportunityOwner__c,
                        Campaign.DigitalCampaignEvent__r.CurrencyIsoCode,
                        Campaign.Digital_Funnel_Stage__c,
                        Campaign.Magazine__c,
                        Campaign.Opportunity_Type__c,
                        ContactId,
                        Contact.Name,
                        Contact.AccountId,
                        Opportunity__c,
                        Opportunity__r.Id,
                        Opportunity__r.Digital_Funnel_Stage__c,
                        LeadId,
                        Lead.LeadSource,
                        Type
                FROM
                        CampaignMember
                WHERE
                Id =: setCampaignMemberId
                AND
                Campaign.Digital_Funnel_Stage__c != null
                AND
                Campaign.Digital_Campaign_Type__c != null
                AND
                Campaign.DigitalCampaignEvent__c != null
                AND
                Campaign.Digital_Funnel__c = true
        ];
    }

    public List<Opportunity> findOpportunityByContactId( Set<Id> setContactId){

        return [
                SELECT
                        Id,
                        Contact__c,
                        Event__c,
                        Digital_Funnel_Stage__c,
                        RecordTypeId,
                        RecordType.DeveloperName
                FROM
                        Opportunity
                WHERE
                        Contact__c IN: setContactId
                AND
                        Campaign.Digital_Funnel__c = true
        ];

    }
    
    public List<Lead> findLeadById(Set<Id> setLeadId){
        return [
                SELECT
                        Id,
                        Company
                FROM
                        Lead
                WHERE
                        Id IN : setLeadId
        ];
        
    }

    public List<Account> findAccountByName(Set<String> setNameCompany){
        return [
                SELECT
                        Id,
                        Name
                FROM
                        Account
                WHERE
                        Name IN: setNameCompany
        ];

    }

    public List<User> findUserCRM(){
        return [
                SELECT
                        Id,
                        Name
                FROM
                        User
                WHERE
                        Name = 'CRM Team'
        ];
    }

}