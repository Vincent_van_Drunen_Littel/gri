/*******************************************************************************
*                               Cloud2b - 2017
*-------------------------------------------------------------------------------
*
* Classe responsável por testar a classe responsável por Criar um novo Membership
* quando opportunity é aprovada e leva o RecordType = Membership.
*
* NAME: OpportunityNewMemberTest.cls
* AUTHOR: CFA                                                  DATE: 22/03/2017
*******************************************************************************/
@isTest
private class OpportunityNewMemberTest {
  
  private static final Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
  private static final Integer LOTE = 150;
  private static Product2 lProduct2;
  private static Account lAccount;
  private static Contact lContact;
  private static PricebookEntry lPricebookEntry;
  private static Club__c lClubc;
  
  static{
      
    Application_Config__c appConfig = new Application_Config__c(Name = 'BucketUserId', Value__c = '00536000004lQpq');
    insert appConfig;       
    
    lProduct2 = SObjectInstance.createProduct2();
    database.insert(lProduct2);
       
    lAccount = SObjectInstance.createAccount2();
    lAccount.BillingCity = 'São Pualo'; 
    lAccount.BillingCountry = 'Brazil' ;
    lAccount.BillingState = 'SP';
    lAccount.BillingStreet = 'Rua teste, 10';
    database.insert(lAccount);
      
    lContact = SObjectInstance.createContato(lAccount.Id);
    database.insert(lContact);  
      
    // Criação do PricebookEntry
    id pbkId = SObjectInstance.catalogoDePrecoPadrao2();
    lPricebookEntry = SObjectInstance.entradaDePreco(pbkId,lProduct2.Id);
    database.insert(lPricebookEntry);      
    
    lClubc = SObjectInstance.createClub();
    lClubc.Name = 'Infra Club LatAm';
    database.insert(lClubc);      
    
  }
    static testMethod void testeFuncionalMember() {
      
      // Criação da Opportunity
      Opportunity lOpportunity = SObjectInstance.createOpportunidadeMenber(lAccount.Id, RT_OPP_MEMBERSHIP, lContact.Id, lClubc.Id);
        system.debug('EADLOG -lOpportunity-> ' + lOpportunity);
        database.insert(lOpportunity);

      lOpportunity.StageName = 'Approved by Finance';
      
      test.startTest();
        database.update(lOpportunity);
      test.stopTest();
      
      list<Contract> lContractAfter = [SELECT Name,Opportunity__c FROM Contract WHERE Opportunity__c =: lOpportunity.Id];
      system.debug('lContractAfter 1 = '+ lContractAfter);
      
      system.assertEquals(1,lContractAfter.size(),'lContractAfter diferente de 1');
      
    }
    
    static testMethod void testeFuncionalMemberLote() {
      
      list<Opportunity> lListOpportunityLote = new list<Opportunity>();
      // Criação da Opportunity
      for( Integer i = 0; i < LOTE; i++)
      {
        Opportunity lOpportunityLote = SObjectInstance.createOpportunidadeMenber(lAccount.Id, RT_OPP_MEMBERSHIP, lContact.Id, lClubc.Id);
        lListOpportunityLote.add(lOpportunityLote);
      }
      database.insert(lListOpportunityLote);

      for( Integer i = 0; i < LOTE; i++)
      {
        lListOpportunityLote[i].StageName = 'Approved by Finance';
      }
      
      test.startTest();
        database.update(lListOpportunityLote);
      test.stopTest();
     
      list<Contract> lContractAfterLote = [SELECT Name,Opportunity__c FROM Contract WHERE Opportunity__c =: lListOpportunityLote];
      system.debug('lContractAfter lote = '+ lContractAfterLote);
      
      system.assertEquals(LOTE,lContractAfterLote.size(),'lContractAfter está diferente do LOTE');
    }
    
    static testMethod void testeMemberError() {
      
      list<OpportunityLineItem> lListOpportunityLineItem = new list<OpportunityLineItem>();
      list<Opportunity> lListOpportunityUpdate = new list<Opportunity>();
      // Criação da Opportunity
      Opportunity lOpportunity = SObjectInstance.createOpportunidadeMenber(lAccount.Id, RT_OPP_MEMBERSHIP, lContact.Id, lClubc.Id);
      database.insert(lOpportunity);
      
      test.startTest();
        database.insert(lListOpportunityLineItem);
      test.stopTest();
     
      list<Contract> lContractAfter = [SELECT Name,Opportunity__c FROM Contract WHERE Opportunity__c =: lOpportunity.Id];
      system.debug('lContractAfter erro = '+ lContractAfter);
      
      system.assertEquals(0,lContractAfter.size(),'lContractAfter está diferente de zero');
      
    }

}