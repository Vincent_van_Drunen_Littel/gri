@isTest(SeeAllData=true) 

    private class CreateInvoicePageTest {

               
     @isTest(SeeAllData=true)  static void test_method_one() {

       String invId = 'a0v36000001LWzF';
        System.debug('####££££££££ invId: ' + invId);


           Test.startTest();
    Invoice__c inv = new Invoice__c();
    inv.id = invId;

    System.debug('####££££££££ inv: ' + inv);
    
    Invoice__c[] newinv= [select Id,Custom_French_Spain_Message__c,ThanksMessage__c, VAT__c, VAT_Country__c,Country_Name__c,Company_BadgeName__c, FirstName__c, LastName__c, Name, Company_Vat_Number__c, Billing_Street__c, Billing_City__c, Billing_PostCode__c, Billing_Country__c, VAT_Number__c, Invoice_Date__c, Customer_Order_Number__c, Order_Description__c, CurrencyIsoCode, Total_Net_Amount__c, Discount__c, VAT_Amount__c, Invoice_Total__c,Reunion_Fee_for_Swiss_Vat__c,Membership_fee_for_Swiss_vat__c,VAT_Amount_for_swiss_vat__c,Total_Invoice_amount_for_swiss__c,wired_payment_details__c,regemail__c from Invoice__c where id =: invId];

    System.debug('####££££££££ newinv: ' + newinv);
    
    ApexPages.StandardController sc = new ApexPages.standardController(inv);

    CreateInvoicePage inv1 = new CreateInvoicePage(sc);

    PageReference pageRef = Page.GRIServicesLtdPDFPage;
    pageRef.getParameters().put('Id', invId);
    System.debug('####££££££££ pageRef: ' + pageRef);
    System.Test.setCurrentPage(pageRef);
    pageRef.setRedirect(true);

    //CreateInvoicePage rwc = new CreateInvoicePage(new ApexPages.StandardController(inv));
    
    //ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(newinv);
    
    //System.debug('####££££££££ sc1: ' + sc1);

    //CreateInvoicePage rwc1 = new CreateInvoicePage(sc);
     //CreateInvoicePage rwc2 = new CreateInvoicePage(sc1);
     
      //System.debug('####££££££££ rwc2 : ' + rwc2 );
    

 List <Invoice__c> lstAccount = new List<Invoice__c>();


 lstAccount.add(inv); 
  

  Test.setCurrentPage(Page.GRIServicesLtdPDFPage);

  ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstAccount);

  stdSetController.setSelected(lstAccount);

  CreateInvoicePage ext = new CreateInvoicePage(stdSetController);

 Test.stopTest();

 }

}