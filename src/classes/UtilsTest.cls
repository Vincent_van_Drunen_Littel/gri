/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Classe que faz o teste de cobertura de teste da classe Utils
* NAME: UtilsTest.cls
* AUTHOR: LRSA                                                 DATE: 09/01/2014
*******************************************************************************/
@isTest
private class UtilsTest 
{
    static testMethod void myUnitTest()
    {
      test.starttest();
      system.assertEquals( '1,234,56', Utils.formatValue( '1234.56', 2 ) );
      system.assertEquals( '-1,234,56', Utils.formatValue( '-1234.56', 2 ) );
      system.assertEquals( '-1,56', Utils.formatValue( '-1.56', 2 ) );
      system.assertEquals( '-1,60', Utils.formatValue( '-1.6', 2 ) );
      system.assertEquals( '-12,345,60', Utils.formatValue( '-12345.6', 2 ) );
      system.assertEquals( '12,345,00', Utils.formatValue( '12345', 2 ) );
      system.assertEquals( '12.34.5', Utils.formatValue( '12.34.5', 2 ) );
      system.assertEquals( 0, Utils.getNum( null ) );
      Utils.formatCEP(null);
      Utils.formatCEP('1'.repeat(8));
      Utils.formatCPF(null);
      Utils.formatCPF('1'.repeat(11));
      Utils.formatCNPJ(null);
      Utils.formatCNPJ('1'.repeat(14));
      Utils.getWrittenForm(1234567890);
      Utils.subirCobertura2();
      test.stoptest();
    }
}