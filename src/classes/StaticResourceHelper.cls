/**
 * @author Renato Matheus Simião
 * @version 1.0
 * @date 2018-06-12
 * @description Generic class for return the file content of specified resource
 */
public with sharing class StaticResourceHelper {
	
	public static String getFileContent(String pResourceName) {

		return getFileContent(pResourceName, '');
	}

	public static String getFileContent(String pResourceName, String pFileName) {

		StaticResource static_resource = [SELECT Id, Name, SystemModStamp
                                  			FROM StaticResource 
                                  		   WHERE Name = :pResourceName
                                  		   LIMIT 1];
		
		String url_file_ref = '/resource/'
		                    + String.valueOf(((DateTime)static_resource.get('SystemModStamp')).getTime())
		                    + '/' 
		                    + static_resource.get('Name')
		                    + pFileName;

		PageReference file_ref = new PageReference(url_file_ref);

		return Test.isRunningTest() ? 'OK' : file_ref.getContent().toString();
	}
}