/**
 * Created by Eric Bueno on 20/01/2019.
 */

@IsTest
public with sharing class EventRESTTest {

    @TestSetup
    static void setup(){

        TestDataFactory.init();

        Integer nConPerAcc = 1;
        // Create Club__c
        Club__c club = TestDataFactory.createClubs( TestDataFactory.CLUB_RE_LATAM );

        // Create Event__c
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent', club);
        theGreatEvent.Vip_Dinner_Included__c = true;
        theGreatEvent.Publish_on_Website__c = true;
        update theGreatEvent;

        // Create campaign of event
        List<Campaign> listCampaigns = TestDataFactory.createCampaigns(1,'BRL');
        listCampaigns[0].Digital_Campaign_Type__c = 'WhatsApp';
        listCampaigns[0].Event__c = theGreatEvent.Id;
        update listCampaigns;

        // Create Contact and Account
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(nConPerAcc);

        // Create opportunity
        List<Opportunity> listOpportunities = TestDataFactory.createEventOpps2(listContacts, theGreatEvent, club, System.today()+10);

        // Create EventAttendee related to opportunity and event__c
        List<Event_Attendee__c> listEventAttendees = TestDataFactory.createEventAttendee(listOpportunities, theGreatEvent);
        for(Event_Attendee__c eventAttendee : listEventAttendees){
            eventAttendee.ShowAttendeeonWebsite__c = true;
        }
        update listEventAttendees;

        // create a Session related to event
        Session__c session = TestDataFactory.createSessionWithEvent(theGreatEvent);
        session.Status__c = 'Confirmed';
        session.Publish_Online__c = true;
        update session;

        // create a Session Attendee related from Session and opportunity
        List<Attendee__c> listAttendees = TestDataFactory.createSessionAttendeesWithSession(listOpportunities,theGreatEvent,session);

        for(Attendee__c attendee : listAttendees){
            attendee.Session_Status__c = 'Confirmed';
        }
        update listAttendees;

        // create an account to sponsor
        Account accountSponsor = TestDataFactory.createAccounts(new List<String>{'Account Sponsor'})[0];
        // create a club to sponsor
        Club__c club2 = TestDataFactory.createClubs('Club Sponsor');
        // create a Sponsor to event
        Order order = TestDataFactory.createOrderWithActEvtClub(accountSponsor, theGreatEvent, club2);

    }

    @IsTest
    static void testFindOppByEventIdWhenEventNotFoundOrNotMatchCriteria(){

        Event__c event = [SELECT Id FROM Event__c LIMIT 1];

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/api/event/find-opp-by-event-id';
        request.httpMethod = 'GET';
        request.addParameter('event-id',String.valueOf(event.Id).substring(0,String.valueOf(event.Id).length() - 2) + '00');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        EventREST eventRESTInstance = new EventREST();
        eventRESTInstance.setReturnMeta(true);
        
        EventREST.processGet();
        RestResponse response = RestContext.response;

        Test.stopTest();

        ReturnRestTO ret = (ReturnRestTO) JSON.deserialize(response.responseBody.toString(),ReturnRestTO.class);

        System.assertEquals(ret.meta.message,'Event not found or does not match search criteria');
        System.assertEquals(ret.meta.statusHttp,400);
        System.assertEquals(response.statusCode,400);
        System.assert(ret.data == null);

    }

    @IsTest
    static void testFindOppByEventId(){

        Event__c event = [SELECT Id,Publish_on_Website__c FROM Event__c LIMIT 1];

        event.Publish_on_Website__c = true;
        update event;

        Opportunity Opportunity = [SELECT Id,Name,Extra_Financial_Status__c,StageName,Closing_Stage__c,Contact__r.Show_on_Website__c,RecordType.Name, (SELECT Id, Session_Status__c, Session__c, Session__r.Status__c, Publish_Event_on_Website__c, Publish_Online__c FROM Opportunity_sessions__r) FROM Opportunity WHERE Event__c =: event.Id LIMIT 1];
        opportunity.StageName = 'WON';
        opportunity.Extra_Financial_Status__c = 'Dinner Cancel Kept';

        update opportunity;

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/api/event/find-opp-by-event-id';
        request.httpMethod = 'GET';
        request.addParameter('event-id',event.Id);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        EventREST.processGet();

        Test.stopTest();

        // get response in map
        Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        SiteTO.EventTO eventTO = (SiteTO.EventTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('data')), SiteTO.EventTO.class);
        ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')),ReturnRestTO.MetaTO.class);


        // Testa se o retorno foi positivo
        System.assertEquals(RestContext.response.statusCode,200);
        System.assertEquals(metaTO.statusHttp,200);
        System.assert(eventTO != null);

        // Testa se vieram as campanhas
        System.assert(!eventTO.campaings.isEmpty());
        // Testa se vieram os atendentes do evento
        System.assert(!eventTO.eventAttendes.isEmpty());
        // Testa se vieram as sessoes
        System.assert(!eventTO.sessions.isEmpty());
        // Testa se vieram os atendentes das sessoes
        System.assert(!eventTO.sessions[0].sessionAttendes.isEmpty());
        // Testa se veio o evento correto
        System.assertEquals(eventTO.id,event.Id);
        // Testa se veio o contato correto
        System.assertEquals(eventTO.companies[0].contactId, opportunity.Contact__c);
        // Testa se vieram os patrocinadores
        System.assert(!eventTO.sponsors.isEmpty());
        // Testa se veio a conta do patrocinador
        System.assert(eventTO.sponsors[0].accountId != null);
        // Testa se veio o club do patrocinador
        System.assert(eventTO.sponsors[0].clubId != null);

    }

    @IsTest
    static void testFindById(){

        Event__c event = [SELECT Id,Publish_on_Website__c FROM Event__c LIMIT 1];

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/api/event/find-by-id';
        request.httpMethod = 'GET';
        request.addParameter('event-id',event.Id);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        EventREST.processGet();

        Test.stopTest();

        // get response in map
        Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        SiteTO.EventTO eventTO = (SiteTO.EventTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('data')), SiteTO.EventTO.class);
        ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')),ReturnRestTO.MetaTO.class);


        System.assertEquals(RestContext.response.statusCode,200);
        System.assertEquals(metaTO.statusHttp,200);
        System.assert(eventTO != null);
        System.assertEquals(eventTO.id,event.Id);

    }

    @IsTest
    static void testFindByIdWhenNotFound(){

        Event__c event = [SELECT Id,Publish_on_Website__c FROM Event__c LIMIT 1];

        String eventId = String.valueOf(event.Id);
        eventId = eventId.substring(0,eventId.length() - 2) + '00';

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/api/event/find-by-id';
        request.httpMethod = 'GET';
        request.addParameter('event-id',eventId);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        EventREST.processGet();

        Test.stopTest();

        // get response in map
        Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        SiteTO.EventTO eventTO = (SiteTO.EventTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('data')), SiteTO.EventTO.class);
        ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')),ReturnRestTO.MetaTO.class);


        System.assertEquals(RestContext.response.statusCode,400);
        System.assertEquals(metaTO.statusHttp,400);
        System.assert(eventTO == null);
        System.assertEquals(metaTO.message,'Event not found');

    }

}