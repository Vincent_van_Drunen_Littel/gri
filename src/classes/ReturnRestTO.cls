/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 * Interface object for service responses.
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 15/01/19  EricB  First version.
 *
 */

public without sharing class ReturnRestTO {

    public Object data {get; set;}
    public MetaTO meta {get; set;}

    public ReturnRestTO(String serviceName){
        this.meta = new MetaTO();
        this.meta.service = serviceName;
        this.meta.dateExecution = System.now();
    }

    public class MetaTO {
        public String service {get; set;}
        public Datetime dateExecution {get; set;}
        public Integer statusHttp {get; set;}
        public String message {get; set;}
    }




}