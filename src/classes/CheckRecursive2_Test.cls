@isTest
public class CheckRecursive2_Test {
	@isTest 
    public static void test_check_consistenvy()
    {
        Test.StartTest();
        System.debug('%% Test not Disabled');
        CheckRecursive2.DisableCheckRecursive2OnTest=False;
        CheckRecursive2.GDPRTriggerHandlerIsActive=True;
        System.assertEquals(True, CheckRecursive2.GDPRTriggerHandlerIsActive);
        CheckRecursive2.AlteraEventAttendeeIsActive=True;
        System.assertEquals(True, CheckRecursive2.AlteraEventAttendeeIsActive);
        CheckRecursive2.GlobalApprovalProcessIsActive=True;
        System.assertEquals(True,  CheckRecursive2.GlobalApprovalProcessIsActive);
        CheckRecursive2.AtualizaLastOpportunityWonIsActive=True;
        System.assertEquals(True, CheckRecursive2.AtualizaLastOpportunityWonIsActive);
        CheckRecursive2.AtualizaSessionAttendeeIsActive=True;
        System.assertEquals(True, CheckRecursive2.AtualizaSessionAttendeeIsActive);
        CheckRecursive2.CriaEventAttendeeIsActive=True;
        System.assertEquals(True, CheckRecursive2.CriaEventAttendeeIsActive);
        CheckRecursive2.CountOppOpenEventIsActive=True;
        System.assertEquals(True, CheckRecursive2.CountOppOpenEventIsActive);
        CheckRecursive2.UpdatePricebookEProdutoIsActive=True;
        System.assertEquals(True, CheckRecursive2.UpdatePricebookEProdutoIsActive);
        //CheckRecursive2.CountOppOpenEventIsActive=True;
        //System.assertEquals(True, CheckRecursive2.CountOppOpenEventIsActive);
        CheckRecursive2.ApprovalCommitteeManagerIsActive=True;
        System.assertEquals(True, CheckRecursive2.ApprovalCommitteeManagerIsActive);
        
        System.debug('%% Test Disabled');
        CheckRecursive2.DisableCheckRecursive2OnTest=True;
        CheckRecursive2.GDPRTriggerHandlerIsActive = True;
        System.assertEquals(False, CheckRecursive2.GDPRTriggerHandlerIsActive);
        CheckRecursive2.AlteraEventAttendeeIsActive = True;
        System.assertEquals(False, CheckRecursive2.AlteraEventAttendeeIsActive);
        CheckRecursive2.GlobalApprovalProcessIsActive = True;
        System.assertEquals(False,  CheckRecursive2.GlobalApprovalProcessIsActive);
        CheckRecursive2.AtualizaLastOpportunityWonIsActive = True;
        System.assertEquals(False, CheckRecursive2.AtualizaLastOpportunityWonIsActive);
        CheckRecursive2.AtualizaSessionAttendeeIsActive = True;
        System.assertEquals(False, CheckRecursive2.AtualizaSessionAttendeeIsActive);
        CheckRecursive2.CriaEventAttendeeIsActive = True;
        System.assertEquals(False, CheckRecursive2.CriaEventAttendeeIsActive);
        CheckRecursive2.CountOppOpenEventIsActive = True;
        System.assertEquals(False, CheckRecursive2.CountOppOpenEventIsActive);
        CheckRecursive2.UpdatePricebookEProdutoIsActive = True;
        System.assertEquals(False, CheckRecursive2.UpdatePricebookEProdutoIsActive);
        //CheckRecursive2.DiamondMemberAutoApprovalIsActive = True;
        //System.assertEquals(True, CheckRecursive2.DiamondMemberAutoApprovalIsActive);
        CheckRecursive2.ApprovalCommitteeManagerIsActive = True;
        System.assertEquals(False, CheckRecursive2.ApprovalCommitteeManagerIsActive);

        Test.StopTest();
    }
       
    @isTest 
    public static void test_disabled()
    {
     	//CheckRecursive2.DisableCheckRecursive2OnTest;   
        System.assertEquals(True, CheckRecursive2.DisableCheckRecursive2OnTest, '***');
        System.assertEquals(False, CheckRecursive2.DisableCheckRecursive2, '*********');
        System.assertEquals(True, CheckRecursive2.isDisabled(), '$$$$$$$$$$$$');
        CheckRecursive2.DisableCheckRecursive2OnTest=False;
        CheckRecursive2.DisableCheckRecursive2=True;
        System.assertEquals(True, CheckRecursive2.m_DisableCheckRecursive2, '?');
        System.assertEquals(False, CheckRecursive2.m_DisableCheckRecursive2OnTest, '#########');
        System.assertEquals(False, CheckRecursive2.isDisabled(), '???????');
    }
    
    @isTest 
    public static void test_conter()
    {
        System.assertEquals(1, CheckRecursive2.conter());
        System.assertEquals(' [1] ', CheckRecursive2.conterValue(), 
                            'Actual$'+CheckRecursive2.conterValue()+'$ -- Expected$' + ' [1]'+'$');
    }
    
    @isTest
    public static void test_test()
    {
         System.assertEquals(True, CheckRecursive2.isDisabled(), 'It is a test context, it should be disabled');
    }
    
    @isTest 
    public static void test_trigger()
    {
        List<Contact> listContacts = [SELECT Id,FirstName,LastName,AccountId FROM Contact LIMIT 30];
        Club__c BrClub = [SELECT Id,Name FROM Club__c WHERE Name=:HelperClub.RE_BRAZIL];
        Club__c InClub = [SELECT Id,Name FROM Club__c WHERE Name=:HelperClub.RE_INDIA];
        
        List<Contract> memberships1 = TestDataFactory.createMemberships(listContacts, 
            BrClub, System.today()+365);
        System.assertEquals(30, memberships1.size(), 'memberships were not created');
        
        CheckRecursive2.DisableCheckRecursive2OnTest=False;
        List<Contract> memberships2 = TestDataFactory.createMemberships(listContacts, 
            InClub, System.today()+365);
        System.assertEquals(0, memberships2.size(), '#######################');
        
    }
    
   	@testSetup 
    static void buildData()
    {
        TestDataFactory.init();
        TestDataFactory.createContactsWithDefaultAccount(30);
        TestDataFactory.createClubs(new List<String>{ HelperClub.RE_LATAM, HelperClub.RE_BRAZIL, HelperClub.RE_INDIA});

    }
    
    
    
}