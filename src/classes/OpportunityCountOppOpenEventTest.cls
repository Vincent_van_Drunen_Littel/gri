/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste da classe "OpportunityCountOppOpenEvent".
*
* NAME: OpportunityCountOppOpenEventTest.cls
* AUTHOR: RVR                                                  DATE: 04/05/2016
* MODIFIER: VMDL                                               DATE: 04/07/2016
*******************************************************************************/
@isTest
private class OpportunityCountOppOpenEventTest 
{
  private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
  private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');

  static testMethod void testeFuncionalOpenEvent()
  {
    Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

    Product2 produto = SObjectInstance.createProduct2();
    produto.NAME = 'Teste prod';
    produto.IsActive = true;
    Database.insert(produto);

    PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
    Database.insert(entradaDePreco);

    Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(conta);

    Contact contato = SObjectInstance.createContato(conta.Id);
    contato.Open_Events_Counter__c = 1;
    contato.Exclusive_Events_Counter__c = 1;
    Database.insert(contato);

    Event__c evento = SObjectInstance.createEvent();
    evento.Type__c = 'Open Event';
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
    oportunidade.Contact__c = contato.Id;
    oportunidade.StageName = 'Approved by Finance';
    oportunidade.Event__c = evento.Id;
    oportunidade.Event_Held__c = true;
    oportunidade.Finance_Status__c = 'Paid';
    oportunidade.Payment_Method__c = 'International Transfer';
    oportunidade.GRI_Branch__c = 'GRI Brazil';
    Database.insert(oportunidade);

    OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
    produtoOpp.TotalPrice = 0; 
    Database.insert(produtoOpp);

    Test.startTest();
    oportunidade.StageName = 'Won';
    Database.update(oportunidade);
    Test.stopTest();
  }

  static testMethod void testeFuncionalClubEvent()
  {
    Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

    Product2 produto = SObjectInstance.createProduct2();
    produto.NAME = 'Teste prod';
    produto.IsActive = true;
    Database.insert(produto);

    PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
    Database.insert(entradaDePreco);

    Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(conta);

    Contact contato = SObjectInstance.createContato(conta.Id);
    contato.Open_Events_Counter__c = 1;
    contato.Exclusive_Events_Counter__c = 1;
    Database.insert(contato);

    Event__c evento = SObjectInstance.createEvent();
    evento.Type__c = 'Club Event';
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
    oportunidade.Contact__c = contato.Id;
    oportunidade.StageName = 'Approved by Finance';
    oportunidade.Event__c = evento.Id;
    oportunidade.Event_Held__c = true;
    oportunidade.Finance_Status__c = 'Paid';
    oportunidade.Payment_Method__c = 'International Transfer';
    oportunidade.GRI_Branch__c = 'GRI Brazil';
    Database.insert(oportunidade);

    OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
    produtoOpp.TotalPrice = 0;
    Database.insert(produtoOpp);

    Test.startTest();
    oportunidade.StageName = 'Won';
    Database.update(oportunidade);
    Test.stopTest();
  }

}