/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * A test class for HelperSObject
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */
 
@IsTest
public class HelperSObject_Test {
    private static final String LOGTAG='@@ HelperSObject_Test:'; 

	@isTest
	public static void testDataTime() 
	{
		Test.startTest();
		DateTime myDateTime = DateTime.newInstance(1997, 1, 31, 7, 8, 16);
		Date myDate = Date.newInstance(1997, 1, 31);
		System.assertEquals(myDate, HelperSObject.Date(myDateTime), 'Datatime and Date dont match');
		Test.stopTest();
	}

	@isTest
	public static void testSObjec() 
	{
		List<Contact> listContacts = [SELECT Id,FirstName,LastName FROM Contact];

		Test.startTest();

		for(Integer i=0; i<listContacts.size(); i++ )
		{
			if(math.mod(i, 2)==0)
				listContacts[i].FirstName='Even';
			else
				listContacts[i].FirstName='Odd';
		}

        System.debug('**Test filterField()**');
		List<Contact> filteredContacts1 =  HelperSObject.filterField(
			listContacts, 'FirstName', 'Even');
		System.assertEquals(10, filteredContacts1.size(), 
			'The number of filteredContacts (Even) is different than expected');

		listContacts[0].FirstName = 'First Contact';
		List<Contact> filteredContacts2 =   HelperSObject.filterField(
			listContacts, 'FirstName', new List<String>{'Even', 'Odd'});
		System.assertEquals(19, filteredContacts2.size(), 
			'The number of filteredContacts (Even, Odd) is different than expected');
		System.assertEquals(9, HelperSObject.countFieldOcurrence('Even', listContacts, 'FirstName'), 
			'The number of ocurrences of FirstName `Even` is different than expected');

        System.debug('**Test setFromField()**');
		Set<String> theFirstNameStrSet = HelperSObject.setFromField(listContacts, 'FirstName');
		System.assertEquals(3, theFirstNameStrSet.size(), 
			'The FirstName Set has a size different than expected');

        System.debug('**Test JSON**');
        String strListJson1 = HelperSObject.json(listContacts, 'Id,FirstName,LastName');
        String strListJson2 = HelperSObject.json(listContacts, ' Id,   FirstName,LastName  ');
        System.assertNotEquals(strListJson1, '');
        System.assertNotEquals(strListJson2, NULL);
        System.assertEquals(strListJson1, strListJson2);

        for(Integer i=0; i<listContacts.size(); i++)
        {
            String strJson1 = HelperSObject.json(listContacts[i], 'Id,FirstName,LastName');
            String strJson2 = HelperSObject.json(listContacts[i], ' Id,   FirstName,LastName  ');  
            System.assertNotEquals(strJson1, '');
            System.assertNotEquals(strJson2, NULL);
            System.assertEquals(strJson1, strJson2);
        }

        System.debug('**Test Map**');
        Map<Id, String> mapContactId_FirstName = HelperSObject.mapIdField(listContacts, 'FirstName');
        Map<String, SObject> mapFirstName_Contacts = HelperSObject.mapFromField(listContacts, 'FirstName');
        System.assertEquals(mapContactId_FirstName.size(), listContacts.size());
        //System.assertEquals(mapFirstName_Contacts.size(), 2, 
        //    'Error on Map test HelperSObject.mapFromField() for evaluating mapFirstName_Contacts.'+
        //    ' mapFirstName_Contacts.size()='+mapFirstName_Contacts.size());

		Test.stopTest();
	}

    @isTest 
    public static void test_mapExeptions()
    {
        List<Contact> listContacts = [SELECT Id,FirstName,LastName FROM Contact LIMIT 10];
        listContacts.add(NULL);
        Map<String, SObject> mapFirstName_Contact = HelperSObject.mapFromField(listContacts, 'FirstName');
        System.assertEquals(mapFirstName_Contact.size(), 10);

        try
        {
            Map<String, SObject> mapAbacate_Contact = HelperSObject.mapFromField(listContacts, 'Abacate');
        }
        catch(HelperSObject.HelperSObjectException e)
        {
            System.debug('exception caught! e='+e);
        }



    }

    @isTest 
    public static void test_jsonExceptions()
    {
        List<Contact> listContacts = [SELECT Id,FirstName,LastName FROM Contact LIMIT 10];

        try
        {
            String strListJson2 = HelperSObject.json(listContacts, ' Id, Ovo, Abacate, AbobrinhaFrita');
        }
        catch(HelperSObject.HelperSObjectException e)
        {
            System.debug('exception caught! e='+e);
        }

    }

	@isTest
	public static void test_was_changed_to()
	{
		Contact ccBefore = new Contact(FirstName='Abacaxi', LastName='Test');
		Contact ccAfter = new Contact(FirstName='Banana', LastName='Test');
		Contact ccWrong = new Contact(FirstName='Caqui', LastName='Test');
    	System.assertEquals(True, HelperSObject.wasChangedTo(ccAfter, ccBefore, 'FirstName', 'Banana'), '*'); 
    	System.assertEquals(False, HelperSObject.wasChangedTo(ccWrong, ccBefore, 'FirstName', 'Banana'), '**');
    	System.assertEquals(False, HelperSObject.wasChangedTo(ccAfter, ccAfter, 'FirstName', 'Banana'), '***');
	}

    @isTest
    public static void test_opp_rectype()
    {
        System.assertNotEquals(NULL, HelperSObject.opportunity_recordTypeIdEvents(), 
            'HelperSObject.opportunity_recordTypeIdEvents() returned NULL');
        System.assertNotEquals(NULL, HelperSObject.opportunity_recordTypeIdMembership(), 
            'HelperSObject.opportunity_recordTypeIdMembership() returned NULL');
        System.assertNotEquals(NULL, HelperSObject.opportunity_recordTypeIdMagazine(), 
            'HelperSObject.opportunity_recordTypeIdMagazine() returned NULL');
        System.assertNotEquals(NULL, HelperSObject.opportunity_recordTypeIdSPEX(), 
            'HelperSObject.opportunity_recordTypeIdSPEX() returned NULL');
        System.assertNotEquals(NULL, HelperSObject.opportunity_recordTypeIdSmartusEvent(), 
            'HelperSObject.opportunity_recordTypeIdSmartusEvent() returned NULL');
        System.assertNotEquals(NULL, HelperSObject.opportunity_recordTypeIdSmartusSpex(), 
            'HelperSObject.opportunity_recordTypeIdSmartusSpex() returned NULL');        
    }

    @isTest 
    public static void test_filterByProductCode()
    {
        // Sandbox tests
        //List<String> listId = new List<String>{'0063600000TU377', '0063600000V0VjV', '0063600000V0oX9', '0060x00000278FC', '0060x00000278FD', '0060x000005eeBx' };
        //List<Opportunity> listOpps = [SELECT Id, Name, (SELECT Id, ProductCode FROM OpportunityLineItems) FROM Opportunity WHERE Id IN :listId];
        //List<Opportunity> listFilteredOpps = HelperSObject.opportunity_filterByProductCode(listOpps, 'P-000049');
        //System.debug('listOpps='+HelperSObject.json(listOpps, 'Id'));
        //System.debug('listFilteredOpps='+HelperSObject.json(listFilteredOpps, 'Id'));

        List<Contact> listContacts = [SELECT Id, FirstName, LastName, AccountId FROM Contact];
        Club__c clubReBrazil = TestDataFactory.createClubs(HelperSObject.CLUB_RE_BRAZIL);
        Event__c eventReBrazil = TestDataFactory.createEvent('Event Re Brazil', clubReBrazil);
        List<Opportunity> listOpps = TestDataFactory.createEventOpps(listContacts, eventReBrazil, clubReBrazil, System.today()+5);
        String productCode = 'P-0001';
        TestDataFactory.addProductToOpportunity(TestDataFactory.createProduct(productCode), listOpps);
        List<Opportunity> updatedOppsWithOli = 
            [SELECT Id, Name, (SELECT Id, ProductCode FROM OpportunityLineItems) 
            FROM Opportunity WHERE Id IN :listOpps];
        List<Opportunity> listFilteredOpps = HelperSObject.opportunity_filterByProductCode(updatedOppsWithOli, productCode);
        System.debug(LOGTAG+'listOpps='+HelperSObject.json(listOpps, 'Id'));
        System.debug(LOGTAG+'listFilteredOpps='+HelperSObject.json(listFilteredOpps, 'Id'));
        System.debug(LOGTAG+'updatedOppsWithOli='+HelperSObject.json(updatedOppsWithOli, 'Id'));
        System.debug(LOGTAG+'listOpps.size()='+listOpps.size());
        System.debug(LOGTAG+'listFilteredOpps.size()='+listFilteredOpps.size());
        System.debug(LOGTAG+'updatedOppsWithOli.size()='+updatedOppsWithOli.size());
        System.assertEquals(listOpps.size(), listFilteredOpps.size(), 'listOpps.size(), listFilteredOpps.size()');
        System.assertEquals(listOpps.size(), updatedOppsWithOli.size(), 'listOpps.size(), updatedOppsWithOli.size()');
        System.assertNotEquals(0, listOpps.size(), '0, listOpps.size()');
    }


    @isTest 
    public static void test_recTypeException()
    {
        try
        {
            HelperSObject.recordType_get('A_Churrasqueira_com_controle_remoto', 'Puta_vida');
        }
        catch(HelperSObject.HelperSObjectException e)
        {
            System.debug('ok');
        }
    }

    @isTest
    public static void test_assetTriggerOrTest()
    {
        HelperSObject.assertTriggerTest();
    }


    @isTest
    public static void test_helperMembership() 
    {
        TestDataFactory.createContactsWithDefaultAccount(3);
        TestDataFactory.createClubs( new List<String> {
            HelperSObject.CLUB_RE_BRAZIL, HelperSObject.CLUB_RE_EUROPE,  HelperSObject.CLUB_RE_LATAM, HelperSObject.CLUB_INFRA_LATAM});
        List<Contact> listTheContact = [SELECT Id,FirstName,LastName,AccountId FROM Contact];
        Contact contact0 = listTheContact[0];
        Contact contact1 = listTheContact[1];
        Contact contact2 = listTheContact[2];
        Club__c brClub = [SELECT Id,Name FROM Club__c WHERE Name=:HelperSObject.CLUB_RE_BRAZIL];
        List<Contract> memberships = TestDataFactory.createMemberships(
            new List<Contact>{contact0, contact1, contact2}, brClub, System.today()+365);
        Contract membershipBr0 = memberships[0];
        Contract membershipBr1 = memberships[1];
        Contract membershipBr2 = memberships[2];

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Start tests
        ///////////////////////////////////////////////////////////////////////////////////////////

        Test.startTest();

        membershipBr1.Allowed_Open_Events_Worldwide__c = '5';
        membershipBr1.Allowed_Exclusive_Events__c = '3';
        Integer eventsWorldwide = HelperSObject.membership_AllowedOpenEventsWorldwide(membershipBr1);
        Integer exclusiveEvents = HelperSObject.membership_AllowedExclusiveClubMeetings(membershipBr1);
        System.assertEquals(5, eventsWorldwide, 'Allowed_Open_Events_Worldwide__c and AllowedOpenEventsWorldwide dont match');
        System.assertEquals(3, exclusiveEvents, 'Allowed_Exclusive_Events__c and AllowedExclusiveClubMeetings dont match');

        membershipBr1.Allowed_Open_Events_Worldwide__c = 'All';
        membershipBr1.Allowed_Exclusive_Events__c = 'All';
        exclusiveEvents = HelperSObject.membership_AllowedExclusiveClubMeetings(membershipBr1);
        eventsWorldwide = HelperSObject.membership_AllowedOpenEventsWorldwide(membershipBr1);
        System.assertEquals(365, exclusiveEvents, 'Allowed_Open_Events_Worldwide__c and AllowedOpenEventsWorldwide dont match');
        System.assertEquals(365, eventsWorldwide, 'Allowed_Exclusive_Events__c and AllowedExclusiveClubMeetings dont match');

        membershipBr1.Allowed_Open_Events_Worldwide__c = 'Bubassauro';
        membershipBr1.Allowed_Exclusive_Events__c = 'Charmander';
        exclusiveEvents = HelperSObject.membership_AllowedExclusiveClubMeetings(membershipBr1);
        eventsWorldwide = HelperSObject.membership_AllowedOpenEventsWorldwide(membershipBr1);
        System.assertEquals(0, exclusiveEvents, 'Allowed_Open_Events_Worldwide__c and AllowedOpenEventsWorldwide dont match');
        System.assertEquals(0, eventsWorldwide, 'Allowed_Exclusive_Events__c and AllowedExclusiveClubMeetings dont match');

        membershipBr0.Status = HelperSObject.MEMBERSHIP_STATUS_ACTIVATED;
        membershipBr1.Status = HelperSObject.MEMBERSHIP_STATUS_EXPIRED;
        membershipBr2.Status = HelperSObject.MEMBERSHIP_STATUS_DRAFT;

        System.assertEquals(True, HelperSObject.membership_isActive(membershipBr0), 'Membership status is wrong');
        System.assertEquals(True, HelperSObject.membership_isActive(membershipBr1), 'Membership status is wrong');
        System.assertEquals(False, HelperSObject.membership_isActive(membershipBr2), 'Membership status is wrong');

        // TODO
        List<Contact> listContacts = new List<Contact>{contact0, contact1};
        List<Contract> listMemberships = new List<Contract>{membershipBr1, membershipBr2};
        //Map<Id, Boolean> contactMembershipMapClub = HelperSObject.membership_contactHasMembership(listContacts, listMemberships, HelperSObject.CLUB_RE_BRAZIL);
        //Map<Id, Boolean> contactMembershipMapAny = HelperSObject.membership_contactHasMembership(listContacts, listMemberships, '');
        System.debug('TODO: replace this by a System.assertEquals()');

        System.assertNotEquals(new Set<String>{''}, HelperSObject.setDiamondPlatinumMemberships());
        System.assertNotEquals(new Set<String>(), HelperSObject.setDiamondPlatinumMemberships());
        System.assertNotEquals(NULL, HelperSObject.setDiamondPlatinumMemberships());

        Test.stopTest();
        
    }

    //@isTest
   // public static void 
    //{
//
   // }

	@testSetup 
    static void buildData()
    {
        TestDataFactory.init();
        TestDataFactory.createContactsWithDefaultAccount(20);
    }
}