/*******************************************************************************
*                               Cloud2b - 2017
*-------------------------------------------------------------------------------
*
* Classe responsavel por atualizar o campo Last_Opportunity_Won__c de Account
* quando a Opportunity for modificada para o status Ganho
*
* NAME: OpportunityAtualizaLastOpportunityWon.cls
* AUTHOR: CFA                                                  DATE: 10/03/2017
*******************************************************************************/
public with sharing class OpportunityAtualizaLastOpportunityWon {
    
  public static void execute()
  {
  	TriggerUtils.assertTrigger();

  	list<Opportunity> lListOpportunity = new list<Opportunity>();
  	list<Account> lListAccount = new list<Account>();
  	set<Id> lSetIdAccount = new set<Id>();
  	
  	for(Opportunity opp : (list<Opportunity>)trigger.new) 
  	{
  	  if(String.isNotBlank(opp.AccountId) && TriggerUtils.wasChangedTo(opp, opportunity.StageName, 'Won'))
  	    lSetIdAccount.add(opp.AccountId);
  	}
  	
  	if (lSetIdAccount.isEmpty()) return;
  	
  	for(Account acc : [SELECT Id, Last_Opportunity_Won__c FROM Account WHERE Id =: lSetIdAccount])
  	{
  	  acc.Last_Opportunity_Won__c = system.today();
  	  lListAccount.add(acc);
  	}
  	
  	if(!lListAccount.isEmpty()) database.update(lListAccount);
  }
}