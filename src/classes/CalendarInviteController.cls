public class CalendarInviteController {
    private static string googleClientID{get;set;}
    private static string googleSecretCode{get;set;}
    private static string redirectURI{get;set;}
    private static string authorizationCode{get;set;}
    private string state = '';
    private static string accessToken;
    private static string refreshToken;
    private string expiresIn;
    private string tokenType;
    public String inviteType{get;set;}
    public List<String> selectedEmailTypes {get;set;}
    public List<Id> eventIds {get;set;}
    public String otherEmail {get;set;}
    public Id EventId {get;set;}
    public string otherEmailAddress{get;set;}
    public string opptyId{get;set;}
    private static string tokenEndPointValue{get;set;}
    private static string endPointValue{get;set;}
    public Opportunity opptyObj{get;set;}
    private List<Calendar_Invitee__c> calendarInviteeLst = new List<Calendar_Invitee__c>();
    public Boolean isDisableBusinessEmail{get;set;}
    public Boolean isDisableSecretaryEmail1{get;set;}
    public Boolean isDisableSecretaryEmail2{get;set;}
    public Boolean businessEmail{get;set;}
    public boolean secretaryEmail1{get;set;}
    public Boolean secretaryEmail2{get;set;}
    private map<Id,string> CalinviteToAttendeeStatusMap = new map<Id,string>();
    public Boolean isHavingBusinessEmail{get;set;}
    public boolean isHavingSecretaryEmail1{get;set;}
    public boolean isHavingSecretaryEmail2{get;set;}
    private Map<Id,string> emailAcceptedMap = new Map<Id,string>();
    private static string batchAttendeeEndpoint;
    public Map<Id,string> removeAttendeeMap = new Map<Id,string>();
    public Map<Id,string> calInviteToEmailMap = new Map<Id,string>();
    private Map<Id,string> emailParentAcceptedMap = new Map<Id,string>();
    public Boolean isYesValue {get;set;}
    
    
    public CalendarInviteController(ApexPages.StandardSetController controller){
    
        opptyId = ApexPages.currentPage().getParameters().get('id');

        if(opptyId != '' && opptyId != null){

            inviteType ='';
            otherEmailAddress ='';
            businessEmail = true;
            secretaryEmail1 = false;
            secretaryEmail2 = false;
            
            isDisableBusinessEmail = false;
            isDisableSecretaryEmail1 = false;
            isDisableSecretaryEmail2 = false;
            isYesValue = false;
            getCalInviteeObjDetails(opptyId);
        }    
    }
    private void getCalInviteeObjDetails(string opptyId){
        
        
        calendarInviteeLst = [SELECT id, InviteeEmail__c,Calendar_Invite__c,InvitationLimitReached__c, Invite_Status__c,Calendar_Invite__r.ParentCalendarInvite__c,Opportunity__c FROM Calendar_Invitee__c WHERE Opportunity__c=:opptyId Order By CreatedDate ASC];
        system.debug('@@calendarInviteeLst'+calendarInviteeLst);
        
        
        set<string> contactEmails =  new set<string>();
        set<string> productIds = new set<string>();
        for(Invite_Type_Product_Code__mdt inviteTpePrdObj: [ SELECT DeveloperName,ProductId__c FROM Invite_Type_Product_Code__mdt ]){
            productIds.add(inviteTpePrdObj.ProductId__c);
        }

   
        opptyObj = [SELECT id, Event__c, Contact__c, (SELECT Quantity, UnitPrice, TotalPrice, PricebookEntry.Name, PricebookEntry.Product2.Family,PricebookEntryId 
                    FROM OpportunityLineItems where Product2Id IN:productIds) FROM Opportunity WHERE id=:opptyId];
        system.debug('@@opptyObj'+opptyObj);
        system.debug('@@opptyObj.OpportunityLineItems'+opptyObj.OpportunityLineItems);
        
        isDisableBusinessEmail = true;
        
        contact contactDetails = getContact(opptyObj.Contact__c);
        system.debug('@@contactDetails'+contactDetails);
        isHavingSecretaryEmail1 = false;
        isHavingSecretaryEmail2 = false;
        isHavingBusinessEmail = false;
        if(contactDetails.Email != null){
            isHavingBusinessEmail = true;
        }
        if(contactDetails.Secretary_Email_1__c != null){
            isHavingSecretaryEmail1= true;
        }
        if(contactDetails.Secretary_Email_2__c != null){
            isHavingSecretaryEmail2 = true;
        }
        for(Calendar_Invitee__c calInviteeObj:calendarInviteeLst){
            if(calInviteeObj.InviteeEmail__c != null){
                system.debug('@@calInviteeObj.InviteeEmail__c'+calInviteeObj.InviteeEmail__c);
                system.debug('calInviteeObj.Calendar_Invite__r.ParentCalendarInvite__c'+calInviteeObj.Calendar_Invite__r.ParentCalendarInvite__c);
                if(contactDetails.Email != null && calInviteeObj.InviteeEmail__c.contains(contactDetails.Email)){
                    CalinviteToAttendeeStatusMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.Invite_Status__c);
                    calInviteToEmailMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.InviteeEmail__c);
                }
                if(calInviteeObj.Invite_Status__c =='Yes'){
                    
                    if(contactDetails.Email != null && calInviteeObj.InviteeEmail__c.contains(contactDetails.Email)){
                       emailAcceptedMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.Invite_Status__c);
                       
                    }
                    if(contactDetails.Secretary_Email_1__c != null && calInviteeObj.InviteeEmail__c.contains(contactDetails.Secretary_Email_1__c)){
                        isDisableSecretaryEmail1 = true;
                        emailAcceptedMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.Invite_Status__c);
                    }
                    if(contactDetails.Secretary_Email_2__c != null && calInviteeObj.InviteeEmail__c.contains(contactDetails.Secretary_Email_2__c)){
                        isDisableSecretaryEmail2 = true;
                        emailAcceptedMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.Invite_Status__c);
                    }
                
                }
            }
        }
    }
    public contact getContact(Id contactId){
        return [SELECT Id, Name,Secretary_Email_1__c, Secretary_Email_2__c, Email FROM Contact WHERE id=:contactId];
    }
    /*public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Day 1','Day 1'));
        options.add(new SelectOption('Day 2','Day 2'));
        options.add(new SelectOption('Dinner','Dinner (If applicable)'));
        options.add(new SelectOption('Lunch','Lunch (If applicable)'));
        return options;
    }*/
    
    public List<cEvent> inviteList {get; set;}
    
    public List<cEvent> getInvites() {
        if(inviteList == null){
            inviteList = new List<cEvent>();
            List<SelectOption> options = new List<SelectOption>();
            eventIds = new List<Id>();
            string messageBody = '';
            opptyId = ApexPages.currentPage().getParameters().get('id');
            getCalInviteeObjDetails(opptyId);
            system.debug('@@opptyId'+opptyId);
            
            if(opptyObj.Event__c != null ){
                system.debug('@@CalinviteToAttendeeStatusMap'+CalinviteToAttendeeStatusMap);
                string calendarMsg ='';
                List<Calendar_Invite__c> finalCalendarInvites = new List<Calendar_Invite__c>();
                Map<Id,Attendee__c> specialLunchSessionMap =  new Map<Id,Attendee__c>();
                Map<Id,Attendee__c> advisoryBoardSessionMap =  new Map<Id,Attendee__c>();
                
                for(Attendee__c attendeeObj:[SELECT Session_Status__c, Opportunity__c,  Event__c , Session__c,  Session__r.Event__c, Session__r.Type__c, Session__r.Status__c FROM Attendee__c WHERE Event__c=:opptyObj.Event__c AND Opportunity__c =: opptyObj.Id]){
                    system.debug('@@attendeeObj.Session_Status__c'+attendeeObj.Session_Status__c+'@@attendeeObj.Session__r.Type__c'+attendeeObj.Session__r.Type__c+'@@attendeeObj.Session__r.Status__c'+attendeeObj.Session__r.Status__c);
                    if(attendeeObj.Session_Status__c == 'Confirmed' && attendeeObj.Session__r.Type__c =='Advisory Board Meeting' && (attendeeObj.Session__r.Status__c =='Confirmed' || attendeeObj.Session__r.Status__c =='In production') && attendeeObj.Session__r.Event__c == opptyObj.Event__c ){
                        advisoryBoardSessionMap.put(attendeeObj.Event__c,attendeeObj);
                    }else if(attendeeObj.Session_Status__c == 'Confirmed' && attendeeObj.Session__r.Type__c =='Special Lunch' && (attendeeObj.Session__r.Status__c =='Confirmed' || attendeeObj.Session__r.Status__c =='In production') && attendeeObj.Session__r.Event__c == opptyObj.Event__c ){
                        specialLunchSessionMap.put(attendeeObj.Event__c,attendeeObj);
                    }
                    
                }
                system.debug('@@advisoryBoardSessionMap'+advisoryBoardSessionMap);
                system.debug('@@specialLunchSessionMap'+specialLunchSessionMap);
                
                for(Calendar_Invite__c calInviteObj:[SELECT id, Name,Venue__r.name, Venue_Address__c,CalendarInviteesCount__c, Calendar_Entry_Name__c,Invite_Type__c,Event__c, Event__r.Vip_Dinner_Included__c,Start_date__c, End_date__c,Event__r.status__c,ParentCalendarInvite__c FROM Calendar_Invite__c WHERE Event__c =:opptyObj.Event__c AND IsInviteesLimitReached__c= false ORDER BY Start_date__c ASC] ){
                    system.debug('@@calInviteObj.Event__r.Vip_Dinner_Included__c'+calInviteObj.Event__r.Vip_Dinner_Included__c);
                    
                    if(calInviteObj.Invite_Type__c == 'Dinner' && (!opptyObj.OpportunityLineItems.isEmpty() || calInviteObj.Event__r.Vip_Dinner_Included__c==true)){
                        finalCalendarInvites.add(calInviteObj);
                    }else if(calInviteObj.Invite_Type__c == 'Special Lunch' && specialLunchSessionMap != null && specialLunchSessionMap.containsKey(calInviteObj.Event__c)){
                        finalCalendarInvites.add(calInviteObj);
                    }else if(calInviteObj.Invite_Type__c == 'Advisory Board Meeting' && advisoryBoardSessionMap != null && advisoryBoardSessionMap.containsKey(calInviteObj.Event__c)){
                        finalCalendarInvites.add(calInviteObj);
                    }else if(calInviteObj.Invite_Type__c != 'Dinner' && calInviteObj.Invite_Type__c != 'Special Lunch' && calInviteObj.Invite_Type__c != 'Advisory Board Meeting' ){
                        finalCalendarInvites.add(calInviteObj);
                    }
                    
                }
                for(Calendar_Invite__c calInviteObj:finalCalendarInvites){
                    if(CalinviteToAttendeeStatusMap.containskey(calInviteObj.Id)){
                        calendarMsg = calInviteObj.Calendar_Entry_Name__c +' - '+CalinviteToAttendeeStatusMap.get(calInviteObj.Id);
                        system.debug('@@calendarMsg'+calendarMsg);  
                    }else{
                        calendarMsg = calInviteObj.Calendar_Entry_Name__c+' - ';
                    }
                    options.add(new SelectOption(calInviteObj.Id,calendarMsg));
                    String d;
                    String dateFormat = 'dd/MM/YYYY\' \'HH:mm\' \'';
                    String endDateFormat = '\' \'HH:mm\' \'';
                    String startDateOutput = calInviteObj.Start_Date__c.format('dd/MM/yyyy');
                    String endDateOutput = calInviteObj.End_Date__c.format('dd/MM/yyyy');
                    
                    if(calInviteObj.Start_Date__c.date().daysBetween(calInviteObj.End_Date__c.date()) == 0){

                        d = calInviteObj.Start_Date__c.format(dateFormat) + '-' + calInviteObj.End_Date__c.format(endDateFormat);
                    }else {
                        d = calInviteObj.Start_Date__c.format(dateFormat) + '-' + calInviteObj.End_Date__c.format(dateFormat);
                    }
                    system.debug('@@d'+d);
                    if(CalinviteToAttendeeStatusMap.containsKey(calInviteObj.ParentCalendarInvite__c)){
                        /*if(calInviteToEmailMap.containsKey(calInviteObj.ParentCalendarInvite__c)){
                            if(CalinviteToAttendeeStatusMap.get(calInviteObj.ParentCalendarInvite__c) !='Yes')
                                removeAttendeeMap.put(calInviteObj.ParentCalendarInvite__c,calInviteToEmailMap.get(calInviteObj.ParentCalendarInvite__c))
                        }*/
                        inviteList.add(new cEvent(calInviteObj,d,CalinviteToAttendeeStatusMap.get(calInviteObj.ParentCalendarInvite__c)));
                    }else{
                        inviteList.add(new cEvent(calInviteObj,d,CalinviteToAttendeeStatusMap.get(calInviteObj.Id)));
                    }
                    
                    eventIds.add(calInviteObj.Id);
                }
            }
        }
        return inviteList;
    }
  
    
    public List<SelectOption> getEmailTypes() {
        List<SelectOption> options = new List<SelectOption>();
        selectedEmailTypes = new List<string>();
        options.add(new SelectOption('Business Email','Business Email'));
        options.add(new SelectOption('Secretary Email 1','Secretary Email 1'));
        options.add(new SelectOption('Secretary Email 2','Secretary Email 2'));
        
        
        return options;
    }
    
    public List<String> getselectedEmailTypes() {
            return selectedEmailTypes;
         }

     public void setselectedEmailTypes(List<String> selectedEmailTypes) {
         this.selectedEmailTypes = selectedEmailTypes;
     }
    
    //this method is being called from AttendeeSyncService class
    public static String getGoogleAccessToken(){
        List<Google_Calendar_API_Detail__mdt> mtdCalendarApiLst =  [ SELECT GoogleClientId__c,GoogleBatchEndpoint__c,GoogleEndPoint__c,GoogleRedirectURI__c,GoogleSecretCode__c,GoogleTokenEndpoint__c, GoogleUserCode__c,GoogleAttendeeEndPoint__c FROM Google_Calendar_API_Detail__mdt ]; 
        system.debug('@@mtdCalendarApiLst'+mtdCalendarApiLst);
        if(mtdCalendarApiLst != null && mtdCalendarApiLst.size()>0){
            googleClientID = mtdCalendarApiLst[0].GoogleClientId__c;
            googleSecretCode = mtdCalendarApiLst[0].GoogleSecretCode__c;
            redirectURI = mtdCalendarApiLst[0].GoogleRedirectURI__c;
            authorizationCode = mtdCalendarApiLst[0].GoogleUserCode__c;
            tokenEndPointValue = mtdCalendarApiLst[0].GoogleTokenEndpoint__c;
            
            endPointValue = mtdCalendarApiLst[0].GoogleAttendeeEndPoint__c;
            batchAttendeeEndpoint = mtdCalendarApiLst[0].GoogleBatchEndpoint__c;
       }
       return getAccessToken();
    }
         
    public static string getAccessToken(){
        
        string bodyRequest = '';
        
        system.debug('@@authorizationCode'+authorizationCode);       
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(tokenEndPointValue);

        bodyRequest = 'refresh_token=' + EncodingUtil.urlEncode(authorizationCode, 'UTF-8');
       
        bodyRequest += '&client_id=' + EncodingUtil.urlEncode(googleClientID, 'UTF-8');
        bodyRequest += '&client_secret=' + EncodingUtil.urlEncode(googleSecretCode, 'UTF-8');
        bodyRequest += '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8');
       
        bodyRequest += '&grant_type=refresh_token';
        req.setBody(bodyRequest);     
        req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setTimeout(10000);
        
        if(!Test.isRunningTest()){
                        
            HttpResponse res = h.send(req);   
            map<string, string> jsonValues = new map<string, string>();
          
            System.debug('Response Value:'+res.getBody());
            jsonValues = CalendarInviteHandler.parseJSONToMap(res.getBody());
            if(jsonValues.containsKey('error')){ 
            }else{
                //Try to get a cell value in the Google Spreadsheet
                accessToken = jsonValues.get('access_token');            
            }
        }else{
            accessToken = 'as23as32ease23asd';
        }
        
        if(accessToken <> ''){
           return accessToken ;
        } else{
        return 'Invalid Refresh token';
        }
           
    }
    private List<Calendar_Invite__c> returnCalendarInviteLst(set<Id> selectedEventIds ){
        system.debug('@@emailAcceptedMap'+emailAcceptedMap);
        List<Calendar_Invite__c> calInviteLst = [SELECT id, Name, Timezone__c, Calendar_Entry_Name__c,Venue__r.name, Venue_Address__c,Invite_Others__c,Modify_Event__c, See_Guest_List__c,Invite_Type__c,
                                                Event__r.name,Start_Date__c,End_Date__c, GoogleCalendarEventId__c,GoogleCalendarId__c, Description__c,ParentCalendarInvite__c,
                                                (SELECT id, InviteeEmail__c,Calendar_Invite__c, Invite_Status__c,Opportunity__c FROM Calendar_Invitees__r) FROM Calendar_Invite__c WHERE Id IN :selectedEventIds AND IsInviteesLimitReached__c = false AND Id NOT IN : emailAcceptedMap.keySet()];
        return calInviteLst;
    }

    public pageReference retrieveGoogleAccessToken(){
        
        List<Google_Calendar_API_Detail__mdt> mtdCalendarApiLst =  [ SELECT GoogleClientId__c,GoogleEndPoint__c,GoogleBatchEndpoint__c,GoogleRedirectURI__c,GoogleSecretCode__c,GoogleTokenEndpoint__c, GoogleUserCode__c,GoogleAttendeeEndPoint__c FROM Google_Calendar_API_Detail__mdt ]; 
        system.debug('@@mtdCalendarApiLst'+mtdCalendarApiLst);
        if(mtdCalendarApiLst != null && mtdCalendarApiLst.size()>0){
            googleClientID = mtdCalendarApiLst[0].GoogleClientId__c;
            googleSecretCode = mtdCalendarApiLst[0].GoogleSecretCode__c;
            redirectURI = mtdCalendarApiLst[0].GoogleRedirectURI__c;
            authorizationCode = mtdCalendarApiLst[0].GoogleUserCode__c;
            tokenEndPointValue = mtdCalendarApiLst[0].GoogleTokenEndpoint__c;
            
            endPointValue = mtdCalendarApiLst[0].GoogleAttendeeEndPoint__c;
            batchAttendeeEndpoint = mtdCalendarApiLst[0].GoogleBatchEndpoint__c;
       }
        
       system.debug('@@eventIds'+eventIds);
        Set<Id> selectedEventIds = new Set<Id>();
        Set<Id> selectedParentIds = new Set<Id>();
        for(cEvent c:inviteList){
            System.debug(c.selected + '-'+ c.invite.Id);
            if(c.selected){
                if(c.attendeeStatus !='Yes'){
                    selectedEventIds.add(c.invite.Id);
                    //if(c.invite.CalendarInviteesCount__c <=0 )
                     //   selectedParentIds.add(c.invite.ParentCalendarInvite__c);
                }
            }
        } 
        system.debug('@@selectedEventIds'+selectedEventIds);
        system.debug('@@selectedParentIds'+selectedParentIds);
        
        //!businessEmailAcceptedMap.containsKey(calInviteObj.Id)
        system.debug('@@emailAcceptedMap'+emailAcceptedMap);
        
        //List<Calendar_Invite__c> calInviteLst = [SELECT id, Name, Timezone__c, Calendar_Entry_Name__c,Venue__r.name, Venue_Address__c, Event__r.name,Start_Date__c,End_Date__c, GoogleCalendarEventId__c,GoogleCalendarId__c FROM Calendar_Invite__c WHERE Id IN :eventIds]; 
        
        List<Contact> conLst = new List<Contact>();
        string contactId =  opptyObj.Contact__c;
        contact contObj = null;
        if(contactId != ''){
            
             contObj = getContact(opptyObj.Contact__c);
        }
        
        set<string> setOfEmails =  new set<string>();
        system.debug('@@selectedEmailTypes'+selectedEmailTypes);
        
       if(businessEmail){
           if(contObj.email != null)
               setOfEmails.add(contObj.email);
       }
       /*if(secretaryEmail1)
           setOfEmails.add(contObj.Secretary_Email_1__c);
       if(secretaryEmail2)
           setOfEmails.add(contObj.Secretary_Email_2__c);
        
        if(otherEmail != ''){
            setOfEmails.add(otherEmail);
        }*/
        system.debug('@@selectedEventIds'+selectedEventIds+'@@setOfEmails'+setOfEmails);
        Map<Id,set<string>> notifiedEmailsMap = new Map<Id,set<string>>();
        set<Id> finalIdsTobeChecked = new set<Id>();
        finalIdsTobeChecked.AddAll(selectedEventIds);
        //finalIdsTobeChecked.AddAll(selectedParentIds);
        system.debug('@@finalIdsTobeChecked'+finalIdsTobeChecked);
        
        
        List<Calendar_Invitee__c> delCalendarInvites = [SELECT id,InviteeEmail__c,Calendar_Invite__c,Opportunity__c FROM Calendar_Invitee__c WHERE InviteeEmail__c IN :setOfEmails AND Opportunity__c=:opptyId AND Invite_Status__c !='Yes' AND (Calendar_Invite__c IN:finalIdsTobeChecked)];
        
        Map<Id,string> parentRemoveEmailMap = new Map<Id,string>();
        set<Id> deletingCalInviteeIds = new set<Id>();
        // To collect emails to send notification
        for(Calendar_Invitee__c calInviteeObj : delCalendarInvites) {
            parentRemoveEmailMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.InviteeEmail__c);
            if(!notifiedEmailsMap.containsKey(calInviteeObj.Calendar_Invite__c)){
                 notifiedEmailsMap.put(calInviteeObj.Calendar_Invite__c, new Set<String>{calInviteeObj.InviteeEmail__c});

            }else{
                 notifiedEmailsMap.get(calInviteeObj.Calendar_Invite__c).add(calInviteeObj.InviteeEmail__c);
            }
            
        }
        String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
        string accessTokenVal = getAccessToken();
      
        system.debug('@@selectedParentIds'+selectedParentIds);
        
        for(Calendar_Invite__c calndrInviteRecord:[SELECT id, Name, Timezone__c, Calendar_Entry_Name__c,Venue__r.name, Venue_Address__c,Invite_Others__c,Modify_Event__c, See_Guest_List__c,Invite_Type__c,
                                                Event__r.name,Start_Date__c,End_Date__c, GoogleCalendarEventId__c,GoogleCalendarId__c, Description__c,ParentCalendarInvite__c,
                                                (SELECT id, InviteeEmail__c,Calendar_Invite__c, Invite_Status__c,Opportunity__c FROM Calendar_Invitees__r ) FROM Calendar_Invite__c WHERE Id IN :parentRemoveEmailMap.keySet()]){
                
                system.debug('@@parentRemoveEmailMap'+parentRemoveEmailMap);
                
                if(calndrInviteRecord.GoogleCalendarEventId__c != null && calndrInviteRecord.GoogleCalendarId__c != null && parentRemoveEmailMap.containsKey(calndrInviteRecord.id)){
                    Http h = new Http();
                    HttpRequest req = new HttpRequest();
                     
                    req.setEndpoint(endPointValue+calndrInviteRecord.GoogleCalendarId__c+'/events/'+calndrInviteRecord.GoogleCalendarEventId__c );
                       
                    set<string> finalSetofEmails =  new set<string>();
                    Map<string,String> inviteeEmails = new Map<string,String>();
                    if(calndrInviteRecord.GoogleCalendarId__c !=''){
                        finalSetofEmails.add(calndrInviteRecord.GoogleCalendarId__c);
                        inviteeEmails.put(calndrInviteRecord.GoogleCalendarId__c,'needsAction');
                    }
                    
                    for(Calendar_Invitee__c calendarInviteeObj:calndrInviteRecord.Calendar_Invitees__r){
                        system.debug('@@opptyObj.Id'+opptyObj.Id+'@@calendarInviteeObj.Opportunity__c'+calendarInviteeObj.Opportunity__c);
                        //if(opptyId != calendarInviteeObj.Opportunity__c){
                            String status = calendarInviteeObj.Invite_Status__c == 'Yes' ? 'accepted' :calendarInviteeObj.Invite_Status__c == 'No' ? 'declined' : calendarInviteeObj.Invite_Status__c == 'Maybe' ? 'tentative' : 'needsAction'; 
                            inviteeEmails.put(calendarInviteeObj.InviteeEmail__c,status);
                        
                        finalSetofEmails.add(calendarInviteeObj.InviteeEmail__c);
                    }
                    System.debug('########'+finalSetofEmails);
                    if(setOfEmails.size()>0){
                        for(string strObj:setOfEmails){
                            if(strObj != null){
                                finalSetofEmails.add(strObj);
                                inviteeEmails.put(strObj, 'needsAction');
                            }
                        }
                    }
                    system.debug('@@finalSetofEmails'+finalSetofEmails);
                    system.debug('@@finalSetofEmails'+inviteeEmails);
                   
                    if(finalSetofEmails.contains(parentRemoveEmailMap.get(calndrInviteRecord.Id))){
                        finalSetofEmails.remove(parentRemoveEmailMap.get(calndrInviteRecord.Id));
                    }
                   
                    system.debug('@@finalSetofEmails'+finalSetofEmails);
                    
                    Integer finalHoursToBeAdded = 0;
                    
                    if(calndrInviteRecord.Timezone__c != null){
                        List<string> dateLst;
                        List<string> finalHoursLst;
                        Boolean IsNegativeValue = false;
                        if(calndrInviteRecord.Timezone__c.contains('+')){
                            dateLst =  calndrInviteRecord.Timezone__c.split('\\+');
                        
                        }else if(calndrInviteRecord.Timezone__c.contains('-')){
                            IsNegativeValue = true;
                            dateLst = calndrInviteRecord.Timezone__c.split('\\-');
                        }
                        system.debug('@dateLst'+dateLst);
                        if(dateLst != null && dateLst.size()>1 ){
                            finalHoursLst = dateLst[1].split('\\:');
                        }
                        system.debug('@finalHoursLst'+finalHoursLst);
                        if(finalHoursLst != null){
                            if(!IsNegativeValue){
                                finalHoursToBeAdded = -1*(Integer.valueOf(finalHoursLst[0]));
                            }else{     
                                finalHoursToBeAdded = Integer.valueOf(finalHoursLst[0]);
                            }
                        }
                    }
                    system.debug('@@finalHoursToBeAdded'+finalHoursToBeAdded);
                    /*if(calndrInviteRecord.Start_Date__c.month() != Date.Today().Month()){
                        if(string.valueOf(finalHoursToBeAdded).contains('-')){
                            finalHoursToBeAdded = finalHoursToBeAdded+1;
                        }else{
                            finalHoursToBeAdded = finalHoursToBeAdded-1;
                        }
                    }*/
                    system.debug('@@finalHoursToBeAddedFinal'+finalHoursToBeAdded);
                    //finalHoursToBeAdded = finalHoursToBeAdded+2;
                    //system.debug('@@finalHoursToBeAdded484'+finalHoursToBeAdded);
            
                    list<CalendarInvite_recurrence> clRecurrenceLst= new List<CalendarInvite_recurrence>();
                    list<CalendarInvite_attendees> notifyAttendeesLst=new List<CalendarInvite_attendees>();
                    system.debug('@@calndrInviteRecord.Start_Date__c'+calndrInviteRecord.Start_Date__c+'@@calndrInviteRecord.End_Date__c'+calndrInviteRecord.End_Date__c);
                    
                    //DateTime startDt = datetime.newInstance(calndrInviteRecord.Start_Date__c.year(), calndrInviteRecord.Start_Date__c.month(),calndrInviteRecord.Start_Date__c.day()); 
                    String startDateString = (calndrInviteRecord.Start_Date__c).addHours(finalHoursToBeAdded).format(dateFormat);
                    System.debug('@@startDateString'+startDateString); // 2016-02-29T22:30:48Z
                    
                    //DateTime endDt = datetime.newInstance(calndrInviteRecord.End_Date__c.year(), calndrInviteRecord.End_Date__c.month(),calndrInviteRecord.End_Date__c.day());
                    String endDateString = calndrInviteRecord.End_Date__c.addHours(finalHoursToBeAdded).format(dateFormat);
                    System.debug('@@endDateString'+endDateString); // 2016-02-29T22:30:48Z
                    
                    system.debug('@@UserInfo.getTimeZone()'+UserInfo.getTimeZone());
                    
                    CalendarInvite_start clStart = new CalendarInvite_start(startDateString,string.valueof(UserInfo.getTimeZone()));
                    CalendarInvite_End clEnd = new CalendarInvite_End(endDateString,string.valueOf(UserInfo.getTimeZone()));
                    system.debug('@@clStart'+clStart+'@@clEnd'+clEnd);
                    
                    for(string strEmail:finalSetofEmails){
                       
                        if(strEmail != null && inviteeEmails.containsKey(strEmail))
                            notifyAttendeesLst.add(new CalendarInvite_attendees(strEmail,inviteeEmails.get(strEmail)));
                            
                    }
                    //clAttendeesLst.add(new CalendarInvite_attendees(defaultGoogleCalendarId));
                    system.debug('@@notifyAttendeesLst'+notifyAttendeesLst);
                    
                    clRecurrenceLst.add(new CalendarInvite_recurrence('RRULE:FREQ=DAILY;UNTIL=20300930T170000Z'));
                    
                    system.debug('@@calndrInviteRecord.Invite_Others__c'+calndrInviteRecord.Invite_Others__c+'@@calndrInviteRecord.Modify_Event__c'+calndrInviteRecord.Modify_Event__c+'@@calndrInviteRecord.See_Guest_List__c'+calndrInviteRecord.See_Guest_List__c);
                    system.debug('@@calndrInviteRecord.Description__c'+calndrInviteRecord.Description__c);
                    
                    CalendarNotifyInviteWrapper calInviteWrapObj = new CalendarNotifyInviteWrapper(calndrInviteRecord.Event__r.name+' - '+ calndrInviteRecord.Invite_Type__c,calndrInviteRecord.Venue__r.name+' , '+calndrInviteRecord.Venue_Address__c,clStart,clEnd, null,notifyAttendeesLst,calndrInviteRecord.Description__c
                                                                                        ,calndrInviteRecord.Invite_Others__c, calndrInviteRecord.Modify_Event__c, calndrInviteRecord.See_Guest_List__c);
                    
                    string bodyRequest = JSON.serialize(calInviteWrapObj);
                    bodyRequest = bodyRequest.replace('"enddate":', '"end":');
                    bodyRequest = bodyRequest.replace('"cld_dateTime":', '"dateTime":');
                    
                    System.debug('@@bodyRequest'+bodyRequest);
                  
                    req.setBody(bodyRequest);     
                    
                    req.setHeader('Authorization', 'Bearer ' + accessTokenVal);
                    req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
                    req.setHeader('Content-Type', 'application/json; charset=UTF-8');
                    
                    req.setMethod('PUT');
                    
                    req.setTimeout(10000);
                    HttpResponse res = h.send(req); 
                    System.debug(JSON.serialize(res.getBody()));
                    
                    Map<string,string> Eventinsertresponse= new Map<string,string>();
                    
                    Eventinsertresponse = parseJSONToMap(res.getBody());
                 
                    system.debug(Eventinsertresponse);
                }
            }
       
       
        system.debug('@@notifiedEmailsMap'+notifiedEmailsMap);
        system.debug('@@CalinviteToAttendeeStatusMap'+CalinviteToAttendeeStatusMap);

        // below condition is to run batch apex only when we create invite first time for the opportunity
        
            CreateCalendarInviteBatch be = new CreateCalendarInviteBatch(returnCalendarInviteLst(selectedEventIds),notifiedEmailsMap,setOfEmails,accessTokenVal,opptyObj,'CalendarInvite',null,null,CalinviteToAttendeeStatusMap);
            database.executeBatch(be,1);
        
        
        if(delCalendarInvites.size()>0){
         delete delCalendarInvites;
        }
      
        PageReference opportunityPage = new ApexPages.StandardController(opptyObj).view();
        opportunityPage.setRedirect(true);
        return opportunityPage;
    }
    
    
    public class cEvent{
        public Boolean selected{get;set;}
        public Calendar_Invite__c invite{get;set;}
        public String eventDate{get;set;}
        public string attendeeStatus {get;set;}
        public cEvent(Calendar_Invite__c ci, String d, string attndeeStatus){
            this.invite = ci;
            this.selected = true;
            this.eventDate = d;
            this.attendeeStatus = attndeeStatus;
        }
    }
 public class CalendarNotifyInviteWrapper{
    public String summary;  //Sales Call
    public String location; //Conference Room A
    public CalendarInvite_start start;
    public CalendarInvite_end enddate;
    public List<CalendarInvite_recurrence> recurrence;
    public List<CalendarInvite_attendees> attendees;
    public string description;
    
    public boolean guestsCanInviteOthers;
    public boolean guestsCanModify;
    public boolean guestsCanSeeOtherGuests;
    
        public CalendarNotifyInviteWrapper(string summaryVal,string locationVal,CalendarInvite_start cld_StartDate,CalendarInvite_end cld_endDate, List<CalendarInvite_recurrence> cld_recurList,List<CalendarInvite_attendees> cld_recurAttList,string calendarDescription,
                                        boolean guestsCanInvite, boolean guestsCanMod, boolean guestsCanSee){
            this.summary = summaryVal;
            this.location = locationVal;
            this.start = cld_StartDate;
            this.endDate = cld_endDate;
            this.recurrence = cld_recurList;
            this.attendees = cld_recurAttList;
            this.description = calendarDescription;
            this.guestsCanInviteOthers = guestsCanInvite;
            this.guestsCanModify = guestsCanMod;
            this.guestsCanSeeOtherGuests = guestsCanSee;
        }
        
    
    }
 
    public static map<string, string> parseJSONToMap(string JSONValue){
        JSONParser parser = JSON.createParser(JSONValue);
        map<string, string> jsonMap = new map<string, string>();
        string keyValue = '';
        string tempValue = '';
        while (parser.nextToken() != null) {
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME){
                keyValue = parser.getText();
                parser.nextToken();
                tempValue = parser.getText();
                jsonMap.put(keyValue, tempValue);             
            }
        }
        return jsonMap;
    }
    public class CalendarInvite_recurrence {
            public String recurrence;
            public CalendarInvite_recurrence(string recurVal){
                this.recurrence = recurVal;
            }
        }
    public class CalendarInvite_start {
            public String cld_dateTime; //2018-09-29T08:00:00.000-07:00
            public String timeZone; //America/Los_Angeles
            public CalendarInvite_start(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
    public class CalendarInvite_end {
            private String cld_dateTime{get;set;} //2018-09-30T08:30:00.000-07:00
            private String timeZone; //America/Los_Angeles
            private CalendarInvite_end(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
 
  
      public class CalendarInvite_attendees {
            public String email;    //maddy.crmdev@gmail.com
            public String responseStatus;
            public CalendarInvite_attendees(string organizerEmail,String status){
                this.email = organizerEmail;
                this.responseStatus= status;
            }
        }

}