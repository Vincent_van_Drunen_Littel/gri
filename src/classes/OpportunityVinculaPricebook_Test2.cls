@isTest
private class OpportunityVinculaPricebook_Test2 
{ 
    // Contacts
    private static Integer nContacts = 3;
    private static List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(nContacts);
    // RE Club Brazil
    private static Club__c clubReBrazil= TestDataFactory.createClubs(TestDataFactory.CLUB_RE_BRAZIL);
     private static Event__c brazilReClubEvent = TestDataFactory.createEvent('Brazil RE 2018',  clubReBrazil);
    // Smartus
    private static Club__c smartusCorp = TestDataFactory.createClubs(TestDataFactory.SMARTUS_REAL_ESTATE);
    private static Event__c smartusEvent = TestDataFactory.createEvent('Smartus Cool Event',  smartusCorp);

    @isTest 
    static void test_club()
    {
        Pricebook2 thePriceBookEvent = TestDataFactory.createPricebookEvent(
            System.today()-100, System.today()+99999, clubReBrazil.Id, brazilReClubEvent.Id);        
        Pricebook2 thePriceBookSpex = TestDataFactory.createPricebookSpex(
            System.today()-100, System.today()+99999, clubReBrazil.Id);
        Pricebook2 thePriceBookSpexEvent = TestDataFactory.createPricebookSpexSingleEvent(
            System.today()-100, System.today()+99999, clubReBrazil.Id, brazilReClubEvent.Id);
        Pricebook2 thePriceBookMembership = TestDataFactory.createPricebookMembership(
            System.today()-100, System.today()+99999, clubReBrazil.Id);
        Pricebook2 thePriceMagazine = TestDataFactory.createPricebookMagazine(
            System.today()-100, System.today()+99999, 
            TestDataFactory.createMagazine('RevistaRecreio').Id);

        Test.startTest();
        TestDataFactory.createMembershipOpps(listContacts, clubReBrazil, System.today()+10);
        TestDataFactory.createEventOpps(listContacts, brazilReClubEvent, clubReBrazil, System.today()+10);
        //TestDataFactory.createSpexOpps(listContacts, NULL, clubReBrazil, System.today()+10);
        TestDataFactory.createSpexOpps(listContacts, brazilReClubEvent, NULL, System.today()+10);
        TestDataFactory.createMagazineOpps(listContacts, clubReBrazil, System.today()+10);
        Test.stopTest();
    }
    
    @isTest 
    static void test_samartus() 
    {
        Pricebook2 thePriceBookEvent = TestDataFactory.createPricebookEvent(
            System.today()-100, System.today()+99999, smartusCorp.Id, smartusEvent.Id);        
        Pricebook2 thePriceBookSpex = TestDataFactory.createPricebookSpex(
            System.today()-100, System.today()+99999, smartusCorp.Id);

        Test.startTest();
        TestDataFactory.createSmartusEventOpps(listContacts, smartusEvent, System.today()+10);
        TestDataFactory.createSmartusSpexOpps(listContacts, smartusEvent, smartusCorp, System.today()+10);
        Test.stopTest();
    }

    @isTest
    static void test_error()
    {
        List<Account> listAcc = TestDataFactory.createAccounts(1);
        List<Contact> listContacts = TestDataFactory.createContacts(listAcc, 15);
        Pricebook2 thePriceBookEvent = TestDataFactory.createPricebookEvent(
            System.today()-100, System.today()+99999, smartusCorp.Id, smartusEvent.Id);
        thePriceBookEvent.RecordTypeId=NULL;
        update  thePriceBookEvent;
        List<Opportunity> listErr = new List<Opportunity>();
        listErr.add(new Opportunity(
            RecordTypeId = TestDataFactory.getRecType('Opportunity', 'Membership'),
            Name = 'Opp Teste1 -- ' + listContacts[0].FirstName,
            StageName = 'Prospect',
            AccountId = listContacts[0].AccountId,
            Contact__c = listContacts[0].Id,
            CloseDate = System.today(),
            OwnerId = UserInfo.getUserId(),
            Type = 'Existing Business',
            Event__c = brazilReClubEvent.Id,
            Pricebook2Id=NULL
        ));
        listErr.add(new Opportunity(
            RecordTypeId = TestDataFactory.getRecType('Opportunity', 'Events'),
            Name = 'Opp Teste2 -- ' + listContacts[1].FirstName,
            StageName = 'Prospect',
            AccountId = listContacts[1].AccountId,
            Contact__c = listContacts[1].Id,
            CloseDate = System.today(),
            OwnerId = UserInfo.getUserId(),
            Type = 'Existing Business',
            Event__c = brazilReClubEvent.Id,
            Pricebook2Id=NULL
        )); 
        listErr.add(new Opportunity(
            RecordTypeId = TestDataFactory.getRecType('Opportunity', 'Magazine Subscription'),
            Name = 'Opp Teste3 -- ' + listContacts[2].FirstName,
            StageName = 'Prospect',
            AccountId = listContacts[2].AccountId,
            Contact__c = listContacts[2].Id,
            CloseDate = System.today(),
            OwnerId = UserInfo.getUserId(),
            Type = 'Existing Business',
            Event__c = brazilReClubEvent.Id,
            Pricebook2Id=NULL
        )); 
        listErr.add(new Opportunity(
            RecordTypeId = TestDataFactory.getRecType('Opportunity', 'Smartus - Spex'),
            Name = 'Opp Teste4 -- ' + listContacts[3].FirstName,
            StageName = 'Prospect',
            AccountId = listContacts[3].AccountId,
            Contact__c = listContacts[3].Id,
            CloseDate = System.today(),
            OwnerId = UserInfo.getUserId(),
            Type = 'Existing Business',
            Event__c = brazilReClubEvent.Id,
            Pricebook2Id=NULL
        )); 
        listErr.add(new Opportunity(
            RecordTypeId = TestDataFactory.getRecType('Opportunity', 'Smartus - Events'),
            Name = 'Opp Teste5 -- ' + listContacts[4].FirstName,
            StageName = 'Prospect',
            AccountId = listContacts[4].AccountId,
            Contact__c = listContacts[4].Id,
            CloseDate = System.today(),
            OwnerId = UserInfo.getUserId(),
            Type = 'Existing Business',
            Event__c = brazilReClubEvent.Id,
            Pricebook2Id=NULL
        )); 
        listErr.add(new Opportunity(
            RecordTypeId = TestDataFactory.getRecType('Opportunity', 'SPEX'),
            Name = 'Opp Teste6 -- ' + listContacts[5].FirstName,
            StageName = 'Prospect',
            AccountId = listContacts[5].AccountId,
            Contact__c = listContacts[5].Id,
            CloseDate = System.today(),
            OwnerId = UserInfo.getUserId(),
            Type = 'Existing Business',
            Event__c = brazilReClubEvent.Id,
            Pricebook2Id=NULL
        ));
        insert  listErr;
    }

    //@isTest
    //private static test_functions()
    //{
    //    Test
    //}

    @testSetup 
    static void buildData()
    {
        TestDataFactory.init();
    }
}