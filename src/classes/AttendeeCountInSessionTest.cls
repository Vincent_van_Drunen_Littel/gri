/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe teste da classe AttendeeCountInSession
*
*
* NAME: AttendeeCountInSessionTest.cls
* AUTHOR: VMDL                                                 DATE: 15/08/2016
*******************************************************************************/
@isTest(SeeAllData=true)
private class AttendeeCountInSessionTest {

  private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
  private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );

  static testMethod void testeFuncionalInsertAttendee()
  {
    Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    Session__c lSession = SObjectInstance.createSession(evento.Id);
    Database.insert(lSession);

    Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(conta);

    Contact contato = SObjectInstance.createContato(conta.Id);
    Database.insert(contato);

    Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
    oportunidade.Contact__c = contato.Id;
    oportunidade.StageName = 'Approved';
    oportunidade.Event__c = evento.Id;
    Database.insert(oportunidade);

    Attendee__c lAtt = SObjectInstance.createAttendee(evento.Id, lSession.Id, oportunidade.Id);

    Test.startTest();
    Database.insert(lAtt);
    Test.stopTest();
  }

  static testMethod void testeFuncionalDeleteAttendee()
  {
    Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    Session__c lSession = SObjectInstance.createSession(evento.Id);
    Database.insert(lSession);

    Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(conta);

    Contact contato = SObjectInstance.createContato(conta.Id);
    Database.insert(contato);

    Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
    oportunidade.Contact__c = contato.Id;
    oportunidade.StageName = 'Approved';
    oportunidade.Event__c = evento.Id;
    Database.insert(oportunidade);

    Attendee__c lAtt = SObjectInstance.createAttendee(evento.Id, lSession.Id, oportunidade.Id);
    Database.insert(lAtt);

    Test.startTest();
    Database.delete(lAtt);
    Test.stopTest();
  }
}