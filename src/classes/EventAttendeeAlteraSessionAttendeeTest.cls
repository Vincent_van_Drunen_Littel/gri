/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe de teste da classe EventAttendeeAlteraSessionAttendee
*
* NAME: EventAttendeeAlteraSessionAttendeeTest.cls
* AUTHOR: EAD                                                  DATE: 22/01/2018
*******************************************************************************/

@isTest
public class EventAttendeeAlteraSessionAttendeeTest {
	public static string RT_OPP = OrgDefs.RT_OPP_UNIQUE;
    public static integer LOTE = 10;

    public static testMethod void test(){
        //=== Custom setting ===
        Application_Config__c appConfig = new Application_Config__c(Name = 'BucketUserId', Value__c = '00536000004lQpq');
    	insert appConfig;     
        
        //=== Account ===
        Account accTest = SObjectInstance.createAccount2();
        insert accTest;
        
        //=== Contact ===
        Contact cttTest = SObjectInstance.createContato(accTest.id);
        insert cttTest;
        
        //=== Opportunity ===
        Opportunity oppTest = SObjectInstance.createOpportunidade(accTest.id, RT_OPP);
        oppTest.Contact__c = cttTest.id;
        insert oppTest;
        oppTest.StageName='Lost';
        update oppTest;
        
        //=== Event ===
        Event__c evtTest = SObjectInstance.createEvent();
        insert evtTest;
        
        //=== Event_Attendee__c ===        
        Event_Attendee__c evAttTest = SObjectInstance.createEventAttendee(oppTest.id, accTest.id, cttTest.id, evtTest.id);
        insert evAttTest;
        
        //=== Sesseion__c ===
        Session__c sesTest = SObjectInstance.createSession(evtTest.id);
        insert sesTest;
        
        //=== Attendee__c (Session attendee) ===
        Attendee__c sesAttTest = SObjectInstance.createAttendee(evtTest.id, sesTest.id, oppTest.id);
        insert sesAttTest;
        
        //##### Tests #####
        Test.startTest();
        evAttTest.Event_Attendees_Status__c = 'Cancelled';
        System.debug('evAttTest='+evAttTest);
        update evAttTest;
        List<Event_Attendee__c> listEvAtt = [SELECT Id, Name, Event_Attendees_Status__c FROM Event_Attendee__c WHERE Id=:evAttTest.Id ];
        System.debug('listEvAtt='+listEvAtt);

        List<Attendee__c> lstSesAtt = [SELECT Session_Status__c FROM Attendee__c];
        System.assertEquals('Declined', lstSesAtt.get(0).Session_Status__c);
        Test.stopTest();
    }
    
    //----------------------------------------------------------
    
    public static testMethod void testLote(){
        //=== Custom setting ===
        Application_Config__c appConfig = new Application_Config__c(Name = 'BucketUserId', Value__c = '00536000004lQpq');
    	insert appConfig;     
        
        //=== Account ===
        Account accTest = SObjectInstance.createAccount2();
        insert accTest;
        
        //=== Contact ===
        Contact cttTest = SObjectInstance.createContato(accTest.id);
        insert cttTest;
        
        //=== Opportunity ===
        list<Opportunity> lstOppTest = new list<Opportunity>();
        for(integer i = 0; i < LOTE; i++){
       		Opportunity oppTest = SObjectInstance.createOpportunidade(accTest.id, RT_OPP);
        	oppTest.Contact__c = cttTest.id;
            lstOppTest.add(oppTest);
        }
        insert lstOppTest;
        for(Opportunity opp:lstOppTest)
        {
            opp.StageName='Lost';
        }
        update lstOppTest;
        
        //=== Event__c==
        list<Event__c> lstEvt = new list<Event__c>();
        for(integer i = 0; i < LOTE; i++){
        	lstEvt.add(SObjectInstance.createEvent());
        }
        insert lstEvt;
        
        //=== Event_Attendee__c ===        
        list<Event_Attendee__c> lstEA = new list<Event_Attendee__c>();
        for(integer i = 0; i < LOTE; i++){
            lstEA.add(SObjectInstance.createEventAttendee(lstOppTest.get(i).id, accTest.id, cttTest.id, lstEvt.get(i).id));
        }
        insert lstEA;
        
        //=== Session__c ===
        list<Session__c> lstSesTest = new list<Session__c>();
        for(integer i = 0; i < LOTE; i++){
        	lstSesTest.add(SObjectInstance.createSession(lstEvt.get(i).id));
        }
        insert lstSesTest;
        
        //=== Attendee__c (Session attendee) === 
        list<Attendee__c> lstAtt = new list<Attendee__c>();
        for(integer i = 0; i < LOTE; i++){
        	lstAtt.add(SObjectInstance.createAttendee(lstEvt.get(i).id, lstSesTest.get(i).id, lstOppTest.get(i).id));
        }
        insert lstAtt;
        
        //##### Tests #####
        Test.startTest();
          for(integer i = 0; i < LOTE/2; i++){
              lstEA.get(i).Event_Attendees_Status__c = 'Cancelled';
          }
          update lstEA;
        
          list<Attendee__c> lstSesAtt = [SELECT Session_Status__c FROM Attendee__c]; 
          system.assertEquals('Declined', lstSesAtt.get(0).Session_Status__c);
          system.assertEquals('Maybe', lstSesAtt.get(LOTE-1).Session_Status__c);
        Test.stopTest();
    }
}