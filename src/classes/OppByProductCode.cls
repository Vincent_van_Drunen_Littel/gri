/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * A class that filters opportunities by product code.
 * It performs One SOQL query by the contructor, and requires a list of Opportunities and
 * product codes as input. 
 * 
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 * 19/06/18    AndPaschoalon  chenged .SObject_idSet to .keySet()
 */ 
public class OppByProductCode {

    private static final String FILTER_WITH = 'with';
    private static final String FILTER_JUST = 'just';
    private List<Opportunity> oppsWithProdCode = new List<Opportunity>();
    
 	/**
 	 * Contructor for OppByProductCode. It makes a query of bunch of Opportunities to be filtered
 	 * by product codes
	 */
    public OppByProductCode(List<Opportunity> lopps)
    {
        //Set<Id> oppIds = SObjectHelper.SObject_idSet(lopps);
        Set<Id> oppIds = (new Map<Id,SObject>(lopps)).keySet();
        System.debug('%%DEBUG lopps='+lopps);
        
		if(oppIds.size() == 0)
        {
            System.debug('No valid Opportunity was passed to the the class OppByProductCode');
            return;
        }
        else{
            this.oppsWithProdCode= [SELECT Id,Name,(
                SELECT Id,ProductCode 
                FROM OpportunityLineItems), Contact__c, Event__r.Club__c, Event__r.End_Date__c, Event__r.Start_Date__c 
                                    FROM Opportunity 
                                    WHERE Id IN :oppIds];
            AutoProcessMembershipOpps.debug_number_of_queries++;
            System.debug('%% Number of Queries::'+AutoProcessMembershipOpps.debug_number_of_queries);
        }
        
        System.debug('%%DEBUG this.oppsWithProdCode[0].OpportunityLineItems='+this.oppsWithProdCode[0].OpportunityLineItems);
        
    }

	/**
	 * Filter Opportunityes by product code based on the list of Opportunities passed by the
	 * constructor. The option reffers to the type of filter applied. The condition "just"
	 * will filter just the opportunities with the product code. The option "with" will 
	 * filter the opportunities with this and others product codes.
	 * 
	 */    
    public List<Opportunity> filter(String strProductCode, String strCondition)
    {
        List<Opportunity> filteredOpps = new List<Opportunity>();
        for(Opportunity opp:this.oppsWithProdCode)
        {
            // debug
            //testProd(opp.Id,  'Well, this is working, look at this opp='+opp);
            
            Boolean belongsTo = False;
            OpportunityLineItem[] vOppLineItem = opp.OpportunityLineItems;
            
            for(OpportunityLineItem oli:vOppLineItem)
            {
                if(strCondition == FILTER_WITH)
                {
                    if(strProductCode == oli.ProductCode){
                        belongsTo = True;
                        break;
                    }
                }
                else //default: FILTER_JUST
                {
                    if(strProductCode != oli.ProductCode)
                    {
                        belongsTo = False;
                    	break;
                    }
                    else{
                        belongsTo = True;
                    }
                }
                    
            }//endfor
        	if(belongsTo) filteredOpps.add(opp);
        }//endfor
        
        System.debug('%%DEBUG this.oppsWithProdCode[0].OpportunityLineItems='+this.oppsWithProdCode[0].OpportunityLineItems);
        
        return(filteredOpps);
    }
    
    /**
     * TODO
     * method that takes a list of produc codes and check if the opportunities have 
     * these product codes.
     * just: just the opportunities with product codes equal to the ones passed are 
     * selected. If an Opp has more products, they are not selected.
     * with: opportunities with any product code listed, and with others, are
     * filtered as well
	 */
    //public List<Opportunity> filter(List<String> strProductCode, String strCondition)
    //{
    //    List<Opportunity> filteredOpps = new List<Opportunity>();
    //    return(filteredOpps);
    //}
       
}