/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure 
 * players 
 ******************************************************************************
 * Remote controller for Javascript calls of the live-chat agent
 *
 * @author  Anderson Paschoalon
 * @since
 * Date      Author     Description
 * 23/05/18  AndersonP  First version. Check if contact or Lead exist. If not, 
 *                      create a new lead.
 */

/**
 * @author AndersonP
 * @date 2018-08-27
 * @group
 * @group-content
 * @description  Remote controller for Javascript calls of the live-chat agent
 * @since   2018-08-27
 */ 
public class SalesPreChatController 
{
    // Properties 

    private static final String LOGTAG = '%% SalesPreChatController: ';
    private static final String chatLeadSource = 'Website, Live Chat';
   	private static final String chatDefaultCompany = 'Default_Company';
    private static final String chatDefaultFirstName = 'John';
    private static final String chatDefaultLastName = 'Doe';
    private static final String FORM_EMAIL = 'Email';
    private static final String FORM_PHONE = 'Phone';
    private static final String CAMPAIGN_SMARTUS_OFFLINE_PRECHAT_NAME = 'Smartus-LiveAgentChat-Offline';
    private static final String CAMPAIGN_SMARTUS_ONLINE_PRECHAT_NAME = 'Smartus-LiveAgentChat-Online';
    private static final String SOBJECT_CONTACT_PHONE = 'Business Phone';
    private static final String SOBJECT_CONTACT_EMAIL = 'E-mail';
    private static final String TASK_LIVEAGENT_OFFLINE_PREFIX = '[Live Agent] Offline: ';
    private static final String TASK_STATUS_OPEN = 'Open';
    private static final String TASK_PRIORITY_HIGH = 'High';

    // Live Agent offline agent prechat Task and add the Contact/Lead/New-Lead to 
    // the Offline chat Campaign
    /**
     * @description Offiline pre-chat procedure: (1) Check if the ingressing person exists
     * on database by Email. If it is a contact does nothing. If it is a exting lead, 
     * updates any blank field on existing Lead. If the Email does not exist on the 
     * Database, creates a new Lead. (2) Updates the prefered contact method (Email or 
     * Phone) on the Lead or Contact. (3) Create a new task for the on the Database.
     * (4) Add the Contact/Lead/new-Lead to the Offline campaign, if it is not already
     * there.  
     */
    @RemoteAction
    public static String offlinePrechatProcedure(
        String theEmail, String thePhone, String communicationMedium, String theFirstName, 
        String theLastName, String theCompanyName, String comment)
    {
        SObject person = createIfDoesntExist(theEmail, theFirstName, theLastName, theCompanyName);
        if(communicationMedium==FORM_PHONE)
            person.put('Preferred_Contact_Method__c', SOBJECT_CONTACT_PHONE);
        else
            person.put('Preferred_Contact_Method__c', SOBJECT_CONTACT_EMAIL);
        if(thePhone!='')
            person.put('Phone', thePhone);
        update person;
        
        System.debug(LOGTAG+'person='+person);
        // create task
        Id recTypeId = HelperSObject.recordType_get('Task', 'Task');
        String taskSubject = TASK_LIVEAGENT_OFFLINE_PREFIX + theFirstName + ' ' + theLastName;
        Task liveAgentTask = new Task(Priority=TASK_PRIORITY_HIGH, Status=TASK_STATUS_OPEN, 
            ActivityDate=System.today(), ReminderDateTime=System.now(), IsReminderSet=TRUE, 
            RecordTypeId= HelperSObject.recordType_get('Task', 'Task'),
            Subject= TASK_LIVEAGENT_OFFLINE_PREFIX + theFirstName + ' ' + theLastName, 
            WhoId=person.Id, OwnerId=getTaskOwnerId(), Description=comment);
        insert liveAgentTask;

        // Add person to Campaign
        HelperSObject.campaign_addLeadOrContact(createOrRetrieveOfflineChatCampaignId(), person, 
            'Filled the offline Chat at '+System.now()+' GMT');
        return 'TRUE';
   }

    /**
     * @description Online pre-chat procedure: (1) Check if the ingressing person exists
     * on database by Email. If it is a contact does nothing. If it is a exting lead, 
     * updates any blank field on existing Lead. If the Email does not exist on the 
     * Database, creates a new Lead. (2) Add the Contact/Lead/new-Lead to the Offline
     * campaign, if it is not already
     * there.  
     */
    @RemoteAction
    public static String onlinePrechatProcedure(String theEmail, String theFirstName, 
        String theLastName, String theCompanyName)
    {
        SObject person = createIfDoesntExist(theEmail, theFirstName, theLastName, theCompanyName);
        HelperSObject.campaign_addLeadOrContact(createOrRetrieveOnlineChatCampaignId(), person, 
            'Filled the online Chat at '+System.now()+' GMT');    
        return 'TRUE';
    }

    /**
     * @description Create Lead if doent exist
     */
    private static SObject createIfDoesntExist(String theEmail, String theFirstName, String theLastName, String theCompanyName)
    {
        // setup
        theFirstName=(theFirstName=='')?chatDefaultFirstName:theFirstName;
        theLastName=(theLastName=='')?chatDefaultLastName:theLastName;
        theCompanyName=(theCompanyName=='')?chatDefaultCompany:theCompanyName;
        System.debug(LOGTAG+'{'+'theEmail:'+theEmail+', theFirstName:'+theFirstName+', theLastName:'+theLastName+', theCompanyName:'+theCompanyName+'}');

        if(theEmail!='')
        {
            // check if contact or lead exist
            List<Contact> listContacts = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE Email=:theEmail LIMIT 1];
            List<Lead> listLeads = [SELECT Id, FirstName, LastName, Company, Email FROM Lead WHERE Email=:theEmail LIMIT 1];
            System.debug(LOGTAG+'listContacts='+HelperSObject.json(listContacts, 'Id, FirstName, LastName, Email'));
            System.debug(LOGTAG+'listLeads='+HelperSObject.json(listLeads, 'Id, FirstName, LastName, Email'));
            System.debug(LOGTAG+'listContacts.isEmpty()='+listContacts.isEmpty());
            System.debug(LOGTAG+'listLeads.isEmpty()='+listLeads.isEmpty());
            if(!listContacts.isEmpty())
                return listContacts[0];
            if(!listLeads.isEmpty())    
                return updateOldLead(listLeads[0], theFirstName, theLastName, theCompanyName);            
        }

        // create new lead
        Lead newChatLead = new Lead(FirstName=theFirstName, LastName=theLastName, 
                                    Company_Name_Badge__c=theCompanyName, Email=theEmail, 
                                    Company=theCompanyName, LeadSource=chatLeadSource);
        insert newChatLead;
        System.debug(LOGTAG+'newChatLead='+HelperSObject.json(newChatLead, 'Id, FirstName, LastName, Email'));
        return newChatLead;       
    }

    // Update Lead fields if they are blank
    private static Lead updateOldLead(Lead oldLead, String theFirstName, String theLastName, String theCompanyName)
    {
        System.debug(LOGTAG+'oldLead.FirstName='+oldLead.FirstName);
        if(oldLead.FirstName==NULL || oldLead.FirstName=='John')
            oldLead.FirstName=theFirstName;
        if(oldLead.LastName=='Doe')
            oldLead.LastName=theLastName;
        if(oldLead.Company=='Default_Company')
            oldLead.Company=theCompanyName;
        update oldLead;
        return oldLead;
    }

    // Method for retrieving Task Owner. To be defined.
    private static Id getTaskOwnerId()
    {
        //return '00536000006EEZ9AAO';
        return '00536000001jFwtAAE';
    }

    // Retrieve the ID of the Offline Campaign, 
    private static Id createOrRetrieveOfflineChatCampaignId()
    {
        List<Campaign> listCampaign = 
            //[SELECT Id, Name FROM Campaign WHERE Name=:CAMPAIGN_OFFLINE_PRECHAT_NAME LIMIT 1];
            [SELECT Id, Name FROM Campaign WHERE Name=:CAMPAIGN_SMARTUS_OFFLINE_PRECHAT_NAME LIMIT 1];
        if(listCampaign.isEmpty())
        {
            listCampaign.add(new Campaign(Opportunity_Type__c='Smartus - Events', 
                Type='Email + Call', CurrencyIsoCode='BRL', IsActive=True, 
                Status='In Progress', Name=CAMPAIGN_SMARTUS_OFFLINE_PRECHAT_NAME));
            insert listCampaign;
        }
        return  listCampaign[0].Id;
    }

    // Retrieve the ID of the Offline Campaign, 
    private static Id createOrRetrieveOnlineChatCampaignId()
    {
        List<Campaign> listCampaign = 
            [SELECT Id, Name FROM Campaign WHERE Name=:CAMPAIGN_SMARTUS_ONLINE_PRECHAT_NAME LIMIT 1];
        if(listCampaign.isEmpty())
        {
            listCampaign.add(new Campaign(Opportunity_Type__c='Smartus - Events', 
                Type='Email + Call', CurrencyIsoCode='BRL', IsActive=True, 
                Status='In Progress', Name=CAMPAIGN_SMARTUS_ONLINE_PRECHAT_NAME));
            insert listCampaign;
        }
        return  listCampaign[0].Id;
    }

}