/**
 * Created by Eric Bueno on 23/02/2019.
 */

public with sharing class CampaignMemberTriggerHandler extends TriggerHandler{

    private static CampaignMemberBO bo = CampaignMemberBO.getInstance();

    public override void afterInsert(){
        // Atribui oportunidade as campanhas de Digital Funnel
        bo.assignCampaignMemberToOpportunity( Trigger.new );
    }

}