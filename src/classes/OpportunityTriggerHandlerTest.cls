/**
 * Created by Eric Bueno on 24/02/2019.
 */

@IsTest
public with sharing class OpportunityTriggerHandlerTest {

    @TestSetup
    static void setup(){

        TestDataFactory.init();

        List<Account> listAcc = TestDataFactory.createAccounts(1);
        List<Contact> newlistContacts = TestDataFactory.createContacts(listAcc[0], 2);
        newlistContacts[0].email = 'joao@gmail.com';
        newlistContacts[1].email = 'maria@gmail.com';
        update newlistContacts;
        Club__c newClub = TestDataFactory.createClubs('New Club');
        Event__c newEvent = TestDataFactory.createEvent('Test Event', newClub);
        newEvent.Vip_Dinner_Included__c = true;
        update newEvent;
        Opportunity newOpp = TestDataFactory.createEventOpps(newlistContacts[0], newEvent, newClub, System.today()+10);
        Opportunity newOpp2 = TestDataFactory.createEventOpps(newlistContacts[1], newEvent, newClub, System.today()+10);
        Product2 newProduct = TestDataFactory.createProduct('new product');
        Set<String> listStr = new Set<String>{newProduct.Id};

        List<Opportunity> newListOpp = [SELECT Id, AccountId, Contact__c FROM Opportunity LIMIT 2];   
        Set<Id> setOpp = new Set<Id>();
        for(Opportunity opp : newListOpp){
                setOpp.add(opp.Id);
        }

        Invite_Type_Product_Code__mdt newITPC = new Invite_Type_Product_Code__mdt();
        newITPC.DeveloperName = 'nome';
        newITPC.ProductId__c = newProduct.Id;

        Calendar_Invite__c newCalendarInvitee = new Calendar_Invite__c();
        newCalendarInvitee.IsInviteesLimitReached__c = true;
        newCalendarInvitee.GoogleCalendarId__c = 'aaaaaaaaa';
        newCalendarInvitee.GoogleCalendarEventId__c = 'bbbbbbb';
        newCalendarInvitee.Invite_Type__c = 'Day 1';
        newCalendarInvitee.Event__c = newEvent.Id;
        newCalendarInvitee.Start_Date__c = System.today();
        newCalendarInvitee.End_Date__c = System.today()+10;
        newCalendarInvitee.Modify_Event__c = true;
        newCalendarInvitee.See_Guest_List__c = true;
        newCalendarInvitee.IsAutomated__c = true;
        newCalendarInvitee.Description__c = 'cccccccccccccccccccccccccccc';
        Insert newCalendarInvitee;

        Calendar_Invitee__c newCalendarInvite = new Calendar_Invitee__c();
        newCalendarInvite.InviteeEmail__c = 'joao@gmail.com'; 
        newCalendarInvite.Invite_Status__c = 'Yes'; 
        newCalendarInvite.Opportunity__c = newOpp2.Id;
        newCalendarInvite.Calendar_Invite__c = newCalendarInvitee.Id; 
        Insert newCalendarInvite; 

        Session__c newSession = TestDataFactory.createSessionWithEvent(newEvent);
        newSession.Type__c = 'Advisory Board Meeting';
        newSession.Status__c = 'In production';
        List<Attendee__c> newListAttendee = TestDataFactory.createSessionAttendeesWithSession(newListOpp, newEvent, newSession);
        newListAttendee[0].Session_Status__c = 'Confirmed';

        Test.startTest();
        List<Account> lAccounts = TestDataFactory.createAccounts(5);
        List<Contact> listContacts = TestDataFactory.createContacts(lAccounts, 1);
        Club__c club = TestDataFactory.createClubs(TestDataFactory.CLUB_RE_LATAM);
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent', club);
        theGreatEvent.Vip_Dinner_Included__c = true;
        theGreatEvent.Publish_on_Website__c = true;
        update theGreatEvent;
        
        OpportunityTriggerHandler.returnOpportunities(listStr, setOpp);
        OpportunityTriggerHandler.CreateInviteesForEvent(setOpp);

        // Cria as campanhas com oportunidades
        List<Campaign> listCampaigns = TestDataFactory.createCampaignsDigitalFunnelWithOpps(1, listContacts, club, theGreatEvent, new List<String>{'Intent'});
        // Criar o membro da campanha
        List<CampaignMember> listCampaignMembersContact = TestDataFactory.createCampaignMembersWithContacts(listCampaigns[0], listContacts, null);


        Test.stopTest();

    }

    @IsTest
    static void testUpdateDigitalFunnelStage(){

        Test.startTest();
        Test.stopTest();

        List<Campaign> listCampaigns = [
                SELECT
                        Id,
                        Digital_Funnel_Stage__c,
                        Digital_Campaign_Type__c,
                        DigitalCampaignEvent__c,
                        Type,
                (
                        SELECT
                                Id,
                                CampaignId,
                                Campaign.OwnerId,
                                Campaign.Event__c,
                                Campaign.Digital_Funnel_Stage__c,
                                Type,
                                Digital_Funnel_Stage__c
                        FROM
                                Opportunities
                        WHERE
                                StageName NOT IN ('Won','Lost','Cancelled')

                        ORDER BY Id
                )

                FROM
                        Campaign
                WHERE
                Digital_Funnel_Stage__c != null
                AND
                DigitalCampaignEvent__c != null
                AND
                Digital_Campaign_Type__c != null
                AND
                Type != null
                AND
                Id IN (
                        SELECT
                                CampaignId
                        FROM
                                Opportunity
                        WHERE
                                StageName NOT IN ('Won','Lost','Cancelled')
                )
        ];


        for(Campaign campaign : listCampaigns){
            // Verifica se o estagio do digital funnel eh o esperado na campanha
            System.assertEquals('Intent', campaign.Digital_Funnel_Stage__c);

            // Verifica se o estagio do digital funnel eh o esperado na oportunidade
            for(Opportunity opportunity : campaign.Opportunities){
                System.assert(opportunity.Digital_Funnel_Stage__c == 'Intent' || opportunity.Digital_Funnel_Stage__c == 'Purchase');
            }

        }
    }

}