/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 *  Class responsible for receiving the request from an external site.
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 15/01/19  EricB  First version.
 *
 */

@RestResource(urlMapping='/event/*')
global with sharing class EventREST extends ServiceRestBase{

    private static final EventREST instance = new EventREST();
    private static EventFacade facade = EventFacade.getInstance();

    global EventREST(){
        super.setServiceRestBase('EventService');
    }

    @HttpGet
    global static void processGet(){
        instance.processService('GET');
    }

    global override void processService(String method) {
        try{
            if(method == 'GET'){

                RestRequest req = RestContext.request;
                String endpoint = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

                if(endpoint == 'find-opp-by-event-id'){

                    super.setServiceRestBaseName('FindOppByEventId');

                    // get params
                    Map<String,String> mapRequestParams = req.params;
                    String eventId = mapRequestParams.get('event-id');

                    SiteTO.EventTO eventTO = facade.findOppByEventId(Id.valueOf(eventId));
                    this.returnSuccess(JSON.serialize(eventTO, true),200);

                }else if(endpoint == 'find-by-id'){

                    super.setServiceRestBaseName('FindById');

                    // get params
                    Map<String,String> mapRequestParams = req.params;
                    String eventId = mapRequestParams.get('event-id');

                    SiteTO.EventTO eventTO = facade.findEventById(Id.valueOf(eventId));
                    this.returnSuccess(JSON.serialize(eventTO, true),200);

                }


            }
        }catch (CustomException e){
            if(e.statusCode != null){
                this.returnError(e.getMessage(),e.statusCode);
            }else{
                this.returnError(e.getMessage());
            }
        }

    }
}