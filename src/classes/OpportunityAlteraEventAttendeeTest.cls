/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe de teste da classe OpportunityAlteraEventAttendee
*
* NAME: OpportunityAlteraEventAttendeeTest.cls
* AUTHOR: EAD                                                  DATE: 22/01/2018
*******************************************************************************/

@isTest
public class OpportunityAlteraEventAttendeeTest {
	public static string RT_OPP = OrgDefs.RT_OPP_UNIQUE;
    public static integer LOTE = 100;
    
    public static testMethod void test(){
        //=== Custom setting ===
        Application_Config__c appConfig = new Application_Config__c(Name = 'BucketUserId', Value__c = '00536000004lQpq');
    	insert appConfig;     
        
        //=== Account ===
        Account accTest = SObjectInstance.createAccount2();
        insert accTest;
        
        //=== Contact ===
        Contact cttTest = SObjectInstance.createContato(accTest.id);
        insert cttTest;
        
        //=== Opportunity ===
        Opportunity oppTest = SObjectInstance.createOpportunidade(accTest.id, RT_OPP);
        oppTest.Contact__c = cttTest.id;
        insert oppTest;
        
        //=== Event ===
        Event__c evtTest = SObjectInstance.createEvent();
        insert evtTest;
        
        //=== Event_Attendee__c ===        
        Event_Attendee__c evAttTest = SObjectInstance.createEventAttendee(oppTest.id, accTest.id, cttTest.id, evtTest.id);
        insert evAttTest;
        
        //=== Installments__c ===
        Installments__c instTest = new Installments__c(Name = '100200301', 
                                                       Opportunity__c = oppTest.id, 
                                                       Installment_Status__c = 'Paid',
                                                       CurrencyIsoCode = 'USD');
        insert instTest;
        
        //##### Tests #####
        Test.startTest();
          oppTest.Extra_Financial_Status__c = 'Cancel Kept';
          update oppTest;  
          list<Event_Attendee__c> lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];	
          system.assertEquals(lstEvAtt.get(0).Event_Attendees_Status__c , 'Not Attending');
        
          oppTest.Extra_Financial_Status__c = 'Replaced';
          update oppTest;  
          lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];	
          system.assertEquals(lstEvAtt.get(0).Event_Attendees_Status__c , 'Replaced');
        
          oppTest.Finance_Status__c = 'Carry Forward';
          update oppTest;
          lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];	
          system.assertEquals(lstEvAtt.get(0).Event_Attendees_Status__c , 'Not Attending');
        
          oppTest.StageName = 'Cancelled';
          update oppTest;  
          lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];
          list<Installments__c> lstiInst = [SELECT Installment_Status__c FROM Installments__c];
          system.assertEquals(lstEvAtt.get(0).Event_Attendees_Status__c , 'Cancelled');
          system.assertEquals(lstiInst.get(0).Installment_Status__c , 'Canceled');
        Test.stopTest();
    }
    
    //-----------------------------------------------------------------------------
    
    public static testMethod void testLote(){
        //=== Custom setting ===
        Application_Config__c appConfig = new Application_Config__c(Name = 'BucketUserId', Value__c = '00536000004lQpq');
    	insert appConfig;     
        
        //=== Account ===
        Account accTest = SObjectInstance.createAccount2();
        insert accTest;
        
        //=== Contact ===
        Contact cttTest = SObjectInstance.createContato(accTest.id);
        insert cttTest;
        
        //=== Opportunity ===
        list<Opportunity> lstOppTest = new list<Opportunity>();
        for(integer i = 0; i < LOTE; i++){
       		Opportunity oppTest = SObjectInstance.createOpportunidade(accTest.id, RT_OPP);
        	oppTest.Contact__c = cttTest.id;
            lstOppTest.add(oppTest);
        }
        insert lstOppTest;
        
        //=== Event ===
        list<Event__c> lstEvt = new list<Event__c>();
        for(integer i = 0; i < LOTE; i++){
        	lstEvt.add(SObjectInstance.createEvent());
        }
        insert lstEvt;
        
        //=== Event_Attendee__c ===        
        list<Event_Attendee__c> lstEA = new list<Event_Attendee__c>();
        for(integer i = 0; i < LOTE; i++){
            lstEA.add(SObjectInstance.createEventAttendee(lstOppTest.get(i).id, accTest.id, cttTest.id, lstEvt.get(i).id));
        }
        insert lstEA;
        
        //=== Installments__c ===
        list<Installments__c> lstInst = new list<Installments__c>();
        for(integer i = 0; i < LOTE; i++){
        lstInst.add(new Installments__c(Name = '10020030' + i, 
                                        Opportunity__c = lstOppTest.get(i).id, 
                                        Installment_Status__c = 'Paid',
                                        CurrencyIsoCode = 'USD'));
        }
        insert lstInst;
        
        //##### Tests #####
        Test.startTest();
          for(integer i = 0; i < LOTE/2; i++){
            lstOppTest.get(i).Extra_Financial_Status__c = 'Cancel Kept';
    	  }
          update lstOppTest;  
          list<Event_Attendee__c> lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];	
          system.assertEquals(lstEvAtt.get(0).Event_Attendees_Status__c , 'Not Attending');
        
          for(integer i = 0; i < LOTE/2; i++){
            lstOppTest.get(i).Extra_Financial_Status__c = 'Replaced';
    	  }
          update lstOppTest;  
          lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];	
          system.assertEquals('Replaced', lstEvAtt.get(0).Event_Attendees_Status__c);
        
          for(integer i = 0; i < LOTE/2; i++){
            lstOppTest.get(i).Finance_Status__c = 'Carry Forward';
    	  }
          update lstOppTest;
          lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];	
          system.assertEquals('Not Attending', lstEvAtt.get(0).Event_Attendees_Status__c);

          for(integer i = 0; i < LOTE/2; i++){
            lstOppTest.get(i).StageName = 'Cancelled';
    	  }        
          update lstOppTest;  
          lstEvAtt = [SELECT Event_Attendees_Status__c FROM Event_Attendee__c];
          list<Installments__c> lstiInst = [SELECT Installment_Status__c FROM Installments__c];
          system.assertEquals('Cancelled', lstEvAtt.get(0).Event_Attendees_Status__c);
          system.assertEquals('Canceled', lstiInst.get(0).Installment_Status__c);
        
          system.assertEquals('Confirmed', lstEvAtt.get(LOTE-1).Event_Attendees_Status__c);
          system.assertEquals('Paid', lstiInst.get(LOTE-1).Installment_Status__c);        
        Test.stopTest();
    }    
}