@isTest
public class Test_CalendarInviteController {

    private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');

  @isTest
  static void TestCalendarInviteController1()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);
    
    Application_Config__c cs = new Application_Config__c();
    cs.Name='BucketUserId';
    cs.value__c ='00536000004lQpq';
        //cs.Other fiels values
    insert cs;
    
    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    Calendar_Invite__c calNewObj1 = new Calendar_Invite__c();
    calNewObj1.Event__c = iEvent.Id;
    calNewObj1.Description__c = 'test Description';
    calNewObj1.End_Date__c = system.today()+2;
    calNewObj1.Invite_Type__c = 'Day 1';
    calNewObj1.See_Guest_List__c = false;
    calNewObj1.Send_Notification__c = 15;
    calNewObj1.Start_Date__c = system.today();
    calNewObj1.isInviteesLimitReached__c = true;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj1.GoogleCalendarEventId__c = 'd2ro10reknvpm8i9hkqac53hu0';
    calNewObj1.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj1.Visibility__c =  'private';
    insert calNewObj1;
    
    Calendar_Invitee__c calInviteeObj5 =  new Calendar_Invitee__c();
    calInviteeObj5.Calendar_Invite__c = calNewObj1.Id;
    calInviteeObj5.Invite_Status__c = 'Pending';
    calInviteeObj5.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj5.Opportunity__c = iOpp.Id;
    insert calInviteeObj5;
    
    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Day 1';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    calNewObj.isInviteesLimitReached__c = false;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.ParentCalendarInvite__c = calNewObj1.id;
    
    calNewObj.GoogleCalendarEventId__c = 'd2ro10reknvpm8i9hkqac53hu0';
    calNewObj.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj.Visibility__c =  'private';
    insert calNewObj;
    

    List<Calendar_Invitee__c> calInviteeLst =  new List<Calendar_Invitee__c>();
    
    
    Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
    calInviteeObj.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj.Invite_Status__c = 'Pending';
    calInviteeObj.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj);
    
    Calendar_Invitee__c calInviteeObj1 =  new Calendar_Invitee__c();
    calInviteeObj1.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj1.Invite_Status__c = 'Pending';
    calInviteeObj1.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj1.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj1);
    
    Calendar_Invitee__c calInviteeObj2 =  new Calendar_Invitee__c();
    calInviteeObj2.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj2.Invite_Status__c = 'Pending';
    calInviteeObj2.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj2.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj2);
    
    Calendar_Invitee__c calInviteeObj3 =  new Calendar_Invitee__c();
    calInviteeObj3.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj3.Invite_Status__c = 'Pending';
    calInviteeObj3.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj3.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj3);
    insert calInviteeLst;
    
    ApexPages.currentPage().getParameters().put('id',iOpp.Id); 
    ApexPages.StandardSetController sc = new ApexPages.StandardSetController(calInviteeLst);
    CalendarInviteController calInviteCntrl = new CalendarInviteController(sc);
       
    PageReference pageRef = Page.CalenderInvitee;
    
    calInviteCntrl.getInvites();
    calInviteCntrl.getEmailTypes();
    calInviteCntrl.businessEmail= true;
    //CalendarInviteController.cEvent
    
    Test.startTest();
    try{
    
    GoolgeCalendarCalloutMock fakeResponse = new GoolgeCalendarCalloutMock(200,
                                                 'Complete',
        //'[{"kind": "calendar#event","etag": "\"3086257014590000\"","id": "i7rl42kjm8qp26s792h9j17gbs","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=aTdybDQya2ptOHFwMjZzNzkyaDlqMTdnYnMgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-25T06:48:27.000Z","updated": "2018-11-25T06:48:27.295Z","summary": "GRI Hoteis 2018 - Day 5","location": "Hotel Grand Hyatt São Paulo , Av. Das Nações Unidas, 13.301, São Paulo, SP, Brazil","creator": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-25T04:48:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-25T09:48:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "i7rl42kjm8qp26s792h9j17gbs@google.com","sequence": 0,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"}]}]',
        '[{"access_token": "ya29.Gl1gBq2PKMPtMkPulLInDEouyFmZ6ciceuPxaGA1oKmoMMxl_or1i3eVB4wT9G1ng-ckgwuADt2SiWki0CpYCVuzXE2xo1wJHdkphvPZtcsNMolI7tEq47e2fs-J-M0","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',
        //'[{"access_token": "ya29.Gl1fBgy8LaEDcBiYocc6kgZGejKrZz1j9_JSJ1zPvHwtC_uOyfOKctTMBqFTZFhWOKZwLdp3PI2dVgl7GBEfWeKPGyGsi0OecyiTBbvsvpqEJmls5h-O7MyGL9SRcN4","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',     
       null);
    Test.setMock(HttpCalloutMock.class, fakeResponse);
    calInviteCntrl.retrieveGoogleAccessToken();
    
    }catch(exception e){
    
    }
    Test.StopTest();
  }
  
  @isTest
  static void TestCalendarInviteController3()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);
    
    Application_Config__c cs = new Application_Config__c();
    cs.Name='BucketUserId';
    cs.value__c ='00536000004lQpq';
        //cs.Other fiels values
    insert cs;
    
    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    Calendar_Invite__c calNewObj1 = new Calendar_Invite__c();
    calNewObj1.Event__c = iEvent.Id;
    calNewObj1.Description__c = 'test Description';
    calNewObj1.End_Date__c = system.today()+2;
    calNewObj1.Invite_Type__c = 'Day 1';
    calNewObj1.See_Guest_List__c = false;
    calNewObj1.Send_Notification__c = 15;
    calNewObj1.Start_Date__c = system.today();
    calNewObj1.isInviteesLimitReached__c = true;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj1.GoogleCalendarEventId__c = 'd2ro10reknvpm8i9hkqac53hu0';
    calNewObj1.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj1.Visibility__c =  'private';
    insert calNewObj1;
    
    Calendar_Invitee__c calInviteeObj5 =  new Calendar_Invitee__c();
    calInviteeObj5.Calendar_Invite__c = calNewObj1.Id;
    calInviteeObj5.Invite_Status__c = 'Pending';
    calInviteeObj5.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj5.Opportunity__c = iOpp.Id;
    insert calInviteeObj5;
    
    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Day 1';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    calNewObj.isInviteesLimitReached__c = false;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.ParentCalendarInvite__c = calNewObj1.id;
    
    calNewObj.GoogleCalendarEventId__c = 'd2ro10reknvpm8i9hkqac53hu0';
    calNewObj.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj.Visibility__c =  'private';
    insert calNewObj;
    

    List<Calendar_Invitee__c> calInviteeLst =  new List<Calendar_Invitee__c>();
    
    
    Calendar_Invitee__c calInviteeObj0 =  new Calendar_Invitee__c();
    calInviteeObj0.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj0.Invite_Status__c = 'Pending';
    calInviteeObj0.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj0.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj0);
    
    Calendar_Invitee__c calInviteeObj1 =  new Calendar_Invitee__c();
    calInviteeObj1.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj1.Invite_Status__c = 'Pending';
    calInviteeObj1.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj1.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj1);
    
    Calendar_Invitee__c calInviteeObj2 =  new Calendar_Invitee__c();
    calInviteeObj2.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj2.Invite_Status__c = 'Pending';
    calInviteeObj2.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj2.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj2);
    
    Calendar_Invitee__c calInviteeObj3 =  new Calendar_Invitee__c();
    calInviteeObj3.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj3.Invite_Status__c = 'Pending';
    calInviteeObj3.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj3.Opportunity__c = iOpp.Id;
    calInviteeLst.add(calInviteeObj3);
    insert calInviteeLst;
    
    list<Calendar_Invite__c> calInviteLst = new List<Calendar_Invite__c>();
    calInviteLst.add(calNewObj);
    map<Id,string> CalinviteToAttendeeStatusMap = new map<Id,string>();
    Map<Id,set<string>> notifiedEmailsMap = new Map<Id,set<string>>();
    for(Calendar_Invitee__c calInviteeObj:calInviteeLst){
        if(!notifiedEmailsMap.containsKey(calInviteeObj.Calendar_Invite__c)){
            notifiedEmailsMap.put(calInviteeObj.Calendar_Invite__c, new Set<String>{calInviteeObj.InviteeEmail__c});
        }else{
            notifiedEmailsMap.get(calInviteeObj.Calendar_Invite__c).add(calInviteeObj.InviteeEmail__c);
        }
        CalinviteToAttendeeStatusMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.Invite_Status__c);
    }
    set<string> setOfEmails =  new set<string>();
    setOfEmails.add('madhTestasd@gmail.com');
    Map<Id,Id> eventToOppMap = new Map<Id,Id>();
    eventToOppMap.put(iOpp.Event__c,iOpp.Id);
    set<Id> oppIds = new set<Id>();
    oppIds.add(iOpp.Id);
 
    ApexPages.currentPage().getParameters().put('id',iOpp.Id); 
    ApexPages.StandardSetController sc = new ApexPages.StandardSetController(calInviteeLst);
    CalendarInviteController calInviteCntrl = new CalendarInviteController(sc);
       
    PageReference pageRef = Page.CalenderInvitee;
    
    calInviteCntrl.getInvites();
    calInviteCntrl.getEmailTypes();
    calInviteCntrl.businessEmail= true;
    //CalendarInviteController.cEvent
    
    Test.startTest();
    try{
    
    GoolgeCalendarCalloutMock fakeResponse = new GoolgeCalendarCalloutMock(200,
                                                 'Complete',
        //'[{"kind": "calendar#event","etag": "\"3086257014590000\"","id": "i7rl42kjm8qp26s792h9j17gbs","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=aTdybDQya2ptOHFwMjZzNzkyaDlqMTdnYnMgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-25T06:48:27.000Z","updated": "2018-11-25T06:48:27.295Z","summary": "GRI Hoteis 2018 - Day 5","location": "Hotel Grand Hyatt São Paulo , Av. Das Nações Unidas, 13.301, São Paulo, SP, Brazil","creator": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-25T04:48:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-25T09:48:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "i7rl42kjm8qp26s792h9j17gbs@google.com","sequence": 0,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"}]}]',
        '[{"access_token": "ya29.Gl1gBq2PKMPtMkPulLInDEouyFmZ6ciceuPxaGA1oKmoMMxl_or1i3eVB4wT9G1ng-ckgwuADt2SiWki0CpYCVuzXE2xo1wJHdkphvPZtcsNMolI7tEq47e2fs-J-M0","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',
        //'[{"access_token": "ya29.Gl1fBgy8LaEDcBiYocc6kgZGejKrZz1j9_JSJ1zPvHwtC_uOyfOKctTMBqFTZFhWOKZwLdp3PI2dVgl7GBEfWeKPGyGsi0OecyiTBbvsvpqEJmls5h-O7MyGL9SRcN4","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',     
       null);
    Test.setMock(HttpCalloutMock.class, fakeResponse);
    //calInviteCntrl.retrieveGoogleAccessToken();
    CreateCalendarInviteBatch be = new CreateCalendarInviteBatch(calInviteLst,notifiedEmailsMap,setOfEmails,'',iOpp,'Opportunity',eventToOppMap,oppIds,CalinviteToAttendeeStatusMap);
    database.executeBatch(be,1);
            
    }catch(exception e){
    
    }
    Test.StopTest();
  }
  
  /*@isTest(seeAlldata=true)
  static void TestCalendarInviteControllerBulk()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);

    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Day 1';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    calNewObj.isInviteesLimitReached__c = false;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.GoogleCalendarEventId__c = 'd2ro10reknvpm8i9hkqac53hu0';
    calNewObj.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj.Visibility__c =  'private';
   
    List<Calendar_Invite__c> calInviteNewLst = new List<Calendar_Invite__c>();
    calInviteNewLst.add(calNewObj);
    insert calInviteNewLst;
    Test.startTest();
        GoolgeCalendarCalloutMock fakeResponse = new GoolgeCalendarCalloutMock(200,
                                                 'Complete',
        //'[{"kind": "calendar#event","etag": "\"3086257014590000\"","id": "i7rl42kjm8qp26s792h9j17gbs","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=aTdybDQya2ptOHFwMjZzNzkyaDlqMTdnYnMgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-25T06:48:27.000Z","updated": "2018-11-25T06:48:27.295Z","summary": "GRI Hoteis 2018 - Day 5","location": "Hotel Grand Hyatt São Paulo , Av. Das Nações Unidas, 13.301, São Paulo, SP, Brazil","creator": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-25T04:48:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-25T09:48:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "i7rl42kjm8qp26s792h9j17gbs@google.com","sequence": 0,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"}]}]',
        '[{"access_token": "ya29.Gl1gBq2PKMPtMkPulLInDEouyFmZ6ciceuPxaGA1oKmoMMxl_or1i3eVB4wT9G1ng-ckgwuADt2SiWki0CpYCVuzXE2xo1wJHdkphvPZtcsNMolI7tEq47e2fs-J-M0","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',
        //'[{"access_token": "ya29.Gl1fBgy8LaEDcBiYocc6kgZGejKrZz1j9_JSJ1zPvHwtC_uOyfOKctTMBqFTZFhWOKZwLdp3PI2dVgl7GBEfWeKPGyGsi0OecyiTBbvsvpqEJmls5h-O7MyGL9SRcN4","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',     
       null);
    Test.setMock(HttpCalloutMock.class, fakeResponse);
       CreateGoogleEventsQueable updateJob = new CreateGoogleEventsQueable(calInviteNewLst);
       ID jobID = System.enqueueJob(updateJob); 
    Test.StopTest();

    
         
    
  }*/
  /*@isTest(seeAlldata=true)
  static void TestCalendarInviteController2()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);

    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    try{
        Calendar_Invite__c calNewObj = new Calendar_Invite__c();
        calNewObj.Event__c = iEvent.Id;
        calNewObj.Description__c = 'test Description';
        calNewObj.End_Date__c = system.today()+2;
        calNewObj.Invite_Type__c = 'Special Lunch';
        calNewObj.See_Guest_List__c = false;
        calNewObj.Send_Notification__c = 15;
        calNewObj.Start_Date__c = system.today();
        //calNewObj.Time_Zone__c = iEvent.Timezone__c;
        //calNewObj.Venue__c = iEvent.Venue__c;
        calNewObj.Visibility__c =  'private';
        insert calNewObj;
        
        Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
        calInviteeObj.Calendar_Invite__c = calNewObj.Id;
        calInviteeObj.Invite_Status__c = 'Pending';
        calInviteeObj.InviteeEmail__c = 'test123@fdsaf.com';
        calInviteeObj.Opportunity__c = iOpp.Id;
        insert calInviteeObj;
        
        List<Calendar_Invitee__c> calInviteeLst =  new List<Calendar_Invitee__c>();
        calInviteeLst.add(calInviteeObj);
        
            ApexPages.currentPage().getParameters().put('id',iOpp.Id);  
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(calInviteeLst);
            CalendarInviteController calInviteCntrl = new CalendarInviteController(sc);
              
            PageReference pageRef = Page.CalenderInvitee;
            calInviteCntrl.getInvites();
            Test.startTest();
            calInviteCntrl.retrieveGoogleAccessToken();
            GoolgeCalendarCalloutMock fakeResponse = new GoolgeCalendarCalloutMock(200,
                                                     'Complete',
            '[{"kind": "calendar#event","etag": "\"3086257014590000\"","id": "i7rl42kjm8qp26s792h9j17gbs","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=aTdybDQya2ptOHFwMjZzNzkyaDlqMTdnYnMgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-25T06:48:27.000Z","updated": "2018-11-25T06:48:27.295Z","summary": "GRI Hoteis 2018 - Day 5","location": "Hotel Grand Hyatt São Paulo , Av. Das Nações Unidas, 13.301, São Paulo, SP, Brazil","creator": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-25T04:48:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-25T09:48:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "i7rl42kjm8qp26s792h9j17gbs@google.com","sequence": 0,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"}]}]',
            //'[{"access_token": "ya29.Gl1fBgy8LaEDcBiYocc6kgZGejKrZz1j9_JSJ1zPvHwtC_uOyfOKctTMBqFTZFhWOKZwLdp3PI2dVgl7GBEfWeKPGyGsi0OecyiTBbvsvpqEJmls5h-O7MyGL9SRcN4","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',     
           null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
    
        Test.StopTest();
    }catch(exception e){
    
    }
  }*/
  /*@isTest(seeAlldata=true)
  static void TestCalendarInviteController3()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);

    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Day 1';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    calNewObj.isInviteesLimitReached__c = false;
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.GoogleCalendarEventId__c = 'd2ro10reknvpm8i9hkqac53hu0';
    calNewObj.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    calNewObj.Visibility__c =  'private';
    try{
    insert calNewObj;
    }catch(exception e){
    
    }
    
    Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
    calInviteeObj.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj.Invite_Status__c = 'Pending';
    calInviteeObj.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj.Opportunity__c = iOpp.Id;
    insert calInviteeObj;
    
    List<Calendar_Invitee__c> calInviteeLst =  new List<Calendar_Invitee__c>();
    calInviteeLst.add(calInviteeObj);
    
    ApexPages.currentPage().getParameters().put('id',iOpp.Id); 
    ApexPages.StandardSetController sc = new ApexPages.StandardSetController(calInviteeLst);
    RecursionCls.isUpdateLogicNotRequired = true;
    CalendarInviteController calInviteCntrl = new CalendarInviteController(sc);
       
    PageReference pageRef = Page.CalenderInvitee;
    calInviteCntrl.getInvites();
    Test.startTest();
    try{
        
        GoolgeCalendarCalloutMock fakeResponse1 = new GoolgeCalendarCalloutMock(200,
                                                     'Complete',
         '[{"kind": "calendar#event","etag": "\"3086413830483000\"","id": "4lapa3bod1eff2viegh6vl7rio","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=NGxhcGEzYm9kMWVmZjJ2aWVnaDZ2bDdyaW8gZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-25T05:56:01.000Z","updated": "2018-11-26T04:35:15.386Z","summary": "GRI Hoteis 2018 - Day 1","description": "Day 1","location": "Taj Mahal , Dharmapuri, Forest Colony, Tajganj, Agra, Uttar Pradesh, India","creator": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-24T17:00:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-24T18:00:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "4lapa3bod1eff2viegh6vl7rio@google.com","attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"}]}]',
           null);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        
        calInviteCntrl.retrieveGoogleAccessToken();
        Test.StopTest();
    }catch(exception e){
    
    }
  }*/
}