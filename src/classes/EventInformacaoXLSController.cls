/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Class controladora das páginas EventInformacaoXLS.page EventBookReportXLS.page
*
* NAME: EventInformacaoXLSController.cls
* AUTHOR: RVR                                                  DATE: 04/04/2016
*******************************************************************************/
public with sharing class EventInformacaoXLSController 
{    
  
    public class SessaoWrapper
    {
      public Session__c pSessao {get; set;}
      public String urlSession 
      { 
        get 
        { 
          return (this.pSessao != null)
         ? System.URL.getSalesforceBaseURL().toExternalForm() + '/' : null; 
        } 
      }

      //Disponibiliza hora gmt
      public DateTime dtStart 
      { 
        get 
        { 
          return (this.pSessao != null)
         ? Datetime.newInstanceGMT(this.pSessao.Start_Date__c.date(), this.pSessao.Start_Date__c.time()) : null; 
        } 
      }
      public DateTime dtEnd 
      {
        get 
        { 
          return (this.pSessao != null)
         ? Datetime.newInstanceGMT(this.pSessao.End_Date__c.date(), this.pSessao.End_Date__c.time()) : null; 
        } 
      }
      //Disponibiliza hora gmt
      public SessaoWrapper(Session__c aSession) { this.pSessao = aSession; }
    }
    private Id eventId {get; set;}

  public EventInformacaoXLSController(ApexPages.StandardController controller)
  {        
        Event__c lEvent = (Event__c)controller.getRecord();
        this.eventId = lEvent.Id;
  }

    public List<SessaoWrapper> getLstSessao()
    {
        list<SessaoWrapper> lstSessao = new list<SessaoWrapper>();
    	
        for (Session__c iSession: [SELECT Id, Name, Session_indexing__c, Room__c, Start_date__c, Start_Time__c, Status__c, 
                                  End_Date__c, End_Time__c, Event__c, Event__r.Name, Title__c, Description__c,
                                  Foreign_title__c, Foreign_description__c,
                                  (SELECT Id, Contact__r.Id ,Session__c, Contact__r.Name, Contact__r.Account.Name, Session_Status__c,
                                  Contact__r.Company_Name_Badge__c, Contact__r.Badge_Name__c, Contact__r.Job_title_badge__c, Contact__r.Title, 
                                  Contact__r.Marketing_Edited_Photo__c, Contact__r.MailingCountry, Contact__r.Foreing_Name__c, Contact__r.Job_title_foreign__c,
                                  Contact__r.Company_Foreign_Name__c, Contact__r.Secretary_Name_1__c, Contact__r.Secretary_Phone_1__c, Contact__r.Phone,
                                  Contact__r.Secretary_Email_1__c, Contact__r.Email, Session_Position__c, Contact__r.Personal_Summary_Biography__c,
                                  Contact__r.Account.Description, Contact__r.Account.Company_Summary_Profile__c
                                  FROM Events_Session_Magazines_participants__r) 
                                  FROM Session__c 
                                  WHERE Event__c =:this.eventId 
                                  AND Status__c != 'Cancelled'
                                  ORDER BY Session_indexing__c ASC])
        {
          lstSessao.add(new SessaoWrapper(iSession));
        }
        return lstSessao;
    }
}