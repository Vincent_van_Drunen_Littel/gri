public class NewInstanceSObject {
    
    public static Account createAccount()
    {
        Account acc = new Account(
            Name = 'Test Account'
        );
        return acc;
    }
    public static Account createAccount(String AccName)
    {
        Account acc =  new Account(
            Name = AccName
        );
        return acc;
    }
    public static Club__c createClub()
    {
        return new Club__c(
            Name = 'Test Club'
        );

    }
    public static Club__c createClub(String clubname)
    {
        return new Club__c(
            Name = clubname
        );
    }
    
    public static Contact createContact(Id AccountId) 
    {
        return new Contact(
            AccountId = AccountId,
            FirstName = 'Teste First',
            LastName = 'Teste Last' );
    }
    public static Contact createContact(Id AccountId, String FirstName, String LastName) 
    {
        return new Contact(
            AccountId = AccountId,
            FirstName = FirstName,
            LastName = LastName);
    }    
    public static Opportunity createOpportunityMembership(Account theAccount,  
                                                          Contact theContact, Club__c club, 
                                                          Date closeDate)
    {
        return new Opportunity(
            RecordTypeId = RecordTypeMemory.getRecType('Opportunity', 'Membership'),
            Name = 'Opp de Teste -- ' + theContact.Name,
            StageName = 'Prospect',
            AccountId = theAccount.Id,
            Club__c = club.Id,
            Contact__c = theContact.Id,
            CloseDate = closeDate,
            Type = 'Venda'
        );
    }
 
    public static Product2 createProduct()
    {
        return new Product2(
            Name = 'Productc Test',
            ProductCode = '123a'
        );
    }
    public static id getStandardPricebookId() 
    {
        return test.getStandardPricebookId();
    }
    public static PricebookEntry pricebookEntry( ID aCatalogo, ID aProduto ) 
    {
        return new PricebookEntry(
            Pricebook2Id = aCatalogo,
            Product2Id = aProduto,
            IsActive = true,
            UnitPrice = 20.00 );
    }
    
    /*
    public static Opportunity createOpportunidadeMagazine(Id acc, Id recType, Id ctc)
    {
        return new Opportunity(
            RecordTypeId = recType,
            Name = 'Opp de Teste',
            StageName = 'Prospect',
            AccountId = acc,
            Contact__c = ctc,
            CloseDate = system.today(),
            Type = 'Venda' );
    }
    */
    /*
    public static Opportunity createOpportunityEvent(Id acc, Id recType, Id ctc, Id event)
    {
        return new Opportunity(
            RecordTypeId = recType,
            Name = 'Opp de Teste',
            StageName = 'Prospect',
            AccountId = acc,
            Event__c = event,
            Contact__c = ctc,
            CloseDate = system.today(),
            Type = 'Venda' );
    }
    */
    /*
    public static Opportunity createOpportunityMember(Id acc, Id recType, Id ctc, Id club)
    {
        return new Opportunity(
            RecordTypeId = recType,
            Name = 'Opp de Teste',
            StageName = 'Prospect',
            AccountId = acc,
            Club__c = club,
            Contact__c = ctc,
            CloseDate = system.today(),
            Type = 'Venda' );
    }
    */
    /*
    public static Opportunity createOpportunitySPEX(Id acc, Id recType, Id ctc)
    {
        return new Opportunity(
            RecordTypeId = recType,
            Name = 'Opp de Teste',
            StageName = 'Prospect',
            AccountId = acc,
            Contact__c = ctc,
            CloseDate = system.today(),
            Type = 'Venda' );
    }
    */
    /*
    public static OpportunityLineItem createOpportunityLineItem( ID aOportunidade, ID aEntradaPreco)
    {
        return new OpportunityLineItem(
            OpportunityId = aOportunidade,
            PricebookEntryId = aEntradaPreco,
            Quantity = 1,
            TotalPrice = 20.00 );
    }
    */
    /*
    public static OpportunityLineItem createOpportunityLineItem( ID aOportunidade, ID aEntradaPreco)
    {
        return new OpportunityLineItem(
            OpportunityId = aOportunidade,
            PricebookEntryId = aEntradaPreco,
            Quantity = 1,
            UnitPrice = 10.00);
    }
    */
}