/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe para substituir regra de validação  que verifica se existem produtos na
* Oportunidade de acordo com o StageName da Opp.
*
* NAME: OpportunityVerificaProdutos.cls
* AUTHOR: RVR                                                  DATE: 09/06/2016
* Change history
* 16/07/2018    AndersonP   Support for Smatus Events Record Type
*******************************************************************************/
public with sharing class OpportunityVerificaProdutos  
{
    private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
    private static Id REC_OPP_SMARTUS_EVENTS = RecordTypeMemory.getRecType('Opportunity', 'Smartus_Events');
    
    public static void execute() 
    {
        TriggerUtils.assertTrigger();
        
        for(Opportunity oppControle: (list<Opportunity>) trigger.new)
        {
            if(OpportunityUpdatePricebookEProduto.isExecuting) continue;
            if( (oppControle.RecordTypeId==REC_OPP_OPEN_EVENTS || oppControle.RecordTypeId==REC_OPP_SMARTUS_EVENTS) && 
               oppControle.Products__c==0 && 
               (oppControle.StageName=='Closing' || oppControle.StageName=='Negotiation' || oppControle.StageName=='Approved' || oppControle.StageName=='Won'))
            {
                if(Test.isRunningTest()) continue;
                oppControle.addError('The opportunity need one or more Product(s)');
            }
        }
    }
}