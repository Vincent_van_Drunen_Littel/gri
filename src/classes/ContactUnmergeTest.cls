/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe de testes para ContactUnmerge.cls
*
* NAME: ContactUnmergeTest.cls
* AUTHOR: VOD                                                 DATE: 30/01/2017
*******************************************************************************/
@isTest
public class ContactUnmergeTest {

  Static String RT_ID = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
    
  static testMethod void testeFuncional(){
    Account accTest = SObjectInstance.createAccount2();
    Database.insert(accTest);
      
    Contact cttTest1 = SObjectInstance.createContato(accTest.id);
    Database.insert(cttTest1);
      
    Contact cttTest2 = SObjectInstance.createContato2(accTest.id);
    Database.insert(cttTest2);
      
    Event__c evtTest = SObjectInstance.createEvent();
    Database.insert(evtTest);
      
    //Configuracao personalizada que o campo valor e um id
    Application_Config__c apConf = new Application_Config__c();
    apConf.Name = 'BucketUserId';
    apConf.Value__c = accTest.id;
    Database.insert(apConf);
      
    Opportunity oppTest1 = SObjectInstance.createOpportunidade2(accTest.id, RT_ID, cttTest1.Id, evtTest.Id);
    Database.insert(oppTest1);
      
    Opportunity oppTest2 = SObjectInstance.createOpportunidade2(accTest.id, RT_ID, cttTest2.Id, evtTest.Id);
    Database.insert(oppTest2);
      
    Test.startTest();
      try{
        Database.merge(cttTest1, cttTest2);
      } 
      catch(Exception e)
      {
        Boolean error = e.getMessage().contains('These Contacts have Opportunities in the same Event. Merge not permitted.');
        System.AssertEquals(error, true);
      }

    Test.stopTest();
    
  }

  static testMethod void testePermissao(){
    //===Teste do bloqueio === 
    ContactUnmergeProfile__c cups = new ContactUnmergeProfile__c(Name = 'teste', Profile_ID__c = String.valueOf(UserInfo.getProfileId()).left(15));
    insert cups;
      
    Account accTest = SObjectInstance.createAccount2();
    Database.insert(accTest);
      
    Contact cttTest1 = SObjectInstance.createContato(accTest.id);
    Database.insert(cttTest1);
      
    Contact cttTest2 = SObjectInstance.createContato2(accTest.id);
    Database.insert(cttTest2);
      
    Event__c evtTest = SObjectInstance.createEvent();
    Database.insert(evtTest);
      
    //Configuracao personalizada que o campo valor e um id
    Application_Config__c apConf = new Application_Config__c();
    apConf.Name = 'BucketUserId';
    apConf.Value__c = accTest.id;
    Database.insert(apConf);
      
    Opportunity oppTest1 = SObjectInstance.createOpportunidade2(accTest.id, RT_ID, cttTest1.Id, evtTest.Id);
    Database.insert(oppTest1);
      
    Opportunity oppTest2 = SObjectInstance.createOpportunidade2(accTest.id, RT_ID, cttTest2.Id, evtTest.Id);
    Database.insert(oppTest2);
      
    Test.startTest();
      try{
        Database.merge(cttTest1, cttTest2);
      } 
      catch(Exception e)
      {
        Boolean error = e.getMessage().contains('These Contacts have Opportunities in the same Event. Merge not permitted.');
        System.AssertEquals(error, true);
      }

    Test.stopTest();
    
  }  
    
    
}