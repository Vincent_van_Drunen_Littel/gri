global class GDPR_Forget_Accounts implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.stateful {
 
    List<Id> contactHistoryToDelete = new List<Id>();
    public Datetime currentTime = System.now();

    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator([SELECT Id, FirstName, LastName, AccountId FROM Contact WHERE Consent_Given_to_GDPR__c = 'No' and GDPR_Delete_Schedule__c >= :Date.today()]);
    } 
   
   global void execute(Database.BatchableContext BC, List<sObject> allContact){
        //Create list of Id's to delete/update
        List<Contact> ContactsToUpdate = (List<Contact>)allContact;
        List<Contact> contactsToInsert = new List<Contact>();
        List<Contact> nullContactList = new List<Contact>();
        List<Id> idList = new List<Id>();

        //Create new contact and assign name so contact doesn't get re-added
        for(Contact c : ContactsToUpdate){
            idList.add(String.valueOf(c.Id));
            contactHistoryToDelete.add(String.valueOf(c.Id));
            Contact newContact = new Contact();
            newContact.FirstName = c.FirstName;
            newContact.LastName = c.LastName;
            newContact.GDPR_Delete_Complete__c = true;
            newContact.Consent_Given_to_GDPR__c = 'No';
            newContact.Status__c = 'Disabled';
            newContact.Disabled_Reason__c = 'Opted To be Forgotten';
            //Account account = c.Account;
            String accountId = String.valueOf(c.AccountId);
            newContact.AccountId = accountId;

            contactsToInsert.add(newContact);

            //Nullify all exsiting contact fields
            nullContactList.add(nullContact(c));
        }
        update nullContactList;
        insert contactsToInsert;

        //Delete all tasks
        List<Task> taskList = [SELECT Id, WhoId FROM Task WHERE WhoId IN :idList];
        delete taskList;

        //Delete all meetings
        List<Event> eventList  = [SELECT Id, WhoId FROM Event WHERE WhoId IN :idList];
        delete eventList;

        //Delete all notes
        List<Note> noteList = [SELECT Id, ParentId FROM Note WHERE ParentId IN :idList];
        delete noteList;

        //Delete all opportunities that aren't won
        List<Opportunity> oppList = [SELECT Id, Contact__c from Opportunity where Contact__c IN :idList AND StageName != 'Won' AND StageName != 'Approved by Finance' AND StageName != 'Approved'];
        delete oppList;

        //Rename won opporunities to ensure customers data is not present

        List<Opportunity> wonOppList = [SELECT Id, Contact__c, CompanyName__c,I_Confirm_Badge_Information_is_Correct__c, Events_Attending_next_twelve__c from Opportunity where Contact__c IN :idList AND (StageName = 'Won' OR StageName = 'Approved by Finance' OR StageName = 'Approved')];
        List<Opportunity> opportunitiesToInsert = new List<Opportunity>();
        List<String> oppsToDeleteList = new List<String>();
        for(Opportunity opp : wonOppList){
            opp.Name = 'Forgotten One ' + String.valueOf(System.Now()) + ' ' + opp.CompanyName__c;
            opp.I_Confirm_Badge_Information_is_Correct__c = true;
            opp.Events_Attending_next_twelve__c = '';
            opportunitiesToInsert.add(opp);

            //Create list of tasks against the opportunity to delete
            oppsToDeleteList.add(String.valueOf(opp.Id));
        }
        Update opportunitiesToInsert;

        //Delete tasks related to opportunity
        List<Task> oppTaskList = [SELECT Id, WhoId FROM Task WHERE WhoId IN :oppsToDeleteList];
        delete oppTaskList;

        //Create list of memberships relating to the contact and nullify name
        List<Contract> membershipsToUpdate = new List<Contract>();
        List<Contract> memberList = [SELECT Id, Contact__c, Club__c FROM Contract where Contact__c IN :idList];
        for(Contract m : memberList){
            m.Name = 'Forgotten One ' + String.valueOf(System.Now()) + ' ' + m.Club__c; 
            m.BillingCountry = '';
            m.BillingStreet = '';
            m.BillingCity = '';
            m.BillingPostalCode = '';
            m.BillingState = '';
            membershipsToUpdate.add(m);
        }
        Update membershipsToUpdate; 

        //Delete all email results relating to the contact
        List<et4ae5__IndividualEmailResult__c> emailResultList = [SELECT Id FROM et4ae5__IndividualEmailResult__c WHERE et4ae5__Contact__c IN :idList];
        delete emailResultList;

        //Delete all campaign history relating to the contact
        List<CampaignMember> campainMemberList = [SELECT Id FROM CampaignMember where Contact.Id IN :idList];
        delete campainMemberList;

        //Delete any questionaires relating to the contact 
        List<Membership_Questionaire__c> questionaireList = [SELECT Id FROM Membership_Questionaire__c WHERE Contact__r.Id IN :idList];
        delete questionaireList;

        //Delete any Notes (Chatter Files) relating to the contact
        List<ContentDocumentLink> chatterFilesList = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :idList];
        delete chatterFilesList;

        //Delete any google docs, notes, and attachments
        List<Note> notesList = [SELECT Id FROM Note WHERE ParentId IN :idList];
        delete notesList;
        
        System.debug('Finished removal of contact: ' + ContactsToUpdate);
    }

    public Contact nullContact(Contact c){

        //Nullify all fields on the contact object
        System.debug('Starting removal of contact: ' + String.valueOf(c.Id)); 
        //c.Account_Name__c = null;
        c.FirstName = 'Forgotten One ' + String.valueOf(System.Now());
        c.LastName = 'Forgotten One';
        c.MiddleName = '';
        c.Birthdate = null;
        c.CurrencyIsoCode = '';
        c.Infra_Club_India_Member__c = false;
        c.Infrastructure__c = false;
        c.Owner = null;
        c.RecordType = null;
        c.Jigsaw = '';
        c.Department = '';
        c.Description = '';
        c.Email = '';
        c.Fax = null;
        c.HomePhone = '';
        c.LeadSource = '';
        c.MobilePhone = '';
        c.OtherPhone = '';
        c.Phone = '';
        c.ReportsTo = null;
        c.Title = '';
        c.GDPR_Delete_Complete__c = true;
        c.Consent_Given_to_GDPR__c = 'No';

        c.MailingStreet = null;
        c.MailingPostalCode = null;
        c.MailingState = null;
        c.MailingCity = null;
        c.MailingCountry = null;
        c.OtherStreet = null;
        c.OtherPostalCode = null;
        c.OtherState = null;
        c.OtherCity = null;
        c.OtherCountry = null;

        c.File_Name_Marketing_Edited_logo__c = null;
        c.File_Name_Original_logo__c = null;
        c.Marketing_Edited_Logo__c = null;
        c.Marketing_Edited_Logo_Confirmed__c = null;
        c.Marketing_Edited_Logo_Updated__c = null;
        c.Global__c = false;
        c.Original_Logo__c = null;
        c.Original_Logo_Updated__c = null;
        c.Is_Marketing_Edited_Logo_Confirmed__c = false;
        c.Magazines_INFRA__c = null;
        c.Magazines_RE__c = null;
        c.of_Emails_Received_in_Last_12_Mon__c = null;
        //c.Access_to_profile_by_others__c = null;
        c.Approval_Status__c = null;
        c.Approved_Co_Chair_New__c = null;
        c.Approved_Sectorial_Co_Chair_New__c = null;
        //c.Asia_Infra__c = null;
        //c.Australia_New_Zeland_Infra__c = null;
        //c.Austria__c = null;
        c.Badge_Name_Rule__c = null;
        c.Balance__c = null;
        c.Bar_Code__c = null;
        //c.Belgium_RE__c = null;
        //c.Block_Geolocation_Tracking__c = false;
        //c.Block_Geolocation_Tracking_Date_Time__c = null;
        //c.Bolivia_RE__c = null;
        //c.Branding_or_Professional_positioning__c = null;
        //c.Brazil_Infra__c = null;
        //c.Brazil_RE__c = null;
        c.Brochure__c = null;
        c.Business_Development__c = null;
        //c.campainColom__c = null;
        //c.Canada_Infra__c = null;
        //c.Canada_RE__c = null;
        //c.Caribbean_Infra__c = null;
        //c.Caribbean_RE__c = null;
        //c.Central_America_Infra__c = null;
        //c.Central_America_RE__c = null;
        //c.Chile_Infra__c = null;
        //c.Chile_RE__c = null;
        //c.China_Infra__c = null;
        c.City_of_Birth__c = null;
        c.Clean_Campaign_Country__c = null;
        c.Cleaning_Campaign__c = false;
        c.C_Level_Hierarchy__c = null;
        //c.Club__c = null;
        c.Club_Events_members_only__c = null;
        //c.Club_RE_Infra__c = null;
        //c.Colombia_Infra__c = null;
        //c.Colombia_RE__c = null;
        c.Description_New__c = null;
        c.Company_Foreign_Name__c = null;
        c.Company_Name_Badge__c = 'null';
        c.Company_Speciality__c = null;
        c.Consent_Given_to_GDPR_by_Date_Time_St__c = null;
        c.CloudingoAgent__CES__c = null;
        c.Contact_Field_unique__c = null;
        c.Contact_Field_Unique2__c = null;
        //c.Contact_Owner_2__c = null;
        //c.Contact_Owner_3__c = null;
        //c.Contact_Owner_4__c = null;
        c.Contact_Source__c = null;
        c.Contact_Source_Description__c = null;
        c.Country_Zone_of_Interest_Americas__c = null;
        c.Country_Zone_of_interest_Asia_Pacific__c = null;
        c.Country_Zone_of_interest_Central_Eastern__c = null;
        c.Country_Zone_of_interest_MEA__c = null;
        c.Country_Zone_of_interest_Western_Nothem__c = null;
        c.rh2__Currency_Test__c = null;
        c.Date_Created_Insightly__c = null;
        c.Date_of_birth__c = null;
        //c.Date_of_Birth_Date_Time_Stamp__c = null;
        c.Date_of_the_interview__c = null;
        c.Date_of_Update_by_Researcher__c = null;
        c.Date_Updated_Insightly__c = null;
        c.rh2__Describe__c = null;
        c.Details_to_show__c = null;
        c.Dietary_preference__c = null;
        c.Direct_Line__c = null;
        c.Display_Name_Rule__c = null;
        c.Division_Profile_Last_Confirmed__c = null;
        c.Division_Profile_Updated__c = null;
        c.Division_Summary_Profile__c = null;
        c.Do_not_allow_Bulk_Email_c__c = null;
        //c.Don_t_Market__c = false;
        //c.Don_t_Market_Date_Time_Stamp__c = null;
        //c.Don_t_Process__c = false;
        //c.Don_t_Process_Date_Time_Stamp__c = null;
        //c.Don_t_Profile__c = false;
        //c.Don_t_Profile_Date_Time_Stamp__c = null;
        //c.Don_t_Track__c = false;
        //c.Don_t_Track_Date_Time_Stamp__c = null; 
        //c.Do_Not_Allow_Phone_Calls_Date_Time_Sta__c = null;
        c.Do_not_Call_Mobile__c = false;
        c.Send_Brochure__c = false;
        c.Dyn_CategoryName__c = null;
        c.Dyn_ContactId__c = null;
        c.Dyn_ContactId2__c = null;
        c.Dyn_Full_Biography__c = null;
        c.Dyn_FullBio__c = null;
        c.Dyn_PersonalSum__c = null;
        c.Education1__c = null;
        c.Eligible_Keynote__c = null;
        c.Eligible_Retreat_Member__c = null;
        c.Eligible_State_Member__c = null;
        c.Email_Click_Time_Stamp__c = null;
        c.Email_Exclusion_Rules__c = null;
        c.Email_Opened_Time_Stamp__c = null;
        //c.Email_Opt_Out_Date_Time_Stamp__c = null;
        c.Email_Salutation_Rule__c = null;
        c.Envelop_Name_Rule__c = null;
        //c.Equador_Infra__c = null;
        //c.Equador_RE__c = null;
        //c.Europe_RE__c = null;
        c.Event_announcements__c = null;
        c.Event_Attendance_Time_Stamp__c = null;
        c.Event_Sponsored__c = null;
        c.Exclusive_Events_Counter__c = null;
        //c.Export_Individual_s_Data__c = false;
        //c.Export_Individual_s_Data_Date_Time_Sta__c = null;
        c.Family_Origins__c = null;
        c.File_Name_Marketing_Edited_photo__c = null;
        c.File_Name_Original_photo__c = null;
        //c.Finland_RE__c = null;
        c.Dyn_CityForeignName__c = null;
        c.Foreign_Description_New__c = null;
        c.Foreign_Company_Description_2__c = null;
        c.Foreign_Description__c = null;
        c.Foreign_Email_Salutation_Rule__c = null;
        c.Dyn_FirstNameForeign__c = null;
        c.Dyn_JobTitleForeign__c = null;
        c.Foreign_Keynote_Summary_new__c = null;
        c.Foreign_Full_Biography__c = null;
        c.Foreign_Full_Bio_Profile_Last_Confirmed__c = null;
        c.Foreign_Full_Bio_Updated__c = null;
        c.Dyn_LastNameForeign__c = null;
        c.Dyn_MiddleNameForeign__c = null;
        c.Dyn_NickNameForeign__c = null;
        c.Foreign_Personal_Bio_Last_Confirmed__c = null;
        c.Foreign_Personal_Bio_Updated__c = null;
        c.Foreign_Personal_Summary_Biography__c = null;
        c.Foreign_Personal_Summary_Biography_2__c = null;
        c.Dyn_PostcodeForeign__c = null;
        c.Dyn_PrefixDearForeign__c = null;
        c.Foreign_Specialty_New__c = null;
        c.Foreign_Speciality_2__c = null;
        c.Foreign_Spccialty__c = null;
        c.Dyn_SuffixForeign__c = null;
        c.Foreing_Name__c = null;
        //c.Forgein_Personal_Bio_Updated__c = null;
        //c.Forget_this_Individual__c = false;
        //c.Forget_this_Individual_Date_Time_Stamp__c = null;
        //c.France_RE__c = null;
        c.Consent_Given_to_GDPR__c = null;
        c.Consent_Given_to_GDPR_by__c = null;
        c.Consent_Given_to_GDPR_Date_Time_Stamp__c = null;
        c.Hard_Consent_Updated_by__c = null;
        c.Gender__c = null;
        //c.Gender_Date_Time_Stamp__c = null;
        //c.Germany_RE__c = null;
        //c.Greece_RE__c = null;
        //c.Grip_Match_Interests__c = null;
        //c.GRI_Press_releases_and_information__c = null;
        //c.Grip_Subject_Interests__c = null;
        //c.Grip_Sync_Timestamp__c = null;
        c.Group_participation__c = null;
        c.Hotel_Jr__c = false;
        c.Hotels__c = null;
        c.How_did_you_hear_from_us__c = null;
        c.How_do_you_develop_new_business__c = null;
        c.How_do_you_invest_your_money__c = null;
        c.How_do_you_work_on_brand__c = null;
        c.How_much_return_you_seek_p_project_RS__c = null;
        c.How_often_travel_abroad_business__c = null;
        c.How_often_travel_abroad_leasure__c = null;
        c.How_to_be_informed_on_Member_only_events__c = null;
        c.How_you_like_to_fund_your_projects__c = null;
        //c.India_Infra__c = null;
        //c.Individual_s_Age__c = null;
        //c.Individual_s_Age_Date_Time_Stamp__c = null;
        c.Industrial__c = null;
        c.Industry_History__c = null;
        c.Infra_Club_India_Member__c = false;
        c.Infrastructure_BR_Club__c = false;
        c.Insightly_RecordID__c = null;
        c.rh2__Integer_Test__c = null;
        c.Interests_and_Hobbies__c = null;
        //c.Ireland_RE__c = null;
        c.Is_Foreign_Personal_Bio_Confirmed__c = false;
        c.Is_Forgein_Full_Bio_Confirmed__c = false;
        c.Is_Marketing_Edited_Photo_Confirmed__c = false;
        c.Is_the_Jewish_Calender_of_Relevance__c = null;
        c.Job_Role__c = null;
        c.Job_title_badge__c = 'null';
        c.Job_title_foreign__c = null;
        c.Key_Classification_Infra__c = null;
        c.Key_Classification_RE__c = null;
        c.Key_Classification_Retail_BR__c = null;
        c.Keynote_Summary_new__c = null;
        c.Full_Biography__c = null;
        c.Full_Bio_Profile_Last_Confirmed__c = null;
        c.Full_Bio_Updated__c = null;
        c.Keywords__c = null;
        c.Knowledge_content__c = null;
        c.Land_Development__c = null;
        c.Latin_America__c = false;
        //c.Latin_America_Infra__c = null;
        //c.Latin_America_RE__c = null;
        c.Legit_Interest_Email_Click__c = false;
        c.Legit_Interest_Email_Opened__c = false;
        c.Legit_Interest_Event_Attendance__c = false;
        c.Legit_Interest_Membership__c = false;
        c.Legit_Interest_Online_Registration_Atte__c = false;
        c.Legit_Interest_Payment_Link__c = false;
        c.Legit_Interest_Sponsorship__c = false;
        c.Level_of_Seniority__c = null;
        c.LinkedIn__c = null;
        c.Lived_Abroad_If_Yes_where__c = null;
        c.Magazine_as_a_gift__c = null;
        c.Mag_Infra_Distribution_List__c = null;
        c.Mag_RE_Distribution_List__c = null;
        c.CloudingoAgent__MAR__c = null;
        c.CloudingoAgent__MAS__c = null;
        c.CloudingoAgent__MAV__c = null;
        c.CloudingoAgent__MRDI__c = null;
        c.CloudingoAgent__MTZ__c = null;
        c.Main_Sector_of_Operation_Infra__c = null;
        c.Main_Sector_of_Operation_RE__c = null;
        c.Marital_Status__c = null;
        c.Marketing_Edited_Photo__c = null;
        c.Marketing_Edited_Photo_Confirmed__c = null;
        c.Marketing_Edited_photo_Updated__c = null;
        c.Marquee_New__c = null;
        c.Marquee_approval__c = false;
        c.Marquee_approval_process__c = null;
        c.MATCHMAKING_are_you_up_for_it__c = null;
        c.Member_bundle_ID_or_email__c = null;
        c.Member_emails_and_newsletters__c = null;
        c.Member_level__c = null;
        c.Member_role__c = null;
        c.Membership__c = null;
        c.Membership_status__c = null;
        c.Membership_Time_Stamp__c = null;
        c.Member_since__c = null;
        c.Member_to_indicate__c = null;
        //c.Mexico_Infra__c = null;
        //c.Mexico_RE__c = null;
        c.Middle_East__c = false;
        c.Middle_East_Country_Zone_of_Interest__c = null;
        c.et4ae5__Mobile_Country_Code__c = null;
        c.et4ae5__HasOptedOutOfMobile__c = false;
        c.Moderator_Rate__c = null;
        c.Most_relevant_media_read_watch__c = null;
        c.Networking_Relationship__c = null;
        c.New_Contact_Review_Task__c = false;
        c.New_Contact_Review_Task_Created__c = false;
        c.New_professional_opportunities__c = null;
        c.Nickname__c = null;
        c.Northern_Africa__c = false;
        c.Northern_Africa_Country_Zone_of_Interest__c = null;
        c.Number_of_Club_Events__c = null;
        c.Offices__c = null;
        //c.OK_to_Store_PII_Data_Elsewhere__c = false;
        c.Online_Registration_Attempt_Time_Stamp__c = null;
        c.Open_events_inclusive__c = null;
        c.Original_Photo__c = null;
        c.Original_photo_Updated__c = null;
        c.Other__c = false;
        c.CloudingoAgent__OAR__c = null;
        c.CloudingoAgent__OAS__c = null;
        c.CloudingoAgent__OAV__c = null;
        c.Other_Email__c = null;
        c.Other_information__c = null;
        //c.Other_Latin_America_Infra__c = null;
        //c.Other_Latin_America_RE__c = null;
        c.Other_Phone__c = null;
        c.CloudingoAgent__ORDI__c = null;
        c.Others__c = null;
        c.Others_Notes__c = null;
        c.Others_1__c = null;
        c.Other_Sectors_of_Interest_Infra__c = null;
        c.Other_Sectors_of_Interest_RE__c = null;
        c.CloudingoAgent__OTZ__c = null;
        c.Package_Price__c = null;
        //c.Paraguay_Infra__c = null;
        //c.Paraguay_RE__c = null;
        c.Personal_and_transferable__c = null;
        c.Personal_Bio_Last_Confirmed__c = null;
        c.Personal_Bio_Updated__c = null;
        c.Personal_Email__c = null;
        //c.Personal_Email_Address__c = null;
        c.Personal_Summary_Biography__c = null;
        //c.Peru_Infra__c = null;
        //c.Peru_RE__c = null;
        c.Photo_albums_enabled__c = false;
        c.Possible_Moderator__c = null;
        c.Prefered_activities__c = null;
        c.Prefered_event_format__c = null;
        c.Prefered_investment_models__c = null;
        c.Preferred_Language__c = null;
        c.Prefix_Envelop__c = null;
        c.Product_of_interest_Africa__c = null;
        c.Product_of_interest_Asia__c = null;
        c.Product_of_interest_Europe__c = null;
        c.Product_of_interest_South_America__c = null;
        c.Publish_Address_c__c = null;
        c.Publish_Address__c = false;
        //c.Publish_Address_Date_Time_Stamp__c = null;
        c.Publish_Email__c = false;
        //c.Publish_Email_Date_Time_Stamp__c = null;
        c.Publish_Phone__c = false;
        //c.Publish_Phone_Date_Time_Stamp__c = null;
        c.Publish_Photo__c = false;
        //c.Publish_Photo_Date_Time_Stamp__c = null;
        c.Questionary__c = null;
        c.Random_Info_Insightly__c = null;
        c.Real_Estate__c = false;
        c.RE_Club_Africa_Member__c = false;
        c.Real_Estate_BR_Club__c = false;
        c.Global_Club_Member__c = false;
        c.India_Club_Member__c = false;
        c.RE_Club_Latam_Member__c = false;
        c.Regions_to_invest__c = null;
        c.Relevant_Info_for_projects_partners__c = null;
        c.Relevant_players_to_your_business__c = null;
        c.Renewal_date_last_changed__c = null;
        c.Renewal_due__c = null;
        c.Researcher_Name__c = null;
        c.Researcher_Resposible__c = null;
        c.Residential__c = null;
        c.Responsible_for_the_interview__c = null;
        c.Retail_Club_BR__c = false;
        c.Retail__c = false;
        c.Retreat_Tier_Level__c = null;
        c.Salutation_Line_Rule__c = null;
        c.Secretary_Email_1__c = null;
        c.Secretary_Email_2__c = null;
        c.Secretary_Name_1__c = null;
        c.Secretary_Name_2__c = null;
        c.Secretary_Phone_1__c = null;
        c.Secretary_Phone_2__c = null;
        c.Sector_of_Operation_Infra__c = null;
        c.Sector_of_Operation_Other__c = null;
        c.Sector_of_Operation_RE__c = null;
        c.Sector_s_of_Operation_Retail__c = null;
        c.Sectorial_Marque_New__c = null;
        c.Sectorial_Marquee_approval__c = false;
        c.Sectors_of_focus_of_your_developements__c = null;
        c.Shopping__c = null;
        c.Show_on_Website__c = false;
        //c.Show_on_Website_Date_Time_Stamp__c = null;
        c.Skype_Phone__c = null;
        c.Soft_Consent_Given_to_GDPR__c = null;
        c.Sofr_Consent_Given_to_GDPR__c = null;
        c.Soft_Consent_Given_to_GDPR_Time_Stamp__c = null;
        c.Soft_Consent_Updated_by__c = null;
        c.Solutions_offered_to_industry_players__c = null;
        c.Source_of_investment__c = null;
        c.Special_Contact__c = false;
        c.Specialty_New__c = null;
        c.Speciality_Last_Confirmed__c = null;
        c.Speciality_Last_Update__c = null;
        c.Dyn_Specialty__c = null;
        c.Spex_Category__c = null;
        c.Sponsorship_Discount__c = null;
        c.Sponsorship_Package_Inclusive__c = null;
        c.Sponsorship_Time_Stamp__c = null;
        c.Spouse_Badge_Name_Rule__c = null;
        c.Spouse_Dietary_Preference__c = null;
        c.Spouse_First_name__c = null;
        c.Spouse_Last_Name__c = null;
        c.Spouse_Middle_Name__c = null;
        c.Spouse_Nickname__c = null;
        c.Spouse_Prefix__c = null;
        //c.Store_PII_Data_Elsewhere_Date_Time_Stamp__c = null;
        c.Sub_Saharan__c = false;
        c.Sub_Saharan_Africa_Country_Zone_of_Inter__c = null;
        c.Subscribed_to_emails__c = null;
        c.Subscription_source__c = null;
        c.synety__Synety_ID__c = null;
        c.TAGS__c = null;
        c.TaskEmail__c = null;
        c.Time_Zone__c = null;
        c.Topics_to_be_discussed_in_our_Magazine__c = null;
        c.Total_donated__c = null;
        c.Club_Event_Counter__c = null;
        c.Open_Events_Counter__c = null;
        c.Type_of_participation_in_our_Magazine__c = null;
        c.Updated_by_Researcher__c = false;
        c.Updated_insightly__c = null;
        //c.Uruguay_Infra__c = null;
        //c.Uruguay_RE__c = null;
        //c.USA_Infra__c = null;
        //c.USA_RE__c = null;
        c.VF_Photo_Logo__c = null;
        c.Website__c = null;
        c.Website_2__c = null;
        //c.Website_Login_Name__c = null;
        //c.Website_Password__c = null;
        c.Westem_Nothem_Europe__c = false;
        c.What_is_the_IRR_expected__c = null;
        //c.What_kind_of_projects_do_you_focus_on__c = null;
        c.WhatsApp_Available_New__c = null;
        //c.WhatsApp_Available_Date_Time_Stamp__c = null;
        c.Which_OPEN_events_to_participate__c = null;
        c.Who_are_your_biggest_Clients__c = null;
        //c.Who_could_add_value_to_my_business__c = null;
        //c.Who_hires_you__c = null;
        c.ZIP_Code__c = null;
        c.Zip_Code_Other__c = null;
        c.GDPR_Delete_Complete__c = true;
        c.Consent_Given_to_GDPR__c = 'No';

        c.Status__c = 'Disabled';
        c.Disabled_Reason__c = 'Opted To be Forgotten';

        return c;
    }
 
    global void finish(Database.BatchableContext BC){
        //Delete all contact history
        List<ContactHistory> contactHistoryList = [SELECT Id FROM ContactHistory WHERE ContactId IN :contactHistoryToDelete];
        delete contactHistoryList; 
    }
}