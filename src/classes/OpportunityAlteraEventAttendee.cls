/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe que altera valores de Event Attendee e installments conforme são 
* feitas alterações na oportunidade.
*
* NAME: OpportunityAlteraEventAttendee.cls
* AUTHOR: EAD                                                  DATE: 19/01/2018
*******************************************************************************/

public class OpportunityAlteraEventAttendee {

    public static void execute(){
        TriggerUtils.assertTrigger();
        map<String, Opportunity> mapOpp = new map<String, Opportunity>();
        
        for(Opportunity opp : (list<Opportunity>)Trigger.new){
            if(TriggerUtils.wasChangedTo(opp, Opportunity.Extra_Financial_Status__c, 'Cancel Kept') || 
               TriggerUtils.wasChangedTo(opp, Opportunity.Extra_Financial_Status__c, 'Refunded') ||  
               TriggerUtils.wasChangedTo(opp, Opportunity.Extra_Financial_Status__c, 'Replaced') ||                
               TriggerUtils.wasChangedTo(opp, Opportunity.Finance_Status__c, 'Carry Forward') || 
               TriggerUtils.wasChangedTo(opp, Opportunity.Finance_Status__c, 'Transferred') ||
               TriggerUtils.wasChangedTo(opp, Opportunity.StageName, 'Cancelled'))
            {
            	mapOpp.put(opp.id, opp);  
            }
        }

        //Recuperando Event Attendee and Installments da oportunidade.
        list<Event_Attendee__c> lstEvent = [SELECT Id FROM Event_Attendee__c WHERE Opportunity__c =: mapOpp.keySet()];
        list<Installments__c> lstInstallments = [SELECT Id FROM Installments__c WHERE Opportunity__c =: mapOpp.keySet()];
      
        for(Opportunity opp : mapOpp.values()){            
            
            //Atribuição para o Event Ateendee 
            if(!lstEvent.isEmpty()){
                for(Event_Attendee__c ev : lstEvent){
                    if(opp.Extra_Financial_Status__c == 'Cancel Kept' || opp.Extra_Financial_Status__c == 'Refunded')
                        ev.Event_Attendees_Status__c = 'Not Attending';
                }    
            }
            
            //Atribuição para o Event Ateendee 
            if(!lstEvent.isEmpty()){
                for(Event_Attendee__c ev : lstEvent){
                    if(opp.Extra_Financial_Status__c == 'Replaced')
                        ev.Event_Attendees_Status__c = 'Replaced';
                }    
            }
            
            //Atribuição para o Event Ateendee 
            if(!lstEvent.isEmpty()){
                for(Event_Attendee__c ev : lstEvent){
                    if(opp.Finance_Status__c == 'Carry Forward' || opp.Finance_Status__c == 'Transferred')
                        ev.Event_Attendees_Status__c = 'Not Attending';
                }    
            }
            
            //Atribuição para o Event Ateendee e Installments
            if(!lstEvent.isEmpty()){
                for(Event_Attendee__c ev : lstEvent){
                    if(opp.StageName == 'Cancelled')
                        ev.Event_Attendees_Status__c = 'Cancelled';
                }
                for(Installments__c ins : lstInstallments){
                    if(opp.StageName == 'Cancelled')
                        ins.Installment_Status__c = 'Canceled';
                }
            }
        }
        if(!lstEvent.isEmpty()) Database.update(lstEvent);
        if(!lstInstallments.isEmpty()) Database.update(lstInstallments);        
    }
}