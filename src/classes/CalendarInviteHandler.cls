public with sharing class CalendarInviteHandler{
    private static string googleClientID{get;set;}
    private static string googleSecretCode{get;set;}
    private static string redirectURI{get;set;}
    private static string authorizationCode{get;set;}
    private string state = '';
    private static string accessToken;
    private static string refreshToken;
    private static string expiresIn;
    private static string tokenType;
    public String inviteType{get;set;}
    public String selectedEmailType {get;set;}
    public String otherEmail {get;set;}
    public Id EventId {get;set;}
    private static string tokenEndPointValue{get;set;}
    private static string endPointValue{get;set;}
    private static string updateEventEndpoint;
    private static string defaultGoogleCalendarId;
    private static string batchEndpoint{get;set;}
    @future(Callout=true)
    public static void CreateInviteInGoolgeCalendar(set<Id> calendarInviteIds, string operation,Map<Id,Boolean> calInviteToEmailMap){
        List<Calendar_Invite__c> calendarInvitesLst = [SELECT id, Name,Alert_Type__c, Calendar_Invite_Time__c, Conferencing__c, Description__c, End_Date__c,
        CalendarInviteOwnerEmail__c, Finish_Hour__c, Finish_Minute__c, Invite_Others__c, Invite_Type__c,Timezone__c, Modify_Event__c, See_Guest_List__c,
        Send_Notification_Time__c,Venue__r.name,Venue_Address__c, Event__r.Timezone__c, EventTimeZone__c, Send_Notification__c, Calendar_Entry_Name__c, Show_As__c, Start_Date__c, Start_Hour__c, Start_Minute__c, Visibility__c, Event__c, Event__r.name,GoogleCalendarEventId__c,GoogleCalendarId__c 
        FROM Calendar_Invite__c WHERE ID IN: calendarInviteIds AND IsInviteesLimitReached__c = false];
        
        List<Google_Calendar_API_Detail__mdt> mtdCalendarApiLst =  [ SELECT GoogleClientId__c,  GoogleCalendarId__c, GoogleBatchEndpoint__c, GoogleEndPoint__c,GoogleAttendeeEndPoint__c,GoogleRedirectURI__c,GoogleSecretCode__c,GoogleTokenEndpoint__c, GoogleUserCode__c FROM Google_Calendar_API_Detail__mdt ]; 
        system.debug('@@mtdCalendarApiLst'+mtdCalendarApiLst);
        if(mtdCalendarApiLst != null && mtdCalendarApiLst.size()>0){
            googleClientID = mtdCalendarApiLst[0].GoogleClientId__c;
            googleSecretCode = mtdCalendarApiLst[0].GoogleSecretCode__c;
            redirectURI = mtdCalendarApiLst[0].GoogleRedirectURI__c;
            authorizationCode = mtdCalendarApiLst[0].GoogleUserCode__c;
            tokenEndPointValue = mtdCalendarApiLst[0].GoogleTokenEndpoint__c;
            endPointValue = mtdCalendarApiLst[0].GoogleEndPoint__c;
            updateEventEndpoint = mtdCalendarApiLst[0].GoogleAttendeeEndPoint__c;
            defaultGoogleCalendarId = mtdCalendarApiLst[0].GoogleCalendarId__c;
            batchEndpoint = mtdCalendarApiLst[0].GoogleBatchEndpoint__c;
        }
        retrieveGoogleAccessToken(calendarInvitesLst,calendarInviteIds,calInviteToEmailMap);
    }
    
    @future(Callout=true)
    public static void DeleteCalendarEvent(set<string> calendarInviteIds,string operation){
        
        List<Google_Calendar_API_Detail__mdt> mtdCalendarApiLst =  [ SELECT GoogleClientId__c,  GoogleCalendarId__c, GoogleEndPoint__c,GoogleAttendeeEndPoint__c,GoogleRedirectURI__c,GoogleSecretCode__c,GoogleTokenEndpoint__c, GoogleUserCode__c FROM Google_Calendar_API_Detail__mdt ]; 
        system.debug('@@mtdCalendarApiLst'+mtdCalendarApiLst);
        if(mtdCalendarApiLst != null && mtdCalendarApiLst.size()>0){
            googleClientID = mtdCalendarApiLst[0].GoogleClientId__c;
            googleSecretCode = mtdCalendarApiLst[0].GoogleSecretCode__c;
            redirectURI = mtdCalendarApiLst[0].GoogleRedirectURI__c;
            authorizationCode = mtdCalendarApiLst[0].GoogleUserCode__c;
            tokenEndPointValue = mtdCalendarApiLst[0].GoogleTokenEndpoint__c;
            endPointValue = mtdCalendarApiLst[0].GoogleEndPoint__c;
            updateEventEndpoint = mtdCalendarApiLst[0].GoogleAttendeeEndPoint__c;
            defaultGoogleCalendarId = mtdCalendarApiLst[0].GoogleCalendarId__c;
        }
    
        getAccessTokenOnDelete(calendarInviteIds);
    }
    private static void getAccessTokenOnDelete(set<string> calendarInviteIds){
        string bodyRequest = '';
        
        system.debug('@@authorizationCode'+authorizationCode);       
        Http h = new Http();
        HttpRequest req = new HttpRequest();
          
        req.setEndpoint(tokenEndPointValue);

        bodyRequest = 'refresh_token=' + EncodingUtil.urlEncode(authorizationCode, 'UTF-8');
        
        bodyRequest += '&client_id=' + EncodingUtil.urlEncode(googleClientID, 'UTF-8');
        bodyRequest += '&client_secret=' + EncodingUtil.urlEncode(googleSecretCode, 'UTF-8');
        bodyRequest += '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8');
        
        bodyRequest += '&grant_type=refresh_token';
        req.setBody(bodyRequest);     
        req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setTimeout(10000);
        HttpResponse res = h.send(req);   
        map<string, string> jsonValues = new map<string, string>();
        
        System.debug('Response Value:'+res.getBody());
        jsonValues = parseJSONToMap(res.getBody());
        if(jsonValues.containsKey('error')){ 
        
        }else{
            //Try to get a cell value in the Google Spreadsheet
            accessToken = jsonValues.get('access_token');
            refreshToken = jsonValues.get('refresh_token');
            expiresIn = jsonValues.get('expires_in');
            tokenType = jsonValues.get('token_type');         
        }
        if(accessToken <> ''){ 
             CalendarDeleteOperation(accessToken,calendarInviteIds); 
        } 
    }
    private static void CalendarDeleteOperation(string actkn,set<string> calendarInviteIds){
        
        for(string idVal:calendarInviteIds){
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            
            req.setEndpoint(updateEventEndpoint+defaultGoogleCalendarId+'/events/'+idVal); 

            req.setHeader('Authorization', 'Bearer ' + actkn);
            
            req.setHeader('Content-Type', 'application/json; charset=UTF-8');
            
            req.setMethod('DELETE');
            req.setTimeout(10000);
            HttpResponse res = h.send(req); 
            System.debug(res.getBody());
            
            Map<string,string> Eventinsertresponse= new Map<string,string>();
            
            Eventinsertresponse = parseJSONToMap(res.getBody());
         
            system.debug('###########'+Eventinsertresponse);
        }
    }
    public static void retrieveGoogleAccessToken(List<Calendar_Invite__c> calndrInviteLst,set<Id> calendarInviteIds,Map<Id,Boolean> calInviteToEmailMap ){
        
        string bodyRequest = '';
        
        system.debug('@@authorizationCode'+authorizationCode);       
        Http h = new Http();
        HttpRequest req = new HttpRequest();
          
        req.setEndpoint(tokenEndPointValue);

        bodyRequest = 'refresh_token=' + EncodingUtil.urlEncode(authorizationCode, 'UTF-8');
        
        bodyRequest += '&client_id=' + EncodingUtil.urlEncode(googleClientID, 'UTF-8');
        bodyRequest += '&client_secret=' + EncodingUtil.urlEncode(googleSecretCode, 'UTF-8');
        bodyRequest += '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8');
        
        bodyRequest += '&grant_type=refresh_token';
        req.setBody(bodyRequest);     
        req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setTimeout(10000);
        
        if(!Test.isRunningTest()){
                        
            HttpResponse res = h.send(req);   
            map<string, string> jsonValues = new map<string, string>();
          
            System.debug('Response Value:'+res.getBody());
            jsonValues = CalendarInviteHandler.parseJSONToMap(res.getBody());
            if(jsonValues.containsKey('error')){ 
            }else{
               //Try to get a cell value in the Google Spreadsheet
            accessToken = jsonValues.get('access_token');
            refreshToken = jsonValues.get('refresh_token');
            expiresIn = jsonValues.get('expires_in');
            tokenType = jsonValues.get('token_type');            
            }
        }else{
            accessToken = 'as23as32ease23asd';
        }
        
        
        if(accessToken <> ''){
             CalendarEventOperation(accessToken,calndrInviteLst,calendarInviteIds,calInviteToEmailMap);
        } 
           
    }

    private static void CalendarEventOperation(string actkn,List<Calendar_Invite__c> calendrInviteLst,set<Id> calendarInviteIds, Map<Id,Boolean> calInviteToEmailMap){
        
        Map<Id,List<Calendar_Invitee__c>> calendarInviteToAttendeeMap =  new Map<Id,List<Calendar_Invitee__c>>();
        
        for(Calendar_Invitee__c calInviteeObj:[SELECT id, InviteeEmail__c,Calendar_Invite__c, Invite_Status__c FROM Calendar_Invitee__c WHERE Calendar_Invite__c IN: calendarInviteIds] ){
            if(calendarInviteToAttendeeMap.containsKey(calInviteeObj.Calendar_Invite__c)){    
                calendarInviteToAttendeeMap.get(calInviteeObj.Calendar_Invite__c).add(calInviteeObj);                        
            }else{
                calendarInviteToAttendeeMap.Put(calInviteeObj.Calendar_Invite__c, new List<Calendar_Invitee__c>{calInviteeObj});     
            }
        }
        
        List<Calendar_Invite__c> calndrInviteRecordLst = new List<Calendar_Invite__c>();
        system.debug('@@calendrInviteLst'+calendrInviteLst);
        for(Calendar_Invite__c calndrInviteRecord:calendrInviteLst){
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            if((calndrInviteRecord.GoogleCalendarEventId__c != null && endPointValue+calndrInviteRecord.GoogleCalendarId__c != null) ){
                if(calInviteToEmailMap.containsKey(calndrInviteRecord.Id)){
                    system.debug('@@calInviteToEmailMap'+calInviteToEmailMap);
                    
                    if(calInviteToEmailMap.get(calndrInviteRecord.Id)==true){
                         req.setEndpoint(updateEventEndpoint+calndrInviteRecord.GoogleCalendarId__c+'/events/'+calndrInviteRecord.GoogleCalendarEventId__c+'?sendNotifications=true&_HttpMethod=PATCH');
                    }else{
                        req.setEndpoint(updateEventEndpoint+calndrInviteRecord.GoogleCalendarId__c+'/events/'+calndrInviteRecord.GoogleCalendarEventId__c+'?_HttpMethod=PATCH');
                    }
                }
            }else{
                req.setEndpoint(updateEventEndpoint+defaultGoogleCalendarId+'/events?sendNotifications=true'); 
            }
            Integer finalHoursToBeAdded = 0;
            
            if(calndrInviteRecord.Timezone__c != null){
                List<string> dateLst;
                List<string> finalHoursLst;
                Boolean IsNegativeValue = false;
                if(calndrInviteRecord.Timezone__c.contains('+')){
                    dateLst =  calndrInviteRecord.Timezone__c.split('\\+');
                
                }else if(calndrInviteRecord.Timezone__c.contains('-')){
                    IsNegativeValue = true;
                    dateLst = calndrInviteRecord.Timezone__c.split('\\-');
                }
                system.debug('@dateLst'+dateLst);
                if(dateLst != null && dateLst.size()>1 ){
                    finalHoursLst = dateLst[1].split('\\:');
                }
                system.debug('@finalHoursLst'+finalHoursLst);
                if(finalHoursLst != null){
                    if(!IsNegativeValue){
                        finalHoursToBeAdded = -1*(Integer.valueOf(finalHoursLst[0]));
                    }else{     
                        finalHoursToBeAdded =  Integer.valueOf(finalHoursLst[0]);
                    }
                }
            }
            system.debug('@@finalHoursToBeAdded'+finalHoursToBeAdded);
            /*if(calndrInviteRecord.Start_Date__c.month() != Date.Today().Month()){
                if(string.valueOf(finalHoursToBeAdded).contains('-')){
                    finalHoursToBeAdded = finalHoursToBeAdded+1;
                }else{
                    finalHoursToBeAdded = finalHoursToBeAdded-1;
                }
            }*/
            system.debug('@@finalHoursToBeAddedFinal'+finalHoursToBeAdded);
            //finalHoursToBeAdded = finalHoursToBeAdded+Integer.ValueOf(Label.TimeZone_Variance);
            system.debug('@@finalHoursToBeAdded242'+finalHoursToBeAdded);
            String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
            list<CalendarInvite_recurrence> clRecurrenceLst= new List<CalendarInvite_recurrence>();
            list<CalendarInvite_attendees> clAttendeesLst=new List<CalendarInvite_attendees>();
            system.debug('@@calndrInviteRecord.Start_Date__c'+calndrInviteRecord.Start_Date__c+'@@calndrInviteRecord.End_Date__c'+calndrInviteRecord.End_Date__c);
            
            //DateTime startDt = datetime.newInstance(calndrInviteRecord.Start_Date__c.year(), calndrInviteRecord.Start_Date__c.month(),calndrInviteRecord.Start_Date__c.day()); 
            String startDateString = (calndrInviteRecord.Start_Date__c).addHours(finalHoursToBeAdded).format(dateFormat);
            System.debug('@@startDateString'+startDateString); // 2016-02-29T22:30:48Z
            
            //DateTime endDt = datetime.newInstance(calndrInviteRecord.End_Date__c.year(), calndrInviteRecord.End_Date__c.month(),calndrInviteRecord.End_Date__c.day());
            String endDateString = calndrInviteRecord.End_Date__c.addHours(finalHoursToBeAdded).format(dateFormat);
            System.debug('@@endDateString'+endDateString); // 2016-02-29T22:30:48Z
            
            system.debug('@@UserInfo.getTimeZone()'+UserInfo.getTimeZone());
            
            CalendarInvite_start clStart = new CalendarInvite_start(startDateString,string.valueof(UserInfo.getTimeZone()));
            CalendarInvite_End clEnd = new CalendarInvite_End(endDateString,string.valueOf(UserInfo.getTimeZone()));
            system.debug('@@clStart'+clStart+'@@clEnd'+clEnd);
            clAttendeesLst.clear();
            if(calndrInviteRecord.GoogleCalendarEventId__c != null && endPointValue+calndrInviteRecord.GoogleCalendarId__c != null){
                //set<string> inviteeEmails = new set<string>();
                if(calInviteToEmailMap.containsKey(calndrInviteRecord.Id)){
                    system.debug('@@calInviteToEmailMap'+calInviteToEmailMap);
                    Map<string,String> inviteeEmails = new Map<String,String>();
                    if(calendarInviteToAttendeeMap.containsKey(calndrInviteRecord.Id)){
                        for(Calendar_Invitee__c calInvitee: calendarInviteToAttendeeMap.get(calndrInviteRecord.Id)){
                            String status = calInvitee.Invite_Status__c == null || calInvitee.Invite_Status__c == 'Pending' ? 'needsAction' : calInvitee.Invite_Status__c == 'Yes'? 'accepted' : calInvitee.Invite_Status__c == 'No' ? 'declined' : 'tentative';
                            inviteeEmails.put(calInvitee.InviteeEmail__c,status);  
                        }
                                
                        if(calInviteToEmailMap.get(calndrInviteRecord.Id)==false){
        
                            for(string strEmail:inviteeEmails.keyset()){
                                clAttendeesLst.add(new CalendarInvite_attendees(strEmail,inviteeEmails.get(strEmail)));
                            }
                            
                        }else{
                            for(string strEmail:inviteeEmails.keyset()){
                                    clAttendeesLst.add(new CalendarInvite_attendees(strEmail,'needsAction'));
                            }
                        }
                    
                     }
                 }
            }
            clAttendeesLst.add(new CalendarInvite_attendees(defaultGoogleCalendarId,'needsAction'));
            
            clRecurrenceLst.add(new CalendarInvite_recurrence('RRULE:FREQ=DAILY;UNTIL=20300930T170000Z'));
            
            system.debug('@@calndrInviteRecord.Invite_Others__c'+calndrInviteRecord.Invite_Others__c+'@@calndrInviteRecord.Modify_Event__c'+calndrInviteRecord.Modify_Event__c+'@@calndrInviteRecord.See_Guest_List__c'+calndrInviteRecord.See_Guest_List__c);
            system.debug('@@calndrInviteRecord.Description__c'+calndrInviteRecord.Description__c);
            
            CalendarInviteWrapper calInviteWrapObj = new CalendarInviteWrapper(calndrInviteRecord.Event__r.name+' - '+ calndrInviteRecord.Invite_Type__c,calndrInviteRecord.Venue__r.name+' , '+calndrInviteRecord.Venue_Address__c,clStart,clEnd, clRecurrenceLst,clAttendeesLst,calndrInviteRecord.Description__c
                                                                                ,calndrInviteRecord.Invite_Others__c, calndrInviteRecord.Modify_Event__c, calndrInviteRecord.See_Guest_List__c);
            
            string bodyRequest = JSON.serialize(calInviteWrapObj);
            bodyRequest = bodyRequest.replace('"enddate":', '"end":');
            bodyRequest = bodyRequest.replace('"cld_dateTime":', '"dateTime":');
            
            System.debug('@@bodyRequest'+bodyRequest);
          
            req.setBody(bodyRequest);     
            
            req.setHeader('Authorization', 'Bearer ' + actkn);
            req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
            req.setHeader('Content-Type', 'application/json; charset=UTF-8');
            if(calndrInviteRecord.GoogleCalendarEventId__c != null && endPointValue+calndrInviteRecord.GoogleCalendarId__c != null ){
                req.setHeader('X-HTTP-Method-Override','PATCH');
                req.setMethod('PUT');
                //req.setHeader('X-HTTP-Method-Override','PATCH');
            }else{
                req.setMethod('POST');
            }
            req.setTimeout(10000);
            HttpResponse res = h.send(req); 
            System.debug(res.getBody());
            
            Map<string,string> Eventinsertresponse= new Map<string,string>();
            
            Eventinsertresponse = parseJSONToMap(res.getBody());
         
            system.debug('###########'+Eventinsertresponse);
            
            if(Eventinsertresponse.containsKey('error')){ 
                system.debug('@@Eventinsertresponse'+Eventinsertresponse);
            }else{
             
                 //Try to get a cell value in the Google Spreadsheet
                 string calendarEventid='';
                 
                 map<string,string> Geteventid = new map<string, string>();   
                 Geteventid = CalendarInviteHandler.parseJSONToMap(res.getBody());
                 if(Geteventid.containsKey('error')){ 
                 }else{
                    //Try to get a cell value in the Google Spreadsheet
                   calendarEventid = Geteventid.get('id');        
                 }
                 system.debug('@@calendarEventid'+calendarEventid);
             
             
                 Http h1 = new Http();
                 HttpRequest req1 = new HttpRequest();
                 
                 req1.setEndpoint('https://www.googleapis.com/calendar/v3/calendars/'+defaultGoogleCalendarId+'/events/watch?sendNotifications=true'); 
                 req1.setMethod('POST');
                 req1.setHeader('Authorization', 'Bearer ' + actkn);
                 req1.setHeader('Content-Type', 'application/json; charset=UTF-8');
                 req1.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
                 req.setTimeout(120000);
                 //Services/apexrest/notifications
                 CalendarEventUpdWrapper calendarUpdjson = new CalendarEventUpdWrapper(calendarEventid,'web_hook','https://eventsync.force.com/services/apexrest/AttendeeSyncService');
                 string bodyRequest1 = JSON.serialize(calendarUpdjson);
                 req1.setBody(bodyRequest1); 
                 HttpResponse res1 = h1.send(req1); 
                 System.debug('@@res1.getBody()'+res1.getBody());
                 String d;
                 String dateStringFormat = 'dd/MM/YYYY\' \'HH:mm\' \'';
                 String endDateFormat = '\' \'HH:mm\' \'';
               
                 if(calndrInviteRecord.Start_Date__c.date().daysBetween(calndrInviteRecord.End_Date__c.date()) == 0){

                    d = calndrInviteRecord.Start_Date__c.format(dateStringFormat) + ' - ' + calndrInviteRecord.End_Date__c.format(endDateFormat);
                 }else {
                    d = calndrInviteRecord.Start_Date__c.format(dateStringFormat) + ' - ' + calndrInviteRecord.End_Date__c.format(dateStringFormat);
                 } 
                 system.debug('@@d'+d);
                
                 if(calendarEventid <> ''){
                     calndrInviteRecord.GoogleCalendarEventId__c= calendarEventid;
                     calndrInviteRecord.GoogleCalendarId__c = defaultGoogleCalendarId;
                     calndrInviteRecord.CalendarDate__c =  d;
                     system.debug('@@calndrInviteRecord.EventTimeZone__c'+calndrInviteRecord.EventTimeZone__c);
                     
                     calndrInviteRecordLst.add(calndrInviteRecord);
                     
                     System.debug('####eventid########'+calendarEventid);
                 }
            }      
        }
        
        RecursionCls.isUpdateLogicNotRequired = true;
        
        system.debug('@@calndrInviteRecordLst'+calndrInviteRecordLst);
        
        if(calndrInviteRecordLst.size()>0)
            update calndrInviteRecordLst;
    }
    public static map<string, string> parseJSONToMap(string JSONValue){
        JSONParser parser = JSON.createParser(JSONValue);
        map<string, string> jsonMap = new map<string, string>();
        string keyValue = '';
        string tempValue = '';
        while (parser.nextToken() != null) {
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME){
                keyValue = parser.getText();
                parser.nextToken();
                tempValue = parser.getText();
                jsonMap.put(keyValue, tempValue);             
            }
        }
        return jsonMap;
    }
    public class CalendarEventUpdWrapper{
        
        public String id;  //Sales Call
        public String type; //Conference Room A
        public string address;
        public string domain;
        public string reason;
        public string message;
        
        public CalendarEventUpdWrapper(string channelId,string channelType,string channelAddress){
            this.id= channelId;
            this.type = channelType;
            this.address = channelAddress;
        }
    }
    public class CalendarInviteWrapper{
    public String summary;  //Sales Call
    public String location; //Conference Room A
    public CalendarInvite_start start;
    public CalendarInvite_end enddate;
    public List<CalendarInvite_recurrence> recurrence;
    public List<CalendarInvite_attendees> attendees;
    public string description;
    
    public boolean guestsCanInviteOthers;
    public boolean guestsCanModify;
    public boolean guestsCanSeeOtherGuests;
    
        public CalendarInviteWrapper(string summaryVal,string locationVal,CalendarInvite_start cld_StartDate,CalendarInvite_end cld_endDate, List<CalendarInvite_recurrence> cld_recurList,List<CalendarInvite_attendees> cld_recurAttList,string calendarDescription,
                                        boolean guestsCanInvite, boolean guestsCanMod, boolean guestsCanSee){
            this.summary = summaryVal;
            this.location = locationVal;
            this.start = cld_StartDate;
            this.endDate = cld_endDate;
            this.recurrence = cld_recurList;
            this.attendees = cld_recurAttList;
            this.description = calendarDescription;
            this.guestsCanInviteOthers = guestsCanInvite;
            this.guestsCanModify = guestsCanMod;
            this.guestsCanSeeOtherGuests = guestsCanSee;
        }
        
    
    }
    public class CalendarInvite_start {
            public String cld_dateTime; //2018-09-29T08:00:00.000-07:00
            public String timeZone; //America/Los_Angeles
            public CalendarInvite_start(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
    public class CalendarInvite_end {
            private String cld_dateTime{get;set;} //2018-09-30T08:30:00.000-07:00
            private String timeZone; //America/Los_Angeles
            private CalendarInvite_end(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
    public class CalendarInvite_recurrence {
            public String recurrence;
            public CalendarInvite_recurrence(string recurVal){
                this.recurrence = recurVal;
            }
        }
    public class CalendarInvite_attendees {
            public String email;    //maddy.crmdev@gmail.com
            public String responseStatus;
            public CalendarInvite_attendees(string organizerEmail,String responseStatus){
                this.email = organizerEmail;
                this.responseStatus = responseStatus;
            }
        }
}