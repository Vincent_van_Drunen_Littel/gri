/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * A test class for TestDataFactory
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */

@isTest
private class AutoProcessMembershipOpps_Test {
    
    private static final Id RECTYPEACC = RecordTypeMemory.getRecType('Account', 'Press');
    private static final id RECTTYPEOPP = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
    private static final Id RECTYPECTRN = RecordTypeMemory.getRecType('Contract', 'Retail_Club'); 

    @testSetup
    static void buildata(){
        Application_Config__c config = new Application_Config__c();
        config.name = 'BucketUserId';
        config.Value__c = UserInfo.getUserId();
        Database.insert(config);
    }
    
    TestMethod static void testAutoApproval(){
        
        Account acc = SObjectInstance.createAccount(RECTYPEACC);         
        Database.insert(acc);

        Contact ctt = SObjectInstance.createContato(acc.id);
        Database.insert(ctt);
        
        Club__c club = SObjectInstance.createClub();
        club.CurrencyIsoCode = 'BRL';
        database.insert(club);
        
        Event__c evt = SObjectInstance.createEvent();
        evt.CurrencyIsoCode = club.CurrencyIsoCode;
        evt.End_Date__c = system.today().addDays(363);
        evt.Start_Date__c = system.today().addDays(2);
        evt.Club__c = club.id;
        Database.insert(evt);
                
        Product2 prd2 = SObjectInstance.createProduct2();
        Database.insert(prd2);
        
        prd2.ProductCode = 'P-000049';
        Database.update(prd2);
        
        ID pb2 = SObjectInstance.catalogoDePrecoPadrao2();
        
        PricebookEntry PBE = SObjectInstance.entradaDePreco(pb2, prd2.id);
        PBE.CurrencyIsoCode = evt.CurrencyIsoCode;
        Database.insert(PBE);
        
        Opportunity opp = SObjectInstance.createOpportunidade2(acc.id, RECTTYPEOPP, ctt.id, evt.id);
        opp.CurrencyIsoCode =  evt.CurrencyIsoCode;
        Database.insert(opp);

        OpportunityLineItem OLI = SObjectInstance.createProdutoOportunidade(opp.id, PBE.id);
        Database.insert(OLI);
        
        Contract ctrn = SObjectInstance.createContract(ctt.id, acc.Id);
        ctrn.RecordTypeId = RECTYPECTRN;
        ctrn.ContractTerm = 12;
        ctrn.Membership_Level__c = HelperSObject.MEMBERSHIP_LV_DIAMOND_MEMBER_INFRA_AND_RE;
        ctrn.Club__c = club.id;
        Database.insert(ctrn);
        
        test.startTest();
          
           opp.StageName = HelperSObject.OPPORTUNITY_STAGE_APPROVED;

           Database.update(opp);        
        test.stopTest();
        
        list<Opportunity> lstOpp = [SELECT bypassValidationRules__c FROM Opportunity WHERE id =: opp.id];
        system.assertEquals(true, lstOpp[0].bypassValidationRules__c);
                
    }
    
    TestMethod static void TestupdateOppDates(){
        
        Account acc = SObjectInstance.createAccount(RECTYPEACC);         
        Database.insert(acc);

        Contact ctt = SObjectInstance.createContato(acc.id);
        Database.insert(ctt);
        
        Opportunity opp = SObjectInstance.createOpportunidade(acc.id, HelperSObject.opportunity_recordTypeIdMembership());
        opp.contact__c =ctt.id;
        Database.insert(opp);
        AutoProcessMembershipOpps.m_updateOppDatesIsActive = false;
        OpportunityNewMember.Exec = true;
        
        test.startTest();
          opp.StageName = HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE;
          Database.update(opp);
        test.stopTest(); 
        
        list<opportunity> oppvld = [SELECT Membership_Announcement_Date__c, Approved_by_Finance_is_Today__c FROM Opportunity WHERE id =: opp.ID];
        
        system.assertEquals(System.today(), oppvld[0].Membership_Announcement_Date__c);
        system.assertEquals(System.today(), oppvld[0].Approved_by_Finance_is_Today__c);
    }
    
    TestMethod static void TestAutoSetWon(){
        
         Account acc = SObjectInstance.createAccount(RECTYPEACC);         
        Database.insert(acc);

        Contact ctt = SObjectInstance.createContato(acc.id);
        Database.insert(ctt);
        
        Club__c club = SObjectInstance.createClub();
        club.CurrencyIsoCode = 'BRL';
        database.insert(club);
        
        Event__c evt = SObjectInstance.createEvent();
        evt.CurrencyIsoCode = club.CurrencyIsoCode;
        evt.Start_Date__c = system.today();
        evt.Club__c = club.id;
        Database.insert(evt);
                        
        Opportunity opp = SObjectInstance.createOpportunidade2(acc.id, RECTTYPEOPP, ctt.id, evt.id);
        opp.CurrencyIsoCode = club.CurrencyIsoCode;
        opp.bypassValidationRules__c = True;       
        Database.insert(opp);

        test.startTest();
        
          opp.StageName = HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE;
          Database.update(opp); 
        
        test.stopTest();   
        
        list<opportunity> oppvld = [SELECT Finance_Status__c, StageName FROM Opportunity WHERE id =: opp.ID];
        
        system.assertEquals('Complimentary', oppvld[0].Finance_Status__c);
        system.assertEquals(HelperSObject.OPPORTUNITY_STAGE_WON, oppvld[0].StageName);

    }
    
}