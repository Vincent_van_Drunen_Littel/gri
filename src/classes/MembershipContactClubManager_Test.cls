@isTest (SeeAllData = true)
public class MembershipContactClubManager_Test {
    // Test Sandbox enviroment
    private static Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
    private static String STAGE_COMMITTEE_REJECTED = 'Rejected';
    private static String STAGE_COMMITTEE_ACCEPTED = 'Approved';
    private static String STAGE_COMMITTEE_PROCESSING = 'Processing';
    private static String STAGE_OPP_APPROVED = 'Approved';
    private static String STAGE_OPP_APPROVED_BY_FINANCE = 'Approved by Finance';
    private static String STAGE_OPP_WON = 'Won';
    private static String MEMBERSHIP_STATUS_CANCELLED = 'Cancelled';
    private static String MEMBERSHIP_STATUS_ACTIVATED = 'Activated';
    // club names
    private static final String CLUB_INFRA_INDIA = 'Infra Club India';
    private static final String CLUB_INFRA_LATAM = 'Infra Club LatAm';
    private static final String CLUB_RE_AFRICA = 'RE Club Africa';
    private static final String CLUB_RE_BRAZIL = 'RE Club Brazil';
    private static final String CLUB_RE_EUROPE = 'RE Club Europe';
    private static final String CLUB_GRI_GLOBAL_CLUB = 'GRI Global Club';
    private static final String CLUB_RE_INDIA = 'RE Club India';
    private static final String CLUB_RE_LATAM = 'RE Club LatAm';
    private static final String CLUB_RETAIL_BRAZIL = 'Retail Club Brazil';
    
    // Prod&pricebook
    private static Product2 s_prod;
    private static PricebookEntry s_pricebook;
    // Account
    private static Account s_acc;
    private static Account s_accLatam;
    private static Account s_accEurope;
    private static Account s_accBrazil;
    private static Account s_accIndia;
    // Contact
    private static Contact s_con;
    private static Contact s_conLatam;
    private static Contact s_conEurope;
    private static Contact s_conBrazil;
    private static Contact s_conIndia;
    
    // Clubs
    private static Club__c s_club;
    private static Club__c s_clubInfraLatam;
    private static Club__c s_clubInfraIndia;
    private static Club__c s_clubREEurope;
    private static Club__c s_clubREBrazil;
    private static Club__c s_clubREIndia;
    private static List<Club__c> clubList = new List<Club__c>();
    
    static{
        s_acc = InsertSObject.createAccount('GRI test');
        s_con = InsertSObject.createContact(s_acc.Id, 'Abacate', 'Banana');
        s_club = NewInstanceSObject.createClub();
        s_clubInfraLatam = NewInstanceSObject.createClub();
        s_clubInfraIndia = NewInstanceSObject.createClub();
        s_clubREEurope = NewInstanceSObject.createClub();
        s_clubREBrazil = NewInstanceSObject.createClub();
        s_clubREIndia = NewInstanceSObject.createClub();
        s_club.Name = CLUB_RE_BRAZIL;
        s_clubInfraLatam.Name = CLUB_INFRA_LATAM;
        s_clubInfraIndia.Name = CLUB_INFRA_INDIA;
        s_clubREEurope.Name = CLUB_RE_EUROPE;
        s_clubREBrazil.Name = CLUB_RE_BRAZIL;
        s_clubREIndia.Name = CLUB_RE_INDIA;        
        clubList.add(s_club);
        clubList.add(s_clubInfraLatam);
        clubList.add(s_clubInfraIndia);
        clubList.add(s_clubREEurope);
        clubList.add(s_clubREBrazil);
        clubList.add(s_clubREIndia);
        insert clubList;
        s_prod = NewInstanceSObject.createProduct();
        //lProduct2 = SObjectInstance.createProduct2();
        database.insert(s_prod);
        // Criação do PricebookEntry
        Id pbkId =  NewInstanceSObject.getStandardPricebookId();
        s_pricebook = NewInstanceSObject.pricebookEntry(pbkId,s_prod.Id);
        database.insert(s_pricebook);
    }
    
    @isTest private static void test_setContactStatus()
    {
        s_con = MembershipContactClubManager.setContactStatus(CLUB_RE_BRAZIL, s_con, True);
        update s_con;
        System.assertEquals(True, s_con.Real_Estate_BR_Club__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.Real_Estate_BR_Club__c='+s_con.Real_Estate_BR_Club__c);
        
        s_con = MembershipContactClubManager.setContactStatus(CLUB_RE_BRAZIL, s_con, False);
        update s_con;
        System.assertEquals(False, s_con.Real_Estate_BR_Club__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.Real_Estate_BR_Club__c='+s_con.Real_Estate_BR_Club__c);

        s_con = MembershipContactClubManager.setContactStatus(CLUB_RE_EUROPE , s_con, True);
        update s_con;
        System.assertEquals(True, s_con.Global_Club_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.Global_Club_Member__c='+s_con.Global_Club_Member__c);        
        
        s_con = MembershipContactClubManager.setContactStatus(CLUB_RE_INDIA, s_con, True);
        update s_con;
        System.assertEquals(True, s_con.India_Club_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.India_Club_Member__c='+s_con.India_Club_Member__c);               
        
        s_con = MembershipContactClubManager.setContactStatus(CLUB_INFRA_INDIA , s_con, True);
        update s_con;
        System.assertEquals(True, s_con.Infra_Club_India_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.Infra_Club_India_Member__c='+s_con.Infra_Club_India_Member__c);            
        
        s_con = MembershipContactClubManager.setContactStatus(CLUB_INFRA_LATAM , s_con, True);
        update s_con;
        System.assertEquals(True, s_con.Infrastructure_BR_Club__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.Infrastructure_BR_Club__c='+s_con.Infrastructure_BR_Club__c);           
        
        s_con = MembershipContactClubManager.setContactStatus(CLUB_RE_LATAM , s_con, True);
        update s_con;
        System.assertEquals(True, s_con.RE_Club_Latam_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.RE_Club_Latam_Member__c='+s_con.RE_Club_Latam_Member__c);           
                
        s_con = MembershipContactClubManager.setContactStatus(CLUB_RETAIL_BRAZIL , s_con, True);
        update s_con;
        System.assertEquals(True, s_con.Retail_Club_BR__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.Retail_Club_BR__c='+s_con.Retail_Club_BR__c);             
        
        s_con = MembershipContactClubManager.setContactStatus(CLUB_GRI_GLOBAL_CLUB , s_con, True);
        update s_con;
        System.assertEquals(True, s_con.Retail_Club_BR__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.Global_Club_Member__c='+s_con.Global_Club_Member__c);
        
        s_con = MembershipContactClubManager.setContactStatus(CLUB_RE_AFRICA , s_con, True);
        update s_con;
        System.assertEquals(True, s_con.RE_Club_Africa_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           's_con.RE_Club_Africa_Member__c='+s_con.RE_Club_Africa_Member__c);   
      
    }
    
    @isTest private static void test_updateContactsFields()
    {
        Date closeDate = System.today();
        Opportunity opp_reBr =  NewInstanceSObject.createOpportunityMembership(s_acc, 
                                                                               s_con, 
                                                                               s_clubREBrazil, 
                                                                               closeDate);
        insert opp_reBr;
        opp_reBr  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:opp_reBr.Id];
        opp_reBr.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED ;
        opp_reBr.StageName = STAGE_OPP_APPROVED_BY_FINANCE ;
        update opp_reBr;
        
        list<Contract> membershipAfter = [SELECT Id,Name,Opportunity__c,Status 
                                         FROM Contract 
                                         WHERE Opportunity__c=:opp_reBr.Id];
        System.debug('%% [Debug] membershipAfter='+membershipAfter);
        membershipAfter[0].Status = MEMBERSHIP_STATUS_ACTIVATED;
        update membershipAfter;
        s_con = [SELECT Id, Real_Estate_BR_Club__c
                FROM Contact
                WHERE Id=:s_con.Id];
        //System.assertEquals(True, s_con.Real_Estate_BR_Club__c, '1 Contact Membership status wasnt rightly updated. s_con='
        //                    +s_con);
        membershipAfter[0].Status = MEMBERSHIP_STATUS_CANCELLED ;
        update membershipAfter;
        s_con = [SELECT Id, Real_Estate_BR_Club__c
                FROM Contact
                WHERE Id=:s_con.Id];
        //System.assertEquals(False, s_con.Real_Estate_BR_Club__c, '2 Contact Membership status wasnt rightly updated. s_con='
        //                    +s_con);
    }
    
}