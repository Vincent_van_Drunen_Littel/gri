/**
 * This class works as a struct to hold the Opportunity and membership from a contract.
 *  
 */
public class ContactOppMembership {
    private Id m_ContactId;
	private Opportunity m_Opportunity;
    private Contract m_Membership;
    private String m_strClub;
    
    public ContactOppMembership(Id ccId, Opportunity opp, Contract memb, String strClubName)
    {
		m_ContactId = ccId;
        m_Opportunity  = opp;  
        m_Membership = memb;
        m_strClub = strClubName;
    }
    
    public Id getContact()
    {
        return m_ContactId;
    }
    
    public Opportunity getOpportunity()
    {
        return m_Opportunity;
    }
    
    public Contract getContract()
    {
        return m_Membership;
    }
    
    public String getClubName()
    {
        return m_strClub;
    }
}