/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste responsável por testar a classe OpportunityCriaEventAttendeeTest
*
* NAME: OpportunityCriaEventAttendeeTest.cls
* AUTHOR: LMdO                                                  DATE: 02/06/2016
*******************************************************************************/
@isTest
private class OpportunityCriaEventAttendeeTest
{

	private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
	private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
	private static Integer COUNT = 49;

	static testMethod void ProductSeatchUpdateA()
    {
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = new Product2();
        produto.NAME = 'Teste prod';
        produto.ProductCode='P-000798';

        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        produto = new Product2();
        produto.NAME = 'Teste prod2';
        produto.ProductCode='P-001319';

        Database.insert(produto);

        entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);

        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(25);
        update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Negotiation';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        Test.startTest();
        oportunidade.StageName = 'Closing';
        Database.update(oportunidade);
        Test.stopTest();
    }

    static testMethod void testSucessoInsert()
    {
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = SObjectInstance.createProduct2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);

        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(25);
        update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Negotiation';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        Test.startTest();
        oportunidade.StageName = 'Approved by Finance';
        Database.update(oportunidade);
        Test.stopTest();
    }

    static testMethod void testSucessoDelete()
    {
        Profile perfil = SObjectInstance.perfil('System Administrator');
        User usuario = SObjectInstance.usuario(perfil.id);

        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = new Product2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);

        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(25);
        update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
        oportunidade.Contact__c = contato.Id;
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        oportunidade.StageName = 'Approved by Finance';
        Database.update(oportunidade);

        Test.startTest();
        System.runAs(usuario)
        {
         oportunidade.StageName = 'Negotiation';
         Database.update(oportunidade);
        }
        Test.stopTest();
    }

    static testMethod void testErrorInsert()
    {
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = new Product2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);

        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(25);
        update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Negotiation';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        Test.startTest();
        oportunidade.StageName = 'Closing';
        Database.update(oportunidade);
        Test.stopTest();
    }

    static testMethod void testErrorDelete()
    {
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = new Product2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
        evento.Type__c = 'Open Event';
        Database.insert(evento);

        evento.Start_Date__c = system.today().addDays(22);
        evento.End_Date__c = system.today().addDays(25);
        update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Negotiation';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        oportunidade.StageName = 'Approved by Finance';
        Database.update(oportunidade);

        Test.startTest();
        oportunidade.StageName = 'Negotiation';
        Database.update(oportunidade);
        Test.stopTest();
    }

    @testSetup
    static void testInit()
    {
        TestDataFactory.init();
    } 

/*
	static testMethod void testSucessoLote()
	{
		list<Account> lstConta = new list<Account>();
		list<Contact> lstContato = new list<Contact>();
		list<Opportunity> lstOpp = new list<Opportunity>();
		list<OpportunityLineItem> lstOppLine = new list<OpportunityLineItem>();

		Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

		Product2 produto = new Product2();
		produto.NAME = 'Teste prod';
		Database.insert(produto);

		PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
		Database.insert(entradaDePreco);

		for(Integer i = 0; i < COUNT; i++)
		{
			Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
			conta.Name = 'Acc Teste' + i;
			lstConta.add(conta);
		}
		Database.insert(lstConta);

		for(Integer i = 0; i < COUNT; i++)
		{
			Contact contato = SObjectInstance.createContato(lstConta[i].Id);
			contato.LastName = 'Teste Dev' + i;
			lstContato.add(contato);
		}
		Database.insert(lstContato);

		list<Event__c> lstEvent = new list<Event__c>();
		for(Integer i = 0; i < COUNT; i++)
		{
			Event__c evento = new Event__c();
	        evento.Name = 'Teste Cloud2b Event' + i;
	        evento.Start_Date__c = system.today().addDays(22);
	        evento.End_Date__c = system.today().addDays(25);
	        evento.Status__c = 'Confirmed';
	        evento.Type__c = 'Open Event';
	        lstEvent.add(evento);
		}
		Database.insert(lstEvent);


		for(Integer i = 0; i < COUNT; i++)
		{
			Opportunity oportunidade = SObjectInstance.createOpportunidade(lstConta[i].Id, ID_EVENT);
			oportunidade.Contact__c = lstContato[i].Id;
			oportunidade.StageName = 'Negotiation';
			oportunidade.Event__c = lstEvent[i].Id;
			lstOpp.add(oportunidade);

			system.debug('>>Victor oportunidade.Contact__c: '+ oportunidade.Contact__c);
			system.debug('>>Victor oportunidade.Event__c: '+ oportunidade.Event__c);
		}
		Database.insert(lstOpp);

		for(Integer i = 0; i < COUNT; i++)
		{
			OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(lstOpp[i].Id, entradaDePreco.Id);
			lstOppLine.add(produtoOpp);
		}
		Database.insert(lstOppLine);

		Test.startTest();
		for(Opportunity opp : lstOpp)
		{
			opp.StageName = 'Approved by Finance';
		}
		Database.update(lstOpp);
		
		Test.stopTest();
	}
    */
}