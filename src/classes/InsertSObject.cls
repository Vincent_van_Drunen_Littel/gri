public class InsertSObject {
    public static Account createAccount()
    {
        Account acc = new Account(
            Name = 'Test Account'
        );
            database.upsert(acc);
        
        return acc;
    }
    public static List<Account> createAccount(Integer n)
    {
        List<Account> listacc = new List<Account>();
        for(Integer i = 0; i < n; i++)
        {
            listacc.add(new Account(Name = 'Test Account n.'+i ));
        }
            database.upsert(listacc);
        return listacc;
    }
    
    public static Account createAccount(String AccName)
    {
        Account acc = new Account(
            Name = AccName
        );

        database.upsert(acc);
        return acc;
    }
    public static List<Account> createAccount(Integer n, String AccName)
    {
        List<Account> listacc = new List<Account>();
        for(Integer i = 0; i < n; i++)
        {
            //String accNameStr = 
            listacc.add(new Account(Name = AccName+' n.'+i ));
        }

        database.upsert(listacc);

        return listacc;
    }    
    
    public static Club__c createClub()
    {
        Club__c theclub = new Club__c(
            Name = 'Test Club'
        );
        database.upsert(theclub);

        return theclub;        

    }
    public static Club__c createClub(String clubname)
    {
        Club__c theclub = new Club__c(
            Name = clubname
        );
        database.upsert(theclub);

        return theclub;    
    }
    
    public static Contact createContact(Id AccountId) 
    {
        Contact ccc = new Contact(
            AccountId = AccountId,
            FirstName = 'Teste First',
            LastName = 'Teste Last' 
        );
        database.upsert(ccc);

        return ccc;
    }
    public static Contact createContact(Id AccountId, String FirstName, String LastName) 
    {
        Contact ccc = new Contact(
            AccountId = AccountId,
            FirstName = FirstName,
            LastName = LastName
        );

        database.upsert(ccc);

        return ccc;
    }
    public static List<Contact> createContact(Integer n, Id theAccountId, String theFirstName, String theLastName)
    {
        List<Contact> listccc = new List<Contact>();
        for(Integer i = 0; i < n; i++)
        {
            //String accNameStr = 
            listccc.add(new Contact(AccountId=theAccountId, FirstName=theFirstName+' n.'+i, LastName=theLastName+' n.'+i));
        }
        database.upsert(listccc);
            
        return listccc;
    }       
    public static Opportunity createOpportunityMembership(Account theAccount,  
                                                          Contact theContact, Club__c club, 
                                                          Date closeDate)
    {
        Opportunity opp =  new Opportunity(
            RecordTypeId = RecordTypeMemory.getRecType('Opportunity', 'Membership'),
            Name = 'Opp de Teste -- ' + theContact.Name,
            StageName = 'Prospect',
            AccountId = theAccount.Id,
            Club__c = club.Id,
            Contact__c = theContact.Id,
            CloseDate = closeDate,
            Type = 'Venda'
        );
        database.upsert(opp);
        

        return opp;
    }
 
    public static Product2 createProduct()
    {
        Product2 ppp = new Product2(
            Name = 'Productc Test',
            ProductCode = '123a'
        );

        database.upsert(ppp);


        return ppp;
    }
    public static id getStandardPricebookId() 
    {
        return test.getStandardPricebookId();
    }
    public static PricebookEntry pricebookEntry(ID aCatalogo, ID aProduto ) 
    {
        PricebookEntry pbe = new PricebookEntry(
            Pricebook2Id = aCatalogo,
            Product2Id = aProduto,
            IsActive = true,
            UnitPrice = 20.00 );

        database.upsert(pbe);


        return pbe;
    }
}