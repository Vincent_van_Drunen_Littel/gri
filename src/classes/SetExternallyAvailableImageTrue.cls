/*******************************************************************************
*                               Werise - 2019
*-------------------------------------------------------------------------------
*
* Classe que altera o campo "IsPublic" para "true " nos registros
* da pasta "MarketingEditedFiles" no Objeto "Document".
*
* NAME: SetExternallyAvailableImageTrue
* AUTHOR: Jefferson F. Presotto                                DATE: 28/03/2019
*******************************************************************************/

global class SetExternallyAvailableImageTrue implements Database.Batchable<sObject>, 
Database.Stateful {

    //Contador do numero de registros processados e Id do FolderId
    global Integer recordsProcessed = 0;
    private final Id MarketingEditedFiles = '00l36000000j90F';

    //Metodo que faz a consulta aos dados do lote
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id, Name FROM Document WHERE FolderId =: MarketingEditedFiles AND IsPublic = false');
    }
    
    //Metodo que executa o lote, cada registro e colocado no loop e atualizado o campo IsPublic = true, 
	//tambem conta o numero de registros processados
    global void execute(Database.BatchableContext bc, List<Document> records){
        List<Document> documents = new List<Document>();
        for (Document dcm : records) {
            dcm.IsPublic = true;
            documents.add(dcm);
            recordsProcessed = recordsProcessed + 1;
            
        }
        update documents;
    }    
    
    //Metodo que traz o numero de registro atualizados e os dados do processo
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed.@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
                            JobItemsProcessed,
                            TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob
                            WHERE Id = :bc.getJobId()];
        System.debug(job);
    }    
}