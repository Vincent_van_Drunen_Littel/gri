/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * RecordType helper class
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * @testClasses HelperRecordType_Test
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */
public with sharing class HelperRecordType {

	private static Map< String, Map< String, Id > > recordTypeMap;


	public static Id get(String pSObject, String pRecerdTypeName)
	{
		try
		{
			Id recTypeId = Schema.getGlobalDescribe().get(pSObject).newSObject().getSObjectType().getDescribe().getRecordTypeInfosByName().get(pRecerdTypeName).getRecordTypeId();
			return recTypeId;
		}
		catch(NullPointerException e)
		{
			throw new HelperRecordTypeException(
				'Record type Id retuned a NULL value. Please review the input parameters: '+
				'pSObject='+pSObject+', pRecerdTypeName='+pRecerdTypeName);
		}		
	}

/*
    public static Id getRecordTypeId(String sObjectType, String developerName) 
    {
        if(String.isEmpty(SObjectType) || String.isEmpty(DeveloperName))
            throw new HelperRecordTypeException('You must provide valid SObject type and developer name arguments.');
            
        if(recordTypeMap == null) 
        {
            recordTypeMap = new Map< String, Map< String, Id > >();
            for(RecordType recType : [select Id, SObjectType, DeveloperName from RecordType]) 
            {
                Map< String, Id > SObjectMap = recordTypeMap.get(recType.SObjectType);
                if(SObjectMap == null) 
                {
                    SObjectMap = new Map< String, Id > {recType.DeveloperName => recType.Id};
                    recordTypeMap.put(recType.SObjectType, SObjectMap);
                } 
                else 
                	SObjectMap.put(recType.DeveloperName, recType.Id);
            }
        }
        
        Map< String, Id > SObjectMap = recordTypeMap.get(SObjectType);
        if(SObjectMap == null) throw new HelperRecordTypeException('Invalid SObject type.');
        
        Id recTypeId = SObjectMap.get(DeveloperName);
        if(recTypeId == null) throw new HelperRecordTypeException('Invalid developer name "' + DeveloperName + '" for SObject type "' + SObjectType + '".');
        
        return recTypeId;
    }

    public static String getRecordTypeDeveloperName(Id idRecordType)
    {
        String ret = '';
        if(String.isNotEmpty(idRecordType))
        {
            for(RecordType rt : [select DeveloperName from RecordType where id=:idRecordType])
            {
                ret = rt.DeveloperName;
            }
        }
        return ret;
    }
    
    public static List<RecordType> getRecordType (String sObjectType, String developerName)
    {
        String url = 'select Id, SObjectType, DeveloperName, Name from RecordType where SObjectType = \''+SObjectType+'\' and DeveloperName = \''+developerName+'\' order by  Name limit 300 ';
        return Database.query(url);
    }
    
    public static List<RecordType> getRecordType (String sObjectType)
    {
        return [ select Id, SObjectType, DeveloperName, Name from RecordType where SObjectType =:SObjectType ];
    }
    
    public static List<RecordType> getRecordType(Id idRecordType)
    {
        String url = 'select Id, SObjectType, DeveloperName, Name from RecordType where Id = \''+idRecordType+'\'';
        return Database.query(url);
    }
    
    public static Map<Id,RecordType> getMapRecordType(String sObjectT)
    {
        String url = 'select Id, SObjectType, DeveloperName, Name from RecordType where SObjectType = \''+sObjectT+'\' ';
        List<RecordType> lsRec =Database.query(url);
        Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>(lsRec);
        return mapRecordType;
    }
*/ 

	public class HelperRecordTypeException extends Exception {} 

	
}