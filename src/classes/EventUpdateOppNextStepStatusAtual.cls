/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
*  1) atualização dos campos Next_Step e Date_of_the_next_step__c 
*     da oportunidade com dados de compromisso quando o flag Upgrade_to_Next_Step__c tá ligado; 
*  2) atualização dos campos Actual_Status__c e Date_of_Actual_Status__c 
*     da oportunidade com dados de compromisso quando o flag Upgrade_to_Actual_Status__c tá ligado; 
*
* NAME: EventUpdateOppNextStepStatusAtual.cls
* AUTHOR: VMDL                                                 DATE: 15/08/2016
*******************************************************************************/
public with sharing class EventUpdateOppNextStepStatusAtual {
	
	public static void execute()
	{
		Set< Id > lOppIds = new Set< Id >();
		List< Event > lEventList = new List< Event >();
		boolean lHasError = false;

		for ( Event lEvent : (list<Event>) trigger.new)
		{
			if ( lEvent.WhatId != null && ( lEvent.Upgrade_to_Next_Step__c || lEvent.Upgrade_to_Actual_Status__c ))
			{
				lOppIds.add( lEvent.WhatId );
				lEventList.add( lEvent );
			}
		}

		if ( lOppIds.isEmpty() ) return;

		Map< Id, Opportunity > lOppMap = new Map< Id, Opportunity >();
		List< Opportunity > lOppList = [ Select id, Date_of_the_next_step__c , Next_Step__c, Date_of_Actual_Status__c,
		Actual_Status__c from Opportunity where id =: lOppIds ];

		for( Opportunity lOpp : lOppList ) lOppMap.put( lOpp.id, lOpp );
		
		lOppList.clear();

		for ( Event lEvent : (list<Event>) trigger.new)
		{
			Opportunity lOpp = (Opportunity) lOppMap.get( lEvent.WhatId );
			if ( lOpp != null )
			{
				if ( lEvent.Upgrade_to_Next_Step__c )
				{
					lOpp.Date_of_the_next_step__c  = lEvent.ActivityDateTime.date();
					if ( lEvent.Description != null )
					if ( lEvent.Description.length() <= 255 )
					lOpp.Next_Step__c = lEvent.Description;
					else
					lOpp.Next_Step__c = lEvent.Description.substring( 0, 255 );
				}
				if ( lEvent.Upgrade_to_Actual_Status__c )
				{
					lOpp.Date_of_Actual_Status__c = lEvent.ActivityDateTime.date();
					if ( lEvent.Description != null )
					if ( lEvent.Description.length() <= 255 )
					lOpp.Actual_Status__c = lEvent.Description;
					else
					lOpp.Actual_Status__c = lEvent.Description.substring( 0, 255 );
				}
				lOppList.add( lOpp );
			}
			else
			{
				if ( lEvent.Upgrade_to_Next_Step__c )
				{
					lEvent.Upgrade_to_Next_Step__c.addError( 'You can only upgrade if the event is related with opportunity' );
					lHasError = true;
				}
				if ( lEvent.Upgrade_to_Actual_Status__c )
				{
					lEvent.Upgrade_to_Actual_Status__c.addError( 'You can only upgrade if the event is related with opportunity' );
					lHasError = true;
				}
			}
		}

		if ( lHasError ) return;

		if (!lOppList.isEmpty())
		update lOppList;

		if (!lEventList.isEmpty())
		{
			for ( Event lEvent : lEventList )
			{
				lEvent.Upgrade_to_Next_Step__c = false;
				lEvent.Upgrade_to_Actual_Status__c = false;
			}
		}		
	}
}