/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsável por sumarizar campos no objeto Evento.
*
* NAME: EventSumOrderAndOpp.cls
* AUTHOR: VMDL                                                 DATE: 02/08/2016
*******************************************************************************/
public with sharing class EventSumOrderAndOpp {
 public EventSumOrderAndOpp (){
    }

    //private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
    //private static Id REC_OPP_SPEX = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
    //private static Id REC_ORDER_ADDPASSES = RecordTypeMemory.getRecType( 'Order', 'Aditional_Passes' );
   //private static Id REC_ORDER_EVENTSPONSOR = RecordTypeMemory.getRecType( 'Order', 'Event_Sponsor' );

    //public static Boolean isExecuting = false;

    //public static Double getConversionType()
  //  {   
        //Double conversionUSD;
        
        //CurrencyType iCon = [SELECT IsoCode, ConversionRate 
         //        FROM CurrencyType
         //        Where IsoCode =: 'USD'];
        
       // conversionUSD = iCon.ConversionRate;

      //  return conversionUSD;
//  }
//
//  public static Map<Id, Event__c> sumOppField(Set<String> aEvent, Set<String> aStatus)
//  {
//      system.debug('@@@@@sumOppField@@@@@');
//      
//      Double conversionUSD = getConversionType();
//      if(conversionUSD == null) conversionUSD = 1;
//
//      Map<Id,Event__c> mapEventToUpdate = new Map<Id,Event__c>();
//
//      for(AggregateResult iEvent : [Select Event__c, SUM(Amount) total
//                                          FROM Opportunity
//                                          Where Event__c =: aEvent
//                                          And StageName =: aStatus
//                                          And RecordTypeId =: REC_OPP_OPEN_EVENTS
//                                          Group By Event__c])
//      {
//          Event__c lEvent = new Event__c(Id = (Id)iEvent.get('Event__c')); 
//          lEvent.Total_of_Opportunity__c = (Decimal) iEvent.get('total') * conversionUSD;
//          mapEventToUpdate.put(lEvent.Id, lEvent);
//      }   
//      return mapEventToUpdate;
//  }
//
//  public static Map<Id, Event__c> sumOrderField(Set<Id> aEvent)
//  {
//      Map<Id,Event__c> mapEventToUpdate = new Map<Id,Event__c>();
//
//      Double conversionUSD = getConversionType();
//      if(conversionUSD == null) conversionUSD = 1;
//
//      for(AggregateResult iEvent : [Select Event__c, SUM(TotalAmount) total
//                                                                  FROM Order
//                                                                  Where Event__c =: aEvent
//                                                                  And Opportunity.RecordTypeId =: REC_OPP_SPEX
//                                                                  Group By Event__c])
//      {
//          Event__c lEvent = new Event__c(Id = (Id)iEvent.get('Event__c')); 
//          lEvent.Total_of_Order_SPEX__c = (Decimal)iEvent.get('total') * conversionUSD;
//          mapEventToUpdate.put(lEvent.Id, lEvent);
//      }
//      return mapEventToUpdate;
//  }
//
//public static void executeSumInOppToEvent() 
//  {
//      system.debug('#####executeSumInOppToEvent#####');
//      system.debug('isExecuting:' +isExecuting);
//      
//      if(isExecuting) return;
//
//      system.debug('#####executeSumInOppToEvent Cont...#####');
//      
//      TriggerUtils.assertTrigger();
//
//      Set<String> setIdEvent = new Set<String>();
//
//      Set<String> setStageName = new Set<String>{'Approved', 'Approved by Finance', 'Won'};
//
//      for(Opportunity iOpp : (list<Opportunity>) trigger.new)
//      {
//          if(String.isBlank(iOpp.Event__c)) continue;
//
//          /*if((setStageName.contains(iOpp.StageName) && iOpp.RecordTypeId == REC_OPP_OPEN_EVENTS) ||
//            (setStageName.contains(iOpp.StageName) && iOpp.RecordTypeId == REC_OPP_OPEN_EVENTS && TriggerUtils.wasChanged( iOpp, Opportunity.Amount )))*/
//          
//          system.debug('#####iOpp.RecordTypeId:' +iOpp.RecordTypeId);
//          system.debug('#####REC_OPP_OPEN_EVENTS:' +REC_OPP_OPEN_EVENTS);
//          system.debug('#####TriggerUtils.wasChanged:' + TriggerUtils.wasChanged( iOpp, Opportunity.Amount ));
//          
//          
//          if(setStageName.contains(iOpp.StageName) && iOpp.RecordTypeId == REC_OPP_OPEN_EVENTS && TriggerUtils.wasChanged( iOpp, Opportunity.Amount ))
//          {
//              setIdEvent.add(iOpp.Event__c);
//          }
//      }
//      
//      system.debug('#####setIdEvent:' +setIdEvent.isEmpty());
//      
//      if(setIdEvent.isEmpty()) return;
//
//      Map<Id,Event__c> mapEventToUpdate = sumOppField(setIdEvent, setStageName);
//
//      if(!mapEventToUpdate.isEmpty()) Database.update(mapEventToUpdate.values());
// }
//
//  public static void executeSumInOppToEventOnDelete()
//{
//      TriggerUtils.assertTrigger();
//
//      Set<String> setIdEvent = new Set<String>();
//
//      Set<String> setStageName = new Set<String>{'Approved', 'Approved by Finance', 'Won'};
//
//      for(Opportunity iOpp : (list<Opportunity>) trigger.old)
//      {
//
//          if(String.isBlank(iOpp.Event__c)) continue;
//          
//          if(setStageName.contains(iOpp.StageName) && iOpp.RecordTypeId == REC_OPP_OPEN_EVENTS)
//          {
//              setIdEvent.add(iOpp.Event__c);
//          }
//      }
//
//      if(setIdEvent.isEmpty()) return;
//
//      Map<Id,Event__c> mapEventToUpdate = sumOppField(setIdEvent, setStageName);
//
//      if(!mapEventToUpdate.isEmpty()) Database.update(mapEventToUpdate.values());
// }
//
//  public static void executeSumInOrderToEvent() 
//  {   
//      TriggerUtils.assertTrigger();
//
//      set<String> setOrderRecordType = new set<String>{REC_ORDER_ADDPASSES, REC_ORDER_EVENTSPONSOR};
//
//      Set<Id> setIdEvent = new Set<Id>();
//
//      for(Order iOrder : (list<Order>) trigger.new)
//      {
//          if(String.isBlank(iOrder.Event__c)) continue;
//
//          if(String.isNotBlank(iOrder.Event__c) && setOrderRecordType.contains(iOrder.RecordTypeId) && 
//              TriggerUtils.wasChanged( iOrder, Order.Sum_Amount__c ) || TriggerUtils.wasChanged( iOrder, Order.Quantity_Sum__c ))
//          {
//              setIdEvent.add(iOrder.Event__c);
//          }
//      }
//
//      if(setIdEvent.isEmpty()) return;
//
//      Map<Id,Event__c> mapEventToUpdate = sumOrderField(setIdEvent);
//
//      if(!mapEventToUpdate.isEmpty()) Database.update(mapEventToUpdate.values());
//  }
//
//  public static void executeSumInOrderToEventOnDelete() 
//  {   
//      TriggerUtils.assertTrigger();
//
//      set<Id> setOrderRecordType = new set<Id>{REC_ORDER_ADDPASSES, REC_ORDER_EVENTSPONSOR};
//
//      Set<Id> setIdEvent = new Set<Id>();
//
//      for(Order iOrder : (list<Order>) trigger.old)
//      {
//          if(String.isBlank(iOrder.Event__c)) continue;
//
//          if(String.isNotBlank(iOrder.Event__c) && setOrderRecordType.contains(iOrder.RecordTypeId))
//          {
//              setIdEvent.add(iOrder.Event__c);
//          }
//      }
//
//      if(setIdEvent.isEmpty()) return;
//
//    Map<Id,Event__c> mapEventToUpdate = sumOrderField(setIdEvent);
//
//      if(!mapEventToUpdate.isEmpty()) Database.update(mapEventToUpdate.values());
 }