@isTest (SeeAllData = true)
public class OpportunityApproovalCommitMan_Test {
    // Test Sandbox enviroment
    private static Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
    private static String STAGE_COMMITTEE_REJECTED = 'Rejected';
    private static String STAGE_COMMITTEE_ACCEPTED = 'Approved';
    private static String STAGE_COMMITTEE_PROCESSING = 'Processing';
    private static String STAGE_OPP_APPROVED = 'Approved';
    private static String STAGE_OPP_APPROVED_BY_FINANCE = 'Approved by Finance';
    private static String STAGE_OPP_WON = 'Won';
    private static String CLUB_RE_BRAZIL = 'RE Club Brazil';
    private static String CLUB_RE_EUROPE = 'RE Club Europe';
    private static String CLUB_INFRA_LATAM = 'Infra Club LatAm';
    private static String CLUB_INFRA_INDIA = 'Infra Club India';
    private static String CLUB_RE_INDIA = 'RE Club India';
    private static final String STATUS_ACTIVATED = 'Activated';
    private static final String STATUS_EXPIRED = 'Expired'; // grace period
    private static Product2 lProduct2;
    private static PricebookEntry lPricebookEntry;
    
    // Account
    private static Account lAccount;
    private static Account lAccount_Latam;
    private static Account lAccount_Europe;
    private static Account lAccount_Brazil;
    private static Account lAccount_India;
    private static Account account2;

    
    // Contact
    private static Contact lContact;
    private static Contact lContact_Latam;
    private static Contact lContact_Europe;
    private static Contact lContact_Brazil;
    private static Contact lContact_India;
    private static Contact contact2;

    
    // Clubs
    private static Club__c lClubc;
    private static Club__c lClubc_Infra_Latam_fake;
    private static Club__c lClubc_Infra_Latam;
    private static Club__c lClubc_Infra_India;
    private static Club__c lClubc_RE_Europe;
    private static Club__c lClubc_RE_Brazil;
    private static Club__c lClubc_RE_India;
    private static Club__c club2;

    
    static{
        //Application_Config__c  fucker = new  Application_Config__c(Name='BucketUserId', Value__c=UserInfo.getUserId());
        //insert fucker;     



        
        lAccount = NewInstanceSObject.createAccount();
        //lAccount = SObjectInstance.createAccount2();
        lAccount.BillingCity = 'São Pualo'; 
        lAccount.BillingCountry = 'Brazil' ;
        lAccount.BillingState = 'SP';
        lAccount.BillingStreet = 'Rua teste, 10';
        database.insert(lAccount);
        System.debug('[Test-Debug] Acc.Name='+lAccount.Name + ', Acc.Id='+lAccount.Id );

        account2 = NewInstanceSObject.createAccount('testeAccount');
        club2 = NewInstanceSObject.createClub('testeclub');
        contact2 = NewInstanceSObject.createContact(lAccount.Id, 'testeFirst', 'testLast');
        
        lContact = NewInstanceSObject.createContact(lAccount.Id);
        //lContact = SObjectInstance.createContato(lAccount.Id);
        database.insert(lContact);
        System.debug('[Test-Debug] lContact.Id='+lContact.Id);
        
        // Club Infra Latam 
        lClubc_Infra_Latam_fake = NewInstanceSObject.createClub();
        lClubc_Infra_Latam = NewInstanceSObject.createClub();
        //lClubc_Infra_Latam = SObjectInstance.createClub();
        lClubc_Infra_Latam_fake.Name = 'Infra Club LatAm fake';
        database.insert(lClubc_Infra_Latam_fake);
        lClubc_Infra_Latam.Name = 'Infra Club LatAm';
        database.insert(lClubc_Infra_Latam);
        System.debug('[Test-Debug] lClubc_Infra_Latam_fake.Name='+lClubc_Infra_Latam_fake.Name +
                     ', lClubc_Infra_Latam_fake.Id='+lClubc_Infra_Latam_fake.Id);
        // Club Infra India
        lClubc_Infra_India = NewInstanceSObject.createClub();
        //lClubc_Infra_India = SObjectInstance.createClub();
        lClubc_Infra_India.Name = 'Infra Club India';
        database.insert(lClubc_Infra_India);
        System.debug('[Test-Debug] lClubc_Infra_India.Name='+lClubc_Infra_India.Name +
                     ', lClubc_Infra_India.Id='+lClubc_Infra_India.Id);    
        // Club RE Europe
        lClubc_RE_Europe = NewInstanceSObject.createClub();
        //lClubc_RE_Europe = SObjectInstance.createClub();
        lClubc_RE_Europe.Name = 'RE Club Europe';
        database.insert(lClubc_RE_Europe);
        System.debug('[Test-Debug] lClubc_RE_Europe.Name='+lClubc_RE_Europe.Name +
                     ', lClubc_RE_Europe.Id='+lClubc_RE_Europe.Id);      
        // Club RE Brazil
        lClubc_RE_Brazil  = NewInstanceSObject.createClub();
        //lClubc_RE_Brazil = SObjectInstance.createClub();
        lClubc_RE_Brazil.Name = 'RE Club Brazil';
        database.insert(lClubc_RE_Brazil);
        System.debug('[Test-Debug] lClubc_RE_Brazil.Name='+lClubc_RE_Brazil.Name +
                     ', lClubc_RE_Brazil.Id='+lClubc_RE_Brazil.Id);    
        // Club RE India
        lClubc_RE_India = NewInstanceSObject.createClub();
        //lClubc_RE_India = SObjectInstance.createClub();
        lClubc_RE_India.Name = 'RE Club India';
        database.insert(lClubc_RE_India);
        System.debug('[Test-Debug] lClubc_Infra_Brazil.Name='+lClubc_RE_India.Name +
                     ', lClubc_Infra_Brazil.Id='+lClubc_RE_India.Id);    
        
        // Product
        lProduct2 = NewInstanceSObject.createProduct();
        //lProduct2 = SObjectInstance.createProduct2();
        database.insert(lProduct2);
        // Criação do PricebookEntry
        Id pbkId =  NewInstanceSObject.getStandardPricebookId();
        //id pbkId = SObjectInstance.catalogoDePrecoPadrao2();
        lPricebookEntry = NewInstanceSObject.pricebookEntry(pbkId,lProduct2.Id);
        //lPricebookEntry = SObjectInstance.entradaDePreco(pbkId,lProduct2.Id);
        database.insert(lPricebookEntry);
        System.debug('[Test-Debug] lPricebookEntry.Name='+lPricebookEntry.Name+', lProduct2.Id='+lProduct2.Id);        
    }
    
    /*
    @isTest private static void test_matchclub_valid()
    {
        test.startTest();
        // Limit date
        Date closeDate = Date.newInstance(2018, 5, 1);
        
        // Opp RE Brazil 1 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create opp for RE Brazil');
        Opportunity lOpportunity_reBr =  NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                                        lContact, 
                                                                                        lClubc_RE_Brazil, 
                                                                                        closeDate);
        database.insert(lOpportunity_reBr);
        System.debug('[Test-Debug] Querry updated Opp');
        lOpportunity_reBr  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:lOpportunity_reBr.Id];
        System.assertEquals('RE Club Brazil', lOpportunity_reBr.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lClubc_RE_Brazil.Name+', lClubc_RE_Brazil='+
                            lOpportunity_reBr.Club_Name__c+' dont match.');
        System.debug('[Test-Debug] Update Opp for RE Brazil');
        lOpportunity_reBr.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED ;
        lOpportunity_reBr.StageName = STAGE_OPP_APPROVED_BY_FINANCE ;
        database.update(lOpportunity_reBr);
        System.assertEquals(STAGE_OPP_APPROVED, lOpportunity_reBr.ApprovalCommitteeResult__c, 
                            '[Test-Error] Field lOpportunity_reBr.ApprovalCommitteeResult__c was not correctely updated.'+
                            'value='+lOpportunity_reBr.ApprovalCommitteeResult__c);
        
        // Activate membership RE Brazil 1
        list<Contract> lContractAfter = [SELECT Name,Opportunity__c,Status 
                                         FROM Contract 
                                         WHERE Opportunity__c=:lOpportunity_reBr.Id];
        //System.assertEquals(1, lContractAfter.size(), '*** wrong number of contracts');
        
        System.debug('[Test-Debug] lContractAfter.size()='+lContractAfter.size());
        setContractStatus(lContractAfter, 'Activated');
        System.debug('[Test-Debug] lContractAfter= '+ lContractAfter);
        database.update(lContractAfter);
        System.debug('[Test-Debug] lContractAfter.size()='+lContractAfter.size());
        System.debug('[Test-Debug] lContractAfter= '+ lContractAfter);
        
        System.assertEquals(1, lContractAfter.size(), '[Test-Error] not the right number of contracts');
        System.assertEquals('Activated', lContractAfter[0].Status, '***Contract not updated');
        
        // Opp RE Brazil 2 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create a new opp for RE Brazil');
        Opportunity lOpportunity_reBr2 = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                                        lContact, 
                                                                                        lClubc_RE_Brazil, 
                                                                                        closeDate);
        database.insert(lOpportunity_reBr2);
        System.debug('[Test-Debug] Querry updated Opp');
        lOpportunity_reBr2  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:lOpportunity_reBr2.Id];
        System.assertEquals('RE Club Brazil', lOpportunity_reBr2.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lClubc_RE_Brazil.Name+',   lOpportunity_reBr2.Club_Name__c='+
                            lOpportunity_reBr2.Club_Name__c+' dont match.');
        System.debug('[Test-Debug] Update Opp for RE Brazil');
        lOpportunity_reBr2.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED ;
        lOpportunity_reBr2.StageName = STAGE_OPP_APPROVED_BY_FINANCE ;
        database.update(lOpportunity_reBr2);
        System.assertEquals(STAGE_OPP_APPROVED, lOpportunity_reBr2.ApprovalCommitteeResult__c, 
                            '[Test-Error] Field lOpportunity_reBr2.ApprovalCommitteeResult__c was not correctely updated.'+
                            'value='+lOpportunity_reBr2.ApprovalCommitteeResult__c);
        
        // Opp RE Europe ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create opp for RE Europe');
        Opportunity lOpportunity_reEu = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                                       lContact, 
                                                                                       lClubc_RE_Europe, 
                                                                                       closeDate);
        database.insert(lOpportunity_reEu);
        System.debug('[Test-Debug] Querry updated Opp');
        lOpportunity_reEu  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:lOpportunity_reEu.Id];
        System.assertEquals('RE Club Europe', lOpportunity_reEu.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lOpportunity_reEu.Name+', lOpportunity_reEu='+
                            lOpportunity_reEu.Club_Name__c+' dont match.');
        System.debug('[Test-Debug] Update Opp for RE Europe');
        
        // Update Opp committee result
        lOpportunity_reEu.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED ;
        database.update(lOpportunity_reEu);
        System.assertEquals(STAGE_OPP_APPROVED, lOpportunity_reEu.ApprovalCommitteeResult__c, 
                            '[Test-Error] Field lOpportunity_reEu.ApprovalCommitteeResult__c was not correctely updated.'+
                            'value='+lOpportunity_reEu.ApprovalCommitteeResult__c);

        // Update Opp Stage
        System.debug('[Test-Debug] Test Brazil RE Opp');
        lOpportunity_reBr.StageName = STAGE_OPP_APPROVED;
        database.update(lOpportunity_reBr);
        
        System.debug('[Test-Debug] Test Europe RE Opp');
        lOpportunity_reEu.StageName = STAGE_OPP_APPROVED;
        database.update(lOpportunity_reEu);
        
        test.stopTest();
    }
	*/

    @isTest private static void test_matchclub_rejectedBr()
    {
        test.startTest();
        
        // Limit date
        Date closeDate = Date.newInstance(2018, 5, 1);
        
        // Opp RE Brazil 1 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create opp for RE Brazil');
        Opportunity lOpportunity_reBr = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                                       lContact, 
                                                                                       lClubc_RE_Brazil, 
                                                                                       closeDate);
        database.insert(lOpportunity_reBr);
        System.debug('[Test-Debug] Querry updated Opp');
        lOpportunity_reBr  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:lOpportunity_reBr.Id];
        System.assertEquals('RE Club Brazil', lOpportunity_reBr.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lClubc_RE_Brazil.Name+', lClubc_RE_Brazil='+
                            lOpportunity_reBr.Club_Name__c+' dont match.');
        System.debug('[Test-Debug] Update Opp for RE Brazil');
        lOpportunity_reBr.ApprovalCommitteeResult__c = STAGE_COMMITTEE_REJECTED ;
        lOpportunity_reBr.StageName = STAGE_OPP_APPROVED_BY_FINANCE ;

        Database.SaveResult  srRej = database.update(lOpportunity_reBr, False);
        System.assertEquals(False, srRej.isSuccess(), 'This should not be updated rightly, since was rejected');

        System.assertEquals(STAGE_COMMITTEE_REJECTED, lOpportunity_reBr.ApprovalCommitteeResult__c, 
                            '[Test-Error] Field lOpportunity_reBr.ApprovalCommitteeResult__c was not correctely updated.'+
                            'value='+lOpportunity_reBr.ApprovalCommitteeResult__c);
        
        // Activate membership RE Brazil 1
        list<Contract> lContractAfter = [SELECT Name,Opportunity__c,Status 
                                         FROM Contract 
                                         WHERE Opportunity__c=:lOpportunity_reBr.Id];
        //System.assertEquals(1, lContractAfter.size(), '*** wrong number of contracts');
        
        System.debug('[Test-Debug] lContractAfter.size()='+lContractAfter.size());
        setContractStatus(lContractAfter, 'Activated');
        System.debug('[Test-Debug] lContractAfter= '+ lContractAfter);
        database.update(lContractAfter);
        System.debug('[Test-Debug] lContractAfter.size()='+lContractAfter.size());
        System.debug('[Test-Debug] lContractAfter= '+ lContractAfter);
        
        System.assertEquals(0, lContractAfter.size(), '[Test-Error] not the right number of contracts');
        //System.assertEquals('Activated', lContractAfter[0].Status, '***Contract not updated');
        
        test.stopTest();        
    }
    
    @isTest private static void test_matchclub_acceptedBr()
    {
        test.startTest();
        
        // Limit date
        Date closeDate = Date.newInstance(2018, 5, 1);
        
        // Opp RE Brazil 2 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create a new opp for RE Brazil');
        Opportunity lOpportunity_reBr2 = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                                        lContact, 
                                                                                        lClubc_RE_Brazil, 
                                                                                        closeDate);
        database.insert(lOpportunity_reBr2);
        System.debug('[Test-Debug] Querry updated Opp');
        lOpportunity_reBr2  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:lOpportunity_reBr2.Id];
        System.assertEquals('RE Club Brazil', lOpportunity_reBr2.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lClubc_RE_Brazil.Name+',   lOpportunity_reBr2.Club_Name__c='+
                            lOpportunity_reBr2.Club_Name__c+' dont match.');
        System.debug('[Test-Debug] Update Opp for RE Brazil');
        
        lOpportunity_reBr2.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED ;
        lOpportunity_reBr2.StageName = STAGE_OPP_APPROVED_BY_FINANCE ;
        database.update(lOpportunity_reBr2);
        
        System.assertEquals(STAGE_COMMITTEE_ACCEPTED, lOpportunity_reBr2.ApprovalCommitteeResult__c, 
                            '[Test-Error] Field lOpportunity_reBr2.ApprovalCommitteeResult__c was not correctely updated.'+
                            'value='+lOpportunity_reBr2.ApprovalCommitteeResult__c);

        // Update Opp Stage
        lOpportunity_reBr2.StageName = STAGE_OPP_APPROVED;
        Database.SaveResult  sr = database.update(lOpportunity_reBr2, False);

        System.assertEquals(sr.isSuccess(), True, 
                               '[Test-Debug] This should not be updated rightly, since it was rejected');  
        
        test.stopTest(); 
    }

    @isTest private static void test_matchclub_acceptedLatam()
    {
        test.startTest();
        
        // Limit date
        Date closeDate = Date.newInstance(2018, 5, 1);
        
        // Opp RE Brazil 2 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create a new opp for RE Brazil');
        Opportunity lOpportunity_InfraLatam = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                                        lContact, 
                                                                                        lClubc_Infra_Latam, 
                                                                                        closeDate);
        database.insert(lOpportunity_InfraLatam);
        System.debug('[Test-Debug] Querry updated Opp');
        lOpportunity_InfraLatam  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:lOpportunity_InfraLatam.Id];
        System.assertEquals('Infra Club LatAm', lOpportunity_InfraLatam.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lClubc_Infra_Latam.Name+',   lOpportunity_InfraLatam.Club_Name__c='+
                            lOpportunity_InfraLatam.Club_Name__c+' dont match.');
        System.debug('[Test-Debug] Update Opp for Infra Club LatAm');
        
        lOpportunity_InfraLatam.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED ;
        lOpportunity_InfraLatam.StageName = STAGE_OPP_APPROVED_BY_FINANCE ;
        database.update(lOpportunity_InfraLatam);
        
        System.assertEquals(STAGE_COMMITTEE_ACCEPTED, lOpportunity_InfraLatam.ApprovalCommitteeResult__c, 
                            '[Test-Error] Field lOpportunity_reBr2.ApprovalCommitteeResult__c was not correctely updated.'+
                            'value='+lOpportunity_InfraLatam.ApprovalCommitteeResult__c);

        // Update Opp Stage
        lOpportunity_InfraLatam.StageName = STAGE_OPP_APPROVED;
        Database.SaveResult  sr = database.update(lOpportunity_InfraLatam, False);

        System.assertEquals(sr.isSuccess(), True, 
                               '[Test-Debug] This should not be updated rightly, since it was rejected');  
        
        test.stopTest(); 
    }    
    
    @isTest private static void test_matchclub_rejectedEu()
    {

        test.startTest();
        // Limit date
        Date closeDate = Date.newInstance(2018, 5, 1);

        // Opp RE Europe ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create opp for RE Europe');
        Opportunity lOpportunity_reEu = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                                       lContact, 
                                                                                       lClubc_RE_Europe, 
                                                                                       closeDate);
        database.insert(lOpportunity_reEu);
        System.debug('[Test-Debug] Querry updated Opp');
        lOpportunity_reEu  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:lOpportunity_reEu.Id];
        System.assertEquals('RE Club Europe', lOpportunity_reEu.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lOpportunity_reEu.Name+', lOpportunity_reEu='+
                            lOpportunity_reEu.Club_Name__c+' dont match.');
        System.debug('[Test-Debug] Update Opp for RE Europe');
        
        // Update Opp committee result
        System.debug('Opportunity RE Europe -> ApprovalCommittee Rejected');
        lOpportunity_reEu.ApprovalCommitteeResult__c = STAGE_COMMITTEE_REJECTED ;
        database.update(lOpportunity_reEu);

        System.assertEquals(STAGE_COMMITTEE_REJECTED, lOpportunity_reEu.ApprovalCommitteeResult__c, 
                            '[Test-Error] Field lOpportunity_reEu.ApprovalCommitteeResult__c was not correctely updated.'+
                            'value='+lOpportunity_reEu.ApprovalCommitteeResult__c);

  
        System.debug('[Test-Debug] Test Europe RE Opp');
        lOpportunity_reEu.StageName = STAGE_OPP_APPROVED;
        try
        {
        	database.update(lOpportunity_reEu);
        }
        catch (Exception e)
        {
            System.debug('[Test-Debug] This should not be updated rightly, since was rejected');
        }
        test.stopTest();        
        
    }    
    
    /*
    @istest private static void test_unmatchclub()
    {
        test.startTest();

        System.debug('[Test-Debug] Club Names');
        System.assertEquals('Infra Club LatAm fake', lClubc_Infra_Latam_fake.Name, 'Club name is wrongly spelled');
        System.assertEquals('RE Club India', lClubc_RE_India.Name, 'Club name is wrongly spelled');
        
        // Test Opp from Infra Latam
        
        // Change status CLub Infra Latam
        Opportunity lOpportunity_latam = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                       lContact, 
                                                       lClubc_Infra_Latam_fake, 
                                                       date.newInstance(2018, 3, 6));
        database.insert(lOpportunity_latam);
        
        // test if the Opp was rightly created
        lOpportunity_latam = [SELECT Id, Name, StageName, Club_Name__c FROM  Opportunity WHERE Id=:lOpportunity_latam.Id];
        System.assertEquals('Infra Club LatAm fake', lOpportunity_latam.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            lOpportunity_latam.Name+', lOpportunity_latam='+
                            lOpportunity_latam.Club_Name__c+' dont match.');
        
        // Update Opp
        lOpportunity_latam.StageName = STAGE_OPP_APPROVED_BY_FINANCE ;
        
        database.update(lOpportunity_latam);

        // Test if the trigger worked correctely
        lOpportunity_latam = [SELECT Id, Name, StageName, Club_Name__c FROM  Opportunity WHERE Id=:lOpportunity_latam.Id];
        System.assertEquals(STAGE_OPP_APPROVED_BY_FINANCE, lOpportunity_latam.StageName, 
                            '[Test-Error] Field lOpportunity_latam.StageName was not correctely updated.'+
                            'value='+lOpportunity_latam.StageName);

        // Test RE India

        // Change status CLub RE India
        Opportunity lOpportunity_india = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                       lContact, 
                                                       lClubc_RE_India, 
                                                       date.newInstance(2018, 6, 6));
        database.insert(lOpportunity_india);
        
        // test if the Opp was rightly created
        lOpportunity_india = [SELECT Id, Name, StageName, Club_Name__c FROM  Opportunity WHERE Id=:lOpportunity_india.Id];
        System.assertEquals( lClubc_RE_India.Name, lOpportunity_india.Club_Name__c, '[Test-Error] Fields lClubc_RE_India.Name='+
                            lClubc_RE_India.Name+', lOpportunity_latam='+
                            lOpportunity_india.Club_Name__c+' dont match.');
        
        // Update Opp
        lOpportunity_india.StageName = STAGE_OPP_WON ;
        database.update(lOpportunity_india);

        // Test if the trigger worked correctely
        lOpportunity_india = [SELECT Id, Name, StageName, Club_Name__c FROM  Opportunity WHERE Id=:lOpportunity_india.Id];
        System.assertEquals('Won', lOpportunity_india.StageName, 
                            '[Test-Error] Field lOpportunity_latam.StageName was not correctely updated.'+
                            'value='+lOpportunity_india.StageName);
                 
        test.stopTest();
    }
	*/
    
    private static void setContractStatus(list<Contract> lContractAfter, String theStatus)
    {
        for (Contract ctr: lContractAfter)
        {
            ctr.Status = theStatus;
        }
    }

    @isTest private static void test_contact_with_active_memberships()
    {
        test.startTest();
        
        Date closeDate = Date.newInstance(2018, 6, 20);
        
        // Opp RE Brazil 1 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create opp for RE Brazil');
        Opportunity oppBr1 = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                            lContact, 
                                                                            lClubc_RE_Brazil, 
                                                                            closeDate);
        //Opportunity oppBr1 = SObjectInstance.createOpportunityMembership(lAccount, 
        //                                                                     RT_OPP_MEMBERSHIP, 
        //                                                                     lContact, 
        //                                                                     lClubc_RE_Brazil,
        //                                                                     closeDate);
        database.insert(oppBr1);

        oppBr1  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:oppBr1.Id];
        System.assertEquals('RE Club Brazil', oppBr1.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            oppBr1.Name+', lClubc_RE_Brazil='+
                            oppBr1.Club_Name__c+' dont match.');        
        System.debug('[Test-Debug] Create a memberhips for the Opp ' + oppBr1.Name);
        oppBr1.StageName = STAGE_OPP_APPROVED_BY_FINANCE;

        oppBr1.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED;
        database.update(oppBr1);
        
        oppBr1  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:oppBr1.Id];
        list<Contract> memb_oppBr1 = [SELECT Name,Opportunity__c,Status 
                                      FROM Contract 
                                      WHERE Opportunity__c=:oppBr1.Id];
        System.assertNotEquals(memb_oppBr1, NULL, '[Test-Debug] Test was not able to create memberhip. memb_oppBr1='+
                              memb_oppBr1+', memb_oppBr1='+memb_oppBr1);
        System.debug('%%[Test-Debug] Test was not able to create memberhip. memb_oppBr1='+
                              memb_oppBr1);
        
       // Opp RE Brazil 2 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create opp for RE Brazil');
        Opportunity oppBr2 = NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                            lContact, 
                                                                            lClubc_RE_Brazil, 
                                                                            closeDate);
        //Opportunity oppBr2 = SObjectInstance.createOpportunityMembership(lAccount, 
        //                                                                     RT_OPP_MEMBERSHIP, 
        //                                                                     lContact, 
		//                                                                 lClubc_RE_Brazil,
        //                                                                     closeDate);
        database.insert(oppBr2);

        oppBr2  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:oppBr2.Id];
        System.assertEquals('RE Club Brazil', oppBr2.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            oppBr2.Name+', lClubc_RE_Brazil='+
                            oppBr2.Club_Name__c+' dont match.');        
        System.debug('[Test-Debug] Create a memberhips for the Opp ' + oppBr2.Name);
        oppBr2.StageName = STAGE_OPP_APPROVED_BY_FINANCE;

        //oppBr2.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED;
        //database.update(oppBr2);
        
        oppBr2  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:oppBr2.Id];
        list<Contract> memb_oppBr2 = [SELECT Name,Opportunity__c,Status 
                                      FROM Contract 
                                      WHERE Opportunity__c=:oppBr2.Id];
        System.assertNotEquals(memb_oppBr2, NULL, '[Test-Debug] Test was not able to create memberhip. memb_oppBr2='+
                              memb_oppBr2);
        test.stopTest();
    }


    @isTest private static void test_contact_with_no_memberships()
    { 
        test.startTest();
        
        Date closeDate = Date.newInstance(2018, 6, 20);
        
        // Opp RE Brazil 1 ------------------------------------------------------------------------
        System.debug('[Test-Debug] Test: Create opp for RE Brazil');
        Opportunity oppBr1 =  NewInstanceSObject.createOpportunityMembership(lAccount, 
                                                                             lContact, 
                                                                             lClubc_RE_Brazil, 
                                                                             closeDate);
        //Opportunity oppBr1 = SObjectInstance.createOpportunityMembership(lAccount, 
        //                                                                     RT_OPP_MEMBERSHIP, 
        //                                                                     lContact, 
        //                                                                     lClubc_RE_Brazil,
        //                                                                     closeDate);
        database.insert(oppBr1);

        oppBr1  = [SELECT Id, Name, Club_Name__c FROM Opportunity WHERE Id=:oppBr1.Id];
        System.assertEquals('RE Club Brazil', oppBr1.Club_Name__c, '[Test-Error] Fields Club.Name='+
                            oppBr1.Name+', lClubc_RE_Brazil='+
                            oppBr1.Club_Name__c+' dont match.');        
        System.debug('[Test-Debug] Create a memberhips for the Opp ' + oppBr1.Name);


        oppBr1.ApprovalCommitteeResult__c = STAGE_COMMITTEE_ACCEPTED;
        try{
            database.update(oppBr1);
        }
        catch (Exception e)
        {
            System.debug('[Test-Debug] e='+e);
        }

        test.stopTest();
    }

}