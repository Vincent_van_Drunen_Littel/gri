/**************************************************************************************************
* GRI Club - Gathering the world’s leading real estate and infrastructure players 
**************************************************************************************************
* A test class for EventAttendeeHandler trigger
* 
* @author  Anderson Paschoalon
* @version 1.0
* @since
* Date        Author         Description
* 24/07/18    AndPaschoalon  First version
*/
@IsTest
public class EventAttendeeHandler_Test {
    private static final String ProdCodeDelegate = 'P-000033';
    private static final String ProdCodeCoChair = 'P-000034';
    private static final String ProdCodeVipDinner = 'P-000798';

    @isTest
    private static void  test_delegateOnly() 
    {
        List<Opportunity> listOpps = [SELECT Id, Name, StageName FROM Opportunity];
        Product2  delegate = TestDataFactory.createProduct(ProdCodeDelegate);
        TestDataFactory.addProductToOpportunity(delegate, listOpps);

        Test.startTest();
        for(Opportunity opp:listOpps)
            opp.StageName = 'Approved';
        update listOpps;
        List<Event_Attendee__c> listEvAtt = [SELECT Id, VIP_Dinner_Invitee_New__c FROM Event_Attendee__c];
        System.assertEquals(listOpps.size(), listEvAtt.size());
        for(Event_Attendee__c evatt:listEvAtt)
        {
            System.assertEquals(false, evatt.VIP_Dinner_Invitee_New__c);
        }
        Test.stopTest();
    }

    @isTest
    private static void  test_vipDinnerOnly()
    {
        List<Opportunity> listOpps = [SELECT Id, Name, StageName FROM Opportunity];
        Product2  delegate = TestDataFactory.createProduct(ProdCodeVipDinner);
        TestDataFactory.addProductToOpportunity(delegate, listOpps);

        Test.startTest();
        for(Opportunity opp:listOpps)
            opp.StageName = 'Approved';
        update listOpps;
        List<Event_Attendee__c> listEvAtt = [SELECT Id, VIP_Dinner_Invitee_New__c FROM Event_Attendee__c];
        System.assertEquals(listOpps.size(), listEvAtt.size());
        for(Event_Attendee__c evatt:listEvAtt)
        {
            System.assertEquals(false, evatt.VIP_Dinner_Invitee_New__c);
        }
        Test.stopTest();        
    }


    @isTest
    private static void  test_cochair()
    {
        List<Opportunity> listOpps = [SELECT Id, Name, StageName FROM Opportunity];
        Product2  cochair = TestDataFactory.createProduct(ProdCodeCoChair);
        TestDataFactory.addProductToOpportunity(cochair, listOpps);

        Test.startTest();
        for(Opportunity opp:listOpps)
            opp.StageName = 'Approved';
        update listOpps;
        List<Event_Attendee__c> listEvAtt = [SELECT Id, VIP_Dinner_Invitee_New__c FROM Event_Attendee__c];
        System.assertEquals(listOpps.size(), listEvAtt.size());
        for(Event_Attendee__c evatt:listEvAtt)
        {
            System.assertEquals(true, evatt.VIP_Dinner_Invitee_New__c);
        }
        Test.stopTest();              
    }

    @isTest
    private static void  test_eventNoVipDinner()
    {
        Event__c theEvent = [SELECT Id, Name,Vip_Dinner_Included__c FROM Event__c];
        theEvent.Vip_Dinner_Included__c=false;
        update theEvent;
        List<Opportunity> listOpps = [SELECT Id, Name, StageName FROM Opportunity];
        Product2  delegate = TestDataFactory.createProduct(ProdCodeVipDinner);
        TestDataFactory.addProductToOpportunity(delegate, listOpps);

        Test.startTest();
        for(Opportunity opp:listOpps)
            opp.StageName = 'Approved';
        update listOpps;
        List<Event_Attendee__c> listEvAtt = [SELECT Id, VIP_Dinner_Invitee_New__c FROM Event_Attendee__c];
        System.assertEquals(listOpps.size(), listEvAtt.size());
        for(Event_Attendee__c evatt:listEvAtt)
        {
            System.assertEquals(true, evatt.VIP_Dinner_Invitee_New__c);
        }
        Test.stopTest(); 
    }

    @isTest
    private static void assertSetup()
    {
        List<Contact> listContacts = [SELECT Id, FirstName, LastName  FROM Contact];
        List<Opportunity> listOpps = [SELECT Id, Name, StageName FROM Opportunity];

        Test.startTest();
        System.assertNotEquals(0, listContacts.size());
        System.assertNotEquals(0, listOpps.size());
        Test.stopTest();
    }

    @testSetup 
    private static void buildData()
    {
        // setup data
        CheckRecursive2.DisableCheckRecursive2OnTest = true;
        TestDataFactory.init();

        Integer nConPerAcc = 15;
        List<Club__c> listClubs = TestDataFactory.createClubs(
            new List<String>{ TestDataFactory.CLUB_RE_LATAM, TestDataFactory.CLUB_RE_BRAZIL, TestDataFactory.CLUB_RE_INDIA});
        Club__c smartusCorp = TestDataFactory.createClubs(TestDataFactory.SMARTUS_REAL_ESTATE);
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent', listClubs[0]);
        theGreatEvent.Vip_Dinner_Included__c = true;
        update theGreatEvent;

        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(nConPerAcc);
        TestDataFactory.createEventOpps(listContacts, theGreatEvent, listClubs[0], System.today()+10);
    }

}