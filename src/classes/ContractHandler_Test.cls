/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure 
 * players
 ******************************************************************************
 * Test Class for ContractHandler
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date      Author     Description
 * 02/08/18  AndersonP  First version. 
 */
@IsTest
private class ContractHandler_Test {

    private static final Integer static_NumberContactsPerAccount=20;
    private static final Integer static_NumberAccounts=1;
    private static final String CLUB_RE_BRAZIL=HelperSObject.CLUB_RE_BRAZIL;
    private static final String CLUB_RE_EUROPE=HelperSObject.CLUB_RE_EUROPE;
    private static final String CLUB_RE_INDIA=HelperSObject.CLUB_RE_INDIA;
    private static final String CLUB_INFRA_INDIA=HelperSObject.CLUB_INFRA_INDIA;
    private static final String CLUB_INFRA_LATAM=HelperSObject.CLUB_INFRA_LATAM;
    private static final String CLUB_RE_LATAM=HelperSObject.CLUB_RE_LATAM;
    private static final String CLUB_RETAIL_BRAZIL=HelperSObject.CLUB_RETAIL_BRAZIL;
    private static final String CLUB_GRI_GLOBAL_CLUB=HelperSObject.CLUB_GRI_GLOBAL_CLUB;
    private static final String CLUB_RE_AFRICA=HelperSObject.CLUB_RE_AFRICA;
    private static final String LOGTAG='@@ ContractHandler_Test: ';

    ///////////////////////////////////////////////////////////////////////////
    // Auto-Won Renewed-Memberships Opportunities
    ///////////////////////////////////////////////////////////////////////////

    @isTest 
    private static void test_autoWonRenewedMemberships() 
    {
        List<Contact> listContacts = [SELECT Id, AccountId, FirstName, LastName FROM Contact LIMIT 20];
        Club__c club_Re_Brazil = [SELECT Id, Name FROM Club__c WHERE Name=:CLUB_RE_BRAZIL];
        Event__c eventBrazilRe = TestDataFactory.createEvent('Test Event Brazil RE' ,club_Re_Brazil);
        eventBrazilRe.Start_Date__c = System.today()+30;
        eventBrazilRe.End_Date__c = System.today()+31;
        update eventBrazilRe;

        List<Opportunity> listOpps = TestDataFactory.createEventOpps(listContacts, eventBrazilRe, club_Re_Brazil, System.today()+10);
        TestDataFactory.addProductToOpportunity(TestDataFactory.createProduct(HelperSObject.PRODUCT_MEMBER_FREE), 
            listOpps);

        for(Opportunity opp:listOpps)
            opp.StageName=HelperSObject.OPPORTUNITY_STAGE_APPROVED;
        update listOpps;


        Test.startTest();
        List<Contract> listMemberships = TestDataFactory.createMemberships(listContacts, club_Re_Brazil, System.today()+365);
        for(Contract membership:listMemberships)
        {
            membership.Membership_Level__c = HelperSObject.MEMBERSHIP_LV_DIAMOND_MEMBER;
            membership.StartDate=System.today();
            membership.ContractTerm=12;
            membership.Status=HelperSObject.MEMBERSHIP_STATUS_ACTIVATED;
        }
        update listMemberships;
        Test.stopTest();

        List<Opportunity> listOppsUpdated = 
            [SELECT Id, Name, Contact__c, AccountId, StageName, 
            (SELECT Id, ProductCode FROM OpportunityLineItems)
            FROM Opportunity WHERE Id IN :listOpps];
        for(Opportunity opp:listOppsUpdated)
        {
            System.assertEquals(HelperSObject.OPPORTUNITY_STAGE_WON, opp.StageName);
        }

    }

    ///////////////////////////////////////////////////////////////////////////
    // Contact Field Club Update 
    ///////////////////////////////////////////////////////////////////////////

    @isTest 
    private static void test_setContactStatus() 
    {
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(50);

        //listContacts[0] = MembershipContactClubManager.setContactStatus(CLUB_RE_BRAZIL, listContacts[0], TRUE);
        listContacts[0] = ContractHandler.setContactStatus(CLUB_RE_BRAZIL, listContacts[0], TRUE);
        update listContacts[0];
        System.assertEquals(TRUE, listContacts[0].Real_Estate_BR_Club__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[0].Real_Estate_BR_Club__c='+listContacts[0].Real_Estate_BR_Club__c);

        //listContacts[1] = MembershipContactClubManager.setContactStatus(CLUB_RE_BRAZIL, listContacts[1], FALSE);
        listContacts[1] = ContractHandler.setContactStatus(CLUB_RE_BRAZIL, listContacts[1], FALSE);
        update listContacts[1];
        System.assertEquals(FALSE, listContacts[1].Real_Estate_BR_Club__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[1].Real_Estate_BR_Club__c='+listContacts[1].Real_Estate_BR_Club__c);

        listContacts[2] = MembershipContactClubManager.setContactStatus(CLUB_RE_EUROPE , listContacts[2], TRUE);
        listContacts[2] = ContractHandler.setContactStatus(CLUB_RE_EUROPE , listContacts[2], TRUE);
        update listContacts[2];
        System.assertEquals(TRUE, listContacts[2].Global_Club_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[2].Global_Club_Member__c='+listContacts[2].Global_Club_Member__c);

        //listContacts[3] = MembershipContactClubManager.setContactStatus(CLUB_RE_INDIA, listContacts[3], TRUE); 
        listContacts[3] = ContractHandler.setContactStatus(CLUB_RE_INDIA, listContacts[3], TRUE);
        update listContacts[3];
        System.assertEquals(TRUE, listContacts[3].India_Club_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[3].India_Club_Member__c='+listContacts[3].India_Club_Member__c);

        //listContacts[4] = MembershipContactClubManager.setContactStatus(CLUB_INFRA_INDIA , listContacts[4], TRUE);
        listContacts[4] = ContractHandler.setContactStatus(CLUB_INFRA_INDIA , listContacts[4], TRUE);
        update listContacts[4];
        System.assertEquals(TRUE, listContacts[4].Infra_Club_India_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[4].Infra_Club_India_Member__c='+listContacts[4].Infra_Club_India_Member__c);

        //listContacts[5] = MembershipContactClubManager.setContactStatus(CLUB_INFRA_LATAM , listContacts[5], TRUE);
        listContacts[5] = ContractHandler.setContactStatus(CLUB_INFRA_LATAM , listContacts[5], TRUE);
        update listContacts[5];
        System.assertEquals(TRUE, listContacts[5].Infrastructure_BR_Club__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[5].Infrastructure_BR_Club__c='+listContacts[5].Infrastructure_BR_Club__c);

        //listContacts[6] = MembershipContactClubManager.setContactStatus(CLUB_RE_LATAM , listContacts[6], TRUE);
        listContacts[6] = ContractHandler.setContactStatus(CLUB_RE_LATAM , listContacts[6], TRUE);
        update listContacts[6];
        System.assertEquals(TRUE, listContacts[6].RE_Club_Latam_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[6].RE_Club_Latam_Member__c='+listContacts[6].RE_Club_Latam_Member__c);

        //listContacts[7] = MembershipContactClubManager.setContactStatus(CLUB_RETAIL_BRAZIL , listContacts[7], TRUE);
        listContacts[7] = ContractHandler.setContactStatus(CLUB_RETAIL_BRAZIL , listContacts[7], TRUE);
        update listContacts[7];
        System.assertEquals(TRUE, listContacts[7].Retail_Club_BR__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[7].Retail_Club_BR__c='+listContacts[7].Retail_Club_BR__c);             

        //listContacts[8] = MembershipContactClubManager.setContactStatus(CLUB_GRI_GLOBAL_CLUB, listContacts[8], TRUE);   
        listContacts[8] = ContractHandler.setContactStatus(CLUB_RETAIL_BRAZIL, listContacts[8], TRUE);
        update listContacts[8];
        System.assertEquals(TRUE, listContacts[8].Retail_Club_BR__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[8].Retail_Club_BR__c='+listContacts[8].Retail_Club_BR__c);

        //listContacts[8] = MembershipContactClubManager.setContactStatus(CLUB_GRI_GLOBAL_CLUB, listContacts[8], TRUE);   
        listContacts[8] = ContractHandler.setContactStatus(CLUB_GRI_GLOBAL_CLUB, listContacts[8], TRUE);
        update listContacts[8];
        System.assertEquals(TRUE, listContacts[8].Global_Club_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[8].Global_Club_Member__c='+listContacts[8].Global_Club_Member__c);


        //listContacts[9] = MembershipContactClubManager.setContactStatus(CLUB_RE_AFRICA , listContacts[9], TRUE);        
        listContacts[9] = ContractHandler.setContactStatus(CLUB_RE_AFRICA , listContacts[9], TRUE);
        update listContacts[9];
        System.assertEquals(TRUE, listContacts[9].RE_Club_Africa_Member__c , 
                            'MembershipContactClubManager.setContactStatus() didnt rightly change the contact club' +
                           'listContacts[9].RE_Club_Africa_Member__c='+listContacts[9].RE_Club_Africa_Member__c);
    }

    @isTest 
    private static void test_updateContactClubFieldsTrue()
    {
        Integer nContactPerAcc = 20;
        List<Contact> listContact = TestDataFactory.createContactsWithDefaultAccount(nContactPerAcc);
        Club__c clubReBrazil = [SELECT Id, Name FROM Club__c WHERE Name=:CLUB_RE_BRAZIL];
        System.assertEquals(CLUB_RE_BRAZIL, clubReBrazil.Name);
        Club__c clubInfraLatam = [SELECT Id, Name FROM Club__c WHERE Name=:CLUB_INFRA_LATAM];
        System.assertEquals(CLUB_INFRA_LATAM, clubInfraLatam.Name);

        Test.startTest();

        List<Contract> listMembershipsClubRebrazil = TestDataFactory.createMemberships(listContact, clubReBrazil, System.today()+365);
        System.assertEquals(nContactPerAcc, listMembershipsClubRebrazil.size(), 
            'nContactPerAcc, listMembershipsClubRebrazil.size()');
        listMembershipsClubRebrazil = [SELECT Id, Contact__c, AccountId, Name, Status, Club__c, Club_Name__c FROM Contract WHERE Id IN :listMembershipsClubRebrazil];
        System.debug(LOGTAG+'listMembershipsClubRebrazil='+HelperSObject.json(listMembershipsClubRebrazil, 'Id, Status, Club__c, Name, Club_Name__c, Contact__c, AccountId'));  
        System.assertEquals(nContactPerAcc, listMembershipsClubRebrazil.size());
        for(Integer i=0; i<nContactPerAcc; i++)
        {
                listMembershipsClubRebrazil[i].Status=HelperSObject.MEMBERSHIP_STATUS_ACTIVATED;
                listMembershipsClubRebrazil[i].StartDate=System.today();
                listMembershipsClubRebrazil[i].ContractTerm=12;
        }
        update listMembershipsClubRebrazil;
        HelperSObject.log(LOGTAG+'listMembershipsClubRebrazil=', listMembershipsClubRebrazil, 
            'Id, Status, StartDate, ContractTerm, EndDate, Club__c, Club_Name__c, Contact__c');

        Test.stopTest();

        List<Contact> listUpdateContacts = 
            [SELECT Id, AccountId, FirstName, LastName, Real_Estate_BR_Club__c, RE_Club_Latam_Member__c,
            Infrastructure_BR_Club__c, Retail_Club_BR__c, India_Club_Member__c, 
            Global_Club_Member__c, RE_Club_Africa_Member__c, Infra_Club_India_Member__c 
            FROM Contact WHERE Id IN :listContact];
        for(Contact con:listUpdateContacts)
        {
                System.assertEquals(TRUE, con.Real_Estate_BR_Club__c, 'con.Real_Estate_BR_Club__c:'+con.Real_Estate_BR_Club__c);
                System.assertEquals(FALSE, con.RE_Club_Latam_Member__c, 'con.RE_Club_Latam_Member__c:'+con.RE_Club_Latam_Member__c);
                System.assertEquals(FALSE, con.Infrastructure_BR_Club__c, 'con.Infrastructure_BR_Club__c:'+con.Infrastructure_BR_Club__c);
                System.assertEquals(FALSE, con.India_Club_Member__c, 'con.India_Club_Member__c:'+con.India_Club_Member__c);
                System.assertEquals(FALSE, con.RE_Club_Africa_Member__c, 'con.RE_Club_Africa_Member__c:'+con.RE_Club_Africa_Member__c);
                System.assertEquals(FALSE, con.Global_Club_Member__c, 'con.Global_Club_Member__c:'+con.Global_Club_Member__c);
        }

    }

    @isTest 
    private static void test_updateContactClubFieldsFalse()
    {
        Integer nContactPerAcc = 20;
        List<Contact> listContact = TestDataFactory.createContactsWithDefaultAccount(nContactPerAcc);
        Club__c clubReBrazil = [SELECT Id, Name FROM Club__c WHERE Name=:CLUB_RE_BRAZIL];
        System.assertEquals(CLUB_RE_BRAZIL, clubReBrazil.Name);
        Club__c clubInfraLatam = [SELECT Id, Name FROM Club__c WHERE Name=:CLUB_INFRA_LATAM];
        System.assertEquals(CLUB_INFRA_LATAM, clubInfraLatam.Name);

        Test.startTest();

        List<Contract> listMembershipsClubRebrazil = TestDataFactory.createMemberships(listContact, clubReBrazil, System.today()+365);
        System.assertEquals(nContactPerAcc, listMembershipsClubRebrazil.size(), 
            'nContactPerAcc, listMembershipsClubRebrazil.size()');
        listMembershipsClubRebrazil = [SELECT Id, Contact__c, AccountId, Name, Status, Club__c, Club_Name__c FROM Contract WHERE Id IN :listMembershipsClubRebrazil];
        System.debug(LOGTAG+'listMembershipsClubRebrazil='+HelperSObject.json(listMembershipsClubRebrazil, 'Id, Status, Club__c, Name, Club_Name__c, Contact__c, AccountId'));  
        System.assertEquals(nContactPerAcc, listMembershipsClubRebrazil.size());
        for(Integer i=0; i<nContactPerAcc; i++)
        {
                listMembershipsClubRebrazil[i].Status=HelperSObject.MEMBERSHIP_STATUS_CANCELLED;
                listMembershipsClubRebrazil[i].StartDate=System.today();
                listMembershipsClubRebrazil[i].ContractTerm=12;
        }
        update listMembershipsClubRebrazil;
        HelperSObject.log(LOGTAG+'listMembershipsClubRebrazil=', listMembershipsClubRebrazil, 
            'Id, Status, StartDate, ContractTerm, EndDate, Club__c, Club_Name__c, Contact__c');

        Test.stopTest();

        List<Contact> listUpdateContacts = 
            [SELECT Id, AccountId, FirstName, LastName, Real_Estate_BR_Club__c, RE_Club_Latam_Member__c,
            Infrastructure_BR_Club__c, Retail_Club_BR__c, India_Club_Member__c, 
            Global_Club_Member__c, RE_Club_Africa_Member__c, Infra_Club_India_Member__c 
            FROM Contact WHERE Id IN :listContact];
        for(Contact con:listUpdateContacts)
        {
                System.assertEquals(FALSE, con.Real_Estate_BR_Club__c, 'con.Real_Estate_BR_Club__c:'+con.Real_Estate_BR_Club__c);
                System.assertEquals(FALSE, con.RE_Club_Latam_Member__c, 'con.RE_Club_Latam_Member__c:'+con.RE_Club_Latam_Member__c);
                System.assertEquals(FALSE, con.Infrastructure_BR_Club__c, 'con.Infrastructure_BR_Club__c:'+con.Infrastructure_BR_Club__c);
                System.assertEquals(FALSE, con.India_Club_Member__c, 'con.India_Club_Member__c:'+con.India_Club_Member__c);
                System.assertEquals(FALSE, con.RE_Club_Africa_Member__c, 'con.RE_Club_Africa_Member__c:'+con.RE_Club_Africa_Member__c);
                System.assertEquals(FALSE, con.Global_Club_Member__c, 'con.Global_Club_Member__c:'+con.Global_Club_Member__c);
        }

    }

    @testSetup 
    static void buildData()
    {
        System.debug(LOGTAG+'buildData() method called');
        TestDataFactory.init();
        Integer nConPerAcc = static_NumberContactsPerAccount;
        Integer nAcc = static_NumberAccounts;
        List<Account> listAccounts = TestDataFactory.createAccounts(nAcc);
        List<Contact> listContacts = TestDataFactory.createContacts(listAccounts, nConPerAcc);
        List<Club__c> listClubs = TestDataFactory.createClubs(TestDataFactory.listGriClubs());
        System.debug(LOGTAG+'listAccounts.size()='+listAccounts.size());
        System.debug(LOGTAG+'listContacts.size()='+listContacts.size());
        System.debug(LOGTAG+'listClubs.size()='+listClubs.size());
    }

}