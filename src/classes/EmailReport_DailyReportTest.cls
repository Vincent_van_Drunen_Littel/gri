@isTest
private class EmailReport_DailyReportTest {

	private static Id REC_OPP_MEMBERSHIP = RecordTypeMemory.getRecType( 'Opportunity', 'Membership' );
	private static Id REC_OPP_MAGAZINE_AD = RecordTypeMemory.getRecType( 'Opportunity', 'Magazine' );
	private static Id REC_OPP_UNIQUE = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
	private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );

	private static Account conta;
	private static Contact contato;
	private static Event__c testEvent;
	private static Club__c club;
	private static Club__c club2;
	private static Magazine__c magazine;
	private static Magazine__c magazine2;
	private static Pricebook2 pb;
	private static Pricebook2 pb2;
	private static Product2 testProduct;
	private static PricebookEntry standardpbe;
	private static PricebookEntry pbe;
	private static PricebookEntry pbe2;

	private static void createTestData()
	{		
		Application_Config__c conf = new Application_Config__c();
		conf.Name = 'BucketUserId';
		conf.Value__c = '00536000004lQpq';
		insert conf;

		conta = SObjectInstance.createAccount2();
	    Database.insert(conta);

	    contato = SObjectInstance.createContato(conta.Id);
	    contato.LastName = 'Teste contato';
	    Database.insert(contato);

	    club = SObjectInstance.createClub();
	    club.Designated_Region__c = 'Brazil RE';
	    Database.insert(club);

	    club2 = SObjectInstance.createClub();
	    club2.Designated_Region__c = 'Infra LatAm';
	    Database.insert(club2);

	    testEvent = SObjectInstance.createEvent();
	    testEvent.Type__c = 'Open Event';
	    testEvent.Club__c = club.Id;
	    insert testEvent;

	    testEvent.Start_Date__c = system.today().addDays(22);
	    testEvent.End_Date__c = system.today().addDays(22);
	    update testEvent ;

	    magazine = SObjectInstance.createMagazine();
	    magazine.CurrencyIsoCode = 'BRL';
	    magazine.Club__c = club.Id;
	    magazine.Designated_Region_2__c = 'Brazil RE';
	    Database.insert(magazine);

	    magazine2 = SObjectInstance.createMagazine();
	    magazine2.CurrencyIsoCode = 'BRL';
	    magazine2.Club__c = club2.Id;
	    magazine2.Designated_Region_2__c = 'Infra LatAm';
	    Database.insert(magazine2);

	    List<Pricebook2> testPricebooks = new List<Pricebook2>();

	    pb = SObjectInstance.catalogoDePreco();
	    pb.Club__c = club.Id;
	    pb.Magazine__c = magazine.Id;
	    pb.Type__c = 'Magazine Subscriptions';
	    pb.Event__c = testEvent.Id;
	    pb.Starting_date__c = system.today();
	    pb.End_date__c = system.today()+10;
	    testPricebooks.add(pb);

	    pb2 = SObjectInstance.catalogoDePreco();
	    pb2.Club__c = club.Id;
	    pb2.Magazine__c = magazine.Id;
	    pb2.Type__c = 'Spex';
	    pb2.Event__c = testEvent.Id;
	    pb2.Starting_date__c = system.today()+3;
	    testPricebooks.add(pb2);

	    insert testPricebooks;

	    System.debug('#### testPricebooks' + testPricebooks);

	    testProduct = SObjectInstance.createProduct2();
	    insert testProduct;

	    standardpbe = SObjectInstance.entradaDePreco(test.getStandardPricebookId(), testProduct.Id);
	    insert standardpbe;

	    pbe = SObjectInstance.entradaDePreco(pb.Id, testProduct.Id);
	    insert pbe;

	    pbe2 = SObjectInstance.entradaDePreco(pb2.Id, testProduct.Id);
	    insert pbe2;

	    System.debug('#### test.getStandardPricebookId()' + test.getStandardPricebookId());
	}

	@isTest static void testDailyReport_OpenEvent() {
		createTestData();

		List<Opportunity> testOpportunities = new List<Opportunity>();

	    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
	    opp.Event__c = testEvent.Id;
	    opp.Contact__c = contato.Id;
	    opp.CloseDate = system.today();
	    opp.StageName = 'Approved By Finance';
	    opp.Approved_by_Finance_is_Today__c = Date.Today();
	    testOpportunities.add(opp);

	    System.debug('#### opp' + opp);

	    Opportunity opp2 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    opp2.Event__c = testEvent.Id;
	    opp2.Contact__c = contato.Id;
	    opp2.CloseDate = system.today();
	    opp2.StageName = 'Approved By Finance';
	    opp2.Approved_by_Finance_is_Today__c = Date.Today();
	    testOpportunities.add(opp2);

	    System.debug('#### opp2' + opp2);

	    insert testOpportunities;

	    List<OpportunityLineItem> testLineItems = new List<OpportunityLineItem>();

	    OpportunityLineItem oppLineItem = new OpportunityLineItem(OpportunityId = opp.Id,
	    														PricebookEntryId = pbe.Id);
	    testLineItems.add(oppLineItem);

	    OpportunityLineItem oppLineItem2 = new OpportunityLineItem(OpportunityId = opp2.Id,
	    														PricebookEntryId = pbe2.Id);
	    testLineItems.add(oppLineItem2);

	    insert testLineItems;

	    List<Event_Attendee__c> eventAttendees = new List<Event_Attendee__c>();

	    Event_Attendee__c eventAttendee = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvent.Id);
	    eventAttendee.Event_Attendees_Status__c = 'Registered';
	    eventAttendee.Session_Position__c = 'Co-Chair';
	    eventAttendees.add(eventAttendee);

	    Event_Attendee__c eventAttendee2 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvent.Id);
	    eventAttendee2.Event_Attendees_Status__c = 'Registered';
	    eventAttendee2.Session_Position__c = 'Delegate';
	    eventAttendees.add(eventAttendee2);

	    Event_Attendee__c eventAttendee3 = SObjectInstance.createEventAttendee(opp.Id, conta.Id, contato.Id, testEvent.Id);
	    eventAttendee3.Event_Attendees_Status__c = 'Registered';
	    eventAttendee3.Session_Position__c = 'Member Free';
	    eventAttendees.add(eventAttendee3);

	    Event_Attendee__c eventAttendee4 = SObjectInstance.createEventAttendee(opp2.Id, conta.Id, contato.Id, testEvent.Id);
	    eventAttendee4.Event_Attendees_Status__c = 'Registered';
	    eventAttendee4.Session_Position__c = 'Co-Chair';
	    eventAttendees.add(eventAttendee4);

	    insert eventAttendees;	    	    
	    
	    Test.startTest();
		EmailReport_DailyReport cont = new EmailReport_DailyReport();
		Test.stopTest();
	}

	@isTest static void testDailyReport_Magazines() {
		createTestData();

		Opportunity magazineOpp1 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    magazineOpp1.Magazine__c = magazine.Id;
	    magazineOpp1.Contact__c = contato.Id;
	    magazineOpp1.CloseDate = system.today();
	    magazineOpp1.StageName = 'Approved By Finance';
	    magazineOpp1.Approved_by_Finance_is_Today__c = Date.Today();
	    magazineOpp1.Spex_Type__c = 'Club Sponsorship';
		magazineOpp1.Club__c = club.Id;
	    Database.insert(magazineOpp1);


	    OpportunityLineItem magazineOpp1LineItem = new OpportunityLineItem(OpportunityId = magazineOpp1.Id,
	    																	PricebookEntryId = pbe2.Id);
	    insert magazineOpp1LineItem;

	    /*Opportunity magazineOpp2 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_MAGAZINE_AD);
	    magazineOpp2.Magazine__c = magazine2.Id;
	    magazineOpp2.Contact__c = contato.Id;
	    magazineOpp2.CloseDate = system.today();
	    magazineOpp2.StageName = 'Approved By Finance';
	    magazineOpp2.Approved_by_Finance_is_Today__c = Date.Today();
	    magazineOpp2.Spex_Type__c = 'Club Sponsorship';
		magazineOpp1.Club__c = club2.Id;
	    Database.insert(magazineOpp2);


	    OpportunityLineItem magazineOpp2LineItem = new OpportunityLineItem(OpportunityId = magazineOpp2.Id,
	    																	PricebookEntryId = pbe2.Id);
	    insert magazineOpp2LineItem;
*/
		EmailReport_DailyReport cont = new EmailReport_DailyReport();
	}

	@isTest static void testDailyReport_Clubs() {
		createTestData();

		Opportunity clubOpp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_MEMBERSHIP);
	    clubOpp.Club__c = club.Id;
	    clubOpp.Contact__c = contato.Id;
	    clubOpp.CloseDate = system.today();
	    clubOpp.StageName = 'Approved By Finance';
	    clubOpp.Approved_by_Finance_is_Today__c = Date.Today();
	    insert clubOpp;


	    System.debug('#### clubOpp' + clubOpp);

	    OpportunityLineItem clubOppLineItem = new OpportunityLineItem(OpportunityId = clubOpp.Id,
	    														PricebookEntryId = pbe2.Id);
	    insert clubOppLineItem;

	    Opportunity clubOpp2 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
	    clubOpp2.Club__c = club.Id;
	    clubOpp2.Spex_Type__c = 'Multi-Product Sponsorship';
	    clubOpp2.Contact__c = contato.Id;
	    clubOpp2.CloseDate = system.today();
	    clubOpp2.StageName = 'Approved By Finance';
	    clubOpp2.Approved_by_Finance_is_Today__c = Date.Today();
	    insert clubOpp2;


	    System.debug('#### clubOpp2' + clubOpp2);

	    OpportunityLineItem clubOpp2LineItem = new OpportunityLineItem(OpportunityId = clubOpp2.Id,
	    														PricebookEntryId = pbe2.Id);
	    insert clubOpp2LineItem;

		EmailReport_DailyReport cont = new EmailReport_DailyReport();

		Application_Config__c conf = new Application_Config__c();
		conf.Name = 'DailyReportRecipientUserIds';
		conf.Value__c = '00536000004lQpq';
		insert conf;

		Application_Config__c conf2 = new Application_Config__c();
		conf2.Name = 'DailyReportRecipientEmails';
		conf2.Value__c = 'example@example.com';
		insert conf2;

		EmailReport_DailyReport_Schedulable sch = new EmailReport_DailyReport_Schedulable();
		sch.execute(null);
	}

}