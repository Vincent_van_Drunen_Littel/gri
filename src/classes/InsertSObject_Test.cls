@isTest (SeeAllData = true)
public class InsertSObject_Test {
    private static Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
    
    @isTest  private static void test()
    {
        test.startTest();
        String AccName = 'Test acc';
        String clubname = 'Amendobobos club';
        String FirstName = 'first-test';
        String LastName = 'last-test';

        Account testAccount1 = InsertSObject.createAccount();
        Account testAccount2 = InsertSObject.createAccount(AccName);

        System.debug('testAccount2.Id='+testAccount2.Id);
        Contact testCc1 = InsertSObject.createContact(testAccount2.Id);
        Contact testCc2 = InsertSObject.createContact(testAccount2.Id, FirstName, LastName);
 
        Club__c testClub1 = InsertSObject.createClub();
        Club__c testClub2 = InsertSObject.createClub(clubname);

        Opportunity testOpp1 = InsertSObject.createOpportunityMembership( 
            testAccount2,  testCc2, testClub2, date.newInstance(2018, 5, 1));
        Product2 testProduct = InsertSObject.createProduct();    
        
        Id pbId = InsertSObject.getStandardPricebookId();
        System.assertNotEquals(NULL, pbId, 'Price book Id list returned more Ids, tahn expected ');

        List<Id> accListId = new List<id>{testAccount1.Id, testAccount2.Id};
            List<Account> lacc = [SELECT Id,Name FROM Account WHERE ( Id IN :accListId )];
        System.assertEquals(2, lacc.size(), 
                            'Account list error, inserted and retrieved accounts dont match lacc='+lacc);
        
        List<Id> clubListId = new List<id>{testClub1.Id, testClub2.Id};
            List<Club__c> lclub = [SELECT Id,Name FROM Club__c WHERE ( Id IN :clubListId )];
        System.assertEquals(2, lclub.size(), 
                            'Club__c list error, inserted and retrieved accounts dont match lclub='+lclub);        
        
        List<Id> ccListId = new List<id>{testCc1.Id, testCc2.Id};
            List<Contact> lcc = [SELECT Id,Name FROM Contact WHERE ( Id IN :ccListId )];
        System.assertEquals(2, lcc.size(), 
                            'Contact list error, inserted and retrieved accounts dont match lcc='+lcc);  
        
        List<Id> lOppId = new List<id>{testOpp1.Id};
        List<Opportunity> lopp = [SELECT Id,Name FROM Opportunity WHERE ( Id IN :lOppId )];
        System.assertEquals(1, lopp.size(), 
        'Contact list error, inserted and retrieved accounts dont match lopp='+lopp); 
        
        List<Id> prodListId = new List<id>{testProduct.Id};
        List<Product2> lprod = [SELECT Id,Name FROM Product2 WHERE ( Id IN :prodListId )];
        System.assertEquals(1, lprod.size(), 
        'Contact list error, inserted and retrieved accounts dont match lcc='+lprod); 
        
        List<Id> pbListId = new List<id>{testCc1.Id, testCc2.Id};
        List<PricebookEntry> lpb = [SELECT Id,Name FROM PricebookEntry WHERE ( Id IN :pbListId )];
        System.assertEquals(2, lclub.size(), 
        'Contact list error, inserted and retrieved accounts dont match lpb='+lpb);
        
        Opportunity testOpp2 = InsertSObject.createOpportunityMembership( 
            testAccount2,  testCc2, testClub2, date.newInstance(2018, 5, 1));
        //insert testOpp2;
        
        //Product2 testProduct2 = InsertSObject.createProduct();
        //insert testOpp2;
        Id  stdPricebook = InsertSObject.getStandardPricebookId();
        PricebookEntry newPricebook = InsertSObject.pricebookEntry( stdPricebook, testProduct.Id );  

        test.stopTest();
    }

    @isTest  private static void testList()
    {
        List<Account> lacc1 = InsertSObject.createAccount(8);
        List<Account> lacc2 = InsertSObject.createAccount(5, 'Caju');
        List<Contact> lccc = InsertSObject.createContact(5, lacc2[0].Id, 'First-name', 'LastName'); 

        System.assertEquals(5, lacc2.size(), '%% [Test-debug] Not the same number of accounts. lacc.size()='+lacc2.size());
        System.assertEquals(5, lacc2.size(), '%% [Test-debug] Not the same number of contacts. lccc.size()='+lccc.size());
        System.assertEquals(8, lacc1.size(), '%% [Test-debug] Not the same number of contacts. lccc.size()='+lacc1.size());
        
    }
}