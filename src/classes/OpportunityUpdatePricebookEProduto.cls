/*******************************************************************************
*                               Cloud2b - 2016 
*-------------------------------------------------------------------------------
*
* Classe para Atualizar o Pricebook da Oportunidade de acordo com as datas 
* do Pricebook, e inserir novamente os produtos com os valores atuais.
*
* NAME: OpportunityUpdatePricebookEProduto.cls
* AUTHOR: RVR                                                  DATE: 12/04/2016
*******************************************************************************/
public with sharing class OpportunityUpdatePricebookEProduto 
{
    private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
    private static Id REC_OPP_SMARTUS_EVENTS = RecordTypeMemory.getRecType('Opportunity', 'Smartus_Events');
    private static Id REC_PRICEBOOK_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Pricebook2', 'Open_Events' ); // Events GRI and Smartus
    public static Boolean isExecuting = false;
    public static Boolean exec = false;
    
    public static void execute() 
    {
        TriggerUtils.assertTrigger();
        if(exec) {return;}
        exec = true;
        
        List<OpportunityLineItem> lstNewLineItems = new List<OpportunityLineItem>();
        List<Opportunity> lstOppControle = new List<Opportunity>();
        List<Opportunity> listOppPBOld = new List<Opportunity>();
        Map<String, Opportunity> mapOppToUpdate = new map<String, Opportunity>();
        Set<String> setEvents = new Set<String>();
        Set<String> setProduct = new Set<String>(); 
        Date lDataIni, lDataFim;
        Decimal PrecoUnitario, Desconto;
        
        for(Opportunity oppControleOLD: (list<Opportunity>) Trigger.old)
        {
            //System.debug('oppControleOLD'+oppControleOLD);
            listOppPBOld.add(oppControleOLD);
        }
        
        //   list<Opportunity> lOppToNone = new list<Opportunity>();
        for(Opportunity oppControle: (list<Opportunity>) Trigger.new)
        {
            //     if(TriggerUtils.wasChanged(oppControle, Opportunity.CloseDate)){
            //         lOppToNone.add(oppControle.clone(true));
            //     }
            if(oppControle.RecordTypeId!=REC_OPP_OPEN_EVENTS && oppControle.RecordTypeId!=REC_OPP_SMARTUS_EVENTS) 
                continue;
            
            // if it is not a test and Close date did not change, continue
            if(!Test.isRunningTest()) 
                if(!TriggerUtils.wasChanged(oppControle, Opportunity.CloseDate)) 
                continue;
            
            if(oppControle.Event__c != NULL) 
                setEvents.add(oppControle.Event__c);
            
            lstOppControle.add(oppControle);
            Date lDate = oppControle.CloseDate;
            
            lDataIni = (lDataIni == NULL || lDataIni > lDate) ? lDate : lDataIni; // catch the first close Date
            lDataFim = (lDataFim == NULL || lDataFim < lDate ) ? lDate : lDataFim; // catch the last close Date
        }
        /*    
if(!lOppToNone.isEmpty()){
for(Opportunity opp : lOppToNone){
opp.Approval_Process__c = null;
}
Database.update(lOppToNone);
}
*/
        if(lstOppControle.isEmpty()) {return;}
        isExecuting = true;
        
        // OppLineItems of the modified opps
        List<OpportunityLineItem> lstOldLineItems = [SELECT Id, PricebookEntry.Product2Id, Opportunity.Trigger_Is_Runnig__c,
                                                     PricebookEntry.Pricebook2Id, Quantity, UnitPrice, TotalPrice, PricebookEntryId, OpportunityId, ListPrice, Opportunity.Approval_Process__c,
                                                     Opportunity.Event__c, Opportunity.CloseDate, Opportunity.Pricebook_Changed__c, Opportunity.StageName, Discount
                                                     FROM OpportunityLineItem
                                                     WHERE OpportunityId =:lstOppControle];
        
        if(lstOldLineItems.isEmpty())
        {
            // create a Map of Events and Pricebook2 Ids
            map<String, String> mapPb = new map<String, String>();
            for(Pricebook2 iPb : [SELECT Id, Starting_date__c, End_date__c,Event__c, IsActive
                                  FROM Pricebook2 
                                  WHERE Event__c=:setEvents AND IsActive=true AND Starting_date__c<=:lDataIni AND End_date__c>=:lDataFim AND RecordTypeId=:REC_PRICEBOOK_OPEN_EVENTS])
            {
                mapPb.put(iPb.Event__c,iPb.Id);
            }
            if(mapPb.isEmpty()) return;
            for(Opportunity iOpp : lstOppControle)// Trigger.new Opps
            {
                String idPricebook2 = mapPb.get(iOpp.Event__c); // catch the event of the same pricebook
                mapOppToUpdate.put(iOpp.Id, new Opportunity(Id=iOpp.Id, Pricebook2Id=idPricebook2, Pricebook_Changed__c=true));
            }
            if(!mapOppToUpdate.isEmpty()) database.update(mapOppToUpdate.values());   
        }
        // set of Product Ids form Opps of lstOppControle
        for (OpportunityLineItem iOLI: lstOldLineItems)
        {
            setProduct.add(iOLI.PricebookEntry.Product2Id);
        }
        // creates a map of PricebookEntries. The key is yhr EventId+'---'+Product2Id
        map<String, list<PricebookEntry>> mapPbe = new map<String, list<PricebookEntry>>();
        for (PricebookEntry iPbe: [SELECT Id, Pricebook2.Id, Pricebook2.Name, Pricebook2.Event__c, Pricebook2.Type__c,
                                   Pricebook2.Starting_date__c, Pricebook2.End_date__c, Product2.Id, Product2.Name, UnitPrice
                                   FROM PricebookEntry WHERE Product2Id = :setProduct
                                   AND Pricebook2.Event__c = :setEvents AND Pricebook2.IsActive = true])
        {
            String lChave = iPbe.Pricebook2.Event__c + '---' + iPbe.Product2.Id;
            
            list<PricebookEntry> lstPbEvent = mapPbe.get(lChave);
            
            if (lstPbEvent == null)
            {
                lstPbEvent = new list<PricebookEntry>();
                mapPbe.put(lChave, lstPbEvent);
            }
            lstPbEvent.add(iPbe);
        }
        
        for (OpportunityLineItem iOLI: lstOldLineItems)
            
        {	
            String lChave = iOLI.Opportunity.Event__c + '---' + iOLI.PricebookEntry.Product2Id;
            list<PricebookEntry> lstPbEvent = mapPbe.get(lChave);
            system.debug('GFS -------------- lstPbEvent'+ lstPbEvent);
            if (lstPbEvent == null) continue;
            
            PricebookEntry lPbe = null;
            for (PricebookEntry iPbe: lstPbEvent)
            {
                // se o closedate estiver dentro do pricebook o unitprice será atualizado com o preço de lista.
                if ((iOLI.Opportunity.CloseDate >= iPbe.PriceBook2.Starting_date__c 
                     && iOLI.Opportunity.CloseDate <= iPbe.PriceBook2.End_date__c))
                {
                    //MP == código comentado por solicitação do suporte, tendo em vista que a regra do close date
                    //não é mais necessária, necessitando verificar somente se o pricebook id mudou
                    lPbe = iPbe;
                    if(lPbe.Pricebook2Id != listOppPBOld[0].Pricebook2Id)
                    {
                        PrecoUnitario = lPbe.UnitPrice;
                        Desconto = 0;
                    }
                    else
                    {
                        PrecoUnitario = iOLI.UnitPrice; 
                        Desconto = iOLI.Discount;
                    }
                }
                /*if ((iOLI.Opportunity.CloseDate <= iPbe.PriceBook2.Starting_date__c 
|| iOLI.Opportunity.CloseDate >= iPbe.PriceBook2.End_date__c))
{
if (PrecoUnitario == null){
PrecoUnitario = iOLI.UnitPrice;
Desconto = iOLI.Discount;
}
}*/
            }
            
            if (lPbe == null) {continue;}
            OpportunityLineItem lNewOLI = new OpportunityLineItem();
            Opportunity opp = new Opportunity();
            lNewOLI.OpportunityId = iOLI.OpportunityId;
            lNewOLI.PricebookEntryId = lPbe.Id;
            lNewOLI.Quantity = iOLI.Quantity;
            //lNewOLI.UnitPrice = (iOLI.Opportunity.Approval_Process__c == '1 - Approval') ? iOLI.UnitPrice : lPbe.UnitPrice;
            if(PrecoUnitario != null) {lNewOLI.UnitPrice = PrecoUnitario;}
            system.debug('lNewOLI.UnitPrice ' + lNewOLI.UnitPrice);
            //lNewOLI.Discount = (iOLI.Discount != null) ? iOLI.Discount : 0;
            if(Desconto != null) {lNewOLI.Discount = Desconto;}
            lstNewLineItems.add(lNewOLI);
            mapOppToUpdate.put(iOLI.OpportunityId, new Opportunity(Id = iOLI.OpportunityId, Pricebook2Id = lPbe.Pricebook2Id, Pricebook_Changed__c = true));
        }
        
        if (lstNewLineItems.isEmpty()) return;
        if(!lstOldLineItems.isEmpty()) database.delete(lstOldLineItems);
        if(!mapOppToUpdate.isEmpty()) database.update(mapOppToUpdate.values());
        if(!lstNewLineItems.isEmpty()) database.insert(lstNewLineItems);
    }
}