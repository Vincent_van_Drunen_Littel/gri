/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste da classe "OpportunityVinculaPricebook".
*
* NAME: OpportunityVinculaPricebookTest.cls
* AUTHOR: RVR                                                  DATE: 07/04/2016
* MODIFIER: LMdO                                               DATE: 04/07/2016
*******************************************************************************/
@isTest
private class OpportunityVinculaPricebookTest 
{

	private static Id REC_OPP_MEMBERSHIP = RecordTypeMemory.getRecType( 'Opportunity', 'Membership' );
	private static Id REC_OPP_MAGAZINE_AD = RecordTypeMemory.getRecType( 'Opportunity', 'Magazine' );
	//private static Id REC_OPP_MAGAZINE_SUBSCRIPTIONS = RecordTypeMemory.getRecType( 'Opportunity', 'Magazine_Subscriptions' );
	private static Id REC_OPP_UNIQUE = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
	private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );

  static testMethod void testeFuncional()
  {
	Account conta = SObjectInstance.createAccount2();
    Database.insert(conta);
    
    Contact contato = SObjectInstance.createContato(conta.Id);
    contato.LastName = 'Teste contato';
    Database.insert(contato);

    Event__c evento = SObjectInstance.createEvent();
    evento.Type__c = 'Open Event';
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    Club__c club = SObjectInstance.createClub();
    Database.insert(club);

    Magazine__c magazine = SObjectInstance.createMagazine();
    magazine.CurrencyIsoCode = 'BRL';
    Database.insert(magazine);

    List<Pricebook2> lstCatalogo = new List<Pricebook2>();
    
    Pricebook2 pb = SObjectInstance.catalogoDePreco();
    pb.Club__c = club.Id;
    pb.Magazine__c = magazine.Id;
    pb.Type__c = 'Magazine Subscriptions';
    pb.Event__c = evento.Id;
    pb.Starting_date__c = system.today();
    pb.End_date__c = system.today()+10;
    Database.insert(pb);
    lstCatalogo.add(pb);

    Pricebook2 pb2 = SObjectInstance.catalogoDePreco();
    pb2.Club__c = club.Id;
    pb2.Magazine__c = magazine.Id;
    pb2.Type__c = 'Spex';
    pb2.Event__c = evento.Id;
    pb2.Starting_date__c = system.today()+3;
    Database.insert(pb2);
    lstCatalogo.add(pb2);
    
    List<Opportunity> lstOpp = new List<Opportunity>();
    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
    opp.Club__c = pb.Club__c;
    opp.Contact__c = contato.Id;
    opp.CloseDate = system.today();
    opp.StageName = 'Suspect';
    Database.insert(opp);
    lstOpp.add(opp);

    Opportunity opp2 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_MAGAZINE_AD);
    opp2.Magazine__c = magazine.Id;
    opp2.Contact__c = contato.Id;
    opp2.CurrencyIsoCode = 'BRL';
    opp2.CloseDate = system.today();
    opp2.StageName = 'Suspect';
    Database.insert(opp2);
    lstOpp.add(opp2);

    Opportunity opp4 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_UNIQUE);
    opp4.Magazine__c = magazine.Id;
    opp4.Contact__c = contato.Id;
    opp4.CloseDate = system.today();
    opp4.StageName = 'Suspect';
    opp4.RecordTypeId = REC_OPP_UNIQUE;
    Database.insert(opp4);
    lstOpp.add(opp4);

    Opportunity opp5 = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
    opp5.Magazine__c = magazine.Id;
    opp5.Contact__c = contato.Id;
    opp5.CloseDate = system.today()+5;
    opp5.StageName = 'Suspect';
    opp5.Event__c = evento.Id;
    Database.insert(opp5);
    lstOpp.add(opp5);

    Test.startTest();
    	opp.Pricebook2Id = pb.Id;
    	Database.update(opp);

    	opp2.Pricebook2Id = pb.Id;
    	Database.update(opp2);

    	opp4.Pricebook2Id = pb2.Id;
    	Database.update(opp4);

    	opp5.Pricebook2Id = pb.Id;
    	Database.update(opp5);
    Test.stopTest();

	}
}