Public class OpportunityTriggerHandler extends TriggerHandler{
    private static string googleClientID;
    private static string googleSecretCode;
    private static string redirectURI;
    private static string authorizationCode;
    private static string tokenEndPointValue;
    private static string endPointValue;
    private static string accessToken;
    private static string refreshToken;
    private static string batchAttendeeEndpoint;
    private static string accessTokenVal;
    private static OpportunityBO bo = OpportunityBO.getInstance();
    
    public static List<opportunity> returnOpportunities(set<string> productIds,set<Id> oppIds){
        return  [SELECT Id, Event__c, Contact__c,Contact__r.email,(SELECT Quantity, UnitPrice, TotalPrice, PricebookEntry.Name, PricebookEntry.Product2.Family,PricebookEntryId 
                    FROM OpportunityLineItems where Product2Id IN:productIds) FROM Opportunity WHERE Id IN: oppIds];
    }
    public static void CreateInviteesForEvent(set<Id> oppIds){
        
        
        set<Id> eventIds = new set<Id>();
        set<string> contactBusinessEmails = new set<string>();
        Map<Id,Id> eventToOppMap = new Map<Id,Id>();
        set<string> productIds = new set<string>();
        for(Invite_Type_Product_Code__mdt inviteTpePrdObj: [ SELECT DeveloperName,ProductId__c FROM Invite_Type_Product_Code__mdt ]){
            productIds.add(inviteTpePrdObj.ProductId__c);
        }

        Map<Id,Map<Id,List<OpportunityLineItem>>> eventToOppToLineItemsMap = new Map<Id,Map<Id,List<OpportunityLineItem>>>();
        for(Opportunity opp:returnOpportunities(productIds,oppIds)){
            eventIds.add(opp.Event__c);
            contactBusinessEmails.add(opp.Contact__r.email);
            eventToOppMap.put(opp.Event__c,opp.Id);
            Map<Id,List<OpportunityLineItem>> oppToLineItemMap = new Map<Id,List<OpportunityLineItem>>();
            oppToLineItemMap.put(opp.Id,opp.OpportunityLineItems);
            eventToOppToLineItemsMap.put(opp.Event__c,oppToLineItemMap);
        }
        system.debug('@@contactBusinessEmails'+contactBusinessEmails);
        system.debug('@@eventToOppToLineItemsMap'+eventToOppToLineItemsMap);
        
        if(contactBusinessEmails.size()>0){
            
            set<Id> calInviteIds = new set<Id>();
            List<Calendar_Invite__c> calInviteLst = [SELECT id, Name,Venue__r.name, Venue_Address__c, IsInviteesLimitReached__c, GoogleCalendarId__c,ParentCalendarInvite__c, Timezone__c,GoogleCalendarEventId__c,
                                                        Calendar_Entry_Name__c,Invite_Type__c,Event__c, Event__r.Vip_Dinner_Included__c, Start_Date__c, End_Date__c, Event__r.name,Invite_Others__c,Modify_Event__c, See_Guest_List__c,
                                                        IsAutomated__c, Description__c FROM Calendar_Invite__c WHERE Event__c IN:eventIds AND IsAutomated__c = true ORDER BY NAME DESC];
                                                        
            for(Calendar_Invite__c calInviteObj:calInviteLst){
                calInviteIds.add(calInviteObj.Id);
                
            }
            /*Map<Id,Calendar_Invite__c> calendarInviteMap = new Map<Id,Calendar_Invite__c>([SELECT id, Name,Venue__r.name, Venue_Address__c, GoogleCalendarId__c,ParentCalendarInvite__c, Timezone__c,GoogleCalendarEventId__c,
                                                        Calendar_Entry_Name__c,Invite_Type__c,Event__c, Event__r.Vip_Dinner_Included__c, Start_Date__c, End_Date__c, Event__r.name,Invite_Others__c,Modify_Event__c, See_Guest_List__c,
                                                        IsAutomated__c, Description__c, (SELECT id, InviteeEmail__c,Calendar_Invite__c,Opportunity__c, Invite_Status__c FROM Calendar_Invitees__r) FROM Calendar_Invite__c
                                                        WHERE Event__c IN:eventIds AND IsInviteesLimitReached__c = false AND IsAutomated__c = true ORDER BY NAME DESC]);*/
            
            Map<Id, string> emailVerificationInInvitee = new Map<Id,string>();
            Map<Id,string> CalinviteToAttendeeStatusMap = new map<Id,string>();
            Map<Id, string> calenderInviteToEmailMap = new Map<Id,string>();
            for(Calendar_Invitee__c calObj:[SELECT id, Name,InviteeEmail__c,Invite_Status__c,Calendar_Invite__c,Opportunity__c FROM Calendar_Invitee__c WHERE Calendar_Invite__c IN:calInviteIds AND Opportunity__c IN:oppIds AND InviteeEmail__c IN:contactBusinessEmails]){
                calenderInviteToEmailMap.put(calObj.Calendar_Invite__c,calObj.InviteeEmail__c);
                 CalinviteToAttendeeStatusMap.put(calObj.Calendar_Invite__c,calObj.Invite_Status__c);
                if(calObj.Invite_Status__c =='Yes'){
                    emailVerificationInInvitee.put(calObj.Calendar_Invite__c,calObj.InviteeEmail__c);
                }
            }
            List<Calendar_Invite__c> processingCalInviteLst =  new List<Calendar_Invite__c>();
            for(Calendar_Invite__c calInviteObj:calInviteLst){
                if(calenderInviteToEmailMap.containskey(calInviteObj.Id)){
                    processingCalInviteLst.add(calInviteObj);
                }
                if(calenderInviteToEmailMap.isEmpty()){
                    if(calInviteObj.IsInviteesLimitReached__c==false){
                        processingCalInviteLst.add(calInviteObj);
                    }
                }          
            }
            system.debug('@@processingCalInviteLst'+processingCalInviteLst);
            
            system.debug('@@emailVerificationInInvitee'+emailVerificationInInvitee);
            Map<Id,Attendee__c> specialLunchSessionMap =  new Map<Id,Attendee__c>();
            Map<Id,Attendee__c> advisoryBoardSessionMap =  new Map<Id,Attendee__c>();
            
            for(Attendee__c attendeeObj:[SELECT Session_Status__c, Opportunity__c,  Event__c , Session__c,  Session__r.Event__c, Session__r.Type__c, Session__r.Status__c FROM Attendee__c WHERE Event__c IN:eventIds AND Opportunity__c IN: oppIds]){
                system.debug('@@attendeeObj.Session_Status__c'+attendeeObj.Session_Status__c+'@@attendeeObj.Session__r.Type__c'+attendeeObj.Session__r.Type__c+'@@attendeeObj.Session__r.Status__c'+attendeeObj.Session__r.Status__c);
                for(Id EventId:eventIds){
                    if(attendeeObj.Session_Status__c == 'Confirmed' && attendeeObj.Session__r.Type__c =='Advisory Board Meeting' && (attendeeObj.Session__r.Status__c =='Confirmed' || attendeeObj.Session__r.Status__c =='In production') && attendeeObj.Session__r.Event__c == EventId){
                        advisoryBoardSessionMap.put(attendeeObj.Event__c,attendeeObj);
                    }else if(attendeeObj.Session_Status__c == 'Confirmed' && attendeeObj.Session__r.Type__c =='Special Lunch' && (attendeeObj.Session__r.Status__c =='Confirmed' || attendeeObj.Session__r.Status__c =='In production') && attendeeObj.Session__r.Event__c == EventId){
                        specialLunchSessionMap.put(attendeeObj.Event__c,attendeeObj);
                    }
                }
            }
            
            system.debug('@@advisoryBoardSessionMap'+advisoryBoardSessionMap);
            system.debug('@@specialLunchSessionMap'+specialLunchSessionMap);
            List<Calendar_Invitee__c> insertCalendarInvites = new List<Calendar_Invitee__c>();
            Set<Calendar_Invite__c> finalCalendarInvites = new Set<Calendar_Invite__c>();
            for(Calendar_Invite__c calInviteOb:processingCalInviteLst){
                system.debug('@@calInviteOb.Event__r.Vip_Dinner_Included__c'+calInviteOb.Event__r.Vip_Dinner_Included__c);
                system.debug('@@eventToOppToLineItemsMap'+eventToOppToLineItemsMap);
                
                if(calInviteOb.Invite_Type__c == 'Dinner' && (eventToOppToLineItemsMap.containsKey(calInviteOb.id) || calInviteOb.Event__r.Vip_Dinner_Included__c==true)){
                        
                        /*if(eventToOppToLineItemsMap.containsKey(calInviteOb.id)){
                        system.debug('eventToOppToLineItemsMap.get(calInviteOb.id)'+eventToOppToLineItemsMap.get(calInviteOb.id).size());
                            if(eventToOppToLineItemsMap.get(calInviteOb.id).size()>0){
                                finalCalendarInvites.add(calInviteOb);
                            }
                        }else if(calInviteOb.Event__r.Vip_Dinner_Included__c==true){
                            finalCalendarInvites.add(calInviteOb);
                        }*/
                        finalCalendarInvites.add(calInviteOb);
                        system.debug('@@eventToOppToLineItemsMap.containsKey(calInviteOb.id)'+eventToOppToLineItemsMap.containsKey(calInviteOb.id)
                                    +'@@eventToOppMap.containsKey(calInviteOb.Event__c)'+eventToOppMap.containsKey(calInviteOb.Event__c));
                                    //+'@@eventToOppToLineItemsMap.get(calInviteOb.id).containsKey(eventToOppMap.get(calInviteOb.Event__c))'+eventToOppToLineItemsMap.get(calInviteOb.id).containsKey(eventToOppMap.get(calInviteOb.Event__c))
                                    //+'@@!eventToOppToLineItemsMap.get(calInviteOb.id).get(eventToOppMap.get(calInviteOb.Event__c)).isEmpty()'+!eventToOppToLineItemsMap.get(calInviteOb.id).get(eventToOppMap.get(calInviteOb.Event__c)).isEmpty());

                        system.debug('@@finalCalendarInvites'+finalCalendarInvites);
                   
                }else if(calInviteOb.Invite_Type__c == 'Special Lunch' && specialLunchSessionMap != null && specialLunchSessionMap.containsKey(calInviteOb.Event__c)){
                    finalCalendarInvites.add(calInviteOb);
                }else if(calInviteOb.Invite_Type__c == 'Advisory Board Meeting' && advisoryBoardSessionMap != null && advisoryBoardSessionMap.containsKey(calInviteOb.Event__c)){
                    finalCalendarInvites.add(calInviteOb);
                }else if(calInviteOb.Invite_Type__c != 'Dinner' && calInviteOb.Invite_Type__c != 'Special Lunch' && calInviteOb.Invite_Type__c != 'Advisory Board Meeting' && !emailVerificationInInvitee.containsKey(calInviteOb.Id)){
                    finalCalendarInvites.add(calInviteOb);
                }
            }
            system.debug('@@finalCalendarInvites'+finalCalendarInvites);
            
            List<Calendar_Invite__c> calObjLst = new List<Calendar_Invite__c>();
            set<Id> calInviteSet = new set<Id>();
            set<Id> parentInviteSet = new set<Id>();
            set<string> setOfEmails = new set<string>();
            
            
            for(string businessEmail:contactBusinessEmails){
                for(Calendar_Invite__c calInviteObj:finalCalendarInvites){
                    
                        calObjLst.add(calInviteObj);  
                        calInviteSet.add(calInviteObj.ParentCalendarInvite__c);
                        calInviteSet.add(calInviteObj.Id);
                        parentInviteSet.add(calInviteObj.ParentCalendarInvite__c);
                        /*for(Calendar_Invitee__c calInviteeObj:calInviteObj.Calendar_Invitees__r){
                            if(calInviteeObj.InviteeEmail__c.contains(businessEmail)){
                                    CalinviteToAttendeeStatusMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.Invite_Status__c);
                            }
                        }*/
                    
                } 
                setOfEmails.add(businessEmail);
            }
            Map<Id,set<string>> notifiedEmailsMap = new Map<Id,set<string>>();
            List<Calendar_Invitee__c> delCalendarInvites = [SELECT id,InviteeEmail__c,Calendar_Invite__c,Opportunity__c FROM Calendar_Invitee__c WHERE InviteeEmail__c IN :setOfEmails AND Opportunity__c IN:eventToOppMap.values() AND Invite_Status__c !='Yes' AND Calendar_Invite__c IN:calInviteSet];
            set<string> notifiedEmails = new set<string>();
            Map<Id,string> parentRemoveEmailMap = new Map<Id,string>();
            // To collect emails to send notification
            for(Calendar_Invitee__c calInviteeObj : delCalendarInvites) {
                /*if(parentInviteSet.contains(calInviteeObj.Calendar_Invite__c))
                    parentRemoveEmailMap.put(calInviteeObj.Calendar_Invite__c,calInviteeObj.InviteeEmail__c);*/
                
                if(!notifiedEmailsMap.containsKey(calInviteeObj.Calendar_Invite__c)){
                     notifiedEmailsMap.put(calInviteeObj.Calendar_Invite__c, new Set<String>{calInviteeObj.InviteeEmail__c});

                }else{
                     notifiedEmailsMap.get(calInviteeObj.Calendar_Invite__c).add(calInviteeObj.InviteeEmail__c);
                }
              
            }
            /*List<Calendar_Invite__c> removeAttendeeCalInviteLst = [SELECT id, Name, Timezone__c, Calendar_Entry_Name__c,Venue__r.name, Venue_Address__c,Invite_Others__c,Modify_Event__c, See_Guest_List__c,Invite_Type__c,
                                                Event__r.name,Start_Date__c,End_Date__c, GoogleCalendarEventId__c,GoogleCalendarId__c, Description__c,ParentCalendarInvite__c,
                                                (SELECT id, InviteeEmail__c,Calendar_Invite__c, Invite_Status__c FROM Calendar_Invitees__r) FROM Calendar_Invite__c WHERE Id IN :parentInviteSet AND IsInviteesLimitReached__c = true];
            if(removeAttendeeCalInviteLst.size()>0 && !parentRemoveEmailMap.isEmpty()){                                   
                RemoveDuplicateInviteeBatch removeDuplicates = new RemoveDuplicateInviteeBatch(removeAttendeeCalInviteLst,setOfEmails,parentRemoveEmailMap);
                database.executeBatch(removeDuplicates,100);
            }
            */
            
            for(Opportunity oppObj:returnOpportunities(productIds,oppIds)){
                system.debug('@@CalinviteToAttendeeStatusMap'+CalinviteToAttendeeStatusMap);
                //CreateCalendarInviteBatch be = new CreateCalendarInviteBatch(calObjLst,notifiedEmailsMap,setOfEmails,'',oppObj,'Opportunity',eventToOppMap,oppIds,CalinviteToAttendeeStatusMap);
                RemoveCalendarInviteBatch be = new RemoveCalendarInviteBatch(calObjLst,notifiedEmailsMap,setOfEmails,'',oppObj,'Opportunity',eventToOppMap,oppIds,CalinviteToAttendeeStatusMap);
                database.executeBatch(be,1);
            }
        }
    }

    public override void beforeUpdate(){
        bo.updateDigitalFunnelStageBefore(Trigger.new);
    }

}