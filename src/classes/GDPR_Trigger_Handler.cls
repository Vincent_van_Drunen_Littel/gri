public with sharing class GDPR_Trigger_Handler
{
    public static void execute(){
      List<Opportunity> opportunityList =  new List<Opportunity>();
      List<Contact> contactList = new List<Contact>();
      Set<Id> idList = new Set<Id>();
      Map<Id,Opportunity> oldOpportunities = (Map<Id,Opportunity>)trigger.oldMap;

      for(Opportunity opp : (List<Opportunity>)trigger.new){
        if(String.isNotBlank(opp.Contact__c) && opp.StageName == 'Approved by Finance' && (oldOpportunities == null || oldOpportunities.get(opp.Id).StageName != 'Approved by Finance')){
          idList.add(opp.Contact__c);
        }
      }

      if(idList.isEmpty()){
        return;
      }

      for(Contact con : [SELECT Id, Consent_Given_to_GDPR__c, Consent_Given_to_GDPR_by__c FROM Contact WHERE Id =: idList]){
        con.Consent_Given_to_GDPR__c = 'yes';
        con.Consent_Given_to_GDPR_by__c = 'Registration Form';
        contactList.add(con);
      }

      if(!contactList.isEmpty()){
         Database.update(contactList);
       }
    }
}