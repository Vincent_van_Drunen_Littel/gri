/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure 
 * players
 ******************************************************************************
 *  Trigger handler for Event_Attendee__c.
 *  The trigger call is handled by the methods before() and after(), that are
 * responsible to call other methods and classes
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 */
public class EventAttendeeHandler 
{
    ///////////////////////////////////////////////////////////////////////////
    // Triggler handlers
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Before Update handler
	 */
    public static void before()
    {
        System.debug(LOGTAG+'EventAttendeeHandler.before() ');
        if(Trigger.isInsert)
        {
            System.debug(LOGTAG+'EventAttendeeHandler.before() ');
            System.debug(LOGTAG+' Trigger.isInsert');
            vipDinner(Trigger.new);
        }
        if(Trigger.isUpdate)
        {
            System.debug(LOGTAG+'EventAttendeeHandler.before() ');
            System.debug(LOGTAG+' Trigger.isUpdate');
            vipDinner(Trigger.new);
        }
    }

    /**
     * After Update handler
	 */
    public static void after()
    {
        if(Trigger.isInsert)
        {
            // nothing todo
        }
        if(Trigger.isUpdate)
        {
            //EventAttendeeAlteraSessionAttendee.execute();
            updateSessionAttendee(Trigger.new);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Properties
    ///////////////////////////////////////////////////////////////////////////
    private static String LOGTAG = '%% EventAttendeeHandler: '; 

    ///////////////////////////////////////////////////////////////////////////
    // VIP DINNER UPDATE
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Update VIP dinner on Event_Attendee.
     * These products includes VIP Dinner, when the Event__c allows:
     * This method update the Event_Attendee__c VIP dinner checkbox, 
     * when it is created, according to the Product and type of event.
     * - P-000034: Co-chair
     * - P-001319: Co-Chair Sponsor - Free   
     * - P-000053: Co-Chair - Advisor    
     * - P-000049: Member Free
     * - P-000064: Member Replacement - Free
     * Vip Dinner Products:
     * - P-000798: Networking Dinner
     * - P-004331: Networking Dinner Member Replacement Free **not on sandbox
     * - P-000804: Networking Dinner Club Member Free
     * - P-000799: Networking Dinner (pay-per-use Club)
     * @param listEvAtt - list of Event_Attendee__c to have VIP Dinner Updated
     */
    public static void vipDinner(List<Event_Attendee__c> listEvAtt)
    {
        final List<String> listVipDinnerProdCodes = new List<String>{
            'P-000798', 'P-004331', 'P-000804', 'P-000799'};
        final List<String> listProdCochairOrMember = new List<String>{
            'P-000034', 'P-001319', 'P-000053', 'P-000049', 'P-000064'};
        //List<Event_Attendee__c> evAttUpdateList = new List<Event_Attendee__c>();
        Set<Id> setOppIdsEvAtt2Update = new Set<Id>();
        Set<Id> setOpportunityIds = new Set<Id>(); // OppIds from listEvAtt
        for(Event_Attendee__c evAtt:listEvAtt)
        {
            setOpportunityIds.add(evAtt.Opportunity__c);
        }
        System.debug(LOGTAG+'setOpportunityIds='+setOpportunityIds);

        List<Opportunity> listOppOli  = 
            [SELECT Id, Name, Event__c, Event__r.Name, Event__r.Vip_Dinner_Included__c, (SELECT Id, Name, ProductCode FROM OpportunityLineItems) 
             FROM Opportunity WHERE Id=:setOpportunityIds];
        System.debug(LOGTAG+'listOppOli='+listOppOli);
        //System.debug(LOGTAG+'listOppOli[0].OpportunityLineItems='+listOppOli[0].OpportunityLineItems);
        
        for(Opportunity opp: listOppOli)
        {
            List<OpportunityLineItem> listOli = opp.OpportunityLineItems;
            System.debug(LOGTAG+'listOli='+listOli);
            System.debug(LOGTAG+'opp.Event__r.Vip_Dinner_Included__c='+opp.Event__r.Vip_Dinner_Included__c);
            if(opp.Event__r.Vip_Dinner_Included__c==true)
            {
                if(oppContainsProduct(listOli, listProdCochairOrMember))
                {
                    //System.debug(LOGTAG+'opp.Event__r.Vip_Dinner_Included__c='+
                    //    opp.Event__r.Vip_Dinner_Included__c+'. EvAtt VIP Dinner=True.');
                    setOppIdsEvAtt2Update.add(opp.Id);
                }
            }
            else
            {
                if(oppContainsProduct(listOli, listVipDinnerProdCodes))
                    setOppIdsEvAtt2Update.add(opp.Id);
            }
        }//endfor
        
        for(Integer i=0; i<listEvAtt.size(); i++)
        {
            if(setOppIdsEvAtt2Update.contains(listEvAtt[i].Opportunity__c))
            {
                //System.debug(LOGTAG+' EventAtt:Id='+listEvAtt[i].Id+' and Opp:Id='+listEvAtt[i].Opportunity__c+' match!!');
                listEvAtt[i].VIP_Dinner_Invitee_New__c=true;
            }
        }
        //System.debug(LOGTAG+listEvAtt);
    }

    public static Boolean oppContainsProduct( 
        List<OpportunityLineItem> listOli, List<String> listProductCode)
    {
        for(OpportunityLineItem oli:listOli)
        {
            //System.debug(LOGTAG+'oli.ProductCode='+oli.ProductCode);
            //System.debug(LOGTAG+'listProductCode='+listProductCode);
            if(listProductCode.contains(oli.ProductCode))
            {
                //System.debug(LOGTAG+'Oli (id='+oli.Id+',Name='+oli.Name+',ProductCode='+
                //    oli.ProductCode+') is on list listProductCode='+listProductCode);
                return true;
            }
        }//endfor
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////
    // SESSION ATTENDEE UPDATE
    ///////////////////////////////////////////////////////////////////////////

    public static void updateSessionAttendee(List<Event_Attendee__c> newEventAtt)
    {

        TriggerUtils.assertTrigger();
        Map<String, Event_Attendee__c> mapEvAtt = new Map<String, Event_Attendee__c>();
        Set<String> listOppId = new Set<string>();
        List<String> listOppLooseStages = new List<String> {
            HelperSObject.OPPORTUNITY_STAGE_LOST,
            HelperSObject.OPPORTUNITY_STAGE_CANCELLED};
        List<String> listEventAttendeeLooseStatus = new List<String>{
            HelperSObject.EVENT_ATT_STATUS_CANCELLED,
            HelperSObject.EVENT_ATT_STATUS_REPLACED,
            HelperSObject.EVENT_ATT_STATUS_NOT_ATTENDING            
        };
        HelperSObject.log(LOGTAG+'updateSessionAttendee(): mapEvAtt.values()=', mapEvAtt.values(), 
            'Id, Opportunity__c, Event_Attendees_Status__c, ');

        for(Event_Attendee__c evAtt : newEventAtt)
        {
            if(listEventAttendeeLooseStatus.contains(evAtt.Event_Attendees_Status__c))
            {
                mapEvAtt.put(evAtt.Id, evAtt);
                listOppId.add(evAtt.Opportunity__c);    
            }
        }
        if(mapEvAtt.isEmpty() || listOppId.isEmpty()) return;
        
        //Recuperando Session attendee
        List<Attendee__c> listSesessionAtt = 
            [SELECT Id, Opportunity__c, Event__c, Name, Session_Status__c, Opportunity__r.StageName
            FROM Attendee__c WHERE Opportunity__c =: listOppId];
        HelperSObject.log(LOGTAG+'execute(): ', listSesessionAtt, 
            'Id, Event__c, Name, Session_Status__c');        

        for(Event_Attendee__c ev : mapEvAtt.values())
        {
            for(Attendee__c sessionAtt : listSesessionAtt)
            {
                if(ev.Opportunity__c != sessionAtt.Opportunity__c) 
                    continue;
                if(listEventAttendeeLooseStatus.contains(ev.Event_Attendees_Status__c) &&
                    listOppLooseStages.contains(sessionAtt.Opportunity__r.StageName) )
                {
                    sessionAtt.Session_Status__c = 'Declined';
                } 
            }            
        }
        HelperSObject.log(LOGTAG+'execute(): listSesessionAtt=', listSesessionAtt, 
            'Id, Event__c, Name, Session_Status__c');        
        if(!listSesessionAtt.isEmpty()) 
            update listSesessionAtt;
    }

}