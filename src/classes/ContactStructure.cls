/**  Copyright (c) 2008, Matthew Friend, Sales Engineering, Salesforce.com Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
*  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
*  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
*  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* To adapt this to anouther Object simply search for "Change" to go to the places 
* where the sObject and query must be changed
*/
public with sharing class ContactStructure{

    //Declare variables
    public String currentId;
    public List<ObjectStructureMap> asm ;
    public Map<String, ObjectStructureMap> masm;
    public List<Integer> maxLevel;
    
    /**
    * Contructor
    */
    public ContactStructure() {
      this.asm = new List<ObjectStructureMap>{};
      this.masm = new Map<String, ObjectStructureMap>{};
      this.maxLevel = new List<Integer>{};
    }
    
    /**
    * Allow page to set the current ID
    */
    public void setcurrentId( String cid ){
        currentId = cid;
    }

    /**
    * Return ObjectStructureMap to page
    * @return asm
    */
    public List<ObjectStructureMap> getObjectStructure(){
        asm.clear();
        if ( currentId == null ) {
            currentId = System.currentPageReference().getParameters().get( 'Id' );
        }
        
        System.assertNotEquals( currentId, null, 'sObject ID must be provided' );
        asm = formatObjectStructure( CurrentId );

        return asm;
    }

    /**
    * Query Account from top down to build the ObjectStructureMap
    * @param currentId
    * @return asm
    */
    public ObjectStructureMap[] formatObjectStructure( String currentId ){
    
        list<ObjectStructureMap> asm = new list<ObjectStructureMap>{}; 
        masm.clear();

        //Change below
        List<Contact> al            = new List<Contact>{};
        List<Opportunity> ol        = new List<Opportunity>{};
        List<ID> currentParent      = new List<ID>{};
        Map<ID, String> nodeList    = new Map<ID, String>{};
        List<String> nodeSortList   = new List<String>{};
        List<Boolean> levelFlag     = new List<Boolean>{};
        List<Boolean> closeFlag     = new List<Boolean>{};
        String nodeId               = '0';
        String nodeType             = 'child';
        Integer count               = 0;
        Integer level               = 0;
        Boolean endOfStructure      = false;
        String nome					= '';
        Id idObjeto					= null;
        String nomeOpp					= '';
        Id idObjetoOpp					= null;
        
        //Find highest level obejct in the structure
        currentParent.add( GetTopElement( currentId ) );

        //Loop though all children
        while ( !endOfStructure ){
			
            if( level == 0 ){
                //Change below     
                al = [ SELECT a.ReportsToId, a.OwnerId, a.Name, a.Id FROM Contact a WHERE a.Id IN : currentParent ORDER BY a.Name ];
                ol = [ SELECT o.Contact__c, o.OwnerId, o.Name, o.Id FROM Opportunity o WHERE o.Contact__c IN : al ORDER BY o.Name ];
            } 
            else {
                //Change below      
                al = [ SELECT a.ReportsToId, a.OwnerId, a.Name, a.Id FROM Contact a WHERE a.ReportsToId IN : currentParent ORDER BY a.Name ];
                ol = [ SELECT o.Contact__c, o.OwnerId, o.Name, o.Id FROM Opportunity o WHERE o.Contact__c IN : al ORDER BY o.Name ];
            }

            if( al.size() == 0){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                
                map<String, list<Opportunity>> mapContactsOpp = new map<String,list<Opportunity>>();
                
                for(Opportunity opp: ol)
                {
				  List<Opportunity> lstOpp = mapContactsOpp.get(opp.Contact__c);               
                  if(lstOpp == null)
                  {
                    lstOpp = new List<Opportunity>();
                  }
                  
                  lstOpp.add(opp);
                  mapContactsOpp.put(opp.Contact__c, lstOpp);  
                }
                                
                for ( Integer i = 0 ; i < al.size(); i++ ){
                    
                    //Integer OppIndex = -1;
                    Contact a = al[i];
                    nome = a.Name;
                    idObjeto = a.Id;
                    nodeId = ( level > 0 ) ? NodeList.get( a.ReportsToId )+'.'+String.valueOf( i ) : String.valueOf( i );
                    masm.put( NodeID, new ObjectStructureMap( nodeID, levelFlag, closeFlag, nodeType, false, false, nome, idObjeto) );
                    currentParent.add( a.Id );
                    nodeList.put( a.id,nodeId );
                    nodeSortList.add( nodeId );
                                        
                    if(mapContactsOpp.get(a.Id) == null) continue;
                    
                    for(Opportunity lopp: mapContactsOpp.get(a.Id))
                    {
                        //OppIndex++;
                        nome = lopp.Name;
                        idObjeto = lopp.Id;

                        nodeId = (a.ReportsToId != null) ? NodeList.get( lopp.Contact__c )+'.'+String.valueOf( lopp.Id ) : String.valueOf( lopp.Id );
                        masm.put( NodeID, new ObjectStructureMap( nodeID, levelFlag, closeFlag, nodeType, false, false, nome, idObjeto ) );
                     	currentParent.add( idObjeto );
                        nodeList.put( idObjeto,nodeId );
                        nodeSortList.add( nodeId );
                        
                        
                    }
                    /*for(Integer j = 0 ; j < ol.size(); j++){
                        Opportunity o = ol[j];
                        nomeOpp = o.Name;
                        idObjetoOpp = o.Id;

                        nodeId = ( level > 0 ) ? NodeList.get( a.ReportsToId )+'.'+String.valueOf( j ) : String.valueOf( j );
                        masm.put( NodeID, new ObjectStructureMap( nodeID, levelFlag, closeFlag, nodeType, false, false, nomeOpp, idObjetoOpp ) );
                     	currentParent.add( idObjetoOpp );
                        nodeList.put( idObjetoOpp,nodeId );
                        nodeSortList.add( nodeId );
                	}*/
                }
                maxLevel.add( level );
                level++;
            }
        }
        
        //Account structure must now be formatted
        NodeSortList.sort();
        for( Integer i = 0; i < NodeSortList.size(); i++ ){
            List<String> pnl = new List<String> {};
            List<String> cnl = new List<String> {};
            List<String> nnl = new List<String> {};
            
            if ( i > 0 ){
                String pn   = NodeSortList[i-1];
                pnl     = pn.split( '\\.', -1 );
            }

            String cn   = NodeSortList[i];
            cnl     = cn.split( '\\.', -1 );

            if( i < NodeSortList.size()-1 ){
                String nn = NodeSortList[i+1];
                nnl = nn.split( '\\.', -1 );
            }
            
            ObjectStructureMap tasm = masm.get( cn );
            if ( cnl.size() < nnl.size() ){
                //Parent
                tasm.nodeType = ( isLastNode( cnl ) ) ? 'parent_end' : 'parent';
            }
            else if( cnl.size() > nnl.size() ){
                tasm.nodeType   = 'child_end';
                tasm.closeFlag   = setcloseFlag( cnl, nnl, tasm.nodeType );
            }
            else{
                tasm.nodeType = 'child';
            }
            
            tasm.levelFlag = setlevelFlag( cnl, tasm.nodeType ); 
            
            //Change below
            if ( tasm.idObjeto == currentId ) {
                tasm.currentNode = true;
            }
            system.debug('tasm = ' + tasm);
            asm.add( tasm );
        }
        
        asm[0].nodeType       = 'start';
        asm[asm.size()-1].nodeType   = 'end';
        
        Id previous;
        
        List<Integer> lstIndexRemove = new List<Integer>();
        
        for(integer i = 0; i < asm.size(); i++)
        {
          if(previous == asm[i].idObjeto) lstIndexRemove.add(i);
             previous = asm[i].idObjeto;            
        }
        
        for(integer i = 1; i <= lstIndexRemove.size(); i++)
        {
          asm.remove(lstIndexRemove[i - 1] - i);
        }
        
        return asm;
    }
    
    /**
    * Determin parent elements relationship to current element
    * @return flagList
    */
    public List<Boolean> setlevelFlag( List<String> nodeElements, String nodeType ){
      
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode   = '';
        String workNode   = '';
        Integer cn       = 0;
        
        for( Integer i = 0; i < nodeElements.size() - 1; i++ ){
            cn = Integer.valueOf( nodeElements[i] );
            cn++;
            searchNode   = workNode + String.valueOf( cn );
            workNode   = workNode + nodeElements[i] + '.';
            if ( masm.containsKey( searchNode ) ){
                flagList.add( true );
            }
            else {
                flagList.add( false );
            }
        }
        
        return flagList;
    }
    
    /**
    * Determin if the element is a closing element
    * @return flagList
    */
    public List<Boolean> setcloseFlag( List<String> cnl, List<String> nnl, String nodeType ){
      
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode   = '';
        String workNode   = '';
        Integer cn       = 0;
        
        for( Integer i = nnl.size(); i < cnl.size(); i++ ){
          flagList.add( true );
        }
        
        return flagList;
    }
    
    /**
    * Determin if Element is the bottom node  
    * @return Boolean
    */
    public Boolean isLastNode( List<String> nodeElements ){
      
        String searchNode   = '';
        Integer cn       = 0;
        
        for( Integer i = 0; i < nodeElements.size(); i++ ){
            if ( i == nodeElements.size()-1 ){
                cn = Integer.valueOf( nodeElements[i] );
                cn++;
                searchNode = searchNode + String.valueOf( cn );
            }
            else {
                searchNode = searchNode + nodeElements[i] + '.';
            }
        }
        if ( masm.containsKey( searchNode ) ){
            return false;
        }
        else{
            return true;
        }
    }
    
    /**
    * Find the tom most element in Hierarchy  
    * @return objId
    */
    public String GetTopElement( String objId ){
      
        Boolean top = false;
        while ( !top ) {
            //Change below
            Contact a = [ Select a.Id, a.ReportsToId From Contact a where a.Id =: objId limit 1 ];
            
            if ( a.ReportsToId != null ) {
                objId = a.ReportsToId;
            }
            else {
                top = true;
            }
        }
        return objId ;
    }
    
  /**
    * Wrapper class
    */
    public with sharing class ObjectStructureMap{

        public String nodeId;
        public Boolean[] levelFlag = new Boolean[]{};
        public Boolean[] closeFlag = new Boolean[]{};
        public String nodeType;
        public Boolean currentNode;
        
        /**
        * @Change this to your sObject
        */
        
        public String nome {get; set;}
        public Id idObjeto {get; set;}
        public String nomeOpp {get; set;}
        public Id idObjetoOpp {get; set;}
        public Contact ctt;
        //public Opportunity opp;
        
        public String getnodeId() { return nodeId; }
        public Boolean[] getlevelFlag() { return levelFlag; }
        public Boolean[] getcloseFlag() { return closeFlag; }
        public String getnodeType() { return nodeType; }
        public Boolean getcurrentNode() { return currentNode; }
        public String getnome() { return nome; }
        public Id getidObjeto() { return idObjeto; }


        /**
        * @Change this to your sObject
        */
        
        //public Contact getcontact() { return ctt; }
        //public Opportunity getopportunity() { return opp; }
        
        public void setnodeId( String n ) { this.nodeId = n; }
        public void setlevelFlag( Boolean l ) { this.levelFlag.add(l); }
        public void setlcloseFlag( Boolean l ) { this.closeFlag.add(l); }
        public void setnodeType( String nt ) { this.nodeType = nt; }
        public void setcurrentNode( Boolean cn ) { this.currentNode = cn; }
        public void setnome(String nome) { this.nome = nome;}
        public void setidObjeto(Id idObjeto) { this.idObjeto = idObjeto; }

        /**
        * @Change this to your sObject
        */
        
        //public void setcontact( Contact a ) { this.ctt = a; }
        //public void setopportunity( Opportunity a ) { this.opp = a; }

        /**
        * @Change the parameters to your sObject
        */
        public ObjectStructureMap( String nodeId, Boolean[] levelFlag,Boolean[] closeFlag , String nodeType, Boolean lastNode, Boolean currentNode, String nome, Id idObjeto){
            
            this.nodeId         = nodeId;
            this.levelFlag      = levelFlag; 
            this.closeFlag      = closeFlag;
            this.nodeType       = nodeType;
            this.currentNode    = currentNode;
            this.nome			= nome;
            this.idObjeto		= idObjeto;

            //Change this to your sObject  
            //this.nome = nome;
            //this.idObjeto = idObjeto;
        }
    }
}