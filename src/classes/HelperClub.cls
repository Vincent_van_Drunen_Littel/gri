public class HelperClub {
    public static String RE_BRAZIL = 'RE Club Brazil'; // 'Infra Club LatAm' => RT_CTC_INFRACLUB,
    public static String RE_EUROPE = 'RE Club Europe'; // 'RE Club Europe' => RT_CTC_EUROPEANCLUB,
    public static String INFRA_LATAM = 'Infra Club LatAm'; //'Infra Club LatAm' => RT_CTC_INFRACLUB,
    public static String INFRA_INDIA = 'Infra Club India'; // 'Infra Club India' => RT_CTC_INFRACLUBINDIA,
    public static String RE_INDIA = 'RE Club India'; // 'RE Club India' => RT_CTC_INDIACLUB,
    public static String RE_LATAM = 'RE Club LatAm';
    public static String RETAIL_BRAZIL = 'Retail Club Brazil';
    public static String RE_CLUB_AFRICA = 'RE Club Africa';

    public static void dummy_method()
    {
    	Contact newContact = new Contact(LastName='A LastName');
    }
}