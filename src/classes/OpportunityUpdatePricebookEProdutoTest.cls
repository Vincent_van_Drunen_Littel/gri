/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste da classe "OpportunityUpdatePricebookEProduto".
*
* NAME: OpportunityUpdatePricebookEProdutoTest.cls
* AUTHOR: RVR                                                  DATE: 14/04/2016
* MODIFIER: LMdO                                               DATE: 04/07/2016
*******************************************************************************/
@IsTest
private class OpportunityUpdatePricebookEProdutoTest  
{
	private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
  private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
  private static Account acc;
  private static Contact ctt;
  private static Event__c evento;
  private static Pricebook2 catalogo2, catalogo3;
  private static List<Pricebook2> lstPbok;
  private static Product2 produto;
  private static Id catalogo;
  private static PricebookEntry pbeStandard, pbe2, pbe3;
  private static list<PricebookEntry> lstPbe;
  private static Opportunity opp;
  private static OpportunityLineItem oppLI;
  private static Application_Config__c appCon;
  
  static
  {
    acc = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(acc);
      
    //=== Custom Setting ===  
    appCon = new Application_Config__c(Name = 'BucketUserId', Value__c = ID_ACCOUNT);
    insert appCon;   

    ctt = SObjectInstance.createContato(acc.Id);
    Database.insert(ctt);

    evento = SObjectInstance.createEvent();
    evento.Type__c = 'Open Event';
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    catalogo2 = SObjectInstance.catalogoDePreco();
    catalogo2.Name = 'Teste';
    catalogo2.Event__c = evento.Id;
    catalogo2.IsActive = true;
    catalogo2.Starting_Date__c = system.today();
    catalogo2.End_Date__c = system.today()+5;

    catalogo3 = SObjectInstance.catalogoDePreco();
    catalogo3.Name = 'Teste';
    catalogo3.Event__c = evento.Id;
    catalogo3.IsActive = true;
    catalogo3.Starting_Date__c = system.today()+6;
    catalogo3.End_Date__c = system.today()+22;

    lstPbok = new List<Pricebook2>{catalogo2, catalogo3};
    Database.insert(lstPbok);

    produto = SObjectInstance.createProduct2();
    produto.IsActive = true;
    Database.insert(produto);    

    catalogo = SObjectInstance.catalogoDePrecoPadrao2();

    pbeStandard = SObjectInstance.entradaDePreco(catalogo, produto.Id);
    pbeStandard.Product2Id = produto.Id;
    pbe2 = SObjectInstance.entradaDePreco(catalogo2.Id, produto.Id);
    pbe2.Product2Id = produto.Id;
    pbe3 = SObjectInstance.entradaDePreco(catalogo3.Id, produto.Id);
    pbe3.Product2Id = produto.Id;
    lstPbe = new list<PricebookEntry>{pbeStandard, pbe2, pbe3};
    Database.insert(lstPbe);

    OpportunityVinculaPricebook.exec = true;
    OpportunityUpdatePricebookEProduto.exec = true;

    opp = SObjectInstance.createOpportunidade(acc.Id, REC_OPP_OPEN_EVENTS);
    opp.Name = 'Teste Cloud2b opp';
    opp.Contact__c = ctt.Id;
    opp.Event__c = evento.Id;
    opp.StageName = 'Prospect';
    opp.Description = 'teste';
    opp.Pricebook2Id = pbe2.Pricebook2Id;
    opp.Approval_Process__c = '1 - Approved';
    Database.insert(opp);
    
    oppLI = SObjectInstance.createProdutoOportunidade( opp.Id, pbe2.Id );
    Database.insert(oppLI);

    oppLI = [SELECT Id, PricebookEntry.Product2Id, Opportunity.Trigger_Is_Runnig__c,
    PricebookEntry.Pricebook2Id, Quantity, UnitPrice, TotalPrice, PricebookEntryId, OpportunityId, ListPrice, Opportunity.Approval_Process__c,
    Opportunity.Event__c, Opportunity.CloseDate, Opportunity.Pricebook_Changed__c, Opportunity.StageName, Discount
    FROM OpportunityLineItem
    WHERE OpportunityId =: opp.Id];
  }

  static testMethod void  testeFuncional()
  {
    Test.startTest();
      OpportunityUpdatePricebookEProduto.exec = false;
      opp.Pricebook_Changed__c  = true;
      opp.CloseDate = system.today()+15;
      Database.update(opp);

      opp = [Select Id, Name, Contact__c, AccountId, Event__c, CloseDate, StageName, Description,
      RecordTypeId, Pricebook2Id, Pricebook_Changed__c
      from Opportunity
      where Id =: opp.Id];
    Test.stopTest();
  }

  static testMethod void  testeFuncionalListEmpty()
  {
    Test.startTest();
      OpportunityUpdatePricebookEProduto.exec = false;
      Database.delete(oppLI);

      opp.Pricebook_Changed__c  = true;
      opp.CloseDate = system.today()+15;
      Database.update(opp);
    Test.stopTest();
  }

}