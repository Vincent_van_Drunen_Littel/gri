/**
 * Created by Eric Bueno on 23/02/2019.
 */

public without sharing class CampaignMemberBO {

    private static final CampaignMemberBO instance = new CampaignMemberBO();
    private static final CampaignMemberDAO dao = CampaignMemberDAO.getInstance();
    private CampaignMemberBO(){}
    public static CampaignMemberBO getInstance(){
        return instance;
    }

    public void assignCampaignMemberToOpportunity (List<CampaignMember> listCampaignMemberNew){

        // Instances
        List<CampaignMember> listCampaignMemberContact = new List<CampaignMember>();
        List<CampaignMember> listCampaignMemberLead = new List<CampaignMember>();
        List<CampaignMember> listCampaignMemberForUpdate = new List<CampaignMember>();
        List<Opportunity> listOpportunityForUpdate = new List<Opportunity>();

        Set<Id> setCampaignMemberId = new Set<Id>();
        Set<Id> setContactId = new Set<Id>();
        Map<Id,Id> mapOwnerIdByCampaignMemberId = new Map<Id, Id>();

        // Recupera o Id os membros da campanha
        for(CampaignMember campaignMember : listCampaignMemberNew){
            setCampaignMemberId.add(campaignMember.Id);
        }

        // Verifica se eh uma campanha de funil digital e remove da lista da trigger os q nao atendem
        List<CampaignMember> listCampaignMemberCriteria = dao.findCampaignMemberFromDigitalCampaignByCampaignMemberId(setCampaignMemberId);

        // Verifica se algum membro da campanha atende aos criterios
        if(listCampaignMemberCriteria.isEmpty()){
            return;
        }

        // Separa os membros que sao Contatos e os que sao Leads
        for(CampaignMember campaignMember : listCampaignMemberNew){

            Boolean meetCriteria = false;
            Id ownerId;
            CampaignMember campaignMember2;

            for(CampaignMember campaignMemberMeetCriteria : listCampaignMemberCriteria){

                if(campaignMemberMeetCriteria.Id == campaignMember.Id){
                    meetCriteria = true;
                    ownerId = campaignMember.Campaign.DigitalCampaignEvent__r.DigitalCampaignOpportunityOwner__c;
                    campaignMember2 = campaignMemberMeetCriteria;
                    break;
                }

            }

            if(!meetCriteria){
                continue;
            }

            // Verifica se eh um contato
            if(campaignMember.ContactId != null){
                listCampaignMemberContact.add(campaignMember2);
            }


            // Verifica se eh um Lead
            if(campaignMember.LeadId != null && campaignMember.ContactId == null){
                listCampaignMemberLead.add(campaignMember);
            }

            mapOwnerIdByCampaignMemberId.put(campaignMember.Id, ownerId);

        }

        // Verifica se existe Membro da campanha que atenderam aos criterios
        if(listCampaignMemberContact.isEmpty() && listCampaignMemberLead.isEmpty()){
            return;
        }

        // Converte os leads dos membros da campanha
        if(!listCampaignMemberLead.isEmpty()) {
            // Converte os Leads
            this.convertLead(listCampaignMemberLead, mapOwnerIdByCampaignMemberId);
        }

        // Verifica se existe membros que sao contatos
        if(listCampaignMemberContact.isEmpty()){
            return;
        }

        // Popula set
        for(CampaignMember campaignMember : listCampaignMemberContact){
            setContactId.add(campaignMember.ContactId);
        }

        // Busca as oportunidades que tem eventos de campanha relacionados
        List<Opportunity> listOpportunities = dao.findOpportunityByContactId(setContactId);

        // Reutiliza o set
        setCampaignMemberId.clear();

        // Separa as oportunidades por contato
        // Membros da campanha com contatos e oportunidades
        for(CampaignMember campaignMember : listCampaignMemberContact){

            Boolean hasOpportunity = false;

            for(Opportunity opportunity : listOpportunities){

                // Verifica se eh uma oportunidade desse membro da campanha e tambem do evento da campanha
                if( CampaignMember.ContactId != Opportunity.Contact__c
                        || CampaignMember.Campaign.DigitalCampaignEvent__c != Opportunity.Event__c
                        || CampaignMember.Campaign.Opportunity_Type__c != Opportunity.RecordType.DeveloperName )
                {

                    continue;
                }

                hasOpportunity = true;

                // Recupera a campanha do membro da campanha
                Campaign campaign = campaignMember.Campaign;

                if(campaignMember.Opportunity__c != opportunity.Id){
                    // Atualiza a oportunidade do membro
                    campaignMember.Opportunity__c = opportunity.Id;
                    listCampaignMemberForUpdate.add(campaignMember);
                }

                // Guarda o digital stage anterior
                String actualDigitalStage = opportunity.Digital_Funnel_Stage__c;

                // Atualiza o digital stage
                OpportunityBO.getInstance().updateDigitalStageOpportunityCriteria(opportunity,campaign);

                // Verifica se alterou o digital stage e atualiza a oportunidade se necessario
                if(actualDigitalStage != opportunity.Digital_Funnel_Stage__c){
                    listOpportunityForUpdate.add(opportunity);
                }
            }

            // Cria a oportunidade se nao houver
            if(!hasOpportunity){
                // Armazena o id do membro da campanha para criar as oportunidades via metodo futuro
                setCampaignMemberId.add(campaignMember.Id);
            }

        }

        // Atualiza as oportunidades
        if(!listOpportunityForUpdate.isEmpty()){
            update listOpportunityForUpdate;
        }

        // Atualiza os membros da campanha
        if(!listCampaignMemberForUpdate.isEmpty()){
            update listCampaignMemberForUpdate;
        }

        // Cria as oportunidades
        if(!setCampaignMemberId.isEmpty()){
            // Crias as oportunidades futuramente
            createOpportunityFuture(setCampaignMemberId, false);
        }
    }

    @future
    public static void createOpportunityFuture(Set<Id> listCampaignMemberId, Boolean createTask)
    {
        // Intancias
        List<Opportunity> listOpportunities = new List<Opportunity>();
        List<Task> listTask = new List<Task>();
        User userCRMTeam;

        // Busca o usuario do time de CRM para as tarefas
        List<User> listUser = dao.findUserCRM();
        if(listUser.isEmpty()){
            // TODO: throw Exception
        }
        userCRMTeam = listUser[0];

        // Busca os membros da campanha
        List<CampaignMember> listCampaignMembers = dao.findCampaignMemberFromDigitalCampaignByCampaignMemberId(listCampaignMemberId);

        // Geracao da oportunidade e atualizacao do digital stage
        for(CampaignMember campaignMember : listCampaignMembers){
            Opportunity opportunity = createOpportunity(campaignMember);
            OpportunityBO.getInstance().updateDigitalStageOpportunityCriteria(opportunity,campaignMember.Campaign);
            listOpportunities.add(opportunity);
        }

        insert listOpportunities;

        // Geracao da tarefa
        if(createTask == true){
        for(Opportunity opportunity : listOpportunities){
            listTask.add(createTask(opportunity, userCRMTeam));
        }
            insert listTask;
        }

        // Atualiza o membro da campanha com a oportunidade
        List<CampaignMember> listCampaignMembersForUpdate = new List<CampaignMember>();
        for(CampaignMember campaignMember : listCampaignMembers){
            for(Opportunity opportunity : listOpportunities){

                if(opportunity.Contact__c == campaignMember.ContactId){
                    campaignMember.Opportunity__c = opportunity.Id;
                    listCampaignMembersForUpdate.add(campaignMember);
                    break;
                }
            }
        }

        if(!listCampaignMembersForUpdate.isEmpty()){
            update listCampaignMembersForUpdate;
        }

    }

    private static Opportunity createOpportunity(CampaignMember campaignMember){

        Opportunity opportunity = new Opportunity(
                Name = campaignMember.Contact.Name + ' - ' + campaignMember.Campaign.Name,
                CloseDate = System.today(),
                OwnerId = campaignMember.Campaign.DigitalCampaignEvent__r.DigitalCampaignOpportunityOwner__c,
                Event__c = campaignMember.Campaign.DigitalCampaignEvent__c,
                Contact__c = campaignMember.ContactId,
                AccountId = campaignMember.Contact.AccountId,
                CampaignId = campaignMember.CampaignId,
                StageName = 'Prospect',
                CurrencyIsoCode = campaignMember.Campaign.DigitalCampaignEvent__r.CurrencyIsoCode,
                RecordTypeId = RecordTypeMemory.getRecType('Opportunity',campaignMember.Campaign.Opportunity_Type__c),
                LeadSource = campaignMember.Lead.LeadSource,
                Club__c = campaignMember.Campaign.Club__c,
                Magazine__c = campaignMember.Campaign.Magazine__c
        );



        return opportunity;
    }

    private static task createTask(Opportunity opportunity, User userCRMTeam){
        Task task = new Task(
                Subject = 'Converted Lead',
                Description = 'Please update and research this Contact and Account',
                WhoId = opportunity.Contact__c,
                OwnerId = userCRMTeam.Id,
                WhatId = opportunity.Id,
                ActivityDate = System.today()+5

        );
        return task;
    }


    private void convertLead(List<CampaignMember> listCampaignMemberLead, Map<Id,Id> mapOwnerIdByCampaignMemberId){

        // Instancias
        Map<Id,CampaignMember> mapCampaignMemberByContactId = new Map<Id,CampaignMember>();
        Map<Id,Lead> mapLeadById = new Map<Id,Lead>();
        Map<String,Id> mapAccountIdByName = new Map<String,Id>();
        Map<Id,Boolean> mapLeadConvertResultsPorLeadId = new Map<Id,Boolean>();
        Set<Id> setCampaignMemberId = new Set<Id>();
        Set<Id> setLeadId = new Set<Id>();
        Set<String> setNameCompany = new Set<String>();
        List<Database.LeadConvert> listLeadConvert = new List<Database.LeadConvert>();

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];

        // Popula set
        for(CampaignMember campaignMember : listCampaignMemberLead){
            setLeadId.add(campaignMember.LeadId);
        }

        // Busca os Leads e guarda o nome da empresa
        for(Lead lead : dao.findLeadById(setLeadId)){
            setNameCompany.add(lead.Company);
            mapLeadById.put(lead.Id, lead);
        }

        // Busca as contas pelo nome da empresa e armazena em um mapa
        for(Account account : dao.findAccountByName(setNameCompany)){
            mapAccountIdByName.put(account.Name, account.Id);
        }

        // Cria o conversor de Lead
        for(CampaignMember campaignMember : listCampaignMemberLead){

            // Cria o conversor de Lead
            Database.LeadConvert lc = new database.LeadConvert();

            // Informa para nao criar a oportunidade
            lc.setDoNotCreateOpportunity(true);

            // Informa o lead
            lc.setLeadId(campaignMember.LeadId);
            lc.setConvertedStatus(convertStatus.MasterLabel);

            // Seta a conta
            Lead lead = mapLeadById.get(campaignMember.LeadId);
            Id accountId = mapAccountIdByName.get(lead.Company);
            if(accountId != null){
                lc.setAccountId(accountId);
            }

            // Recupera o owner da campanha
            lc.setOwnerId(mapOwnerIdByCampaignMemberId.get(campaignMember.Id));

            listLeadConvert.add(lc);
            setCampaignMemberId.add(campaignMember.Id);

        }

        // Converte os Leads
        List<Lead> listLeadWithError = new List<Lead>();
        List<Database.LeadConvertResult> listLeadConvertResults = Database.convertLead(listLeadConvert);

        // Verifica os resultados das conversoes
        for(Database.LeadConvertResult lcr : listLeadConvertResults){

            // Verifica se o lead nao foi convertido
            if(!lcr.isSuccess()){
                Lead lead = new Lead();
                lead.Id = lcr.LeadId;
                lead.HasErrorConversion__c = true;
                listLeadWithError.add(lead);
            }

            // Recupera o status da conversao
            mapLeadConvertResultsPorLeadId.put(lcr.leadId,lcr.isSuccess());

        }

        // Atualiza os leads com erro
        if(!listLeadWithError.isEmpty()){
            update listLeadWithError;
        }

        // Busca os membros da campanha que foram convertidos para contato
        List<CampaignMember> listCampaignMember = dao.findCampaignMemberFromDigitalCampaignByCampaignMemberId(setCampaignMemberId);

//        List<Opportunity> listOpportunities = new List<Opportunity>();
        List<Task> listTasks = new List<Task>();
        Set<Id> setCampaignMembersId = new Set<Id>();

        // Atualiza a oportunidade e o membro da campanha
        for (CampaignMember campaignMember : listCampaignMember) {

            // Verifica se o Lead foi convertido com sucesso e cria a oportunidade
            if(mapLeadConvertResultsPorLeadId.get(campaignMember.LeadId)){
                // Armazena os membros da campanha para criar as oportunidades via metodo futuro
                setCampaignMembersId.add(campaignMember.Id);
            }

        }

        // Crias as oportunidades futuramente
        if(!setCampaignMembersId.isEmpty()){
            createOpportunityFuture(setCampaignMembersId, true);
        }
    }

}