/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe teste da classe EventSumOrderAndOpp
*
*
* NAME: EventSumOrderAndOppTest.cls
* AUTHOR: VMDL                                                 DATE: 12/08/2016
*******************************************************************************/
@isTest
private class EventSumOrderAndOppTest {



    private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
    private static Id REC_OPP_SPEX = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );
    private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
    private static Id REC_ORD_ADD_PASSES = RecordTypeMemory.getRecType( 'Order', 'Aditional_Passes' );
    
    static testMethod void testeFuncionalUpdateOpp()
    {
    EventSumOrderAndOpp testCont = new EventSumOrderAndOpp();
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = SObjectInstance.createProduct2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Negotiation';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        Test.startTest();
            oportunidade.StageName = 'Approved';
            Database.update(oportunidade);
        Test.stopTest();
    }

    static testMethod void testeFuncionalDeleteOpp()
    {
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = SObjectInstance.createProduct2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Approved';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        Test.startTest();
            Database.delete(oportunidade);
        Test.stopTest();
    }

    static testMethod void testeFuncionalUpdateOrder()
    {
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = SObjectInstance.createProduct2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_SPEX);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Negotiation';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        Order lOrder = SObjectInstance.createorder(REC_ORD_ADD_PASSES, conta.Id, idCatalogoPreco, evento.Id);
        lOrder.OpportunityId = oportunidade.Id;
        Database.insert(lOrder);

        OrderItem lOrderItens = SObjectInstance.createOrderItens(lOrder.Id, entradaDePreco.Id);
        Database.insert( lOrderItens );

        Test.startTest();
            lOrderItens.Quantity = 2;
            Database.update(lOrderItens);

            lOrder = [Select id, RecordTypeId, Name, OpportunityId, AccountId, Event__c, EffectiveDate,
            Status, Pricebook2Id, Quantity_Sum__c, Sum_Amount__c
            from Order
            Where Id =: lOrder.Id ];

        Test.stopTest();
    }

    static testMethod void testeFuncionalDeleteOrder()
    {
        Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();

        Product2 produto = SObjectInstance.createProduct2();
        produto.NAME = 'Teste prod';
        Database.insert(produto);

        PricebookEntry entradaDePreco = SObjectInstance.entradaDePreco(idCatalogoPreco, produto.Id);
        Database.insert(entradaDePreco);

        Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
        Database.insert(conta);

        Contact contato = SObjectInstance.createContato(conta.Id);
        Database.insert(contato);

        Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

        Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_SPEX);
        oportunidade.Contact__c = contato.Id;
        oportunidade.StageName = 'Negotiation';
        oportunidade.Event__c = evento.Id;
        Database.insert(oportunidade);

        OpportunityLineItem produtoOpp = SObjectInstance.createProdutoOportunidade(oportunidade.Id, entradaDePreco.Id);
        Database.insert(produtoOpp);

        Order lOrder = SObjectInstance.createorder(REC_ORD_ADD_PASSES, conta.Id, idCatalogoPreco, evento.Id);
        lOrder.OpportunityId = oportunidade.Id;
        Database.insert(lOrder);

        OrderItem lOrderItens = SObjectInstance.createOrderItens(lOrder.Id, entradaDePreco.Id);
        Database.insert( lOrderItens );

        Test.startTest();
            Database.delete(lOrder);
        Test.stopTest();
    }

}