global class EmailReport_ComparisonReportGen_Step1 implements Schedulable {
	global void execute(SchedulableContext sc) {
		
		EmailReport_ComparisonReportGen_Helper comparisonReport = new EmailReport_ComparisonReportGen_Helper();

		comparisonReport.queryClubComparison();

		List<EmailReport__c> emailReport = [SELECT Id, Report_Type__c 
											FROM EmailReport__c 
											WHERE Report_Date__c = TODAY AND Report_Type__c = 'Comparison'];

		EmailReport__c report;

		if(emailReport.Size() > 0)
		{
			report = emailReport[0];
		}
		else
		{
			report = new EmailReport__c(Report_Date__c = Date.Today(), Report_Type__c = 'Comparison', Report_Week__c = System.Now().Format('w'));
			insert report;
		}

		List<Attachment> reportAttachments = new List<Attachment>();

		reportAttachments.add(new Attachment(Name = 'ClubComparison.txt', Body = Blob.valueOf(JSON.serialize(comparisonReport.club)), ParentId = report.Id));

		insert reportAttachments;
	}
}