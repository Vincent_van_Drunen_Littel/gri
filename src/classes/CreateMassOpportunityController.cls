/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe para criação de Oportunidades de uma única vez através do obj Cmpanha
*
* NAME: CreateMassOpportunityController.cls
* AUTHOR: KHPS                                                 DATE: 14/04/2016
* MAINTENANCE: AFC                                             DATE: 27/06/2016
*******************************************************************************/
public class CreateMassOpportunityController
{
  @TestVisible private static final Id recOppMagazineAD = RecordTypeMemory.getRecType('Opportunity', 'Magazine');
  //private static final Id recOppMagazineSub = RecordTypeMemory.getRecType('Opportunity', 'Magazine_Subscriptions');
  @TestVisible private static final Id recOppMembership = RecordTypeMemory.getRecType('Opportunity', 'Membership');
  @TestVisible private static final Id recOppOpenEvents = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  @TestVisible private static final Id recOppUnique = RecordTypeMemory.getRecType('Opportunity', 'SPEX');

  public class OpportunityWrapper
  {
    public Id oppRecType {get; set;}
    public String oppName {get; set;}
    public String campType {get; set;}
    public String campStatus {get; set;}
    public String campTitle {get; set;}
    public String campContact {get; set;}
    public String campAccount {get; set;}
    public String campId {get; set;}
    public String oppAccount {get; set;}
    public String campMoeda {get; set;}
    public String oppUnique {get; set;}
    public CampaignMember campMember {get; set;}

    public OpportunityWrapper(Id oppRecType)
    {
      this.oppRecType = oppRecType;
    }
  }

  public class ProductWrapper
  {
    public Product2 prod {get; set;}
    public Boolean IsSelected {get; set;}
    public Integer Quantity {get; set;}
    public Double UnitPrice {get; set;}
    public String ListPrice {get; set;}

    public ProductWrapper(Product2 prod)
    {
      this.prod = prod;
      this.IsSelected = false;
    }
  }

  public Boolean loadPopup {get; set;}
  public String busca {get; set;}
  public Integer qtdProduct {get; set;}
//  public Integer qdtOpportunity {get { return lstOppWrapper.size(); } }
  public Integer qdtOpportunity {get; set;}
  public Opportunity opp {get; set;}
  public Campaign camp {get; set;}
//  public String oppCampType {get; set;}
  public String oppCampType {get { return this.camp.Opportunity_Type__c; } }
  public String oppStatus {get; set;}
  public String dateSelected {get; set;}
  private Id campId;
  public list<CampaignMember> lstCampMember;

  public List<SelectOption> getTypeOppCamp
  {
    get
    {
      List<SelectOption> lstOptions = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Campaign.Opportunity_Type__c.getDescribe();
      List<Schema.PicklistEntry> lstPickListEntry = fieldResult.getPicklistValues();

      for(Schema.PicklistEntry ple : lstPickListEntry)
      {
        lstOptions.add(new SelectOption(ple.getLabel(), ple.getValue()));
      }
      return lstOptions;
    }
  }
  public List<SelectOption> getStatusOpp
  {
    get
    {
      List<SelectOption> lstOptionsOpp = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
      List<Schema.PicklistEntry> lstPickListEntry = fieldResult.getPicklistValues();

      for(Schema.PicklistEntry ple : lstPickListEntry)
      {
        lstOptionsOpp.add(new SelectOption(ple.getLabel(), ple.getValue()));
      }
      return lstOptionsOpp;
    }
  }
  public List<SelectOption> getCurrencyOpp
  {
    get
    {
      List<SelectOption> lstCurrencyOpp = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Opportunity.CurrencyIsoCode.getDescribe();
      List<Schema.PicklistEntry> lstPickListEntry = fieldResult.getPicklistValues();

      for(Schema.PicklistEntry ple : lstPickListEntry)
      {
        lstCurrencyOpp.add(new SelectOption(ple.getValue(), ple.getValue()));
      }
      return lstCurrencyOpp;
    }
  }
  public list<OpportunityWrapper> lstOppWrapper {get; set;}
  public list<ProductWrapper> lstProdutoWpp {get; set;}

  public CreateMassOpportunityController(ApexPages.StandardController controller)
  {
    this.dateSelected = String.valueOf(System.today());
    this.campId = controller.getId();
    this.qtdProduct = 0;
    this.loadPopup = false;

    list<Campaign> lstCamp = [SELECT Id, Product__c, Opportunity_Type__c, Event__c, Magazine__c, Club__c, CurrencyIsoCode
      FROM Campaign WHERE Id = :campId limit 1];
    this.camp = lstCamp[0];

    this.opp = new Opportunity();
    this.opp.CloseDate = System.today();
    this.opp.Date_of_actual_status__c = System.today();
    this.opp.Date_of_the_next_step__c = System.today();
    this.opp.StageName = 'Suspect';
    this.opp.Magazine__c = this.camp.Magazine__c;
    this.opp.CurrencyIsoCode = this.camp.CurrencyIsoCode;
    this.opp.Event__c = this.camp.Event__c;
    this.opp.Club__c = this.camp.Club__c;

    SelectDate();
    if(String.isNotBlank(this.opp.Event__c) || String.isNotBlank(this.opp.Magazine__c)) this.lstOppWrapper = fillOppWrapper(this.campId);
    if(String.isNotBlank(this.opp.Club__c)) this.lstOppWrapper = fillOppWrapperClub(this.campId);
    if(this.qdtOpportunity == 0) adicionaAviso('Some Contacts already have Opportunities with this conditions!');
  }

  public void SelectDate()
  {
    dateSelected = String.valueOf(this.opp.CloseDate);
    SelecionaPricebook();
    CarregaProdutos();
  }

  private list<OpportunityWrapper> fillOppWrapperClub(Id aCampId)
  {
    list<Opportunity> lstOppControle = new list<Opportunity>();
    list<OpportunityWrapper> lOppsWrapper = new list<OpportunityWrapper>();

    list<AggregateResult> lLstAggr = [select CampaignId, count(Id) Qtd from CampaignMember 
      where Opportunity_Created__c = false AND ContactId != null and CampaignId = :aCampId group by CampaignId];

    this.qdtOpportunity = (lLstAggr.isEmpty()) ? 0 : (Integer)lLstAggr[0].get('Qtd');

    this.lstCampMember = [SELECT Id, ContactId, Status, CampaignId, CurrencyIsoCode,
      Campaign.Opportunity_Type__c, Opportunity_Created__c, Campaign.Club__c,
      Contact.FirstName, Contact.LastName, Campaign.Event__c, Contact.Id,
      Contact.Job_title_badge__c, Contact.AccountId, Contact.Account.Name,
      Campaign.Magazine__r.Name, Campaign.Club__r.Name, Campaign.Event__r.Name
      FROM CampaignMember
      WHERE CampaignId = :aCampId
      AND ContactId != null
      AND Opportunity_Created__c != true
      limit 1];

    if (this.lstCampMember.isEmpty()) return new list<OpportunityWrapper>();
/*
    set<String> setIdClubMember = new set<String>(), setIdContactMember = new set<String>();
    for(CampaignMember iCampMember :this.lstCampMember)
    {
      if(String.isBlank(iCampMember.Campaign.Club__c)) continue;

      setIdClubMember.add(iCampMember.Campaign.Club__c);
      setIdContactMember.add(iCampMember.Contact.Id);
    }

    for(Opportunity iOpp : [SELECT Id, Name, Event__r.Name, Magazine__r.Name, Club__r.Name, RecordTypeId,
                            Contact__c, CampaignId, CurrencyIsoCode, opp_unique__c
                            ,Club__c, IsClosed, StageName
                            FROM Opportunity
                            where Club__c =: setIdClubMember
                            And Contact__c =: setIdContactMember
                            limit 200])
    {
      lstOppControle.add(iOpp);
    }
*/
/*
// RLdO - trecho inutil
    //Se o membro da Campanha não possuir nenhuma oportunidade de membership para o Club informado na Campanha
    if(lstOppControle.isEmpty())
    {
      for (CampaignMember campMember: this.lstCampMember)
      {
        Id oppRecType = recOppMembership;
        String oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
          campMember.Contact.Account.Name;

        OpportunityWrapper oppWrapper = new OpportunityWrapper(oppRecType);
        oppWrapper.oppName = oppName;
        oppWrapper.campType = 'Contact';
        oppWrapper.campStatus = campMember.Status;
        oppWrapper.campTitle = campMember.Contact.Job_title_badge__c;
        oppWrapper.campContact = campMember.ContactId;
        oppWrapper.campAccount = campMember.Contact.Account.Name;
        oppWrapper.campId = campMember.CampaignId;
        oppWrapper.campMoeda = campMember.CurrencyIsoCode;
        oppWrapper.oppAccount = campMember.Contact.AccountId;
        oppWrapper.campMember = campMember;

        lOppsWrapper.add(oppWrapper);
      }
      return lOppsWrapper;
    }
    //Se o membro da Campanha não possuir nenhuma oportunidade de membership para o Club informado na Campanha
*/
    for (CampaignMember campMember : this.lstCampMember)
    {
      Id OppIdRec = RecordTypeMemory.getRecType('Opportunity', campMember.Campaign.Opportunity_Type__c);
/*
// este trecho devera ser levado para uma trigger ("regra de validacao" impedindo criacao)
      for(Opportunity opp : lstOppControle)
      {
        //Se o membro da Campanha possuir uma oportunidade aberta do Club informado na Campanha, a Opp não será criada.
        if(opp.RecordTypeId == OppIdRec &&
           campMember.ContactId == opp.Contact__c &&
           campMember.Campaign.Club__r.Name == opp.Club__r.Name &&
           opp.IsClosed == false)
        {
          adicionaAviso('Some Contacts already have Opportunities with this conditions!');
          campMember.Opportunity_Created__c = true;
        }
      }

      if(campMember.Opportunity_Created__c) continue;
*/
      Id oppRecType = recOppMembership;
      String oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
        campMember.Contact.Account.Name;

      OpportunityWrapper oppWrapper = new OpportunityWrapper(oppRecType);
      oppWrapper.oppName = oppName;
      oppWrapper.campType = 'Contact';
      oppWrapper.campStatus = campMember.Status;
      oppWrapper.campTitle = campMember.Contact.Job_title_badge__c;
      oppWrapper.campContact = campMember.ContactId;
      oppWrapper.campAccount = campMember.Contact.Account.Name;
      oppWrapper.campId = campMember.CampaignId;
      oppWrapper.campMoeda = campMember.CurrencyIsoCode;
      oppWrapper.oppAccount = campMember.Contact.AccountId;
      oppWrapper.campMember = campMember;

      lOppsWrapper.add(oppWrapper);
    }
    return lOppsWrapper;
  }

  private list<OpportunityWrapper> fillOppWrapper(Id aCampId)
  {
    list<Opportunity> lstOppControle = new list<Opportunity>();
    list<OpportunityWrapper> lOppsWrapper = new list<OpportunityWrapper>();

    list<AggregateResult> lLstAggr = [select CampaignId, count(Id) Qtd from CampaignMember 
      where Opportunity_Created__c = false AND ContactId != null and CampaignId = :aCampId group by CampaignId];

    this.qdtOpportunity = (lLstAggr.isEmpty()) ? 0 : (Integer)lLstAggr[0].get('Qtd');

    this.lstCampMember = [SELECT Id, ContactId, Status, CampaignId, CurrencyIsoCode,
      Campaign.Opportunity_Type__c, Opportunity_Created__c, Campaign.Club__c,
      Contact.FirstName, Contact.LastName, Campaign.Event__c, Contact.Id,
      Contact.Job_title_badge__c, Contact.AccountId, Contact.Account.Name,
      Campaign.Magazine__r.Name, Campaign.Club__r.Name, Campaign.Event__r.Name
      FROM CampaignMember
      WHERE CampaignId = :aCampId
      AND ContactId != null
      AND Opportunity_Created__c != true
      limit 1];
/*
    if (this.lstCampMember.isEmpty()) return new list<OpportunityWrapper>();

    set<String> setkeyOppUnique = new set<String>();
    for(CampaignMember iCampMB : this.lstCampMember)
    {
      String chaveOpp = iCampMB.ContactId+' - '+iCampMB.Contact.AccountId+' - '+iCampMB.Campaign.Event__c;
      setkeyOppUnique.add(chaveOpp);
    }

    for(Opportunity iOpp : [SELECT Id, Name, Event__r.Name, Magazine__r.Name, Club__r.Name, RecordTypeId,
                            Contact__c, CampaignId, CurrencyIsoCode, opp_unique__c
                            FROM Opportunity
                            where opp_unique__c =: setkeyOppUnique
                            limit 200])
    {
      lstOppControle.add(iOpp);
    }
*/
/*
// RLdO - trecho inutil
    if(lstOppControle.isEmpty())
    {
      for(CampaignMember campMember : this.lstCampMember)
      {
        Id oppRecType = recOppMagazineAD;
        String oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName;
        String chaveOpp = campMember.ContactId+' - '+campMember.Contact.AccountId+' - '+campMember.Campaign.Event__c;

        if(oppCampType == 'Magazine')
        {
          oppRecType = recOppMagazineAD;
          oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
            campMember.Contact.Account.Name;
        }
        else if(oppCampType == 'Membership')
        {
          oppRecType = recOppMembership;
          oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
            campMember.Contact.Account.Name;
        }
        else if(oppCampType == 'Open_Events')
        {
          oppRecType = recOppOpenEvents;
          oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
            campMember.Contact.Account.Name;
        }
        else if(oppCampType == 'SPEX')
        {
          oppRecType = recOppUnique;
          oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
            campMember.Contact.Account.Name;
        }

        OpportunityWrapper oppWrapper = new OpportunityWrapper(oppRecType);
        oppWrapper.oppName = oppName;
        oppWrapper.campType = 'Contact';
        oppWrapper.campStatus = campMember.Status;
        oppWrapper.campTitle = campMember.Contact.Job_title_badge__c;
        oppWrapper.campContact = campMember.ContactId;
        oppWrapper.campAccount = campMember.Contact.Account.Name;
        oppWrapper.campId = campMember.CampaignId;
        oppWrapper.campMoeda = campMember.CurrencyIsoCode;
        oppWrapper.oppAccount = campMember.Contact.AccountId;
        oppWrapper.campMember = campMember;
        oppWrapper.oppUnique = chaveOpp;

        lOppsWrapper.add(oppWrapper);
      }
      return lOppsWrapper;
    }
*/
    for (CampaignMember campMember : this.lstCampMember)
    {
      Id oppRecType = recOppMagazineAD;
      String oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName;
      Id OppIdRec = RecordTypeMemory.getRecType('Opportunity', campMember.Campaign.Opportunity_Type__c);
      String chaveOpp = campMember.ContactId+' - '+campMember.Contact.AccountId+' - '+campMember.Campaign.Event__c;
/*
// este trecho devera ser levado para uma trigger ("regra de validacao" impedindo criacao)
      for(Opportunity opp : lstOppControle)
      {
        if(opp.RecordTypeId == OppIdRec && OppIdRec != recOppUnique &&
           campMember.ContactId == opp.Contact__c &&
           campMember.Campaign.Magazine__r.Name == opp.Magazine__r.Name &&
           campMember.Campaign.Event__r.Name == opp.Event__r.Name &&
           campMember.Campaign.Club__r.Name == opp.Club__r.Name)
        {
          adicionaAviso('Some Contacts already have Opportunities with this conditions!');
          campMember.Opportunity_Created__c = true;
        }
      }

      if(campMember.Opportunity_Created__c) continue;
*/
      if(Test.isRunningTest()) oppRecType = recOppUnique;

      if(this.oppCampType == 'Magazine' /* && campContact != campMember.ContactId*/)
      {
        oppRecType = recOppMagazineAD;
        oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
          campMember.Contact.Account.Name;
      }
      else if(this.oppCampType == 'Membership')
      {
        oppRecType = recOppMembership;
        oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
          campMember.Contact.Account.Name;
      }
      else if(this.oppCampType == 'Open_Events')
      {
        oppRecType = recOppOpenEvents;
        oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
          campMember.Contact.Account.Name;
      }
      else if(this.oppCampType == 'SPEX')
      {
        oppRecType = recOppUnique;
        oppName = campMember.Contact.FirstName + ' ' + campMember.Contact.LastName + ' - ' +
          campMember.Contact.Account.Name;
      }

      OpportunityWrapper oppWrapper = new OpportunityWrapper(oppRecType);
      oppWrapper.oppName = oppName;
      oppWrapper.campType = 'Contact';
      oppWrapper.campStatus = campMember.Status;
      oppWrapper.campTitle = campMember.Contact.Job_title_badge__c;
      oppWrapper.campContact = campMember.ContactId;
      oppWrapper.campAccount = campMember.Contact.Account.Name;
      oppWrapper.campId = campMember.CampaignId;
      oppWrapper.campMoeda = campMember.CurrencyIsoCode;
      oppWrapper.oppAccount = campMember.Contact.AccountId;
      oppWrapper.campMember = campMember;
      oppWrapper.oppUnique = chaveOpp;

      lOppsWrapper.add(oppWrapper);
    }
    return lOppsWrapper;
  }

  public PageReference createMassOpportunities()
  {
/*
    OpportunityVinculaPricebook.exec = true;
    OpportunityUpdatePricebookEProduto.exec = true;
    
    
*/
    if (this.qdtOpportunity == 0) return adicionaErro('There are opportunities to be created');
    
    if (this.camp.Opportunity_Type__c == 'Open_Events' && String.isNotBlank(this.camp.Club__c)) return adicionaErro('Cannot Create Opportunities Please Contact Admin');
    if (this.camp.Opportunity_Type__c == 'Membership' && String.isNotBlank(this.camp.Event__c)) return adicionaErro('Cannot Create Opportunities Please Contact Admin');
    
    if (this.camp.Opportunity_Type__c == 'Open_Events' && String.isBlank(this.camp.Event__c)) return adicionaErro('Please Select Event!');
    if (this.camp.Opportunity_Type__c == 'Membership' && String.isBlank(this.camp.Club__c)) return adicionaErro('Please Select Club!');

    if (this.opp.CloseDate == null) return adicionaAviso('You must enter a value for Close date');
    else if(this.opp.OwnerId == null) return adicionaAviso('You must enter a value for Owner');
/*
    list<Opportunity> lstUpsertOpportunity = new list<Opportunity>();
    list<CampaignMember> lstCampMemberControle = new list<CampaignMember>();
*/
    map<String, Mass_Opportunity__c> lMapMassOpp = new map<String, Mass_Opportunity__c>();
    map<String, Mass_Opportunity_Product__c> lMapMassOppProd = new map<String, Mass_Opportunity_Product__c>();

    for(OpportunityWrapper oppWrapper : this.lstOppWrapper)
    {
      String lCurrIsoCode = (opp.Magazine__c != null) ? 'BRL' : this.opp.CurrencyIsoCode; //opp.Magazine__c != null ? 'BRL' : oppWrapper.campMoeda;
/*
      Opportunity lOpp = new Opportunity();
      lOpp.RecordTypeId = oppWrapper.oppRecType;
      lOpp.Name = oppWrapper.oppName;
      lOpp.StageName = opp.StageName;
      lOpp.Date_of_actual_status__c = opp.Date_of_actual_status__c;
      lOpp.Date_of_the_next_step__c = opp.Date_of_the_next_step__c;
      lOpp.CloseDate = opp.CloseDate;
      lOpp.CampaignId = oppWrapper.campId;
      lOpp.AccountId = oppWrapper.oppAccount;
      lOpp.Contact__c = oppWrapper.campContact;
      lOpp.CurrencyIsoCode = lCurrIsoCode;
      lOpp.OwnerId = opp.OwnerId;
      lOpp.Next_Step__c = opp.Next_Step__c;
      lOpp.Actual_Status__c = opp.Actual_Status__c;
      lOpp.Magazine__c = opp.Magazine__c;
      lOpp.Club__c = opp.Club__c;
      lOpp.Event__c = opp.Event__c;
      lOpp.Pricebook2Id = opp.Pricebook2Id;
      lOpp.opp_unique__c = oppWrapper.oppUnique;
      lOpp.MassOppOn__c = true;

      oppWrapper.campMember.Opportunity_Created__c = true;

      lstUpsertOpportunity.add(lOpp);
      lstCampMemberControle.add(oppWrapper.campMember);
*/
      // RLdO - cria o registro de configuracao - header - inicio
      Mass_Opportunity__c lMassOpp = new Mass_Opportunity__c(Campaign__c = this.campId);
      lMassOpp.CurrencyIsoCode = lCurrIsoCode;
      lMassOpp.Opp_Owner__c = opp.OwnerId;
      lMassOpp.Opp_CloseDate__c = opp.CloseDate;
      lMassOpp.Opp_StageName__c = opp.StageName;
      lMassOpp.Magazine__c = opp.Magazine__c;
      lMassOpp.Club__c = opp.Club__c;
      lMassOpp.Event__c = opp.Event__c;
      lMassOpp.Next_Step__c = opp.Next_Step__c;
      lMassOpp.Actual_Status__c = opp.Actual_Status__c;
      lMassOpp.Pricebook__c = opp.Pricebook2Id;
      lMassOpp.Date_of_actual_status__c = opp.Date_of_actual_status__c;
      lMassOpp.Date_of_the_next_step__c = opp.Date_of_the_next_step__c;

      lMapMassOpp.put(this.campId, lMassOpp);
      // RLdO - cria o registro de configuracao - header - fim
    }
/*
    if(!lstUpsertOpportunity.isEmpty()) Database.upsert( lstUpsertOpportunity );
    if(!lstCampMemberControle.isEmpty()) Database.update( lstCampMemberControle );
    if(!lstCampMember.isEmpty()) Database.update( lstCampMember );
*/
    System.debug('Lista de mass opp' + lMapMassOpp);
    if (!lMapMassOpp.isEmpty()) database.insert(lMapMassOpp.values());

    map<Id, Id> mapProdPriceEntry = new map<Id, Id>();
    for(ProductWrapper iProd : lstProdutoWpp)
    {
      if(iProd.IsSelected) mapProdPriceEntry.put(iProd.prod.Id, null);
    }

    for(PricebookEntry iPbe : [SELECT Id, Product2Id, CurrencyIsoCode FROM PricebookEntry
      WHERE Pricebook2Id = :opp.Pricebook2Id
      AND Product2Id = :mapProdPriceEntry.keySet()
//      And CurrencyIsoCode =: lstUpsertOpportunity[0].CurrencyIsoCode
      And CurrencyIsoCode = :this.opp.CurrencyIsoCode
      ])
    {
      mapProdPriceEntry.put(iPbe.Product2Id, iPbe.Id);
    }

    list<OpportunityLineItem> lstOliInsert = new list<OpportunityLineItem>();
//    for(Opportunity iOpp : lstUpsertOpportunity)
    {
      for(ProductWrapper iProd : lstProdutoWpp)
      {
        if(!iProd.IsSelected) continue;
        Id lPbeId = mapProdPriceEntry.get(iProd.prod.Id); 
/*
        OpportunityLineItem oli = new OpportunityLineItem();
//        oli.OpportunityId = iOpp.Id;
        oli.PricebookEntryId = mapProdPriceEntry.get(iProd.prod.Id);
        oli.UnitPrice = iProd.UnitPrice;
        oli.Quantity = iProd.Quantity;

        lstOliInsert.add(oli);
*/
        // RLdO - cria o registro de configuracao - produtos - inicio
        Mass_Opportunity__c lMassOpp = lMapMassOpp.get(this.campId);
        if (lMassOpp == null || String.isBlank(lMassOpp.Id)) continue;

        Mass_Opportunity_Product__c lMassOpProd = new Mass_Opportunity_Product__c(Mass_Opportunity__c = lMassOpp.Id);
        lMassOpProd.Quantity__c = iProd.Quantity;
        lMassOpProd.UnitPrice__c = iProd.UnitPrice;
        lMassOpProd.Product__c = iProd.prod.Id;
        lMassOpProd.PricebookEntry__c = lPbeId;

        lMapMassOppProd.put(this.campId + '---' + lMassOpProd.PricebookEntry__c, lMassOpProd);
        // RLdO - cria o registro de configuracao - produtos - fim
      }
    }
//    if(!lstOliInsert.isEmpty()) insert lstOliInsert;
	system.debug('lMapMassOppProd' + lMapMassOppProd);
    if (!lMapMassOppProd.isEmpty()) database.insert(lMapMassOppProd.values());

    // RLdO - inicia o processamento em batch da configuracao gerada
    SchGenMassOpportunity.iniciar();

    //adicionaSucesso('Opportunities has been created!');  
    
    return new PageReference('/' + this.campId);
    
  }
  
  public void SelectedCurrency()
  {
    SelecionaPricebook();
    CarregaProdutos();
  }

  public void CarregaProdutos()
  {
    String lBusca = (String.isNotBlank(this.busca)) ? this.busca : '';
    lBusca = lBusca.replace('\'','\\\'');

    String lQuery = '';
    lQuery += 'SELECT Id, Product2Id, Product2.Name, Product2.ProductCode, UnitPrice, CurrencyIsoCode, isActive'
      + ' FROM PricebookEntry WHERE isActive = true ';
    lQuery += (String.isNotBlank(opp.Pricebook2Id)) ? ' and Pricebook2Id = \'' + opp.Pricebook2Id + '\' ' : '';
    lQuery += ' And CurrencyIsoCode = \'' + opp.CurrencyIsoCode + '\' ';
    lQuery += (String.isNotBlank(lBusca)) ? ' and ( Product2.Name like \'%' + lBusca + '%\' or Product2.Family like \'%' + lBusca + '%\' or Product2.ProductCode like \'%' + lBusca + '%\')' : '';
    lQuery += ' order by Product2.Name limit 20';

    list<PricebookEntry> lstPriceEntry = Database.query(lQuery);

    lstProdutoWpp = new list<ProductWrapper>();

    for(PricebookEntry iPbe : lstPriceEntry)
    {
/*
      Product2 prod = new Product2(
        Id = iPbe.Product2Id,
        Name = iPbe.Product2.Name,
        ProductCode = iPbe.Product2.ProductCode
      );
      ProductWrapper lProdWpp = new ProductWrapper(prod);
*/
      ProductWrapper lProdWpp = new ProductWrapper(iPbe.Product2);
      lProdWpp.ListPrice = Utils.formatValue(iPbe.UnitPrice, 2);
      lProdWpp.UnitPrice = iPbe.UnitPrice;
      lstProdutoWpp.add(lProdWpp);
    }
  }

  public void SelecionaPricebook()
  {
    Id IdRelacionaPB = (camp.Event__c != null) ? camp.Event__c : (camp.Magazine__c != null) ? camp.Magazine__c : camp.Club__c;

    //adicionado And...
    for(Pricebook2 iPb : [SELECT Id, Starting_date__c, End_date__c, CurrencyIsoCode
      FROM Pricebook2
      WHERE (Event__c = :IdRelacionaPB OR Magazine__c = :IdRelacionaPB OR Club__c = :IdRelacionaPB)
      And Starting_date__c <=: Date.valueOf(dateSelected)
      And End_date__c >=: Date.valueOf(dateSelected)])
    {
      /*
       if((oppCampType == 'Open_Events') && (opp.CloseDate >= iPb.Starting_date__c && opp.CloseDate <= iPb.End_date__c))
       {
          opp.Pricebook2Id = iPb.Id;
       }
       opp.Pricebook2Id = iPb.Id;*/


      if(this.oppCampType != 'Open_Events')
      {
        opp.Pricebook2Id = iPb.Id;
        return;
      }

      if (opp.CloseDate >= iPb.Starting_date__c && opp.CloseDate <= iPb.End_date__c)
      {
        opp.Pricebook2Id = iPb.Id;
        return;
      }
    }
    opp.Pricebook2Id = null;
  }

  public void loadPopup()
  {
    this.loadPopup = true;
  }

  public void closePopup()
  {
    this.loadPopup = false;
    countProductSelect();
  }

  public void closePopupAndClear()
  {
    this.loadPopup = false;
    CarregaProdutos();
    countProductSelect();
  }

  public void countProductSelect()
  {
    this.qtdProduct = 0;
    for(ProductWrapper iPWpp : lstProdutoWpp)
      if(iPwpp.IsSelected)
        this.qtdProduct++;
  }

  public PageReference cancel()
  {
    return new PageReference('/' + campId);
  }

  public void changeCloseDate()
  {
    SelecionaPricebook();
    CarregaProdutos();
  }

  @testvisible
  private static PageReference adicionaErro(String msg)
  {
    ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.ERROR, msg) );
    return null;
  }

  @testvisible
  private static PageReference adicionaAviso(String msg)
  {
    ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.WARNING, msg) );
    return null;
  }
}