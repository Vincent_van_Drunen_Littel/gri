@isTest (SeeAllData = true)
public class SObjectHelper_Test {
    // Constants 
    private static Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
    private static String STAGE_COMMITTEE_REJECTED = 'Rejected';
    private static String STAGE_COMMITTEE_ACCEPTED = 'Approved';
    private static String STAGE_COMMITTEE_PROCESSING = 'Processing';
    private static String STAGE_OPP_PROSPECT = 'Prospect';
    private static String STAGE_OPP_ON_HOLD = 'On Hold';
    private static String STAGE_OPP_APPROVED = 'Approved';
    private static String STAGE_OPP_APPROVED_BY_FINANCE = 'Approved by Finance';
    private static String STAGE_OPP_WON = 'Won';
    private static String CLUB_RE_BRAZIL = 'RE Club Brazil';
    private static String CLUB_RE_EUROPE = 'RE Club Europe';
    private static String CLUB_INFRA_LATAM = 'Infra Club LatAm';
    private static String CLUB_INFRA_INDIA = 'Infra Club India';
    private static String CLUB_RE_INDIA = 'RE Club India';
    private static final String STATUS_ACTIVATED = 'Activated';
    private static final String STATUS_EXPIRED = 'Expired'; // grace period
    // Account
    private static Account s_acc1;
    private static Account s_acc_Latam1;
    private static Account s_acc_Europe1;
    private static Account s_acc_Brazil1;
    private static Account s_acc_India1;
    // Contact
    private static Contact s_ccc1;
    private static Contact s_ccc_br1;
    private static Contact s_ccc_latam1;
    private static Contact s_ccc_eu1;
    private static Contact s_ccc_in1;
    private static  List<Contact> s_listTestContacts = new List<Contact>();
    // Clubs
    private static Club__c s_Clubc;
    private static Club__c s_Clubc_Infra_Latam;
    private static Club__c s_Clubc_Infra_India;
    private static Club__c s_Clubc_RE_Europe;
    private static Club__c s_Clubc_RE_Brazil;
    private static Club__c s_Clubc_RE_India;
    // Product and pricebook
    private static Product2 s_Product2;
    private static PricebookEntry s_PricebookEntry;
    
    static{ // Build test Sandbox

        /// Create accounts ////////////////////////////////////////////////////////////
        // Brazil
        //s_acc_Brazil1 = SObjectInstance.createAccount2();
        s_acc_Brazil1 = NewInstanceSObject.createAccount();
        //s_acc_Brazil.BillingCity = 'São Pualo'; 
        s_acc_Brazil1.BillingCountry = 'Brazil' ;
        //s_acc_Brazil.BillingState = 'SP';
        //s_acc_Brazil.BillingStreet = 'Rua teste, 10';
        database.insert(s_acc_Brazil1);
        System.debug('[Test-Debug] s_acc_Brazil.Name='+s_acc_Brazil1.Name + ', s_acc_Brazil.Id='+s_acc_Brazil1.Id );
        // LatAm
        s_acc_Latam1 = NewInstanceSObject.createAccount();
        //s_acc_Latam1 = SObjectInstance.createAccount2();
        s_acc_Latam1.BillingCountry = 'Chile' ;
        database.insert(s_acc_Latam1);
        System.debug('[Test-Debug] s_acc_Latam1.Name='+s_acc_Latam1.Name + ', s_acc_Latam1.Id='+s_acc_Latam1.Id );
        // Europe
        s_acc_Europe1 = NewInstanceSObject.createAccount();
        //s_acc_Europe1 = SObjectInstance.createAccount2();
        s_acc_Europe1.BillingCountry = 'Spain' ;
        database.insert(s_acc_Europe1);
        System.debug('[Test-Debug] s_acc_Europe1.Name='+s_acc_Europe1.Name + ', s_acc_Europe1.Id='+s_acc_Europe1.Id );
        // India
        s_acc_India1 = NewInstanceSObject.createAccount();
        //s_acc_India1 = SObjectInstance.createAccount2();
        s_acc_India1.BillingCountry = 'India' ;
        database.insert(s_acc_India1);
        System.debug('[Test-Debug] acc_India.Name='+s_acc_India1.Name + ', s_acc_India1.Id='+s_acc_India1.Id );        
        
        /// Create contacts ////////////////////////////////////////////////////////////
        s_ccc1 = NewInstanceSObject.createContact(s_acc_Brazil1.Id, 'test1', 'br');
        //s_ccc1 = SObjectInstance.createContact2('test1', 'br', s_acc_Brazil1.Id);
        database.insert(s_ccc1);
        //s_ccc_br1 = SObjectInstance.createContact2('test1', 'br', s_acc_Brazil1.Id);
        s_ccc_br1 = NewInstanceSObject.createContact(s_acc_Brazil1.Id, 'test1', 'br');
        database.insert(s_ccc_br1);
        s_ccc_latam1 = NewInstanceSObject.createContact(s_acc_Latam1.Id, 'test1', 'latam');
        s_ccc_latam1 = NewInstanceSObject.createContact(s_acc_Latam1.Id, 'test1', 'latam');
        //s_ccc_latam1 = SObjectInstance.createContact2('test1', 'latam', s_acc_Latam1.Id);
        database.insert(s_ccc_latam1);
        s_ccc_eu1 = NewInstanceSObject.createContact(s_acc_Europe1.Id, 'test1', 'eu');
        //s_ccc_eu1 = SObjectInstance.createContact2('test1', 'eu', s_acc_Europe1.Id);
        database.insert(s_ccc_eu1);
        s_ccc_in1 = NewInstanceSObject.createContact(s_acc_India1.Id, 'test1', 'india');
        //s_ccc_in1 = SObjectInstance.createContact2('test1', 'india', s_acc_India1.Id);
        database.insert(s_ccc_in1);
        s_listTestContacts.add(s_ccc1);
        s_listTestContacts.add(s_ccc_br1);
        s_listTestContacts.add(s_ccc_latam1);
        s_listTestContacts.add(s_ccc_eu1);
        s_listTestContacts.add(s_ccc_in1); 
        
        /// Create Clubs ////////////////////////////////////////////////////////////
        // Club Infra Latam 
        s_Clubc_Infra_Latam = NewInstanceSObject.createClub();
        //s_Clubc_Infra_Latam = SObjectInstance.createClub();
        s_Clubc_Infra_Latam.Name = 'Infra Club LatAm';
        database.insert(s_Clubc_Infra_Latam);
        System.debug('[Test-Debug] s_Clubc_Infra_Latam.Name='+s_Clubc_Infra_Latam.Name +
                     ', s_Clubc_Infra_Latam.Id='+s_Clubc_Infra_Latam.Id);
        // Club Infra India
        s_Clubc_Infra_India = NewInstanceSObject.createClub();
        //s_Clubc_Infra_India = SObjectInstance.createClub();
        s_Clubc_Infra_India.Name = 'Infra Club India';
        database.insert(s_Clubc_Infra_India);
        System.debug('[Test-Debug] lClubc_Infra_India.Name='+s_Clubc_Infra_India.Name +
                     ', lClubc_Infra_India.Id='+s_Clubc_Infra_India.Id);    
        // Club RE Europe
        s_Clubc_RE_Europe = NewInstanceSObject.createClub();
        //s_Clubc_RE_Europe = SObjectInstance.createClub();
        s_Clubc_RE_Europe.Name = 'RE Club Europe';
        database.insert(s_Clubc_RE_Europe);
        System.debug('[Test-Debug] lClubc_RE_Europe.Name='+s_Clubc_RE_Europe.Name +
                     ', s_Clubc_RE_Europe.Id='+s_Clubc_RE_Europe.Id);      
        // Club RE Brazil
        s_Clubc_RE_Brazil = NewInstanceSObject.createClub();
        //s_Clubc_RE_Brazil = SObjectInstance.createClub();
        s_Clubc_RE_Brazil.Name = 'RE Club Brazil';
        database.insert(s_Clubc_RE_Brazil);
        System.debug('[Test-Debug] lClubc_RE_Brazil.Name='+s_Clubc_RE_Brazil.Name +
                     ', lClubc_RE_Brazil.Id='+s_Clubc_RE_Brazil.Id);    
        // Club RE India
        s_Clubc_RE_India = NewInstanceSObject.createClub();
        //s_Clubc_RE_India = SObjectInstance.createClub();
        s_Clubc_RE_India.Name = 'RE Club India';
        database.insert(s_Clubc_RE_India);
        System.debug('[Test-Debug] lClubc_Infra_Brazil.Name='+s_Clubc_RE_India.Name +
                     ', lClubc_Infra_Brazil.Id='+s_Clubc_RE_India.Id);    
        
        /// Create produc and pricebook ////////////////////////////////////////////////////////////
        // Product
        s_Product2 = NewInstanceSObject.createProduct();
        //s_Product2 = SObjectInstance.createProduct2();
        database.insert(s_Product2);
        // create PricebookEntry
        //id s_pbkId = SObjectInstance.catalogoDePrecoPadrao2();
        //s_PricebookEntry = SObjectInstance.entradaDePreco(s_pbkId,s_Product2.Id);
        id s_pbkId = NewInstanceSObject.getStandardPricebookId();
        s_PricebookEntry = NewInstanceSObject.pricebookEntry(s_pbkId,s_Product2.Id);
        database.insert(s_PricebookEntry);
        System.debug('[Test-Debug] lPricebookEntry.Name='+s_PricebookEntry.Name+', s_Product2.Id='+s_Product2.Id);        
    }
    
    @isTest private static void test_OppHelpers()
    {
        test.startTest();
        List<String> stages = new List<String>{'Prospect', 'On Hold'};
            
        List<Opportunity> lopps = new List<Opportunity>();
        //Opportunity opp1 = SObjectInstance.createOpportunityMembership(s_acc_Brazil1, 
        //                                                               RT_OPP_MEMBERSHIP, 
        //                                                               s_ccc1, 
        //                                                               s_Clubc_RE_Brazil,
        //                                                               System.today());
                Opportunity opp1 = NewInstanceSObject.createOpportunityMembership(s_acc_Brazil1, 
                    s_ccc1, 
                    s_Clubc_RE_Brazil, 
                    System.today()); 
        opp1.StageName = stages[0];
        //Opportunity opp2 = SObjectInstance.createOpportunityMembership(s_acc_Brazil1, 
        //                                                               RT_OPP_MEMBERSHIP, 
        //                                                               s_ccc1, 
        //                                                               s_Clubc_RE_Europe,
        //                                                               System.today());
        Opportunity opp2 = NewInstanceSObject.createOpportunityMembership(s_acc_Brazil1, 
                                                                       s_ccc1, 
                                                                       s_Clubc_RE_Europe,
                                                                       System.today());
        opp2.StageName = stages[1];

        lopps.add(opp1);
        lopps.add(opp2);
        database.insert(lopps);

        List<Opportunity> retOppStages = SObjectHelper.Opportunity_retStage(lopps, stages);
        List<Id> retOppIds = SObjectHelper.Opportunity_returnIdsFromOpps(lopps);
        List<Id> retOppConId = SObjectHelper.Opportunity_returnContactIdListNoRepeat(lopps);

        test.stopTest();
    }
    
    @isTest private static void test_MembershipHelpers()
    {
        test.startTest();
            
        List<Opportunity> lopps = new List<Opportunity>();
        //Opportunity opp1 = SObjectInstance.createOpportunityMembership(s_acc_Brazil1, 
        ///                                                               RT_OPP_MEMBERSHIP, 
        //                                                               s_ccc1, 
        //                                                               s_Clubc_Infra_Latam,
        //                                                               System.today());
        Opportunity opp1 = NewInstanceSObject.createOpportunityMembership(s_acc_Brazil1, 
                                                                       s_ccc1, 
                                                                       s_Clubc_Infra_Latam,
                                                                       System.today());
        opp1.StageName = STAGE_OPP_ON_HOLD;
        //Opportunity opp2 = SObjectInstance.createOpportunityMembership(s_acc_Brazil1, 
        //                                                               RT_OPP_MEMBERSHIP, 
        //                                                               s_ccc1, 
        //                                                               s_Clubc_Infra_Latam,
        //                                                               System.today());
        Opportunity opp2 = NewInstanceSObject.createOpportunityMembership(s_acc_Brazil1, 
                                                                       s_ccc1, 
                                                                       s_Clubc_Infra_Latam,
                                                                       System.today());
        opp2.StageName = STAGE_OPP_ON_HOLD;
        //Opportunity opp3 = SObjectInstance.createOpportunityMembership(s_acc_Brazil1, 
        //                                                               RT_OPP_MEMBERSHIP, 
        //                                                               s_ccc1, 
        //                                                               s_Clubc_Infra_Latam,
        //                                                               System.today());
        Opportunity opp3 = NewInstanceSObject.createOpportunityMembership(s_acc_Brazil1, 
                                                                       s_ccc1, 
                                                                       s_Clubc_Infra_Latam,
                                                                       System.today());
        opp3.StageName = STAGE_OPP_PROSPECT;        
        lopps.add(opp1);
        lopps.add(opp2);
        lopps.add(opp3);
        database.insert(lopps);
        
        List<Id> OppIdList = SObjectHelper.Opportunity_returnIdsFromOpps(lopps);
        lopps = [SELECT Id,Name,StageName FROM Opportunity WHERE Id IN :OppIdList];   
        lopps[0].StageName = STAGE_OPP_APPROVED_BY_FINANCE;
        lopps[1].StageName = STAGE_OPP_APPROVED_BY_FINANCE; 
        lopps[2].StageName = STAGE_OPP_APPROVED_BY_FINANCE;
        database.update(lopps);
        
        List<Contract> memberships = [SELECT Id, ContractNumber, Opportunity__c 
                                      FROM Contract 
                                      WHERE (Opportunity__c IN :OppIdList)];
        System.debug('[Debug-Test] memberships=' + memberships);
        
        test.stopTest();
    }
    
    
    @isTest private static void test_ContactHelpers()
    {
        test.startTest();
        Contact theContact = SObjectHelper.Contact_getId(s_listTestContacts, s_ccc_latam1.Id);
        System.assertEquals(theContact.Id, s_ccc_latam1.Id, 'Contact Ids do not match. theContact.Id='+ 
            theContact.Id+', s_ccc_latam1.Id='+s_ccc_latam1.Id );
        
        //System.assertEquals(NULL, SObjectHelper.Contact_getId(NULL, theContact.Id), 'Returned value is not NULL ' + 
        //                    SObjectHelper.Contact_getId(NULL, theContact.Id) );
        //test.stopTest();

    }
        
        
}