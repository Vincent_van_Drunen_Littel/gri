/**
 * Created by Eric Bueno on 24/02/2019.
 */
 
public with sharing class CampaignDAO {

    private static final CampaignDAO instance = new CampaignDAO();
    private CampaignDAO(){}
    public static CampaignDAO getInstance(){
        return instance;
    }

    public List<CampaignMember> findCampaignMemberByContactOpportunityId(Set<Id> setOpportunityId, Set<Id> setContactId){
        return [
                SELECT
                        Id,
                        CampaignId,
                        Campaign.Id,
                        Campaign.Digital_Funnel_Stage__c,
                        Opportunity__c,
                        ContactId,
                        Digital_Funnel_Stage__c
                FROM
                        CampaignMember
                WHERE
                        Opportunity__c IN: setOpportunityId
                AND
                        ContactId IN: setContactId
                AND
                        Campaign.Digital_Funnel__c = true
        ];
    }

}