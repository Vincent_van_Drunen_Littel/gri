/*******************************************************************************
*                     Copyright (C) 2016 - Cloud2b
*-------------------------------------------------------------------------------
*
* Classe que faz o teste da: 
*  1) atualização dos campos Next_Step e Date_of_the_next_step__c 
*     da oportunidade com dados da tarefa quando o flag Upgrade_to_Next_Step__c tá ligado; 
*  2) atualização dos campos Actual_Status__c e Date_of_Actual_Status__c 
*     da oportunidade com dados da tarefa quando o flag Upgrade_to_Actual_Status__c tá ligado; 
* NAME: TaskUpdateOppNextStepStatusAtualTest.cls
* AUTHOR: IGOR GOIS                         DATE: 04/05/2016
*
*-------------------------------------------------------------------------------
* MAINTENANCE
* AUTHOR:                                           DATE:
* MODIFIER: LMdO                                               DATE: 04/07/2016
*******************************************************************************/

@isTest
private class TaskUpdateOppNextStepStatusAtualTest {
  
    static testMethod void myUnitTest() 
    {
      Account lAcc = SObjectInstance.createAccount2();
      insert lAcc;

      Contact ctt = SObjectInstance.createContato(lAcc.Id);
      Database.insert(ctt);

      Event__c event = SObjectInstance.createEvent();
      event.Type__c = 'Open Event';
      Database.insert(event);

      event.Start_Date__c = system.today().addDays(22);
      event.End_Date__c = system.today().addDays(22);
      update(event);
      
      String lRecId = [ Select id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Open_Events' ].id; 
      
      Opportunity lOpp = SObjectInstance.createOpportunidade(lAcc.Id, lRecId);
      lOpp.ForecastCategoryName = 'Pipeline';
      lOpp.Contact__c = ctt.Id;
      lOpp.Event__c = event.Id;
      lOpp.StageName = 'Suspect';
      insert lOpp;
        
      Task lTask = SObjectInstance.createTask( lOpp.Id );
      insert lTask;
      
      lTask.Upgrade_to_Next_Step__c = false;
      lTask.Upgrade_to_Actual_Status__c = true;
      lTask.Description = 'Descrição da tarefa de teste. Descrição da tarefa de teste. Descrição da tarefa de teste. ';
      lTask.Description += lTask.Description;
      lTask.Description += lTask.Description;
      update lTask;
        
      lTask.Upgrade_to_Next_Step__c = true;
      lTask.Upgrade_to_Actual_Status__c = false;
      update lTask;
      
    }
}