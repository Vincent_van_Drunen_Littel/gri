/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * 
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 * 21/06/2018  AndPaschoalon  Updates: - took OppByProductCode outside of the loop, this is bad
 *                                     - fixed the updateOppDates: just in a trigger, just if is 
 *                                       changed
 *                                     - m_AutoSetWonIsActive to avoid recusivity on AutoSetWon()
 * 25/07/2018  APaschoalon    Update: (1) this class uses just the HelperSObject (2) Announcement
 *                            updateOppDates now supports SmartusEvents and SmartusSpex
 */                                   

public class AutoProcessMembershipOpps {
    // class constants
    private static final String PRODUCT_CODE_DIAMOND_MEMBER = 'P-000049';
    private static final String CONTACT_API_NAME = 'Contact__c';
    // debug delete it
    public static Integer counterAutoWon = 0;
    public static Integer debug_number_of_queries = 0;
    // recusion prevention
    private static Boolean m_AutoSetWonIsActive = False;
    public static Boolean m_updateOppDatesIsActive = False;

    /*
     *
     */
    public static void DiamondMemberAutoApproval(List<Opportunity> updatedOpps)
    {
        Id RecordTypeIdContact = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId();
        List<ContactOppMembership> lComSameClub = new List<ContactOppMembership>();
        List<ContactOppMembership> lComDiffClub = new List<ContactOppMembership>();
        List<Opportunity> listOppEventApproved = new List<Opportunity>();
        List<Opportunity> listOppFilterStageProd = new List<Opportunity>();

        for(Opportunity opp:updatedOpps)
        {

            if((opp.StageName == HelperSObject.OPPORTUNITY_STAGE_APPROVED) && (opp.RecordTypeId == RecordTypeIdContact))
            { 
                listOppEventApproved.add(opp);
            }          
        }
        
        if(listOppEventApproved.size()==0) return;


        OppByProductCode oppCodeFilter = new OppByProductCode(listOppEventApproved);
        listOppFilterStageProd = oppCodeFilter.filter(PRODUCT_CODE_DIAMOND_MEMBER, 'just'); 
        if(listOppFilterStageProd.size()==0) return;

        
        Set<String> contactOppIds = HelperSObject.setFromField(listOppFilterStageProd, CONTACT_API_NAME);
        List<String> membValidStatus = new List<String>{ HelperSObject.MEMBERSHIP_STATUS_ACTIVATED, HelperSObject.MEMBERSHIP_STATUS_DRAFT};
        List<Contract> listMemberships = [SELECT Id, Name, Club__c, Contact__c, Membership_Level__c, StartDate, EndDate
                                          FROM Contract
                                          WHERE(
                                              (Contact__c IN :contactOppIds) AND
                                              (Status IN :membValidStatus))];
        AutoProcessMembershipOpps.debug_number_of_queries++;
        System.debug('%% Number of Queries::'+AutoProcessMembershipOpps.debug_number_of_queries);                                      
        
        for(Opportunity opp:listOppFilterStageProd)
        {
            Id contactId = opp.Contact__c;
            Contract membFinal;
            Id membClubId;
            Boolean sameClub = False;
            for(Contract memb:listMemberships) // usar map aqui
            {
               
                if(contactId == memb.Contact__c)
                {
                    membFinal = memb;
                    //membClubStr = memb.Club__r.Name;
                    membClubId = memb.Club__c;
                    //if(membClubStr == opp.Event__r.Club__r.Name)
                    if(membClubId == opp.Event__r.Club__c)
                    {
                        sameClub = True;
                        break;
                    }
                }
            }
            if(membFinal != NULL)
            {
                 // check membership level
                if (((membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_PLATINUM_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_ADVISOR_MEMBER_DIAMOND) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_STATE_HIGH_PLATINUM_MEMBERSHIP) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_DIAMOND_LATAM_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_DIAMOND_STATE_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_DIAMOND_STATE_RETREAT_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_PLATINUM_LATAM_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_PLATINUM_MEMBER_INFRA_AND_RE) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_PLATINUM_STATE_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_PLATINUM_STATE_RETREAT_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_STATE_DIAMOND_LATAM_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_STATE_PLATINUM_COMPANY_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_STATE_LATAM_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_STATE_DIAMOND_COMPANY_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_ADVISOR_DIAMOND_LATAM_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_DIAMOND_LATAM_COMPANY_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_PLATINUM_LATAM_COMPANY_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_PLATINUM_COMPANY_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_STATE_MEMBER_ADVISORY_BOARD_INFRA_AND_RE) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_DIAMOND_MEMBER_INFRA_AND_RE) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_STATE_LATAM_COMPANY_MEMBER) ||
                    (membFinal.Membership_Level__c==HelperSObject.MEMBERSHIP_LV_DIAMOND_MEMBER)) &&
                    (HelperSObject.date(opp.Event__r.End_Date__c)<=membFinal.EndDate) && 
                    (HelperSObject.date(opp.Event__r.Start_Date__c)>=membFinal.StartDate) )
                {
                    ContactOppMembership cOppMem = new ContactOppMembership(contactId, opp, membFinal, String.valueOf(membClubId));
                    if(sameClub)
                    {
                        lComSameClub.add(cOppMem);
                    }
                    else
                    {
                        lComDiffClub.add(cOppMem);
                    }
                }
            } 
        }
		List<Opportunity> listOppStageProdMembership = new List<Opportunity>();
        for(ContactOppMembership contactOppMemb : lComSameClub)
        {
            listOppStageProdMembership.add(contactOppMemb.getOpportunity());
        }
        
        // Send Email to finance team about the Auto-approve
        //sentAutoAproovedMail(listOppFilterStageProd);
        //sentAutoAproovedMail(listOppStageProdMembership);
        for(Opportunity opp:updatedOpps) 
        {
            for(Opportunity filteredOpps:listOppStageProdMembership) // use map here
                if(filteredOpps.Id == opp.Id)
                {
                    opp.StageName = HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE;
                    opp.bypassValidationRules__c=True;
            }
        }
    }      

    /*
     *
     */
    public static void AutoSetWon(List<Opportunity> updatedOpps)
    {
        if(m_AutoSetWonIsActive==True) // prevent update loop
            return;

        List<Opportunity> listOppsAutoApprovedByFinance = new List<Opportunity>();
        for(Opportunity opp:updatedOpps)
        {
            if( (opp.StageName==HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE) && 
                (opp.bypassValidationRules__c==True) &&
                (opp.RecordTypeId==HelperSObject.opportunity_recordTypeIdEvents()))
            {
                Opportunity updateOpp = new Opportunity();
                updateOpp.StageName = HelperSObject.OPPORTUNITY_STAGE_WON;
                updateOpp.Finance_Status__c = 'Complimentary';
                updateOpp.Id = opp.Id;
                listOppsAutoApprovedByFinance.add(updateOpp);
            }
        }
        if(listOppsAutoApprovedByFinance.size()>0)
        {
            counterAutoWon++;
            //System.debug('%% AutoProcessMembershipOpps|counterAutoWon='+counterAutoWon);
            m_AutoSetWonIsActive = True;
            update listOppsAutoApprovedByFinance;
        }
    }

    public static void updateOppDates(List<Opportunity> updatedOpps)
    {
        // prevent recursion and assert it is in a
        if(m_updateOppDatesIsActive) 
            return;
        m_updateOppDatesIsActive=True;    
        TriggerUtils.assertTrigger();

        for(Opportunity opp:updatedOpps)
        {
            Opportunity oppOldMap = (Opportunity)Trigger.oldMap.get(opp.Id);

            //if(TriggerUtils.wasChangedTo(opp, Opportunity.StageName, HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE))
            if(HelperSObject.wasChangedTo(opp, oppOldMap, 'StageName', HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE))
            {
                if(opp.RecordTypeId==HelperSObject.opportunity_recordTypeIdEvents() || 
                   opp.RecordTypeId==HelperSObject.opportunity_recordTypeIdSPEX() ||
                   opp.RecordTypeId==HelperSObject.opportunity_recordTypeIdSmartusSpex() ||
                   opp.RecordTypeId==HelperSObject.opportunity_recordTypeIdSmartusEvent() )
                {
                    
                    opp.Approved_by_Finance_is_Today__c = System.today();
                }
                else if(opp.RecordTypeId==HelperSObject.opportunity_recordTypeIdMembership())
                {
                    opp.Membership_Announcement_Date__c = System.today();
                    opp.Approved_by_Finance_is_Today__c = System.today();
                }               
            }
        }
    }
}