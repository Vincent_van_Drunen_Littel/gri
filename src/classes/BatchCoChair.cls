/*
Update: 25,26 July 2017 by Darren 
Added extra exeption handaling in to the final delete and insert section so that it will show when there is an error and what opertunaty there is an error with
Also had to change the following line:

From: if(sA.Session_Status__c=='Suggested' || sA.Session_Status__c=='Agreed') {
To:   if(sA.Session_Status__c=='Interested' || sA.Session_Status__c=='Confirmed') {

The above line was changed due to an issue with the API names not matching the Values. 

Aso added code on the insert so if there was an error then it will generate a task for the owner of the opt
to look at the products. 

*/

global class BatchCoChair implements Database.Batchable<sObject>{
        
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('--------------------------------------------------');
        System.debug('SQL Start');
        String query='';
        Date d=Date.today();
        String dayString = d.format();

        query = ' Select Id ';
        query += ' FROM Opportunity ';
        query += ' Where ';
        query += ' Event__r.End_Date__c > TODAY AND Event__r.Type__c = \'Open Event\' AND ';
        query += ' (Event__r.Co_Chair_Deadline__c <:d) AND ';
        query += '  (StageName= \'Prospect\'';
        query += ' or StageName=\'Information Sent\'';
        query += ' or StageName=\'Negotiation\'';
        query += ' or StageName=\'Closing\'';
        query += ' or StageName=\'Lost\'';
        query +=') ';
        query +=' AND (Co_Chair_Exception__c =false)';
        query +=' AND (Account_Disabled__c = false)';
        query +=' AND Id IN (SELECT OpportunityId FROM OpportunityLineItem)';
        //query +=' AND (id=\'006K000000F0PrC\')'; //Only enable this line if testing agains a selected op!

        System.debug(dayString);
        System.debug(query);
        System.debug('--------------------------------------------------');
        System.debug('SQL End');
        return Database.getQueryLocator(query);
    }
    
    public void doExecute(List<sObject> scope)
    {
        System.debug('Executing');

        List<Opportunity> opt = (List<Opportunity>)scope;

        opt = [SELECT Id, Event__c, Pricebook2Id,StageName, Closing_Stage__c, OwnerId, 
                    (SELECT id, Session_Status__c, Event__c 
                    FROM Opportunity_sessions__r), 
                    (SELECT Id, OpportunityId, SortOrder, PricebookEntryId, Product2Id, ProductCode, Name, CurrencyIsoCode, Quantity, Discount,
                        TotalPrice, UnitPrice, ListPrice, ServiceDate, Description, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp,
                        IsDeleted, Comments__c, opp__c, Currency_Code__c, ProdName__c, Sales_Price_different_List_Price__c 
                    FROM OpportunityLineItems)
                FROM Opportunity
                WHERE Id IN :opt];    
        
        List<Opportunity> optt = new List<Opportunity>();
        List<Opportunity> opttFix = new List<Opportunity>();
        List<OpportunityLineItem> optLine = new List<OpportunityLineItem>();
        List<Attendee__c> delAttendeeList = new List<Attendee__c>();
        Set<Id> priceBookIds;
        Set<String> productCodes;
        
        String code = '';
        String id = '';
        
        System.debug('**************************************************');
        System.debug('Scope Record Size');
        System.debug(opt.Size());
        System.debug('**************************************************');
        
        list<Attendee__c> sessionAttendees =[Select Session__c, Session_Unique__c, Session_Type__c, 
        Session_Status__c, Session_Position__c, Session_Indexing__c, Opportunity__c, Id From Attendee__c where Opportunity__c in : opt];
        
        list<Attendee__c> sessionAttendeesTemp;
        
        for(Opportunity o :opt){
            Boolean useOpertunaty=true;
            list<Attendee__c> att =o.Opportunity_sessions__r;
            
            
            System.debug('**************************************************');
            System.debug('Attendee Count:' +att.size());
            System.debug('**************************************************');
            
            for(Attendee__c a: att){
                if (a.Session_Status__c =='Confirmed' || a.Session_Status__c =='Interested' && a.Event__c == o.Event__c){
                    System.debug('Setting use flag to false one!');
                    useOpertunaty=false;
                }
            }

            System.debug('@@@@useOpertunaty:' + useOpertunaty);
            
            sessionAttendeesTemp = new list<Attendee__c>();
            
            System.debug('**************************************************');
            System.debug('Checking for delete rule!');
            System.debug('**************************************************');
            
            System.debug('StageName:' +o.StageName);
            System.debug('Closing_Stage__c:' +o.Closing_Stage__c);
            
            if(o.StageName=='Closing' && o.Closing_Stage__c=='Confirmed' ){
                
                System.debug('**************************************************');
                System.debug('Delete rule passed first stage!');
                System.debug('**************************************************');
                
                boolean CochairFound=false;
                optLine =o.OpportunityLineItems;
                System.debug('optLine:' + optLine);
                for(OpportunityLineItem oli:optLine){
                    code=oli.ProductCode;
                    System.debug('Product Code:' + oli.ProductCode+ ' Name:' +oli.Name);
                    if (code=='P-000034'){
                        System.debug('**************************************************');
                        System.debug('Co-Char Found!!');
                        System.debug('**************************************************');
                        CochairFound=true;  
                    }
                }
                if(CochairFound==true){
                    if (sessionAttendees.size() >=1){
                        for(Attendee__c sA : sessionAttendees ){
                            if (Sa.Opportunity__c ==o.id){
                            	System.debug('**************************************************');
                                System.debug('Attempting to processing!');
                                System.debug('Session_Status__c:' + sA.Session_Status__c);
                                System.debug('**************************************************');
                                
                                /*
                                Update: 25-07-2017
                                Had to change the values it look for as the API names did not match the 
                                Vaue. 
                                */
                                // if(sA.Session_Status__c=='Suggested' || sA.Session_Status__c=='Agreed') {
                                if(sA.Session_Status__c=='Interested' || sA.Session_Status__c=='Confirmed') {
                                    System.debug('**************************************************');
                                    System.debug('*Setting use flag to false two!                  *');
                                    System.debug('**************************************************');
                                    useOpertunaty=false;
                                    break;
                                } else {
                                	System.debug('**************************************************');
                                    System.debug('*Setting use flag to true two!                    *');
                                    System.debug('**************************************************');
                                    useOpertunaty=true;
                                }
                            }   
                        }
                    }
                }
                
                
            }else if(o.StageName=='Closing' && o.Closing_Stage__c !='Confirmed' ){
                useOpertunaty=true;
                if (sessionAttendees.size() >=1){
                    for(Attendee__c sA : sessionAttendees ){
                            if (Sa.Opportunity__c ==o.id){
                                delAttendeeList.add(sA);
                        }   
                    }
                }
            }
            
            
            //Back to the original code at this point!
            if (useOpertunaty==true){
                System.debug('Adding to new op list!');
                optt.add(o);
            }
            
        }
        
       
        if (optt.size() <=0 && delAttendeeList.size() <=0){
            System.debug('No records found Aborting........');
            return;
        }else if (optt.size() <=0 && delAttendeeList.size() >=1){
            System.debug('Del only records found!');
            System.debug('Calling Del Attendees :' + delAttendeeList.size());   
            DelExecute(delAttendeeList);
            
            return;
        }
        
        System.debug('***********************');
        System.debug('*Running secoond loop!*');
        System.debug('***********************');
         
        //Create the list that will be used to add and delete the opertunaty lines
        list<OpportunityLineItem> DelList = new list<OpportunityLineItem>();
        list<OpportunityLineItem> AddList = new list<OpportunityLineItem>();
        
        boolean AddToDelList=false;
        
        
        priceBookIds = new set<Id>();
        productCodes = new set<String>();
        
        System.debug('**************************************************');
        System.debug('Optt Size:' +optt.size());
        System.debug('**************************************************');
                
        for(Opportunity o :optt){
            System.debug('Getting Opertunaty lines!');
            
            optLine =o.OpportunityLineItems;
            
            System.debug('Option Lines:' + optLine.size());
            
            if (optLine.size() >=1) {
                System.debug('1 or more lines');
                
                for(OpportunityLineItem oli:optLine){
                    System.debug('Got some option line processing.......');
                    
                    code=oli.ProductCode;
                    System.debug('Product Code:' + oli.ProductCode+ ' Name:' +oli.Name);
                    
                    if (code=='P-000034' || code =='P-001319' || code =='P-000053'){
                        System.debug('Checking for PriceBook2Id');
                        //System.debug('o.priceBook2Id:' + o.priceBook2Id);
                        if(o.priceBook2Id != null)
                            {
                                System.debug('PriceBook2Id Found!');
                                System.debug('PriceBook2Id:' + o.Pricebook2Id);
                                System.debug('PriceBook2Id Set');
                                priceBookIds.add(o.Pricebook2Id);
                                
                            }   
                    }
                                
                    if(code=='P-000034'){
                        //P-000033
                        System.debug('P-000033');
                        productCodes.add('P-000033');
                        DelList.add(oli);
                        
                    }else if (code =='P-001319'){
                        //P-001320
                        System.debug('P-001320');
                        productCodes.add('P-001320');
                        DelList.add(oli);
                        
                    }else if (code =='P-000053'){
                        //P-001488
                        System.debug('P-001488');
                        productCodes.add('P-001488');
                        DelList.add(oli);
                        
                    }
                    
                }
            }
            
        }
        System.debug('**************************************************');
        System.debug('No Records to PriceBook:' + priceBookIds.size());
        System.debug('No Records to Product Code:' + productCodes.size());
        System.debug('**************************************************');
                
        List<PricebookEntry> PricebookEntrys = [select id, priceBook2Id , productCode, UnitPrice from PricebookEntry Where priceBook2Id in :priceBookIds and productCode in :productCodes];
        System.debug('**************************************************');
        System.debug('No Records found:' + PricebookEntrys.size());
        System.debug('**************************************************');
        
        if (PricebookEntrys.size() >=1){
            
            System.debug('Running 3rd loop!');
            for(PricebookEntry pb: PricebookEntrys){
                
                System.debug('PDID:' + pb.id + ' priceBook2Id:' + pb.priceBook2Id + ' productCode:' + pb.productCode);
                
                String PDID =(String)pb.id;
                String priceBook2Id=(String)pb.priceBook2Id;
                String productCode=(String) pb.productCode;
                
                for(Opportunity o :optt){
                    optLine =o.OpportunityLineItems;
                    for(OpportunityLineItem oli:optLine){
                        String oliPricebookEntryId=(String)oli.PricebookEntryId;
                        String gotId=(String)pb.id;
                        
                        System.debug('PricebookEntryId Op Lines:' + oli.PricebookEntryId + '  PricebookEntry ID:'+gotId);
                        
                        if(PDID == gotId){
                            System.debug('****** MATCH FOUND ******');
                            System.debug('pbproductCode:' + pb.productCode  + ' oliProductCode:' + oli.ProductCode);
                            
                            OpportunityLineItem oppLineItem = new OpportunityLineItem();
                            oppLineItem.OpportunityId = o.Id;
                            oppLineItem.PricebookEntryId = pb.id;
                            oppLineItem.Quantity = oli.Quantity;
                            //oppLineItem.UnitPrice = oli.UnitPrice;
                            oppLineItem.UnitPrice = pb.UnitPrice;
                            
                            Boolean wasFound=false;
                            
                            for(OpportunityLineItem oppLine :AddList){
                                if (oppLine == oppLineItem)
                                    wasFound=true;
                            }
                            
                            if (wasFound==false)
                                AddList.add(oppLineItem);
                            /*Fixed an issue where it was deleteing all items and not just the ones I needed to delete that had been 
                            replaced. 
                            */
                            /*wasFound=false;
                            for(OpportunityLineItem oppLine :DelList){
                                if (oli == oppLine)
                                    wasFound=true;
                            }
                            
                            if (wasFound==false)    
                                DelList.add(oli);
                            */
                                            
                        }
                        
                    }
                    
                    
                }
            }
            
        }   
        
        
	    				
       	System.debug('********************************');
       	System.debug('Executing Updates and deleteds!*');
       	System.debug('********************************');
       			
       try {
       	
       	 	System.debug('Calling Add lines :' + Addlist.size());
        	if(Addlist.size() >=1)
            	insert Addlist;
            
        	
       }  catch (Exception err){
       		
       		System.debug('********************************');
       		System.debug('Error on insert!               *');
       		System.debug('********************************');
       		
       		for(OpportunityLineItem oli:Addlist){
       		 	System.debug('Opt Id:'  + oli.OpportunityId);
       		 	Opportunity gotErrorOpt;
       		 	
       		 	for(Opportunity o :optt) {
       		 	 	if (o.id ==oli.OpportunityId){
       		 	 		
       		 	 		gotErrorOpt=o;
       		 	 		
       		 	 		if (gotErrorOpt !=null) {
	           		 		System.debug('********************************');
		           			System.debug('Building Task                  *');
		           			System.debug('********************************');
		           				
		           		 	Task t = new Task(Subject = ('Error Processing Opportunity '  + oli.OpportunityId ) , 
					        Description = ('One or more product lines have issues, please check price books.' + err.getMessage()), 
					        WhatId  = oli.OpportunityId, 
					        OwnerId = gotErrorOpt.OwnerId,
					        ReminderDateTime = Date.today(),
					        IsReminderSet = true,
					        Status = 'Open');
					        insert t;
					        
					        System.debug('********************************');
		           			System.debug('Building Completed             *');
		           			System.debug('********************************');
		        
       		 			}
       		 	
       		 	 	}
       		 	}
       		 		
	        
       		}
       		
       		System.assert(true, err.getMessage());        
       	
       }
       
       
       try {
       	
       	 	
            
        	System.debug('Calling Dellines :' + DelList.size());
        	if(DelList.size() >=1)
            	delete DelList;
        
        
       }  catch (Exception err){
       		
       		System.debug('********************************');
       		System.debug('Error on delete A !              *');
       		System.debug('********************************');
       		
       		for(OpportunityLineItem oli:DelList){
       		 	System.debug('Opt Id:'  + oli.OpportunityId);
       		}
       		
       		System.assert(true, err.getMessage());   
       	
       }
       
       try {
       	
        	System.debug('Calling Del Attendees :' + delAttendeeList.size());   
        	DelExecute(delAttendeeList);
        
       }  catch (Exception err){
       		
       		System.debug('********************************');
       		System.debug('Error on delete B !              *');
       		System.debug('********************************');
       		System.assert(true, err.getMessage());
       }  
    }
    
    public void DelExecute(list<Attendee__c> delAttendeeList){
        
        System.debug('Calling Del Attendees :' + delAttendeeList.size());   
        
        try {
        	 if(delAttendeeList.size()>=1)
            	delete delAttendeeList;
            
        }catch (Exception err){
           		
           		System.debug('********************************');
           		System.debug('Error on delete B !              *');
           		System.debug('********************************');
           		System.assert(true, err.getMessage());   
           	
       }
       
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        System.debug('**************************************************');
        System.debug('*            Execute been called!                *');
        System.debug('**************************************************');
    
        
        doExecute(scope);    
    }
    
   
    
     global void finish(Database.BatchableContext BC){
        System.debug('**************************************************');
        System.debug('*            Execute Finished!                   *');
        System.debug('**************************************************');
     }
}