/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste da classe "OpportunitySetPrimaryContactRole".
*
* NAME: OpportunitySetPrimaryContactRoleTest.cls
* AUTHOR: RVR                                                  DATE: 07/04/2016
*******************************************************************************/
@isTest
private class OpportunitySetPrimaryContactRoleTest 
{   
  private static Id REC_OPP_SPEX = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );

  static testMethod void 	testeFuncional()
  {
    Test.startTest(); 

    Account conta = SObjectInstance.createAccount2();
    Database.insert(conta);

    Contact contato = SObjectInstance.createContato(conta.Id);
    Database.insert(contato);

    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_SPEX);
    opp.Contact__c = contato.Id;
    opp.StageName = 'Negotiation';
    Database.insert(opp);

    OpportunityContactRole oppCttRole = new OpportunityContactRole();
    oppCttRole.ContactId = opp.Contact__c;
    oppCttRole.OpportunityId = opp.Id;
    oppCttRole.IsPrimary = true;
    oppCttRole.Role = 'Decision Maker';
    Database.insert(oppCttRole);

    Test.stopTest();
  }
}