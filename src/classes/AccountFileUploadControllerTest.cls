/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsavel pela cobertura do controller AccountFileUploadController
*
*
* NAME: AccountFileUploadControllerTest.cls
* AUTHOR: VMDL                                                 DATE: 25/11/2016
*******************************************************************************/
@isTest
private class AccountFileUploadControllerTest {

	private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
	private static Photo_Name__c rename;
	static
	{
	  rename = new Photo_Name__c();
	  rename.Name = 'PhotoName';
	  rename.separator__c = '_';
	  rename.Extension__c = '.jpg';
	  
	  insert rename;
	}

    static testMethod void TestSucesso() 
    {
      Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
  		Database.insert(conta);
          
  		AccountFileUploadController controller = new AccountFileUploadController(new ApexPages.StandardController(conta));
  		
  		controller.previsao = 'Original Logo';
  		controller.document.Body = Blob.valueOf('testeMarketing');
  		controller.document.Name = 'Teste1';
  		controller.upload();
  		
  		controller.previsao = 'Marketing Edited Logo';
  		controller.document.Body = Blob.valueOf('testeMarketing');
  		controller.document.Name = 'Teste3';
  		controller.upload();			
    }

    static testMethod void TestErro() 
    {
    	Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
		  Database.insert(conta);
        
		  AccountFileUploadController controller = new AccountFileUploadController(new ApexPages.StandardController(conta));
	    //erro sem previsao
  		controller.upload();
  		//erro sem documento
  		controller.previsao = 'Original Photo';
	    controller.upload();		
    }
}