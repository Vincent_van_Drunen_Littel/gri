/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure  
 * players
 ******************************************************************************
 * Class responsible for Handling the Opportunity Approval process
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * NAME: OpportunityGlobalApprovalProcess.cls
 * Orginal AUTHOR: CFA(Cloud2b)                          DATE: 15/03/2017
 * Manteiner: Anderson Paschoalon                        DATE: 26/07/18
 * Date        Author         Description
 * 31/07/18    AndersonP      
 *   AndersonP    Code simplification, optimization and 
 *****************************************************************************/

public with sharing class OpportunityGlobalApprovalProcess {    

    // This map Maps the Opp.Approver_Division__c and ProcessDefinition.DeveloperName
    // Used on Events Approval process
    private static final map<String,String> MAP_OPPAPPDIV_PROCDEF = new map<String,String>
    {
        'BR RE' => 'BR_RE_Events',
        'BR INFRA' => 'BR_INFRA_Events',
        'LATAM RE' => 'LATAM_RE_Events',
        'LATAM INFRA' => 'LATAM_INFRA_Events',
        'UK1' => 'UK1_Events',
        'UK2' => 'UK2_Events',
        'UK3' => 'UK3_Events',
        'UK4' => 'UK4_Events',
        'UK5' => 'UK5_Events',
        'Global' => 'Global_Events',
        'Smartus' => 'Smartus',
        'PropTech' => 'PropTech'
    };
    // This Map maps the Opp.Club__c and its ProcessDefinition.DeveloperName
    // Used on  Memberships approval process
    private static final map<String,String> MAP_OPPCLUB_PROCDEF = new map<String,String>
    {
        'Infra Club LatAm' => 'Infrastructure_Club',
        'RE Club Brazil' => 'Real_Estate_Club',
        'RE Club LatAm' => 'Club_RE_LatAm',
        'Retail Club Brazil' => 'Club_Retail_BR',
        'RE Club Europe' => 'CLUB_EUROPE',
        'RE Club India' => 'India_Club',
        'Infra Club India' => 'Infra_Club_India',
        'RE Club Africa' => 'RE_Club_Africa'
    }; 
    // This Map maps the Opp.Pricebook2.Name and its ProcessDefinition.DeveloperName  
    // Used on SPEX approval process          
    private static final map<String,String> MAP_SPEXPRICEBOOK_PROCDEF = new Map<String,String>
    {
        'Sponsorship Brazil and LatAm' => 'SPEX_BR',
        'Sponsorship India' => 'SPEX_India',
        'Sponsorship Brazil RE' => 'SPEX_BR',
        'Sponsorship LatAm RE' => 'SPEX_LatAm_RE',
        'Sponsorship Infra LatAm' => 'SPEX_Infra_LatAm',
        'Sponsorship Europe' => 'SPEX_UK',
        'Sponsorship India RE' => 'SPEX_India',
        'Sponsorship India Infra' => 'SPEX_India_Infra',
        'Sponsorship PropTech' => 'SPEX_PropTech',
        'Sponsorship Smartus' => 'SPEX_Smartus'
    };
    private static final String DEFAULT_SEPEX_PROCDEF = 'SPEX_BR';
    // maps the Opp.Pricebook2.Club and its ProcessDefinition.DeveloperName 
    // Used on SPEX event only aprooval process    
    private static final map<String,String> MAP_EVENTCLUB_PROCDEF = new Map<String,String>
    {
        'RE Club Africa' => 'SPEX_India_RE_Event_Only',
        'RE Club Brazil' => 'SPEX_BR_RE_Event_Only',
        'RE Club LatAm' => 'SPEX_LatAm_RE_Event_Only',
        'Infra Club LatAm' => 'SPEX_Infra_LatAm_Event_Only',
        'RE Club Europe' => 'SPEX_Europe_Event_Only',
        'RE Club India' => 'SPEX_India_RE_Event_Only',
        'Infra Club India' => 'SPEX_India_Infra_Event_Only',
        'PropTech' => 'SPEX_PropTech_Event_Only',
        'Smartus - Real Estate' => 'SPEX_Smartus_Event_Only'
    };
    private static final Id RT_OPP_MEMBERSHIP=RecordTypeMemory.getRecType('Opportunity', 'Membership');
    private static final Id RT_OPP_MAGAZINE=RecordTypeMemory.getRecType('Opportunity', 'Magazine');
    private static final Id RT_OPP_EVENTS=RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
    private static final Id RT_OPP_SPEX=RecordTypeMemory.getRecType('Opportunity', 'SPEX');
    private static final Id RT_OPP_SMARTUS_EVENTS=RecordTypeMemory.getRecType( 'Opportunity', 'Smartus_Events');
    private static final Id RT_OPP_SMARTUS_SPEX=RecordTypeMemory.getRecType( 'Opportunity', 'Smartus_Spex');
    private static final map<String,String> mapAppProcess = new map<String,String>();
    private static final String LOGTAG = '%% OpportunityGlobalApprovalProcess: ';

    /**
     * Trigger handler, called on the Opportunity Trigger
     */
    public static void execute()
    {
        TriggerUtils.assertTrigger();
        List<Opportunity> listOpps = Trigger.new;

        // Set of Pricebooks Ids from Trigger.new
        Set<Id> setOppPricebooksIds = new Set<Id>();
        for(Opportunity opp:listOpps)
            setOppPricebooksIds.add(opp.Pricebook2Id);

        // Map PricebooksIds vs Pricebook2 from setOppPricebooksIds
        Map<Id, Pricebook2> mapId_Pricebooks = new Map<Id, Pricebook2>(
            [SELECT Id, Name, Club__c, Club__r.Name, Event__c, Event__r.Club_Name__c, Single_Event_Pricebook__c 
            FROM Pricebook2 WHERE Id IN :setOppPricebooksIds]);

        // Map Opp Ids from Trigger.new vs. Pricebook2
        Map<Id, Pricebook2> mapOppIds_Pricebooks = new Map<Id, Pricebook2>();
        for(Opportunity opp:listOpps)
        {
            mapOppIds_Pricebooks.put(opp.Id, mapId_Pricebooks.get(opp.Pricebook2Id));
        }

        List<Opportunity> listOpp2ApproveEvents = new List<Opportunity>();
        List<Opportunity> listOpp2ApproveMembership = new List<Opportunity>();
        List<Opportunity> listOpp2ApproveSpex = new List<Opportunity>();
        List<Opportunity> listOpp2ApproveSpexEvOnly = new List<Opportunity>();
        List<Opportunity> lListApplOppDisc = new List<Opportunity>();
        List<Opportunity> listOpp2ApproveSmartusEv = new List<Opportunity>();
        List<Opportunity> listOpp2ApproveSmartusSpex = new List<Opportunity>();
        List<Opportunity> listOpp2ApproveSmartusSpexEvOnly = new List<Opportunity>();
        List<Opportunity> listOpp2ApproveCurrency = new List<Opportunity>();
        Set<String> lstOppAPId = new Set<String>();   
        List<Approval.ProcessSubmitRequest> listApprovals = new List<Approval.ProcessSubmitRequest>();

        // create a map of all approval processes available
        for(ProcessDefinition objCS : [SELECT DeveloperName,State FROM ProcessDefinition ORDER BY State ASC NULLS FIRST])
        {
            mapAppProcess.put(objCS.DeveloperName, objCS.State);
        }
        
        for(Opportunity opp : listOpps)
        {
            if(TriggerUtils.wasChangedTo(opp, Opportunity.Approval_Process__c, '3 - Rejected')) lstOppAPId.add(opp.id);

            // currency apporval process
            if(opp.Payment_Current_Currency__c) listOpp2ApproveCurrency.add(opp);
            
            if(!requiresApprovalProcess(opp)) 
            { 
                System.debug(LOGTAG+'Opportunity `'+opp.Name+'` do not requires ApprovalProcess');
                continue;
            }
            System.debug(LOGTAG+'Opportunity `'+opp.Name+'` *REQUIRES* ApprovalProcess');
            //System.debug(LOGTAG+'opp='+opp);
            System.debug(LOGTAG+'opp.OppRecordTypeName__c='+opp.OppRecordTypeName__c);

            Boolean isSpexEvOnly = mapOppIds_Pricebooks.get(opp.Id)!=NULL?mapOppIds_Pricebooks.get(opp.Id).Single_Event_Pricebook__c:FALSE;
            //System.debug(LOGTAG+'isSpexEvOnly='+isSpexEvOnly);     
            if(opp.RecordTypeId==RT_OPP_EVENTS || opp.RecordTypeId==RT_OPP_SMARTUS_EVENTS)
                listOpp2ApproveEvents.add(opp);
            else if(opp.RecordTypeId==RT_OPP_MEMBERSHIP)
                listOpp2ApproveMembership.add(opp);
            else if( (opp.RecordTypeId==RT_OPP_SPEX || opp.RecordTypeId==RT_OPP_SMARTUS_SPEX)
                && !isSpexEvOnly)
                listOpp2ApproveSpex.add(opp);
            else if( (opp.RecordTypeId==RT_OPP_SPEX || opp.RecordTypeId==RT_OPP_SMARTUS_SPEX) 
                && isSpexEvOnly)
                listOpp2ApproveSpexEvOnly.add(opp);

        }
        // Price corretion
        if(!lstOppAPId.isEmpty()) 
            productPriceCorrection(lstOppAPId);
        // Approval process events
        if(!listOpp2ApproveEvents.isEmpty())
            listApprovals.addAll(approvalOpportunityEvent(listOpp2ApproveEvents));
        // Approval process membership
        if(!listOpp2ApproveMembership.isEmpty())
            listApprovals.addAll(approvalOpportunityMembership(listOpp2ApproveMembership));
        // Approval process Spex 
        if(!listOpp2ApproveSpex.isEmpty())
            listApprovals.addAll(approvalOpportunitySpex2(listOpp2ApproveSpex, mapOppIds_Pricebooks));
        // Approval process Spex (Event Only)
        if(!listOpp2ApproveSpexEvOnly.isEmpty())
            listApprovals.addAll(approvalOpportunitySpexEvOnly(listOpp2ApproveSpexEvOnly, mapOppIds_Pricebooks));

        Approval.ProcessResult[] result;
        if(!listApprovals.isEmpty()) result = Approval.process(listApprovals, FALSE);  
    }

    /**
     * Test if the Opportunity requires Approval process.
     */
    private static Boolean requiresApprovalProcess(Opportunity opp)
    {   
        //String logMsg = '';
        Opportunity oldOpp = (Opportunity)Trigger.oldMap.get(opp.Id);
        Decimal discountActual = (opp.Discount_of_opportunity__c==NULL)?0:opp.Discount_of_opportunity__c;
        Decimal discountOld = (oldOpp.Discount_of_opportunity__c==NULL)?0:oldOpp.Discount_of_opportunity__c;
        //logMsg='{opp.Id:"'+opp.Id+
        //    '", discountActual:"'+discountActual+
        //    '", discountOld:"'+discountOld+
        //    '", opp.discount_percentage__c:"'+opp.discount_percentage__c+
        //    '", oldOpp.discount_percentage__c:"'+oldOpp.discount_percentage__c+
        //    '", opp.Sales_price_List_price__c:"'+opp.Sales_price_List_price__c+
        //    '", oldOpp.Sales_price_List_price__c:"'+oldOpp.Sales_price_List_price__c+
        //    '", opp.Different_prices__c:"'+opp.Different_prices__c+
        //    '", oldOpp.Different_prices__c:"'+oldOpp.Different_prices__c+'"'+'}';
        //System.debug(logMsg);
        return
            (discountActual>discountOld) || 
            (opp.discount_percentage__c > oldOpp.discount_percentage__c)||
            (opp.Sales_price_List_price__c > oldOpp.Sales_price_List_price__c) || 
            (opp.Different_prices__c > oldOpp.Different_prices__c);
    }   

    /**
     * This method grab the Approver_Division__c for a list o opportunities, and 
     * find what is the correct approval process using the map MAP_OPPAPPDIV_PROCDEF
     */
    public static List<Approval.ProcessSubmitRequest> approvalOpportunityEvent(List<Opportunity> oppEvent)
    {
        List<Approval.ProcessSubmitRequest> listRequestEvents = new List<Approval.ProcessSubmitRequest>();
        String logMsg='approvalOpportunityEvent:';   
        for(Opportunity opp : oppEvent)
        {      
            Approval.ProcessSubmitRequest requestEvents = new Approval.ProcessSubmitRequest();
            Id lUserIdE = UserInfo.getUserId();
            requestEvents.setObjectId(opp.Id);
            requestEvents.setSubmitterId(lUserIdE);
            if(mapAppProcess.get(MAP_OPPAPPDIV_PROCDEF.get(opp.Approver_Division__c)) == 'Inactive') continue;
            requestEvents.setProcessDefinitionNameOrId(MAP_OPPAPPDIV_PROCDEF.get(opp.Approver_Division__c));
            requestEvents.setSkipEntryCriteria(FALSE);
            listRequestEvents.add(requestEvents);
            logMsg+='{ opp.Name:`'+opp.Name+'`, opp.Approver_Division__c:`'+
                opp.Approver_Division__c+'`, MAP_OPPAPPDIV_PROCDEF.get(opp.Approver_Division__c):`'+
                MAP_OPPAPPDIV_PROCDEF.get(opp.Approver_Division__c)+'`}, ';
        }
        System.debug(LOGTAG+logMsg);
        return listRequestEvents;
    }
    
    /**
     * This method grab the Club_Name__c for the list o opportunities, and 
     * find what is the correct approval process using the map MAP_OPPAPPDIV_PROCDEF
     */
    public static list<Approval.ProcessSubmitRequest> approvalOpportunityMembership(List<Opportunity> oppMember)
    {
        List<Approval.ProcessSubmitRequest> listRequestMemberships = new List<Approval.ProcessSubmitRequest>();
        String logMsg='approvalOpportunityMembership:';
        for(Opportunity opp : oppMember)
        {
            Approval.ProcessSubmitRequest requestMemberhip = new Approval.ProcessSubmitRequest();
            Id lUserIdM = UserInfo.getUserId();
            requestMemberhip.setObjectId(opp.Id);
            requestMemberhip.setSubmitterId(lUserIdM);
            if(mapAppProcess.get(MAP_OPPCLUB_PROCDEF.get(opp.Club_Name__c)) == 'Inactive') continue;
            requestMemberhip.setProcessDefinitionNameOrId(MAP_OPPCLUB_PROCDEF.get(opp.Club_Name__c));
            requestMemberhip.setSkipEntryCriteria(FALSE);
            listRequestMemberships.add(requestMemberhip);
            logMsg+='{ opp.Name:`'+opp.Name+'`, opp.Club_Name__c:`'+
                opp.Club_Name__c+'`, MAP_OPPCLUB_PROCDEF.get(opp.Club_Name__c):`'+
                MAP_OPPCLUB_PROCDEF.get(opp.Club_Name__c)+'`}, ';
        }
        System.debug(LOGTAG+logMsg);
        return listRequestMemberships;   
    }
 
    /**
     * This method grab the Pricebook Name for the list o opportunities, and 
     * find what is the correct approval process using the map MAP_SPEXPRICEBOOK_PROCDEF
     */   
     /*
    public static list<Approval.ProcessSubmitRequest> approvalOpportunitySpex(list<Opportunity> listOpp)
    {
        
        List<Approval.ProcessSubmitRequest> listRequestSpex = new List<Approval.ProcessSubmitRequest>();
        String logMsg='approvalOpportunitySpex:';
        for(Opportunity opp : listOpp)
        {     
            Approval.ProcessSubmitRequest requestSpex = new Approval.ProcessSubmitRequest();
            Id lUserIdU = UserInfo.getUserId();
            requestSpex.setObjectId(opp.Id);
            requestSpex.setSubmitterId(lUserIdU);
            if(mapAppProcess.get(MAP_SPEXPRICEBOOK_PROCDEF.get(opp.Pricebook2.Name)) == 'Inactive') continue;
            requestSpex.setProcessDefinitionNameOrId(MAP_SPEXPRICEBOOK_PROCDEF.get(opp.Pricebook2.Name));
            requestSpex.setSkipEntryCriteria(FALSE);
            listRequestSpex.add(requestSpex);
            logMsg+='{ opp.Name:`'+opp.Name+'`, MAP_SPEXPRICEBOOK_PROCDEF.get(opp.Pricebook2.Name):`'+
                MAP_SPEXPRICEBOOK_PROCDEF.get(opp.Pricebook2.Name)+'`, MAP_OPPCLUB_PROCDEF.get(opp.Club_Name__c):`'+
                MAP_OPPCLUB_PROCDEF.get(opp.Club_Name__c)+'`, opp.Pricebook2.Name:`'+opp.Pricebook2.Name+'`}, ';
        }
        System.debug(LOGTAG+logMsg);
        return listRequestSpex;
    }
    */

    /**
     * This method grab the Pricebook Name for the list o opportunities, and 
     * find what is the correct approval process using the map MAP_SPEXPRICEBOOK_PROCDEF
     */   
    public static list<Approval.ProcessSubmitRequest> approvalOpportunitySpex2(
        list<Opportunity> listOpp, Map<Id, Pricebook2> mapOppId_Pricebook)
    {
        List<Approval.ProcessSubmitRequest> listRequestSpex = new List<Approval.ProcessSubmitRequest>();
        String logMsg='approvalOpportunitySpex2';
        String procDef='';
        System.debug(LOGTAG+'listOpp='+listOpp);
        for(Opportunity opp : listOpp)
        {
            Approval.ProcessSubmitRequest requestSpex = new Approval.ProcessSubmitRequest();
            Id lUserIdU = UserInfo.getUserId();
            requestSpex.setObjectId(opp.Id);
            requestSpex.setSubmitterId(lUserIdU);
            Pricebook2 oppPricebook = mapOppId_Pricebook.get(opp.Id);
            if(oppPricebook!=NULL)
                procDef=MAP_SPEXPRICEBOOK_PROCDEF.get(oppPricebook.Name);
            else // default procdef
                procDef=DEFAULT_SEPEX_PROCDEF;
            if(mapAppProcess.get(procDef)=='Inactive') continue;
            requestSpex.setProcessDefinitionNameOrId(procDef);
            requestSpex.setSkipEntryCriteria(FALSE);
            listRequestSpex.add(requestSpex);
            // comment it
            //logMsg+='{opp.Id:'+opp.Id+', opp.Name:'+opp.Name+'`, mapOppId_Pricebook.get(opp.Id).Name:`'+
            //    mapOppId_Pricebook.get(opp.Id).Name+'`, procDef:`'+procDef+'`, mapOppId_Pricebook.get(opp.Id).Name:`'+
            //    mapOppId_Pricebook.get(opp.Id).Name+'`, mapOppId_Pricebook.get(opp.Id).Id:`'+
            //    mapOppId_Pricebook.get(opp.Id).Id+'`, procDef:'+procDef+', mapAppProcess.get(procDef):`'+
            //    mapAppProcess.get(procDef)+'` }, ';
        }
        System.debug(LOGTAG+logMsg);
        return listRequestSpex;
    }

    /**
     * This method grab the Opportunity event club Pricebook for the list o opportunities, and 
     * find what is the correct approval process using the map MAP_SPEXPRICEBOOK_PROCDEF
     */   
    public static List<Approval.ProcessSubmitRequest> approvalOpportunitySpexEvOnly(
            List<Opportunity> listOpp, Map<Id, Pricebook2> mapOppId_Pricebook)
    {
        List<Approval.ProcessSubmitRequest> listRequestSpexEvOnly = new List<Approval.ProcessSubmitRequest>();
        String logMsg='approvalOpportunitySpexEvOnly:';
        String procDef=''; 
        String eventClub='';
        Id userId;
        for(Opportunity opp:listOpp)
        {
            Approval.ProcessSubmitRequest requestSpexEvOnly = new Approval.ProcessSubmitRequest();
            userId=UserInfo.getUserId();
            requestSpexEvOnly.setObjectId(opp.Id);
            requestSpexEvOnly.setSubmitterId(userId);
            eventClub = mapOppId_Pricebook.get(opp.Id).Event__r.Club_Name__c;
            procDef = MAP_EVENTCLUB_PROCDEF.get(eventClub);
            if(mapAppProcess.get(procDef)=='Inactive') continue;
            requestSpexEvOnly.setProcessDefinitionNameOrId(procDef);
            requestSpexEvOnly.setSkipEntryCriteria(FALSE);
            listRequestSpexEvOnly.add(requestSpexEvOnly);
            logMsg+='{opp.Name:`'+opp.Name+'`, eventClub:`'+eventClub+'`, procDef:`'+procDef+'`'+
            ', mapAppProcess.get(procDef):`'+mapAppProcess.get(procDef)+'` }, ';
        }
        System.debug(LOGTAG+logMsg);
        return listRequestSpexEvOnly;   
    }

    /**
     * Approval process for currency
     */
    public static List<Approval.ProcessSubmitRequest> approvalOpportunityCurrency(List<Opportunity> lstOpp)
    {
        List<Approval.ProcessSubmitRequest> listRequest = new List<Approval.ProcessSubmitRequest>();
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        Id lUserId = UserInfo.getUserId();
        
        for(Opportunity opp : lstOpp)
        {
            request.setObjectId(opp.Id);
            request.setSubmitterId(lUserId);
            if(mapAppProcess.get('Payment_Current_Currency') == 'Inactive') continue;
            request.setProcessDefinitionNameOrId('Payment_Current_Currency');
            request.setSkipEntryCriteria(FALSE);
            listRequest.add(request);
        }
        return listRequest;
    }   
    
    //This method applies the listPrice to the unitPrice(sales Price) in case of Approval being rejected
    public static void productPriceCorrection(set<String> lstOppId){
        List<OpportunityLineItem> lstOli = [SELECT id, UnitPrice, ListPrice, Discount 
                                            FROM OpportunityLineItem WHERE OpportunityId =: lstOppId]; 
        for(OpportunityLineItem oli : lstOli){
            oli.UnitPrice = oli.ListPrice; 
            oli.Discount = null;
        }   
        if(!lstOli.isEmpty()) Database.update(lstOli);
        
    }
}


/*
// old
public static void execute()
{
TriggerUtils.assertTrigger();
List<Opportunity> listOpp2ApproveEvents = new List<Opportunity>();
List<Opportunity> listOpp2ApproveMembership = new List<Opportunity>();
List<Opportunity> listOpp2ApproveSpex = new List<Opportunity>();
List<Opportunity> lListApplOppDisc = new List<Opportunity>();
List<Opportunity> listOpp2ApproveSmartusEv = new List<Opportunity>();
List<Opportunity> listOpp2ApproveSmartusSpex = new List<Opportunity>();
List<Opportunity> listOpp2ApproveCurrency = new List<Opportunity>();
Set<String> lstOppAPId = new Set<String>();   
List<Approval.ProcessSubmitRequest> listApprovalSend = new List<Approval.ProcessSubmitRequest>();

for(ProcessDefinition objCS : [SELECT DeveloperName,State FROM ProcessDefinition ORDER BY State ASC NULLS FIRST])
{
mapAppProcess.put(objCS.DeveloperName, objCS.State);
}

boolean lValidationE = FALSE;
boolean lValidationF = FALSE;

// anderson
//Id SmartusRecTypeEv   = Schema.getGlobalDescribe().get('Opportunity').newSObject().getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('Smartus_Events').getRecordTypeId();
//Id SmartusRecTypeSpex = Schema.getGlobalDescribe().get('Opportunity').newSObject().getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('Smartus_Spex').getRecordTypeId();
//Id SmartusRecTypeEv   = OrgDefs.RT_OPP_OPENEVENTS; // wrong
//Id SmartusRecTypeSpex   = OrgDefs.RT_OPP_OPENEVENTS; // wrong
Id SmartusRecTypeEv = RecordTypeMemory.getRecType( 'Opportunity', 'Smartus_Events');
Id SmartusRecTypeSpex = RecordTypeMemory.getRecType( 'Opportunity', 'Smartus_Spex');

for(Opportunity opp : (List<Opportunity>)Trigger.new)
{       

lValidationE = (opp.Discount_of_opportunity__c > 0
&& opp.Approval_Process__c != '1 - Approved'
&& !TriggerUtils.wasChanged(opp, Opportunity.StageName));

lValidationE &= (TriggerUtils.wasChanged(opp, Opportunity.Discount_of_opportunity__c) 
|| TriggerUtils.wasChanged(opp, Opportunity.Sales_price_List_price__c));

lValidationF = (TriggerUtils.wasChanged(opp, Opportunity.Different_prices__c)
&& opp.Different_prices__c > 0);

if(TriggerUtils.wasChangedTo(opp, Opportunity.Approval_Process__c, '3 - Rejected')) lstOppAPId.add(opp.id);

if(opp.Payment_Current_Currency__c) listOpp2ApproveCurrency.add(opp);

if(!lValidationE && !lValidationF && opp.Approval_Process__c != '2 - Pending') continue;

if(opp.RecordTypeId == OrgDefs.RT_OPP_OPENEVENTS)
listOpp2ApproveEvents.add(opp);

else if(opp.RecordTypeId == OrgDefs.RT_OPP_MEMBERSHIP)
listOpp2ApproveMembership.add(opp);

else if(opp.RecordTypeId == OrgDefs.RT_OPP_UNIQUE)
listOpp2ApproveSpex.add(opp);

// anderson
else if(opp.RecordTypeId == SmartusRecTypeEv)
listOpp2ApproveSmartusEv.add(opp);
else if(opp.RecordTypeId == SmartusRecTypeSpex)
listOpp2ApproveSmartusSpex .add(opp);


// Schema.getGlobalDescribe().get('Opportunity').newSObject().getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('Open_Events').getRecordTypeId()  

}

if(!lstOppAPId.isEmpty()) productPriceCorrection(lstOppAPId);

if(!listOpp2ApproveEvents.isEmpty())
listApprovals.addAll(approvalOpportunityEvent(listOpp2ApproveEvents));

if(!listOpp2ApproveMembership.isEmpty())
listApprovals.addAll(approvalOpportunityMembership(listOpp2ApproveMembership));

if(!listOpp2ApproveSpex.isEmpty())
listApprovals.addAll(approvalOpportunitySpex(listOpp2ApproveSpex)); 

// anderson
// smartus events
if(!listOpp2ApproveSmartusEv.isEmpty())
listApprovals.addAll(approvalOpportunityEvent(listOpp2ApproveSmartusEv)); 
// smartus spex
if(!listOpp2ApproveSmartusSpex.isEmpty())
listApprovals.addAll(approvalOpportunitySpex(listOpp2ApproveSmartusSpex));                 


if(!listOpp2ApproveCurrency.isEmpty())
listApprovals.addAll(approvalOpportunityCurrency(listOpp2ApproveCurrency));

Approval.ProcessResult[] result;
if(!listApprovals.isEmpty()) result = Approval.process(listApprovals, FALSE);

}
*/