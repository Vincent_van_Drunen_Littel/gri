/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * This class updates the Opportunity Sales division field wih the User updated Title,
 * when it is passed to Approved by Finance and/or Won.
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * Date        Author         Description
 * 19/07/18    AndPaschoalon  First version
 */

public class OpportunityUpdateSalesDivision 
{
    public static String OPP_WON = HelperSObject.OPPORTUNITY_STAGE_WON;
    public static String OPP_APP_FINANCE = HelperSObject.OPPORTUNITY_STAGE_APPROVED_BY_FINANCE;
    public static Boolean isExecuting = false; //prevent recursivity 
    
    /**
     * Execute the Update of the Opportunity. Called by the Opportunity Trigger
     * @param newOpps - List of updated Opportunities (Trigger.new)
     * @param oldOpps - List of old values of updated Opps (Trigger.old)
     */
    public static void execute(List<Opportunity> newOpps, List<Opportunity> oldOpps)
    {
        System.debug('%% Trigger OpportunityUpdateSalesDivision');
        assertTriggerInput(newOpps, oldOpps);
        List<Opportunity> listOpp=salesDivisionOpps(newOpps, oldOpps);
        
        if(listOpp.size()==0) return;
        if(isExecuting) return;
        isExecuting=true;
        
        Set<String> setOppOwnersIds = HelperSObject.setFromField(listOpp, 'OwnerId');
        Map<Id, User> mapUsers = new Map<Id, User>([SELECT Id, Name, Title FROM User WHERE Id=:setOppOwnersIds]);
        System.debug('%% OpportuntiyUpdateSalesDivision: setOppOwnersIds.size()='+
                     setOppOwnersIds.size()+', mapUsers.size()='+mapUsers.size());
        for(Integer i=0; i<listOpp.size(); i++)
        {
            User oppOwner = mapUsers.get(listOpp[i].OwnerId);
            if(oppOwner==NULL)
                throw new OpportuntiyUpdateSalesDivisionException('Error on trigger `OpportunityUpdateSalesDivision` for Opportunity '+ 
                                                                  listOpp[i].Name+': user was not found on Database. Opp.OwnserId='+ listOpp[i].OwnerId);
            listOpp[i].Sales_Division_new__c = oppOwner.Title;
        }
    }
    
    /*
    public static void execute2()
    {
        List<Opportunity> newOpps = Trigger.new;
        List<Opportunity> oldOpps = Trigger.old;
        assertTriggerInput(newOpps, oldOpps);
        List<Opportunity> listOpp=salesDivisionOpps(newOpps, oldOpps);
        if(listOpp.size()==0) return;
        if(isExecuting) return;
        isExecuting=true;
        
        Set<String> setOppOwnersIds = HelperSObject.setFromField(listOpp, 'OwnerId');
        Map<Id, User> mapUsers = new Map<Id, User>([SELECT Id, Name, Title FROM User WHERE Id=:setOppOwnersIds]);
        System.debug('%% OpportuntiyUpdateSalesDivision: setOppOwnersIds.size()='+
                     setOppOwnersIds.size()+', mapUsers.size()='+mapUsers.size());
        for(Integer i=0; i<listOpp.size(); i++)
        {
            User oppOwner = mapUsers.get(listOpp[i].OwnerId);
            if(oppOwner==NULL)
                throw new OpportuntiyUpdateSalesDivisionException('Error on trigger `OpportunityUpdateSalesDivision` for Opportunity '+ 
                                                                  listOpp[i].Name+': user was not found on Database. Opp.OwnserId='+ listOpp[i].OwnerId);
            listOpp[i].Sales_Division_new__c = oppOwner.Title;
        }
    }
    */
    
    // asserts if the new and old opps size matches
    private static void assertTriggerInput(List<Opportunity> newOpps, List<Opportunity> oldOpps)
    {
        if(newOpps.size()!=oldOpps.size())
        {
            throw new OpportuntiyUpdateSalesDivisionException(
                'Trigger Error at class:`OpportuntiyUpdateSalesDivision`.'+
                'The size of newOpps(Trigger.new) and oldOpps(Trigger.old) dont match. '+
                'newOpps.size()='+newOpps.size()+', oldOpps.size()='+oldOpps.size());
        }  
    }
    
    // returns a list of Opportunities that matches the Sales Division Update requirement
    private static List<Opportunity> salesDivisionOpps(List<Opportunity> newOpps, List<Opportunity> oldOpps)
    {
        List<Opportunity> listOpp = new List<Opportunity>();
        for(Integer i=0; i<newOpps.size(); i++ )
        {
            if (HelperSObject.wasChangedTo(newOpps[i], oldOpps[i], 'StageName', OPP_WON)||
                HelperSObject.wasChangedTo(newOpps[i], oldOpps[i], 'StageName', OPP_APP_FINANCE))
            {
                listOpp.add(newOpps[i]);
            }
        }
        System.debug('%% OpportuntiyUpdateSalesDivision: listOpp.size()='
                     +listOpp.size()+', isExecuting='+isExecuting);
        return listOpp;
    }
    
    // OpportuntiyUpdateSalesDivision exceptions
    public class OpportuntiyUpdateSalesDivisionException extends Exception {} 
    
}