@isTest
public class Test_CalendarInviteHandler {

    private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');

  @isTest
  static void TesteInviteHandler1()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);
    
    Application_Config__c cs = new Application_Config__c();
    cs.Name='BucketUserId';
    cs.value__c ='00536000004lQpq';
        //cs.Other fiels values
    insert cs;
    
    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Special Lunch';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.Visibility__c =  'private';
    insert calNewObj;
    
    Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
    calInviteeObj.Calendar_Invite__c = calNewObj.Id;
    calInviteeObj.Invite_Status__c = 'Pending';
    calInviteeObj.InviteeEmail__c = 'test123@fdsaf.com';
    calInviteeObj.Opportunity__c = iOpp.Id;
    insert calInviteeObj;
    
    
    Test.startTest();
    
        GoolgeCalendarCalloutMock fakeResponse = new GoolgeCalendarCalloutMock(200,
                                                 'Complete',
        //'[{"kind": "calendar#event","etag": "\"3086257014590000\"","id": "i7rl42kjm8qp26s792h9j17gbs","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=aTdybDQya2ptOHFwMjZzNzkyaDlqMTdnYnMgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-25T06:48:27.000Z","updated": "2018-11-25T06:48:27.295Z","summary": "GRI Hoteis 2018 - Day 5","location": "Hotel Grand Hyatt São Paulo , Av. Das Nações Unidas, 13.301, São Paulo, SP, Brazil","creator": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-25T04:48:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-25T09:48:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "i7rl42kjm8qp26s792h9j17gbs@google.com","sequence": 0,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"}]}]',
'[{"access_token": "ya29.Gl1fBgy8LaEDcBiYocc6kgZGejKrZz1j9_JSJ1zPvHwtC_uOyfOKctTMBqFTZFhWOKZwLdp3PI2dVgl7GBEfWeKPGyGsi0OecyiTBbvsvpqEJmls5h-O7MyGL9SRcN4","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',     
       null);
    Test.setMock(HttpCalloutMock.class, fakeResponse);
    
    Test.StopTest();
  }
  @isTest
  static void TesteInviteHandler2()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);
    
    Application_Config__c cs = new Application_Config__c();
    cs.Name='BucketUserId';
    cs.value__c ='00536000004lQpq';
        //cs.Other fiels values
    insert cs;
    
    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    try{
        Calendar_Invite__c calNewObj = new Calendar_Invite__c();
        calNewObj.Event__c = iEvent.Id;
        calNewObj.Description__c = 'test Description';
        calNewObj.End_Date__c = system.today()+2;
        calNewObj.Invite_Type__c = 'Special Lunch';
        calNewObj.See_Guest_List__c = false;
        calNewObj.Send_Notification__c = 15;
        calNewObj.Start_Date__c = system.today();
        //calNewObj.Time_Zone__c = iEvent.Timezone__c;
        //calNewObj.Venue__c = iEvent.Venue__c;
        calNewObj.Visibility__c =  'private';
        insert calNewObj;
        
        Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
        calInviteeObj.Calendar_Invite__c = calNewObj.Id;
        calInviteeObj.Invite_Status__c = 'Pending';
        calInviteeObj.InviteeEmail__c = 'test123@fdsaf.com';
        calInviteeObj.Opportunity__c = iOpp.Id;
        insert calInviteeObj;
        
        
        Test.startTest();
        
            GoolgeCalendarCalloutMock fakeResponse = new GoolgeCalendarCalloutMock(200,
                                                     'Complete',
            '[{"kind": "calendar#event","etag": "\"3086257014590000\"","id": "i7rl42kjm8qp26s792h9j17gbs","status": "confirmed","htmlLink": "https://www.google.com/calendar/event?eid=aTdybDQya2ptOHFwMjZzNzkyaDlqMTdnYnMgZ3JpY2x1YkBncmljbHViLm9yZw","created": "2018-11-25T06:48:27.000Z","updated": "2018-11-25T06:48:27.295Z","summary": "GRI Hoteis 2018 - Day 5","location": "Hotel Grand Hyatt São Paulo , Av. Das Nações Unidas, 13.301, São Paulo, SP, Brazil","creator": {"email": "griclub@griclub.org","self": true},"start": {"dateTime": "2018-11-25T04:48:00-02:00","timeZone": "America/Sao_Paulo"},"end": {"dateTime": "2018-11-25T09:48:00-02:00","timeZone": "America/Sao_Paulo"},"iCalUID": "i7rl42kjm8qp26s792h9j17gbs@google.com","sequence": 0,"attendees": [{"email": "griclub@griclub.org","organizer": true,"self": true,"responseStatus": "needsAction"}]}]',
            //'[{"access_token": "ya29.Gl1fBgy8LaEDcBiYocc6kgZGejKrZz1j9_JSJ1zPvHwtC_uOyfOKctTMBqFTZFhWOKZwLdp3PI2dVgl7GBEfWeKPGyGsi0OecyiTBbvsvpqEJmls5h-O7MyGL9SRcN4","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',     
           null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
    
        Test.StopTest();
    }catch(exception e){
    
    }
  }
  @isTest
  static void TesteInviteHandlerDelete()
  {
    Account iConta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(iConta);

    Contact iContato = SObjectInstance.createContato(iConta.Id);
    Database.insert(iContato);

    Event__c iEvent = SObjectInstance.createEvent();
    Database.insert(iEvent);

    Session__c iSession = new Session__c();
    iSession.Event__c = iEvent.Id;
    iSession.Type__c = 'Discussion';
    iSession.Start_date__c = System.Today();
    iSession.End_Date__c = System.Today();
    iSession.Status__c = 'In production';
    iSession.Title__c = 'Teste Teste';
    Database.Insert(iSession);
    
    Application_Config__c cs = new Application_Config__c();
    cs.Name='BucketUserId';
    cs.value__c ='00536000004lQpq';
        //cs.Other fiels values
    insert cs;
    
    Opportunity iOpp = new Opportunity();
    iOpp.RecordTypeId = ID_EVENT;
    iOpp.Name = 'Teste';
    iOpp.Contact__c = iContato.Id;
    iOpp.Event__c = iEvent.Id;
    iOpp.CloseDate = System.Today();
    iOpp.StageName = 'Prospect';
    iOpp.CurrencyIsoCode = 'USD';
    Database.insert(iOpp);

    Attendee__c iSessionAttendee = new Attendee__c();
    iSessionAttendee.Opportunity__c = iOpp.Id;
    iSessionAttendee.Session__c = iSession.Id;
    iSessionAttendee.Session_Status__c = 'Maybe';
    iSessionAttendee.Session_Position__c = 'Co-Chair';
    iSessionAttendee.Event__c = iEvent.Id;
    Database.Insert(iSessionAttendee);
    
    Calendar_Invite__c calNewObj = new Calendar_Invite__c();
    calNewObj.Event__c = iEvent.Id;
    calNewObj.Description__c = 'test Description';
    calNewObj.End_Date__c = system.today()+2;
    calNewObj.Invite_Type__c = 'Special Lunch';
    calNewObj.See_Guest_List__c = false;
    calNewObj.Send_Notification__c = 15;
    calNewObj.Start_Date__c = system.today();
    //calNewObj.Time_Zone__c = iEvent.Timezone__c;
    //calNewObj.Venue__c = iEvent.Venue__c;
    calNewObj.Visibility__c =  'private';
    calNewObj.GoogleCalendarEventId__c = 'd2ro10reknvpm8i9hkqac53hu0';
    calNewObj.GoogleCalendarId__c = 'testt.ssd@gmail.com';
    insert calNewObj;
    
    delete calNewObj;
    
    Test.startTest();
     

    GoolgeCalendarCalloutMock fakeResponse1 = new GoolgeCalendarCalloutMock(200,
                                                 'Complete',
     '[{"access_token": "ya29.Gl1gBgzYvJYRwjcYbkNHqzWtkI0GVNcrbE7BIwZMQ7WmmHVh4foDfaV4Pk290GfIUAqjRDfaF2UI3SsPCRCdoMtulds8yKOMG2E5OrDjQy02dJAJeiqJlmW9iTO2Mvg","expires_in": 3600,"scope": "https://www.googleapis.com/auth/calendar","token_type": "Bearer"}]',
       null);
    Test.setMock(HttpCalloutMock.class, fakeResponse1);

    Test.StopTest();
  }
}