/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe responsável por alterar as oportunidade após alterações efetuadas
* em pricebook.
*
* NAME: PricebookUpdatesOportunidades.cls
* AUTHOR: EAD                                                  DATE: 07/02/2018
*******************************************************************************/

public class PricebookUpdatesOportunidades{

  public static void execute(){
    TriggerUtils.assertTrigger();
    set<String> lstPbId = new set<String>();
      
    for(Pricebook2 pb : (list<Pricebook2>)Trigger.new){        
      if(TriggerUtils.wasChanged(pb, Pricebook2.Starting_date__c) || TriggerUtils.wasChanged(pb, Pricebook2.End_date__c))
        lstPbId.add(pb.id);      
    }
    
    if(!lstPbId.isEmpty()) SchUpdateOppByPricebook.iniciar(lstPbId);   
  }

}