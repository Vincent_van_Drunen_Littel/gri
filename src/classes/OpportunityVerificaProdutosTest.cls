/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste da classe "OpportunityVerificaProdutos".
*
* NAME: OpportunityVerificaProdutosTest.cls
* AUTHOR: RVR                                                  DATE: 09/06/2016
*******************************************************************************/
@isTest
private class OpportunityVerificaProdutosTest 
{
	private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );

	static testMethod void 	testeFuncional()
  {

  	Account acc = SObjectInstance.createAccount2();
    acc.ShippingStreet     = '1 Main St.';
    acc.ShippingState    = 'VA';
    acc.ShippingPostalCode  = '12345';
    acc.ShippingCountry    = 'USA';
    acc.ShippingCity      = 'Anytown';
    acc.Description      = 'This is a test account';
    acc.BillingStreet    = '1 Main St.';
    acc.BillingState      = 'VA';
    acc.BillingPostalCode  = '12345';
    acc.BillingCountry     = 'USA';
    acc.BillingCity      = 'Anytown';
    acc.AnnualRevenue    = 10000;
    acc.ParentId        = null;
  	Database.insert(acc);

  	Contact ctt = SObjectInstance.createContato(acc.Id);
  	ctt.LastName = 'Teste Cloud2b Contact';
  	Database.insert(ctt);

  	Event__c evento = SObjectInstance.createEvent();
  	Database.insert(evento);

  	Id catalogo = SObjectInstance.catalogoDePrecoPadrao2();

    Pricebook2 catalogo2 = SObjectInstance.catalogoDePreco();
    catalogo2.Event__c = evento.Id;
    catalogo2.IsActive = true;
    catalogo2.Starting_Date__c = system.today();
    catalogo2.End_Date__c = system.today()+32;
    Database.insert(catalogo2);

    Product2 produto = SObjectInstance.createProduct2();
    produto.Name = 'Teste Clou2d Produto';
    produto.IsActive = true;
    Database.insert(produto);    

    list<PricebookEntry> lstPbeOld = new list<PricebookEntry>();
    list<PricebookEntry> lstPbeNew = new list<PricebookEntry>();

    PricebookEntry pbe = SObjectInstance.entradaDePreco(catalogo, produto.Id);
    pbe.Product2Id = produto.Id;
    Database.insert(pbe);
    lstPbeOld.add(pbe);

    PricebookEntry pbe2 = SObjectInstance.entradaDePreco(catalogo2.Id, produto.Id);
    pbe2.Product2Id = produto.Id;
    Database.insert(pbe2);
    lstPbeOld.add(pbe2);

    Test.startTest();
    OpportunityUpdatePricebookEProduto.isExecuting = false;

  	list<Opportunity> lstOpp = new list<Opportunity>();
  	Opportunity opp = SObjectInstance.createOpportunidade(acc.Id, REC_OPP_OPEN_EVENTS);
  	opp.Name = 'Teste Cloud2b opp';
  	opp.Contact__c = ctt.Id;
  	opp.Event__c = evento.Id;
  	opp.CloseDate = system.today()+1;
  	opp.StageName = 'Approved';
  	opp.Description = 'teste';
  	Database.SaveResult lstTestResult = Database.insert(opp, false);

    lstPbeOld.clear();
    lstPbeNew.add(pbe);
    lstPbeNew.add(pbe2);
  	Test.stopTest();
	}
}