/**
 * Created by Eric Bueno on 02/02/2019.
 */

@IsTest
public with sharing class ClubRESTTest {

    @TestSetup
    static void setup(){

        TestDataFactory.init();

        Integer nConPerAcc = 1;
        // Create Club__c
        Club__c club = TestDataFactory.createClubs(TestDataFactory.CLUB_RE_LATAM);

        // Create Event__c
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent', club);
        theGreatEvent.Vip_Dinner_Included__c = true;
        theGreatEvent.Publish_on_Website__c = true;
        update theGreatEvent;

        // Create Contact and Account
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(nConPerAcc);

        // Create Contract (Membership)
        List<Contract> listContract = TestDataFactory.createMemberships(listContacts, club, System.today().addDays(2));

        // Add Account to Contract
        for(Contract contract : listContract){
            contract.AccountId = listContacts[0].AccountId;
            contract.Status = 'Activated';
        }

        update listContract;

        // Cria uma conta para o Sponsor
        Account accountSponsor = TestDataFactory.createAccounts(new List<String>{'Account Sponsor'})[0];

        // Cria um Sponsor para o evento
        Order order = TestDataFactory.createOrderWithActEvtClub(accountSponsor, theGreatEvent, club);

    }

    @IsTest
    static void testFindById(){

        Club__c club = [SELECT Id, (SELECT Id, Name FROM Orders__r) FROM Club__c LIMIT 1];

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/api/club/find-by-id';
        request.httpMethod = 'GET';
        request.addParameter('club-id',club.Id);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        ClubREST.processGet();

        Test.stopTest();

        // get response in map
        Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        SiteTO.ClubTO clubTO = (SiteTO.ClubTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('data')), SiteTO.ClubTO.class);
        ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')),ReturnRestTO.MetaTO.class);

        System.debug('PAYLOAD DE RETORNO --------------------------------- ' + JSON.serialize(clubTO));

        // Verifica se a resposta foi positiva
        System.assertEquals(RestContext.response.statusCode,200);
        System.assertEquals(metaTO.statusHttp,200);
        // Verifica se o club foi encontrado
        System.assert(clubTO != null);
        System.assertEquals(clubTO.id,club.Id);

        // Testa se veio o contato e a conta da empresa
        System.assert(clubTO.company != null);
        System.assert(clubTO.company.accountId != null);
        System.assert(clubTO.company.contactId != null);

        // Testa se vieram os patrocinadores
        System.assert(!clubTO.sponsors.isEmpty());
        // Testa se veio a conta do patrocinador
        System.assert(clubTO.sponsors[0].accountId != null);
        // Testa se veio o club do patrocinador
        System.assert(clubTO.sponsors[0].clubId != null);

    }

    @IsTest
    static void testFindByIdWhenNotFound(){

        Club__c club = [SELECT Id FROM Club__c LIMIT 1];

        String clubId = String.valueOf(club.Id);
        clubId = clubId.substring(0,clubId.length() - 2) + '00';

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/api/club/find-by-id';
        request.httpMethod = 'GET';
        request.addParameter('club-id',clubId);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        ClubREST.processGet();

        Test.stopTest();

        // get response in map
        Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        SiteTO.ClubTO clubTO = (SiteTO.ClubTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('data')), SiteTO.ClubTO.class);
        ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')),ReturnRestTO.MetaTO.class);

        System.debug('PAYLOAD DE RETORNO --------------------------------- ' + clubTO);


        System.assertEquals(RestContext.response.statusCode,400);
        System.assertEquals(metaTO.statusHttp,400);
        System.assert(clubTO == null);
        System.assertEquals(metaTO.message,'Club not found or does not match search criteria');

    }

    @IsTest
    static void testFindSponsorsByClubId(){

        Club__c club = [SELECT Id, (SELECT Id, Name FROM Orders__r) FROM Club__c LIMIT 1];

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/api/club/find-sponsors-by-club-id';
        request.httpMethod = 'GET';
        request.addParameter('club-id',club.Id);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        ClubREST.processGet();

        Test.stopTest();

        // get response in map
        Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        SiteTO.ClubTO clubTO = (SiteTO.ClubTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('data')), SiteTO.ClubTO.class);
        ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO) JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')),ReturnRestTO.MetaTO.class);

        System.debug('PAYLOAD DE RETORNO --------------------------------- ' + JSON.serialize(clubTO));

        // Verifica se a resposta foi positiva
        System.assertEquals(RestContext.response.statusCode,200);
        System.assertEquals(metaTO.statusHttp,200);
        // Verifica se o club foi encontrado
        System.assert(clubTO != null);
        System.assertEquals(clubTO.id,club.Id);
        // Testa se vieram os patrocinadores
        System.assert(!clubTO.sponsors.isEmpty());
        // Testa se veio a conta do patrocinador
        System.assert(clubTO.sponsors[0].accountId != null);
        // Testa se veio o club do patrocinador
        System.assert(clubTO.sponsors[0].clubId != null);

    }

}