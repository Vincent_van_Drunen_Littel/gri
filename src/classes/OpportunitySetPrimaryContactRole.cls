/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe para vincular Contato da Oportunidade como Contact Role
*
* NAME: OpportunitySetPrimaryContactRole.cls
* AUTHOR: KHPS                                                  DATE: 06/04/2016
*******************************************************************************/
public with sharing class OpportunitySetPrimaryContactRole {

    public static void execute() {
        
        TriggerUtils.assertTrigger();

        list<OpportunityContactRole> lstInsertContactRole = new list<OpportunityContactRole>();

        for(Opportunity opp : (list<Opportunity>)trigger.new) {

            OpportunityContactRole oppCttRole = new OpportunityContactRole();
            if(opp.Contact__c != null)
            {
                oppCttRole.ContactId = opp.Contact__c;
            }else{
                continue;
            }
            oppCttRole.OpportunityId = opp.Id;
            oppCttRole.IsPrimary = true;
            oppCttRole.Role = 'Decision Maker';
            lstInsertContactRole.add(oppCttRole);
        }

        if(!lstInsertContactRole.isEmpty()) insert lstInsertContactRole;

    }

}