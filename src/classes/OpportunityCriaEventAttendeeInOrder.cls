/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsável por criar/excluir registro Event Attendee conforme
* informações e status da Oportunidade dentro do Pedido.
*
* NAME: OpportunityCriaEventAttendeeInOrder.cls
* AUTHOR: VMDL                                                 DATE: 11/08/2016
*******************************************************************************/
public with sharing class OpportunityCriaEventAttendeeInOrder {

	private static Id REC_OPP_SPEX = RecordTypeMemory.getRecType( 'Opportunity', 'SPEX' );

	private static Id REC_ORD_ADD_PASSES = RecordTypeMemory.getRecType( 'Order', 'Aditional_Passes' );
	private static Id REC_ORD_EVENT_SPONSOR = RecordTypeMemory.getRecType( 'Order', 'Event_Sponsor' );

	public static void execute()
	{
		TriggerUtils.assertTrigger();

		Set<String> setStageName = new Set<String>{'Approved', 'Approved by Finance', 'Won', 'Cancelled', 'Lost'};
		Set<String> setStageNameToDel = new Set<String>{'Suspect', 'Prospect', 'Information Sent', 'Negotiation', 'Closing'};
		Set<Id> setRecordIdOrder = new Set<Id>{REC_ORD_ADD_PASSES, REC_ORD_EVENT_SPONSOR};

		map<Id, Opportunity> mapOpp = new map<Id, Opportunity>();

		for(Opportunity opp : (list<Opportunity>) trigger.new)
		{	
			//Para criar Event Attendee
			if(TriggerUtils.wasChanged( opp, Opportunity.StageName ) && setStageName.contains(opp.StageName) && opp.RecordTypeId == REC_OPP_SPEX)
			{
			   mapOpp.put(opp.Id, opp);
			}
			
			if(TriggerUtils.wasChanged( opp, Opportunity.StageName ) && setStageNameToDel.contains(opp.StageName) && opp.RecordTypeId == REC_OPP_SPEX) 
			{
				//Para excluir Event Attendee
				mapOpp.put(opp.Id, opp);
			}
		}
		if(mapOpp.isEmpty()) return;

		//selecionar a Order
		list<Order> lstOrder = new list<Order>();
		set<Id> setIdOrderOpp = new set<Id>();	
		for(Order iOdr : [Select Id, Name, OpportunityId, Event__c,
				   Opportunity.Contact__c, Opportunity.AccountId, Opportunity.StageName
				   FROM Order
				   Where OpportunityId =: mapOpp.keyset()
				   And RecordTypeId =: setRecordIdOrder])
		{
			setIdOrderOpp.add(iOdr.OpportunityId);
			lstOrder.add(iOdr);
		}

		//entrar no event e criar o Event Attendee
		list<Event_Attendee__c> lstEventAtt = [SELECT Id, Contact__c, Opportunity__c, 
											  Event_Attendees_Status__c, Event__c 
											  FROM Event_Attendee__c 
											  Where Opportunity__c =: setIdOrderOpp];

		//Criar a quantidade de Attendee se não existir nada na lista...
		if(lstEventAtt.isEmpty() || lstEventAtt == null)
		{	
			for(Order iOdr: lstOrder)
			{
				Event_Attendee__c objEvent = new Event_Attendee__c();
				objEvent.Opportunity__c = iOdr.OpportunityId;
				objEvent.Account__c = iOdr.Opportunity.AccountId;
				objEvent.Contact__c = iOdr.Opportunity.Contact__c;
				objEvent.Event__c = iOdr.Event__c;
				objEvent.Event_Attendees_Status__c = (iOdr.Opportunity.StageName == 'Approved') ? 'Processing' : 
													 (iOdr.Opportunity.StageName == 'Approved by Finance')	? 'Confirmed' :
													 (iOdr.Opportunity.StageName  == 'Won') ? 'Registered' : 
												 	 (iOdr.Opportunity.StageName  == 'Cancelled') ? 'Not Attending' : 'Cancelled';
				if(iOdr.Opportunity.StageName  == 'Approved' || iOdr.Opportunity.StageName  == 'Approved by Finance') lstEventAtt.add(objEvent);
			}

			if(!lstEventAtt.isEmpty()) Database.insert(lstEventAtt);
			return;
		}
		
		//se nao for vazia faz update ou delete dependendo do stageName da opp SPEX
		list<Event_Attendee__c> lstToUpdateEventAtt = new list<Event_Attendee__c>(); 
		list<Event_Attendee__c> lstEventToDel = new list<Event_Attendee__c>(); 
		for(Event_Attendee__c iEventAtt : lstEventAtt)
		{
			Opportunity lOpp = mapOpp.get(iEventAtt.Opportunity__c);
			if(lOpp == null) continue;
			if(lOpp.Id == iEventAtt.Opportunity__c)
			{
				if(setStageName.contains(lOpp.StageName))
				{
					Event_Attendee__c objEvent = new Event_Attendee__c(Id = iEventAtt.Id);
					objEvent.Event_Attendees_Status__c = (lOpp.StageName == 'Approved') ? 'Processing' : 
														 (lOpp.StageName == 'Approved by Finance')	? 'Confirmed' :
														 (lOpp.StageName  == 'Won') ? 'Registered' : 
													 	 (lOpp.StageName  == 'Cancelled') ? 'Not Attending' : 'Cancelled';
					lstToUpdateEventAtt.add(objEvent);
				}
				else if(setStageNameToDel.contains(lOpp.StageName))
				{
					Event_Attendee__c objEvent = new Event_Attendee__c(Id = iEventAtt.Id);
					lstEventToDel.add(objEvent);
				}
			}
		}
		if(!lstToUpdateEventAtt.isEmpty()) Database.update(lstToUpdateEventAtt);
		if(!lstEventToDel.isEmpty()) Database.delete(lstEventToDel);
	}
}