/**
 * - make it by default inactive on tests
 * - just run the tests
*/
public class CheckRecursive2 
{
    // this variable disable this class if is set True. Any request, will return false in this case. 
    // So, if any variable is set as True, it will return false anyway
    @TestVisible private static Boolean m_DisableCheckRecursive2 = False; 
    @TestVisible private static Boolean m_DisableCheckRecursive2OnTest = True; 
    private static Integer m_counter = 0;
    private static Boolean m_GDPRTriggerHandlerIsActive = False; //1
    private static Boolean m_AlteraEventAttendeeIsActive = False; //2
    private static Boolean m_GlobalApprovalProcessIsActive = False; //3
    private static Boolean m_AtualizaLastOpportunityWonIsActive = False; //4
    private static Boolean m_AtualizaSessionAttendeeIsActive = False; //5
    private static Boolean m_CriaEventAttendeeIsActive = False;  //6
    private static Boolean m_CountOppOpenEventIsActive = False; //7
    private static Boolean m_UpdatePricebookEProdutoIsActive = False; //8
	private static Boolean m_ApprovalCommitteeManagerIsActive = False;
    private static Boolean m_DiamondMemberAutoApprovalIsActive = False;

    public static Boolean DisableCheckRecursive2OnTest
    {
        get {return m_DisableCheckRecursive2OnTest; }
        set {m_DisableCheckRecursive2OnTest = value;}        
    }
    public static Boolean DisableCheckRecursive2
    {
        get {return m_DisableCheckRecursive2; }
        set
        {
            m_DisableCheckRecursive2 = value;
            if(isDisabled()) 
                System.debug('%% Warning: CheckRecursive2 was disabled out of the Test context.');
        }        
    }
    public static Integer conter()
    {
        m_counter++;
        System.debug('%% ***** CheckRecursive2 conter is '+ m_counter);
        return m_counter;
    }
    public  static String conterValue()
    {
		return ' ['+ String.valueOf(m_counter) + '] ';
    }
    
    public static Boolean GDPRTriggerHandlerIsActive //1
    { 
        get 
        {
            System.debug('%% CheckRecursive2.GDPRTriggerHandlerIsActive is '+m_GDPRTriggerHandlerIsActive+conterValue());
            return m_GDPRTriggerHandlerIsActive;
        } 
        set 
        {
            m_GDPRTriggerHandlerIsActive = value; 
            if(isDisabled()) m_GDPRTriggerHandlerIsActive = False;
            System.debug('%% CheckRecursive2.GDPRTriggerHandlerIsActive was set '+m_GDPRTriggerHandlerIsActive+conterValue());
        } 
    }  
    
    public static Boolean AlteraEventAttendeeIsActive  //2
    { 
    	get 
        {
            System.debug('%% CheckRecursive2.AlteraEventAttendeeIsActive is '+m_AlteraEventAttendeeIsActive+conterValue());
            return m_AlteraEventAttendeeIsActive;
        } 
        set 
        {
            m_AlteraEventAttendeeIsActive = value;
            if(isDisabled()) m_AlteraEventAttendeeIsActive = False;
            System.debug('%% CheckRecursive2.AlteraEventAttendeeIsActive was set '+m_AlteraEventAttendeeIsActive+conterValue());
        } 
    }  
    
	public static Boolean GlobalApprovalProcessIsActive //3 
    { 
        get 
        {
            System.debug('%% CheckRecursive2.GlobalApprovalProcessIsActive is '+m_GlobalApprovalProcessIsActive+conterValue());
            return m_GlobalApprovalProcessIsActive;
        } 
        set 
        {
            m_GlobalApprovalProcessIsActive = value;
            if(isDisabled()) m_GlobalApprovalProcessIsActive = False;
            System.debug('%% CheckRecursive2.GlobalApprovalProcessIsActive was set '+m_GlobalApprovalProcessIsActive+conterValue());
        } 
    }  

    public static Boolean AtualizaLastOpportunityWonIsActive //4
    { 
        get 
        {
            System.debug('%% CheckRecursive2.AtualizaLastOpportunityWonIsActive is '+m_AtualizaLastOpportunityWonIsActive+conterValue());
            return m_AtualizaLastOpportunityWonIsActive;
        } 
        set 
        {
            m_AtualizaLastOpportunityWonIsActive = value;
            if(isDisabled()) m_AtualizaLastOpportunityWonIsActive = False;
            System.debug('%% CheckRecursive2.AtualizaLastOpportunityWonIsActive was set '+m_AtualizaLastOpportunityWonIsActive+conterValue());
        } 
    }  
    
	public static Boolean AtualizaSessionAttendeeIsActive //5
    { 
        get 
        {
            System.debug('%% CheckRecursive2.AtualizaSessionAttendeeIsActive is '+m_AtualizaSessionAttendeeIsActive+conterValue());
            return m_AtualizaSessionAttendeeIsActive;
        } 
        set 
        {
            m_AtualizaSessionAttendeeIsActive = value;
            if(isDisabled()) m_AtualizaSessionAttendeeIsActive = False;
            System.debug('%% CheckRecursive2.AtualizaSessionAttendeeIsActive was set '+m_AtualizaSessionAttendeeIsActive+conterValue());
        } 
    }  
    
	public static Boolean CriaEventAttendeeIsActive //6
    { 
        get 
        {
            System.debug('%% CheckRecursive2.CriaEventAttendeeIsActive is '+m_CriaEventAttendeeIsActive+conterValue());
            return m_CriaEventAttendeeIsActive;
        } 
        set 
        {
            m_CriaEventAttendeeIsActive = value;
            if(isDisabled()) m_CriaEventAttendeeIsActive = False;
            System.debug('%% CheckRecursive2.CriaEventAttendeeIsActive was set '+m_CriaEventAttendeeIsActive+conterValue());
        } 
    }  

	public static Boolean CountOppOpenEventIsActive //7
    { 
        get 
        {
            System.debug('%% CheckRecursive2.CountOppOpenEventIsActive is '+m_CountOppOpenEventIsActive+conterValue());
            return m_CountOppOpenEventIsActive;
        } 
        set 
        {
            m_CountOppOpenEventIsActive = value;
            if(isDisabled()) m_CountOppOpenEventIsActive = False;
            System.debug('%% CheckRecursive2.CountOppOpenEventIsActive was set '+m_CountOppOpenEventIsActive+conterValue());
        } 
    } 
    
	public static Boolean UpdatePricebookEProdutoIsActive //8
    { 
        get 
        {
            System.debug('%% CheckRecursive2.UpdatePricebookEProdutoIsActive is '+m_UpdatePricebookEProdutoIsActive+conterValue());
            return m_UpdatePricebookEProdutoIsActive;
        } 
        set 
        {
            m_UpdatePricebookEProdutoIsActive = value;
            if(isDisabled()) m_UpdatePricebookEProdutoIsActive = False;

            if(m_UpdatePricebookEProdutoIsActive){
            //System.assertEquals(1,0, 'Debug %%** O valor m_DisableCheckRecursive2 é '+m_DisableCheckRecursive2+
            //    ' e o valor m_UpdatePricebookEProdutoIsActive é '+m_UpdatePricebookEProdutoIsActive)
            System.debug('Debug %%** O valor m_DisableCheckRecursive2 é '+m_DisableCheckRecursive2+
                ' e o valor m_UpdatePricebookEProdutoIsActive é '+m_UpdatePricebookEProdutoIsActive);
        }
            System.debug('%% CheckRecursive2.UpdatePricebookEProdutoIsActive was set '+m_UpdatePricebookEProdutoIsActive+conterValue());
        } 
    }  
    
	//public static Boolean CountOppOpenEventIsActive //9
    //{ 
    //    get {return m_CountOppOpenEventIsActive;} 
    //    set {m_CountOppOpenEventIsActive = value; } 
    //}
    
    public static Boolean ApprovalCommitteeManagerIsActive
    {
        get 
        {
            System.debug('%% CheckRecursive2.ApprovalCommitteeManagerIsActive is '+m_ApprovalCommitteeManagerIsActive+conterValue());
            return m_ApprovalCommitteeManagerIsActive;
        }
        set 
        {
            m_ApprovalCommitteeManagerIsActive = value;
            if(isDisabled()) m_ApprovalCommitteeManagerIsActive = False;
            System.debug('%% CheckRecursive2.ApprovalCommitteeManagerIsActive was set '+m_ApprovalCommitteeManagerIsActive+conterValue());
        }
    }
    
    public static Boolean DiamondMemberAutoApprovalIsActive
    {
        get 
        {
            System.debug('%% CheckRecursive2.DiamondMemberAutoApprovalIsActive is '+m_DiamondMemberAutoApprovalIsActive+conterValue());
            return m_DiamondMemberAutoApprovalIsActive;
        }
        set 
        {
            m_DiamondMemberAutoApprovalIsActive = value;
            if(isDisabled()) m_DiamondMemberAutoApprovalIsActive = False;
            System.debug('%% CheckRecursive2.DiamondMemberAutoApprovalIsActive was set '+m_DiamondMemberAutoApprovalIsActive+conterValue());
        }
    }
    
    public static Boolean isDisabled()
    { 
        return Test.isRunningTest()? m_DisableCheckRecursive2OnTest: m_DisableCheckRecursive2;
    }
    
}