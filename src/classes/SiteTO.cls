/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 * Event transfer object class
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 20/01/19  EricB  First version.
 *
 */

public with sharing class SiteTO {

    public class EventTO{

        public String id { get; set; }
        public String name { get; set; }
        public String address { get; set; }
        public String bussinessUnit { get; set; }
        public String clubName { get; set; }
        public String clubId { get; set; }
        public Datetime endDate { get; set; }
        public Datetime startDate { get; set; }
        public String description { get; set; }
        public String descriptionForeignLanguage { get; set; }
        public String descriptionForeignLanguage2 { get; set; }
        public String eventTime { get; set; }
        public String eventYear { get; set; }
        public String financeTeamEmail { get; set; }
        public String informalName { get; set; }
        public String invoiceGRIEmail { get; set; }
        public String ownerId { get; set; }
        public String ownerName { get; set; }
        public String phone { get; set; }
        public Boolean publishOnWebsite { get; set; }
        public String regPhone { get; set; }
        public String registerLink { get; set; }
        public String sector { get; set; }
        public String status { get; set; }
        public String type { get; set; }
        public Boolean vipDinnerIncluded { get; set; }
        public String website { get; set; }
        public List<CampaignTO> campaings { get; set; }
        public List<CompanyTO> companies { get; set; }
        public List<SessionTO> sessions { get; set; }
        public List<EventAttendeTO> eventAttendes { get; set; }
        public List<SponsorTO> sponsors { get; set; }

    }

    public class ClubTO {

        public String id { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String ownerId { get; set; }
        public String ownerName { get; set; }
        public CompanyTO company { get; set; }
        public MembershipTO membership { get; set; }
        public List<SponsorTO> sponsors { get; set; }

    }

    public class CampaignTO {

        public String id { get; set; }
        public String name { get; set; }
        public String typeDigitalCampaign { get; set; }

    }

    public class CompanyTO{
        public String name { get; set; }
        public String nameBadge { get; set; }
        public String accountId { get; set; }
        public String contactId { get; set; }
        public String contactNameBadge { get; set; }
        public String contactJobTitleBadge { get; set; }
        public String contactIdCaseSafe { get; set; }
        public String city { get; set; }
        public String country{ get; set; }
        public String countryCode{ get; set; }
        public String state { get; set; }
        public String stateCode { get; set; }
        public String specialty { get; set; }
        public String foreignSpecialty { get; set; }
        public String foreignSpecialty2 { get; set; }
        public String description { get; set; }
        public String foreignDescription { get; set; }
        public String foreignDescription2 { get; set; }
        public String foreignPersonalSummaryBiography { get; set; }
        public String foreignPersonalSummaryBiography2 { get; set; }
        public String keynoteSummary { get; set; }
        public String foreignKeynoteSummary { get; set; }
        public String foreignKeynoteSummary2 { get; set; }
        public String personalSummaryBiography { get; set; }
        public String profile { get; set; }
        public String phone { get; set; }
        public String summaryProfile { get; set; }
        public String logoName { get; set; }
        public String logoUrl { get; set; }
        //public String photoName { get; set; }
        public String photoUrl { get; set; }
        public String advisorCompanyMemberBlack { get; set; }
        public String foreignCompanySummaryProfile { get; set; }
        public String foreignInformationLanguage2 { get; set; }
        public String foreignInformationLanguage { get; set; }
        public String foreignSponsorProfileBrochure { get; set; }
        public String foreignSponsorProfileBrochure2 { get; set; }
        public String foreignSponsorProfileProgramBook { get; set; }
        public String foreignSponsorProfileProgramBook2 { get; set; }
        public String InformalName { get; set; }
        public String SponsorProfileBrochure { get; set; }
        public String SponsorProfileProgramBook { get; set; }

    }

    public class MembershipTO{

        public String businessUnit { get; set; }
        public String division { get; set; }
        public String specialParticipation { get; set; }
        public String countryClub { get; set; }

    }

    public class EventAttendeTO {

        public String contactId { get; set; }
        public String eventAttendeeId { get; set; }
        public String eventAttendeeName { get; set; }
        public Boolean showAttendeeonWebsite { get; set; }
    }

    public class SessionAttendeTO {

        public String contactId { get; set; }
        public String sessionAttendeeId { get; set; }
        public String sessionAttendeeName { get; set; }
        public Boolean publishOnline { get; set; }
        public Boolean publishSession { get; set; }
        public String sessionPosition { get; set; }
        public String sessionStatus { get; set; }
        public Boolean programFeature { get; set; }
        public Boolean showasAttendingCoChair { get; set; }
    }

    public class SessionTO {

        public String id { get; set; }
        public String name { get; set; }
        public String foreignTitle { get; set; }
        public String foreignTitle2 { get; set; }
        public String description { get; set; }
        public String language { get; set; }
        public String foreignLanguage { get; set; }
        public String foreignLanguage2 { get; set; }
        public String foreignDescription { get; set; }
        public String foreignDescription2 { get; set; }
        public String programIndexing { get; set; }
        public String se { get; set; }
        public String room { get; set; }
        public Boolean publishOnline { get; set; }
        public String title { get; set; }
        public String translation { get; set; }
        public String type { get; set; }
        public String status { get; set; }
        public Decimal sessionIndexing { get; set; }
        public String eventId { get; set; }
        public List<SessionAttendeTO> sessionAttendes { get; set; }

    }

    public class SponsorTO {

        public String account { get; set; }
        public String accountId { get; set; }
        public String a4CompanyProfile { get; set; }
        public String bookLogoPosition { get; set; }
        public String companyProfile { get; set; }
        public String club { get; set; }
        public String clubId { get; set; }
        public String businessUnit { get; set; }
        public String eventId { get; set; }
        public String eventClubId { get; set; }
        public String eventBussinessUnit { get; set; }
        public String eventDivision { get; set; }
        public String fileLogo { get; set; }
        public String fileLogoUrl { get; set; }
        public String logoUrl { get; set; }
        public String orderStartDate { get; set; }
        public String orderEndDate { get; set; }
        public String sizing { get; set; }
        public String sponsorType { get; set; }
        public String status { get; set; }

    }

}