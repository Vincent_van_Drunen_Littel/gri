@isTest
public class WSSerproCalloutTest {
	
	@isTest
	private static void testOpportunityCNPJ() {

		TestDataFactory.init();
        Integer nConPerAcc = 10;
        Integer nAcc = 2;
        List<Account> listAcc = TestDataFactory.createAccounts(nAcc);
        List<Contact> listCon = TestDataFactory.createContacts(listAcc, nConPerAcc);
        List<Club__c> listClubs = TestDataFactory.createClubs(TestDataFactory.listGriClubs());
		Opportunity membOpp = TestDataFactory.createMembershipOpps(listCon[0],  listClubs[0], System.today()+10);

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CalloutMockSerproCNPJ');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		
		membOpp.CNPJ_Number__c = '11965362000133';
		update membOpp;

		Test.stopTest();

		System.assertEquals('11965362000133', [SELECT CNPJ_Number__c FROM Opportunity WHERE Id = :membOpp.Id].CNPJ_Number__c);
	}

	@isTest
	private static void testContactCNPJ() {

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CalloutMockSerproCNPJ');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

		Contact c = TestDataFactory.createContactsWithDefaultAccount();

		Test.startTest();
		c.CNPJ__c = '11965362000133';
		update c;

		Test.stopTest();

		System.assertEquals('11965362000133', [SELECT CNPJ__c FROM Contact WHERE Id = :c.Id].CNPJ__c);
	}

	@isTest
	private static void testCPF() {

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CalloutMockSerproCPF');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

		WSSerpro.WrapperConsulta wConsulta = WSSerpro.consultarCPF('36250216847');
		Test.startTest();
		System.assert(true,'Do nothing');
		Test.stopTest();

		System.assertEquals('success', wConsulta.status);
	}
}