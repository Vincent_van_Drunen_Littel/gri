public virtual class HelperSObject { 

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // SOBJECT
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Create a String Set (no repetitions) of a given field form a SObject List.
	 */    
    public static Set<String> setFromField(List<SObject> listObj, String apiFieldName)
    {
        Set<String> setStr = new Set<String>();
        for(SObject obj:listObj)
        {
            setStr.add(obj.get(apiFieldName).toString());
        }
        return setStr;
    }

    /**
     *
     */
    public static Set<String> setFromFieldSafe(List<SObject> listObj, String apiFieldName)
    {
        Set<String> setStr = new Set<String>();
        String element = '';
        for(SObject obj:listObj)
        {
            try {
                element = obj.get(apiFieldName).toString();
                if(element!=''){
                    //System.debug('element='+element);
                    setStr.add(element);
                }
            }
            catch(System.NullPointerException e)
            {
                //System.debug('Continue the loop');
            }
        }
        return setStr;
    }

    /**
     *
     */
    public static Map<Id, String> mapIdField(List<SObject> listObj, String apiFieldName)
    {
        Map<Id, String> mapStr = new Map<Id, String>();
        for(SObject obj:listObj)
        {
            mapStr.put(obj.Id, obj.get(apiFieldName).toString());
        }
        return mapStr;
    }
    
    /**
     * 
	 */
    public static Integer countFieldOcurrence(String fieldValue, List<SObject> listObj, String apiFieldName)
    {
        Integer i = 0;
        for( SObject obj:listObj)
        {
            if(obj.get(apiFieldName).toString() == fieldValue) i++;
        }
        return i;        
    }

    /**
     *
     */    
    public static List<SObject> filterField(List<SObject> lpObj, String pApiFieldName, List<String> lpFieldValues)
    {
        List<SObject> lOutObj = new List<SObject>();
        for(SObject obj:lpObj)
        {
            for(String fieldValue:lpFieldValues)
            {
                if(obj.get(pApiFieldName).toString() == fieldValue)
                    lOutObj.add(obj);
            }
        }
        return lOutObj;
    }
    
    /**
     *
     */    
    public static List<SObject> filterField(List<SObject> lpObj, String pApiFieldName, String fieldValue)
    {
        List<SObject> lOutObj = new List<SObject>();
        for(SObject obj:lpObj)
        {
        	if(obj.get(pApiFieldName).toString() == fieldValue)
        		lOutObj.add(obj);

        }
        return lOutObj;        
    }

    /**
     * Create a Map from a a SObject list based on a object field.
     * For example, create a map of Opportunities using the contacts Ids 
     * from Contact__c as key:
     * Map<String, Opportunity> mapOpps =  
     *     HelperSObject.mapFromField(listOpportunities, 'Contact__c');
     * It does not use empty strings or NULL values are map key.
     */
    public static Map<String, SObject> mapFromField(List<SObject> lpObj,  String pApiFieldName)
    {
        Map<String, SObject> mapOut = new Map<String, SObject>();
        String strKey = '';
        for(Integer i=0; i<lpObj.size(); i++)
        {
            try 
            {
                strKey = lpObj[i].get(pApiFieldName).toString();
                System.debug('strKey='+strKey); //delete
                if(strKey != '')
                    mapOut.put(strKey, lpObj[i]);
            }
            catch(System.NullPointerException e)
            {
                System.debug('Exception caught! e=`'+e+'`. Do not add this element to the Map, just continue.');
                continue; // continue the loop
            }
            catch(System.SObjectException e)
            {
                throw new HelperSObjectException('Invalid field: '+pApiFieldName+
                    ' used on HelperSObject.mapFromField(). '+e);
            }  
        }
        return mapOut;
    } 

    /**
     * Convert Datatime format (DD:MM:YYYY::HH:MM:SS) to Date (DD:MM:YYYY)
     */
    public static Date date(Datetime dT) 
    {
        return Date.newInstance(dT.year(), dT.month(), dT.day());
    }

    /**
     * Returns TRUE if a certain field from a SObject have *changed* to anoter value.
     * Returns FALSE otherwise.
     */
    public static Boolean wasChangedTo(SObject objNow, SObject objBefore, 
        String pApiFieldName, Object valueExpected)
    {
        Object actualValue = objNow.get(pApiFieldName);
        return (actualValue == valueExpected) && 
            (actualValue != objBefore.get(pApiFieldName)) ;
    }

    /**
     * Asserts if it is in a trigger execution. If it is not, 
     * throw an assertion error
     */
    public static void assertTriggerTest()
    {
        System.assert( Trigger.isExecuting||Test.isRunningTest(), 
            'This method cant be called outside of a Trigger or Test context.' );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // LOG AND TEXT PARSING 
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public static void log(List<SObject> lpSobjects, String pCsvApiFields)
    {
            System.debug(HelperSObject.json(lpSobjects, pCsvApiFields));
    }

    public static void log(String pTag, List<SObject> lpSobjects, String pCsvApiFields)
    {
            System.debug(pTag+HelperSObject.json(lpSobjects, pCsvApiFields));
    }


    /**
     *
     */
    public static String json(List<SObject> lpSobjects, String lpCsvApiFields)
    {
        lpCsvApiFields = lpCsvApiFields.replaceAll('\\s+', '');
        List<String> listcsv = lpCsvApiFields.split(','); 
        return json(lpSobjects, listcsv);
    }

    /**
     *
     */
    public static String json(SObject pSobject, String lpCsvApiFields)
    {
        lpCsvApiFields = lpCsvApiFields.replaceAll('\\s+', '');
        List<String> listcsv = lpCsvApiFields.split(','); 
        return json(pSobject, listcsv);
    }

    //
    private static String json(List<SObject> lpSobjects, List<String> lpApiFields)
    {
        String out = '{';
        for(Integer i=0; i<lpSobjects.size(); i++)
        {
            out+=jsonElement(lpSobjects[i], lpApiFields);
            if(i<lpSobjects.size()-1)
                out+=', ';
            else
                out+='';            
        }
        out += '}';
        return out;
    }

    //
    private static String json(SObject pSobject, List<String> lpApiFields)
    {
        String out = '{"';
        out+=jsonElement(pSobject, lpApiFields);
        out += '}';
        return out;
    }

    //
    private static String jsonElement(SObject pSobject, List<String> lpApiFields)
    {
        String out='"';
        String sobjectName = pSobject.getSObjectType().getDescribe().getName();
        out += sobjectName + '": {';
        for(Integer i=0; i<lpApiFields.size(); i++)
        {
            String value='';
            out+= '"'+lpApiFields[i]+'":"';
            try
            {
                out+=pSobject.get(lpApiFields[i]);
            }
            catch (System.SObjectException e)
            {
                throw new HelperSObjectException('Error on JSON parser: could not retrieve element '+
                    lpApiFields[i]+' from SObject '+sobjectName+'. '+e);
            }
            if(i<lpApiFields.size()-1)
                out+='", ';
            else
                out+='"';
        }
        out += '}'; 
        return out;       
    }



    ///////////////////////////////////////////////////////////////////////////////////////////////
    // OPPORTUNITY
    ///////////////////////////////////////////////////////////////////////////////////////////////

    //  Approval Committee
    public static final String OPPORTUNITY_COMMITTEE_REJECTED = 'Rejected';
    public static final String OPPORTUNITY_COMMITTEE_ACCEPTED = 'Approved';
    public static final String OPPORTUNITY_COMMITTEE_PROCESSING = 'Processing';
    // Opportunity stages
    public static final String OPPORTUNITY_STAGE_PROSPECT = 'Prospect';
    public static final String OPPORTUNITY_STAGE_ON_HOLD = 'On Hold';
    public static final String OPPORTUNITY_STAGE_APPROVED = 'Approved';
    public static final String OPPORTUNITY_STAGE_APPROVED_BY_FINANCE = 'Approved by Finance';
    public static final String OPPORTUNITY_STAGE_WON = 'Won';
    public static final String OPPORTUNITY_STAGE_LOST='Lost';
    public static final String OPPORTUNITY_STAGE_CANCELLED='Cancelled';





    // Record Types
    public static final String OPPORTUNITY_RECORD_TYPE_EVENTS = 'Events';
    public static final String OPPORTUNITY_RECORD_TYPE_MEMBERSHIP = 'Membership';
    public static final String OPPORTUNITY_RECORD_TYPE_MAGAZINE = 'Magazine';
    public static final String OPPORTUNITY_RECORD_TYPE_SPEX = 'SPEX';

    /**
     *
     */
    public static Id opportunity_recordTypeIdEvents()
    {
        return recordType_get('Opportunity', 'Events');
    }

    /**
     *
     */
    public static Id opportunity_recordTypeIdMembership()
    {
        return recordType_get('Opportunity', 'Membership');
    }

    /**
     *
     */    
    public static Id opportunity_recordTypeIdMagazine()
    {
        return recordType_get('Opportunity', 'Magazine Subscription');
    }

    /**
     *
     */    
    public static Id opportunity_recordTypeIdSPEX()
    {
        return recordType_get('Opportunity', 'SPEX');
    }

    /**
     *
     */    
    public static Id opportunity_recordTypeIdSmartusEvent()
    {
        return recordType_get('Opportunity', 'Smartus - Events');
    }

    /** 
     *
     */
    public static Id opportunity_recordTypeIdSmartusSpex()
    {
        return recordType_get('Opportunity', 'Smartus - Spex');
    }
    
    /**
     * It filters the Opportunities with product code equals to the one
     * provided as second parameter.
     * This method to work properly, need a on the Opportunity, a sub-query for
     * OpportunityLineItems requesting ProductCode.
     * For example:
     * List<Opportunity> listOpps = 
     *     [SELECT Id, Name, (SELECT Id, ProductCode FROM OpportunityLineItems) 
     *     FROM Opportunity LIMIT 100];
     * String productCode = "P-000049"
     * List<Opportunity> HelperSObect.opportunity_filterByProductCode(
     *     listOpps, productCode);
     *  
     */
    public static List<Opportunity> opportunity_filterByProductCode(
            List<Opportunity> plOpp, String pProductCode)
    {
        List<Opportunity> listOppByProd = new List<Opportunity>();
        for(Opportunity opp:plOpp)
        {
            List<OpportunityLineItem> listOli = new List<OpportunityLineItem>();
            if(opp.OpportunityLineItems!=NULL && opp.OpportunityLineItems.size()!=0)
            {
                listOli=opp.OpportunityLineItems;
                Boolean rightProductCode = TRUE;
                for(OpportunityLineItem oli:listOli)
                {
                    if(oli.ProductCode != pProductCode)
                    {
                        rightProductCode=FALSE;
                        break;
                    }
                }
                if(rightProductCode==TRUE)
                    listOppByProd.add(opp);
            }
        }
        return listOppByProd;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // MEMBERSHIP
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // membership status
    public static final String MEMBERSHIP_STATUS_ACTIVATED = 'Activated';
    public static final String MEMBERSHIP_STATUS_EXPIRED = 'Expired'; // grace period
    public static final String MEMBERSHIP_STATUS_DRAFT= 'Draft';
    public static final String MEMBERSHIP_STATUS_IN_APPROVAL_PROCESS  = 'In Approval Process';   
    public static final String MEMBERSHIP_STATUS_EXPIRED_RENEWED = 'Expired - Renewed';  
    public static final String MEMBERSHIP_STATUS_PENDING = 'Pending';
    public static final String MEMBERSHIP_STATUS_SUSPENDED = 'Suspended';
    public static final String MEMBERSHIP_STATUS_CANCELLED = 'Cancelled';
    public static final String MEMBERSHIP_STATUS_TEMP_STATUS = 'Temp Status';
    public static final String MEMBERSHIP_STATUS_TRANSFERRED = 'Transferred';    
    public static final String MEMBERSHIP_STATUS_UPGRADED = 'Upgraded';
    //Membership level
    public static final String MEMBERSHIP_LV_PLATINUM_MEMBER='Platinum Member';
    public static final String MEMBERSHIP_LV_DIAMOND_MEMBER='Diamond Member';
    public static final String MEMBERSHIP_LV_ADVISOR_MEMBER_DIAMOND='Advisor Member Diamond';
    public static final String MEMBERSHIP_LV_STATE_HIGH_PLATINUM_MEMBERSHIP='State High Platinum Membership';
    public static final String MEMBERSHIP_LV_DIAMOND_LATAM_MEMBER='Diamond Latam Member';
    public static final String MEMBERSHIP_LV_DIAMOND_STATE_MEMBER='Diamond State Member';
    public static final String MEMBERSHIP_LV_DIAMOND_STATE_RETREAT_MEMBER='Diamond State Retreat Member';
    public static final String MEMBERSHIP_LV_PLATINUM_LATAM_MEMBER='Platinum Latam Member';
    public static final String MEMBERSHIP_LV_PLATINUM_MEMBER_INFRA_AND_RE='Platinum Member INFRA and RE';
    public static final String MEMBERSHIP_LV_PLATINUM_STATE_MEMBER='Platinum State Member';
    public static final String MEMBERSHIP_LV_PLATINUM_STATE_RETREAT_MEMBER='Platinum State Retreat Member';
    public static final String MEMBERSHIP_LV_STATE_DIAMOND_LATAM_MEMBER='State Diamond Latam Member';
    public static final String MEMBERSHIP_LV_STATE_PLATINUM_COMPANY_MEMBER='State Platinum Company member';
    public static final String MEMBERSHIP_LV_STATE_LATAM_MEMBER='State Latam Member';
    public static final String MEMBERSHIP_LV_STATE_DIAMOND_COMPANY_MEMBER='State Diamond Company member';
    public static final String MEMBERSHIP_LV_ADVISOR_DIAMOND_LATAM_MEMBER='Advisor Diamond Latam Member';
    public static final String MEMBERSHIP_LV_DIAMOND_LATAM_COMPANY_MEMBER='Diamond Latam Company Member';
    public static final String MEMBERSHIP_LV_PLATINUM_LATAM_COMPANY_MEMBER='Platinum Latam Company Member';
    public static final String MEMBERSHIP_LV_PLATINUM_COMPANY_MEMBER='Platinum Company Member';
    public static final String MEMBERSHIP_LV_STATE_MEMBER_ADVISORY_BOARD_INFRA_AND_RE='State Member Advisory Board INFRA and RE';
    public static final String MEMBERSHIP_LV_DIAMOND_MEMBER_INFRA_AND_RE='Diamond Member Infra and RE';
    public static final String MEMBERSHIP_LV_STATE_LATAM_COMPANY_MEMBER='State Latam Company member';

    /**
     *
     */
    public static Integer membership_AllowedOpenEventsWorldwide(Contract membership)
    {
        try
        {
            return Integer.valueOf(membership.Allowed_Open_Events_Worldwide__c);
        }
        catch (System.TypeException e)
        {
            if(membership.Allowed_Open_Events_Worldwide__c == 'All')
                return 365;
            else
                return 0;
        }
    }
    
    /**
     *
     */
    public static Integer membership_AllowedExclusiveClubMeetings(Contract membership)
    {
        try
        {
            return Integer.valueOf(membership.Allowed_Exclusive_Events__c);
        }
        catch (System.TypeException e)
        {
            if(membership.Allowed_Exclusive_Events__c == 'All')
                return 365;
            else
                return 0;
        }
    }

    /**
     *
     */
    public static Boolean membership_isActive(Contract membership)
    {
        if( (membership.Status==MEMBERSHIP_STATUS_EXPIRED) || 
            (membership.Status==MEMBERSHIP_STATUS_ACTIVATED))
            return True;
        return False;
    }

    /**
     * This method returns a set of all diamond and platinum memberships
     * available
     */
    public static Set<String> setDiamondPlatinumMemberships()
    {
        return new Set<String>{
            MEMBERSHIP_LV_PLATINUM_MEMBER,
            MEMBERSHIP_LV_DIAMOND_MEMBER,
            MEMBERSHIP_LV_ADVISOR_MEMBER_DIAMOND,
            MEMBERSHIP_LV_STATE_HIGH_PLATINUM_MEMBERSHIP,
            MEMBERSHIP_LV_DIAMOND_LATAM_MEMBER,
            MEMBERSHIP_LV_DIAMOND_STATE_MEMBER,
            MEMBERSHIP_LV_DIAMOND_STATE_RETREAT_MEMBER,
            MEMBERSHIP_LV_PLATINUM_LATAM_MEMBER,
            MEMBERSHIP_LV_PLATINUM_MEMBER_INFRA_AND_RE,
            MEMBERSHIP_LV_PLATINUM_STATE_MEMBER,
            MEMBERSHIP_LV_PLATINUM_STATE_RETREAT_MEMBER,
            MEMBERSHIP_LV_STATE_DIAMOND_LATAM_MEMBER,
            MEMBERSHIP_LV_STATE_PLATINUM_COMPANY_MEMBER,
            MEMBERSHIP_LV_STATE_LATAM_MEMBER,
            MEMBERSHIP_LV_STATE_DIAMOND_COMPANY_MEMBER,
            MEMBERSHIP_LV_ADVISOR_DIAMOND_LATAM_MEMBER,
            MEMBERSHIP_LV_DIAMOND_LATAM_COMPANY_MEMBER,
            MEMBERSHIP_LV_PLATINUM_LATAM_COMPANY_MEMBER,
            MEMBERSHIP_LV_PLATINUM_COMPANY_MEMBER,
            MEMBERSHIP_LV_STATE_MEMBER_ADVISORY_BOARD_INFRA_AND_RE,
            MEMBERSHIP_LV_DIAMOND_MEMBER_INFRA_AND_RE,
            MEMBERSHIP_LV_STATE_LATAM_COMPANY_MEMBER
        };  
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // CLUB
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // GRI Infra
    public static final String CLUB_INFRA_LATAM = 'Infra Club LatAm'; //'Infra Club LatAm' => RT_CTC_INFRACLUB,
    public static final String CLUB_INFRA_INDIA = 'Infra Club India'; // 'Infra Club India' => RT_CTC_INFRACLUBINDIA,
    // GRI Real Estate
    public static final String CLUB_RE_BRAZIL = 'RE Club Brazil'; // 'Infra Club LatAm' => RT_CTC_INFRACLUB,
    public static final String CLUB_RE_EUROPE = 'RE Club Europe'; // 'RE Club Europe' => RT_CTC_EUROPEANCLUB,
    public static final String CLUB_RE_INDIA = 'RE Club India'; // 'RE Club India' => RT_CTC_INDIACLUB,
    public static final String CLUB_RE_LATAM = 'RE Club LatAm';
    public static final String CLUB_RE_CLUB_AFRICA = 'RE Club Africa';
    public static final String CLUB_RE_AFRICA = 'RE Club Africa';
    public static final String CLUB_GRI_GLOBAL_CLUB = 'GRI Global Club'; 
    public static final String CLUB_RETAIL_BRAZIL = 'Retail Club Brazil'; // deprecated
    // Smartus
    public static final String SMARTUS_REAL_ESTATE = 'Smartus - Real Estate';

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // PRODUCT
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public static final String PRODUCT_MEMBER_FREE = 'P-000049';


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // EVENT ATTENDEE
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public static final String EVENT_ATT_STATUS_PROCESSING='Processing';
    public static final String EVENT_ATT_STATUS_CONFIRMED='Confirmed';
    public static final String EVENT_ATT_STATUS_REGISTERED='Registered';
    public static final String EVENT_ATT_STATUS_REPLACED='Replaced';
    public static final String EVENT_ATT_STATUS_CANCELLED='Cancelled';
    public static final String EVENT_ATT_STATUS_NO_SHOW='No Show';
    public static final String EVENT_ATT_STATUS_NOT_ATTENDING='Not Attending';    

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // RECORD TYPE
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public static Id recordType_get(String pSObject, String pRecordTypeName)
    {
        try
        {
            Id recTypeId = Schema.getGlobalDescribe().get(pSObject).newSObject().getSObjectType().getDescribe().getRecordTypeInfosByName().get(pRecordTypeName).getRecordTypeId();
            return recTypeId;
        }
        catch(NullPointerException e)
        {
            throw new HelperSObjectException(
                'Record type Id retuned a NULL value. Please review the input parameters: '+
                'pSObject='+pSObject+', pRecordTypeName='+pRecordTypeName);
        }       
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // EXCEPTIONS
    ///////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // CAMPAIGN 
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public static Boolean campaign_addLeadOrContact(Id pCampaignId, SObject pLeadOrContact, 
        String pNote)
    {
        CampaignMember member = new CampaignMember(CampaignId=pCampaignId,
            Notes__c=pNote);
        String sObjectName = pLeadOrContact.getSObjectType().getDescribe().getName();
        if(sObjectName=='Contact')
            member.ContactId = pLeadOrContact.Id;
        else // sObjectName == Lead 
            member.LeadId = pLeadOrContact.Id;
        try 
        {
            insert member;
        }
        catch (System.DmlException e){
            System.debug('Contact or Lead is already on Campaign CampaignMember:'+
                HelperSObject.json(member, 'Id, CampaignId, LeadId, ContactId')+'. e:'+e);
            return FALSE;
        }
        return TRUE;
    }

    /**
     *
     */
    public class HelperSObjectException extends Exception {} 

    /**
     *
     */
    public class HelperOpportunityException extends Exception {}

    /**
     *
     */    
    public class HelperMembershipException extends Exception {}  

}