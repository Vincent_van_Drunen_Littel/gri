public class OpportunityApproovalCommitteeManager {
    
    private static final date LIMIT_DATE = date.newInstance(2018, 5, 1);
    
    private static final String RECORD_TYPE_MEMBERSHIP = 'Membership';
    
    private static final String RE_CLUB_BRAZIL = 'RE Club Brazil';
    private static final String RE_CLUB_EUROPE = 'RE Club Europe';
    private static final String RE_CLUB_LATAM = 'RE Club LatAm';
    private static final String INFRA_CLUB_LATAM = 'Infra Club LatAm';
    private static final String RE_CLUB_INDIA = 'RE Club India';
    
    private static final String STAGE_APPROVED = 'Approved';
    private static final String STAGE_APPROVED_BY_FINANCE = 'Approved by Finance';
    private static final String STAGE_WON = 'Won';
     
    private static final String STATUS_ACTIVATED = 'Activated';
    private static final String STATUS_EXPIRED = 'Expired'; // grace period
    
    private static final String APPROVAL_COMMITTE_APPROVED = 'Approved';
    private static final String APPROVAL_COMMITTE_REJECTED = 'Rejected';
    private static final List<String> STAGES_OPP_FOR_MEMBERSHIP = 
        new List<String>{STAGE_APPROVED , STAGE_APPROVED_BY_FINANCE ,  STAGE_WON };


    /**
    * This method test the conditions for Approoval committee. If a 'not satisfied'
    * condittion occours, it returns false, and a error messege is passed to the
    * List<String> argument. Otherwise returns True
    * The conditions are:
    *   -   Conditions are tested if the opportunity belongs to the 
    *       list Clubs that have approval committee (RE Club Brazil, RE Club 
    *       Europe), the opportunity as created after 01/05/2018 and is a
    *       Opportunity of Membership. Otherwise returns True
    *   -   If the Contact from the Opportunity already has one or more
    *       membership (on the same Club?) it returns True
    *   -   If the Approoval committe Result was 'Approoved', it returns True.
    *       Otherwise, it returns False
    **/
    public static Boolean execute(List<Opportunity> listopps, List<String> errorMessege)
    {
        System.debug('%% In listopps='+SObjectHelper.Opportunity_returnIdsFromOpps(listopps));
        
        List<Opportunity> matchOpps = oppsMatchConditionsApprovalCommittee(listopps);
        
        System.debug('%% Ids='+SObjectHelper.Opportunity_returnIdsFromOpps(matchOpps));
        
        if(matchOpps.size() != 0 )
        {
            // Stages that need to be checked
            List<Opportunity> matchOpps_AproAprofinWon = 
                SObjectHelper.Opportunity_retStage(matchOpps, STAGES_OPP_FOR_MEMBERSHIP);
            System.debug('%% [info] matchOpps_AproAprofinWon='+matchOpps_AproAprofinWon);
            
            if(matchOpps_AproAprofinWon.size() != 0)
            {
                // contact id list from opps
                List<Id> contactIdList = SObjectHelper.Opportunity_returnContactIdListNoRepeat(matchOpps_AproAprofinWon);
                
                List<String> validMembershipStatus = new List<String>{STATUS_ACTIVATED, STATUS_EXPIRED};
                    Map<Id, Integer> nMemberContact = nMembershipsPerContactId(contactIdList, validMembershipStatus);    
                System.debug('%% [info] nMemberContact='+nMemberContact);
                
                List<Opportunity> listRejectedOpps = new List<Opportunity>();
                List<Opportunity> listBlankOpps = new List<Opportunity>();
                
                for(Opportunity opp:matchOpps_AproAprofinWon)
                {
                    /// check if is null
                    Integer numberOfMemberhipsActive = nMemberContact.get(opp.Contact__c);
                    if(numberOfMemberhipsActive == NULL )
                        numberOfMemberhipsActive = 0;
                    
                    if(numberOfMemberhipsActive == 0)
                    {
                        if(opp.ApprovalCommitteeResult__c == APPROVAL_COMMITTE_REJECTED)
                        {
                            listRejectedOpps.add(opp);
                        }
                        else if(opp.ApprovalCommitteeResult__c != STAGE_APPROVED)
                        {
                            listBlankOpps.add(opp);
                        }   
                    }
                    else{
                        if(opp.ApprovalCommitteeResult__c == APPROVAL_COMMITTE_REJECTED)
                        {
                            System.debug('%% Contact ' + opp.Contact__c + 'has approval committe resulult '+ 
                                         APPROVAL_COMMITTE_REJECTED + 
                                         ', but already has memberhip, so he was Approoved');
                        }
                    }  
                }
                if( (listRejectedOpps.size() > 0) || (listBlankOpps.size() > 0) )
                {
                    errorMessege.add(errMsgStage());
                    //errorMessege.add('Rejected Approval Committee: ' + errMsgOpps(listRejectedOpps) 
                    //                 + ' Blank Approval Committee: ' + errMsgOpps(listBlankOpps) );
                    return False;
                }
                System.debug('%% [info] Approval committee tests success');
            }
            else
            {
                System.debug('%% [info] The conditions were filled (matchOpps.size()=' + 
                             matchOpps.size() + '), but no opp were on stages STAGES_OPP_FOR_MEMBERSHIP=' + 
                             STAGES_OPP_FOR_MEMBERSHIP + '.');
            }
            
        }
        else
        {
            System.debug('%% [Info] No Opp Opportunity the conditions for Approval committee test. Just update.');
        }
        
        return True;
    }
    
    // test the match conditions for apporval committee Club, limit date and membership
    private static List<Opportunity> oppsMatchConditionsApprovalCommittee(List<Opportunity> listopps)
    {
        
        List<Opportunity> listOppMatchConditions = new List<Opportunity>();
        for(Opportunity opp:listopps)
        {
            if(
                ( (opp.Club_Name__c==RE_CLUB_BRAZIL) || (opp.Club_Name__c==RE_CLUB_EUROPE) ||  
                  (opp.Club_Name__c==RE_CLUB_LATAM) || (opp.Club_Name__c==INFRA_CLUB_LATAM) ||
                  (opp.Club_Name__c==RE_CLUB_INDIA) ) && 
                //(opp.CreatedDate > LIMIT_DATE ) &&
                (opp.OppRecordTypeName__c== RECORD_TYPE_MEMBERSHIP))
            {
                System.debug('%% ' + opp.Club_Name__c + ', '+ opp.CreatedDate + ', '
                             + opp.OppRecordTypeName__c);
                
                listOppMatchConditions.add(opp);
            }
        }
        System.debug('listOppMatchConditions='+listOppMatchConditions);
        return(listOppMatchConditions);
    }
    
    // return a list the number of memberships od a given contact Id
    private static Integer countNumberOfMemberships(Id contactId, List<Contract> memberships)
    {
        Integer i = 0;
        for( Contract memb:memberships)
        {
            if(memb.Contact__c == contactId) i++;
        }
        return i;
    }
    
    // Create a Map of Contacts Ids and the number of active memberships for each
    public static Map<Id, Integer> nMembershipsPerContactId(List<Id> listContactIds, 
                                                            List<String> validStatus)
    {
        Map<Id, Integer> validMembershipsPerCcId2 = new Map<Id, Integer>();

        System.debug('%% [Debug] listContactIds='+listContactIds);
        System.debug('%% [Debug] validStatus='+validStatus);
        
        if(listContactIds.size() == 0 )
        {
            System.debug('%% [info] number of active memberships is zero. No entry is returned');
        }
        else if(validStatus.size() == 0)
        {
            System.debug('%% [info] no valid status to be for opp provided');
        }
        else{
            
            List<Contract> listMemberships = [SELECT Id,Name,Status,Club_Name__c,Contact__c
                                              FROM Contract 
                                              WHERE( 
                                                  (Contact__c IN  :listContactIds) AND
                                                  (Status IN :validStatus) 
                                              )];

            for(Id cccId:listContactIds)
            {
                validMembershipsPerCcId2.put(cccId, countNumberOfMemberships(cccId, listMemberships));
            }

        }
        return validMembershipsPerCcId2 ;
    }
    

    // Returns an string with an erroer message to inform an invalid Opp stage
    private static String errMsgStage()
    {
   		return('You cannot put the Opportunity as Approved, Approved by Finance or Won because the entry of this Contact to the Club hasn\'t been Approved by the Approval Committee');
    }
    
 
}