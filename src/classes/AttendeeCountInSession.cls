/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe responsável por contabilizar Session Ateendee no objeto Session.
*
* NAME: AttendeeCountInSession.cls
* AUTHOR: VMDL                                                 DATE: 15/08/2016
*******************************************************************************/
public with sharing class AttendeeCountInSession {

//public static Map<Id, Session__c> countAttendeeSession(Set<Id> aSession)
 {
//   map<Id, Session__c> mapSessionToUpdate = new map<Id, Session__c>();
//   for(AggregateResult iAtt : [Select count(id) quant, Session__c idSession
//                     FROM Attendee__c
//                    Where Session__c =: aSession
//                    Group By Session__c ])
//   {
//     Session__c lSession = new Session__c(Id = (Id)iAtt.get('idSession'));
//     lSession.Final_participants__c = (Integer) iAtt.get('quant');
//     mapSessionToUpdate.put(lSession.Id, lSession);
//   }
//   return mapSessionToUpdate;
 }

public static void executeCountInsert()
{
//   TriggerUtils.assertTrigger();

//   set<Id> setIdSession = new set<Id>();

//   for(Attendee__c iAtt : (list<Attendee__c>) trigger.new)
//   {
//     if(iAtt.Session__c == null) continue;
//     setIdSession.add(iAtt.Session__c);
//   }
//   if(setIdSession.isEmpty()) return;

//   Map<Id, Session__c> mapSessionToUpdate = countAttendeeSession(setIdSession);

//   if(!mapSessionToUpdate.isEmpty()) Database.update(mapSessionToUpdate.values());
 }

 public static void executeCountDelete()
{
//   TriggerUtils.assertTrigger();

//   set<Id> setIdSession = new set<Id>();
//
//   for(Attendee__c iAtt : (list<Attendee__c>) trigger.old)
//   {
//     if(iAtt.Session__c == null) continue;
//     setIdSession.add(iAtt.Session__c);
//   }
//   if(setIdSession.isEmpty()) return;

//   Map<Id, Session__c> mapSessionToUpdate = countAttendeeSession(setIdSession);

//   if(!mapSessionToUpdate.isEmpty()) Database.update(mapSessionToUpdate.values());
}
}