@RestResource(urlMapping='/AttendeeSyncService')
global class AttendeeSyncService {
    public String status;   //confirmed
    public cls_attendees[] attendees; 
   public static AttendeeSyncService parse(String json){
        return (AttendeeSyncService) System.JSON.deserialize(json, AttendeeSyncService.class);
    }
     public class cls_attendees {
        public String email;    //griclub@griclub.org
        public boolean organizer;
        public boolean self;
        public String responseStatus;   //needsAction
    }
    
    @HttpPOST
    global static void dopost() {
        String name = RestContext.request.params.get('name');
        System.debug(JSON.serialize(RestContext.request));
        System.debug(RestContext.request.headers.get('X-Goog-Channel-ID'));
        if(RestContext.request.headers.get('X-Goog-Channel-ID') != null){
            Map<String,Calendar_Invitee__c> mapOfAttendeesToUpdate = new Map<String,Calendar_Invitee__c>();
            for(Calendar_Invitee__c invitee : [Select Id,Calendar_Invite__r.GoogleCalendarEventId__c, InviteeEmail__c,Invite_Status__c from Calendar_Invitee__c where Calendar_Invite__r.GoogleCalendarEventId__c = :RestContext.request.headers.get('X-Goog-Channel-ID') Order By CreatedDate ASC]){
                mapOfAttendeesToUpdate.put(invitee.InviteeEmail__c,invitee);
            }
            system.debug('@@mapOfAttendeesToUpdate'+mapOfAttendeesToUpdate);
            
            if(mapOfAttendeesToUpdate != null){
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                //RestContext.request.headers.get('X-Goog-Resource-URI').subString(0)
                String XGoogResourceURI = RestContext.request.headers.get('X-Goog-Resource-URI');
                XGoogResourceURI = XGoogResourceURI.substringBefore('?');
                String XGoogChannelID = RestContext.request.headers.get('X-Goog-Channel-ID');
                string endPoint =  XGoogResourceURI +'/' +XGoogChannelID;
                //string endPoint =  'https://www.googleapis.com/calendar/v3/calendars/griclub@griclub.org/events/'+RestContext.request.headers.get('X-Goog-Channel-ID');   
                req.setEndpoint(endPoint);
                string accesstoken='';
                if(test.isRunningtest()){
                    accesstoken = 'ya29.Gl1fBohCioa6NaqgeK-hKec6gYZGOil2xomyLB9vnaKG-XV32i18lzgB08rGkf5-jWDE-d1RfHnzWaWoY3NlWXnOVEYOlE-wFnwf-xlEzBSP1viBHzx32hFJXOsYIRM';
                }else{
                    accesstoken = CalendarInviteController.getGoogleAccessToken();
                }
                req.setHeader('Authorization', 'Bearer ' +accesstoken);
                req.setHeader('Content-Type', 'application/json; charset=UTF-8');
                req.setMethod('GET'); // Put for update but it is overriding
                req.setTimeout(10000);
                HttpResponse res = h.send(req); 
                System.debug(res.getBody());
                AttendeeSyncService esr = (AttendeeSyncService)JSON.deserialize(res.getBody(), AttendeeSyncService.class);
                system.debug('@@esr.attendees'+esr.attendees);
                system.debug('@@esr.status'+esr.status);
                List<Calendar_Invitee__c> delInvitees = new List<Calendar_Invitee__c>();
                
                if(esr.status != 'Cancelled'){
                    for(AttendeeSyncService.cls_attendees attendee : esr.attendees){
                        system.debug('@@attendee.email'+attendee.email);
                        system.debug('@@attendee.responseStatus'+attendee.responseStatus);
                        
                        String status = attendee.responseStatus.equalsIgnoreCase('declined') ? 'No' : attendee.responseStatus.equalsIgnoreCase('accepted') ? 'Yes' : attendee.responseStatus.equalsIgnoreCase('tentative') ? 'Maybe': 'Pending' ;
                        system.debug('@@attendee.responseStatus'+attendee.responseStatus);
                        
                        if(mapOfAttendeesToUpdate.containsKey(attendee.email)){
                            Calendar_Invitee__c invitee = mapOfAttendeesToUpdate.get(attendee.email);
                            System.debug('@@attendee.responseStatus'+attendee.responseStatus);
                            
                            system.debug('@@status'+status+'@@status'+status);
                            
                            if(invitee.Invite_Status__c != status){
                                invitee.Invite_Status__c = status;
                            }
                            mapOfAttendeesToUpdate.put(attendee.email,invitee);
                           
                        }
                        
                    }
                }
                
                if(esr.status == 'Cancelled'){
                    List<Calendar_Invite__c> calObjLst = [SELECT Id FROM Calendar_Invite__c WHERE GoogleCalendarEventId__c = :RestContext.request.headers.get('X-Goog-Channel-ID')];
                    system.debug('@@calObjLst'+calObjLst);
                    if(calObjLst != null & calObjLst.size()>0){
                        delete calObjLst;
                    }
                }
               
                system.debug('@@mapOfAttendeesToUpdate'+mapOfAttendeesToUpdate);
                if(mapOfAttendeesToUpdate != null && mapOfAttendeesToUpdate.size()>0 ){
                    update mapOfAttendeesToUpdate.values();
                }
                 
            }
        }
    }
    
}