/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe de teste da classe PricebookUpdatesOportunidades.
*
* NAME: PricebookUpdatesOportunidadesTest.cls
* AUTHOR: EAD                                                  DATE: 07/02/2018
*******************************************************************************/
@isTest
public class PricebookUpdatesOportunidadesTest {
  public static String RT_ACC = RecordTypeMemory.getRecType('Account', 'Customer');
  public static String RT_OPP = OrgDefs.RT_OPP_OPENEVENTS;  
  public static final integer LOTE = 200;
  private static Application_Config__c appCon;
  private static Account acc;
  private static Contact ctt;
  private static Pricebook2 pb;
    
  static{
    //===Custom Setting===
    appCon = new Application_Config__c(Name = 'BucketUserId', Value__c = RT_ACC);
    insert appCon;   
    //===Account===  
    acc = SObjectInstance.createAccount(RT_ACC); 
    insert acc;  
    //===Contact===
    ctt = SObjectInstance.createContato(acc.id);
    insert ctt;  
    //===Pricebook=== 
    pb = SObjectInstance.catalogoDePreco();
    pb.IsActive = true;  
    insert pb;   
  }  
    
  public static testMethod void testSingle(){
    //=== Opportunity ===
    Opportunity opp = SObjectInstance.createOpportunidade(acc.id, RT_OPP);
    opp.Pricebook2Id = pb.id; 
    opp.Contact__c = ctt.id;  
    opp.Approval_Process__c = '1 - Approved';
    insert opp;    
      
    set<String> lIdPb = new set<String>();
	lIdPb.add(pb.id);
      
    //============ Test Session ===========
    Test.startTest();
      list<Opportunity> lOpp = [SELECT Approval_Process__c FROM Opportunity LIMIT 1];
      System.assertEquals('1 - Approved', lOpp.get(0).Approval_Process__c);
      pb.End_date__c = system.today() + 5;
      update pb;
    Test.stopTest();
    lOpp.clear();
      
    lOpp = [SELECT Approval_Process__c FROM Opportunity];      
    System.assertEquals(null, lOpp.get(0).Approval_Process__c);     
  }
  
  //=====================================================================  
  public static testMethod void testLote(){
    list<Account> lstAccLote = new list<Account>();
    list<Contact> lstCttLote = new list<Contact>();  
    list<Opportunity> lstOppLote = new list<Opportunity>();  
    
    //===Accounts===  
    for(integer i = 0; i < LOTE; i++){
      Account acc = SObjectInstance.createAccount(RT_ACC); 
      lstAccLote.add(acc);     
    }
    insert lstAccLote;
    //===Contacts===  
    for(integer i = 0; i < LOTE; i++){
      Contact ctt = SObjectInstance.createContato(lstAccLote.get(i).id);
      ctt.LastName += i;   
      lstCttLote.add(ctt);    
    }  
    insert lstCttLote;

    //===Opportunities===   
    for(integer i = 0; i < LOTE-1; i++){
      Opportunity opp = SObjectInstance.createOpportunidade(lstAccLote.get(i).id, RT_OPP);
      opp.Pricebook2Id = pb.id; 
      opp.Contact__c = lstCttLote.get(i).id;  
      opp.Approval_Process__c = '1 - Approved';
      lstOppLote.add(opp);  
    }
    insert lstOppLote; 
      
    //===Pricebook=== 
    Pricebook2 pb2 = SObjectInstance.catalogoDePreco();
    pb2.IsActive = true;  
    insert pb2; 
      
    //=== Opportunity ===
    Opportunity opp2 = SObjectInstance.createOpportunidade(acc.id, RT_OPP);
    opp2.Pricebook2Id = pb2.id; 
    opp2.Contact__c = ctt.id;  
    opp2.Approval_Process__c = '1 - Approved';
    insert opp2; 
      
    Test.startTest();
      list<Opportunity> lOpp = [SELECT Approval_Process__c FROM Opportunity LIMIT 200];
      for(integer i = 0; i < LOTE; i++){
        System.assertEquals('1 - Approved', lOpp.get(i).Approval_Process__c);  
      }
      
      pb.End_date__c = system.today() + 5;
      update pb;
    Test.stopTest();
      
    lOpp.clear();  
    lOpp = [SELECT Approval_Process__c FROM Opportunity LIMIT 200];
    for(integer i = 0; i < LOTE-1; i++){
      System.assertEquals(null, lOpp.get(i).Approval_Process__c);  
    } 
    System.assertEquals('1 - Approved', lOpp.get(LOTE-1).Approval_Process__c);  
  }
}