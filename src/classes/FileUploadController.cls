/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe resposanvel pela inserção de imagens no registro de contato
*
*
*
* NAME: FileUploadController
* AUTHOR: JFS                                                 DATE: 27/09/2016
*******************************************************************************/
public with sharing class FileUploadController {
    
    public Document document 
    {
    get {
      if (document == null)
        document = new Document();
      return document;
    }
    set;
  }

    public List<SelectOption> lstOptionsPhoto {get; set;}
    public String previsao {get; set;}
    private Id lContactId {get; set;}
    private Contact ctt = new Contact();
    private Photo_Name__c photoSeparator = Photo_Name__c.getValues('PhotoName');
 
    public FileUploadController(ApexPages.StandardController controller) 
    {
        this.lstOptionsPhoto = new List<SelectOption>();
        this.lContactId = controller.getId();
        
        this.ctt = [SELECT Name, Account.Name FROM Contact WHERE Id =: lContactId];
        
        lstOptionsPhoto.add(New SelectOption('','--None--'));

        Schema.DescribeFieldResult options = Contact.VF_Photo_Logo__c.getDescribe();
        List<Schema.PicklistEntry> lsOpt = options.getPicklistValues();

        for(Schema.PicklistEntry value : lsOpt)
        { 
            lstOptionsPhoto.add(New SelectOption(value.getValue(), value.getValue()));
        }

    }

    public PageReference upload() {
 
    document.AuthorId = UserInfo.getUserId();

    if( previsao == 'Original Photo'/* || previsao == 'Original Logo' */)
    {
        String folderName = 'Original Files';
        document.FolderId = [SELECT Id FROM Folder 
                            WHERE Name =: folderName].Id;
    }
    else if( previsao == 'Marketing Edited Photo'/* || previsao == 'Marketing Edited Logo' */)
    {   
        String folderName = 'Marketing Edited Files';
        document.FolderId = [SELECT Id FROM Folder 
                            WHERE Name =: folderName].Id;
    } 
    try {
        if(String.isBlank(previsao))
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Select File Type'));
            return null;    
        }
        else if(document.body == null)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Select a File'));
            return null;
        }
        
        document.type = 'jpg'; 
        document.Name = lContactId + photoSeparator.separator__c + previsao.replace(' ','_') + photoSeparator.Extension__c;
        //lContactId + photoSeparator.separator__c + this.ctt.Name + photoSeparator.separator__c + this.ctt.Account.Name 
         // + photoSeparator.separator__c + previsao + photoSeparator.Extension__c;
        Database.insert( document );
        
        String urlDoc = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=' + document.Id;
        Contact lCon = new Contact(Id = lContactId);
        
        if(previsao == 'Original Photo')
        {
            lCon.Original_Photo__c = urlDoc;
            lCon.File_Name_Original_photo__c = document.Name;
        }
 /*       else if(previsao == 'Original Logo')
        {
            lCon.Original_Logo__c = urlDoc;
            lCon.File_Name_Original_logo__c = document.Name;
        }*/
        else if(previsao == 'Marketing Edited Photo')
        {
            lCon.Marketing_Edited_Photo__c = urlDoc;
            lCon.File_Name_Marketing_Edited_photo__c = document.Name;
        }
/*        else if(previsao == 'Marketing Edited Logo')
        {
            lCon.Marketing_Edited_Logo__c = urlDoc;
            lCon.File_Name_Marketing_Edited_logo__c = document.Name;    
        }*/
        Database.update( lCon );
        //Clear
        document = new Document();
        previsao = null;
        document.body = null; 
        //Clear
        return new PageReference( URL.getSalesforceBaseUrl().toExternalForm() + '/' + lContactId );
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
      return null;
    }
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully'));
    return null;
  }

}