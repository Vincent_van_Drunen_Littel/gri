/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 * Class responsible for accessing org data for Club object
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 02/02/19  EricB  First version.
 *
 */

public with sharing class ClubDAO {

    public static final ClubDAO instance = new ClubDAO();
    private ClubDAO (){}
    public static ClubDAO getInstance(){ return instance; }

    public List<Contract> findContractByClubId(Set<Id> setId){

        return [
                SELECT
                        Id,
                        AccountId,
                        Account.Id,
                        Account.Name,
                        Account.Phone,
                        Account.Company_Profile__c,
                        Account.Company_Summary_Profile__c,
                        Account.File_Name_Marketing_Edited_logo__c,
                        Account.Marketing_Edited_Logo__c,
                        Account.Advisor_Company_Member_Black__c,
                        Account.Foreign_Company_Summary_Profile__c,
                        Account.Foreign_Information_2_Language__c,
                        Account.Foreign_Information_Language__c,
                        Account.Foreign_Sponsor_Profile_for_Brochure__c,
                        Account.Foreign_Sponsor_Profile_for_Brochure_2__c,
                        Account.Foreign_Sponsor_Profile_for_Program_Book__c,
                        Account.Foreign_Sponsor_Profile_for_ProgramBook2__c,
                        Account.Informal_Name__c,
                        Account.Sponsor_Profile_for_Brochure__c,
                        Account.Sponsor_Profile_for_Program_Book__c,
                        Club__c,
                        Club__r.Id,
                        Club__r.Name,
                        Club__r.Club_Description__c,
                        Club__r.OwnerId,
                        Club__r.Owner.Name,
                        Contact__c,
                        Contact__r.Id,
                        Contact__r.Badge_Name__c,
                        Contact__r.Job_title_badge__c,
                        Contact__r.Company_Name_Badge__c,
                        Country_Club__c,
                        Membership_Business_Unit__c,
                        Membership_Division__c,
                        Special_Participation__c
                FROM
                        Contract
                WHERE
                        Club__c IN : setId
                AND
                        Status IN ('Activated','Expired')
                AND
                        Special_Participation__c Not IN ('Hotel Committee','Industrial Committee','Office Committee','Residential Committee','Shopping Committee')
        ];

    }

    public List<Club__c> findClubById(Set<Id> setId){

        return [
                SELECT
                        Id,
                        Name,
                        Club_Description__c,
                        OwnerId,
                        Owner.Name
                FROM
                        Club__c
                WHERE
                Id IN : setId
        ];

    }

    public List<Order> findOrderByClubId(Set<Id> setClubId){
        return [
                SELECT
                        Id,
                        AccountId,
                        Account.Name,
                        Account.URL_External_Logo__c,
                        A4_Company_Profile_New__c,
                        Brochure_Programme_Book_Logo_Position_Ne__c,
                        Business_Unit2__c,
                        Club__c,
                        Club__r.Name,
                        CompanyAuthorizedById,
                        CompanyAuthorizedBy.Name,
                        EffectiveDate,
                        EndDate,
                        Event__c,
                        Event_s_Club__c,
                        Event_Business_Unit__c,
                        Event_Division__c,
                        Logo__c,
                        OpportunityId,
                        Sizing_New__c,
                        Sponsor_Type__c,
                        Status__c
                FROM
                        Order
                WHERE
                Club__c IN : setClubId
                AND
                Status__c =: 'On Going'
                AND
                EndDate >= : System.today()
        ];
    }


}