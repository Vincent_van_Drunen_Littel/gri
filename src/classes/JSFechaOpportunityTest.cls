/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste da classe "JSFechaOpportunity".
*
* NAME: JSFechaOpportunityTest.cls
* AUTHOR: RVR                                                  DATE: 14/04/2016
*******************************************************************************/
@isTest
private class JSFechaOpportunityTest 
{
  private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');

  static testMethod void  testeFuncional()
  {
    Account acc = SObjectInstance.createAccount2();
    Database.insert(acc);

    Contact ctt = SObjectInstance.createContato(acc.Id);
    Database.insert(ctt);

    Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    Opportunity opp = SObjectInstance.createOpportunidade(acc.Id, ID_EVENT);
    opp.Contact__c = ctt.Id;
    opp.Event__c = evento.Id;
    opp.StageName = 'Prospect';
    Database.insert(opp);

    Test.startTest();

    JSFechaOpportunity.fechaOportunidade(evento.Id);
    opp.StageName = 'Prospect';
    opp.Lost_reason__c = 'Campaing closed';
    Database.update(opp);

    Test.stopTest();    

  }

 }