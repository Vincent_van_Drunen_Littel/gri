/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Scheduler para atualização das oportunidades após alteração do pricebook.
* Depende das classes PricebookUpdatesOportunidades e PricebookUpdatesOportunidadesTest
*
* NAME: SchUpdateOppByPricebook.cls
* AUTHOR: EAD                                                  DATE: 01/03/2018
*******************************************************************************/

global without sharing class SchUpdateOppByPricebook implements Database.Batchable<sObject>{

  private set<String> lstPBId = new set<String>(); 
  public static set<String> setLstOpp = new set<String>{
   'On Hold',
   'Prospect',
   'Information Sent',
   'Negotiation',
   'Closing'        
  };  
    
  //===== Constructor =====  
  public SchUpdateOppByPricebook(set<String> listPBId) 
  {
    this.lstPBId = listPBId;
  }    
    
  //===== QueryLocator =====
  global Database.QueryLocator start(Database.BatchableContext BC)
  {  
    String lQuery = 'SELECT Id FROM Opportunity WHERE Pricebook2Id =: lstPBId AND StageName =: setLstOpp';
    return Database.getQueryLocator(lQuery);
  }   
   
  //===== Execute ====  
  global void execute(Database.BatchableContext BC, list<Opportunity> scope)
  {
    if(scope.isEmpty()) return;  
    list<Opportunity> lstOpp = new list<Opportunity>();  
    for(Opportunity opp : (list<Opportunity>)scope)
    {
      opp.Approval_Process__c = null;  
      lstOpp.add(opp);  
    }
    database.update(lstOpp);  
  } 
  
  //===== Finish =====  
  global void finish(Database.BatchableContext info)
  {    
  }  
    
  //==== Método de chamada ==== 
  global static void iniciar(set<String> lPbId)
  {
    SchUpdateOppByPricebook lSch = new SchUpdateOppByPricebook(lPbId);
    database.executeBatch(lSch);
  }  
}