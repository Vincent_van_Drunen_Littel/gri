/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * Testclass for HelperMembership Class
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * @testClasses HelperSObject_Test  
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */

@IsTest
public class HelperMembership_Test {

	@isTest
	public static void HelperMembership_Test() 
	{
		List<Contact> listTheContact = [SELECT Id,FirstName,LastName,AccountId FROM Contact];
		Contact contact0 = listTheContact[0];
		Contact contact1 = listTheContact[1];
		Contact contact2 = listTheContact[2];
		Club__c brClub = [SELECT Id,Name FROM Club__c WHERE Name=:HelperClub.RE_BRAZIL];
		List<Contract> memberships = TestDataFactory.createMemberships(
			new List<Contact>{contact0, contact1, contact2}, brClub, System.today()+365);
		Contract membershipBr0 = memberships[0];
		Contract membershipBr1 = memberships[1];
		Contract membershipBr2 = memberships[2];

		///////////////////////////////////////////////////////////////////////////////////////////
		// Start tests
		///////////////////////////////////////////////////////////////////////////////////////////

		Test.startTest();

		membershipBr1.Allowed_Open_Events_Worldwide__c = '5';
		membershipBr1.Allowed_Exclusive_Events__c = '3';
		Integer eventsWorldwide = HelperMembership.AllowedOpenEventsWorldwide(membershipBr1);
		Integer exclusiveEvents = HelperMembership.AllowedExclusiveClubMeetings(membershipBr1);
		System.assertEquals(5, eventsWorldwide, 'Allowed_Open_Events_Worldwide__c and AllowedOpenEventsWorldwide dont match');
		System.assertEquals(3, exclusiveEvents, 'Allowed_Exclusive_Events__c and AllowedExclusiveClubMeetings dont match');

		membershipBr1.Allowed_Open_Events_Worldwide__c = 'All';
		membershipBr1.Allowed_Exclusive_Events__c = 'All';
		exclusiveEvents = HelperMembership.AllowedExclusiveClubMeetings(membershipBr1);
		eventsWorldwide = HelperMembership.AllowedOpenEventsWorldwide(membershipBr1);
		System.assertEquals(365, exclusiveEvents, 'Allowed_Open_Events_Worldwide__c and AllowedOpenEventsWorldwide dont match');
		System.assertEquals(365, eventsWorldwide, 'Allowed_Exclusive_Events__c and AllowedExclusiveClubMeetings dont match');

		membershipBr1.Allowed_Open_Events_Worldwide__c = 'Bubassauro';
		membershipBr1.Allowed_Exclusive_Events__c = 'Charmander';
		exclusiveEvents = HelperMembership.AllowedExclusiveClubMeetings(membershipBr1);
		eventsWorldwide = HelperMembership.AllowedOpenEventsWorldwide(membershipBr1);
		System.assertEquals(0, exclusiveEvents, 'Allowed_Open_Events_Worldwide__c and AllowedOpenEventsWorldwide dont match');
		System.assertEquals(0, eventsWorldwide, 'Allowed_Exclusive_Events__c and AllowedExclusiveClubMeetings dont match');

		membershipBr0.Status = HelperMembership.STATUS_ACTIVATED;
		membershipBr1.Status = HelperMembership.STATUS_EXPIRED;
		membershipBr2.Status = HelperMembership.STATUS_DRAFT;

		System.assertEquals(True, HelperMembership.isActive(membershipBr0), 'Membership status is wrong');
		System.assertEquals(True, HelperMembership.isActive(membershipBr1), 'Membership status is wrong');
		System.assertEquals(False, HelperMembership.isActive(membershipBr2), 'Membership status is wrong');

		// TODO
		List<Contact> listContacts = new List<Contact>{contact0, contact1};
		List<Contract> listMemberships = new List<Contract>{membershipBr1, membershipBr2};
		Map<Id, Boolean> contactMembershipMapClub = HelperMembership.contactHasMembership(listContacts, listMemberships, HelperClub.RE_BRAZIL);
		Map<Id, Boolean> contactMembershipMapAny = HelperMembership.contactHasMembership(listContacts, listMemberships, '');
		System.debug('TODO: replace this by a System.assertEquals()');

		Test.stopTest();
		
	}

    @testSetup 
    static void buildData()
    {
    	TestDataFactory.init();
    	TestDataFactory.createContactsWithDefaultAccount(3);
    	TestDataFactory.createClubs( 
    		new List<String> {HelperClub.RE_BRAZIL, HelperClub.RE_EUROPE, 
    			HelperClub.RE_LATAM, HelperClub.INFRA_LATAM});
   	}

}