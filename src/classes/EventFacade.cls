/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 * Class responsible for the abstraction of external systems with internal
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 15/01/19  EricB  First version.
 *
 */

public without sharing class EventFacade {

    public static final EventFacade instance = new EventFacade();
    public static final EventBO bo = EventBO.getInstance();

    private EventFacade(){}

    public static EventFacade getInstance(){ return instance;}

    public SiteTO.EventTO findOppByEventId(Id eventId){

        // Instancias
        SiteTO.EventTO eventTO = new SiteTO.EventTO();

        // Busca o evento pelo id do evento
        List<Event__c> listEvent = bo.findOppByEventId(eventId);

        // Verifica se o evento relacionado ao evento foi encontrado ou atendeu aos critérios
        if(listEvent.isEmpty()){
            throw new CustomException('Event not found or does not match search criteria',400);
        }
        Id IdEvento = listEvent[0].Id;

        Event__c event = listEvent[0];

        // Busca as campanhas relacionadas ao evento
        List<Campaign> listCampaign = bo.findCampaignsByEventId(IdEvento);

        // Busca os patrocinadores relacionados ao evento
        List<Order> listOrder = bo.findOrderByEventId(IdEvento);

        // Converte o patrocinador para EventTO
        if(!listOrder.isEmpty()){
            eventTO.sponsors = this.parseToSponsorTOFromListOrder(listOrder);
        }

        // Converte o evento para EventTO
        eventTO = this.parseToEventTOFromEvent(eventTO, event);

        // Converte o oportunidade para EventTO
        eventTO = this.parseToEventTOFromCampaign(eventTO, event, listCampaign);

        return  eventTO;
    }

    public SiteTO.EventTO findEventById(Id eventId){

        // Instancias
        SiteTO.EventTO eventTO = new SiteTO.EventTO();

        //Busca o evento pelo id
        List<Event__c> listEvent = bo.findEventById(eventId);

        // Verifica se o event foi encontrado
        if(listEvent.isEmpty()){
            throw new CustomException('Event not found',400);
        }
        Event__c event = listEvent[0];

        // Parse para EventTO
        eventTO = this.parseToEventTOFromEvent(eventTO, event);

        return  eventTO;

    }

    public List<SiteTO.SponsorTO> parseToSponsorTOFromListOrder(List<Order> listOrder){

        List<SiteTO.SponsorTO> listSponsorTO = new List<SiteTO.SponsorTO>();

        for(Order order : listOrder){
            SiteTO.SponsorTO sponsorTO = new SiteTO.SponsorTO();
            listSponsorTO.add(this.parseToSponsorTOFromOrder(sponsorTO, order));
        }

        return listSponsorTO;


    }

    public SiteTO.SponsorTO parseToSponsorTOFromOrder(SiteTO.SponsorTO sponsorTO, Order order){

        try{
            sponsorTO.account = order.Account.Name;
            sponsorTO.accountId = order.AccountId;
            sponsorTO.logoUrl = order.Account.URL_External_Logo__c;
            sponsorTO.a4CompanyProfile = order.A4_Company_Profile_New__c;
            sponsorTO.bookLogoPosition = order.Brochure_Programme_Book_Logo_Position_Ne__c;
            sponsorTO.companyProfile = order.CompanyAuthorizedBy.Name;
            sponsorTO.businessUnit = order.Business_Unit2__c;
            sponsorTO.eventId = order.Event__c;
            sponsorTO.eventClubId = order.Event_s_Club__c;
            sponsorTO.eventBussinessUnit = order.Event_Business_Unit__c;
            sponsorTO.eventDivision = order.Event_Division__c;
            sponsorTO.club = order.Club__r.Name;
            sponsorTO.clubId = order.Club__c;
            sponsorTO.sizing = order.Sizing_New__c;
            sponsorTO.sponsorType = order.Sponsor_Type__c;
            sponsorTO.status = order.Status__c;
            sponsorTO.orderStartDate = String.valueOf(order.EffectiveDate);
            sponsorTO.orderEndDate = String.valueOf(order.EndDate);

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return sponsorTO;

    }

    public SiteTO.EventTO parseToEventTOFromEvent(SiteTO.EventTO eventTO, Event__c event){

        try{
            eventTO.id = event.Id;
            eventTO.name = event.Name;
            eventTO.address = event.Billing_Address__c;
            eventTO.bussinessUnit = event.Business_Unit__c;
            eventTO.clubId = event.Club__c;
            eventTO.clubName = event.Club__r.Name;
            eventTO.endDate = event.End_Date__c;
            eventTO.startDate = event.Start_Date__c;
            eventTO.description = event.Event_Description__c;
            eventTO.descriptionForeignLanguage = event.Event_Description_Foreign_Language__c;
            eventTO.descriptionForeignLanguage2 = event.Event_Description_Foreign_Language_2__c;
            eventTO.eventTime = event.Event_Time__c;
            eventTO.eventYear = event.Event_Year2__c;
            eventTO.financeTeamEmail = event.Finance_Team_Email__c;
            eventTO.informalName = event.Informal_Name__c;
            eventTO.invoiceGRIEmail = event.Invoice_GRI_Email__c;
            eventTO.ownerId = event.OwnerId;
            eventTO.ownerName = event.Owner.Name;
            eventTO.phone = event.Phone__c;
            eventTO.publishOnWebsite = event.Publish_on_Website__c;
            eventTO.regPhone = event.Reg_Phone__c;
            eventTO.registerLink = event.RegisterLink__c;
            eventTO.sector = event.Sector__c;
            eventTO.status = event.Status__c;
            eventTO.type = event.Type__c;
            eventTO.vipDinnerIncluded = event.Vip_Dinner_Included__c;
            eventTO.website = event.Website__c;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return eventTO;
    }

    public SiteTO.EventTO parseToEventTOFromCampaign(SiteTO.EventTO eventTO, Event__c event, List<Campaign> listCampaign){

        try{
            eventTO.id = event.Id;
            eventTO.name = event.Name;
            eventTO.address = event.Billing_Address__c;
            eventTO.bussinessUnit = event.Business_Unit__c;
            eventTO.clubId = event.Club__c;
            eventTO.clubName = event.Club__r.Name;
            eventTO.endDate = event.End_Date__c;
            eventTO.startDate = event.Start_Date__c;
            eventTO.description = event.Event_Description__c;
            eventTO.descriptionForeignLanguage = event.Event_Description_Foreign_Language__c;
            eventTO.descriptionForeignLanguage2 = event.Event_Description_Foreign_Language_2__c;
            eventTO.eventTime = event.Event_Time__c;
            eventTO.eventYear = event.Event_Year2__c;
            eventTO.financeTeamEmail = event.Finance_Team_Email__c;
            eventTO.informalName = event.Informal_Name__c;
            eventTO.invoiceGRIEmail = event.Invoice_GRI_Email__c;
            eventTO.ownerId = event.OwnerId;
            eventTO.ownerName = event.Owner.Name;
            eventTO.phone = event.Phone__c;
            eventTO.publishOnWebsite = event.Publish_on_Website__c;
            eventTO.regPhone = event.Reg_Phone__c;
            eventTO.registerLink = event.RegisterLink__c;
            eventTO.sector = event.Sector__c;
            eventTO.status = event.Status__c;
            eventTO.type = event.Type__c;
            eventTO.vipDinnerIncluded = event.Vip_Dinner_Included__c;
            eventTO.website = event.Website__c;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        // Parse of Compaign
        if(!listCampaign.isEmpty()){
            eventTO.campaings = new List<SiteTO.CampaignTO>();

            for(Campaign campaign : listCampaign){
                // parse of Campaing
                eventTO.campaings.add(this.parseTOCampaignTOFromCampaign(campaign));
            }

        }

        if(!event.Events_Attendes__r.isEmpty()){
            List<SiteTO.EventAttendeTO> listEventAttendeTO = new List<SiteTO.EventAttendeTO>();

            for(Event_Attendee__c eventAttendee : event.Events_Attendes__r){
                listEventAttendeTO.add(this.parseToEventAttendeTOFromEventAttende(eventAttendee));
            }

            eventTO.eventAttendes = listEventAttendeTO;
        }

        // Parse of Contact
        List<SiteTO.CompanyTO> listCompanyTO = new List<SiteTO.CompanyTO>();
        Set<Contact> setContact = new Set<Contact>();

        List<Event_Attendee__c> eventAttendeeList= event.Events_Attendes__r;
        for(Event_Attendee__c evt : eventAttendeeList){
            setContact.add(evt.Contact__r);
        }


        List<Attendee__c> sessionAttendeeList = event.Attendees__r;
        for(Attendee__c sess : sessionAttendeeList){
            setContact.add(sess.Contact__r);
        }
        
        List<Contact> conList = bo.findContactByContactSet(setContact);

        for(Contact con : conList){
            listCompanyTO.add(parseTOCompanyTOFromContact(con));
        }

        eventTO.companies = listCompanyTO;

        if(!event.Sessions__r.isEmpty()){
            List<SiteTO.SessionTO> listSessionTO = new List<SiteTO.SessionTO>();
            Map<Id, List<SiteTO.SessionAttendeTO>> mapSessionAttendeTOBySessionId = new Map<Id, List<SiteTO.SessionAttendeTO>>();
            Map<Id, SiteTO.SessionTO> mapSessionTOBySessionId = new Map<Id, SiteTO.SessionTO>();

            // Iteration SessionAttende
            for(Attendee__c attendee : event.Attendees__r){

                // Parse of Session
                SiteTO.SessionTO sessionTO = this.parseToSessionTOFromAttende(attendee);

                // Parse of Session Attende
                SiteTO.SessionAttendeTO sessionAttendeTO = this.parseToSessionAttendeTOFromAttendee(attendee);

                // insert Session into map
                mapSessionTOBySessionId.put(attendee.Session__c, sessionTO);

                // insert SessionAttendeTO into map
                if(!mapSessionAttendeTOBySessionId.containsKey(attendee.Session__c)){
                    mapSessionAttendeTOBySessionId.put(attendee.Session__c,new List<SiteTO.SessionAttendeTO>());
                }
                mapSessionAttendeTOBySessionId.get(attendee.Session__c).add(sessionAttendeTO);
            }

            // get Session Attende
            for(SiteTO.SessionTO sessionTO : mapSessionTOBySessionId.values()){
                List<SiteTO.SessionAttendeTO> listSessionAttendeTO = mapSessionAttendeTOBySessionId.get(sessionTO.id);
                sessionTO.sessionAttendes = listSessionAttendeTO;
                listSessionTO.add(sessionTO);
            }

            eventTO.sessions = listSessionTO;
        }

        return eventTO;
    }

    public SiteTO.CampaignTO parseTOCampaignTOFromCampaign (Campaign campaign){

        SiteTO.CampaignTO campaignTO = new SiteTO.CampaignTO();
        try{
            campaignTO.id = campaign.Id;
            campaignTO.name = campaign.Name;
            campaignTO.typeDigitalCampaign = campaign.Digital_Campaign_Type__c;
        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return campaignTO;
    }

        public SiteTO.CompanyTO parseTOCompanyTOFromContact(Contact contact){

        SiteTO.CompanyTO companyTO = new SiteTO.CompanyTO();
        try{
            companyTO.nameBadge = contact.Company_Name_Badge__c;
            companyTO.contactId = contact.Id;
            companyTO.contactNameBadge = contact.Badge_Name__c;
            companyTO.contactJobTitleBadge = contact.Job_title_badge__c;
            companyTO.contactIdCaseSafe = contact.ContactIDCaseSafe__c;
            companyTO.city = contact.MailingCity;
            companyTO.country= contact.MailingCountry;
            companyTO.countryCode= contact.MailingCountryCode;
            companyTO.state = contact.MailingState;
            companyTO.stateCode = contact.MailingStateCode;
            companyTO.specialty = contact.Specialty_New__c;
            companyTO.foreignSpecialty = contact.Foreign_Specialty_New__c;
            companyTO.foreignSpecialty2 = contact.Foreign_Speciality_2__c;
            companyTO.description = contact.Description_New__c;
            companyTO.foreignDescription = contact.Foreign_Description_New__c;
            companyTO.foreignDescription2 = contact.Foreign_Company_Description_2__c;
            companyTO.foreignPersonalSummaryBiography = contact.Foreign_Personal_Summary_Biography__c;
            companyTO.foreignPersonalSummaryBiography2 = contact.Foreign_Personal_Summary_Biography_2__c;
            companyTO.foreignKeynoteSummary = contact.Foreign_Keynote_Summary_new__c;
            companyTO.foreignKeynoteSummary2 = contact.Foreign_Keynote_Summary_2__c;
            companyTO.keynoteSummary = contact.Keynote_Summary_new__c;
            companyTO.personalSummaryBiography = contact.Personal_Summary_Biography__c;
            companyTO.phone = contact.Phone;
            companyTO.photoUrl = contact.URL_External__c;
            companyTO.logoUrl = contact.Account.URL_External_Logo__c;

            //companyTO.logoName = contact.Account.;
            //companyTO.summaryProfile = contact.Personal_Summary_Biography__c;
            /*companyTO.advisorCompanyMemberBlack = contact.Personal_Summary_Biography__c;
            companyTO.foreignCompanySummaryProfile = contact.Personal_Summary_Biography__c;
            companyTO.foreignInformationLanguage2 = contact.Foreign_Information_2_Language__c;
            companyTO.foreignInformationLanguage = contact.Personal_Summary_Biography__c;
            companyTO.foreignSponsorProfileBrochure = contact.Personal_Summary_Biography__c;
            companyTO.foreignSponsorProfileBrochure2 = contact.Personal_Summary_Biography__c;
            companyTO.foreignSponsorProfileProgramBook = contact.Personal_Summary_Biography__c;
            companyTO.foreignSponsorProfileProgramBook2 = contact.Personal_Summary_Biography__c;
            companyTO.InformalName
            companyTO.SponsorProfileBrochure
            companyTO.SponsorProfileProgramBook*/

           }  catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }
        return companyTO;
    }

    public SiteTO.SessionAttendeTO parseToSessionAttendeTOFromAttendee (Attendee__c attendee){

        SiteTO.SessionAttendeTO sessionAttendeTO = new SiteTO.SessionAttendeTO();

        try{

            sessionAttendeTO.contactId = attendee.Contact__c;
            sessionAttendeTO.sessionAttendeeId = attendee.Id;
            sessionAttendeTO.sessionAttendeeName = attendee.Name;
            sessionAttendeTO.publishOnline = attendee.Publish_Online__c;
            sessionAttendeTO.publishSession = attendee.Publish_Session__c;
            sessionAttendeTO.sessionPosition = attendee.Session_Position__c;
            sessionAttendeTO.sessionStatus = attendee.Session_Status__c;
            sessionAttendeTO.programFeature = attendee.Program_Feature__c;
            sessionAttendeTO.showasAttendingCoChair = attendee.Show_as_Attending_Co_Chair__c;

        } catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return sessionAttendeTO;

    }

    public SiteTO.EventAttendeTO parseToEventAttendeTOFromEventAttende (Event_Attendee__c eventAttendee){

        SiteTO.EventAttendeTO eventAttendeeTO = new SiteTO.EventAttendeTO();

        try{
            eventAttendeeTO.contactId = eventAttendee.Contact__c;
            eventAttendeeTO.eventAttendeeId = eventAttendee.Id;
            eventAttendeeTO.eventAttendeeName = eventAttendee.Name;
            eventAttendeeTO.showAttendeeonWebsite = eventAttendee.ShowAttendeeonWebsite__c;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return eventAttendeeTO;

    }

    public SiteTO.SessionTO parseToSessionTOFromAttende (Attendee__c attendee){

        SiteTO.SessionTO sessionTO = new SiteTO.SessionTO();
        try{
            sessionTO.id  = attendee.Session__c;
            sessionTO.name  = attendee.Session__r.Name;
            sessionTO.foreignTitle  = attendee.Session__r.Foreign_title__c;
            sessionTO.foreignTitle2  = attendee.Session__r.Foreign_title_2__c;
            sessionTO.description  = attendee.Session__r.Description__c;
            sessionTO.language  = attendee.Session__r.Language__c;
            sessionTO.foreignLanguage  = attendee.Session__r.Foreign_Language_1__c;
            sessionTO.foreignLanguage2  = attendee.Session__r.Foreign_Language_2__c;
            sessionTO.foreignDescription  = attendee.Session__r.Foreign_description__c;
            sessionTO.foreignDescription2  = attendee.Session__r.Foreign_description_2__c;
            sessionTO.programIndexing  = attendee.Session__r.Program_Indexing__c;
            sessionTO.se  = attendee.Session__r.Se__c;
            sessionTO.room  = attendee.Session__r.Room__c;
            sessionTO.publishOnline  = attendee.Session__r.Publish_Online__c;
            sessionTO.title  = attendee.Session__r.Title__c;
            sessionTO.translation  = attendee.Session__r.Translation__c;
            sessionTO.type  = attendee.Session__r.Type__c;
            sessionTO.status  = attendee.Session__r.Status__c;
            sessionTO.sessionIndexing  = attendee.Session__r.Session_indexing__c;
            sessionTO.eventId  = attendee.Session__r.Event__c;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return sessionTO;

    }
}