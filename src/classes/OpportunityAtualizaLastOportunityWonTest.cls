/*******************************************************************************
*                               Cloud2b - 2017
*-------------------------------------------------------------------------------
*
* Classe responsável por fazer o test da classe OpportunityAtualizaLastOpportunityWon
* que está sendo disparada pelo Trigger Opportunity_after
* 
*
* NAME: OpportunityAtualizaLastOportunityWonTest.cls
* AUTHOR: CFA                                                  DATE: 13/03/2017
*******************************************************************************/
@isTest
private class OpportunityAtualizaLastOportunityWonTest {
	
  private static final Id RECOPPORTUNITY = RecordTypeMemory.getRecType('Opportunity','Open_Events');
  private static final Integer LOTE = 200;
  private static Product2 lProduct2;
  private static Account lAccount;
  private static Contact lContact;
  private static Event__c lEventc;
  private static PricebookEntry lPricebookEntry;
   
  static{
    
      lProduct2 = SObjectInstance.createProduct2();
      database.insert(lProduct2);
       
      lAccount = SObjectInstance.createAccount2();
      database.insert(lAccount);
      
      lContact = SObjectInstance.createContato(lAccount.Id);
      database.insert(lContact);  
      
      lEventc = SObjectInstance.createEvent();
      database.insert(lEventc);
      
      // Criação do PricebookEntry
      id pbkId = SObjectInstance.catalogoDePrecoPadrao2();
      lPricebookEntry = SObjectInstance.entradaDePreco(pbkId,lProduct2.Id);
      database.insert(lPricebookEntry);      
    
  }
    static testMethod void testeFuncional() {
    	
     list<OpportunityLineItem> lListOpportunityLineItem = new list<OpportunityLineItem>();
     
     // Criação da Opportunity
     Opportunity lOpportunity = SObjectInstance.createOpportunidade2(lAccount.Id, RECOPPORTUNITY, lContact.Id, lEventc.Id);
     database.insert(lOpportunity);
     system.debug('Criação da Opportunity: '+lOpportunity);
       
     // Criação do OpportunityLineItem
     OpportunityLineItem lOpportunityLineItem = SObjectInstance.createProdutoOportunidade(lOpportunity.Id, lPricebookEntry.Id);
     lListOpportunityLineItem.add(lOpportunityLineItem);
     database.insert(lListOpportunityLineItem);

     test.startTest();
     lOpportunity.StageName = 'Won';
     database.update(lOpportunity);
     test.stopTest();
     
     Account lAccountAfter = [select Id, Last_Opportunity_Won__c from Account where id =: lOpportunity.AccountId];
     
     system.assert(lAccountAfter.Last_Opportunity_Won__c == system.today());
     
    }
    
    static testMethod void testeLote() {
      
      list<OpportunityLineItem> lListOpportunityLineItemLote = new list<OpportunityLineItem>();
      list<Opportunity> lListOpportunityLote = new list<Opportunity>();
      list<Event__c> lListEventcLote = new list<Event__c>();
     
      for( Integer i = 0; i < LOTE; i++)
      {
        Event__c lEventcLote = SObjectInstance.createEvent();
        lListEventcLote.add(lEventcLote);
      }
      database.insert(lListEventcLote);
     
      // Criação da Opportunity
      for( Integer i = 0; i < LOTE; i++)
      {
        Opportunity lOpportunityLote = SObjectInstance.createOpportunidade2(lAccount.Id, RECOPPORTUNITY, lContact.Id, lListEventcLote[i].Id);
        lOpportunityLote.MassOppOn__c = true;
        lListOpportunityLote.add(lOpportunityLote);
      }
      database.insert(lListOpportunityLote);
       
      // Criação do OpportunityLineItem
      for(Opportunity opp : lListOpportunityLote)
      {
        OpportunityLineItem lOpportunityLineItemLote = SObjectInstance.createProdutoOportunidade(opp.Id, lPricebookEntry.Id);
        lListOpportunityLineItemLote.add(lOpportunityLineItemLote);
      }
      database.insert(lListOpportunityLineItemLote);
      
      list<Opportunity> lListOpportuniyUpdate = new list<Opportunity>();
      for(Opportunity opp : [select Id, Account.Last_Opportunity_Won__c from Opportunity where Id =: lListOpportunityLote])
      {
        opp.StageName = 'Won';
        lListOpportuniyUpdate.add(opp);
      }
      
      test.startTest();
      database.update(lListOpportuniyUpdate);
      test.stopTest();
      
      list<Opportunity> lOppAccount = [select Id, Account.Last_Opportunity_Won__c from Opportunity where Id =: lListOpportunityLote];
      
      for(Opportunity opps : lOppAccount)
      {
     	  system.assertEquals(system.today(), opps.Account.Last_Opportunity_Won__c);
      }
     
    }
    
    static testMethod void testeErro() {
    	
     list<OpportunityLineItem> lListOpportunityLineItemErro = new list<OpportunityLineItem>();
     list<Opportunity> lListOpportuniyUpdateErro = new list<Opportunity>();

     // Criação da Opportunity
     Opportunity lOpportunityErro = SObjectInstance.createOpportunidade2(lAccount.Id, RECOPPORTUNITY, lContact.Id, lEventc.Id);
     database.insert(lOpportunityErro);
            
     // Criação do OpportunityLineItem
     OpportunityLineItem lOpportunityLineItemErro = SObjectInstance.createProdutoOportunidade(lOpportunityErro.Id, lPricebookEntry.Id);
     lListOpportunityLineItemErro.add(lOpportunityLineItemErro);
     database.insert(lListOpportunityLineItemErro);
     
     for(Opportunity opp : [select Id, StageName from Opportunity where Id =: lOpportunityErro.Id])
     {
       opp.StageName = 'Approved';
       lListOpportuniyUpdateErro.add(opp);
     }

     test.startTest();
       database.update(lListOpportuniyUpdateErro);
     test.stopTest();
     
     Account lAccountAfterErro = [select Id, Last_Opportunity_Won__c from Account where id =: lOpportunityErro.AccountId];
     
     system.assert(lAccountAfterErro.Last_Opportunity_Won__c != system.today());    	
    }
}