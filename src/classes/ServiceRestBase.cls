/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 *  Base class of a service rest.
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 15/01/19  EricB  First version.
 *
 */

global abstract without sharing class ServiceRestBase {

    private ReturnRestTO responseData {get; set;}
    private String serviceName {get; set;}
    private RestResponse response {get; set;}
    private Boolean isReturnMetadata {get; set;}

    /**
     *
     *
     * @param method HTTP protocol method
     */
    global abstract void processService(String method);

    /**
     * set name of service and instance response object
     *
     * @param nameService
     */
    global void setServiceRestBase(String nameService){
        this.responseData = new ReturnRestTO(nameService);
        this.response = RestContext.response;
        this.isReturnMetadata = true;
    }

    /**
     * set name of service and instance response object
     *
     * @param nameService
     */
    global void setServiceRestBaseName(String nameService){
        this.responseData = new ReturnRestTO(nameService);
    }


    /**
     * determines whether to return metadata
     *
     * @param isReturnMeta
     */
    global void setReturnMeta(Boolean isReturnMeta){
        this.isReturnMetadata = isReturnMeta;
    }

    /**
     * Assign data to be returned
     *
     * @param dataReturn data that will be returned
     * @param statusCodeHttp status response code
     */
    global void returnSuccess(String dataReturn, Integer statusCodeHttp){

        Object datasReturnObj = JSON.deserializeUntyped(dataReturn);


            this.responseData.data = JSON.deserializeUntyped(JSON.serialize(datasReturnObj));
            this.responseData.meta.statusHttp = statusCodeHttp;
            this.responseData.meta.message = 'OK';

//            datasReturnObj = JSON.deserializeUntyped(JSON.serialize(this.responseData));


        this.executeReturn(this.responseData);
    }

    /**
     * Assign data error to be returned
     *
     * @param errorMessage Error message
     */
    global void returnError(String errorMessage){
        this.responseData.meta.statusHttp = 500;
        this.responseData.meta.message = errorMessage;

        this.executeReturn(this.responseData);
    }

    /**
     * Assign data error to be returned
     *
     * @param errorMessage Error message
     * * @param statusCode Status Code
     */
    global void returnError(String errorMessage,Integer statusCode){
        this.responseData.meta.statusHttp = statusCode;
        this.responseData.meta.message = errorMessage;

        this.executeReturn(this.responseData);
    }

    /**
     * Generate response to who called the service
     *
     * @param returnRestTO response body ReturnRestTO
     */
    private void executeReturn(ReturnRestTO returnRestTO){

        if(this.isReturnMetadata){
            this.response.responseBody = Blob.valueOf(JSON.serialize(JSON.deserializeUntyped(JSON.serialize(returnRestTO))));
        }else{
            String returnRestTOSTring = JSON.serialize(JSON.deserializeUntyped(JSON.serialize(returnRestTO.data)));
            this.response.responseBody = Blob.valueOf(returnRestTOSTring);
        }

        this.response.statusCode = returnRestTO.meta.statusHttp;
        this.response.addHeader('Content-Type', 'application/json');
    }



}