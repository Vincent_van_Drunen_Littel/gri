@isTest
private class GDPR_Forget_Accounts_Test {
	static testMethod void GDPR_Forget_Accounts_Test() {
		Contact contact = new Contact();
		Account account = new Account();
		Opportunity opportunity = new Opportunity();
		Contract contract = new Contract();
		Club__c club = new Club__c();
		Application_Config__c apConf = new Application_Config__c();

		club.Name = 'testClub';
		club.CurrencyIsoCode = 'GBP';
		insert club;

		account.Name = 'testAccount';
		account.Type = 'Customer';
		insert account;

        apConf.Name = 'BucketUserId';
        apConf.Value__c = account.id;
        Database.insert(apConf);

		contact.FirstName = 'testFirst';
		contact.LastName = 'testLast';
		contact.Consent_Given_to_GDPR__c = 'No';
		contact.AccountId = account.Id;
		contact.Consent_Given_to_GDPR_Date_Time_Stamp__c = Date.today();
		insert contact;

		contract.AccountId = account.Id;
		contract.Status = 'Draft';
		contract.StartDate = Date.today();
		contract.CurrencyIsoCode = 'GBP';
		contract.Club__c = club.Id; 
		contract.Contact__c = contact.Id;
		insert contract;

		opportunity.Type = 'Membership';
		opportunity.Name = 'testOpp';
		opportunity.Contact__c = contact.Id;
		opportunity.Club__c = club.Id;
		opportunity.CloseDate = Date.today();
		opportunity.StageName = 'Won';

		insert opportunity;

		GDPR_Forget_Accounts forgetAccounts = new GDPR_Forget_Accounts();
		forgetAccounts.currentTime = Date.today().addDays(365);
  		database.executebatch(forgetAccounts, 25);

  		
  		Test.StartTest();
  		GDPR_Forget_Account_Schedulable mytest = new GDPR_Forget_Account_Schedulable ();
		SchedulableContext sc;
		mytest.execute(sc);
  		Test.stopTest();
	}
}