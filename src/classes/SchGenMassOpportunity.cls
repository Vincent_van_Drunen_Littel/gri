/*******************************************************************************
*                               Cloud2b - 2017
*-------------------------------------------------------------------------------
*
* Scheduler para geracao em massa de oportunidades
*
* NAME: SchGenMassOpportunity.cls
* AUTHOR: RLdO                                                 DATE: 02/03/2017
*******************************************************************************/
global without sharing class SchGenMassOpportunity implements Schedulable, Database.Batchable<SObject>, Database.Stateful
{
  global map<String, Mass_Opportunity__c> pMapMassOpp {get; set;}
  // retorna o e-mail do usuário que esta inicializando a criação de opps
  public static final String USR_EMAIL = UserInfo.getUserEmail();    

  // construtor
  global SchGenMassOpportunity()
  {      
    // inicializa mapa de configuracoes de mass opp por campanha, privilegiando a configuracao mais antiga
    this.pMapMassOpp = new map<String, Mass_Opportunity__c>();

    for (Mass_Opportunity__c iMassOpp: [select Id, CurrencyIsoCode, Campaign__c, Actual_Status__c, Club__c,
      Date_of_actual_status__c, Date_of_the_next_step__c, Event__c, Magazine__c,
      Next_Step__c, Opp_CloseDate__c, Opp_Currency__c, Opp_Owner__c, Opp_StageName__c,
      Pricebook__c,
      (select Id, PricebookEntry__c, Product__c, Quantity__c, UnitPrice__c from Mass_Opportunity_Products__r)
      FROM Mass_Opportunity__c where Processed__c = false order by CreatedDate desc])
    {
      this.pMapMassOpp.put(iMassOpp.Campaign__c, iMassOpp);
    }
  }

  global Database.Querylocator start(Database.Batchablecontext bc)
  {
//    set<String> lSetCamp = new set<String>();
    set<String> lSetCamp = this.pMapMassOpp.keyset();
    String lQuery = 'SELECT Id, ContactId, Status, CampaignId, CurrencyIsoCode, Opportunity_Created__c, '
      + ' Contact.FirstName, Contact.LastName, Contact.Id, '
      + ' Contact.Job_title_badge__c, Contact.AccountId, Contact.Account.Name, '
      + ' Campaign.Opportunity_Type__c, Campaign.Club__c, Campaign.Event__c, Campaign.Magazine__c, '
      + ' Campaign.Club__r.Name, Campaign.Event__r.Name, Campaign.Magazine__r.Name '
      + ' FROM CampaignMember '
      + ' WHERE CampaignId = :lSetCamp ' 
      + ' AND ContactId != null '
      + ' AND Opportunity_Created__c != true';
    return Database.getQueryLocator(lQuery);
  }

  global void execute(SchedulableContext sc)
  {
  }
  
  global void execute(Database.Batchablecontext bc, list<SObject> scope)
  {
    OpportunityVinculaPricebook.exec = true;
    OpportunityUpdatePricebookEProduto.exec = true;

    map<String, Opportunity> lMapOpp = new map<String, Opportunity>();
    map<String, OpportunityLineItem> lMapOppLI = new map<String, OpportunityLineItem>();

    for (CampaignMember iCM: (list<CampaignMember>)scope)
    {
      // recupera a configuracao de oportunidade
      Mass_Opportunity__c lMassOpp = this.pMapMassOpp.get(iCM.CampaignId);

      if (lMassOpp == null) continue;
      lMassOpp.Processed__c = true;

      String lOppRecType, lChaveOpp, lOppName = iCM.Contact.FirstName + ' ' + iCM.Contact.LastName + ' - ' + 
        iCM.Contact.Account.Name;

      if (String.isNotBlank(iCM.Campaign.Event__c) || String.isNotBlank(iCM.Campaign.Magazine__c))
      {
        //O recordtype das opps será de acordo com o campo na campanha Opportunity_Type__c
        //lOppRecType = OrgDefs.RT_OPP_MEMBERSHIP;
        lChaveOpp = iCM.ContactId + ' - ' + iCM.Contact.AccountId + ' - ' + iCM.Campaign.Event__c;
      }
      
      //if (String.isNotBlank(iCM.Campaign.Club__c))
      //{
      //seleciona o recordtype das opps
      lOppRecType = RecordTypeMemory.getRecType('Opportunity', iCM.Campaign.Opportunity_Type__c);
      //}

      iCM.Opportunity_Created__c = true;

      Opportunity lOpp = new Opportunity(CampaignId = iCM.CampaignId, RecordTypeId = lOppRecType, Name = lOppName);
      lOpp.AccountId = iCM.Contact.AccountId;
      lOpp.Contact__c = iCM.ContactId;
      if (String.isNotBlank(lChaveOpp)) lOpp.opp_unique__c = lChaveOpp;
      lOpp.StageName = lMassOpp.Opp_StageName__c;
      lOpp.Date_of_actual_status__c = lMassOpp.Date_of_actual_status__c;
      lOpp.Date_of_the_next_step__c = lMassOpp.Date_of_the_next_step__c;
      lOpp.CloseDate = lMassOpp.Opp_CloseDate__c;
      lOpp.CurrencyIsoCode = (lMassOpp.Magazine__c != null) ? 'BRL' : lMassOpp.CurrencyIsoCode; //opp.Magazine__c != null ? 'BRL' : oppWrapper.campMoeda;
      lOpp.OwnerId = lMassOpp.Opp_Owner__c;
      lOpp.Next_Step__c = lMassOpp.Next_Step__c;
      lOpp.Actual_Status__c = lMassOpp.Actual_Status__c;
      lOpp.Magazine__c = lMassOpp.Magazine__c;
      lOpp.Club__c = lMassOpp.Club__c;
      lOpp.Event__c = lMassOpp.Event__c;
      lOpp.Pricebook2Id = lMassOpp.Pricebook__c;
      lOpp.MassOppOn__c = true;
      lMapOpp.put(iCM.ContactId, lOpp);

      for (Mass_Opportunity_Product__c iMOP: lMassOpp.Mass_Opportunity_Products__r)
      {
        OpportunityLineItem lOli = new OpportunityLineItem(Opportunity = lOpp, PricebookEntryId = iMOP.PricebookEntry__c);
        lOli.Quantity = iMOP.Quantity__c;
        lOli.UnitPrice = iMOP.UnitPrice__c;

        lMapOppLI.put(iCM.ContactId + '---' + iMOP.PricebookEntry__c, lOli);
      }
    }

    if (!lMapOpp.isEmpty()) database.insert(lMapOpp.values(), false);
    for (OpportunityLineItem iOLI: lMapOppLI.values()) iOLI.OpportunityId = iOLI.Opportunity.Id;
    if (!lMapOppLI.isEmpty()) database.insert(lMapOppLI.values(), false);
    database.update(scope);
  }

  global void finish(Database.Batchablecontext bc)
  {
    if (!this.pMapMassOpp.isEmpty()) database.update(this.pMapMassOpp.values());
      
    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
    message.toAddresses = new String[] {USR_EMAIL};
    message.subject = 'Mass Opportunities - Status';
    message.plainTextBody = 'The opportunities were created at ' + system.now().addHours(-2);
    Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);  
  }

  global static void iniciar()
  {
    SchGenMassOpportunity lSch = new SchGenMassOpportunity();
    database.executeBatch(lSch);
  }
}