/*******************************************************************************
*                               Cloud2b - 2017
*-------------------------------------------------------------------------------
*
* Classe de definicoes gerais para a org
* - depende de RecordTypeMemory
*
* NAME: OrgDefs.cls
* AUTHOR: RLdO                                                 DATE: 02/03/2017
*******************************************************************************/
public without sharing class OrgDefs
{
  public static final Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
  public static final Id RT_OPP_MAGAZINEAD = RecordTypeMemory.getRecType('Opportunity', 'Magazine');
  public static final Id RT_OPP_OPENEVENTS = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  public static final Id RT_OPP_UNIQUE = RecordTypeMemory.getRecType('Opportunity', 'SPEX');
}