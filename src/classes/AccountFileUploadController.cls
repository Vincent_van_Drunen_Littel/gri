/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe resposanvel pela inserção de imagens no registro de conta
*
*
*
* NAME: AccountFileUploadController
* AUTHOR: VMDL                                               DATE: 25/11/2016
*******************************************************************************/
public with sharing class AccountFileUploadController {

	public Document document 
    {
    	get 
	    {
	      if (document == null)
	        document = new Document();
	      return document;
	    }
	  set;
 	 }

    public List<SelectOption> lstOptionsPhoto {get; set;}
    public String previsao {get; set;}
    private Id lContaId {get; set;}
    private Account lAcc = new Account();
    private Photo_Name__c photoSeparator = Photo_Name__c.getValues('PhotoName');

	public AccountFileUploadController(ApexPages.StandardController controller) 
	{
		this.lstOptionsPhoto = new List<SelectOption>();
        this.lContaId = controller.getId();

     	this.lAcc = [SELECT Id , Name FROM Account WHERE Id =: lContaId];

     	lstOptionsPhoto.add(New SelectOption('','--None--'));

     	Schema.DescribeFieldResult options = Account.VF_Photo_Logo__c.getDescribe();
        List<Schema.PicklistEntry> lsOpt = options.getPicklistValues();

        for(Schema.PicklistEntry value : lsOpt)
        { 
            lstOptionsPhoto.add(New SelectOption(value.getValue(), value.getValue()));
        }
	}

	public PageReference upload() 
	{
		document.AuthorId = UserInfo.getUserId();

		if( previsao == 'Original Logo' )
		{
			String folderName = 'Original Files';
        	document.FolderId = [SELECT Id FROM Folder 
                        WHERE Name =: folderName].Id;
		}
		else if( previsao == 'Marketing Edited Logo' )
	    {   
	        String folderName = 'Marketing Edited Files';
	        document.FolderId = [SELECT Id FROM Folder 
	                            WHERE Name =: folderName].Id;
	    }
		try
		{
			if(String.isBlank(previsao)) 
	        {
	            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Select File Type'));
	            return null;    
	        }
	        else if(document.body == null)
	        {
	            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Select a File'));
	            return null;
	        }
	        document.type = 'jpg'; 
        	document.Name = lContaId + photoSeparator.separator__c + previsao.replace(' ','_') + photoSeparator.Extension__c;
        	Database.insert( document );

        	String urlDoc = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=' + document.Id;
        	Account lAcc = new Account(Id = lContaId);

        	if(previsao == 'Original Logo')
	        {
	            lAcc.Original_Logo__c = urlDoc;
	            lAcc.File_Name_Original_logo__c = document.Name;
	        }
	        else if(previsao == 'Marketing Edited Logo')
	        {
	            lAcc.Marketing_Edited_Logo__c = urlDoc;
	            lAcc.File_Name_Marketing_Edited_logo__c = document.Name;    
	        }
	        Database.update( lAcc );
	        //Clear
		        document = new Document();
		        previsao = null;
		        document.body = null; 
	        //Clear
			return new PageReference( URL.getSalesforceBaseUrl().toExternalForm() + '/' + lContaId );
		}
		catch (DMLException e) 
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
      		return null;
		}
		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully'));
    	return null;	
	}


}