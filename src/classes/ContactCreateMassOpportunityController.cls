/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe para criar oportunidades em massa através de Contato.
*
* NAME: ContactCreateMassOppController.cls
* AUTHOR: KHPS                                                 DATE: 14/04/2016
* MAINTENANCE: AFC                                             DATE: 30/06/2016
*******************************************************************************/
public class ContactCreateMassOpportunityController 
{
  private static final Id recOppMagazineAD = RecordTypeMemory.getRecType('Opportunity', 'Magazine');
  private static final Id recOppMembership = RecordTypeMemory.getRecType('Opportunity', 'Membership');
  private static final Id recOppOpenEvents = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  private static final Id recOppUnique = RecordTypeMemory.getRecType('Opportunity', 'SPEX');
  
  public Boolean loadPopup {get; set;}
  public String busca {get; set;}
  public Integer qtdProduct {get; set;}
  public Integer qdtOpportunity {get{return lstOppWrapper.size();}}
  
  public Opportunity opp {get; set;}
  public String oppCttType {get; set;}
    
  public List<SelectOption> getTypeOppCamp 
  {
    get 
    {
      List<SelectOption> lstOptions = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Campaign.Opportunity_type__c.getDescribe();
      List<Schema.PicklistEntry> lstPickListEntry = fieldResult.getPicklistValues();
    
    for(Schema.PicklistEntry ple : lstPickListEntry) 
    {
      lstOptions.add(new SelectOption(ple.getLabel(), ple.getValue()));
    }
      return lstOptions;
    }
  }    
    
  public list<OpportunityWrapper> lstOppWrapper {get; set;}
    
  public class OpportunityWrapper 
  {
    public Id oppRecType {get; set;}
    public String oppName {get; set;}
    public String cttJobTitle {get; set;}
    public String cttStatus {get; set;}
    public String cttPhone {get; set;}
    public String cttEmail {get; set;}
    public String campContact {get; set;}
    public String cttAccount {get; set;}
    public String cttId {get; set;}
    public String oppAccount {get; set;}
    public Contact lCtt {get; set;}
    
    public OpportunityWrapper(Id oppRecType) 
    {
      this.oppRecType = oppRecType;
    }
  }
  
  public list<ProductWrapper> lstProdutoWpp {get; set;}
    
  public class ProductWrapper
  {
    public Product2 prod {get; set;}
    public Boolean IsSelected {get; set;}
    public Integer Quantity {get; set;}
    public Double UnitPrice {get; set;}
    public String ListPrice {get; set;}
    
    public ProductWrapper(Product2 prod)
    {
      this.prod = prod;
      this.IsSelected = false;
    }
  }
    
  public list<Contact> stdCtt {get; set;}
  private Id cttId;
    
  public ContactCreateMassOpportunityController(ApexPages.StandardSetController controller) 
  {
    this.qtdProduct = 0;
    this.loadPopup = false;
    
    opp = new Opportunity();
    opp.CloseDate = System.today();
    opp.StageName = 'Suspect';
    
    oppCttType = 'Magazine';
    
    stdCtt = (list<Contact>)controller.getRecords();
    
    //SelecionaPricebook();
    //CarregaProdutos();
    
    lstOppWrapper = fillOppWrapper(stdCtt);
  }
    
  private list<OpportunityWrapper> fillOppWrapper(list<Contact> stdCtt) 
  {
    list<Contact> lstContact = [SELECT Id, FirstName, LastName, Job_title_badge__c, 
      Phone, Email, Status__c, AccountId, Account.Name 
      FROM Contact WHERE Id = :stdCtt];
      
    if(lstContact.isEmpty()) return new list<OpportunityWrapper>();
      
    list<OpportunityWrapper> lOppsWrapper = new list<OpportunityWrapper>();
      
    for(Contact lCtt : lstContact) 
    {
      Id oppRecType = recOppMagazineAD;
      String oppName = lCtt.FirstName + ' ' + lCtt.LastName;
      
      if(oppCttType == 'Magazine') 
      {
        oppRecType = recOppMagazineAD;
        oppName = lCtt.FirstName + ' ' + lCtt.LastName + ' - ' + 
         lCtt.Account.Name;
      }
      else if(oppCttType == 'Membership') 
      {
        oppRecType = recOppMembership;
        oppName = lCtt.FirstName + ' ' + lCtt.LastName + ' - ' + 
         lCtt.Account.Name;               
      }
      else if(oppCttType == 'Open_Events') 
      {
        oppRecType = recOppOpenEvents;
        oppName = lCtt.FirstName + ' ' + lCtt.LastName + ' - ' + 
         lCtt.Account.Name;                
      }
      else if(oppCttType == 'SPEX') 
      {
        oppRecType = recOppUnique;
        oppName = lCtt.FirstName + ' ' + lCtt.LastName + ' - ' + 
         lCtt.Account.Name;                
      }
          
      OpportunityWrapper oppWrapper = new OpportunityWrapper(oppRecType);
      oppWrapper.oppName = oppName;
      oppWrapper.cttJobTitle = lCtt.Job_title_badge__c;
      oppWrapper.cttStatus = lCtt.Status__c;
      oppWrapper.cttPhone = lCtt.Phone;
      oppWrapper.cttEmail = lCtt.Email;
      oppWrapper.campContact = lCtt.Id;
      oppWrapper.cttAccount = lCtt.Account.Name;
      oppWrapper.cttId = lCtt.Id;
      oppWrapper.oppAccount = lCtt.AccountId;
      oppWrapper.lCtt = lCtt;
          
      lOppsWrapper.add(oppWrapper);
    }
    return lOppsWrapper;
  }
  
  public PageReference createMassOpportunities() 
  {
    list<Opportunity> lstUpsertOpportunity = new list<Opportunity>(); 
    
    if(opp.CloseDate == null) 
    {
      adicionaErro('You must enter a value for Close date');
      return null;
    }
    else if(oppCttType == 'Magazine' && opp.Magazine__c == null) 
    {
      adicionaErro('You must enter a value for Magazine');
      return null;
    }
    else if(oppCttType == 'Open_Events' && opp.Event__c == null) 
    {
      adicionaErro('You must enter a value for Event');
      return null;
    }
    else if(oppCttType == 'Membership' && opp.Club__c == null) 
    {
      adicionaErro('You must enter a value for Club');
      return null;
    }
    else if(opp.OwnerId == null) 
    {
      adicionaAviso('You must enter a value for Owner');
      return null;
    }
    else if(qtdProduct == 0)
    {
      adicionaErro('You must select Product.');
      return null;
    }      

    for(OpportunityWrapper oppWrapper : lstOppWrapper) 
    {
      Opportunity lOpp = new Opportunity();
      lOpp.RecordTypeId = oppWrapper.oppRecType;
      lOpp.Name = oppWrapper.oppName;
      lOpp.StageName = opp.StageName;
      lOpp.CloseDate = opp.CloseDate;
      lOpp.AccountId = oppWrapper.oppAccount;
      lOpp.Contact__c = oppWrapper.cttId;
      lOpp.Magazine__c = opp.Magazine__c;
      lOpp.Event__c = opp.Event__c;
      lOpp.Club__c = opp.Club__c;
      lOpp.CurrencyIsoCode = opp.Magazine__c != null ? 'BRL' : 'USD';
      
      lOpp.OwnerId = opp.OwnerId;
	    lOpp.Next_Step__c = opp.Next_Step__c;
	    lOpp.Actual_Status__c = opp.Actual_Status__c;
	    
	    lOpp.Pricebook2Id = opp.Pricebook2Id;
      
      lstUpsertOpportunity.add(lOpp);
    }
    
    if(!lstUpsertOpportunity.isEmpty()) upsert lstUpsertOpportunity;
    
    map<Id, Id> mapProdPriceEntry = new map<Id, Id>();
    for(ProductWrapper iProd : lstProdutoWpp)
    {
      if(iProd.IsSelected)
      {
        mapProdPriceEntry.put(iProd.prod.Id, null);
      }
    }
    
    for(PricebookEntry iPbe : [SELECT Id, Product2Id FROM PricebookEntry 
      WHERE Pricebook2Id = :opp.Pricebook2Id AND Product2Id = :mapProdPriceEntry.keySet()])
    {
      mapProdPriceEntry.put(iPbe.Product2Id, iPbe.Id);
    }
      
    list<OpportunityLineItem> lstOliInsert = new list<OpportunityLineItem>();  
    for(Opportunity iOpp : lstUpsertOpportunity)
    {
      for(ProductWrapper iProd : lstProdutoWpp)
      {
        if(iProd.IsSelected)
        {
          OpportunityLineItem oli = new OpportunityLineItem();
          oli.OpportunityId = iOpp.Id;
          oli.PricebookEntryId = mapProdPriceEntry.get(iProd.prod.Id);
          oli.UnitPrice = iProd.UnitPrice;
          oli.Quantity = iProd.Quantity;
          
          lstOliInsert.add(oli);              
        }
      }
    }
    if(!lstOliInsert.isEmpty()) insert lstOliInsert;
    
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Opportunities has been created!'));
    
    return new PageReference('/006');
  }
    
  public PageReference cancel() 
  {
    return new PageReference('/003');
  }

  public PageReference changeName() 
  {
    for(OpportunityWrapper oppWrapper : lstOppWrapper) 
    {
      if(oppCttType == 'Magazine') 
      {
        oppWrapper.oppRecType = recOppMagazineAD;
        oppWrapper.oppName = oppWrapper.lCtt.FirstName + ' ' + oppWrapper.lCtt.LastName + ' - ' + 
         oppWrapper.lCtt.Account.Name;
        
        if(opp.Magazine__c == null) opp.Pricebook2Id = null;
      }
      else if(oppCttType == 'Membership') 
      {
        oppWrapper.oppRecType = recOppMembership;
        oppWrapper.oppName = oppWrapper.lCtt.FirstName + ' ' + oppWrapper.lCtt.LastName + ' - ' + 
         oppWrapper.lCtt.Account.Name;               
         
        if(opp.Club__c == null) opp.Pricebook2Id = null;
      }
      else if(oppCttType == 'Open_Events') 
      {
        oppWrapper.oppRecType = recOppOpenEvents;
        oppWrapper.oppName = oppWrapper.lCtt.FirstName + ' ' + oppWrapper.lCtt.LastName + ' - ' + 
         oppWrapper.lCtt.Account.Name;
        
        if(opp.Event__c == null) opp.Pricebook2Id = null;              
      }
      else if(oppCttType == 'SPEX') 
      {
        oppWrapper.oppRecType = recOppUnique;
        oppWrapper.oppName = oppWrapper.lCtt.FirstName + ' ' + oppWrapper.lCtt.LastName + ' - ' + 
         oppWrapper.lCtt.Account.Name;                
      }         
    }
    return null;        
  }
  
  public void SelecionaPricebook()
  {
    Id IdRelacionaPB = (opp.Event__c != null) ? opp.Event__c : (opp.Magazine__c != null) ? opp.Magazine__c : opp.Club__c;
      
    list<Pricebook2> lstPricebook = [SELECT Id, Starting_date__c, End_date__c FROM Pricebook2 
      WHERE Event__c = :IdRelacionaPB OR Magazine__c = :IdRelacionaPB OR Club__c = :IdRelacionaPB];
      
    for(Pricebook2 iPb : lstPricebook)
    {
      if(oppCttType == 'Open_Events')
      {
        if (opp.CloseDate >= iPb.Starting_date__c && opp.CloseDate <= iPb.End_date__c)
        {
          opp.Pricebook2Id = iPb.Id;
          return;
        }
      }
      else
      {
        opp.Pricebook2Id = iPb.Id;
        return;
      }
    }
    opp.Pricebook2Id = null;
  }
  
  public void CarregaProdutos()
  {
    String lBusca = (String.isNotBlank(this.busca)) ? this.busca : '';
    lBusca = lBusca.replace('\'','\\\'');
    
    String lQuery = '';
    lQuery += 'SELECT Id, Product2Id, Product2.Name, Product2.ProductCode, UnitPrice'
      + ' FROM PricebookEntry WHERE Pricebook2Id = \'' + opp.Pricebook2Id + '\'';
    
    if(String.isNotBlank(lBusca)) lQuery += ' and ( Product2.Name like \'%' + lBusca + '%\' or Product2.Family like \'%' + lBusca + '%\' or Product2.ProductCode like \'%' + lBusca + '%\')';
    
    lQuery += ' order by Product2.Name limit 20';
    
    system.debug('AFC lQuery = '+JSON.serializePretty(lQuery));
    
    list<PricebookEntry> lstPriceEntry = Database.query(lQuery);
    
    lstProdutoWpp = new list<ProductWrapper>();
      
    for(PricebookEntry iPbe : lstPriceEntry)
    {
      Product2 prod = new Product2(
        Id = iPbe.Product2Id,
        Name = iPbe.Product2.Name,
        ProductCode = iPbe.Product2.ProductCode
      );
      ProductWrapper lProdWpp = new ProductWrapper(prod);
      lProdWpp.ListPrice = Utils.formatValue(iPbe.UnitPrice,2);
      lProdWpp.UnitPrice = iPbe.UnitPrice;
      lstProdutoWpp.add(lProdWpp);
    }
  }
  
  public void loadPopup()
	{
	  this.loadPopup = true;
	}
	
	public void closePopup()
	{
	  this.loadPopup = false;
	  
	  if(opp.Pricebook2Id == null) return;
	  
	  countProductSelect();
	}
	
	public void closePopupAndClear()
	{
	  this.loadPopup = false;
	  
	  if(opp.Pricebook2Id == null) return;
	  
	  CarregaProdutos();
	  countProductSelect();
	}    
	    
	public void countProductSelect()
	{
	  qtdProduct = 0;
	  for(ProductWrapper iPWpp : lstProdutoWpp)
	    if(iPwpp.IsSelected)
	      qtdProduct++;
	}
	
	public void changeInformation()
	{
	  SelecionaPricebook();
	  CarregaProdutos();
	}
  
  @testvisible
  private static void adicionaErro(String msg)
  {
    ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.ERROR, msg) );
  }

  @testvisible
  private static void adicionaAviso(String msg)
  {
    ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.WARNING, msg) );
  }
}