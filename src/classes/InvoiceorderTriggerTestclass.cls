@isTest
public  class InvoiceorderTriggerTestclass {
 @isTest
  public static void Test1()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI Services Ltd';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'UK';
    insert event;
   
    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    insert opp;
    
     List<Application_Config__c> appconfigObjects1 = new List<Application_Config__c>();
     Application_Config__c appconfig1 = new Application_Config__c();
     appconfig1.Name                  = 'orgwideemailaddressid';
     appconfig1.Value__c              = '0D27A0000004Cu9';
     appconfigObjects1.add(appconfig1);
     insert appconfigObjects1;
     
     List<Application_Config__c> appconfigObjects2 = new List<Application_Config__c>();
     Application_Config__c appconfig2 = new Application_Config__c();
     appconfig2.Name                  = 'GRIUSAIncSwiss';
     appconfig2.Value__c              = '00X7A000000Do61';
     appconfigObjects2.add(appconfig2);
     insert appconfigObjects2;
     
     List<Application_Config__c> appconfigObjects3 = new List<Application_Config__c>();
     Application_Config__c appconfig3 = new Application_Config__c();
     appconfig3.Name                  = 'GRIUSAInc';
     appconfig3.Value__c              = '00X7A000000MHqaUAG';
     appconfigObjects3.add(appconfig3);
     insert appconfigObjects3;
     
     List<Application_Config__c> appconfigObjects4 = new List<Application_Config__c>();
     Application_Config__c appconfig4 = new Application_Config__c();
     appconfig4.Name                  = 'GRIServicesLtdSwiss';
     appconfig4.Value__c              = '00X7A000000Do5w';
     appconfigObjects4.add(appconfig4);
     insert appconfigObjects4;
     
     List<Application_Config__c> appconfigObjects5 = new List<Application_Config__c>();
     Application_Config__c appconfig5 = new Application_Config__c();
     appconfig5.Name                  = 'GRIServicesLtd';
     appconfig5.Value__c              = '00X7A000000MHqVUAW';
     appconfigObjects5.add(appconfig5);
     insert appconfigObjects5;
     
     List<Application_Config__c> appconfigObjects6 = new List<Application_Config__c>();
     Application_Config__c appconfig6 = new Application_Config__c();
     appconfig6.Name                  = 'GRIAmericanEuropeanLLCSwiss';
     appconfig6.Value__c              = '00X7A000000Do5r';
     appconfigObjects6.add(appconfig6);
     insert appconfigObjects6;
     
     List<Application_Config__c> appconfigObjects7 = new List<Application_Config__c>();
     Application_Config__c appconfig7 = new Application_Config__c();
     appconfig7.Name                  = 'GRIAmericanEuropeanLLC';
     appconfig7.Value__c              = '00X7A000000MHqQUAW';
     appconfigObjects7.add(appconfig7);
     insert appconfigObjects7;
    
    Invoice__c inv = new Invoice__c();
    inv.Account__c = acc.id;
    inv.Opportunity__c = opp.id;
    inv.CurrencyIsoCode = 'USD';
    inv.Email_Sent__c = false;
    inv.Send_Invoice__c = true;
    inv.Order_Description__c ='<table border="0" cellspacing="0" cellpadding="0" style="text-align:left; padding:10px"><tr><th style="border-bottom:1px solid black;text-align:left;"><b>Quantity</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Details</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Amount</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th></tr><tr><td>1.00&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>Co-chair&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>£1875.00&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td></tr></table>';
    insert inv;
    }
    
    @isTest
  public static void Test2()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'Swiss';
    insert event;
   
    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    opp.Apply_SWISS_Vat__c = true;
    insert opp;
    
        
     List<Application_Config__c> appconfigObjects1 = new List<Application_Config__c>();
     Application_Config__c appconfig1 = new Application_Config__c();
     appconfig1.Name                  = 'orgwideemailaddressid';
     appconfig1.Value__c              = '0D27A0000004Cu9';
     appconfigObjects1.add(appconfig1);
     insert appconfigObjects1;
     
     List<Application_Config__c> appconfigObjects2 = new List<Application_Config__c>();
     Application_Config__c appconfig2 = new Application_Config__c();
     appconfig2.Name                  = 'GRIUSAIncSwiss';
     appconfig2.Value__c              = '00X7A000000Do61';
     appconfigObjects2.add(appconfig2);
     insert appconfigObjects2;
     
     List<Application_Config__c> appconfigObjects3 = new List<Application_Config__c>();
     Application_Config__c appconfig3 = new Application_Config__c();
     appconfig3.Name                  = 'GRIUSAInc';
     appconfig3.Value__c              = '00X7A000000MHqaUAG';
     appconfigObjects3.add(appconfig3);
     insert appconfigObjects3;
     
     List<Application_Config__c> appconfigObjects4 = new List<Application_Config__c>();
     Application_Config__c appconfig4 = new Application_Config__c();
     appconfig4.Name                  = 'GRIServicesLtdSwiss';
     appconfig4.Value__c              = '00X7A000000Do5w';
     appconfigObjects4.add(appconfig4);
     insert appconfigObjects4;
     
     List<Application_Config__c> appconfigObjects5 = new List<Application_Config__c>();
     Application_Config__c appconfig5 = new Application_Config__c();
     appconfig5.Name                  = 'GRIServicesLtd';
     appconfig5.Value__c              = '00X7A000000MHqVUAW';
     appconfigObjects5.add(appconfig5);
     insert appconfigObjects5;
     
     List<Application_Config__c> appconfigObjects6 = new List<Application_Config__c>();
     Application_Config__c appconfig6 = new Application_Config__c();
     appconfig6.Name                  = 'GRIAmericanEuropeanLLCSwiss';
     appconfig6.Value__c              = '00X7A000000Do5r';
     appconfigObjects6.add(appconfig6);
     insert appconfigObjects6;
     
     List<Application_Config__c> appconfigObjects7 = new List<Application_Config__c>();
     Application_Config__c appconfig7 = new Application_Config__c();
     appconfig7.Name                  = 'GRIAmericanEuropeanLLC';
     appconfig7.Value__c              = '00X7A000000MHqQUAW';
     appconfigObjects7.add(appconfig7);
     insert appconfigObjects7;
    
    Invoice__c inv = new Invoice__c();
    inv.Account__c = acc.id;
    inv.Opportunity__c = opp.id;
    inv.CurrencyIsoCode = 'USD';
    inv.Order_Description__c ='<table border="0" cellspacing="0" cellpadding="0" style="text-align:left; padding:10px"><tr><th style="border-bottom:1px solid black;text-align:left;"><b>Quantity</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Details</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Amount</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th></tr><tr><td>1.00&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>Co-chair&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>£1875.00&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td></tr></table>';
    insert inv;
    
    }
    
     @isTest
  public static void Test3()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'UK';
    insert event;
   
    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    opp.Apply_SWISS_Vat__c = false;
    insert opp;

    
     List<Application_Config__c> appconfigObjects1 = new List<Application_Config__c>();
     Application_Config__c appconfig1 = new Application_Config__c();
     appconfig1.Name                  = 'orgwideemailaddressid';
     appconfig1.Value__c              = '0D27A0000004Cu9';
     appconfigObjects1.add(appconfig1);
     insert appconfigObjects1;
     
     List<Application_Config__c> appconfigObjects2 = new List<Application_Config__c>();
     Application_Config__c appconfig2 = new Application_Config__c();
     appconfig2.Name                  = 'GRIUSAIncSwiss';
     appconfig2.Value__c              = '00X7A000000Do61';
     appconfigObjects2.add(appconfig2);
     insert appconfigObjects2;
     
     List<Application_Config__c> appconfigObjects3 = new List<Application_Config__c>();
     Application_Config__c appconfig3 = new Application_Config__c();
     appconfig3.Name                  = 'GRIUSAInc';
     appconfig3.Value__c              = '00X7A000000MHqaUAG';
     appconfigObjects3.add(appconfig3);
     insert appconfigObjects3;
     
     List<Application_Config__c> appconfigObjects4 = new List<Application_Config__c>();
     Application_Config__c appconfig4 = new Application_Config__c();
     appconfig4.Name                  = 'GRIServicesLtdSwiss';
     appconfig4.Value__c              = '00X7A000000Do5w';
     appconfigObjects4.add(appconfig4);
     insert appconfigObjects4;
     
     List<Application_Config__c> appconfigObjects5 = new List<Application_Config__c>();
     Application_Config__c appconfig5 = new Application_Config__c();
     appconfig5.Name                  = 'GRIServicesLtd';
     appconfig5.Value__c              = '00X7A000000MHqVUAW';
     appconfigObjects5.add(appconfig5);
     insert appconfigObjects5;
     
     List<Application_Config__c> appconfigObjects6 = new List<Application_Config__c>();
     Application_Config__c appconfig6 = new Application_Config__c();
     appconfig6.Name                  = 'GRIAmericanEuropeanLLCSwiss';
     appconfig6.Value__c              = '00X7A000000Do5r';
     appconfigObjects6.add(appconfig6);
     insert appconfigObjects6;
     
     List<Application_Config__c> appconfigObjects7 = new List<Application_Config__c>();
     Application_Config__c appconfig7 = new Application_Config__c();
     appconfig7.Name                  = 'GRIAmericanEuropeanLLC';
     appconfig7.Value__c              = '00X7A000000MHqQUAW';
     appconfigObjects7.add(appconfig7);
     insert appconfigObjects7;
    
    Invoice__c inv = new Invoice__c();
    inv.Account__c = acc.id;
    inv.Opportunity__c = opp.id;
    inv.CurrencyIsoCode = 'USD';
    inv.Order_Description__c ='<table border="0" cellspacing="0" cellpadding="0" style="text-align:left; padding:10px"><tr><th style="border-bottom:1px solid black;text-align:left;"><b>Quantity</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Details</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Amount</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th></tr><tr><td>1.00&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>Co-chair&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>£1875.00&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td></tr></table>';
    insert inv;
    
    }

     @isTest
  public static void Test4()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI Services Ltd';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'UK';
    insert event;
   
    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    opp.Apply_SWISS_Vat__c = true;
    insert opp;

    
     List<Application_Config__c> appconfigObjects1 = new List<Application_Config__c>();
     Application_Config__c appconfig1 = new Application_Config__c();
     appconfig1.Name                  = 'orgwideemailaddressid';
     appconfig1.Value__c              = '0D27A0000004Cu9';
     appconfigObjects1.add(appconfig1);
     insert appconfigObjects1;
     
     List<Application_Config__c> appconfigObjects2 = new List<Application_Config__c>();
     Application_Config__c appconfig2 = new Application_Config__c();
     appconfig2.Name                  = 'GRIUSAIncSwiss';
     appconfig2.Value__c              = '00X7A000000Do61';
     appconfigObjects2.add(appconfig2);
     insert appconfigObjects2;
     
     List<Application_Config__c> appconfigObjects3 = new List<Application_Config__c>();
     Application_Config__c appconfig3 = new Application_Config__c();
     appconfig3.Name                  = 'GRIUSAInc';
     appconfig3.Value__c              = '00X7A000000MHqaUAG';
     appconfigObjects3.add(appconfig3);
     insert appconfigObjects3;
     
     List<Application_Config__c> appconfigObjects4 = new List<Application_Config__c>();
     Application_Config__c appconfig4 = new Application_Config__c();
     appconfig4.Name                  = 'GRIServicesLtdSwiss';
     appconfig4.Value__c              = '00X7A000000Do5w';
     appconfigObjects4.add(appconfig4);
     insert appconfigObjects4;
     
     List<Application_Config__c> appconfigObjects5 = new List<Application_Config__c>();
     Application_Config__c appconfig5 = new Application_Config__c();
     appconfig5.Name                  = 'GRIServicesLtd';
     appconfig5.Value__c              = '00X7A000000MHqVUAW';
     appconfigObjects5.add(appconfig5);
     insert appconfigObjects5;
     
     List<Application_Config__c> appconfigObjects6 = new List<Application_Config__c>();
     Application_Config__c appconfig6 = new Application_Config__c();
     appconfig6.Name                  = 'GRIAmericanEuropeanLLCSwiss';
     appconfig6.Value__c              = '00X7A000000Do5r';
     appconfigObjects6.add(appconfig6);
     insert appconfigObjects6;
     
     List<Application_Config__c> appconfigObjects7 = new List<Application_Config__c>();
     Application_Config__c appconfig7 = new Application_Config__c();
     appconfig7.Name                  = 'GRIAmericanEuropeanLLC';
     appconfig7.Value__c              = '00X7A000000MHqQUAW';
     appconfigObjects7.add(appconfig7);
     insert appconfigObjects7;
    
    Invoice__c inv = new Invoice__c();
    inv.Account__c = acc.id;
    inv.Opportunity__c = opp.id;
    inv.CurrencyIsoCode = 'USD';
    inv.Order_Description__c ='<table border="0" cellspacing="0" cellpadding="0" style="text-align:left; padding:10px"><tr><th style="border-bottom:1px solid black;text-align:left;"><b>Quantity</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Details</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Amount</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th></tr><tr><td>1.00&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>Co-chair&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>£1875.00&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td></tr></table>';
    insert inv;
    
    }

     @isTest
  public static void Test5()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI USA Inc';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'UK';
    insert event;
   
    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    opp.Apply_SWISS_Vat__c = true;
    insert opp;

    
     List<Application_Config__c> appconfigObjects1 = new List<Application_Config__c>();
     Application_Config__c appconfig1 = new Application_Config__c();
     appconfig1.Name                  = 'orgwideemailaddressid';
     appconfig1.Value__c              = '0D27A0000004Cu9';
     appconfigObjects1.add(appconfig1);
     insert appconfigObjects1;
     
     List<Application_Config__c> appconfigObjects2 = new List<Application_Config__c>();
     Application_Config__c appconfig2 = new Application_Config__c();
     appconfig2.Name                  = 'GRIUSAIncSwiss';
     appconfig2.Value__c              = '00X7A000000Do61';
     appconfigObjects2.add(appconfig2);
     insert appconfigObjects2;
     
     List<Application_Config__c> appconfigObjects3 = new List<Application_Config__c>();
     Application_Config__c appconfig3 = new Application_Config__c();
     appconfig3.Name                  = 'GRIUSAInc';
     appconfig3.Value__c              = '00X7A000000MHqaUAG';
     appconfigObjects3.add(appconfig3);
     insert appconfigObjects3;
     
     List<Application_Config__c> appconfigObjects4 = new List<Application_Config__c>();
     Application_Config__c appconfig4 = new Application_Config__c();
     appconfig4.Name                  = 'GRIServicesLtdSwiss';
     appconfig4.Value__c              = '00X7A000000Do5w';
     appconfigObjects4.add(appconfig4);
     insert appconfigObjects4;
     
     List<Application_Config__c> appconfigObjects5 = new List<Application_Config__c>();
     Application_Config__c appconfig5 = new Application_Config__c();
     appconfig5.Name                  = 'GRIServicesLtd';
     appconfig5.Value__c              = '00X7A000000MHqVUAW';
     appconfigObjects5.add(appconfig5);
     insert appconfigObjects5;
     
     List<Application_Config__c> appconfigObjects6 = new List<Application_Config__c>();
     Application_Config__c appconfig6 = new Application_Config__c();
     appconfig6.Name                  = 'GRIAmericanEuropeanLLCSwiss';
     appconfig6.Value__c              = '00X7A000000Do5r';
     appconfigObjects6.add(appconfig6);
     insert appconfigObjects6;
     
     List<Application_Config__c> appconfigObjects7 = new List<Application_Config__c>();
     Application_Config__c appconfig7 = new Application_Config__c();
     appconfig7.Name                  = 'GRIAmericanEuropeanLLC';
     appconfig7.Value__c              = '00X7A000000MHqQUAW';
     appconfigObjects7.add(appconfig7);
     insert appconfigObjects7;
    
    Invoice__c inv = new Invoice__c();
    inv.Account__c = acc.id;
    inv.Opportunity__c = opp.id;
    inv.CurrencyIsoCode = 'USD';
    inv.Order_Description__c ='<table border="0" cellspacing="0" cellpadding="0" style="text-align:left; padding:10px"><tr><th style="border-bottom:1px solid black;text-align:left;"><b>Quantity</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Details</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Amount</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th></tr><tr><td>1.00&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>Co-chair&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>£1875.00&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td></tr></table>';
    insert inv;
    
    }

     @isTest
  public static void Test6()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI USA Inc';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'UK';
    insert event;
   
    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    opp.Apply_SWISS_Vat__c = false;
    insert opp;

    
     List<Application_Config__c> appconfigObjects1 = new List<Application_Config__c>();
     Application_Config__c appconfig1 = new Application_Config__c();
     appconfig1.Name                  = 'orgwideemailaddressid';
     appconfig1.Value__c              = '0D27A0000004Cu9';
     appconfigObjects1.add(appconfig1);
     insert appconfigObjects1;
     
     List<Application_Config__c> appconfigObjects2 = new List<Application_Config__c>();
     Application_Config__c appconfig2 = new Application_Config__c();
     appconfig2.Name                  = 'GRIUSAIncSwiss';
     appconfig2.Value__c              = '00X7A000000Do61';
     appconfigObjects2.add(appconfig2);
     insert appconfigObjects2;
     
     List<Application_Config__c> appconfigObjects3 = new List<Application_Config__c>();
     Application_Config__c appconfig3 = new Application_Config__c();
     appconfig3.Name                  = 'GRIUSAInc';
     appconfig3.Value__c              = '00X7A000000MHqaUAG';
     appconfigObjects3.add(appconfig3);
     insert appconfigObjects3;
     
     List<Application_Config__c> appconfigObjects4 = new List<Application_Config__c>();
     Application_Config__c appconfig4 = new Application_Config__c();
     appconfig4.Name                  = 'GRIServicesLtdSwiss';
     appconfig4.Value__c              = '00X7A000000Do5w';
     appconfigObjects4.add(appconfig4);
     insert appconfigObjects4;
     
     List<Application_Config__c> appconfigObjects5 = new List<Application_Config__c>();
     Application_Config__c appconfig5 = new Application_Config__c();
     appconfig5.Name                  = 'GRIServicesLtd';
     appconfig5.Value__c              = '00X7A000000MHqVUAW';
     appconfigObjects5.add(appconfig5);
     insert appconfigObjects5;
     
     List<Application_Config__c> appconfigObjects6 = new List<Application_Config__c>();
     Application_Config__c appconfig6 = new Application_Config__c();
     appconfig6.Name                  = 'GRIAmericanEuropeanLLCSwiss';
     appconfig6.Value__c              = '00X7A000000Do5r';
     appconfigObjects6.add(appconfig6);
     insert appconfigObjects6;
     
     List<Application_Config__c> appconfigObjects7 = new List<Application_Config__c>();
     Application_Config__c appconfig7 = new Application_Config__c();
     appconfig7.Name                  = 'GRIAmericanEuropeanLLC';
     appconfig7.Value__c              = '00X7A000000MHqQUAW';
     appconfigObjects7.add(appconfig7);
     insert appconfigObjects7;
    
    Invoice__c inv = new Invoice__c();
    inv.Account__c = acc.id;
    inv.Opportunity__c = opp.id;
    inv.CurrencyIsoCode = 'USD';
    inv.Order_Description__c ='<table border="0" cellspacing="0" cellpadding="0" style="text-align:left; padding:10px"><tr><th style="border-bottom:1px solid black;text-align:left;"><b>Quantity</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Details</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th><th style="border-bottom:1px solid black;text-align:left;"><b>Amount</b>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</th></tr><tr><td>1.00&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>Co-chair&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td><td>£1875.00&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td></tr></table>';
    insert inv;
    
    }
   
  }