/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * A collection of methods and constants for the Membership SObject
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * @testClasses HelperSObject_Test  
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */

public class HelperMembership {
    public static final String  STATUS_ACTIVATED = 'Activated';
    public static final String  STATUS_EXPIRED = 'Expired'; // grace period
    public static final String  STATUS_DRAFT= 'Draft';
    public static final String  IN_APPROVAL_PROCESS  = 'In Approval Process';   
    public static final String  EXPIRED = 'Expired';
    public static final String  EXPIRED_RENEWED = 'Expired - Renewed';  
    public static final String  PENDING = 'Pending';
    public static final String  SUSPENDED = 'Suspended';
    public static final String  CANCELLED = 'Cancelled';
    public static final String  TEMP_STATUS = 'Temp Status';
    public static final String  TRANSFERRED = 'Transferred';    
    public static final String  UPGRADED = 'Upgraded';
    //Membership level
    public static final String LV_PLATINUM_MEMBER='Platinum Member';
    public static final String LV_DIAMOND_MEMBER='Diamond Member';
    public static final String LV_ADVISOR_MEMBER_DIAMOND='Advisor Member Diamond';
    public static final String LV_STATE_HIGH_PLATINUM_MEMBERSHIP='State High Platinum Membership';
    public static final String LV_DIAMOND_LATAM_MEMBER='Diamond Latam Member';
    public static final String LV_DIAMOND_STATE_MEMBER='Diamond State Member';
    public static final String LV_DIAMOND_STATE_RETREAT_MEMBER='Diamond State Retreat Member';
    public static final String LV_PLATINUM_LATAM_MEMBER='Platinum Latam Member';
    public static final String LV_PLATINUM_MEMBER_INFRA_AND_RE='Platinum Member INFRA and RE';
    public static final String LV_PLATINUM_STATE_MEMBER='Platinum State Member';
    public static final String LV_PLATINUM_STATE_RETREAT_MEMBER='Platinum State Retreat Member';
    public static final String LV_STATE_DIAMOND_LATAM_MEMBER='State Diamond Latam Member';
    public static final String LV_STATE_PLATINUM_COMPANY_MEMBER='State Platinum Company member';
    public static final String LV_STATE_LATAM_MEMBER='State Latam Member';
    public static final String LV_STATE_DIAMOND_COMPANY_MEMBER='State Diamond Company member';
    public static final String LV_ADVISOR_DIAMOND_LATAM_MEMBER='Advisor Diamond Latam Member';
    public static final String LV_DIAMOND_LATAM_COMPANY_MEMBER='Diamond Latam Company Member';
    public static final String LV_PLATINUM_LATAM_COMPANY_MEMBER='Platinum Latam Company Member';
    public static final String LV_PLATINUM_COMPANY_MEMBER='Platinum Company Member';
    public static final String LV_STATE_MEMBER_ADVISORY_BOARD_INFRA_AND_RE='State Member Advisory Board INFRA and RE';
    public static final String LV_DIAMOND_MEMBER_INFRA_AND_RE='Diamond Member Infra and RE';
    public static final String LV_STATE_LATAM_COMPANY_MEMBER='State Latam Company member';

    public static Integer AllowedOpenEventsWorldwide(Contract membership)
    {
        try
        {
            return Integer.valueOf(membership.Allowed_Open_Events_Worldwide__c);
        }
        catch (System.TypeException e)
        {
            if(membership.Allowed_Open_Events_Worldwide__c == 'All')
            	return 365;
            else
                return 0;
		}
    }
    
	public static Integer AllowedExclusiveClubMeetings(Contract membership)
    {
        try
        {
            return Integer.valueOf(membership.Allowed_Exclusive_Events__c);
        }
        catch (System.TypeException e)
        {
            if(membership.Allowed_Exclusive_Events__c == 'All')
            	return 365;
            else
                return 0;
		}
    }

    public static Boolean isActive(Contract membership)
    {
        if( (membership.Status==STATUS_EXPIRED) || 
            (membership.Status==STATUS_ACTIVATED))
            return True;
        return False;
    }
     
    // if club name is an empty string, it does not check the club
    public static Map<Id, Boolean> contactHasMembership(List<Contact> plContacts, 
        List<Contract> plMemberships, String pStrClubName)
    {
        Map<Id, Boolean> mContactMember = new Map<Id, Boolean>();

        // TODO 
        //throw new HelperMembershipException('TODO contactHasMembership()');

        return mContactMember;
    }

    public class HelperMembershipException extends Exception {} 
}