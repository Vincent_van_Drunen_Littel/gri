/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe que altera valores de Event Attendee e installments conforme são 
* feitas alterações na oportunidade.
*
* NAME: EventAttendeeAlteraSessionAttendee.cls
* AUTHOR: EAD                                                  DATE: 19/01/2018
*******************************************************************************/

public class EventAttendeeAlteraSessionAttendee {
	
    public static void execute(){
        TriggerUtils.assertTrigger();
        map<String, Event_Attendee__c> mapEvAtt = new map<String, Event_Attendee__c>();
        set<String> lstOppId = new set<string>();
        
        for(Event_Attendee__c evAtt : (list<Event_Attendee__c>)Trigger.new){
            if(TriggerUtils.wasChangedTo(evAtt, Event_Attendee__c.Event_Attendees_Status__c, 'Cancelled') || 
               TriggerUtils.wasChangedTo(evAtt, Event_Attendee__c.Event_Attendees_Status__c, 'Replaced') || 
               TriggerUtils.wasChangedTo(evAtt, Event_Attendee__c.Event_Attendees_Status__c, 'Not Attending'))
            {
            	mapEvAtt.put(evAtt.id, evAtt);
                lstOppId.add(evAtt.Opportunity__c);
            }
        }
        if(mapEvAtt.isEmpty() || lstOppId.isEmpty()) return;
        
        //Recuperando Session attendee
        list<Attendee__c> lstSesAtt = [SELECT id, Opportunity__c FROM Attendee__c WHERE Opportunity__c =: lstOppId];
        
        for(Event_Attendee__c ev : mapEvAtt.values()){
            for(Attendee__c Satt : lstSesAtt){
                if(ev.Opportunity__c != Satt.Opportunity__c) continue;
            	if(ev.Event_Attendees_Status__c == 'Cancelled' || ev.Event_Attendees_Status__c == 'Replaced' || 
                   ev.Event_Attendees_Status__c == 'Not Attending')
                {
                	Satt.Session_Status__c = 'Declined';
          		} 
            }            
        }
        
        if(!lstSesAtt.isEmpty()) Database.update(lstSesAtt);
        
    }
}