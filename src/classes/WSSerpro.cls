/**
 * @author Renato Matheus Simião
 * @date 2018-06-12
 * @description Integration with Serpro API (REST) with (Receita Federal - Brazil)
 *              properties ending with 'F' is not part of the model, they are formatting for DML statements
 */
public with sharing class WSSerpro {

	private static Setting__mdt SETTINGS = [SELECT SetMockTokenInHeader__c, UseMockResponse__c
											  FROM Setting__mdt
											 WHERE DeveloperName = 'Default'];

	private static String CURRENT_TOKEN = null;

	private static Map<String,String> mSituacaoCadastral = new Map<String,String>{
		'2' => 'Ativa',
		'3' => 'Suspensa',
		'4' => 'Inapta',
		'5' => 'Nula',
		'8' => 'Baixada'
	};
	
	private static Map<String,String> mQualificacao = new Map<String,String>{
		'1'		=> 'Acionista',
		'2'		=> 'Acionista Controlador',
		'3'		=> 'Acionista Diretor',
		'4'		=> 'Acionista Presidente',
		'5'		=> 'Administrador',
		'6'		=> 'Administradora de consórcio de Empresas ou Grupo de Empresas',
		'7'		=> 'Comissário',
		'8'		=> 'Conselheiro de Administração',
		'9'		=> 'Curador',
		'10'	=> 'Diretor',
		'11'	=> 'Interventor',
		'12'	=> 'Inventariante',
		'13'	=> 'Liquidante',
		'14'	=> 'Mãe',
		'15'	=> 'Pai',
		'16'	=> 'Presidente',
		'17'	=> 'Procurador',
		'18'	=> 'Secretário',
		'19'	=> 'Síndico (Condomínio)',
		'20'	=> 'Sociedade Consorciada',
		'21'	=> 'Sociedade Filiada',
		'22'	=> 'Sócio',
		'23'	=> 'Sócio Capitalista',
		'24'	=> 'Sócio Comanditado',
		'25'	=> 'Sócio Comanditário',
		'26'	=> 'Sócio de Indústria',
		'27'	=> 'Sócio Residente ou Domiciliado no Exterior',
		'28'	=> 'Sócio-Gerente',
		'29'	=> 'Sócio ou Acionista Incapaz ou Relativamente Incapaz (exceto menor)',
		'30'	=> 'Sócio ou Acionista Menor (Assistido/Representado)',
		'31'	=> 'Sócio Ostensivo',
		'32'	=> 'Tabelião',
		'33'	=> 'Tesoureiro',
		'34'	=> 'Titular de Empresa Individual Imobiliária',
		'35'	=> 'Tutor',
		'36'	=> 'Gerente-Delegado',
		'37'	=> 'Sócio Pessoa Jurídica Domiciliado no Exterior',
		'38'	=> 'Sócio Pessoa Física Residente ou Domiciliado no Exterior',
		'39'	=> 'Diplomata',
		'40'	=> 'Cônsul',
		'41'	=> 'Representante de Organização Internacional',
		'42'	=> 'Oficial de Registro',
		'43'	=> 'Responsável',
		'44'	=> 'Sócio Participante',
		'45'	=> 'Sócio Investidor',
		'46'	=> 'Ministro de Estado das Relações Exteriores',
		'47'	=> 'Sócio Pessoa Física Residente no Brasil',
		'48'	=> 'Sócio Pessoa Jurídica Domiciliado no Brasil',
		'49'	=> 'Sócio-Administrador',
		'50'	=> 'Empresário',
		'51'	=> 'Candidato a Cargo Politico Eletivo',
		'52'	=> 'Sócio com Capital',
		'53'	=> 'Sócio sem Capital',
		'54'	=> 'Fundador',
		'55'	=> 'Sócio Comanditado Residente no Exterior',
		'56'	=> 'Sócio Comanditário Pessoa Física Residente no Exterior',
		'57'	=> 'Sócio Comanditário Pessoa Jurídica Domiciliado no Exterior',
		'58'	=> 'Sócio Comanditário Incapaz',
		'59'	=> 'Produtor Rural',
		'60'	=> 'Cônsul Honorário',
		'61'	=> 'Responsável Indigena',
		'62'	=> 'Representante das Instituições Extraterritoriais',
		'63'	=> 'Cotas em Tesouraria',
		'64'	=> 'Administrador Judicial',
		'65'	=> 'Titular Pessoa Física Residente ou Domiciliado no Brasil',
		'66'	=> 'Titular Pessoa Física Residente ou Domiciliado no Exterior',
		'67'	=> 'Titular Pessoa Física Incapaz ou Relativamente Incapaz (exceto menor)',
		'68'	=> 'Titular Pessoa Física Menor (Assistido/Representado)',
		'69'	=> 'Beneficiário Final',
		'70'	=> 'Administrador Residente ou Domiciliado no Exterior',
		'71'	=> 'Conselheiro de Administração Residente ou Domiciliado no Exterior',
		'72'	=> 'Diretor Residente ou Domiciliado no Exterior',
		'73'	=> 'Presidente Residente ou Domiciliado no Exterior',
		'74'	=> 'Sócio-Administrador Residente ou Domiciliado no Exterior',
		'75'	=> 'Fundador Residente ou Domiciliado no Exterior'
	};

	public class Token {
		public String scope;
		public String token_type;
		public Integer expires_in;
		public String access_token;
	}
	
	public class ConsultaCPF {
		public String ni;
		public String nome;
		public String nascimento;
		public Date nascimentoF {
			get {
				if (String.isEmpty(nascimento)) return null;

				return Date.newInstance(
					Integer.valueOf(nascimento.right(4)),
					Integer.valueOf(nascimento.mid(2,2)),
					Integer.valueOf(nascimento.left(2))
				);
			}
		}
		public Situacao situacao;
		public String obito;
	}

	public class Situacao {
		public String codigo;
		public String descricao;
	}

	public class ConsultaCNPJ {

		public String ni;
		public String data_abertura;
		public Date data_aberturaF {
			get {
				
				if (String.isEmpty(data_abertura)) return null;

				List<String> dtSplitted = data_abertura.left(10).split('-');
				
				return Date.newInstance(
					Integer.valueOf(dtSplitted.get(0)),
					Integer.valueOf(dtSplitted.get(1)),
					Integer.valueOf(dtSplitted.get(2))
				);
			}
		}
		public String nome_empresarial;
		public String nome_fantasia;
		public CNAE cnae_principal;
		public NaturezaJuridica natureza_juridica;
		public Endereco endereco;
		public String situacao_especial;
		public Situacao_cadastral situacao_cadastral;
		public String orgao;
		public String tipo_estabelecimento;
		public String correio_eletronico;
		public Decimal capital_social {
			get {
				if (capital_social == null) return null;

				return capital_social / 100;
			}
		}
		public String porte;
		public List<CNAE> cnae_secundarias;
		public String nome_orgao;
		public String ente_federativo;
		public String data_situacao_especial;
		public List<Socio> socios;
	}

	public class Situacao_cadastral {
		public String codigo;
		public String motivo;
		public String data;
		// custom field for return correct description
		public String descricao {
			get {
				if (String.isEmpty(codigo))
					return '';
				
				return mSituacaoCadastral.get(codigo);
			}
		}
	}

	public class Socio {
		public String nome;
		public String qualificacao;
		public String qualificacao_descricao {
			get {
				if (String.isEmpty(qualificacao))
					return '';
				
				return mQualificacao.get(qualificacao);
			}
		}
		public String captalSocial;
		public Decimal captalSocialF {
			get {
				if (String.isEmpty(captalSocial)) return null;
				return Decimal.valueOf(captalSocial) / 100;
			}
		}
		public String codigoPais;
		public String qualificacaoRepLegal;
		public String nomeRepLegal;
	}

	public class NaturezaJuridica {
		public String codigo;
		public String descricao;
	}

	public class CNAE {
		public String codigo {
			get {
				if (String.isEmpty(codigo))
					return '';

				return codigo.left(4) + '-' + codigo.mid(4,1) + '/' + codigo.right(2);
			}
		}
		public String descricao;
	}

	public class Endereco {
		public String bairro;
		public String complemento;
		public String cep;
		public String municipio;
		public String uf;
		public String logradouro;
		public String numero;
	}

	public class Telefones {
		public String ddd;
		public String numero;
	}

	public class WrapperConsulta {

		public String status;
		public String message;

		public ConsultaCNPJ consultaCNPJ;
		public ConsultaCPF consultaCPF;

		public WrapperConsulta(ConsultaCNPJ pConsultaCNPJ) {

			this(pConsultaCNPJ, null, 'success', '');
		}

		public WrapperConsulta(ConsultaCPF pConsultaCPF) {

			this(null, pConsultaCPF, 'success', '');
		}

		public WrapperConsulta(ConsultaCNPJ pConsultaCNPJ, ConsultaCPF pConsultaCPF, String pStatus, String pMessage) {

			this.consultaCNPJ = pConsultaCNPJ;
			this.consultaCPF = pConsultaCPF;
			this.status = pStatus;
			this.message = pMessage;
		}
	}

	/**
	* This function generate a Token to use in calls
	* @return       the object Token
	*/
	public static Token gerarToken() {
		
		Token ret = null;
		HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        req.setEndpoint('https://apigateway.serpro.gov.br/token');
        req.setTimeout(100000);
        req.setMethod('POST');
        req.setHeader(
        	'Authorization',
        	'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(Label.SerproConsumerKey+':'+Label.SerproConsumerSecret))
        );
	    //req.setHeader('Content-Type', 'aplication/json;charset=UTF-8');
	    req.setBody('grant_type=client_credentials');

        if (!Test.isRunningTest() && (SETTINGS.UseMockResponse__c || SETTINGS.SetMockTokenInHeader__c)) {
        	res.setBody(
        		StaticResourceHelper.getFileContent('CalloutMockSerproToken')
        	);
        } else res = http.send(req);
        
        ret = (Token) JSON.deserialize(res.getBody(), Token.class);

        CURRENT_TOKEN = ret.access_token;

        return ret;
	}
	
	/**
	* This function consult the CNPJ number in the Receita Federal
	* @param  pCNPJ CNPJ number without the mask, ex: 11965362000133
	* @return       the object ConsultaCNPJ with all informations about this service
	*/
	public static WrapperConsulta consultarCNPJ(String pCNPJ) {

		WrapperConsulta wConsulta;

		try {

			if (String.isEmpty(pCNPJ)) return null;
			if (String.isEmpty(CURRENT_TOKEN)) gerarToken();

			// remove mask . / -
			pCNPJ = pCNPJ.replaceAll('\\.|/|-', '');
			
			HttpRequest req = new HttpRequest();
	        HttpResponse res = new HttpResponse();
	        Http http = new Http();

	        req.setEndpoint('callout:SerproCNPJ/cnpj/'+pCNPJ);
	        req.setTimeout(100000);
	        req.setMethod('GET');
	        req.setHeader('Content-Type', 'aplication/json;charset=UTF-8');
	        req.setHeader('Authorization', 'Bearer ' + CURRENT_TOKEN);
	        
	        if (!Test.isRunningTest() && SETTINGS.UseMockResponse__c) {
	        	res.setBody(
	        		StaticResourceHelper.getFileContent('CalloutMockSerproCNPJ')
	        	);
	        } else res = http.send(req);

	        System.debug('serpro-cnpj='+JSON.serialize(res.getBody()));

	        if (res.getStatusCode() == 404) {
	       
	        	wConsulta = new WrapperConsulta(null, null, 'error', res.getBody());
	       	
	        } else {

	        	wConsulta = new WrapperConsulta(
	        		(ConsultaCNPJ) JSON.deserialize(res.getBody(), ConsultaCNPJ.class)
	        	);
			}
		} catch (Exception e) {

			wConsulta = new WrapperConsulta(null, null, 'error', e.getMessage());
		}
        
        return wConsulta;
	}

	/**
	* This function consult the CPF number in the Receita Federal
	* @param  pCPF CPF number without the mask, ex: 24237268721
	* @return       the object ConsultaCPF with all informations about this service
	*/
	public static WrapperConsulta consultarCPF(String pCPF) {

		WrapperConsulta wConsulta;

		try {

			if (String.isEmpty(pCPF)) return null;
			if (String.isEmpty(CURRENT_TOKEN)) gerarToken();

			// remove mask . -
			pCPF = pCPF.replaceAll('\\.|-', '');
			
			HttpRequest req = new HttpRequest();
	        HttpResponse res = new HttpResponse();
	        Http http = new Http();

	        req.setEndpoint('callout:SerproCPF/cpf/'+pCPF);
	        //req.setEndpoint('https://apigateway.serpro.gov.br/consulta-cpf-trial/1/cpf/'+pCPF);
	        req.setTimeout(100000);
	        req.setMethod('GET');
	        req.setHeader('Content-Type', 'aplication/json;charset=UTF-8');
	        req.setHeader('Authorization', 'Bearer ' +CURRENT_TOKEN);
	        //req.setBody('{"cpf":"'+pCPF+'"}');
	        
	        if (!Test.isRunningTest() && SETTINGS.UseMockResponse__c) {
	        	res.setBody(
	        		StaticResourceHelper.getFileContent('CalloutMockSerproCPF')
	        	);
	        } else res = http.send(req);

	        System.debug('serpro-cpf='+JSON.serialize(res.getBody()));

	        if (res.getStatusCode() == 404) {

	        	wConsulta = new WrapperConsulta(null, null, 'error', res.getBody());
	        
	        } else {

	        	wConsulta = new WrapperConsulta(
	        		(ConsultaCPF) JSON.deserialize(res.getBody(), ConsultaCPF.class)
	        	);
	        }
    	} catch (Exception e) {

    		wConsulta = new WrapperConsulta(null, null, 'error', e.getMessage());
    	}

        return wConsulta;
	}
}