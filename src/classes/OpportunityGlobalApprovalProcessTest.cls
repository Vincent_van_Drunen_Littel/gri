/*******************************************************************************
*                               Cloud2b - 2017
*-------------------------------------------------------------------------------
*
* Classe responsável por testar a classe que envia a Opportunity para o 
* processo de aprovação quando é do tipo Open_events sendo direcionado por
* Approver_Division__c
*
* NAME: OpportunityGlobalApprovalProcessTest.cls
* AUTHOR: CFA                                                  DATE: 20/03/2017
*******************************************************************************/
@isTest
private class OpportunityGlobalApprovalProcessTest { 
  
  private static final Id RT_OPP_OPENEVENTS = RecordTypeMemory.getRecType('Opportunity','Open_Events');
  private static final Id RT_OPP_MEMBERSHIP = RecordTypeMemory.getRecType('Opportunity', 'Membership');
  private static final Id RT_OPP_UNIQUE = RecordTypeMemory.getRecType('Opportunity', 'SPEX');
  private static final Id RT_OPP_MAGAZINEAD = RecordTypeMemory.getRecType('Opportunity', 'Magazine');
  private static final Integer LOTE = 20;
  private static Product2 lProduct2;
  private static Account lAccount;
  private static Contact lContact;
  private static PricebookEntry lPricebookEntry;
  
  static{
      lProduct2 = SObjectInstance.createProduct2();
      database.insert(lProduct2);
       
      lAccount = SObjectInstance.createAccount2();
      database.insert(lAccount);
      
      //=== Custom Setting ===  
      Application_Config__c appCon = new Application_Config__c(Name = 'BucketUserId', Value__c = lAccount.id);
      database.insert(appCon); 
      
      lContact = SObjectInstance.createContato(lAccount.Id);
      database.insert(lContact);  
      
      // Criação do PricebookEntry
      id pbkId = SObjectInstance.catalogoDePrecoPadrao2();
      lPricebookEntry = SObjectInstance.entradaDePreco(pbkId,lProduct2.Id);
      database.insert(lPricebookEntry);      
    
  }
  
   static testMethod void testeFuncionalCurrency() {
      
      list<OpportunityLineItem> lListOpportunityLineItemCC = new list<OpportunityLineItem>();
     
      Event__c lEventcCC = SObjectInstance.createEvent();
      database.insert(lEventcCC);
     
      // Criação da Opportunity
      Opportunity lOpportunityCC = SObjectInstance.createOpportunidade2(lAccount.Id, RT_OPP_OPENEVENTS, lContact.Id, lEventcCC.Id);
      database.insert(lOpportunityCC);
      
      lOpportunityCC.Payment_Current_Currency__c = true;
      
      test.startTest();
        database.update(lOpportunityCC);
      test.stopTest();
     
      Opportunity lOpportunityAfterCC = [select Approval_Process__c ,Payment_Current_Currency__c, Id from Opportunity where Id =: lOpportunityCC.Id];
            
      system.assert(lOpportunityAfterCC.Payment_Current_Currency__c);
      system.assertNotEquals('1 - Approved', lOpportunityAfterCC.Approval_Process__c, 'Approval_Process__c igual a: 1 - Approved');
      system.assertNotEquals('3 - Rejected', lOpportunityAfterCC.Approval_Process__c, 'Approval_Process__c igual a: 3 - Rejected');
   }
   
   /*static testMethod void testeFuncionalDisc() {
      
      list<OpportunityLineItem> lListOpportunityLineItemDS = new list<OpportunityLineItem>();
     
      Event__c lEventcDS = SObjectInstance.createEvent();
      database.insert(lEventcDS);
     
      // Criação da Opportunity
      Opportunity lOpportunityDS = SObjectInstance.createOpportunidadeMagazine(lAccount.Id, RT_OPP_MAGAZINEAD, lContact.Id);
      database.insert(lOpportunityDS); 
       
      // Criação do OpportunityLineItem
      OpportunityLineItem lOpportunityLineItemDS = SObjectInstance.createProdutoOportunidade2(lOpportunityDS.Id, lPricebookEntry.Id);
      lOpportunityLineItemDS.Discount = 10.0;
      lListOpportunityLineItemDS.add(lOpportunityLineItemDS);
      
      test.startTest();
        database.insert(lListOpportunityLineItemDS);
      test.stopTest();
     
      Opportunity lOpportunityAfterDS = [select Discount_of_opportunity__c, Approval_Process__c, Id from Opportunity where Id =: lOpportunityDS.Id];
            
      system.assert(lOpportunityAfterDS.Discount_of_opportunity__c > 0);
      system.assertNotEquals('1 - Approved', lOpportunityAfterDS.Approval_Process__c, 'Approval_Process__c igual a: 1 - Approved');
      system.assertNotEquals('3 - Rejected', lOpportunityAfterDS.Approval_Process__c, 'Approval_Process__c igual a: 3 - Rejected');
   }*/
  
   static testMethod void testeFuncionalEvent() {
      
      list<OpportunityLineItem> lListOpportunityLineItem = new list<OpportunityLineItem>();
     
      Event__c lEventc = SObjectInstance.createEvent();
      database.insert(lEventc);
     
      // Criação da Opportunity
      Opportunity lOpportunity = SObjectInstance.createOpportunidade2(lAccount.Id, RT_OPP_OPENEVENTS, lContact.Id, lEventc.Id);
      lOpportunity.Approval_Process__c = '1 - Approved';
      database.insert(lOpportunity);
      lOpportunity.Approval_Process__c = '3 - Rejected';
//      Opportunity oppToRej = SObjectInstance.createOpportunidade2(lAccount.Id, RT_OPP_OPENEVENTS, lContact.Id, lEventc.Id);
//      oppTORej.Approval_Process__c = '1 - Approved';
//      database.insert(oppToRej);
//      oppToRej.Approval_Process__c = '3 - Rejected';
       
      // Criação do OpportunityLineItem
      OpportunityLineItem lOpportunityLineItem = SObjectInstance.createProdutoOportunidade2(lOpportunity.Id, lPricebookEntry.Id);
      lOpportunityLineItem.Discount = 10.0;
      lListOpportunityLineItem.add(lOpportunityLineItem);
       
//      OpportunityLineItem lOpportunityLineItemR = SObjectInstance.createProdutoOportunidade2(oppToRej.Id, lPricebookEntry.Id);
//      lOpportunityLineItem.UnitPrice = 150.0; 
//      lListOpportunityLineItem.add(lOpportunityLineItemR); 
      
      test.startTest();
        database.insert(lListOpportunityLineItem);
      test.stopTest();
     
      Opportunity lOpportunityAfter = [select Discount_of_opportunity__c, Id, Approval_Process__c 
                                       from Opportunity where Id =: lOpportunity.Id];
      
      system.assert(lOpportunityAfter.Discount_of_opportunity__c > 0);
      system.assertEquals('2 - Pending', lOpportunityAfter.Approval_Process__c, 'Approval_Process__c igual a: 1 - Approved');
      system.assertNotEquals('3 - Rejected', lOpportunityAfter.Approval_Process__c, 'Approval_Process__c igual a: 3 - Rejected');
    
	  database.update(lOpportunity);
      OpportunityLineItem oli = [SELECT ListPrice, UnitPrice FROM OpportunityLineItem WHERE id =: lOpportunityLineItem.id LIMIT 1]; 
      system.assertEquals(oli.ListPrice, oli.UnitPrice);
    }
    
    static testMethod void testeFuncionalMember() {
      
      list<OpportunityLineItem> lListOpportunityLineItemM = new list<OpportunityLineItem>();
           
      Club__c lClubcM = SObjectInstance.createClub();
      database.insert(lClubcM);

      // Criação da Opportunity
      Opportunity lOpportunityM = SObjectInstance.createOpportunidadeMenber(lAccount.Id, RT_OPP_MEMBERSHIP, lContact.Id, lClubcM.Id);
      database.insert(lOpportunityM);

      // Criação do OpportunityLineItem
      OpportunityLineItem lOpportunityLineItemM = SObjectInstance.createProdutoOportunidade2(lOpportunityM.Id, lPricebookEntry.Id);
      lOpportunityLineItemM.Discount = 10.0;
      lListOpportunityLineItemM.add(lOpportunityLineItemM);

      test.startTest();
        database.insert(lListOpportunityLineItemM);
      test.stopTest();
     
      Opportunity lOpportunityAfterM = [select Discount_of_opportunity__c, Approval_Process__c, Id from Opportunity where Id =: lOpportunityM.Id];
 
      system.assert(lOpportunityAfterM.Discount_of_opportunity__c > 0);
      system.assertNotEquals('1 - Approved', lOpportunityAfterM.Approval_Process__c, 'Approval_Process__c igual a: 1 - Approved');
      system.assertNotEquals('3 - Rejected', lOpportunityAfterM.Approval_Process__c, 'Approval_Process__c igual a: 3 - Rejected');
    }
    
    /*
    static testMethod void testeFuncionalSPEX() {
      
      list<OpportunityLineItem> lListOpportunityLineItemS = new list<OpportunityLineItem>();
 
 	  // Criação do club
 	  Club__c club = SObjectInstance.createClub();
      database.insert(club);  
        
      // Criação da Opportunity
      Opportunity lOpportunityS = SObjectInstance.createOpportunidadeSPEX(lAccount.Id, RT_OPP_UNIQUE, lContact.Id);
      lOpportunityS.Club__c = club.id;
      lOpportunityS.Spex_Type__c = 'Club Sponsorship';
      database.insert(lOpportunityS);
      
      // Criação do OpportunityLineItem
      OpportunityLineItem lOpportunityLineItemS = SObjectInstance.createProdutoOportunidade2(lOpportunityS.Id, lPricebookEntry.Id);
      lOpportunityLineItemS.Discount = 10.0;
      lListOpportunityLineItemS.add(lOpportunityLineItemS);

      test.startTest();
        database.insert(lListOpportunityLineItemS);
      test.stopTest();
     
      Opportunity lOpportunityAfterS = [select Discount_of_opportunity__c, Approval_Process__c, Id from Opportunity where Id =: lOpportunityS.Id];
      system.assert(lOpportunityAfterS.Discount_of_opportunity__c > 0);
      system.assertNotEquals('1 - Approved', lOpportunityAfterS.Approval_Process__c, 'Approval_Process__c igual a: 1 - Approved');
      system.assertNotEquals('3 - Rejected', lOpportunityAfterS.Approval_Process__c, 'Approval_Process__c igual a: 3 - Rejected');
      
    }
    */

@isTest 
static void test_ApprovalSpex2()
{
    // Insert a test product.
    Product2 prod = new Product2(Name = 'Sponsorship-Prod', 
        Family = 'Sponsorship');
    insert prod;
    
    // Get standard price book ID.
    Id pricebookId = Test.getStandardPricebookId();
    
    // 1. Insert a price book entry for the standard price book.
    PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
        UnitPrice = 10000, IsActive = true);
    insert standardPrice;
    
    // Create a custom price book
    Pricebook2 customPB = new Pricebook2(Name='Sponsorship Brazil RE', isActive=true);
    insert customPB;
    
    // 2. Insert a price book entry with a custom price.
    PricebookEntry customPrice = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = prod.Id,
        UnitPrice = 12000, IsActive = true);
    insert customPrice;

    List<Club__c> listClubs = TestDataFactory.createClubs( 
        new List<String>{TestDataFactory.CLUB_RE_BRAZIL});
    Club__c brazilReClub = listClubs[0];
    List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(20);
    Event__c brazilReEvent= TestDataFactory.createEvent('BrazilReEvent',  brazilReClub);
    List<Opportunity> listOpps = TestDataFactory.createSpexOpps(listContacts, brazilReEvent, brazilReClub, System.today()+10);
    for(Opportunity opp:listOpps)
        opp.Pricebook2Id=customPB.Id;
    update listOpps;

    // Criação do OpportunityLineItem
    List<OpportunityLineItem> listOppLineItem = new List<OpportunityLineItem>();
    for(Opportunity opp:listOpps){
        OpportunityLineItem opportunityLineItem = new OpportunityLineItem();//  SObjectInstance.createProdutoOportunidade2(opp.Id, pbEntry.Id);
        opportunityLineItem.Discount = 10.0;
        opportunityLineItem.PricebookEntryId=customPrice.Id;
        opportunityLineItem.OpportunityId=opp.Id;
        opportunityLineItem.Product2Id=prod.Id;
        listOppLineItem.add(opportunityLineItem);
    }

    test.startTest();
    insert listOppLineItem;
    test.stopTest();

}


    // Utility method that can be called by Apex tests to create price book entries.
    @isTest
    static  void addPricebookEntries() {
        // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        // Next, perform some tests with your test price book entries.
    }

    
    static testMethod void testeLote() {
      
      list<OpportunityLineItem> lListOpportunityLineItemLote = new list<OpportunityLineItem>();
      list<Opportunity> lListOpportunityLote = new list<Opportunity>();
      list<Club__c> lListClubcLote = new list<Club__c>();
          
      Club__c lClubcLote = SObjectInstance.createClub();
      lListClubcLote.add(lClubcLote);
      database.insert(lClubcLote);
      
      // Criação da Opportunity
      for( Integer i = 0; i < LOTE; i++)
      {
        Opportunity lOpportunityLote = SObjectInstance.createOpportunidadeMenber(lAccount.Id, RT_OPP_MEMBERSHIP, lContact.Id, lClubcLote.Id);
        lListOpportunityLote.add(lOpportunityLote);
      }
      database.insert(lListOpportunityLote);
       
      // Criação do OpportunityLineItem
      for(Opportunity opp : lListOpportunityLote)
      {
        OpportunityLineItem lOpportunityLineItemLote = SObjectInstance.createProdutoOportunidade(opp.Id, lPricebookEntry.Id);
        lOpportunityLineItemLote.Discount = 10.0;
        lListOpportunityLineItemLote.add(lOpportunityLineItemLote);
      }
 
      test.startTest();
        database.insert(lListOpportunityLineItemLote);
      test.stopTest();
      
      list<Opportunity> lOpportunityAfterLote = [select Id, Approval_Process__c, Discount_of_opportunity__c from Opportunity where Id =: lListOpportunityLote];
     
      for(Opportunity opps : lOpportunityAfterLote)
      {
        system.debug('values Opportunity Before test LOTE: '+ opps.Approval_Process__c);
        system.assert(opps.Discount_of_opportunity__c > 0);
        system.assertNotEquals('1 - Approved', opps.Approval_Process__c, 'Approval_Process__c igual a: 1 - Approved');
        system.assertNotEquals('3 - Rejected', opps.Approval_Process__c, 'Approval_Process__c igual a: 3 - Rejected');
      }
    }
    
    static testMethod void testeErro() {
      
      list<OpportunityLineItem> lListOpportunityLineItemErro = new list<OpportunityLineItem>();

      Club__c lClubcErro = SObjectInstance.createClub();
      database.insert(lClubcErro);
     
      // Criação da Opportunity
      Opportunity lOpportunityErro = SObjectInstance.createOpportunidadeMenber(lAccount.Id, RT_OPP_MEMBERSHIP, lContact.Id, lClubcErro.Id);
      database.insert(lOpportunityErro);
            
      // Criação do OpportunityLineItem
      OpportunityLineItem lOpportunityLineItemErro = SObjectInstance.createProdutoOportunidade(lOpportunityErro.Id, lPricebookEntry.Id);
      lListOpportunityLineItemErro.add(lOpportunityLineItemErro);
      
      
      test.startTest();
        database.insert(lListOpportunityLineItemErro);
      test.stopTest();
     
      Opportunity lOpportunityAfterError = [select Id, Approval_Process__c, Discount_of_opportunity__c from Opportunity where id =: lOpportunityErro.Id];
     
      system.assertNotEquals('2 - Pending', lOpportunityAfterError.Approval_Process__c, 'Approval_Process__c igual a: 2 - Pending');
      system.assertEquals(null, lOpportunityAfterError.Discount_of_opportunity__c, 'Discount_of_opportunity__c diferente de null');
       
    }
   
}