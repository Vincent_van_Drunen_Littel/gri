/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe para contar o número de Oportunidade do tipo Open Events que um contato
* tem.
*
* NAME: OpportunityCountOppOpenEvent.cls
* AUTHOR: RVR                                                  DATE: 28/04/2016
*******************************************************************************/
public with sharing class OpportunityCountOppOpenEvent
{
	private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
	
	public static void execute() 
	{
		TriggerUtils.assertTrigger();

		Set<Id> setContacts = new Set<Id>();

		list<Opportunity> lstOppIN = (Trigger.isDelete) ? 
			(List<Opportunity>)Trigger.old : (List<Opportunity>)Trigger.new;

		for(Opportunity opp: lstOppIN)
		{
			if(opp.RecordTypeId == REC_OPP_OPEN_EVENTS && opp.StageName == 'Won')
			{
				setContacts.add(opp.Contact__c);
			}
		}
		if(setContacts.isEmpty()) return;

		map<Id, Integer> mapOppCount = new map<Id, Integer>();
		for(AggregateResult iOpp : [Select count(id) quant, Contact__c contactId
			FROM Opportunity
			WHERE Contact__c =: setContacts
			AND RecordTypeId =: REC_OPP_OPEN_EVENTS 
			AND StageName = 'Won' 
			AND Event__r.Type__c = 'Open Event'
			Group by Contact__c])
		{
			mapOppCount.put((Id)iOpp.get('contactId'), (Integer)iOpp.get('quant'));
		}

		List<Contact> lstCttUpdate = new List<Contact>();

		for(Contact ctt: [SELECT Id, Open_Events_Counter__c FROM Contact WHERE Id IN : setContacts])
		{
			Integer lCount = mapOppCount.get(ctt.Id);
			ctt.Open_Events_Counter__c = (lCount == null) ? 0 : lCount;

			lstCttUpdate.add(ctt);
		}

		map<Id, Integer> mapOppClosedCount = new map<Id, Integer>();
		for(AggregateResult iOpp : [Select count(id) quant, Contact__c contactId
			FROM Opportunity
			WHERE Contact__c =: setContacts
			AND RecordTypeId =: REC_OPP_OPEN_EVENTS 
			AND StageName = 'Won' 
			AND Event__r.Type__c = 'Club Event'
			Group by Contact__c])
		{
			mapOppClosedCount.put((Id)iOpp.get('contactId'), (Integer)iOpp.get('quant'));
		}

		List<Contact> lstCttUpdateClosed = new List<Contact>();

		for(Contact ctt: [SELECT Id, Club_Event_Counter__c FROM Contact WHERE Id IN : setContacts])
		{
			Integer lCount = mapOppClosedCount.get(ctt.Id);
			ctt.Club_Event_Counter__c = (lCount == null) ? 0 : lCount;

			lstCttUpdateClosed.add(ctt);
		}
		
		if(!lstCttUpdate.isEmpty()) Database.update( lstCttUpdate );
		if(!lstCttUpdateClosed.isEmpty()) Database.update( lstCttUpdateClosed );		
	}	
}