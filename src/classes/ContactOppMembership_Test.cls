@isTest  
public class ContactOppMembership_Test {
    
    @isTest private static void createEntry()
    {
        Integer numContactsPerAcct = 1;
        List<Contact> listContacts =  TestDataFactory.createContactsWithDefaultAccount(numContactsPerAcct);
        List<Club__c> listClubs = TestDataFactory.createClubs(new List<String> {HelperClub.RE_BRAZIL});
        Club__c clubReBr = listClubs[0];
        List<Opportunity> listOpps = TestDataFactory.createMembershipOpps(listContacts, clubReBr, System.Today());
        Product2 prod = TestDataFactory.createProduct('P-001212');
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(prod);


        List<Contract> memberships = TestDataFactory.createMemberships(listContacts, 
            clubReBr, System.today()+365);

        Test.startTest();
        ContactOppMembership conOppMem = new ContactOppMembership(listContacts[0].Id, listOpps[0], memberships[0], clubReBr.Name);
        Id someId = conOppMem.getContact();
        Opportunity someOpp = conOppMem.getOpportunity();
        String someClubName = conOppMem.getClubName();
        Contract someMembership = conOppMem.getContract();
        System.assertNotEquals(NULL, conOppMem);
        System.assertNotEquals(NULL, someId);
        System.assertNotEquals(NULL, someOpp);
        System.assertNotEquals(NULL, someClubName);
        System.assertNotEquals(NULL, someMembership);
        Test.stopTest();
    }

    @testSetup 
    static void buildData()
    {
        TestDataFactory.init();
    }
}