/**
 * Created by Eric Bueno on 19/01/2019.
 */

public without sharing class CustomException extends Exception {

        public Integer statusCode;

        public CustomException(String message, Integer statusCode){
            this.setMessage(message);
            this.statusCode = statusCode;
        }

}