/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 * Class responsible for the abstraction of external systems with internal
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 02/02/19  EricB  First version.
 *
 */

public without sharing class ClubFacade {

    public static final ClubFacade instance = new ClubFacade();
    public static final ClubBO bo = ClubBO.getInstance();

    private ClubFacade(){}

    public static ClubFacade getInstance(){ return instance;}

    public SiteTO.ClubTO findClubById(Id clubId){

        // Instancias
        SiteTO.ClubTO clubTO = new SiteTO.ClubTO();

        // Busca a oportunidade pelo id do evento
        List<Contract> listContract = bo.findContractByClubId(clubId);

        // Verifica se o contrato relacionado ao club foi encontrada ou atendeu aos critérios
        if(listContract.isEmpty()){
            throw new CustomException('Club not found or does not match search criteria',400);
        }

        Contract contract = listContract[0];

        // Busca os sponsors relacionados ao club
        List<Order> listOrder = bo.findOrderByClubId(clubId);

        // Verifica se o sponsor relacionado ao club foi encontrada ou atendeu aos critérios
        if(!listOrder.isEmpty()){
            clubTO.sponsors = this.parseToSponsorTOFromListOrder(listOrder);
        }

        // Converte o club para ClubTO
        clubTO = this.parseToClubTOFromClub(clubTO, contract.Club__r);

        // Converte a conta para CompanyTO
        clubTO.company = new SiteTO.CompanyTO();
        clubTO.company = this.parseToCompanyTOFromAccount(clubTO.company, contract.Account);

        // Converte o contato para CompanyTO
        clubTO.company = this.parseToCompanyTOFromContact(clubTO.company, contract.Contact__r);

        // Converte o contrato para MembershipTO
        clubTO.membership = new SiteTO.MembershipTO();
        clubTO.membership = this.parseToMembershipTOFromContract(clubTO.membership, contract);

        return  clubTO;
    }

    public SiteTO.ClubTO findClubSponsorByClubId(Id clubId){

        // Instancias
        SiteTO.ClubTO clubTO = new SiteTO.ClubTO();

        // Busca a oportunidade pelo id do evento
        List<Club__c> listClub = bo.findClubById(clubId);

        // Verifica se o contrato relacionado ao club foi encontrada ou atendeu aos critérios
        if(listClub.isEmpty()){
            throw new CustomException('Club not found',400);
        }

        Club__c club = listClub[0];

        // Busca os sponsors relacionados ao club
        List<Order> listOrder = bo.findOrderByClubId(clubId);

        // Verifica se o sponsor relacionado ao club foi encontrada ou atendeu aos critérios
        if(!listOrder.isEmpty()){
            clubTO.sponsors = this.parseToSponsorTOFromListOrder(listOrder);
        }

        // Converte o club para ClubTO
        clubTO = this.parseToClubTOFromClub(clubTO, club);

        return  clubTO;
    }

    public List<SiteTO.SponsorTO> parseToSponsorTOFromListOrder(List<Order> listOrder){

        List<SiteTO.SponsorTO> listSponsorTO = new List<SiteTO.SponsorTO>();

        for(Order order : listOrder){
            SiteTO.SponsorTO sponsorTO = new SiteTO.SponsorTO();
            listSponsorTO.add(this.parseToSponsorTOFromOrder(sponsorTO, order));
        }

        return listSponsorTO;


    }

    public SiteTO.SponsorTO parseToSponsorTOFromOrder(SiteTO.SponsorTO sponsorTO, Order order){

        try{
            sponsorTO.account = order.Account.Name;
            sponsorTO.accountId = order.AccountId;
            sponsorTO.a4CompanyProfile = order.A4_Company_Profile_New__c;
            sponsorTO.bookLogoPosition = order.Brochure_Programme_Book_Logo_Position_Ne__c;
            sponsorTO.companyProfile = order.CompanyAuthorizedBy.Name;
            sponsorTO.businessUnit = order.Business_Unit2__c;
            sponsorTO.eventId = order.Event__c;
            sponsorTO.eventClubId = order.Event_s_Club__c;
            sponsorTO.eventBussinessUnit = order.Event_Business_Unit__c;
            sponsorTO.eventDivision = order.Event_Division__c;
            sponsorTO.club = order.Club__r.Name;
            sponsorTO.clubId = order.Club__c;
            sponsorTO.logoUrl = order.Account.URL_External_Logo__c;
            sponsorTO.sizing = order.Sizing_New__c;
            sponsorTO.sponsorType = order.Sponsor_Type__c;
            sponsorTO.status = order.Status__c;
            sponsorTO.orderStartDate = String.valueOf(order.EffectiveDate);
            sponsorTO.orderEndDate = String.valueOf(order.EndDate);


        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return sponsorTO;

    }

    public SiteTO.MembershipTO parseToMembershipTOFromContract(SiteTO.MembershipTO membershipTO, Contract contract){

        try{
            membershipTO.businessUnit = contract.Membership_Business_Unit__c;
            membershipTO.division = contract.Membership_Division__c;
            membershipTO.specialParticipation = Contract.Special_Participation__c;
            membershipTO.countryClub = Contract.Country_Club__c;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return membershipTO;
    }

    public SiteTO.CompanyTO parseToCompanyTOFromContact(SiteTO.CompanyTO companyTO, Contact contact){
        try{

            companyTO.contactId = contact.Id;
            companyTO.contactNameBadge = contact.Badge_Name__c;
            companyTO.contactJobTitleBadge = contact.Job_title_badge__c;
            companyTO.nameBadge = contact.Company_Name_Badge__c;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return companyTO;
    }

    public SiteTO.CompanyTO parseToCompanyTOFromAccount(SiteTO.CompanyTO companyTO, Account account){

        try{

            companyTO.accountId = account.Id;
            companyTO.name = account.Name;
            companyTO.phone = account.Phone;
            companyTO.profile = account.Company_Profile__c;
            companyTO.summaryProfile = account.Company_Summary_Profile__c;
            companyTO.logoName = account.File_Name_Marketing_Edited_logo__c;
            companyTO.logoUrl = account.Marketing_Edited_Logo__c;
            companyTO.advisorCompanyMemberBlack = account.Advisor_Company_Member_Black__c;
            companyTO.foreignCompanySummaryProfile = account.Foreign_Company_Summary_Profile__c;
            companyTO.foreignInformationLanguage2 = account.Foreign_Information_2_Language__c;
            companyTO.foreignInformationLanguage = account.Foreign_Information_Language__c;
            companyTO.foreignSponsorProfileBrochure = account.Foreign_Sponsor_Profile_for_Brochure__c;
            companyTO.foreignSponsorProfileBrochure2 = account.Foreign_Sponsor_Profile_for_Brochure_2__c;
            companyTO.foreignSponsorProfileProgramBook = account.Foreign_Sponsor_Profile_for_Program_Book__c;
            companyTO.foreignSponsorProfileProgramBook2 = account.Foreign_Sponsor_Profile_for_ProgramBook2__c;
            companyTO.InformalName = account.Informal_Name__c;
            companyTO.SponsorProfileBrochure = account.Sponsor_Profile_for_Brochure__c;
            companyTO.SponsorProfileProgramBook = account.Sponsor_Profile_for_Program_Book__c;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return companyTO;

    }

    public SiteTO.ClubTO parseToClubTOFromClub(SiteTO.ClubTO clubTO, Club__c club){

        try{
            clubTO.id = club.Id;
            clubTO.name = club.Name;
            clubTO.description = club.Club_Description__c;
            clubTO.ownerId = club.OwnerId;
            clubTO.ownerName = club.Owner.Name;

        }catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '. In line: ' + e.getLineNumber());
            throw new CustomException(e.getMessage());
        }

        return clubTO;

    }

}