/*******************************************************************************
*                               Cloud2b - 2017
*-------------------------------------------------------------------------------
*
* Classe responsável por Criar um novo Membership quando opportunity é aprovada
* e e leva o RecordType = Membership
*
* NAME: OpportunityNewMember.cls
* AUTHOR: CFA                                                  DATE: 17/03/2017
*******************************************************************************/
public with sharing class OpportunityNewMember {
    
    public static final Id RT_CTC_INFRACLUBINDIA = RecordTypeMemory.getRecType('Contract', 'Infra_Club_India');
    public static final Id RT_CTC_INFRACLUB = RecordTypeMemory.getRecType('Contract', 'Infra_Club');
    public static final Id RT_CTC_RECLUBAFRICA = RecordTypeMemory.getRecType('Contract', 'RE_Club_Africa');
    public static final Id RT_CTC_REALESTATECLUB = RecordTypeMemory.getRecType('Contract', 'Real_Estate_Club');
    public static final Id RT_CTC_EUROPEANCLUB = RecordTypeMemory.getRecType('Contract', 'European_Club');
    public static final Id RT_CTC_INDIACLUB = RecordTypeMemory.getRecType('Contract', 'India_Club');
    public static final Id RT_CTC_RECLUBLATAM = RecordTypeMemory.getRecType('Contract', 'RE_Club_LatAm');
    public static final Id RT_CTC_GRITECHCLUB = RecordTypeMemory.getRecType('Contract', 'GRITechClub');
    public static final Id RT_CTC_RETAILCLUB = RecordTypeMemory.getRecType('Contract', 'Retail_Club');
    
    public static Boolean Exec = false;
    
    private static final map<String,Id> mapRTFormula = new map<String,Id>
    {
        'Infra Club LatAm' => RT_CTC_INFRACLUB,
            'RE Club Brazil' => RT_CTC_REALESTATECLUB,
            'RE Club LatAm' => RT_CTC_RECLUBLATAM,
            'Retail Club Brazil' => RT_CTC_RETAILCLUB,
            'RE Club Europe' => RT_CTC_EUROPEANCLUB,
            'RE Club India' => RT_CTC_INDIACLUB,
            'Infra Club India' => RT_CTC_INFRACLUBINDIA, 
            'GRI Tech Club' => RT_CTC_GRITECHCLUB,
            'RE Club Africa' => RT_CTC_RECLUBAFRICA
            };
                
                private static final map<Id,String> mapContratName = new map<Id,String>
            {
                RT_CTC_INFRACLUB => 'Infra Club LatAm Membership ',
                    RT_CTC_REALESTATECLUB => 'RE Club Brazil Membership ',
                    RT_CTC_RECLUBLATAM => 'RE Club LatAm Membership ',
                    RT_CTC_RETAILCLUB => 'Retail Club Brazil Membership ',
                    RT_CTC_EUROPEANCLUB => 'RE Club Europe Membership ',
                    RT_CTC_INDIACLUB => 'RE Club India Membership ',
                    RT_CTC_INFRACLUBINDIA => 'Infra Club India Membership ',
                    RT_CTC_GRITECHCLUB => 'GRI Tech Club Membership ',
                    RT_CTC_RECLUBAFRICA =>'RE Club Africa Membership '
                    };
                        
                        public static void execute()
                    {
                        TriggerUtils.assertTrigger();
                        
                        if(Exec) return;
                        
                        set<Opportunity> setOpp = new set<Opportunity>();
                        
                        boolean LValidation = false;
                        
                        for(Opportunity opp : (list<Opportunity>)trigger.new)
                        {
                            
                            LValidation = (opp.RecordTypeId == OrgDefs.RT_OPP_MEMBERSHIP
                                           && TriggerUtils.wasChangedTo(opp, Opportunity.StageName, 'Approved by Finance'));
                            
                            if(LValidation) setOpp.add(opp);
                        }
                        
                        if(setOpp.isEmpty()) return;
                        
                        list<Contract> lListInserContract = new list<Contract>();
                        
                        lListInserContract = createNewMember(setOpp);
                        
                        if(!lListInserContract.isEmpty()) database.insert(lListInserContract);
                        Exec = true;
                    }
    
    public static list<Contract> createNewMember(set<Opportunity> aOppObj)
    {
        list<Contract> lListCTC = new list<Contract>();
        
        list<Opportunity> lOppObj = [SELECT AccountId, 
                                     Account.BillingCity, 
                                     Account.BillingCountry, 
                                     Account.BillingState, 
                                     Account.BillingStreet,
                                     Club__c, 
                                     Club__r.Name,
                                     Contact__c,
                                     Contact__r.FirstName,
                                     Contact__r.LastName
                                     FROM Opportunity 
                                     WHERE Id =: aOppObj];
        
        system.debug('Lista de lOppObj = '+ lOppObj);
        
        for(Opportunity opp: lOppObj)
        {
            Contract lContractObj = new Contract();
            
            lContractObj.AccountId = opp.AccountId;
            lContractObj.BillingCity = opp.Account.BillingCity;
            lContractObj.BillingCountry = opp.Account.BillingCountry;
            lContractObj.BillingState = opp.Account.BillingState;
            lContractObj.BillingStreet = opp.Account.BillingStreet;
            lContractObj.Club__c = opp.Club__c;
            lContractObj.Contact__c = opp.Contact__c;
            lContractObj.ContractTerm = 12 ;
            lContractObj.RecordTypeId = mapRTFormula.get(opp.Club__r.Name);
            lContractObj.Name = mapContratName.get(lContractObj.RecordTypeId)+''+opp.Contact__r.FirstName+' '+opp.Contact__r.LastName;
            lContractObj.Opportunity__c  = opp.Id;
            lContractObj.Contact__c = opp.Contact__c;
            lContractObj.OwnerExpirationNotice  = '60';
            lContractObj.StartDate  = system.today();
            lContractObj.Status  = 'Draft';
            
            system.debug('Objeto lContractObj: '+lContractObj);
            
            lListCTC.add(lContractObj);
        }
        return lListCTC;
    }
}