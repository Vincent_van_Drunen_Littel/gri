global class CreateCalendarInviteBatch implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts
{

    public List<Calendar_Invite__c> calinviteObjLst{get;set;}
    public Map<Id,set<string>> notifiedEmailsMap{get;set;}
    public set<string> setOfEmails{get;set;}
    public string accessToken{get;set;}
    public Opportunity opptyObj{get;set;}
     private static string tokenEndPointValue{get;set;}
    private static string endPointValue{get;set;}
     private static string googleClientID{get;set;}
    private static string googleSecretCode{get;set;}
    private static string redirectURI{get;set;}
    private static string authorizationCode{get;set;}
    private static string batchAttendeeEndpoint;
    public Map<Id,Id> eventToOppMap{get;set;}
    public string automationType{get;set;}
    public set<Id> oppIds{get;set;}
    private static string accessToken;
    private map<Id,string> CalinviteToAttendeeStatusMap=new map<Id,string>();
    public List<Calendar_Invitee__c> insertCalendarInvites = new List<Calendar_Invitee__c>();
    public CreateCalendarInviteBatch(list<Calendar_Invite__c> calInviteLst,Map<Id,set<string>> notifiedEmailsMapVal,set<string> setOfEmailsVal,string  accessTokenVal,Opportunity opptyObjVal, string automationTypeVal,Map<Id,Id> eventToOppMapVal,set<Id> oppIdsVal,map<Id,string> CalinviteToAttendeeStatusMapVal) {
        calinviteObjLst = calInviteLst;
        notifiedEmailsMap = notifiedEmailsMapVal;
        setOfEmails = setOfEmailsVal;
        accessToken = accessTokenVal;
        opptyObj = opptyObjVal;
        automationType = automationTypeVal;
        eventToOppMap = eventToOppMapVal;
        oppIds = oppIdsVal;
        CalinviteToAttendeeStatusMap = CalinviteToAttendeeStatusMapVal;
    }
    global List<SObject> start(Database.BatchableContext context)
    {
        system.debug('@@calinviteObjLstStart'+calinviteObjLst);
        return calinviteObjLst;
    }   
    
    global void execute(Database.BatchableContext BC, List<Calendar_Invite__c> scope)
    { 
        system.debug('@@scope'+scope);
        List<Google_Calendar_API_Detail__mdt> mtdCalendarApiLst =  [ SELECT GoogleClientId__c,GoogleBatchEndpoint__c,GoogleEndPoint__c,GoogleRedirectURI__c,GoogleSecretCode__c,GoogleTokenEndpoint__c, GoogleUserCode__c,GoogleAttendeeEndPoint__c FROM Google_Calendar_API_Detail__mdt ]; 
        system.debug('@@mtdCalendarApiLst'+mtdCalendarApiLst);
        String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
        list<CalendarInvite_attendees> clAttendeesLst = new List<CalendarInvite_attendees>();
        if(mtdCalendarApiLst != null && mtdCalendarApiLst.size()>0){
            googleClientID = mtdCalendarApiLst[0].GoogleClientId__c;
            googleSecretCode = mtdCalendarApiLst[0].GoogleSecretCode__c;
            redirectURI = mtdCalendarApiLst[0].GoogleRedirectURI__c;
            authorizationCode = mtdCalendarApiLst[0].GoogleUserCode__c;
            tokenEndPointValue = mtdCalendarApiLst[0].GoogleTokenEndpoint__c;
            
            endPointValue = mtdCalendarApiLst[0].GoogleAttendeeEndPoint__c;
            batchAttendeeEndpoint = mtdCalendarApiLst[0].GoogleBatchEndpoint__c;
       }
        
            for(Calendar_Invite__c calInviteObj:scope){
                //if(CalinviteToAttendeeStatusMap.containsKey(calInviteObj.id)){
                //if(CalinviteToAttendeeStatusMap.get(calInviteObj.id) != 'Yes'){
                system.debug('@@CalinviteToAttendeeStatusMap'+CalinviteToAttendeeStatusMap);
                
                if(clAttendeesLst.size()>0){
                    clAttendeesLst.clear();
                }
                
                Map<string,String> inviteeEmails = new Map<string,String>();
                for(Calendar_Invitee__c calendarInviteeObj:calInviteObj.Calendar_Invitees__r){
                    system.debug('@@opptyObj.Id'+opptyObj.Id+'@@calendarInviteeObj.Opportunity__c'+calendarInviteeObj.Opportunity__c);
                    if(opptyObj.Id != calendarInviteeObj.Opportunity__c){
                        String status = calendarInviteeObj.Invite_Status__c == 'Yes' ? 'accepted' :calendarInviteeObj.Invite_Status__c == 'No' ? 'declined' : calendarInviteeObj.Invite_Status__c == 'Maybe' ? 'tentative' : 'needsAction'; 
                        inviteeEmails.put(calendarInviteeObj.InviteeEmail__c,status);
                    }else{
                        String status = calendarInviteeObj.Invite_Status__c == 'Yes' ? 'accepted' :calendarInviteeObj.Invite_Status__c == 'No' ? 'needsAction' : calendarInviteeObj.Invite_Status__c == 'Maybe' ? 'needsAction' : 'needsAction'; 
                        inviteeEmails.put(calendarInviteeObj.InviteeEmail__c,status);
                    }
                    
                }
                system.debug('@@inviteeEmails'+inviteeEmails);
                if(setOfEmails.size()>0){
                        for(string strObj:setOfEmails){
                            if(strObj != null)
                                inviteeEmails.put(strObj, 'needsAction');
                        }
                    }
                if(calInviteObj.GoogleCalendarId__c !=''){
                    inviteeEmails.put(calInviteObj.GoogleCalendarId__c,'needsAction');
                }
                system.debug('@@inviteeEmails'+inviteeEmails);
                if(inviteeEmails.size()>0){
                    for(string inviteStr:inviteeEmails.keySet()){
                        if(inviteStr != null && inviteeEmails.containsKey(inviteStr))
                            clAttendeesLst.add(new CalendarInvite_attendees(inviteStr,inviteeEmails.get(inviteStr)));
                    }   
                }
                if(setOfEmails.isEmpty() && inviteeEmails.isEmpty()){
                    clAttendeesLst.add(new CalendarInvite_attendees('','')); 
                }
                system.debug('@@clAttendeesLst'+clAttendeesLst);
                Integer finalHoursToBeAdded = 0;
                if(calInviteObj.Timezone__c != null){
                    List<string> dateLst;
                    List<string> finalHoursLst;
                    Boolean IsNegativeValue = false;
                    if(calInviteObj.Timezone__c.contains('+')){
                        dateLst =  calInviteObj.Timezone__c.split('\\+');
                    
                    }else if(calInviteObj.Timezone__c.contains('-')){
                        IsNegativeValue = true;
                        dateLst = calInviteObj.Timezone__c.split('\\-');
                    }
                    system.debug('@dateLst'+dateLst);
                    if(dateLst != null && dateLst.size()>1 ){
                        finalHoursLst = dateLst[1].split('\\:');
                    }
                    system.debug('@finalHoursLst'+finalHoursLst);
                    if(finalHoursLst != null){
                        if(!IsNegativeValue){
                            finalHoursToBeAdded =  -1*(Integer.valueOf(finalHoursLst[0]));
                        }else{
                            
                            finalHoursToBeAdded =Integer.valueOf(finalHoursLst[0]);
                        }
                    }
                }
            system.debug('@@finalHoursToBeAdded'+finalHoursToBeAdded);
            /*if(calInviteObj.Start_Date__c.month() != Date.Today().Month()){
                if(string.valueOf(finalHoursToBeAdded).contains('-')){
                    finalHoursToBeAdded = finalHoursToBeAdded+1;
                }else{
                    finalHoursToBeAdded = finalHoursToBeAdded-1;
                }
            }*/
            system.debug('@@finalHoursToBeAddedFinal'+finalHoursToBeAdded);
            //finalHoursToBeAdded = finalHoursToBeAdded+2;
            system.debug('@@finalHoursToBeAdded135'+finalHoursToBeAdded);

            //DateTime startDt = datetime.newInstance(calInviteObj.Start_Date__c.year(), calInviteObj.Start_Date__c.month(),calInviteObj.Start_Date__c.day()); 
            string startDateString;
            if(calInviteObj.Start_Date__c != null){
                startDateString = (calInviteObj.Start_Date__c.addHours(finalHoursToBeAdded)).format(dateFormat);
            }else{
                startDateString = '';
            }
            System.debug(startDateString); // 2016-02-29T22:30:48Z
            
            //DateTime endDt = datetime.newInstance(calInviteObj.End_Date__c.year(), calInviteObj.End_Date__c.month(),calInviteObj.End_Date__c.day());
            String endDateString;
            if(calInviteObj.End_Date__c != null){
                endDateString = (calInviteObj.End_Date__c.addHours(finalHoursToBeAdded)).format(dateFormat);
            }else{
                endDateString = '';
            }
            System.debug(endDateString); // 2016-02-29T22:30:48Z
            
            CalendarInvite_start clStart = new CalendarInvite_start(startDateString,string.valueof(UserInfo.getTimeZone()));
            CalendarInvite_End clEnd = new CalendarInvite_End(endDateString,string.valueof(UserInfo.getTimeZone()));

            string calenderId=calInviteObj.GoogleCalendarId__c;

            string eventid = calInviteObj.GoogleCalendarEventId__c;//'h62e0bbtfh99lnnhijcsqve5eg';
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            string endPoint =  'https://www.googleapis.com/calendar/v3/calendars/'+calenderId+'/events/'+eventid+'?sendUpdates=all';   
            calenderId='';
            eventid ='';
            system.debug('&&&&&&&&'+endPoint);
            req.setEndpoint(endPoint);
            
            list<CalendarInvite_overrides> clOverridesLst = new List<CalendarInvite_overrides>();
            clOverridesLst.add(new CalendarInvite_overrides('email',15));
            CalendarInvite_reminders calReminder = new CalendarInvite_reminders(false,clOverridesLst);
            
            CalendarInvite_Organizer calOrganizer = new CalendarInvite_Organizer(calInviteObj.GoogleCalendarId__c,'Organizer',true);
            CalendarInviteWrapper calInviteWrapObj = new CalendarInviteWrapper(calInviteObj.GoogleCalendarId__c, calInviteObj.Calendar_Entry_Name__c,calInviteObj.Venue__r.name+' , '+calInviteObj.Venue_Address__c,clStart,clEnd, clAttendeesLst,calOrganizer,false,'opaque',calReminder,calInviteObj.Description__c);
            
            string bodyRequest = JSON.serialize(calInviteWrapObj);
            bodyRequest = bodyRequest.replace('"enddate":', '"end":');
            bodyRequest = bodyRequest.replace('"cld_dateTime":', '"dateTime":');
            
            req.setBody(bodyRequest);     
            System.debug(bodyRequest);
            if(automationType =='CalendarInvite'){
                req.setHeader('Authorization', 'Bearer ' +accessToken);
            }else if(automationType =='Opportunity'){
                req.setHeader('Authorization', 'Bearer ' +getAccessToken());
            }
            
            req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
            req.setHeader('Content-Type', 'application/json; charset=UTF-8');
            //  req.setHeader('Accept','application/json');
            
            //req.setMethod('PUT');

            req.setMethod('PUT'); // Put for update but it is overriding
            req.setHeader('X-HTTP-Method-Override','PATCH');
            req.setTimeout(10000);
            Integer statusCode = null;
            if(!Test.isRunningtest()){
                HttpResponse res = h.send(req); 
               
                System.debug(res.getBody());
                statusCode = res.getStatusCode();
            }else{
                statusCode = 200;
            }
            if( statusCode !=200){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 'There is a error on Google Calendar Integration'));

            }else{
                setOfEmails.remove(calInviteObj.GoogleCalendarId__c);
                for(string strEmail:setOfEmails){
                    if(strEmail != null && strEmail !=''){
                        Calendar_Invitee__c calInviteeObj =  new Calendar_Invitee__c();
                        calInviteeObj.Calendar_Invite__c = calInviteObj.Id;
                        calInviteeObj.Invite_Status__c = 'Pending';
                        calInviteeObj.InviteeEmail__c = strEmail;
                        
                        if(notifiedEmailsMap.containsKey(calInviteObj.Id)){
                            system.debug('@@notifiedEmailsMap.get(calInviteObj.Id)'+notifiedEmailsMap.get(calInviteObj.Id));
                            if(notifiedEmailsMap.get(calInviteObj.Id).contains(strEmail)){
                                calInviteeObj.IsResending__c = true;
                            }
                        }
                        if(automationType =='CalendarInvite'){
                            calInviteeObj.Opportunity__c = opptyObj.Id;
                        }else if(automationType =='Opportunity'){
                            if(eventToOppMap.containsKey(calInviteObj.Event__c)){
                                
                                calInviteeObj.Opportunity__c = eventToOppMap.get(calInviteObj.Event__c);
                            }
                        }
                        insertCalendarInvites.add(calInviteeObj);
                    }
                }
           //}
           //}
           }
        }
        

    }  
    global void finish(Database.BatchableContext BC)
    {
       system.debug('@@insertCalendarInvites'+insertCalendarInvites);
       
       if(insertCalendarInvites.size()>0){
            
            
            if(automationType =='Opportunity'){    
                   set<string> calInviteEmsils = new set<string>();
                   for(Calendar_Invitee__c calObj:insertCalendarInvites){
                      calInviteEmsils.add(calObj.InviteeEmail__c);
                 
                   }
                   List<Calendar_Invitee__c> delCalendarInvites = [SELECT id FROM Calendar_Invitee__c WHERE InviteeEmail__c IN :calInviteEmsils AND Opportunity__c IN:oppIds AND Invite_Status__c !='Yes'];
                
                
                   if(delCalendarInvites != null & delCalendarInvites.size()>0){
                        delete delCalendarInvites;
                   }
            }    
            insert insertCalendarInvites;
            
            /*List<Calendar_Invite__c> calInvitesObjLst = new List<Calendar_Invite__c>();
            if(automationType =='CalendarInvite'){
                system.debug('@@opptyObj.Event__c'+opptyObj.Event__c);
                for(calendar_Invite__c calObj:[SELECT id, CalendarInviteesCount__c, Event__c, Description__c, End_Date__c,Invite_Type__c,
                                                                                        See_Guest_List__c, Send_Notification__c , Start_Date__c, EventTimeZone__c,Venue__c, Venue_Address__c, 
                                                                                        Visibility__c FROM Calendar_Invite__c WHERE Event__c =:opptyObj.Event__c AND IsInviteesLimitReached__c = false ]){
                                                                                        
                     system.debug('@@calObj'+calObj+'@@calObj.CalendarInviteesCount__c'+calObj.CalendarInviteesCount__c);
                     if(Integer.ValueOf(calObj.CalendarInviteesCount__c) >3 ){
                         system.debug('@@calObjInside'+calObj);
                         calInvitesObjLst.add(calObj);
                     }                                                                   
                }
            
            
            }else if(automationType =='Opportunity'){
               for(Calendar_Invite__c calObj: [SELECT id, CalendarInviteesCount__c, Event__c, Description__c, End_Date__c,Invite_Type__c,
                                                                                        See_Guest_List__c, Send_Notification__c , Start_Date__c, EventTimeZone__c,Venue__c, Venue_Address__c, 
                                                                                        Visibility__c FROM Calendar_Invite__c WHERE Event__c IN:eventToOppMap.Keyset() AND IsInviteesLimitReached__c = false]){
                
                   if(calObj.CalendarInviteesCount__c >3 ){
                         calInvitesObjLst.add(calObj);
                   } 
               }
               
            }
            system.debug('@@calInvitesObjLst'+calInviteObjLst);
            CreateGoogleEventsQueable updateJob = new CreateGoogleEventsQueable(calInvitesObjLst);
            
            // enqueue the job for processing
            ID jobID = System.enqueueJob(updateJob);*/
    }
    }  
     
    public static string getAccessToken(){
        
        string bodyRequest = '';
        
        system.debug('@@authorizationCode'+authorizationCode);       
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(tokenEndPointValue);

        bodyRequest = 'refresh_token=' + EncodingUtil.urlEncode(authorizationCode, 'UTF-8');
       
        bodyRequest += '&client_id=' + EncodingUtil.urlEncode(googleClientID, 'UTF-8');
        bodyRequest += '&client_secret=' + EncodingUtil.urlEncode(googleSecretCode, 'UTF-8');
        bodyRequest += '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8');
       
        bodyRequest += '&grant_type=refresh_token';
        req.setBody(bodyRequest);     
        req.setHeader('Content-length', string.ValueOf(bodyRequest.length())); 
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setTimeout(10000);
        if(!Test.isRunningTest()){
                        
            HttpResponse res = h.send(req);   
            map<string, string> jsonValues = new map<string, string>();
          
            System.debug('Response Value:'+res.getBody());
            jsonValues = parseJSONToMap(res.getBody());
            if(jsonValues.containsKey('error')){ 
            }else{
                //Try to get a cell value in the Google Spreadsheet
                accessToken = jsonValues.get('access_token');            
            }
        }else{
            accessToken = 'as23as32ease23asd12';
        }
        
        if(accessToken <> ''){
           return accessToken ;
        } else{
        return 'Invalid Refresh token';
        }
           
    }
    public class CalendarInviteWrapper{
    public string calendarId;
    public String summary;  //Sales Call
    public String location; //Conference Room A
    public CalendarInvite_start start;
    public CalendarInvite_end enddate;
    public List<CalendarInvite_attendees> attendees;
    public CalendarInvite_Organizer organizer;
    public Boolean guestsCanSeeOtherGuests;
    public string transparency;
    public CalendarInvite_reminders reminders;
     public string description;
        public CalendarInviteWrapper(string calendarIdval,string summaryVal,string locationVal,CalendarInvite_start cld_StartDate,CalendarInvite_end cld_endDate, List<CalendarInvite_attendees> cld_recurAttList,CalendarInvite_Organizer organizerVal,Boolean calendarPrivacySetting,string transparencyVal,CalendarInvite_reminders remindersVal,string calendarDescription){
            this.calendarId = calendarIdval; 
            this.summary = summaryVal;
            this.location = locationVal;
            this.start = cld_StartDate;
            this.endDate = cld_endDate;
            
            this.attendees = cld_recurAttList;
            this.organizer = organizerVal;
            this.guestsCanSeeOtherGuests = calendarPrivacySetting;
            this.transparency = transparencyVal;
            this.reminders = remindersVal;
             this.description = calendarDescription;
        }
        
    
    }
    public static map<string, string> parseJSONToMap(string JSONValue){
        JSONParser parser = JSON.createParser(JSONValue);
        map<string, string> jsonMap = new map<string, string>();
        string keyValue = '';
        string tempValue = '';
        while (parser.nextToken() != null) {
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME){
                keyValue = parser.getText();
                parser.nextToken();
                tempValue = parser.getText();
                jsonMap.put(keyValue, tempValue);             
            }
        }
        return jsonMap;
    }
 
    public class CalendarInvite_start {
            public String cld_dateTime; //2018-09-29T08:00:00.000-07:00
            public String timeZone; //America/Los_Angeles
            public CalendarInvite_start(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
    public class CalendarInvite_end {
            private String cld_dateTime{get;set;} //2018-09-30T08:30:00.000-07:00
            private String timeZone; //America/Los_Angeles
            private CalendarInvite_end(string dateTimeVal,string timeZoneVal){
                this.cld_dateTime = dateTimeVal;
                this.timeZone = timeZoneVal;
            }
        }
    public class CalendarInvite_overrides{
        private String method; //2018-09-30T08:30:00.000-07:00
        private Integer minutes; //America/Los_Angeles
        public CalendarInvite_overrides(string methodVal, Integer minutesVal){
            this.method = methodVal;
            this.minutes = minutesVal;
        }
        
    }
   
     public class CalendarInvite_reminders {
        private Boolean useDefault; //2018-09-30T08:30:00.000-07:00
        private List<CalendarInvite_overrides> overrides; //America/Los_Angeles
        private CalendarInvite_reminders(Boolean useDefaultVal,List<CalendarInvite_overrides> overridesVal){
            this.useDefault = useDefaultVal;
            this.overrides = overridesVal;
        }
    }
       
    public class CalendarInvite_attendees {
            public String email;    //maddy.crmdev@gmail.com
            public String responseStatus;
            public CalendarInvite_attendees(string organizerEmail, String status){
                this.email = organizerEmail;
                this.responseStatus= status;
            }
        }
        public class CalendarInvite_Organizer {
            public String email;    //maddy.crmdev@gmail.com
            public string displayName;
            public Boolean self;
            public CalendarInvite_Organizer(string organizerEmail,string displayNameVal, Boolean selfVal){
                this.email = organizerEmail;
                this.displayName = displayNameVal;
                this.self = selfVal;
            }
        }
          //this method is being called from AttendeeSyncService class
   
}