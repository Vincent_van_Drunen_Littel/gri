/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe de teste da classe OpportunityLineItemUpdatesOpp.
*
* NAME: OpportunityLineItemUpdatesOppTest.trigger
* AUTHOR: EAD                                                  DATE: 08/02/2018
*******************************************************************************/
@isTest
public class OpportunityLineItemUpdatesOppTest {
  public static String RT_ACC = RecordTypeMemory.getRecType('Account', 'Customer');
  public static String RT_OPP = OrgDefs.RT_OPP_MEMBERSHIP;  
  public static Account acc;
  public static Opportunity opp;
  public static Opportunity opp2;  
  public static PricebookEntry pbe;  
  public static String pbId;  
  public static Product2 pd2;  
    
  static{
    //=== Custom Setting ===  
    Application_Config__c appCon = new Application_Config__c(Name = 'BucketUserId', Value__c = RT_ACC);
    insert appCon;  
      
    //=== Account ===
    acc = SObjectInstance.createAccount(RT_ACC); 
    insert acc;
      
    //=== Contact ===
    Contact ctt = SObjectInstance.createContato(acc.id);
    insert ctt;
      
    //=== Pricebook2 ===
    pbId = SObjectInstance.catalogoDePrecoPadrao2();
    
    //=== Product2 ===
    pd2 = SObjectInstance.createProduct2();
    insert pd2;  
      
    //=== Opportunity to insert ===
    opp = SObjectInstance.createOpportunidade(acc.id, RT_OPP);
    opp.Pricebook2Id = pbId; 
    opp.Contact__c = ctt.id;  
    opp.Approval_Process__c = '1 - Approved';
    insert opp; 
      
    //=== Opportunity to update ===
    opp2 = SObjectInstance.createOpportunidade(acc.id, RT_OPP);
    opp2.Pricebook2Id = pbId; 
    opp2.Contact__c = ctt.id;  
    opp2.Approval_Process__c = '3 - Rejected';
    insert opp2;   
      
    //=== PricebookEntry ===
    pbe = new PricebookEntry();
    pbe.Pricebook2Id = pbId;  
    pbe.Product2Id = pd2.id;  
    pbe.UnitPrice = 45.00;
    pbe.IsActive = true;  
    insert pbe;
  }  
    
  public static testMethod void testSingleInsert(){
    //== OLI ===
    OpportunityLineItem oli = SObjectInstance.createProdutoOportunidade(opp.id, pbe.id);
    oli.Discount = 0;  
      
    OpportunityLineItem oli2 = SObjectInstance.createProdutoOportunidade(opp.id, pbe.id);
    oli2.Discount = 20;    
	
    //======== Test Session ========
    Test.startTest();
      insert oli;
      list<Opportunity> lstOpp = [SELECT Approval_Process__c FROM Opportunity WHERE id =: opp.id];
      System.assertEquals('1 - Approved', lstOpp.get(0).Approval_Process__c);
      
      insert oli2;
      lstOpp = [SELECT Approval_Process__c FROM Opportunity WHERE id =: opp.id];
      System.assertEquals('2 - Pending', lstOpp.get(0).Approval_Process__c);
    Test.stopTest(); 
  }
    
  public static testMethod void testSingleUpdate(){
    //== OLI ===
    OpportunityLineItem oli = SObjectInstance.createProdutoOportunidade(opp2.id, pbe.id);
    oli.Discount = 0;  
	insert oli;
      
    //======== Test Session ========
    Test.startTest();
      list<Opportunity> lstOpp = [SELECT Approval_Process__c FROM Opportunity WHERE id =: opp2.id];
      System.assertEquals('3 - Rejected', lstOpp.get(0).Approval_Process__c);
      
      oli.Discount = 20;
      update oli;
      
      lstOpp = [SELECT Approval_Process__c FROM Opportunity WHERE id =: opp2.id];
      System.assertEquals('2 - Pending', lstOpp.get(0).Approval_Process__c);
    Test.stopTest(); 
  }  
}