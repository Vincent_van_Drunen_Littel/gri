public class SObjectHelper {

    private static final String MEMBERSHIP_STATUS_ACTIVATED = 'Activated';
    private static final String MEMBERSHIP_STATUS_EXPIRED = 'Expired'; // grace period
    private static final String MEMBERSHIP_STATUS_DRAFT= 'Draft';
    //private static final String MEMBERSHIP_STATUS_EXPIRED = 'Expired'; // grace period

    
    ///////////////////////////////////////////////////////////////////////////
    // Opportunity helpers
    ///////////////////////////////////////////////////////////////////////////
    
    /**
     * Return all opps form a list at a given list of StageNames
     */
    public static List<Opportunity> Opportunity_retStage(List<Opportunity> opps, List<String> strStagesNames)
    {
        List<Opportunity> listOppStages = new List<Opportunity>();
        for(Opportunity opp:opps)
        {
            for(String stage:strStagesNames)
            {
                if(opp.StageName == stage)
                {
                    listOppStages.add(opp);
                }
            }
        }
        return listOppStages;
    }
    
    /**
     * Return a list of Opportunity Ids 
    */
    public static List<Id> Opportunity_returnIdsFromOpps(List<Opportunity> listOpp)
    {
        List<Id> oppIds = new List<Id>();
        for(Opportunity opp:listOpp)
        {
            oppIds.add(opp.Id);
        }
        return(oppIds);
    }

    /**
     * Return the contact Ids from a Opp list, no repetition
     */    
    public static List<Id> Opportunity_returnContactIdListNoRepeat(List<Opportunity> listOpp)
    {
        Set<Id> idSet = new Set<Id>();
        for(Opportunity opp: listOpp)
        {
            idSet.add(opp.Contact__c);
        }
        List<Id> listIds = new List<Id>(idSet);
        listIds.sort();
        return listIds;
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Membership(Contract) helpers
    /////////////////////////////////////////////////////////////////////////// 
    
    /**
     * Returns a list of contact IDs from memberhips
     */
    public static List<Id> Membership_returnContactIds(List<Contract> memberships)
    {
        List<Id> ContactIds = new List<Id>();
        for(Contract memb:memberships)
        {
            ContactIds.add(memb.Contact__c);
        }
        return ContactIds;
    }
      
    ///////////////////////////////////////////////////////////////////////////
    // Contact helpers
    /////////////////////////////////////////////////////////////////////////// 
    public static Contact Contact_getId(List<Contact> listContacts, Id contactId)
    {
        for(Contact ci : listContacts)
        {
            if(ci.Id == contactId)
                return(ci);
        }
        return NULL; // return empty contact
    }


}