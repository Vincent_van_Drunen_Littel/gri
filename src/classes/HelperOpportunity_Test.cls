/**************************************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure players
 **************************************************************************************************
 * Testclass for HelperOpportunity Class
 *
 * @author  Anderson Paschoalon
 * @version 1.0
 * @since
 * @testClasses HelperSObject_Test  
 * Date        Author         Description
 * 11/06/18    AndPaschoalon  First version
 */

@IsTest
public with sharing class HelperOpportunity_Test {

	@isTest
	public static void testRecTypeEvents() 
	{
		Test.startTest();
		System.assertNotEquals(NULL, HelperOpportunity.recordTypeIdEvents(), 'Record Type for Events retuned NULL');
		Test.stopTest();
	}

	@isTest
	public static void testRecTypeMembership() 
	{
		Test.startTest();
		System.assertNotEquals(NULL, HelperOpportunity.recordTypeIdMembership(), 'Record Type for Membership retuned NULL');
		Test.stopTest();
	}

	@isTest
	public static void testRecTypeMagazine() 
	{
		Test.startTest();
		System.assertNotEquals(NULL, HelperOpportunity.recordTypeIdMagazine(), 'Record Type for Magazine retuned NULL');
		Test.stopTest();
	}


	@isTest
	public static void testRecTypeSpex() 
	{
		Test.startTest();
		System.assertNotEquals(NULL, HelperOpportunity.recordTypeIdSPEX(), 'Record Type for SPEX retuned NULL');
		Test.stopTest();
	}
}