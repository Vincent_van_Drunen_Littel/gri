@isTest(SeeAllData=true)
//@isTest
private class VATonOpportunityTriggerTest {
    
    @isTest
    public static void myUnitTestA() {
        
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI Services Ltd';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'UK';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    insert opp;   
    }
    
    @isTest
  public static void UkVatTest()
  {
   Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI Services Ltd';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'UK';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    opp.CompanyName__c = 'GRI Services Ltd';
    insert opp;
  }
 @isTest
  public static void FranceVatTest()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'France';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'FR';
    opp.Event__c   = event.Id;
    opp.VAT_Number__c = 'FR00000000000';
    insert opp;

   
  }
 @isTest
  public static void FranceVatTest20()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'France';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'FR';
    opp.Event__c   = event.Id;
    opp.VAT_Number__c = 'FR0000';
    insert opp;
  }
 @isTest
  public static void SpainVatTest()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'Spain';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'ES';
    opp.Event__c   = event.Id;
    opp.VAT_Number__c = 'ES111111111';
    insert opp;
  }
 @isTest
  public static void SpainVatTest20()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'Spain';
    insert event;

    Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];

    Pricebook2 pbk1 = new Pricebook2 (Name='Test Pricebook Entry 1',Description='Test Pricebook Entry 1', isActive=true);
    insert pbk1;

    Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1',productCode = 'ABC', isActive = true);
    insert prd1;

    PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=standardPb.id,UnitPrice=50, isActive=true);
    insert pbe1;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.Pricebook2Id = pbe1.Pricebook2Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'ES';
    opp.Event__c   = event.Id;
    opp.VAT_Number__c = 'ES111111';
    insert opp;

    OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp.id,PriceBookEntryID=pbe1.id, quantity=4, totalprice=200);
    insert lineItem1;

  }
 @isTest
  public static void GermanyVatTest()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'Germany';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'DE';
    opp.Event__c   = event.Id;
    opp.VAT_Number__c = 'DE111111111';
    insert opp;
  }

 @isTest
  public static void GermanyVatTest19()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'Germany';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'DE';
    opp.Event__c   = event.Id;
    opp.VAT_Number__c = '';
    insert opp;
  }
 @isTest
   public static void SwissVatTest()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI American European LLC';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'Swiss';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'SW';
    opp.Event__c   = event.Id;
    opp.VAT_Number__c = 'SW111111111';
    insert opp;
  }
   @isTest
     public static void REclubMembershipVatTest()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Club__c club = new Club__c();
    club.Name = 'RE Club Europe';
    club.Company_Name__c = 'GRI Services Ltd';
    club.Vendor_Name__c  = 'globalreal';
    insert club;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Membership').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'GB';
    opp.Club__c    = club.Id;
    opp.VAT_Number__c = 'SW111111111';
    insert opp;
  }
 
 @isTest
     public static void SMembershipVatTest()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Club__c club = new Club__c();
    club.Name = 'Test';
    club.Company_Name__c = 'GRI Services Ltd';
    club.Vendor_Name__c  = 'globalreal';
    insert club;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Membership').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'UK';
    opp.Club__c    = club.Id;
    opp.VAT_Number__c = 'SW111111111';
    insert opp;
  }
 @isTest
     public static void SMembershipVatTest20()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Club__c club = new Club__c();
    club.Name = 'Test';
    club.Company_Name__c = 'GRI Services Ltd';
    club.Vendor_Name__c  = 'globalreal';
    insert club;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Membership').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'DE';
    opp.Club__c    = club.Id;
    opp.VAT_Number__c = 'SW111111111';
    insert opp;
  }
 @isTest
     public static void SMembershipExceptionTest()
  {
    Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Club__c club = new Club__c();
    club.Name = 'Test';
    club.Vendor_Name__c  = 'globalreal';
    insert club;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Membership').getRecordTypeId(); 
    opp.CompanyName__c = 'GRI American European LLC';
    opp.Billing_Country__c = 'DE';
    opp.Club__c    = club.Id;
    opp.VAT_Number__c = 'SW111111111';
    insert opp;
  }
 //@isTest
 //  public static void ExceptionTest()
 // {
 //   Account acc  = new Account();
 //   acc.Name     = 'Test acc';
 //   acc.BillingStreet     = 'test';
 //   acc.BillingCity       = 'test';
 //   acc.BillingPostalCode = 'hu5 4bs';
 //   acc.BillingCountry    = 'UK';
 //   acc.ZIP_Code__c       = 'test';
 //   insert acc;
    
 //   Contact cc   = new Contact();
 //   cc.FirstName = 'Test';
 //   cc.LastName  = 'Test cc';
 //   cc.Email = 'test@test.com';
 //   cc.AccountId = acc.Id;
 //   insert cc;

 //   Event__c event        = new Event__c();
 //   event.Name            = 'Events';
 //   event.Start_date__c   = system.today();
 //   event.End_date__c     = system.today();
 //   event.Status__c       = 'In Production';
 //   event.VAT_Country__c  = 'Swiss';
 //   insert event;

 //   Opportunity opp = new Opportunity();
 //   opp.Name = 'Opp Test';
 //   opp.Contact__c = cc.Id;
 //   opp.AccountId  = acc.Id;
 //   opp.CloseDate  = system.today();
 //   opp.StageName  = 'Suspect';
 //   opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
 //   opp.CompanyName__c = 'GRI American European LLC';
 //   opp.Billing_Country__c = 'SW';
 //   opp.CurrencyIsoCode = 'GBP';

 //   opp.VAT_Number__c = 'SW111111111';
 //   insert opp;
 //   Quote testQuote = new Quote();
 //   testQuote.Name = 'test2';
 //   testQuote.OpportunityId = opp.Id;
 //   testQuote.ContactId = cc.Id;
 //   testQuote.Proposal_Valid_Until__c = Date.today();
 //   insert testQuote;
    
 //   opp.Billing_Country__c = 'SW2';
 //   update opp;
 // }

@isTest
  public static void PortugalVatTest()
  {
   Account acc  = new Account();
    acc.Name     = 'Test acc';
    acc.BillingStreet     = 'test';
    acc.BillingCity       = 'test';
    acc.BillingPostalCode = 'hu5 4bs';
    acc.BillingCountry    = 'UK';
    acc.ZIP_Code__c       = 'test';
    insert acc;
    
    Contact cc   = new Contact();
    cc.FirstName = 'Test';
    cc.LastName  = 'Test cc';
    cc.Email = 'test@test.com';
    cc.AccountId = acc.Id;
    insert cc;

    Event__c event        = new Event__c();
    event.Name            = 'Events';
    event.Start_date__c   = system.today();
    event.End_date__c     = system.today();
    event.Status__c       = 'In Production';
    event.Company_Name__c = 'GRI Services Ltd';
    event.Vendor_Name__c  = 'globalreal';
    event.VAT_Country__c  = 'Portugal';
    insert event;

    Opportunity opp = new Opportunity();
    opp.Name = 'Opp Test';
    opp.Contact__c = cc.Id;
    opp.AccountId  = acc.Id;
    opp.CloseDate  = system.today();
    opp.StageName  = 'Suspect';
    opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Events').getRecordTypeId(); 
    opp.Event__c   = event.Id;
    //opp.CompanyName__c = 'GRI Services Ltd';
    insert opp;
  }
}