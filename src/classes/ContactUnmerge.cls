/*******************************************************************************
*                               Cloud2b - 2018
*-------------------------------------------------------------------------------
*
* Classe que pede o merge de contatos quandos estes estão no mesmo evento e conta.
*
* NAME: ContactUnmerge.cls
* AUTHOR: VOD                                                 DATE: 29/01/2017
*******************************************************************************/
public class ContactUnmerge {
	
    public class mergeWrapper{
        Contact cttMaster;
        Contact cttMerge;
        map<String, String> mapCttMasEvt;
        map<String, String> mapCttMerEvt;
        
        mergeWrapper(){
            cttMaster = new Contact();
            cttMerge = new Contact();
            mapCttMasEvt = new map<String, String>();
            mapCttMerEvt = new map<String, String>();
        }
        
    }
    
    public static void execute()
    {
        //=== Retorna lista de perfil do usuário que pode dar unmerge...
        set<String> perfilUnmerge = new set<String>(); 
        for(ContactUnmergeProfile__c CUP : [SELECT Profile_ID__c FROM ContactUnmergeProfile__c]){
            perfilUnmerge.add(CUP.Profile_ID__c);    			
        }
        //... se o perfil do usuário estiver na lista, encerra o método.
        if(perfilUnmerge.contains(String.valueOf(UserInfo.getProfileId()).left(15))) return;
    	
        map<String, Contact> mapContact = new map<String, Contact>();
        map<mergeWrapper, string> mapWrp = new map<mergeWrapper, String>();
        for(Contact ctt : (list<Contact>) trigger.new){
            mapContact.put(ctt.Id, ctt);
        }
        
        //O campo MasterRecordId só é preenchido quando o registro é deletado por um merge
        if(mapContact.isEmpty()) return;
        for(Contact ctt : [SELECT id, MasterRecordId, AccountId FROM Contact WHERE MasterRecordId =: mapContact.keyset() and isDeleted = true ALL ROWS]){
            mergeWrapper wrp = new mergeWrapper();
            wrp.cttMerge = ctt;
            wrp.cttMaster = mapContact.get(ctt.MasterRecordId);
            mapWrp.put(wrp, null);
        }
        
        //Remove os Contatos que não são da mesma conta
        if(mapWrp.isEmpty()) return;
        set<String> setCtt = new set<String>();
        for(mergeWrapper wrp : mapWrp.keySet()){
            if(wrp.cttMaster.AccountId != wrp.cttMerge.AccountId){
                mapWrp.remove(wrp);
            }
            else{
                setCtt.add(wrp.cttMaster.Id);
                setCtt.add(wrp.cttMerge.Id);
            }
        }
        
        // Verifica que Contatos possuem um ou mais eventos com oportunidade
        if(mapWrp.isEmpty()) return;
        for(Opportunity opp : [SELECT id, Contact__c, Event__c, opp_unique__c FROM Opportunity 
                            WHERE Contact__c =: setCtt])
        {
            for(mergeWrapper wrp : mapWrp.keySet()){                
                if(opp.opp_unique__c == null) continue;
                if(opp.Event__c == null) continue;
                
	              if(opp.opp_unique__c.contains(wrp.cttMaster.Id)) wrp.mapCttMasEvt.put(opp.Event__c, null);
	              if(opp.opp_unique__c.contains(wrp.cttMerge.Id)) wrp.mapCttMerEvt.put(opp.Event__c, null);
                
            }
        }
        
        // Verifica quais contatos estão em um mesmo evento 
        for(mergeWrapper wrp : mapWrp.keySet()){
            Boolean match = false;
            for(String evt1 : wrp.mapCttMasEvt.keySet()){
                if(wrp.mapCttMerEvt.containsKey(evt1)) match = true;
            }
            if(!match) mapWrp.remove(wrp);
        }
        
        // Os registros que sobraram foram deletados e atualizados pelo merge e se encontram na mesma conta e evento
        if(mapWrp.isEmpty()) return;
        set<String> setUndelete = new set<String>();
        map<String, String> mapAddError = new map<String, String>();
        for(mergeWrapper wrp : mapWrp.keySet()){
            mapAddError.put(wrp.cttMaster.Id, null);
            setUndelete.add(wrp.cttMerge.Id);
        }
        
        // Restaura os contatos que foram deletados
        if(!setUndelete.isEmpty()){
            list<String> lstUndelete = new list<String>(setUndelete);
            Database.undelete(lstUndelete);
        } 
        
        // E impede a atualização dos merged
        if(!mapAddError.isEmpty()){
            for(Contact ctt : (list<Contact>) trigger.new){
							if(mapAddError.containsKey(ctt.Id)){
              	ctt.addError(Label.Merge_error_message);
              }
            }
        } 
        
    }
    
}