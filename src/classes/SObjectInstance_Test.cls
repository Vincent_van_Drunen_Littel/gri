/**************************************************************************************************
* GRI Club - Gathering the world’s leading real estate and infrastructure players 
**************************************************************************************************
* A test class for Cloud2B class SObjectInstance, since it had no Test Class for it
* 
* @author  Anderson Paschoalon
* @version 1.0
* @since
* Date        Author         Description
* 25/07/18    AndPaschoalon  First version 
*/
@IsTest
public class SObjectInstance_Test {

    @isTest
    private static void test()
    {
        Id oppId  = '0060x000005e4cf'; 
        Id conId  = '0033600001LNare'; 
        Id accId  = '0013600000xoKdf'; 
        Id userId = '00536000000qAkkAAE'; 
        Id clubId = 'a0136000003kO5jAAE';
        Id eventId = 'a020x000001LgY8AAK';
        Id recTypeId = '0060x000003lm5UAAQ';
        Id taskId = '00T0x000001ptbBEAQ';
        Id sessId = 'a0636000006BaanAAC';
        Id pbId = '01s0x0000000i2KAAQ';
        Id peId = '01u0x000000imwCAAQ';
        Id orId = '80136000001LjXyAAK';
        Id ssId = 'a0636000006BaanAAC';
        String str = 'str';
        Date dd = System.today();
        
        SObjectInstance.catalogoDePrecoPadrao2();
        SObjectInstance.catalogoDePreco();
        SObjectInstance.createProdutoOportunidade2( oppId, peId);
        SObjectInstance.usuario(userId);
        SObjectInstance.usuario(userId, str);
        SObjectInstance.createorder(recTypeId, accId, pbId, eventId);
        SObjectInstance.createOrderItens(orId, peId);
        Contact ccc = SObjectInstance.createContato(accId);
        Contact ccc2 = SObjectInstance.createContato2(accId); 
        SObjectInstance.createProdutoOportunidade( oppId, peId);
        SObjectInstance.createOpportunidadeSPEX(accId, recTypeId, conId);
        SObjectInstance.createOpportunidadeMenber(accId, recTypeId, conId, clubId);
        SObjectInstance.createOpportunidade2(accId, recTypeId, conId, eventId);
        SObjectInstance.createOpportunidadeMagazine(accId, recTypeId, conId); 
        SObjectInstance.createOpportunidade(accId, recTypeId);
        SObjectInstance.createTask(pbId) ;       

        SObjectInstance.createProduct2();
        SObjectInstance.createContato(accId);
        SObjectInstance.createAccount(recTypeId);
        SObjectInstance.createAccount2();
        SObjectInstance.createSession(eventId);
        SObjectInstance.createAttendee(eventId, ssId, oppId);
        SObjectInstance.createContract(conId, accId);
        SObjectInstance.entradaDePreco(pbId, peId);
        
        SObjectInstance.createEventAttendee(oppId, accId, conId, eventId);
        SObjectInstance.createEventDefault(oppId);
        SObjectInstance.createMagazine();
        SObjectInstance.createClub();
        SObjectInstance.createCampaign();
        SObjectInstance.createCampaignMember();
        SObjectInstance.createEvent();
        //SObjectInstance.perfil('aaa');
        SObjectInstance.subirCobertura();       
    }
    
}