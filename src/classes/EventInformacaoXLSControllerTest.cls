/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe de teste da classe "EventInformacaoXLSController".
*
* NAME: EventInformacaoXLSControllerTest.cls
* AUTHOR: RVR                                                  DATE: 07/04/2016
*******************************************************************************/
@isTest
private class EventInformacaoXLSControllerTest 
{
  private static ID ID_EVENT = RecordTypeMemory.getRecType('Opportunity', 'Open_Events');
  static testMethod void 	testeFuncional()
  {
    Account conta = SObjectInstance.createAccount2();
    Database.insert(conta);

    Contact contato = SObjectInstance.createContato(conta.Id);
    Database.insert(contato);

    Test.startTest();
    Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);

    evento.Start_Date__c = system.today().addDays(22);
    evento.End_Date__c = system.today().addDays(22);
    update(evento);

    Id catalogo = SObjectInstance.catalogoDePrecoPadrao2();

    Pricebook2 catalogo2 = SObjectInstance.catalogoDePreco();
    catalogo2.Event__c = evento.Id;
    catalogo2.IsActive = true;
    catalogo2.Starting_Date__c = system.today();
    catalogo2.End_Date__c = system.today()+32;
    Database.insert(catalogo2);

    Product2 produto = SObjectInstance.createProduct2();
    Database.insert(produto);    

    list<PricebookEntry> lstPbeOld = new list<PricebookEntry>();
    list<PricebookEntry> lstPbeNew = new list<PricebookEntry>();

    PricebookEntry pbe = SObjectInstance.entradaDePreco(catalogo, produto.Id);
    pbe.Product2Id = produto.Id;
    Database.insert(pbe);
    lstPbeOld.add(pbe);

    PricebookEntry pbe2 = SObjectInstance.entradaDePreco(catalogo2.Id, produto.Id);
    pbe2.Product2Id = produto.Id;
    Database.insert(pbe2);
    lstPbeOld.add(pbe2);

    Opportunity opp = SObjectInstance.createOpportunidade(conta.Id, ID_EVENT);
    opp.Contact__c = contato.Id;
    opp.StageName = 'Negotiation';
    opp.Event__c = evento.Id;
    Database.insert(opp);

    list<OpportunityLineItem> lstOppLI = new list<OpportunityLineItem>();
    OpportunityLineItem oppLI = SObjectInstance.createProdutoOportunidade( opp.Id, pbe2.Id );
    oppLI.Quantity = 1;
    Database.insert(oppLI);
    lstOppLI.add(oppLI);

    Session__c sessao = SObjectInstance.createSession(evento.Id);
    sessao.Start_Time__c = '10:00 AM';
    sessao.End_Time__c = '10:00 PM';
    Database.insert(sessao);

    List<Session__c> lstSessao = new List<Session__c>{sessao};

    Attendee__c participante = SObjectInstance.createAttendee(evento.Id, sessao.Id, opp.Id);
    participante.Contact__c = contato.Id;
    participante.Account__c = conta.Id;
    Database.insert(participante);

    EventInformacaoXLSController controller = new EventInformacaoXLSController(new ApexPages.Standardcontroller(evento));
    controller.getLstSessao();
    EventInformacaoXLSController.SessaoWrapper sessaoWrapper = new EventInformacaoXLSController.SessaoWrapper(sessao);
    DateTime dataStarte = sessaoWrapper.dtStart;
    DateTime dataEnd = sessaoWrapper.dtEnd;
    String url = sessaoWrapper.urlSession;
    Test.stopTest();
  }

}