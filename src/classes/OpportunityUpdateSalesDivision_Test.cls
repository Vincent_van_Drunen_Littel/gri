@IsTest
public class OpportunityUpdateSalesDivision_Test {
    //private static String jobRole = 'Business Analyst';
    private static String jobTitle = 'test-job-title-UpdateSalesDivision';
    private static List<Club__c> listClubs = TestDataFactory.createClubs(
        new List<String>{ TestDataFactory.CLUB_RE_LATAM, TestDataFactory.CLUB_RE_BRAZIL, 
            TestDataFactory.CLUB_RE_INDIA});
    private static List<User> listCreatedUsers = 
        [SELECT Id, Name, Title, IsActive FROM User WHERE IsActive=true LIMIT 50];
    private static Integer numberOfUsers = listCreatedUsers.size();

    @isTest 
    public static void test_SalesDivisionUpdate()
    {   
        System.assertNotEquals(0, listCreatedUsers.size(), 'Users were not created!');
        System.assertEquals(numberOfUsers, listCreatedUsers.size(), 'number of users on listCreatedUsers is wrong!');
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(listCreatedUsers.size());
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent',  listClubs[0]);
        List<Opportunity> listOpps = TestDataFactory.createEventOpps(listContacts, theGreatEvent, listClubs[0], System.today()+10);
     
		Test.startTest();
        for(Integer i=0; i<listOpps.size(); i++)
        {
            listOpps[i].OwnerId=listCreatedUsers[i].Id;
            listOpps[i].Sales_Division_new__c='';
        }
        update listOpps;

        for(Integer i=0; i<listOpps.size(); i++)
        {
            listOpps[i].StageName=TestDataFactory.OPP_STAGE_APPROVED_BY_FINANCE;
        }
        update listOpps;
        List<Opportunity> updatedlistOpps = [SELECT Id, Name, StageName, OwnerId, Sales_Division_new__c FROM Opportunity];
        for(Integer i=0; i<updatedlistOpps.size(); i++)
        {
            System.assertEquals(listCreatedUsers[i].Title, updatedlistOpps[i].Sales_Division_new__c, 'Sales division name on Opps and User Title dont match!');
        }
        System.debug('Test Sucessefull!!');
        Test.stopTest();
    }
    
    @isTest 
    public static void test_methods()
    {
        System.assertNotEquals(0, listCreatedUsers.size(), 'Users were not created!');
        System.assertEquals(numberOfUsers, listCreatedUsers.size(), 'number of users on listCreatedUsers is wrong!');
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(listCreatedUsers.size());
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent',  listClubs[0]);
        List<Opportunity> listOpps = TestDataFactory.createEventOpps(listContacts, theGreatEvent, listClubs[0], System.today()+10);
     	List<Opportunity> newOpps = new List<Opportunity>();
        for(Opportunity opp:listOpps)
        {
            opp.StageName='Prospect';
            Opportunity singleNewOpp = new Opportunity(OwnerId=opp.OwnerId, 
                                                 StageName=TestDataFactory.OPP_STAGE_APPROVED_BY_FINANCE,
                                                 Name=opp.Name, Sales_Division_new__c=opp.Sales_Division_new__c);
            newOpps.add(singleNewOpp);
        }
        OpportunityUpdateSalesDivision.execute(newOpps, listOpps);
    }
    
    @isTest
    public static void test_errors()
    {
        System.assertNotEquals(0, listCreatedUsers.size(), 'Users were not created!');
        System.assertEquals(numberOfUsers, listCreatedUsers.size(), 'number of users on listCreatedUsers is wrong!');
        List<Contact> listContacts = TestDataFactory.createContactsWithDefaultAccount(listCreatedUsers.size());
        Event__c theGreatEvent = TestDataFactory.createEvent('theGreatEvent',  listClubs[0]);
        List<Opportunity> listOpps = TestDataFactory.createEventOpps(listContacts, theGreatEvent, listClubs[0], System.today()+10);
     	List<Opportunity> emptyList = new List<Opportunity>();
                
        try
        {
            OpportunityUpdateSalesDivision.execute(listOpps, emptyList);
        }
        catch(OpportunityUpdateSalesDivision.OpportuntiyUpdateSalesDivisionException e)
        {
            System.debug('ok!');
        }  
    }
    
    
    @testSetup 
    public static void buildData()
    {
        TestDataFactory.init();
        numberOfUsers = listCreatedUsers.size();
        //List<User> listCreatedUsers = TestDataFactory.createUsers(jobTitle, numberOfUsers, jobTitle);
        //List<User> listCreatedUsers = [SELECT Id, Name, Title FROM User LIMIT :numberOfUsers];
        //listCreatedUsers = [SELECT Id, Name, Title FROM User LIMIT :numberOfUsers];
        for(User singleUser:listCreatedUsers)
        {   
            singleUser.Title=jobTitle;
        }
        update listCreatedUsers;
       
    }

}