@IsTest
public class SalesPreChatController_Test
{
	
    private static final String oldcontactEmail = 'test@mail.com';
    private static final String oldleadEmail = 'oldlead@mail.com';
    
    @isTest
    private static void test_prechat()
    {
        System.debug('Offline Contact/Lead');
		SalesPreChatController.offlinePrechatProcedure(oldcontactEmail, '9997799', 'Email', 'Test First Name', 'Test Last Name', 'Acme LTDA', 'Test comment');        
		SalesPreChatController.offlinePrechatProcedure(oldleadEmail, '9997799', 'Email', 'Test First Name', 'Test Last Name', 'Acme LTDA', 'Test comment');
        SalesPreChatController.offlinePrechatProcedure(oldleadEmail, '9997799', 'Phone', 'Test First Name', 'Test Last Name', 'Acme LTDA', 'Test comment');           
		SalesPreChatController.offlinePrechatProcedure('newlead@email.com', '9997799', 'Email', 'Test First Name', 'Test Last Name', 'Acme LTDA', 'Test comment');        

        System.debug('Offline Contact/Lead');
        SalesPreChatController.onlinePrechatProcedure(oldcontactEmail, 'Test First Name', 'Test Last Name', 'Acme LTDA');
        SalesPreChatController.onlinePrechatProcedure(oldleadEmail, 'Test First Name', 'Test Last Name', 'Acme LTDA');
        SalesPreChatController.onlinePrechatProcedure('newlead@email.com', 'Test First Name', 'Test Last Name', 'Acme LTDA');
    }        
        
    @testSetup
	private static void buildData()
    {
        TestDataFactory.init();
        
        List<Contact> listContact = TestDataFactory.createContactsWithDefaultAccount(1);
        listContact[0].Email = oldcontactEmail;
        update listContact;
        
        List<Lead> listLead = TestDataFactory.createLeadsWithDefaultCompany(1);
        listLead[0].Email = oldleadEmail;
        listLead[0].FirstName = 'John';
        listLead[0].LastName = 'Doe';
        listLead[0].Company = 'Default_Company';
        update listLead;
    }
}