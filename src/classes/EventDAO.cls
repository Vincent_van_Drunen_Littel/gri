/******************************************************************************
* GRI Club - Gathering the world’s leading real estate and infrastructure
* players
******************************************************************************
* Class responsible for accessing org data for Event object
*
* @author  Eric Bueno
* @version 1.0
* @since
* Date      Author       Description
* 15/01/19  EricB  First version.
*
*/

public without sharing class EventDAO {
    
    private static EventDAO instance = new EventDAO();
    
    private EventDAO(){}
    public static EventDAO getInstance(){ return instance; }
    
    public List<Event__c> findOppByEventId (Set<Id> setEventId){
        
        List<Opportunity> listOpp = [
            SELECT
            Id,
            Name,
            Event__c,
            Extra_Financial_Status__c,
            StageName,
            Closing_Stage__c,
            Event__r.Billing_Address__c
            FROM
            Opportunity
            WHERE
            Event__c IN : setEventId
            AND
            Extra_Financial_Status__c NOT IN ('Cancel Kept','Refunded','Replaced')
            AND
            Contact__r.Show_on_Website__c = true
            AND
            RecordType.Name = 'Events'
            AND
            (	
                StageName IN ('Approved','Approved by Finance','Won')
                OR
                (
                    StageName = 'Closing'
                    AND
                    Closing_Stage__c = 'Confirmed'
                )
            )
        ];
        
        return [
            SELECT
            Id,
            Name,
            Billing_Address__c,
            Business_Unit__c,
            Club__c,
            Club__r.Name,
            End_Date__c,
            Event_Description__c,
            Event_Description_Foreign_Language__c,
            Event_Description_Foreign_Language_2__c,
            Event_Time__c,
            Event_Year2__c,
            Finance_Team_Email__c,
            Informal_Name__c,
            Invoice_GRI_Email__c,
            OwnerId,
            Owner.Name,
            Phone__c,
            Publish_on_Website__c,
            Reg_Phone__c,
            RegisterLink__c,
            Sector__c,
            Start_Date__c,
            Status__c,
            Type__c,
            Vip_Dinner_Included__c,
            Website__c,
            (
                SELECT
                Id,
                Name,
                Status__c,
                Publish_Online__c,
                Event__c
                FROM
                Sessions__r
                WHERE
                Event__c IN : setEventId
                AND 
                Status__c = 'Confirmed'
            ),                              
            (
                SELECT
                Id,
                Name,
                Account__c,
                Account__r.File_Name_Marketing_Edited_logo__c,
                Account__r.Marketing_Edited_Logo__c,
                Account__r.URL_External_Logo__c,
                Billing_Country__c,
                Contact__c,
                Contact__r.File_Name_Marketing_Edited_photo__c,
                Contact__r.Marketing_Edited_Photo__c,
                Contact__r.Badge_Name__c,
                Contact__r.Job_title_badge__c,
                Contact__r.Company_Name_Badge__c,
                Contact__r.ContactIDCaseSafe__c,
                Contact__r.URL_External__c,
                Contact__r.MailingCity,
                Contact__r.MailingCountry,
                Contact__r.MailingCountryCode,
                Contact__r.MailingPostalCode,
                Contact__r.MailingState,
                Contact__r.MailingStateCode,
                Contact__r.MailingStreet,
                Contact__r.Specialty_New__c,
                Contact__r.Foreign_Specialty_New__c,
                Contact__r.Foreign_Speciality_2__c,
                Contact__r.Description_New__c,
                Contact__r.Foreign_Description_New__c,
                Contact__r.Foreign_Company_Description_2__c,
                Contact__r.Foreign_Personal_Summary_Biography__c,
                Contact__r.Foreign_Personal_Summary_Biography_2__c,
                Contact__r.Foreign_Keynote_Summary_new__c,
                Contact__r.Foreign_Keynote_Summary_2__c,
                Contact__r.Keynote_Summary_new__c,
                Contact__r.Personal_Summary_Biography__c,
                Contact__r.Phone,
                Event__c,
                Job_Title_Badge__c,
                Opportunity__c,
                Publish_Online__c,
                Publish_Session__c,
                Session_Position__c,
                Session_Status__c,
                Specialty__c,
                Session__c,
                Session__r.Name,
                Session__r.Foreign_title__c,
                Session__r.Foreign_title_2__c,
                Session__r.Description__c,
                Session__r.Language__c,
                Session__r.Foreign_Language_1__c,
                Session__r.Foreign_Language_2__c,
                Session__r.Foreign_description__c,
                Session__r.Foreign_description_2__c,
                Session__r.Program_Indexing__c,
                Session__r.Se__c,
                Session__r.Room__c,
                Session__r.Publish_Online__c,
                Session__r.Title__c,
                Session__r.Translation__c,
                Session__r.Type__c,
                Session__r.Status__c,
                Session__r.Session_indexing__c,
                Session__r.Event__c,
                Program_Feature__c,
                Show_as_Attending_Co_Chair__c
                FROM
                Attendees__r
                WHERE
                Session_Status__c IN ('Interested','Confirmed')
                AND
                Publish_Event_on_Website__c = true
                AND
                Publish_Online__c = true
                AND 
                Event__c IN : setEventId
                AND 
                Opportunity__c IN : listOpp
            ),
            (
                SELECT
                Id,
                Name,
                Account__c,
                Account__r.URL_External_Logo__c,
                Billing_Country__c,
                Contact__c,
                Contact__r.Company_Name_Badge__c,
                Contact__r.URL_External__c,
                Event_Attendees_Status__c,
                Event__c,
                GRI_Club_Member__c,
                Opportunity__c,
                Show_on_Website__c,
                ShowAttendeeonWebsite__c,
                Contact__r.Badge_Name__c,
                Contact__r.Job_title_badge__c,
                Contact__r.ContactIDCaseSafe__c,
                Contact__r.MailingCity,
                Contact__r.MailingCountry,
                Contact__r.MailingCountryCode,
                Contact__r.MailingPostalCode,
                Contact__r.MailingState,
                Contact__r.MailingStateCode,
                Contact__r.MailingStreet,
                Contact__r.Specialty_New__c,
                Contact__r.Foreign_Specialty_New__c,
                Contact__r.Foreign_Speciality_2__c,
                Contact__r.Description_New__c,
                Contact__r.Foreign_Description_New__c,
                Contact__r.Foreign_Company_Description_2__c,
                Contact__r.Foreign_Personal_Summary_Biography__c,
                Contact__r.Foreign_Personal_Summary_Biography_2__c,
                Contact__r.Foreign_Keynote_Summary_new__c,
                Contact__r.Foreign_Keynote_Summary_2__c,
                Contact__r.Keynote_Summary_new__c,
                Contact__r.Personal_Summary_Biography__c,
                Contact__r.Phone
                FROM
                Events_Attendes__r
                WHERE
                ShowAttendeeonWebsite__c = true
                AND
                Event__c IN : setEventId
            )
            FROM
            Event__c
            WHERE
            Id IN : setEventId
            AND 
            Publish_on_Website__c = true
        ];
    }
    
    public List<Event__c> findEventById (Set<Id> setEventId){
        
        return [
            SELECT
            Id,
            Name,
            Billing_Address__c,
            Business_Unit__c,
            Club__c,
            Club__r.Name,
            End_Date__c,
            Event_Description__c,
            Event_Description_Foreign_Language__c,
            Event_Description_Foreign_Language_2__c,
            Event_Time__c,
            Event_Year2__c,
            Finance_Team_Email__c,
            Informal_Name__c,
            Invoice_GRI_Email__c,
            OwnerId,
            Owner.Name,
            Phone__c,
            Publish_on_Website__c,
            Reg_Phone__c,
            RegisterLink__c,
            Sector__c,
            Start_Date__c,
            Status__c,
            Type__c,
            Vip_Dinner_Included__c,
            Website__c
            FROM
            Event__c
            WHERE
            Id IN : setEventId
        ];
    }
    
    public LIst<Campaign> findCampaignsByEventId(Set<Id> setEventId){
        return [
            SELECT
            Id,
            Name,
            Digital_Campaign_Type__c
            FROM
            Campaign
            WHERE
            Digital_Campaign_Type__c != null
            AND
            Event__c IN : setEventId
        ];
    }
    
    public List<Order> findOrderByEventId(Set<Id> setEventId){
        return [
            SELECT
            Id,
            AccountId,
            Account.Name,
            Account.URL_External_Logo__c,
            A4_Company_Profile_New__c,
            Brochure_Programme_Book_Logo_Position_Ne__c,
            Business_Unit2__c,
            Club__c,
            Club__r.Name,
            CompanyAuthorizedById,
            CompanyAuthorizedBy.Name,
            EffectiveDate,
            EndDate,
            Event__c,
            Event_s_Club__c,
            Event_Business_Unit__c,
            Event_Division__c,
            File_Name_Marketing_Edited_logo__c,
            Logo__c,
            OpportunityId,
            Sizing_New__c,
            Sponsor_Type__c,
            Status__c
            FROM
            Order
            WHERE
            Event__c IN : setEventId
            AND
            Status__c =: 'On Going'
            AND
            EndDate >= : System.today()
        ];
    }

    public List<Contact> findContactByContactSet(Set<Contact> contact){
        return [
            SELECT
                Id,
                Name,
                Account.File_Name_Marketing_Edited_logo__c,
                Account.Marketing_Edited_Logo__c,
                Account.URL_External_Logo__c,
                File_Name_Marketing_Edited_photo__c,
                Marketing_Edited_Photo__c,
                Badge_Name__c,
                Job_title_badge__c,
                Company_Name_Badge__c,
                ContactIDCaseSafe__c,
                URL_External__c,
                MailingCity,
                MailingCountry,
                MailingCountryCode,
                MailingPostalCode,
                MailingState,
                MailingStateCode,
                MailingStreet,
                Specialty_New__c,
                Foreign_Specialty_New__c,
                Foreign_Speciality_2__c,
                Description_New__c,
                Foreign_Description_New__c,
                Foreign_Company_Description_2__c,
                Foreign_Personal_Summary_Biography__c,
                Foreign_Personal_Summary_Biography_2__c,
                Foreign_Keynote_Summary_new__c,
                Foreign_Keynote_Summary_2__c,
                Keynote_Summary_new__c,
                Personal_Summary_Biography__c,
                Phone
                FROM
                Contact
                WHERE
                Id IN : contact
        ];
    }
}