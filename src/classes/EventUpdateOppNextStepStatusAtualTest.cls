/*******************************************************************************
*                               Cloud2b - 2016
*-------------------------------------------------------------------------------
*
* Classe teste da classe EventUpdateOppNextStepStatusAtual
*
*
* NAME: EventUpdateOppNextStepStatusAtualTest.cls
* AUTHOR: VMDL                                                 DATE: 15/08/2016
*******************************************************************************/
@isTest
private class EventUpdateOppNextStepStatusAtualTest {

  static testMethod void myUnitTest() 
  {
    Account lAcc = SObjectInstance.createAccount2();
    insert lAcc;

    Contact ctt = SObjectInstance.createContato(lAcc.Id);
    Database.insert(ctt);

    Event__c event = SObjectInstance.createEvent();
    Database.insert(event);

    event.Start_Date__c = system.today().addDays(22);
    event.End_Date__c = system.today().addDays(22);
    update(event);
    
    String lRecId = [ Select id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Open_Events'].Id; 
    
    Opportunity lOpp = SObjectInstance.createOpportunidade(lAcc.Id, lRecId);
    lOpp.ForecastCategoryName = 'Pipeline';
    lOpp.Contact__c = ctt.Id;
    lOpp.Event__c = event.Id;
    lOpp.StageName = 'Suspect';
    insert lOpp;
      
    Event lEvent = SObjectInstance.createEventDefault(lOpp.Id);
 
    Test.startTest();
    	Database.insert( lEvent );
    Test.stopTest();
  }

}