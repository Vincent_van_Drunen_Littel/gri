/*******************************************************************************
*                               Werise - 2019
*-------------------------------------------------------------------------------
*
* Classe de teste para "SetExternallyAvailableImageTrue"
*
* NAME: SetExternallyAvailableImageTrueTest
* AUTHOR: Jefferson F. Presotto                                DATE: 28/03/2019
*******************************************************************************/
@isTest
public with sharing class SetExternallyAvailableImageTrueTest {

//Cria 100 documentos com campo isPublic = false no folder "MarketingEditedFiles"
@testSetup 
static void setup() {
List<Document> documents = new List<Document>();
// insert 10 Documents
for (Integer i=0;i<100;i++) {
documents.add(new Document(name='test '+i,
						FolderId= '00l36000000j90F'));
}
insert documents;
}

//Metodo de teste que executa a classe e compara resultados
static testmethod void test() {        
Test.startTest();
SetExternallyAvailableImageTrue upd = new SetExternallyAvailableImageTrue();
Id batchId = Database.executeBatch(upd);
Test.stopTest();
System.assertEquals(100, [select count() from Document where IsPublic = true]);
}
}