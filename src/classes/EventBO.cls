/******************************************************************************
 * GRI Club - Gathering the world’s leading real estate and infrastructure
 * players
 ******************************************************************************
 * Class responsible for event business rule
 *
 * @author  Eric Bueno
 * @version 1.0
 * @since
 * Date      Author       Description
 * 15/01/19  EricB  First version.
 *
 */

public without sharing class EventBO {

    private static EventBO instance = new EventBO();
    private static EventDAO dao = EventDAO.getInstance();

    private EventBO(){}
    public static EventBO getInstance(){ return instance; }

    public List<Event__c> findOppByEventId (Id eventId){
        return dao.findOppByEventId(new Set<Id>{eventId});
    }

    public List<Event__c> findEventById (Id eventId){
        return dao.findEventById(new Set<Id>{eventId});
    }

    public List<Campaign> findCampaignsByEventId (Id eventId){
        return dao.findCampaignsByEventId(new Set<Id>{eventId});
    }

    public List<Order> findOrderByEventId(Id eventId){
        return dao.findOrderByEventId(new Set<Id>{eventId});
    }

    public List<Contact> findContactByContactSet(Set<Contact> eventId){
    return dao.findContactByContactSet(eventId);
    }

}