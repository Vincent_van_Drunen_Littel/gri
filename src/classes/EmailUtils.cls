global  class EmailUtils {
global static void sendEmail(List<String> recipients,List<String> ccrecipients,Id templateid, ID WhatID, ID TargetObjectId, ID orgwideemailid) { 
        if(recipients == null) return;
        if(recipients.size() == 0) return;
        // Create a new single email message object
        // that will send out a single email to the addresses in the To, CC & BCC list.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        //the email is not saved as an activity.
        //mail.setSaveAsActivity(false);
        // Assign the addresses for the To lists to the mail object.
        mail.setToAddresses(recipients);          
        // Specify the subject line for your email address.
         mail.setCCAddresses(ccrecipients); 
        //mail.setSubject(emailSubject);
        // Set to True if you want to BCC yourself on the email.
        mail.setBccSender(false);
        // The email address of the user executing the Apex Code will be used.
        mail.setUseSignature(false);
        //if (useHTML) {
            // Specify the html content of the email.
        //mail.setHtmlBody(body);
        
        mail.setTemplateId(templateid);
        
        mail.setTargetObjectId(TargetObjectId);
        
        mail.setWhatId(WhatID);
        
        mail.setSaveAsActivity(true);
        
        
        mail.setOrgWideEmailAddressId(orgwideemailid);
        //mail.setReplyTo(replytoaddress);
        //mail.isUserMail()

        
	    try{
	    	 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	    }
		catch(System.EmailException emlEx) {
		system.debug('Email Failed: ' + emlEx);
		}
        
       
        
     
        
        
    }
}