/**
 * Created by Eric Bueno on 24/02/2019.
 */

public without sharing class OpportunityBO {

    private static final OpportunityBO instance = new OpportunityBO();
    private static final CampaignDAO dao = CampaignDAO.getInstance();
    private OpportunityBO(){}
    public static OpportunityBO getInstance(){
        return instance;
    }

    private static final String DIGITAL_FUNNEL_STAGE_AWARENESS = 'Awareness';
    private static final String DIGITAL_FUNNEL_STAGE_INTEREST = 'Interest';
    private static final String DIGITAL_FUNNEL_STAGE_CONSIDERATION = 'Consideration';
    private static final String DIGITAL_FUNNEL_STAGE_INTENT = 'Intent';
    private static final String DIGITAL_FUNNEL_STAGE_PURCHASE = 'Purchase';

    private static Map< String, Integer > mapSequenceDigitalFunnelStage = new Map< String, Integer >{
            DIGITAL_FUNNEL_STAGE_AWARENESS => 1,
            DIGITAL_FUNNEL_STAGE_INTEREST => 2,
            DIGITAL_FUNNEL_STAGE_CONSIDERATION => 3,
            DIGITAL_FUNNEL_STAGE_INTENT => 4,
            DIGITAL_FUNNEL_STAGE_PURCHASE => 5
    };

    public void updateDigitalFunnelStageBefore(List<Opportunity> listOpportunityNew){

        // Instancias
        Set<Id> setContactId = new Set<Id>();
        Set<Id> setOpportunityId = new Set<Id>();
        Map <String, List<Campaign>> mapCampaignByOpportunityContactId = new Map<String, List<Campaign>>();

        // Armazena os ids da campanhas
        for(Opportunity opportunity : listOpportunityNew){
            setContactId.add(opportunity.Contact__c);
            setOpportunityId.add(opportunity.Id);
        }

        // Busca o membro da campanha dos contatos
        for(CampaignMember campaignMember : dao.findCampaignMemberByContactOpportunityId(setOpportunityId, setContactId)){
            String chave = String.valueOf(campaignMember.Opportunity__c) + String.valueOf(campaignMember.ContactId);

            if(!mapCampaignByOpportunityContactId.containsKey(chave)){
                mapCampaignByOpportunityContactId.put( chave,new List<Campaign>());
            }
            mapCampaignByOpportunityContactId.get(chave).add(campaignMember.Campaign);
        }

        // Atribui o Digital Funnel Stage a oportunidade
        for(Opportunity opportunity : listOpportunityNew){

            String chave = String.valueOf(opportunity.Id) + String.valueOf(opportunity.Contact__c);

            List<Campaign> listCampaign = mapCampaignByOpportunityContactId.get(chave);

            // Atualiza o digital stage da oportunidade mantendo o stage da campanha mais avancada
            if(listCampaign != null){
                for(Campaign campaign : listCampaign){
                    updateDigitalStageOpportunityCriteria(opportunity, campaign);
                }
            }

        }

    }

    public Opportunity updateDigitalStageOpportunityCriteria (Opportunity opportunity, Campaign campaign){

        // Verifica se o digital stage da oportunidade e a campanha sao iguais
        if(opportunity.Digital_Funnel_Stage__c == campaign.Digital_Funnel_Stage__c)
        {

            return opportunity;

            // Verifica se o digital stage da oportunidade eh branco
        }else if( String.isBlank(opportunity.Digital_Funnel_Stage__c) ){

            opportunity.Digital_Funnel_Stage__c = campaign.Digital_Funnel_Stage__c;

            // Verifica se a campanha esta em um estagio mais avancado que a oportunidade e atribui o estagio mais avancado a oportunidade
        }else if(mapSequenceDigitalFunnelStage.get(campaign.Digital_Funnel_Stage__c) > mapSequenceDigitalFunnelStage.get(opportunity.Digital_Funnel_Stage__c) ){

            opportunity.Digital_Funnel_Stage__c = campaign.Digital_Funnel_Stage__c;

        }

        return opportunity;

    }



}