@isTest
public class Test_SObjectInstance {
private static ID ID_ACCOUNT = RecordTypeMemory.getRecType('Account', 'Press');
  private static Id REC_OPP_OPEN_EVENTS = RecordTypeMemory.getRecType( 'Opportunity', 'Open_Events' );
  private static Id REC_ORD_ADD_PASSES = RecordTypeMemory.getRecType( 'Order', 'Aditional_Passes' );
  
  @isTest(seeAlldata=true)
  static void Test_SObjectInstance1()
  {
    test.StartTest();
    Event__c evento = SObjectInstance.createEvent();
    Database.insert(evento);
    Account conta = SObjectInstance.createAccount(ID_ACCOUNT);
    Database.insert(conta);

    Contact contato = SObjectInstance.createContato(conta.Id);
    Database.insert(contato);

    Opportunity oportunidade = SObjectInstance.createOpportunidade(conta.Id, REC_OPP_OPEN_EVENTS);
    oportunidade.Contact__c = contato.Id;
    oportunidade.StageName = 'Approved';
    oportunidade.Event__c = evento.Id;
    Database.insert(oportunidade);
    
    Id catelogPriceBookId = SObjectInstance.catalogoDePrecoPadrao2();
    //Pricebook2 priceBookObj = SObjectInstance.catalogoDePrecoPadrao();
    Pricebook2 priceBookObj1 = SObjectInstance.catalogoDePreco();
    PricebookEntry priceBookEntry1 = SObjectInstance.entradaDePreco( catelogPriceBookId,priceBookObj1.Id );
    Product2 productObj = SObjectInstance.createProduct2();
    Account accObj11 = SObjectInstance.createAccount2();
    
    Session__c sessionObj = SObjectInstance.createSession(evento.Id);
    
    Attendee__c accObj = SObjectInstance.createAttendee(evento.Id, sessionObj.Id, oportunidade.Id);
    
    Event_Attendee__c eventAttendeeObj = SObjectInstance.createEventAttendee(oportunidade.Id, conta.id, contato.Id, evento.Id);
    
    Event eventObj = SObjectInstance.createEventDefault(oportunidade.Id);
     
    Magazine__c magzineObj = SObjectInstance.createMagazine();
    
    Club__c clubObj = SObjectInstance.createClub();
    
    Campaign campaignObj = SObjectInstance.createCampaign();
    CampaignMember campaignMbrObj = SObjectInstance.createCampaignMember();
    Task taskObj = SObjectInstance.createTask( oportunidade.Id);
    Opportunity oppObj = SObjectInstance.createOpportunidade(conta.id, REC_OPP_OPEN_EVENTS);
    Opportunity oppObj1 = SObjectInstance.createOpportunidadeMagazine(conta.id, REC_OPP_OPEN_EVENTS,contato.Id);
    Opportunity oppObj2 = SObjectInstance.createOpportunidade2(conta.id, REC_OPP_OPEN_EVENTS,contato.Id, evento.Id);
    Opportunity oppObj3 = SObjectInstance.createOpportunidadeMenber(conta.id, REC_OPP_OPEN_EVENTS,contato.Id, clubObj.Id);
    Opportunity oppObj4 = SObjectInstance.createOpportunidadeSPEX(conta.id, REC_OPP_OPEN_EVENTS, contato.Id);
    OpportunityLineItem oppItemObj = SObjectInstance.createProdutoOportunidade( oportunidade.Id, priceBookEntry1.Id);
    Profile perfil = SObjectInstance.perfil('System Administrator');
    User usuario = SObjectInstance.usuario(perfil.id);
    
    User userObj1 = SObjectInstance.usuario( perfil.id, 'Test Abdc' );
    Id idCatalogoPreco = SObjectInstance.catalogoDePrecoPadrao2();
    
    Order orderObj = SObjectInstance.createorder(REC_ORD_ADD_PASSES, conta.id, idCatalogoPreco, evento.Id);
    OrderItem orderItemObj = SObjectInstance.createOrderItens(orderObj.Id, priceBookEntry1.Id);
     

    Session__c lSession = SObjectInstance.createSession(evento.Id);
    Database.insert(lSession);
    test.StopTest();
  }
  
}