@isTest
public with sharing class OppByProductCode_Test {

	@isTest
	static void test1()
	{
		Test.startTest();

		List<Opportunity> allOpps = [SELECT Id,Name FROM Opportunity];
        List<Product2> prods = [SELECT Id,Name,ProductCode FROM Product2];
		//System.debug('allOpps.size()='+allOpps.size());
        OppByProductCode oppProd = new OppByProductCode(allOpps);
        List<Opportunity> listOppsP001 = oppProd.filter('P-001', 'with');
        List<Opportunity> listOppsAbacate = oppProd.filter('Abacate', 'just');

        System.assert(listOppsP001.size() > 0 , 'List the Opps for the product P-001 is null');
        System.assert(listOppsAbacate.size() > 0 , 'List the Opps for the product Abacate is null');

		Test.stopTest();
	}

	@testSetup  
    static void buildData()
    {
    	// create Opps with many two prod code P-001' and  'Abacate'
        Application_Config__c appConfig = new Application_Config__c(Name='BucketUserId', 
                Value__c=UserInfo.getUserId());
        insert appConfig;
        List<Account> lAccounts = TestDataFactory.createAccounts(5);
        List<Contact> lContacts1 = TestDataFactory.createContacts(lAccounts, 5);
        List<Contact> lContacts2 = TestDataFactory.createContacts(lAccounts, 5);
        List<String> lClubsNames = new List<String>{'RE Brazil Test', 'RE Europe Test', 'Infra India Test'};
        List<Club__c> lClubs = TestDataFactory.createClubs(lClubsNames);
        Product2 prodP001 = TestDataFactory.createProduct('P-001');
        Product2 prodAbacate = TestDataFactory.createProduct('Abacate');
        Product2 prodSorvete = TestDataFactory.createProduct('Sorvete');
        Id standardPricebookId = Test.getStandardPricebookId();
        PricebookEntry pricebookAbacate = TestDataFactory.createPricebookEntry(prodAbacate);
        PricebookEntry pricebookP001 = TestDataFactory.createPricebookEntry(prodP001);
        PricebookEntry pricebookSorvete = TestDataFactory.createPricebookEntry(prodSorvete);
        Event__c amazingEvent = TestDataFactory.createEvent('Amazing Event', lClubs[2]);

        List<Opportunity> lOppMemb = TestDataFactory.createMembershipOpps(lContacts1, lClubs[0], System.today()+10);
        TestDataFactory.addProductToOpportunity(pricebookP001, prodP001, lOppMemb);                
        List<Opportunity> lOppEvent = TestDataFactory.createEventOpps(lContacts2, amazingEvent, lClubs[2], System.today()+10);
        TestDataFactory.addProductToOpportunity(pricebookAbacate, prodAbacate, lOppEvent);        

/*
        System.debug('%%%%listContacts='+listContacts);
        System.assertEquals(10, listContacts.size(), '**************');

        List<String> lClubsNames = new List<String>{'RE Brazil Test', 'RE Europe Test', 'Infra India Test'};
        List<Club__c> lClubs = TestDataFactory.createClubs(lClubsNames);
        Product2 prodP001 = TestDataFactory.createProduct('P-001');
        Product2 prodAbacate = TestDataFactory.createProduct('Abacate');
        Id standardPricebookId = Test.getStandardPricebookId();
        PricebookEntry pricebookAbacate = TestDataFactory.createPricebookEntry(prodAbacate);
        PricebookEntry pricebookP001 = TestDataFactory.createPricebookEntry(prodP001);
        
        List<Opportunity> lOppMemb = TestDataFactory.createMembershipOpps(listContacts, lClubs[0], System.today()+10);
        TestDataFactory.addProductToOpportunity(pricebookP001, prodP001, lOppMemb);
        List<Opportunity> lOppEvent = TestDataFactory.createEventOpps(listContacts, lClubs[0], System.today()+10);
        TestDataFactory.addProductToOpportunity(pricebookAbacate, prodAbacate, lOppEvent);
*/
    }
}